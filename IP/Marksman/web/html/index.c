/* Created with PageBuilder version 4.04 on Fri Jul 30 13:38:17 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ************************************* */
/* *    Built from "html\index.htm"    * */
/* ************************************* */

extern rpObjectDescription Pgindex;

static char Pgindex_Item_1[] =
	C_oHTML;

static char Pgindex_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern char *getProductName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpTextDisplayItem htmlProductName = {
	(void *) getProductName,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	C_xHEAD C_oBODY ">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

char htmlLogo[] =
	"\n"
	C_oANCHOR_HREF "http://www.foxjet.com/\"><img" C_ALT "Foxjet, Inc\" "
	"src=\"/Images/logo.gif\"" C_BORDER "\"0\"/>"
	C_xANCHOR;

static char Pgindex_Item_6[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgindex_Item_8[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

static char Pgindex_Item_10[] =
	"<table class=\"statusbox\"" C_BORDER "\"0\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\"" C_ALIGN_CENTER C_WIDTH "\"100%\">\n"
	C_oTR "\n"
	C_oTD_ALIGN_LEFT ">\n";

extern char *getMessageText(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_11 = {
	(void *) getMessageText,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_12[] =
	C_xTD C_xTR
	C_xTABLE;

rpItem htmlStatusMsgs_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_10 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_11 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_12 }, 
	{ eRpItemType_LastItemInList } 
};

extern void repeatWhileMoreStatusMessages(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
rpRepeatGroupDynItem htmlStatusMsgs = {
	(void *) repeatWhileMoreStatusMessages,
	htmlStatusMsgs_Group
};

static char Pgindex_Item_13[] =
	"<!-- ERROR MESSAGES -->\n";

static char Pgindex_Item_15[] =
	"<table class=\"errorbox\"" C_BORDER "\"0\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\"" C_ALIGN_CENTER C_WIDTH "\"100%\">\n"
	C_oTR "\n"
	C_oTD_ALIGN_LEFT ">\n";

extern char *getMessageText(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_16 = {
	(void *) getMessageText,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_17[] =
	C_xTD C_xTR
	C_xTABLE;

rpItem htmlErrorMsgs_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_15 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_16 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_17 }, 
	{ eRpItemType_LastItemInList } 
};

extern void repeatWhileMoreErrorMessages(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
rpRepeatGroupDynItem htmlErrorMsgs = {
	(void *) repeatWhileMoreErrorMessages,
	htmlErrorMsgs_Group
};

static char Pgindex_Item_18[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- MENU -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

static char Pgindex_Item_20[] =
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	"<td nowrap=\"\">\n";

static char Pgindex_Item_22[] =
	"\n"
	C_oANCHOR_HREF "\n";

extern char *html_getMenuItemUrl(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_23 = {
	(void *) html_getMenuItemUrl,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_24[] =
	"\">\n";

extern char *html_getMenuItemName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_25 = {
	(void *) html_getMenuItemName,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_26[] =
	C_xANCHOR "<br/>\n";

static rpItem Pgindex_Item_21_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_22 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_23 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_24 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_25 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_26 }, 
	{ eRpItemType_LastItemInList } 
};

extern void html_getMenuListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
static rpRepeatGroupDynItem Pgindex_Item_21 = {
	(void *) html_getMenuListWhile,
	Pgindex_Item_21_Group
};

static char Pgindex_Item_27[] =
	C_xTD C_xTR
	C_xTABLE;

rpItem htmlMenu[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_20 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &Pgindex_Item_21 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_27 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgindex_Item_28[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- PAGE CONTENT -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Home" C_xH1 C_oP "Welcome to the "
	"management and configuration web interface.\n"
	C_oP "You can use the navigation menus on the left to access additional"
	" pages.\n"
	C_oP C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\"" C_CELLSPACING
	"\"0\">\n"
	C_oTR "\n";

static char Pgindex_Item_29[] =
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Name:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern rpTextDisplayItem htmlProductName;

static char Pgindex_Item_31[] =
	C_xTD C_xTR C_oTR C_oTD C_NBSP C_xTD C_xTR "<!-- blank line -->\n";

static char Pgindex_Item_34[] =
	C_oTR "\n"
	C_oTD C_oH3 "Ethernet" C_xH3 C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">MAC Address:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_35 = {
	(void *) getMacAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_36[] =
	C_xTD C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">IPv4 Addresses:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_37 = {
	(void *) getCurrentIpAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_38[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_39 = {
	(void *) getCurrentAutoIpAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_40[] =
	C_xTD C_xTR C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">IPv6 Addresses:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_41 = {
	(void *) getCurrentIv6pAddress1,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_42[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_43 = {
	(void *) getCurrentIv6pAddress2,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_44[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_45 = {
	(void *) getCurrentIv6pAddress3,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_46[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_47 = {
	(void *) getCurrentIv6pAddress4,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_48[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_49 = {
	(void *) getCurrentIv6pAddress5,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_50[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_51 = {
	(void *) getCurrentIv6pAddress6,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_52[] =
	C_xTD C_xTR C_oTR C_oTD C_NBSP C_xTD C_xTR "<!-- blank line -->\n"
	"<!-- "
	"//////////////////////////////////////////////////////////////////////"
	"////////////////////////////// -->\n"
	C_oTR "\n"
	C_oTD C_oH3 "Controller settings" C_xH3 C_xTD C_oTD C_NBSP C_xTD C_oTD
	C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">Selected message:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSelectedMessage(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_53 = {
	(void *) getSelectedMessage,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_54[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">GROUP_NAME:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getGROUP_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_55 = {
	(void *) getGROUP_NAME,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_56[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">GROUP_COUNT:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getGROUP_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_57 = {
	(void *) getGROUP_COUNT,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_58[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">GROUP_IDS:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getGROUP_IDS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_59 = {
	(void *) getGROUP_IDS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_60[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">GROUP_IPADDRS:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getGROUP_IPADDRS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_61 = {
	(void *) getGROUP_IPADDRS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_62[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">UNITS:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getUNITS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_63 = {
	(void *) getUNITS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_64[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">TIME:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getTIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_65 = {
	(void *) getTIME,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_66[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">ENCODER:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getENCODER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_67 = {
	(void *) getENCODER,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_68[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">ENCODER_COUNT:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getENCODER_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_69 = {
	(void *) getENCODER_COUNT,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_70[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">ENCODER_WHEEL:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getENCODER_WHEEL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_71 = {
	(void *) getENCODER_WHEEL,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_72[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">ENCODER_DIVISOR:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getENCODER_DIVISOR(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_73 = {
	(void *) getENCODER_DIVISOR,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_74[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">CONVEYOR_SPEED:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getCONVEYOR_SPEED(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_75 = {
	(void *) getCONVEYOR_SPEED,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_76[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">PRINT_RESOLUTION:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getPRINT_RESOLUTION(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_77 = {
	(void *) getPRINT_RESOLUTION,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_78[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">INK_SIZE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getINK_SIZE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_79 = {
	(void *) getINK_SIZE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_80[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">INK_COST:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getINK_COST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_81 = {
	(void *) getINK_COST,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_82[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">SYSTEM_TIME:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSYSTEM_TIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_83 = {
	(void *) getSYSTEM_TIME,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_84[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">INTERNAL_CLOCK:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getINTERNAL_CLOCK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_85 = {
	(void *) getINTERNAL_CLOCK,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_86[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">TIME_ZONE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getTIME_ZONE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_87 = {
	(void *) getTIME_ZONE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_88[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">OBSERVE_USA_DST:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getOBSERVE_USA_DST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_89 = {
	(void *) getOBSERVE_USA_DST,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_90[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">FIRST_DAY:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getFIRST_DAY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_91 = {
	(void *) getFIRST_DAY,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_92[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DAYS_WEEK1:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDAYS_WEEK1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_93 = {
	(void *) getDAYS_WEEK1,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_94[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">WEEK53:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWEEK53(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_95 = {
	(void *) getWEEK53,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_96[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">ROLLOVER_HOURS:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getROLLOVER_HOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_97 = {
	(void *) getROLLOVER_HOURS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_98[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFT1:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFT1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_99 = {
	(void *) getSHIFT1,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_100[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFT2:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFT2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_101 = {
	(void *) getSHIFT2,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_102[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFT3:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFT3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_103 = {
	(void *) getSHIFT3,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_104[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFTCODE1:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFTCODE1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_105 = {
	(void *) getSHIFTCODE1,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_106[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFTCODE2:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFTCODE2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_107 = {
	(void *) getSHIFTCODE2,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_108[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">SHIFTCODE3:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSHIFTCODE3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_109 = {
	(void *) getSHIFTCODE3,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_110[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">DAYS:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getDAYS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_111 = {
	(void *) getDAYS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_112[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">MONTHS:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getMONTHS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_113 = {
	(void *) getMONTHS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_114[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DAYS_SPECIAL:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDAYS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_115 = {
	(void *) getDAYS_SPECIAL,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_116[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">MONTHS_SPECIAL:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getMONTHS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_117 = {
	(void *) getMONTHS_SPECIAL,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_118[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">HOURS:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getHOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_119 = {
	(void *) getHOURS,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_120[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">AMPM:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getAMPM(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_121 = {
	(void *) getAMPM,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_122[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">IOBOX:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getIOBOX(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_123 = {
	(void *) getIOBOX,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_124[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">IOBOX_IP:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getIOBOX_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_125 = {
	(void *) getIOBOX_IP,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_126[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">IOBOX_STROBE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getIOBOX_STROBE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_127 = {
	(void *) getIOBOX_STROBE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_128[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">INTERNET_CONTROL:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getINTERNET_CONTROL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_129 = {
	(void *) getINTERNET_CONTROL,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_130[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DNS_IP:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDNS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_131 = {
	(void *) getDNS_IP,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_132[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">EMAIL_IP:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getEMAIL_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_133 = {
	(void *) getEMAIL_IP,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_134[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">EMAIL_SERVER:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getEMAIL_SERVER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_135 = {
	(void *) getEMAIL_SERVER,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_136[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">EMAIL_SOURCE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getEMAIL_SOURCE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_137 = {
	(void *) getEMAIL_SOURCE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_138[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">EMAIL_INK:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getEMAIL_INK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_139 = {
	(void *) getEMAIL_INK,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_140[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">EMAIL_SERVICE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getEMAIL_SERVICE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_141 = {
	(void *) getEMAIL_SERVICE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_142[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">WINS_IP:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWINS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_143 = {
	(void *) getWINS_IP,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_144[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">NETBIOS_TYPE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getNETBIOS_TYPE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_145 = {
	(void *) getNETBIOS_TYPE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_146[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">BC_N:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getBC_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_147 = {
	(void *) getBC_N,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_148[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_NAME:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_149 = {
	(void *) getBC_NAME,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_150[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_TY:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_TY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_151 = {
	(void *) getBC_TY,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_152[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_H:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_153 = {
	(void *) getBC_H,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_154[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_CD:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_CD(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_155 = {
	(void *) getBC_CD,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_156[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_HB:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_157 = {
	(void *) getBC_HB,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_158[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_VB:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_VB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_159 = {
	(void *) getBC_VB,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_160[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_QZ:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_QZ(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_161 = {
	(void *) getBC_QZ,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_162[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_BASE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_BASE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_163 = {
	(void *) getBC_BASE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_164[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_B1:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_B1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_165 = {
	(void *) getBC_B1,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_166[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_B2:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_B2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_167 = {
	(void *) getBC_B2,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_168[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_B3:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_B3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_169 = {
	(void *) getBC_B3,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_170[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">BC_B4:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getBC_B4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_171 = {
	(void *) getBC_B4,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_172[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">DM_N:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getDM_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_173 = {
	(void *) getDM_N,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_174[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DM_NAME:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDM_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_175 = {
	(void *) getDM_NAME,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_176[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DM_H:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDM_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_177 = {
	(void *) getDM_H,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_178[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DM_HB:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDM_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_179 = {
	(void *) getDM_HB,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_180[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DM_W:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDM_W(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_181 = {
	(void *) getDM_W,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_182[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">DM_WB:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getDM_WB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_183 = {
	(void *) getDM_WB,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_184[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">PRINT_MODE:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getPRINT_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_185 = {
	(void *) getPRINT_MODE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_186[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "<td nowrap=\"\""
	C_ALIGN_RIGHT ">APS_MODE:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getAPS_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_187 = {
	(void *) getAPS_MODE,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_188[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTD C_NBSP C_xTD
	C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\"" C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_READY_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_READY_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_189 = {
	(void *) getSTROBE_MODE_READY_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_190[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_READY_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_READY_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_191 = {
	(void *) getSTROBE_MODE_READY_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_192[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_READY_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_READY_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_193 = {
	(void *) getSTROBE_MODE_READY_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_194[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_APS_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_APS_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_195 = {
	(void *) getSTROBE_MODE_APS_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_196[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_APS_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_APS_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_197 = {
	(void *) getSTROBE_MODE_APS_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_198[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_APS_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_APS_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_199 = {
	(void *) getSTROBE_MODE_APS_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_200[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_LOWTEMP_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWTEMP_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_201 = {
	(void *) getSTROBE_MODE_LOWTEMP_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_202[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_LOWTEMP_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWTEMP_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_203 = {
	(void *) getSTROBE_MODE_LOWTEMP_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_204[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_LOWTEMP_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWTEMP_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_205 = {
	(void *) getSTROBE_MODE_LOWTEMP_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_206[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_HV_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_HV_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_207 = {
	(void *) getSTROBE_MODE_HV_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_208[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_HV_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_HV_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_209 = {
	(void *) getSTROBE_MODE_HV_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_210[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_HV_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_HV_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_211 = {
	(void *) getSTROBE_MODE_HV_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_212[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_LOWINK_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_213 = {
	(void *) getSTROBE_MODE_LOWINK_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_214[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_LOWINK_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_215 = {
	(void *) getSTROBE_MODE_LOWINK_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_216[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_LOWINK_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_LOWINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_217 = {
	(void *) getSTROBE_MODE_LOWINK_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_218[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_OUTOFINK_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_OUTOFINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_219 = {
	(void *) getSTROBE_MODE_OUTOFINK_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_220[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_OUTOFINK_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_OUTOFINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_221 = {
	(void *) getSTROBE_MODE_OUTOFINK_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_222[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_OUTOFINK_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_OUTOFINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_223 = {
	(void *) getSTROBE_MODE_OUTOFINK_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_224[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">STROBE_MODE_DRIVER_G:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_DRIVER_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_225 = {
	(void *) getSTROBE_MODE_DRIVER_G,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_226[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_DRIVER_Y:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_DRIVER_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_227 = {
	(void *) getSTROBE_MODE_DRIVER_Y,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_228[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD "<td nowrap=\"\""
	C_ALIGN_RIGHT ">STROBE_MODE_DRIVER_R:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getSTROBE_MODE_DRIVER_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_229 = {
	(void *) getSTROBE_MODE_DRIVER_R,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_230[] =
	C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD C_xTR C_xTABLE C_oP
	C_oTABLE_BORDER "\"1\"" C_CELLPADDING "\"2\"" C_CELLSPACING "\"0\">\n"
	C_oTR C_oTD C_oH4 "Index" C_xH4 C_oTD C_oH4 "Dynamic data" C_xH4 C_xTD
	C_oTD C_oH4 "Index" C_xH4 C_xTD C_oTD C_oH4 "Dynamic data" C_xH4 C_xTD
	C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">1: " C_xTD
	C_oTD;

extern char *getDYNDATA1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_231 = {
	(void *) getDYNDATA1,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_232[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">26:" C_xTD
	C_oTD;

extern char *getDYNDATA26(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_233 = {
	(void *) getDYNDATA26,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_234[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">2: " C_xTD
	C_oTD;

extern char *getDYNDATA2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_235 = {
	(void *) getDYNDATA2,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_236[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">27:" C_xTD
	C_oTD;

extern char *getDYNDATA27(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_237 = {
	(void *) getDYNDATA27,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_238[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">3: " C_xTD
	C_oTD;

extern char *getDYNDATA3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_239 = {
	(void *) getDYNDATA3,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_240[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">28:" C_xTD
	C_oTD;

extern char *getDYNDATA28(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_241 = {
	(void *) getDYNDATA28,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_242[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">4: " C_xTD
	C_oTD;

extern char *getDYNDATA4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_243 = {
	(void *) getDYNDATA4,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_244[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">29:" C_xTD
	C_oTD;

extern char *getDYNDATA29(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_245 = {
	(void *) getDYNDATA29,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_246[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">5: " C_xTD
	C_oTD;

extern char *getDYNDATA5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_247 = {
	(void *) getDYNDATA5,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_248[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">30:" C_xTD
	C_oTD;

extern char *getDYNDATA30(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_249 = {
	(void *) getDYNDATA30,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_250[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">6: " C_xTD
	C_oTD;

extern char *getDYNDATA6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_251 = {
	(void *) getDYNDATA6,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_252[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">31:" C_xTD
	C_oTD;

extern char *getDYNDATA31(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_253 = {
	(void *) getDYNDATA31,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_254[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">7: " C_xTD
	C_oTD;

extern char *getDYNDATA7(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_255 = {
	(void *) getDYNDATA7,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_256[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">32:" C_xTD
	C_oTD;

extern char *getDYNDATA32(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_257 = {
	(void *) getDYNDATA32,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_258[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">8: " C_xTD
	C_oTD;

extern char *getDYNDATA8(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_259 = {
	(void *) getDYNDATA8,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_260[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">33:" C_xTD
	C_oTD;

extern char *getDYNDATA33(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_261 = {
	(void *) getDYNDATA33,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_262[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">9: " C_xTD
	C_oTD;

extern char *getDYNDATA9(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_263 = {
	(void *) getDYNDATA9,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_264[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">34:" C_xTD
	C_oTD;

extern char *getDYNDATA34(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_265 = {
	(void *) getDYNDATA34,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_266[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">10:" C_xTD
	C_oTD;

extern char *getDYNDATA10(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_267 = {
	(void *) getDYNDATA10,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_268[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">35:" C_xTD
	C_oTD;

extern char *getDYNDATA35(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_269 = {
	(void *) getDYNDATA35,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_270[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">11:" C_xTD
	C_oTD;

extern char *getDYNDATA11(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_271 = {
	(void *) getDYNDATA11,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_272[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">36:" C_xTD
	C_oTD;

extern char *getDYNDATA36(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_273 = {
	(void *) getDYNDATA36,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_274[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">12:" C_xTD
	C_oTD;

extern char *getDYNDATA12(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_275 = {
	(void *) getDYNDATA12,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_276[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">37:" C_xTD
	C_oTD;

extern char *getDYNDATA37(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_277 = {
	(void *) getDYNDATA37,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_278[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">13:" C_xTD
	C_oTD;

extern char *getDYNDATA13(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_279 = {
	(void *) getDYNDATA13,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_280[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">38:" C_xTD
	C_oTD;

extern char *getDYNDATA38(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_281 = {
	(void *) getDYNDATA38,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_282[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">14:" C_xTD
	C_oTD;

extern char *getDYNDATA14(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_283 = {
	(void *) getDYNDATA14,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_284[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">39:" C_xTD
	C_oTD;

extern char *getDYNDATA39(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_285 = {
	(void *) getDYNDATA39,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_286[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">15:" C_xTD
	C_oTD;

extern char *getDYNDATA15(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_287 = {
	(void *) getDYNDATA15,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_288[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">40:" C_xTD
	C_oTD;

extern char *getDYNDATA40(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_289 = {
	(void *) getDYNDATA40,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_290[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">16:" C_xTD
	C_oTD;

extern char *getDYNDATA16(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_291 = {
	(void *) getDYNDATA16,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_292[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">41:" C_xTD
	C_oTD;

extern char *getDYNDATA41(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_293 = {
	(void *) getDYNDATA41,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_294[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">17:" C_xTD
	C_oTD;

extern char *getDYNDATA17(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_295 = {
	(void *) getDYNDATA17,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_296[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">42:" C_xTD
	C_oTD;

extern char *getDYNDATA42(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_297 = {
	(void *) getDYNDATA42,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_298[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">18:" C_xTD
	C_oTD;

extern char *getDYNDATA18(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_299 = {
	(void *) getDYNDATA18,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_300[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">43:" C_xTD
	C_oTD;

extern char *getDYNDATA43(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_301 = {
	(void *) getDYNDATA43,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_302[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">19:" C_xTD
	C_oTD;

extern char *getDYNDATA19(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_303 = {
	(void *) getDYNDATA19,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_304[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">44:" C_xTD
	C_oTD;

extern char *getDYNDATA44(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_305 = {
	(void *) getDYNDATA44,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_306[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">20:" C_xTD
	C_oTD;

extern char *getDYNDATA20(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_307 = {
	(void *) getDYNDATA20,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_308[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">45:" C_xTD
	C_oTD;

extern char *getDYNDATA45(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_309 = {
	(void *) getDYNDATA45,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_310[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">21:" C_xTD
	C_oTD;

extern char *getDYNDATA21(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_311 = {
	(void *) getDYNDATA21,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_312[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">46:" C_xTD
	C_oTD;

extern char *getDYNDATA46(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_313 = {
	(void *) getDYNDATA46,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_314[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">22:" C_xTD
	C_oTD;

extern char *getDYNDATA22(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_315 = {
	(void *) getDYNDATA22,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_316[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">47:" C_xTD
	C_oTD;

extern char *getDYNDATA47(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_317 = {
	(void *) getDYNDATA47,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_318[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">23:" C_xTD
	C_oTD;

extern char *getDYNDATA23(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_319 = {
	(void *) getDYNDATA23,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_320[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">48:" C_xTD
	C_oTD;

extern char *getDYNDATA48(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_321 = {
	(void *) getDYNDATA48,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_322[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">24:" C_xTD
	C_oTD;

extern char *getDYNDATA24(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_323 = {
	(void *) getDYNDATA24,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_324[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">49:" C_xTD
	C_oTD;

extern char *getDYNDATA49(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_325 = {
	(void *) getDYNDATA49,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_326[] =
	C_xTD C_xTR C_oTR "<td nowrap=\"\"" C_ALIGN_RIGHT ">25:" C_xTD
	C_oTD;

extern char *getDYNDATA25(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_327 = {
	(void *) getDYNDATA25,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_328[] =
	C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">50:" C_xTD
	C_oTD;

extern char *getDYNDATA50(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_329 = {
	(void *) getDYNDATA50,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_330[] =
	C_xTD C_xTR C_xTABLE C_oP C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\""
	C_CELLSPACING "\"0\">\n"
	"<!-- "
	"//////////////////////////////////////////////////////////////////////"
	"////////////////////////////// -->\n";

static rpItem Pgindex_Item_33[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_34 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_35 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_36 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_37 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_38 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_39 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_40 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_41 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_42 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_43 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_44 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_45 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_46 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_47 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_48 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_49 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_50 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_51 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_52 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_53 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_54 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_55 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_56 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_57 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_58 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_59 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_60 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_61 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_62 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_63 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_64 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_65 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_66 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_67 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_68 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_69 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_70 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_71 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_72 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_73 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_74 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_75 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_76 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_77 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_78 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_79 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_80 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_81 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_82 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_83 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_84 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_85 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_86 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_87 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_88 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_89 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_90 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_91 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_92 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_93 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_94 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_95 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_96 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_97 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_98 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_99 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_100 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_101 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_102 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_103 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_104 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_105 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_106 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_107 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_108 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_109 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_110 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_111 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_112 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_113 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_114 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_115 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_116 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_117 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_118 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_119 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_120 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_121 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_122 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_123 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_124 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_125 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_126 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_127 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_128 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_129 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_130 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_131 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_132 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_133 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_134 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_135 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_136 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_137 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_138 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_139 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_140 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_141 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_142 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_143 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_144 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_145 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_146 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_147 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_148 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_149 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_150 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_151 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_152 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_153 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_154 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_155 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_156 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_157 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_158 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_159 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_160 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_161 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_162 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_163 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_164 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_165 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_166 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_167 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_168 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_169 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_170 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_171 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_172 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_173 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_174 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_175 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_176 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_177 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_178 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_179 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_180 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_181 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_182 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_183 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_184 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_185 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_186 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_187 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_188 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_189 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_190 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_191 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_192 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_193 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_194 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_195 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_196 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_197 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_198 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_199 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_200 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_201 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_202 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_203 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_204 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_205 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_206 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_207 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_208 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_209 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_210 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_211 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_212 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_213 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_214 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_215 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_216 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_217 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_218 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_219 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_220 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_221 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_222 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_223 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_224 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_225 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_226 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_227 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_228 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_229 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_230 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_231 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_232 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_233 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_234 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_235 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_236 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_237 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_238 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_239 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_240 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_241 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_242 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_243 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_244 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_245 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_246 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_247 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_248 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_249 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_250 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_251 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_252 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_253 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_254 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_255 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_256 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_257 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_258 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_259 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_260 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_261 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_262 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_263 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_264 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_265 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_266 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_267 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_268 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_269 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_270 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_271 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_272 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_273 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_274 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_275 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_276 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_277 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_278 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_279 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_280 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_281 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_282 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_283 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_284 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_285 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_286 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_287 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_288 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_289 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_290 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_291 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_292 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_293 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_294 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_295 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_296 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_297 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_298 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_299 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_300 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_301 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_302 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_303 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_304 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_305 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_306 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_307 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_308 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_309 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_310 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_311 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_312 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_313 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_314 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_315 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_316 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_317 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_318 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_319 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_320 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_321 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_322 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_323 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_324 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_325 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_326 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_327 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_328 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_329 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_330 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgindex_Item_332[] =
	"<!-- No Ethernet -->\n";

static rpItem Pgindex_Item_331[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_332 }, 
	{ eRpItemType_LastItemInList } 
};

rpItem htmlEthernetSupported_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_33 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_331 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_ETHERNET_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
rpDynamicDisplayItem htmlEthernetSupported = {
	(void *) IS_ETHERNET_SUPPORTED,
	eRpVarType_Complex,
	2,
	htmlEthernetSupported_Group
};

static char Pgindex_Item_335[] =
	C_oTR "\n"
	C_oTD C_oH3 "Wireless" C_xH3 C_xTD C_oTD C_NBSP C_xTD C_oTD C_NBSP
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Wireless Network:" C_xTD C_oTD
	C_NBSP C_xTD
	C_oTD;

extern char *getWirelessConnection(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_336 = {
	(void *) getWirelessConnection,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_337[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">MAC Address:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getWirelessMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_338 = {
	(void *) getWirelessMacAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_339[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">IPv4 Addresses:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getWirelessCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_340 = {
	(void *) getWirelessCurrentIpAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_341[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_342 = {
	(void *) getWirelessCurrentAutoIpAddress,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_343[] =
	C_xTD C_xTR C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">IPv6 Addresses:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_344 = {
	(void *) getWirelessCurrentIv6pAddress1,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_345[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_346 = {
	(void *) getWirelessCurrentIv6pAddress2,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_347[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_348 = {
	(void *) getWirelessCurrentIv6pAddress3,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_349[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_350 = {
	(void *) getWirelessCurrentIv6pAddress4,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_351[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_352 = {
	(void *) getWirelessCurrentIv6pAddress5,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_353[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_NBSP C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getWirelessCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_354 = {
	(void *) getWirelessCurrentIv6pAddress6,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgindex_Item_355[] =
	C_xTD C_xTR C_oTR C_oTD C_NBSP C_xTD C_xTR "<!-- blank line -->\n";

static rpItem Pgindex_Item_334[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_335 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_336 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_337 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_338 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_339 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_340 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_341 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_342 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_343 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_344 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_345 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_346 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_347 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_348 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_349 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_350 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_351 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_352 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_353 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_354 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_355 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgindex_Item_357[] =
	"<!-- No Ethernet -->\n";

static rpItem Pgindex_Item_356[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_357 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgindex_Item_333_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_334 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_356 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_WIRELESS_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgindex_Item_333 = {
	(void *) IS_WIRELESS_SUPPORTED,
	eRpVarType_Complex,
	2,
	Pgindex_Item_333_Group
};

static char Pgindex_Item_360[] =
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">CPU Utilization:" C_xTD C_oTD C_NBSP
	C_xTD
	C_oTD;

extern Signed32 getCpuUtilization(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_361 = {
	(void *) getCpuUtilization,
	eRpVarType_Complex,
	eRpTextType_Signed32,
	20
};

static char Pgindex_Item_362[] =
	"%\n"
	C_xTD
	C_xTR;

static rpItem Pgindex_Item_359[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_360 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_361 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_362 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgindex_Item_364[] =
	"<!-- No CPU Utilization Monitor -->\n";

static rpItem Pgindex_Item_363[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_364 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgindex_Item_358_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_359 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgindex_Item_363 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 getIsCPUMonitorEnabled(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgindex_Item_358 = {
	(void *) getIsCPUMonitorEnabled,
	eRpVarType_Complex,
	2,
	Pgindex_Item_358_Group
};

static char Pgindex_Item_365[] =
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Up Time:" C_xTD C_oTD C_NBSP C_xTD
	C_oTD;

extern char *getUpTime(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgindex_Item_366 = {
	(void *) getUpTime,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgindex_Item_367[] =
	C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE C_xTD C_xTR
	C_xTABLE;

char htmlCopyright[] =
	"\n"
	"<table" C_WIDTH "\"100%\"" C_BORDER "\"0\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	C_oTD_ALIGN_CENTER " class=\"COPYRIGHT\">\n"
	"Copyright &copy; 1996-2010 Foxjet, Inc. All rights reserved.\n"
	C_xTD C_xTR
	C_xTABLE;

static char Pgindex_Item_369[] =
	C_xBODY_xHTML;


static rpItem Pgindex_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_6 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_8 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_13 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_18 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_28 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_29 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_31 }, 
	{ eRpItemType_DynamicDisplay, (void *) &htmlEthernetSupported }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgindex_Item_333 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgindex_Item_358 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_365 }, 
	{ eRpItemType_DisplayText, (void *) &Pgindex_Item_366 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_367 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgindex_Item_369 }, 
	{ eRpItemType_LastItemInList } 
};


extern void indexPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgindex_ObjectExtension = {
	indexPagePreProc,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgindex = {
	"/index.htm",
	Pgindex_Items,
	&Pgindex_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
