/* Created with PageBuilder version 4.04 on Fri Jul 30 13:18:19 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* *************************************** */
/* *    Built from "scripts\utils.js"    * */
/* *************************************** */

extern rpObjectDescription Pgutils;

static char Pgutils_Item_1[] =
	"// Open online help window.\n"
	"// The page parameter is the name of help file to load, relative to "
	"the help directory.\n"
	"// Example: openHelp(\'port_config_hlp.htm\')\n"
	"function openHelp() {\n"
	"var helpWindow = "
	"window.open(\'/help.htm\',\'CanonRemoteProjectorHelpWindow\',\'width=7"
	"00,height=600,scrollbars=yes,resizable=yes,status=yes,menubar=no,toolb"
	"ar=no\');\n"
	"helpWindow.focus();\n"
	"}\n"
	"// Close online help window\n"
	"function closeHelp() {\n"
	"var helpWindow = "
	"window.open(\"/help.htm\",\'CanonRemoteProjectorHelpWindow\',\'width=0";

static char Pgutils_Item_2[] =
	",height=0,scrollbars=yes,resizable=yes,status=yes,menubar=no,toolbar=n"
	"o\');\n"
	"helpWindow.close();\n"
	"}\n"
	"// Cross browser access to HTML elements.\n"
	"// This function gets the HTML element by name and returns an object\n"
	"// that contains properties for both the html element object and the "
	"style.\n"
	"// Usage:\n"
	"// var x = new getObj(\'objectname\');\n"
	"// x.obj.innerHTML = \"Howdy\";\n"
	"// x.style.top = \'20px\';\n"
	"function getObj(name) {\n"
	"if (document.getElementById) { // IE 5+\n"
	"this.obj = document.getElementById(name);\n";

static char Pgutils_Item_3[] =
	"if (document.getElementById(name))\n"
	"this.style = document.getElementById(name).style;\n"
	"}\n"
	"else if (document.all) { // IE 4+\n"
	"this.obj = document.all[name];\n"
	"if (document.all[name])\n"
	"this.style = document.all[name].style;\n"
	"}\n"
	"else if (document.layers) { // NS 4\n"
	"this.obj = document.layers[name];\n"
	"this.style = document.layers[name];\n"
	"}\n"
	"}\n";


static rpItem Pgutils_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgutils_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgutils_Item_2 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgutils_Item_3 }, 
	{ eRpItemType_LastItemInList } 
};


rpObjectDescription Pgutils = {
	"/utils.js",
	Pgutils_Items,
	(rpObjectExtensionPtr) 0,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeJs,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
