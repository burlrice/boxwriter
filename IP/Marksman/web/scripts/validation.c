/* Created with PageBuilder version 4.04 on Fri Jul 30 13:18:19 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ******************************************** */
/* *    Built from "scripts\validation.js"    * */
/* ******************************************** */

extern rpObjectDescription Pgvalidation;

static char Pgvalidation_Item_1[] =
	"//\n"
	"// Validation functions\n"
	"//\n"
	"// These validation functions are intended primarily for the "
	"validation of user\n"
	"// input in form controls. Most work with string values.\n"
	"//\n"
	"function isDottedForm(strVal) {\n"
	"if ((strVal == null) || (strVal.length < 7))\n"
	"return false;\n"
	"var rc = true;\n"
	"if(strVal.search(/^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$/) != -1)"
	" {\n"
	"var myArray = strVal.split(/\\./);\n"
	"if(myArray[0] > 255 || myArray[1] > 255 || myArray[2] > 255 || "
	"myArray[3] > 255) {\n"
	"rc = false;\n"
	"}\n"
	"} else {\n"
	"rc = false;\n"
	"}\n";

static char Pgvalidation_Item_2[] =
	"return rc;\n"
	"}\n"
	"function isValidNetworkName(strVal) {\n"
	"if (strVal == null)\n"
	"return false;\n"
	"if (isEmptyString(strVal))\n"
	"return false;\n"
	"if (strVal.search(/^[a-zA-Z0-9.-]*$/) != -1)\n"
	"return true;\n"
	"return false;\n"
	"}\n"
	"function isValidGateway(strVal) {\n"
	"if ((strVal == null) || (strVal.length < 7))\n"
	"return false;\n"
	"var rc = true;\n"
	"if (isDottedForm(strVal)) {\n"
	"if(strVal.search(/^0{1,3}\\.0{1,3}\\.0{1,3}\\.0{1,3}$/) != -1)\n"
	"rc = false;\n"
	"} else {\n"
	"rc = false;\n"
	"}\n"
	"return rc;\n"
	"}\n"
	"function isValidIP(strVal) {\n";

static char Pgvalidation_Item_3[] =
	"if ((strVal == null) || (strVal.length < 7))\n"
	"return false;\n"
	"var rc = true;\n"
	"if (isDottedForm(strVal)) {\n"
	"var myArray = strVal.split(/\\./);\n"
	"if(myArray[0] == 255 && myArray[1] == 255 && myArray[2] == 255 && "
	"myArray[3] == 255) {\n"
	"rc = false;\n"
	"}\n"
	"} else {\n"
	"rc = false;\n"
	"}\n"
	"return rc;\n"
	"}\n"
	"function isValidNonZeroIP(strVal) {\n"
	"if ((strVal == null) || (strVal.length < 7))\n"
	"return false;\n"
	"var rc = true;\n"
	"if (isDottedForm(strVal)) {\n"
	"var myArray = strVal.split(/\\./);\n";

static char Pgvalidation_Item_4[] =
	"if(myArray[0] == 255 && myArray[1] == 255 && myArray[2] == 255 && "
	"myArray[3] == 255)\n"
	"rc = false;\n"
	"else if (myArray[0] == 0 && myArray[1] == 0 && myArray[2] == 0 && "
	"myArray[3] == 0)\n"
	"rc = false;\n"
	"}\n"
	"else rc = false;\n"
	"return rc;\n"
	"}\n"
	"function isValidEmail(strVal) {\n"
	"if (strVal == null || (strVal.length < 5))\n"
	"return false;\n"
	"return "
	"(strVal.search(/^[a-zA-Z0-9-_.]*[a-zA-Z0-9-_.]\\@[a-zA-Z0-9].+[a-zA-Z0"
	"-9]+[a-zA-Z0-9]$/) != -1);\n"
	"}\n"
	"function isValidHex(strVal) {\n"
	"if (strVal == null)\n"
	"return true;\n";

static char Pgvalidation_Item_5[] =
	"if (isEmptyString(strVal))\n"
	"return true;\n"
	"if (strVal.search(/^[abcdefABCDEF0-9]*$/) != -1)\n"
	"return true;\n"
	"return false;\n"
	"}\n"
	"function isValidInteger(strVal) {\n"
	"if ((strVal == null) || (strVal.length == 0))\n"
	"return false;\n"
	"var intFound = false;\n"
	"var i;\n"
	"for (i = 0; i < strVal.length; i++) {\n"
	"// Check that current character is number.\n"
	"var c = strVal.charAt(i);\n"
	"if (c == \"-\") {\n"
	"// minus sign must be come before any numbers\n"
	"if (intFound) {\n"
	"return false;\n"
	"}\n"
	"} else if (isDigit(c)) {\n"
	"intFound = true;\n"
	"} else {\n";

static char Pgvalidation_Item_6[] =
	"return false;\n"
	"}\n"
	"}\n"
	"return intFound;\n"
	"}\n"
	"function isInRange(strVal, minInt, maxInt) {\n"
	"if ((strVal == null) || (strVal.length == 0))\n"
	"return false;\n"
	"if (!isValidInteger(strVal))\n"
	"return false;\n"
	"if ((minInt == null) ||\n"
	"(maxInt == null))\n"
	"return false;\n"
	"var val = parseInt(strVal);\n"
	"return ((val >= minInt) && (val <= maxInt));\n"
	"}\n"
	"function isValidSigned8(strVal) {\n"
	"return isInRange(strVal, -128, 127);\n"
	"}\n"
	"function isValidUnsigned8(strVal) {\n"
	"return isInRange(strVal, 0, 255);\n"
	"}\n"
	"function isValidSigned16(strVal) {\n";

static char Pgvalidation_Item_7[] =
	"return isInRange(strVal, -32768, 32767);\n"
	"}\n"
	"function isValidUnsigned16(strVal) {\n"
	"return isInRange(strVal, 0, 65535);\n"
	"}\n"
	"function isValidSigned32(strVal) {\n"
	"return isInRange(strVal, -2147483648, 2147483647);\n"
	"}\n"
	"function isValidUnsigned32(strVal) {\n"
	"return isInRange(strVal, 0, 4294967295);\n"
	"}\n"
	"function isValidPassword(strVal) {\n"
	"if (strVal == null || strVal.length < 1 || strVal.length > 15)\n"
	"return false;\n"
	"return true;\n"
	"}\n"
	"function isEmptyString(strVal, trimFirst) {\n"
	"if ((strVal == null))\n"
	"return true;\n";

static char Pgvalidation_Item_8[] =
	"if ((trimFirst == null) || (trimFirst == true))\n"
	"strVal = trim(strVal);\n"
	"return (strVal.length == 0);\n"
	"}\n"
	"function trim(strVal) {\n"
	"if ((strVal == null) || (strVal.length == 0))\n"
	"return \"\";\n"
	"// trim leading blanks\n"
	"var i = 0;\n"
	"while ((i < strVal.length) && (strVal.charAt(i) == \" \")) {\n"
	"++i;\n"
	"}\n"
	"var tmpVal = strVal.substring(i);\n"
	"// trim trailing blanks\n"
	"i=tmpVal.length;\n"
	"while ((i > 0) && (tmpVal.charAt(i-1) == \" \")) {\n"
	"--i;\n"
	"}\n"
	"return tmpVal.substring(0, i);\n"
	"}\n"
	"function isDigit(c) {\n";

static char Pgvalidation_Item_9[] =
	"return ((c >= \"0\") && (c <= \"9\"))\n"
	"}\n";


static rpItem Pgvalidation_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_2 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_3 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_6 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_7 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_8 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgvalidation_Item_9 }, 
	{ eRpItemType_LastItemInList } 
};


rpObjectDescription Pgvalidation = {
	"/validation.js",
	Pgvalidation_Items,
	(rpObjectExtensionPtr) 0,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeJs,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
