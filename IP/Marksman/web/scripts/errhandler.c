/* Created with PageBuilder version 4.04 on Fri Jul 30 13:18:19 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ******************************************** */
/* *    Built from "scripts\errhandler.js"    * */
/* ******************************************** */

extern rpObjectDescription Pgerrhandler;

static char Pgerrhandler_Item_1[] =
	"// The ErrorHandler class is used to log one or more error\n"
	"// messages encountered when validating form fields. After\n"
	"// all errors have been logged the errors can then be displayed\n"
	"// in an alert box, the failed form field controls highlighted, and\n"
	"// focus/selection set on the first failingform field control.\n"
	"//\n"
	"// NOTE: the ErrorHandler object should be instantiated at\n"
	"// global scope. It must be kept around for subsequent\n"
	"// calls. Use the clearErrors message between those calls to\n";

static char Pgerrhandler_Item_2[] =
	"// reset the highlighted fields on the screen.\n"
	"//\n"
	"// EXAMPLE:\n"
	"// var err = new ErrorHandler();\n"
	"// function doValidate() {\n"
	"// err.clearErrors();\n"
	"// if (!isValidIP(document.network_form.ip_addr.value))\n"
	"// err.addError(document.network_form.ip_addr, \"Invalid IP "
	"address.\");\n"
	"// if (!isDottedForm(document.network_form.submask.value))\n"
	"// err.addError(document.network_form.submask, \"Invalid subnet "
	"mask.\");\n"
	"// ...\n"
	"// err.showError();\n"
	"// return !err.hasError();\n"
	"// }\n"
	"// Constructor\n";

static char Pgerrhandler_Item_3[] =
	"function ErrorHandler() {\n"
	"this.messages = new Array();\n"
	"this.failedFormCtrls = new Array();\n"
	"}\n"
	"// Add an error message and the form control that caused the error.\n"
	"ErrorHandler.prototype.addError = function(formCtrl, errMsg) {\n"
	"if (errMsg != null) {\n"
	"this.messages[this.messages.length] = errMsg;\n"
	"}\n"
	"if (formCtrl != null) {\n"
	"this.failedFormCtrls[this.failedFormCtrls.length] = formCtrl;\n"
	"}\n"
	"}\n"
	"// Determine if errors have been logged (returns boolean).\n"
	"ErrorHandler.prototype.hasError = function() {\n";

static char Pgerrhandler_Item_4[] =
	"return (this.messages.length > 0);\n"
	"}\n"
	"// Show error messages and process form controls (highlight and set "
	"focus).\n"
	"ErrorHandler.prototype.showError = function() {\n"
	"if (this.messages.length == 0) {\n"
	"return;\n"
	"} else if (this.messages.length == 1) {\n"
	"alert(\'Please correct the following problem and try again:\\n\\n\' + "
	"this.messages[0]);\n"
	"} else {\n"
	"var msg = \'Please correct the following problems and try again:\\n\';"
	"\n"
	"var i;\n"
	"for (i=0; i<this.messages.length; ++i) {\n"
	"msg = msg + \"\\n- \" + this.messages[i];\n"
	"}\n";

static char Pgerrhandler_Item_5[] =
	"alert(msg);\n"
	"}\n"
	"var f;\n"
	"var focusSet = false;\n"
	"for (f=0; f<this.failedFormCtrls.length; ++f) {\n"
	"// Older browsers don\'t support the className attribute.\n"
	"if (this.failedFormCtrls[f].className != null)\n"
	"this.failedFormCtrls[f].className = \'errorfield\';\n"
	"// Set focus and select text on first failed field.\n"
	"if (!focusSet) {\n"
	"this.failedFormCtrls[f].focus();\n"
	"this.failedFormCtrls[f].select();\n"
	"focusSet = true;\n"
	"}\n"
	"}\n"
	"}\n"
	"// Clear all error messages and reset the styles on the form controls "
	"that were\n";

static char Pgerrhandler_Item_6[] =
	"// previously flagged for errors.\n"
	"ErrorHandler.prototype.clearErrors = function() {\n"
	"// Reset messages\n"
	"this.messages.length = 0;\n"
	"// Reset styles for failed controls\n"
	"for (f=0; f<this.failedFormCtrls.length; ++f) {\n"
	"// Older browsers don\'t support the className attribute.\n"
	"if (this.failedFormCtrls[f].className != null)\n"
	"this.failedFormCtrls[f].className = \'\';\n"
	"}\n"
	"this.failedFormCtrls.length = 0;\n"
	"}\n";


static rpItem Pgerrhandler_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_2 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_3 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgerrhandler_Item_6 }, 
	{ eRpItemType_LastItemInList } 
};


rpObjectDescription Pgerrhandler = {
	"/errhandler.js",
	Pgerrhandler_Items,
	(rpObjectExtensionPtr) 0,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeJs,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
