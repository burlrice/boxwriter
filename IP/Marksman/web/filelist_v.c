/* Created with PageBuilder version 4.04 on Mon Mar 13 10:30:46 2006 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AsExtern.h"

#if RomPagerServer

#include <Npttypes.h>
#include <fs_api.h>
#include <sysAccess.h>
#include <fileinit.h>
#include "http/httpsvr.h"
#include "session.h"


static void getFSMessage(int fs_error_code, char *errMsg);
void html_Release_DirectoryInfo(NAFS_DIR_ENTRY_INFO *directory_info);
void html_Setup_DirectoryInfo(void *theTaskDataPtr, char *volume_name, 
							   NAFS_DIR_ENTRY_INFO **directory_info, 
							   unsigned *directory_entry_count);

extern rpObjectDescription Pgfilelist;
#define FILELIST_ID &Pgfilelist

// Page Cache
typedef struct {
	NAFS_DIR_ENTRY_INFO *html_directory_info;
	unsigned int html_directory_entry_count;
	char html_directory_name[NAFS_DIR_NAME_SIZE];
	char html_filelist_url[NAFS_DIR_NAME_SIZE];
	char msg[100];
} FilelistPgCache;

FilelistPgCache *getFilelistPageCache(void *theTaskDataPtr)
{    
    SessionCache *sess = getSessionCache(theTaskDataPtr);
    if (isPageCacheEmpty(sess) || !isSessionCacheID(sess, FILELIST_ID))
        setPageCache(sess, malloc(sizeof(FilelistPgCache)), FILELIST_ID);
    return (FilelistPgCache *) getPageCache(sess, FILELIST_ID);
}



/* **************************************** */
/* *    Built from "html\filelist.htm"    * */
/* **************************************** */

extern char *html_getDirName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getDirName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
	return cache->html_directory_name;
}

extern char *html_getCurrentDir(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getCurrentDir(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
	return cache->html_directory_name;
}

extern void html_setCurrentDir(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void html_setCurrentDir(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
	/* We use a hidden field to set the current directory name for the */
	/* current http request. */
	strcpy(cache->html_directory_name, theValuePtr);
    RpHSSetCurrentDirectory(RpGetCurrentConnection(theTaskDataPtr), cache->html_directory_name);
	return;
}

extern char *html_fileListUrl(void);
char *html_fileListUrl(void) {
	char * theResult;
    theResult = Pgfilelist.fURL;
	return theResult;
}

extern Unsigned8 html_getFileTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 html_getFileTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	Unsigned8  theResult = 2;
    NAFS_DIR_ENTRY_INFO *entryInfo;
    
    entryInfo = (NAFS_DIR_ENTRY_INFO *)RpGetRepeatWhileValue(theTaskDataPtr);
    if (IS_FILE_TYPE(entryInfo->entry_type))
        theResult = 0;
    else if (IS_DIRECTORY_TYPE(entryInfo->entry_type))
        theResult = 1;
        
	return theResult;
}

extern Unsigned8 html_getFileDirTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 html_getFileDirTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	FilelistPgCache     *cache = getFilelistPageCache(theTaskDataPtr);
	Unsigned8           theResult = 2;
    NAFS_DIR_ENTRY_INFO *entryInfo;
    char                *dir;
    
    entryInfo = (NAFS_DIR_ENTRY_INFO *)RpGetRepeatWhileValue(theTaskDataPtr);
    
    if (IS_FILE_TYPE(entryInfo->entry_type))
    {
        dir = strstr(cache->html_directory_name, "/");
        if (strlen(dir) > 1)
            theResult = 0;  /* not root directory must add "/" before filename */
        else
            theResult = 1;  /* root directory so no "/" added */
        
    }
    else if (IS_DIRECTORY_TYPE(entryInfo->entry_type))
        theResult = 1;
        
	return theResult;
}

extern char *html_getFileName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getFileName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	char * theResult;
    NAFS_DIR_ENTRY_INFO *entryInfo;
    
    entryInfo = (NAFS_DIR_ENTRY_INFO *)RpGetRepeatWhileValue(theTaskDataPtr);
    theResult = entryInfo->entry_name;
	return theResult;
}

extern char *html_getFileType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getFileType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	char * theResult;
    NAFS_DIR_ENTRY_INFO *entryInfo;
        
    
    entryInfo = (NAFS_DIR_ENTRY_INFO *)RpGetRepeatWhileValue(theTaskDataPtr);
    if (IS_FILE_TYPE(entryInfo->entry_type))
        theResult = "File";
    else if (IS_DIRECTORY_TYPE(entryInfo->entry_type))
        theResult = "Directory";
    else
        theResult = "Unknown";
        
	return theResult;
}

extern Unsigned32 html_getFileSize(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Unsigned32 html_getFileSize(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
    Unsigned32          theResult;
    NAFS_DIR_ENTRY_INFO *entryInfo;
    
    entryInfo = (NAFS_DIR_ENTRY_INFO *)RpGetRepeatWhileValue(theTaskDataPtr);
    
    theResult = entryInfo->file_size;
    
    return theResult;
}

extern void html_getFileListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
void html_getFileListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr) {
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
    Signed16 theIndex;
    
	if (*theRepeatGroupValuePtr == (void *) 0)
	{
		html_Setup_DirectoryInfo(theTaskDataPtr, cache->html_directory_name,
								&cache->html_directory_info,
								&cache->html_directory_entry_count);
	}

    *theRepeatGroupValuePtr = (void *) 0;		
    
    theIndex = *theIndexPtr;
    if (theIndex < cache->html_directory_entry_count)
    {
        *theRepeatGroupValuePtr = &cache->html_directory_info[theIndex];
        theIndex++;
    }
	else 
	{
		html_Release_DirectoryInfo(cache->html_directory_info);
		cache->html_directory_info = NULL;
		cache->html_directory_entry_count = 0;

	}
    *theIndexPtr = theIndex;
	return;
}

extern void upload_submit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void upload_submit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {

	SessionCache *sess = getSessionCache(theTaskDataPtr);
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
    int errorStatus;
	int	theFileNumber;
    

	theFileNumber =  RpGetCurrentConnection(theTaskDataPtr);

    RpHSGetErrorStatus(theFileNumber, &errorStatus);
#if (DEBUG_PRINTF > 0)
	printf("Calling: RpHSGetErrorStatus(%d, %d)\n", theFileNumber, errorStatus);
#endif
    
    if (errorStatus == NAFS_SUCCESS)
    {
		setCacheInvalid(sess);
    	addSessionMsg(sess, WEBUI_STATUS_MSG, "File uploaded successfully.");
    }
    else
    {
		setCacheValid(sess); // Leave user entered data in form
        getFSMessage(errorStatus, cache->msg);
		addSessionMsg(sess, WEBUI_ERROR_MSG, cache->msg);
    }

    strcpy(cache->html_filelist_url, Pgfilelist.fURL);
    strcat(cache->html_filelist_url, "?");
    strcat(cache->html_filelist_url, cache->html_directory_name);
    RpSetNextFilePage(theTaskDataPtr, cache->html_filelist_url);
    
	return;
}

extern void html_initFileList(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void html_initFileList(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	SessionCache *sess = getSessionCache(theTaskDataPtr);
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
    /* query "?" text is stored in FormBuffer */
    char *dir = RpGetFormBufferPtr(theTaskDataPtr);
    
	if (!isPageCacheValid(sess, FILELIST_ID)) {
		// Set defaults.
		memset(cache, 0, sizeof(FilelistPgCache));
	}
	
    if (dir == NULL || strlen(dir) == 0) {
    	strcpy(cache->html_directory_name, FS_FLASH_VOLUME_ROOT_DIR);
    }
    else {
	    strcpy(cache->html_directory_name, RpGetFormBufferPtr(theTaskDataPtr));
    }
    
    RpHSSetCurrentDirectory(RpGetCurrentConnection(theTaskDataPtr), cache->html_directory_name);
	RpHSSetStorageLocation(RpGetCurrentConnection(theTaskDataPtr), NAFS_LOCATION_FILE_SYSTEM);
    
	setCacheInvalid(sess);
	return;
}




static void getFSMessage(int fs_error_code, char *errMsg)
{
    switch (fs_error_code)
    {
        case NAFS_NULL_POINTER:
            sprintf(errMsg, "Internal Error! %d", fs_error_code);;
            break;
            
        case NAFS_INVALID_DIR_PATH_LENGTH:
            sprintf(errMsg, "No Directory exist! %d", fs_error_code);;
            break;
        case NAFS_INVALID_VOLUME_NAME:
            sprintf(errMsg, "Invalid volume. %d" , fs_error_code);;
            break;
        case NAFS_INVALID_FILE_NAME:
            sprintf(errMsg, "Invalid File Name. %d" , fs_error_code);;
            break;
        case NAFS_IO_REQUEST_CB_BUSY:
        case NAFS_IO_REQUEST_CB_NOT_INITIALIZED:
            sprintf(errMsg, "File System is busy. %d" , fs_error_code);;
            break;
        case NAFS_INVALID_DIR_NAME_LENGTH:
            sprintf(errMsg, "Invalie directory path. %d" , fs_error_code);;
            break;
        case NAFS_NO_WRITE_PERMISSION:
            sprintf(errMsg, "No write permission. %d" , fs_error_code);;
            break;
        case NAFS_NO_READ_PERMISSION:
            sprintf(errMsg, "No read permission. %d" , fs_error_code);;
            break;
        case NAFS_FILE_IS_OPEN:
            sprintf(errMsg, "File is current opened. %d" , fs_error_code);;
            break;
        case NAFS_DIR_TABLE_FULL:
        case NAFS_FILE_TABLE_FULL:
        case NAFS_BUFFER_POOL_EMPTY:
        case NAFS_NO_FREE_BLOCKS:
            sprintf(errMsg, "No disk space. %d" , fs_error_code);;
            break;
        case NAFS_DUPLICATE_DIR_ENTRY:
            sprintf(errMsg, "File is already existed. %d" , fs_error_code);;
            break;
        default:
            sprintf(errMsg, "File System Error! %d" , fs_error_code);;

    }       
}


void html_Release_DirectoryInfo(NAFS_DIR_ENTRY_INFO *directory_info)
{
	if (directory_info != NULL)
	{
		free (directory_info);
	}
}


void html_Setup_DirectoryInfo(void *theTaskDataPtr, char *volume_name, 
							   NAFS_DIR_ENTRY_INFO **directory_info, 
							   unsigned *directory_entry_count)
{

    NAFS_IO_REQUEST_CB  io_request;
    unsigned int entry_count, status, request_type;
    unsigned int bytes_transferred;
    int          rc, error_code;
	SessionCache *sess = getSessionCache(theTaskDataPtr);
	FilelistPgCache *cache = getFilelistPageCache(theTaskDataPtr);
    
#define _MAX_DIR_ERROR_MSG   "Excess max of directories"
    
	NA_ASSERT((int)theTaskDataPtr);
	NA_ASSERT((int)volume_name);
	NA_ASSERT((int)directory_entry_count);

    *directory_entry_count = 0;
	*directory_info = NULL;
    
    rc = NAFSinit_io_request_cb(&io_request, NULL, 0);
    if (rc != NAFS_SUCCESS)
    {
        goto _html_Setup_DirectoryInfo;
    }
        
    
    /* get number of entry in the volume */
    rc = NAFSdir_entry_count(volume_name, NASYSACC_FS_SUPER_USER, &entry_count, &io_request);
    if (rc != NAFS_SUCCESS)
    {
        goto _html_Setup_DirectoryInfo;
    }
        
    while (1)
    {
        tx_thread_sleep(5);
        rc = NAFSio_request_cb_status(&io_request, &request_type, &status, &error_code, 
                                      &bytes_transferred);
        if (rc != NAFS_SUCCESS)
        {
            goto _html_Setup_DirectoryInfo;
        }
        else if (status == NAFS_IO_REQUEST_TERMINATED || status == NAFS_IO_REQUEST_FREE)
		{
		#if (DEBUG_PRINTF > 0)
			printf("Setup_DirectoryInfo: io_request status %d error_code %d\n", status, error_code);
		#endif
            goto _html_Setup_DirectoryInfo;
		}
        else if (status == NAFS_IO_REQUEST_COMPLETED)
            break;
    }
    
	if (entry_count == 0)
	{
        goto _html_Setup_DirectoryInfo;
	}

	*directory_info = (NAFS_DIR_ENTRY_INFO *)calloc(entry_count, sizeof(NAFS_DIR_ENTRY_INFO));
	if (*directory_info == NULL)
	{
        rc = NAFS_MALLOC_FAILED;
        goto _html_Setup_DirectoryInfo;
	}

#if (DEBUG_PRINTF > 0)
	printf("Calling: calloc returns 0x%X\n", (unsigned)*directory_info);
#endif
    /* get the directory listing */            
    rc = NAFSlist_dir(volume_name, NASYSACC_FS_SUPER_USER, *directory_info, entry_count, 
                      &io_request);
    
    if (rc != NAFS_SUCCESS)
    {
        goto _html_Setup_DirectoryInfo;
    }
    
    while (1)
    {
        tx_thread_sleep(5);
        rc = NAFSio_request_cb_status(&io_request, &request_type, &status, &error_code, 
                                      &bytes_transferred);
        if (rc != NAFS_SUCCESS)
        {
            goto _html_Setup_DirectoryInfo;
        }
        else if (status == NAFS_IO_REQUEST_TERMINATED || status == NAFS_IO_REQUEST_FREE)
		{
        #if (DEBUG_PRINTF > 0)
			printf("Setup_DirectoryInfo: io_request status %d error_code %d\n", status, error_code);
        #endif
            goto _html_Setup_DirectoryInfo;
		}
        else if (status == NAFS_IO_REQUEST_COMPLETED)
            break;
    }
    
    *directory_entry_count = entry_count;
        
_html_Setup_DirectoryInfo:    
	if (rc != NAFS_SUCCESS)
	{
		html_Release_DirectoryInfo(*directory_info);
		*directory_info = NULL;
        getFSMessage(rc, cache->msg);
		addSessionMsg(sess, WEBUI_ERROR_MSG, cache->msg);
	}
	return;

}


#endif	/* RomPagerServer */
