/* Created with PageBuilder version 4.04 on Thu Jul 13 11:08:39 2006 */

#include "AsExtern.h"
#include <bsp.h>
#include <boardParams.h>
#include <wln_api.h>
#include "dnsc_api.h"
#include "webutil.h"
#include "session.h"

#if RomPagerServer

#if BSP_WANT_WIRELESS

extern rpObjectDescription Pgwireless_ip_config;
extern rpObjectDescription Pgerrhandler;

#define WIRELESS_IP_CONFIG_ID &Pgwireless_ip_config

enum {
    dhcp_enabled, dhcp_disabled
};

// Page Cache
typedef struct {
	devBoardParamsType nvParams;
	NaIamIfAddrInfo_t addrInfo;
    rpOneOfSeveral dhcp;
	unsigned char ipv6IsEnabled;
	unsigned char ipv6PrefixLen;
    char ipAddress[ROMPAGER_IP_ARRAY_LENGTH];
    char subMask[ROMPAGER_IP_ARRAY_LENGTH];
    char gateway[ROMPAGER_IP_ARRAY_LENGTH];
    char primaryDns[ROMPAGER_IP_ARRAY_LENGTH];
    char secondaryDns[ROMPAGER_IP_ARRAY_LENGTH];
    char ipv6Address[MAX_IP_ADDR_STR_LEN];

} WirelessIpPgCache;

WirelessIpPgCache *getWirelessIpPageCache(void *theTaskDataPtr)
{    
    SessionCache *sess = getSessionCache(theTaskDataPtr);
    if (isPageCacheEmpty(sess) || !isSessionCacheID(sess, WIRELESS_IP_CONFIG_ID))
        setPageCache(sess, malloc(sizeof(WirelessIpPgCache)), WIRELESS_IP_CONFIG_ID);
    return (WirelessIpPgCache *) getPageCache(sess, WIRELESS_IP_CONFIG_ID);
}

#endif // BSP_WANT_WIRELESS


/* ************************************************** */
/* *    Built from "html\wireless_ip_config.htm"    * */
/* ************************************************** */

extern rpOneOfSeveral getWirelessDhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getWirelessDhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->dhcp;
#else
	return 0;
#endif // BSP_WANT_WIRELESS		
}


extern void setWirelessDhcp(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessDhcp(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	cache->dhcp = theValue;
#endif	
}

extern char *getWirelessIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->ipAddress;
#else
	return "";
#endif // BSP_WANT_WIRELESS	
}

extern void setWirelessIPAddress(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessIPAddress(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	memcpy(cache->ipAddress, theValuePtr, ROMPAGER_IP_ARRAY_LENGTH);       
#endif	
}

extern char *getWirelessSubMask(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessSubMask(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->subMask;
#else
	return "";
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessSubMask(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessSubMask(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	memcpy(cache->subMask, theValuePtr, ROMPAGER_IP_ARRAY_LENGTH);       
#endif	
}

extern char *getWirelessGateway(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessGateway(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->gateway;
#else
	return "";
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessGateway(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessGateway(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	memcpy(cache->gateway, theValuePtr, ROMPAGER_IP_ARRAY_LENGTH);       
#endif	
}

extern char *getWirelessPrimaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessPrimaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->primaryDns;
#else
	return "";
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessPrimaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessPrimaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	memcpy(cache->primaryDns, theValuePtr, ROMPAGER_IP_ARRAY_LENGTH);       
#endif	
}

extern char *getWirelessSecondaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessSecondaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->secondaryDns;
#else
	return "";
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessSecondaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessSecondaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	memcpy(cache->secondaryDns, theValuePtr, ROMPAGER_IP_ARRAY_LENGTH);       
#endif	
}

extern Boolean getWirelessIpv6Dhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Boolean getWirelessIpv6Dhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return (cache->ipv6IsEnabled & NA_IAM_DHCPV6_ENABLED);
#else
	return FALSE;
#endif
}

extern void setWirelessIpv6Dhcp(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessIpv6Dhcp(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS	
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);

    if (theValue == True)
    {  /* Enable DHCP v6 */
        cache->ipv6IsEnabled |= NA_IAM_DHCPV6_ENABLED;
    }
    else
    {  /* Disable DHCP v6 */
        cache->ipv6IsEnabled &= ~NA_IAM_DHCPV6_ENABLED;
    }
#endif
	return;
}

extern Boolean getWirelessIpv6Static(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Boolean getWirelessIpv6Static(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS	
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return (cache->ipv6IsEnabled & NA_IAM_STATICV6_ENABLED);
#else
	return FALSE;
#endif
}

extern void setWirelessIpv6Static(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessIpv6Static(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS	
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);

    if (theValue == True)
    {  /* Enable StaticV6 */
        cache->ipv6IsEnabled |= NA_IAM_STATICV6_ENABLED;
    }
    else
    {  /* Disable StaticV6 */
        cache->ipv6IsEnabled &= ~NA_IAM_STATICV6_ENABLED;
    }
#endif
	return;
}

extern char *getWirelessIpv6Address(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessIpv6Address(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	return cache->ipv6Address;
#else
	return "";
#endif // BSP_WANT_WIRELESS	
	
}

extern int validateIpv6Information(struct sockaddr_storage * ipAddrPtr, int prefixLen);


extern void setWirelessIPv6Address(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessIPv6Address(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS	

	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);

	strcpy(cache->ipv6Address, theValuePtr);
#endif
	return;
}

extern Unsigned8 getWirelessPrefixLength(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Unsigned8 getWirelessPrefixLength(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS	
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);

	return cache->ipv6PrefixLen;
#else
	return 64;
#endif
}

extern void setWirelessPrefixLength(void *theTaskDataPtr, Unsigned8 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessPrefixLength(void *theTaskDataPtr, Unsigned8 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	cache->ipv6PrefixLen = theValue;
#endif
	return;
}



extern void wirelessSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void wirelessSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	SessionCache *sess = getSessionCache(theTaskDataPtr);
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	struct sockaddr_storage ipv6addr;

	int status;
	NaIamParams_t *interface;

	interface = customizeIamFindInterfaceConfig(BP_WLN_INTERFACE, &cache->nvParams.iamParamsInfo);
	
	if (cache->dhcp == dhcp_enabled) {
 	    // Disable the static configuration to make ACE get an address from the network.
        interface->staticParams.isEnabled = FALSE;
    } else {
		interface->staticParams.IPV4_ADDR(ipAddress) = rompager_to_ipaddr(cache->ipAddress);        
		interface->staticParams.subnetMask = rompager_to_ipaddr(cache->subMask);        
		interface->staticParams.IPV4_ADDR(gateway) = rompager_to_ipaddr(cache->gateway);        
        interface->staticParams.IPV4_ADDR(primaryDns) = rompager_to_ipaddr(cache->primaryDns);
        interface->staticParams.IPV4_ADDR(secondaryDns) = rompager_to_ipaddr(cache->secondaryDns);
        
        //Enabling the static configuration will cause DHCP, BOOTP, etc. to be
        // ignored on this interface.
        interface->staticParams.isEnabled = TRUE;
    }

	interface->staticParams.ipv6IsEnabled = cache->ipv6IsEnabled;
	if (cache->ipv6IsEnabled & NA_IAM_STATICV6_ENABLED)
	{
		if (inet_pton(AF_INET6, cache->ipv6Address, &ipv6addr.addr.ipv6.sin6_addr) == 1) 
		{
			ipv6addr.ss_family = AF_INET6;
			ipv6addr.ss_len = sizeof(struct sockaddr_in6);

			if (validateIpv6Information(&ipv6addr, cache->ipv6PrefixLen) == 0)
			{
				interface->staticParams.ipv6Address = ipv6addr;
				interface->staticParams.ipv6PrefixLen = cache->ipv6PrefixLen;
			}
			else
			if (validateIpv6Information(&ipv6addr, 64) == 0)
			{
				interface->staticParams.ipv6Address = ipv6addr;
				interface->staticParams.ipv6PrefixLen = 64;
			}
			else
				interface->staticParams.ipv6IsEnabled &= ~NA_IAM_STATICV6_ENABLED;
		}
		else if (validateIpv6Information(&interface->staticParams.ipv6Address, interface->staticParams.ipv6PrefixLen))
			interface->staticParams.ipv6IsEnabled &= ~NA_IAM_STATICV6_ENABLED;
	}
	status = customizeWriteDevBoardParams(&cache->nvParams);

	if (status != BP_SUCCESS) {
		setCacheValid(sess); // Leave user entered data in form
		addSessionMsg(sess, WEBUI_ERROR_MSG, "Critical error saving wireless IP settings!");
	} else {
		setCacheInvalid(sess);
		addSessionMsg(sess, WEBUI_STATUS_MSG, "Settings have been saved. You must reboot for changes to take effect.");
	}

#endif // BSP_WANT_WIRELESS
	return;
}

extern void wirelessIpConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void wirelessIpConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	
#if BSP_WANT_WIRELESS

	SessionCache *sess = getSessionCache(theTaskDataPtr);
	WirelessIpPgCache *cache = getWirelessIpPageCache(theTaskDataPtr);
	NaIamParams_t *interface;
	char * ipv6_addr;
	
	if (!isPageCacheValid(sess, WIRELESS_IP_CONFIG_ID)) {
		// Set defaults.
		cache->dhcp = dhcp_enabled;
		memset(cache->ipAddress, 0, sizeof(cache->ipAddress));
		memset(cache->subMask, 0, sizeof(cache->subMask));
		memset(cache->gateway, 0, sizeof(cache->gateway));
		memset(cache->primaryDns, 0, sizeof(cache->primaryDns));
		memset(cache->secondaryDns, 0, sizeof(cache->secondaryDns));
		memset(cache->ipv6Address, 0, sizeof(cache->ipv6Address));

		cache->ipv6IsEnabled = 0;
		cache->ipv6PrefixLen = 64;
				
		// Read current settings.
		customizeReadDevBoardParams(&cache->nvParams);
		
		// Extract the configuration structure from the NVRAM structure.  There should be a configuration
		// settings for the wln0 interface.  If not, then reinitialize the structure.
		interface = customizeIamFindInterfaceConfig(BP_WLN_INTERFACE, &cache->nvParams.iamParamsInfo);
		if (interface == NULL) {
			customizeIamGetDefaultConfig(&cache->nvParams.iamParamsInfo);
			interface = customizeIamFindInterfaceConfig(BP_WLN_INTERFACE, &cache->nvParams.iamParamsInfo);
		}

	    // Static configuration?
	    if (interface->staticParams.isEnabled) {
	    	cache->dhcp = dhcp_disabled;
	    	ipaddr_to_rompager(cache->ipAddress, interface->staticParams.IPV4_ADDR(ipAddress));
	    	ipaddr_to_rompager(cache->subMask, interface->staticParams.subnetMask);
	    	ipaddr_to_rompager(cache->gateway, interface->staticParams.IPV4_ADDR(gateway));
	    	ipaddr_to_rompager(cache->primaryDns, interface->staticParams.IPV4_ADDR(primaryDns));
	    	ipaddr_to_rompager(cache->secondaryDns, interface->staticParams.IPV4_ADDR(secondaryDns));
	    	
	    } else {
	    	customizeIamGetIfAddrInfo(BP_WLN_INTERFACE, &cache->addrInfo);
	    	
			cache->dhcp = dhcp_enabled;
			ipaddr_to_rompager(cache->ipAddress, IPV4_ADDR(cache->addrInfo.v4Info.ipAddress));
			ipaddr_to_rompager(cache->subMask, cache->addrInfo.v4Info.subnetMask);
			ipaddr_to_rompager(cache->gateway, IPV4_ADDR(cache->addrInfo.v4Info.defaultGateway));
#if IPV6_ENABLED			
			ipaddr_to_rompager(cache->primaryDns, cache->addrInfo.primaryDnsServer.addr.ipv6.sin6_addr.ip6Addr.ip6U32[3]);
			ipaddr_to_rompager(cache->secondaryDns, cache->addrInfo.secondaryDnsServer.addr.ipv6.sin6_addr.ip6Addr.ip6U32[3]);
#else			
			ipaddr_to_rompager(cache->primaryDns, IPV4_ADDR(cache->addrInfo.primaryDnsServer));
			ipaddr_to_rompager(cache->secondaryDns, IPV4_ADDR(cache->addrInfo.secondaryDnsServer));
#endif		
	    }
		
	}

	cache->ipv6IsEnabled = interface->staticParams.ipv6IsEnabled;
	ipv6_addr = (char *)&interface->staticParams.ipv6Address.addr.ipv6.sin6_addr;
	cache->ipv6PrefixLen = interface->staticParams.ipv6PrefixLen;
	    	
	/* Convert v6 address to ASCII. */
	if (!inet_ntop(AF_INET6, ipv6_addr, cache->ipv6Address, MAX_IP_ADDR_STR_LEN))
		cache->ipv6Address[0] = '\0';
	
	setCacheInvalid(sess);
	
#endif // BSP_WANT_WIRELESS
	
	return;
}


#endif	/* RomPagerServer */
