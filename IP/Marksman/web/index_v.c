/* Created with PageBuilder version 4.04 on Sun Mar  5 14:23:57 2006 */

#include "AsExtern.h"
#include <Npttypes.h>
#include <bsp.h>
#include <boardParams.h>
#include <iam_netos.hh>
#include <iam.hh>
#include <narmapi.h>
#include <wln_api.h>
#include <appconf_api.h>
#include "webmenu.h"
#include "cpu_utilization.h"
#include "nsuptime.h"
#include "session.h"

#include "fj.h"
#include "fj_cvt.h"

#define BUFFERSIZE 256


#if RomPagerServer


extern rpObjectDescription Pgindex;
#define INDEX_PAGE_ID &Pgindex

// Page Cache
typedef struct {
	NaIamIfIpCfg_t  	ethConfig;
	NaIamIfIpCfg_t		wlnConfig;
	wln_status			wlnStatus;	
	char				tmpArea[256];
} IndexPgCache;


static char defaultProductName[] = "Unknown Product";

enum {description_buffer_size = 16};
enum {combine_buffer_size = 32};
enum {minutes_per_hour = 60};
enum {seconds_per_minute = 60};
enum {hours_per_day = 24};

IndexPgCache *getIndexPageCache(void *theTaskDataPtr) {    
    SessionCache *sess = getSessionCache(theTaskDataPtr);
    if (isPageCacheEmpty(sess) || !isSessionCacheID(sess, INDEX_PAGE_ID))
        setPageCache(sess, malloc(sizeof(IndexPgCache)), INDEX_PAGE_ID);
    return (IndexPgCache *) getPageCache(sess, INDEX_PAGE_ID);
}

void format_uptime_string(char* dest_buffer, char* string, unsigned long number)
{
	char combine_buffer[combine_buffer_size];
	char description_buffer[description_buffer_size];
	strcpy(description_buffer, string);
	
	if(number > 1){
		strcat(description_buffer, "s");
	}
	
	sprintf(combine_buffer, " %lu %s", number, description_buffer);
	strcat(dest_buffer, combine_buffer);
}


/* ************************************* */
/* *    Built from "html\index.htm"    * */
/* ************************************* */

extern char *getProductName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getProductName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };

    sprintf (theResult, "Marksman NEXT, version %s", fj_getVersionNumber ());

	return theResult;
}

extern char *html_getMenuItemUrl(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getMenuItemUrl(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
    html_menu_item *menuItem;
    
    menuItem = (html_menu_item *)RpGetRepeatWhileValue(theTaskDataPtr);
    return (char *)menuItem->page->fURL;
}

extern char *html_getMenuItemName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *html_getMenuItemName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
    html_menu_item *menuItem;
    
    menuItem = (html_menu_item *)RpGetRepeatWhileValue(theTaskDataPtr);
    return (char *)menuItem->name;
}

extern void html_getMenuListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
void html_getMenuListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr) {

	if (*theRepeatGroupValuePtr == (void *) 0) {	/* first time */
		*theIndexPtr = 0;
	} else {
		(*theIndexPtr)++;
	}
	
	*theRepeatGroupValuePtr = getHtmlMenuItem(*theIndexPtr);
	
	return;
}

extern char *getCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
	char * theResult = NULL;

#if BSP_WANT_ETHERNET
	if (IPV4_ADDR(cache->ethConfig.addrV4[0].ipAddress))
	{
		NAInet_toa(IPV4_ADDR(cache->ethConfig.addrV4[0].ipAddress), cache->tmpArea);
		theResult = cache->tmpArea;
	}
#endif
	return theResult;
}

extern char *getCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
	char * theResult = NULL;

#if BSP_WANT_ETHERNET
	if (IPV4_ADDR(cache->ethConfig.addrV4[1].ipAddress))
	{
		NAInet_toa(IPV4_ADDR(cache->ethConfig.addrV4[1].ipAddress), cache->tmpArea);
		theResult = cache->tmpArea;
	}
#endif
	return theResult;
}

extern char *getMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
#if BSP_WANT_ETHERNET	
	BYTE mac[6];
	int rc;

	strcpy(cache->tmpArea, "");
	rc = customizeGetInterfaceMACAddress(BP_ETH_INTERFACE, mac);
    if (rc == BP_SUCCESS || rc == BP_FAILURE)
    {
		sprintf(cache->tmpArea, "%02X:%02X:%02X:%02X:%02X:%02X",
				mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }
#else
	cache->tmpArea[0] = '\0';
#endif
        	
	return cache->tmpArea;
}



static void getIPv6AddressString(NaIamIfIpCfg_t * config, int idx_ui, char * str)
{
#if BSP_WANT_ETHERNET
	NaIamIfAddress_t * v6Addresses = &config->autoCfgV6[0];
	int i, cnt = 0;

	for (i = 0; i < NA_MAX_HOMES_PER_IF + 2; i++)
	{
		if (v6Addresses[i].method != NA_IAM_METHOD_NONE)
		{
			if ((++cnt == idx_ui) && 
				inet_ntop(AF_INET6, &v6Addresses[i].ipAddress.addr.ipv6.sin6_addr, str, MAX_IP_ADDR_STR_LEN))
			{
				return;
			}
				
		}
	}
#endif
	*str = '\0';
}


extern char *getCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->ethConfig, 1, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->ethConfig, 2, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
	
	getIPv6AddressString(&cache->ethConfig, 3, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->ethConfig, 4, cache->tmpArea);

	return cache->tmpArea;

}

extern char *getCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->ethConfig, 5, cache->tmpArea);

	return cache->tmpArea;

}

extern char *getCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->ethConfig, 6, cache->tmpArea);
	return cache->tmpArea;
}

#include "fj_message.h"

extern char *getSelectedMessage(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSelectedMessage(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	
	if (fj_CVT.pfph->pfmSelected) {
		long lCount = fj_scratchpadReadCount ();
		
		sprintf (theResult, "%s [%d]", fj_CVT.pfph->pfmSelected->strName, lCount);
	}
	else
		sprintf (theResult, "(null)");
		
	return theResult;
}

extern char *getGROUP_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getGROUP_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetID (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getGROUP_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getGROUP_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetCount (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getGROUP_IDS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getGROUP_IDS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetMemIDs (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getGROUP_IPADDRS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getGROUP_IPADDRS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetMemIPAddrs (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getUNITS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getUNITS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetUnits (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getTIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getTIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetTimeDisp (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getENCODER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getENCODER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShaftEncoder (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getENCODER_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getENCODER_COUNT(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEncoderCounts (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getENCODER_WHEEL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getENCODER_WHEEL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEncoderWheel (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getENCODER_DIVISOR(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getENCODER_DIVISOR(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEncoderDivisor (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getCONVEYOR_SPEED(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getCONVEYOR_SPEED(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	char sz [BUFFERSIZE] = { 0 };
	
	fj_SysGetConveyorSpeed (fj_CVT.pfsys, sz);
	double d = atof (sz);
	sprintf (theResult, "%.02f in/sec [%.02f ft/min]", d, d * 5.0);
	
	return theResult;
}

extern char *getPRINT_RESOLUTION(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getPRINT_RESOLUTION(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetPrintResolution (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getINK_SIZE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getINK_SIZE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetInkBottleSize (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getINK_COST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getINK_COST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetInkBottleCost (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSYSTEM_TIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSYSTEM_TIME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetTime (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getINTERNAL_CLOCK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getINTERNAL_CLOCK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetTimeSource (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getTIME_ZONE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getTIME_ZONE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetTimeZone (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getOBSERVE_USA_DST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getOBSERVE_USA_DST(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDSTUSA (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getFIRST_DAY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getFIRST_DAY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetFirstDay (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDAYS_WEEK1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDAYS_WEEK1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetWeek1 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getWEEK53(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWEEK53(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetWeek53 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getROLLOVER_HOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getROLLOVER_HOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetRollover (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFT1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFT1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShift1	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFT2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFT2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShift2	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFT3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFT3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShift3	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFTCODE1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFTCODE1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShiftCode1	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFTCODE2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFTCODE2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShiftCode2	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSHIFTCODE3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSHIFTCODE3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetShiftCode3	(fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDAYS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDAYS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDays (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getMONTHS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getMONTHS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetMonths (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDAYS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDAYS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDaysSpecial (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getMONTHS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getMONTHS_SPECIAL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetMonthsSpecial (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getHOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getHOURS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetHours (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getAMPM(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getAMPM(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetAMPM (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getIOBOX(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getIOBOX(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetIOBox (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getIOBOX_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getIOBOX_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetIOBOXIP (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getIOBOX_STROBE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getIOBOX_STROBE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetIOBOXStrobe (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getINTERNET_CONTROL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getINTERNET_CONTROL(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetInternetControl (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDNS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDNS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDNSIP (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getEMAIL_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getEMAIL_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEmailIP (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getEMAIL_SERVER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getEMAIL_SERVER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEmailServer (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getEMAIL_SOURCE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getEMAIL_SOURCE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEmailAppendage (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getEMAIL_INK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getEMAIL_INK(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEmailInk (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getEMAIL_SERVICE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getEMAIL_SERVICE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetEmailService (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getWINS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWINS_IP(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetWINSIP (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getNETBIOS_TYPE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getNETBIOS_TYPE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetWINSType (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCIndex (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCName (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_TY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_TY(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCType (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCHeight (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_CD(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_CD(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCCheck (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBearerH (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_VB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_VB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBearerV (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_QZ(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_QZ(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCQuiet (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_BASE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_BASE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBase (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_B1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_B1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBleed1 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_B2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_B2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBleed2 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_B3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_B3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBleed3 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getBC_B4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getBC_B4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetBCBleed4 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getAPP_VER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getAPP_VER(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetAppVer (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_N(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixIndex (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_NAME(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixName (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_H(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixBleed1 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_HB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixBleed2 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_W(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_W(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixBleed3 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getDM_WB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getDM_WB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetDataMatrixBleed4 (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getPRINT_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getPRINT_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetPrintMode (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getAPS_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getAPS_MODE(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetAPSMode (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSTROBE_MODE_READY_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_READY_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeReadyGreen (fj_CVT.pfsys, theResult);
	return theResult;
}

extern char *getSTROBE_MODE_READY_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_READY_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeReadyYellow (fj_CVT.pfsys, theResult);	    
	return theResult;
}

extern char *getSTROBE_MODE_READY_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_READY_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeReadyRed (fj_CVT.pfsys, theResult);	    
	return theResult;
}

extern char *getSTROBE_MODE_APS_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_APS_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeAPSGreen (fj_CVT.pfsys, theResult);	    
	return theResult;
}

extern char *getSTROBE_MODE_APS_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_APS_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeAPSYellow (fj_CVT.pfsys, theResult);	    
	return theResult;
}

extern char *getSTROBE_MODE_APS_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_APS_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeAPSRed (fj_CVT.pfsys, theResult);			
	return theResult;
}

extern char *getSTROBE_MODE_LOWTEMP_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWTEMP_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowTempGreen (fj_CVT.pfsys, theResult);    
	return theResult;
}

extern char *getSTROBE_MODE_LOWTEMP_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWTEMP_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowTempYellow (fj_CVT.pfsys, theResult);   
	return theResult;
}

extern char *getSTROBE_MODE_LOWTEMP_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWTEMP_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowTempRed (fj_CVT.pfsys, theResult);      
	return theResult;
}

extern char *getSTROBE_MODE_HV_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_HV_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeHVGreen (fj_CVT.pfsys, theResult);			
	return theResult;
}

extern char *getSTROBE_MODE_HV_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_HV_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeHVYellow (fj_CVT.pfsys, theResult);		
	return theResult;
}

extern char *getSTROBE_MODE_HV_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_HV_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeHVRed (fj_CVT.pfsys, theResult);			
	return theResult;
}

extern char *getSTROBE_MODE_LOWINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowInkGreen (fj_CVT.pfsys, theResult);		
	return theResult;
}

extern char *getSTROBE_MODE_LOWINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowInkYellow (fj_CVT.pfsys, theResult);	
	return theResult;
}

extern char *getSTROBE_MODE_LOWINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_LOWINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeLowInkRed (fj_CVT.pfsys, theResult);		
	return theResult;
}

extern char *getSTROBE_MODE_OUTOFINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_OUTOFINK_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeOutOfInkGreen (fj_CVT.pfsys, theResult);	
	return theResult;
}

extern char *getSTROBE_MODE_OUTOFINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_OUTOFINK_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeOutOfInkYellow (fj_CVT.pfsys, theResult);	
	return theResult;
}

extern char *getSTROBE_MODE_OUTOFINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_OUTOFINK_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeOutOfInkRed (fj_CVT.pfsys, theResult);     
	return theResult;
}

extern char *getSTROBE_MODE_DRIVER_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_DRIVER_R(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeDriverRed (fj_CVT.pfsys, theResult);     
	return theResult;
}

extern char *getSTROBE_MODE_DRIVER_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_DRIVER_Y(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeDriverYellow (fj_CVT.pfsys, theResult);     
	return theResult;
}

extern char *getSTROBE_MODE_DRIVER_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSTROBE_MODE_DRIVER_G(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	static char theResult [BUFFERSIZE] = { 0 };
	fj_SysGetStrobeModeDriverGreen (fj_CVT.pfsys, theResult);     
	return theResult;
}

extern Unsigned8 IS_ETHERNET_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 IS_ETHERNET_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_ETHERNET
	return 0;
#else
	return 1;
#endif
}

extern char *getWirelessConnection(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessConnection(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
#if BSP_WANT_WIRELESS	
	char *stateStr = "unknown";
	
	strncpy(cache->tmpArea, (char *) cache->wlnStatus.ssid, cache->wlnStatus.ssid_len);
	cache->tmpArea[cache->wlnStatus.ssid_len] = '\0'; // null terminate
	
	strcat(cache->tmpArea, " (");
	switch(cache->wlnStatus.state) {
		case WLN_ST_STOPPED:
			stateStr = "disabled";
			break;
		case WLN_ST_SCANNING:
			stateStr = "searching";
			break;
		case WLN_ST_ASSOC_ESS:
			// WPA/RSN info length field is in wpa_info[1]
			if (cache->wlnStatus.wpa_info[1] == 0) {
				stateStr = "connected"; // If not WPA then we're connected
			} else {
				stateStr = "not authenticated";
			}
			break;
		case WLN_ST_AUTH_ESS:
			stateStr = "connected";
			break;
		case WLN_ST_JOIN_IBSS:
		case WLN_ST_START_IBSS:
			stateStr = "ad-hoc";
			break;
                case WLN_ST_ASSOC_WPS:
                        stateStr = "connected for WPS";
                        break;
	}
	strcat(cache->tmpArea, stateStr);
	strcat(cache->tmpArea, ")");
#else
	cache->tmpArea[0] = '\0';
#endif
	
	return cache->tmpArea;
}

extern char *getWirelessCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	if (IPV4_ADDR(cache->wlnConfig.addrV4[0].ipAddress))
	{
		NAInet_toa(IPV4_ADDR(cache->wlnConfig.addrV4[0].ipAddress), cache->tmpArea);
		theResult = cache->tmpArea;
	}
#endif
	return theResult;
}

extern char *getWirelessCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentAutoIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	if (IPV4_ADDR(cache->wlnConfig.addrV4[1].ipAddress))
	{
		NAInet_toa(IPV4_ADDR(cache->wlnConfig.addrV4[1].ipAddress), cache->tmpArea);
		theResult = cache->tmpArea;
	}
#endif
	return theResult;
}

extern char *getWirelessCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 1, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getWirelessCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 2, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getWirelessCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 3, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getWirelessCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress4(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 4, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getWirelessCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress5(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 5, cache->tmpArea);

	return cache->tmpArea;
}

extern char *getWirelessCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessCurrentIv6pAddress6(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);

	getIPv6AddressString(&cache->wlnConfig, 6, cache->tmpArea);

	return cache->tmpArea;
}


extern char *getWirelessMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessMacAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
#if BSP_WANT_WIRELESS	
	BYTE mac[6];
	int rc;

	strcpy(cache->tmpArea, "");
	rc = customizeGetInterfaceMACAddress(BP_WLN_INTERFACE, mac);
    if (rc == BP_SUCCESS || rc == BP_FAILURE)
    {
		sprintf(cache->tmpArea, "%02X:%02X:%02X:%02X:%02X:%02X",
				mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }
#else
	cache->tmpArea[0] = '\0';
#endif
        	
	return cache->tmpArea;
}

extern Unsigned8 IS_WIRELESS_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 IS_WIRELESS_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	return 0;
#else
	return 1;
#endif
}

extern Unsigned8 getIsCPUMonitorEnabled(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 getIsCPUMonitorEnabled(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if CPU_LOAD_MEASURING
	return 0;
#else
	return 1;
#endif
}

extern Signed32 getCpuUtilization(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Signed32 getCpuUtilization(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {

#if CPU_LOAD_MEASURING
	return getCpuLoad();
#else
	return 0;
#endif
}

extern char *getUpTime(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getUpTime(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
	char *uptimeString = cache->tmpArea;
	unsigned long days = 0;
	unsigned long hours = 0;
	unsigned long minutes = 0;
	unsigned long seconds = ns_get_uptime_sec();
	
	if((minutes = (seconds / seconds_per_minute)) != 0){
		if((hours = (minutes / minutes_per_hour)) != 0){
			if((days = (hours / hours_per_day)) != 0){
				hours = (hours % hours_per_day);
			}
			minutes = (minutes % minutes_per_hour);
		}
		seconds = (seconds % seconds_per_minute);
	}
	
	strcpy(uptimeString, "");
	
	if(days > 0)   {format_uptime_string(uptimeString, "day", days);}
	if(hours > 0)  {format_uptime_string(uptimeString, "hour", hours);}
	if(minutes > 0){format_uptime_string(uptimeString, "minute", minutes);}
	if(seconds > 0){format_uptime_string(uptimeString, "second", seconds);}
	
	return uptimeString;
}

extern void indexPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void indexPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {

	SessionCache *sess = getSessionCache(theTaskDataPtr);
	IndexPgCache *cache = getIndexPageCache(theTaskDataPtr);
	
	if (!isPageCacheValid(sess, INDEX_PAGE_ID)) {
		/* Set defaults. */
		memset(cache, 0, sizeof(IndexPgCache));
		
#if BSP_WANT_ETHERNET
		customizeIamGetIfconfig(BP_ETH_INTERFACE, &cache->ethConfig); 
#endif		
#if BSP_WANT_WIRELESS
		customizeIamGetIfconfig(BP_WLN_INTERFACE, &cache->wlnConfig); 
		wln_get_status(&cache->wlnStatus);
#endif		
	}

	setCacheInvalid(sess);
}

void getDYNDATA (int nIndex, char * sz, int nLen)
{
	if (nIndex >= 0 && nIndex < FJMESSAGE_DYNAMIC_SEGMENTS)
		strncpy (sz, fj_CVT.pFJglobal->DynamicSegments [nIndex].strDynData, nLen);
}	
					
char *getDYNDATA1 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (0, sz, BUFFERSIZE); return sz; }
char *getDYNDATA2 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (1, sz, BUFFERSIZE); return sz; }
char *getDYNDATA3 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (2, sz, BUFFERSIZE); return sz; }
char *getDYNDATA4 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (3, sz, BUFFERSIZE); return sz; }
char *getDYNDATA5 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (4, sz, BUFFERSIZE); return sz; }
char *getDYNDATA6 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (5, sz, BUFFERSIZE); return sz; }
char *getDYNDATA7 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (6, sz, BUFFERSIZE); return sz; }
char *getDYNDATA8 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (7, sz, BUFFERSIZE); return sz; }
char *getDYNDATA9 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (8, sz, BUFFERSIZE); return sz; }
char *getDYNDATA10(void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {		static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (9, sz, BUFFERSIZE); return sz; }

char *getDYNDATA11 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (10, sz, BUFFERSIZE); return sz; }
char *getDYNDATA12 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (11, sz, BUFFERSIZE); return sz; }
char *getDYNDATA13 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (12, sz, BUFFERSIZE); return sz; }
char *getDYNDATA14 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (13, sz, BUFFERSIZE); return sz; }
char *getDYNDATA15 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (14, sz, BUFFERSIZE); return sz; }
char *getDYNDATA16 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (15, sz, BUFFERSIZE); return sz; }
char *getDYNDATA17 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (16, sz, BUFFERSIZE); return sz; }
char *getDYNDATA18 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (17, sz, BUFFERSIZE); return sz; }
char *getDYNDATA19 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (18, sz, BUFFERSIZE); return sz; }
char *getDYNDATA20 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (19, sz, BUFFERSIZE); return sz; }

char *getDYNDATA21 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (20, sz, BUFFERSIZE); return sz; }
char *getDYNDATA22 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (21, sz, BUFFERSIZE); return sz; }
char *getDYNDATA23 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (22, sz, BUFFERSIZE); return sz; }
char *getDYNDATA24 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (23, sz, BUFFERSIZE); return sz; }
char *getDYNDATA25 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (24, sz, BUFFERSIZE); return sz; }
char *getDYNDATA26 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (25, sz, BUFFERSIZE); return sz; }
char *getDYNDATA27 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (26, sz, BUFFERSIZE); return sz; }
char *getDYNDATA28 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (27, sz, BUFFERSIZE); return sz; }
char *getDYNDATA29 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (28, sz, BUFFERSIZE); return sz; }
char *getDYNDATA30 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (29, sz, BUFFERSIZE); return sz; }

char *getDYNDATA31 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (30, sz, BUFFERSIZE); return sz; }
char *getDYNDATA32 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (31, sz, BUFFERSIZE); return sz; }
char *getDYNDATA33 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (32, sz, BUFFERSIZE); return sz; }
char *getDYNDATA34 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (33, sz, BUFFERSIZE); return sz; }
char *getDYNDATA35 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (34, sz, BUFFERSIZE); return sz; }
char *getDYNDATA36 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (35, sz, BUFFERSIZE); return sz; }
char *getDYNDATA37 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (36, sz, BUFFERSIZE); return sz; }
char *getDYNDATA38 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (37, sz, BUFFERSIZE); return sz; }
char *getDYNDATA39 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (38, sz, BUFFERSIZE); return sz; }
char *getDYNDATA40 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (39, sz, BUFFERSIZE); return sz; }

char *getDYNDATA41 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (40, sz, BUFFERSIZE); return sz; }
char *getDYNDATA42 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (41, sz, BUFFERSIZE); return sz; }
char *getDYNDATA43 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (42, sz, BUFFERSIZE); return sz; }
char *getDYNDATA44 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (43, sz, BUFFERSIZE); return sz; }
char *getDYNDATA45 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (44, sz, BUFFERSIZE); return sz; }
char *getDYNDATA46 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (45, sz, BUFFERSIZE); return sz; }
char *getDYNDATA47 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (46, sz, BUFFERSIZE); return sz; }
char *getDYNDATA48 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (47, sz, BUFFERSIZE); return sz; }
char *getDYNDATA49 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (48, sz, BUFFERSIZE); return sz; }
char *getDYNDATA50 (void *theTaskDataPtr, char *theNamePtr, Signed16Ptr theIndexValuesPtr) {	static char sz [BUFFERSIZE] = { 0 }; getDYNDATA (49, sz, BUFFERSIZE); return sz; }

#endif	/* RomPagerServer */
