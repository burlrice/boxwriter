/* Created with PageBuilder version 4.04 on Thu Jul 13 11:08:39 2006 */

#include "AsExtern.h"
#include <stdlib.h>
#include <tx_api.h>
#include <gpiomux_def.h>
#include <bsp_api.h>
#include <bsp.h>
#include <boardParams.h>
#include <narmapi.h>
#include <iam_netos.hh>
#include "iam.hh"
#include <sysaccess.h>

#if BSP_WANT_WPA
#include "wpa_api.h"
#endif
#include "wln_api.h"
#include "webutil.h"
#include "session.h"

#if RomPagerServer

enum { NOT_SUPPORTED, SUPPORTED };


#if BSP_WANT_WIRELESS
#include "country_codes.h"

#define SUBMIT_TYPE_APPLY           0
#define SUBMIT_TYPE_UPDATE_COUNTRY  1
#define SUBMIT_TYPE_UPDATE_BAND     2


#if (AIROHA_TRANSCEIVER == 1)
extern BOOLEAN isRF_AIROHA_7230(void);
#endif
extern int wln_get_chan_info(int chan_index, int *chan_freq);

extern rpObjectDescription Pgwireless_network_config;
extern rpObjectDescription Pgerrhandler;

#define WIRELESS_NET_CONFIG_ID &Pgwireless_network_config

// Page Cache
typedef struct {
	devBoardParamsType nvParams;
    rpOneOfSeveral bss_type;
    Unsigned8 channel;
    char ssid[WLN_SSID_SIZE+1];
    rpOneOfSeveral authen;
    rpOneOfSeveral encrypt;
    char wepkey0[WLN_WEP104_SIZE * 2 + 1];
    char wepkey1[WLN_WEP104_SIZE * 2 + 1];
    char wepkey2[WLN_WEP104_SIZE * 2 + 1];
    char wepkey3[WLN_WEP104_SIZE * 2 + 1];        
    char login[WLN_IDENT_SIZE + 1]; 
    char password[WLN_PWD_SIZE + 1];  
    char wpaPass[64];  
    rpOneOfSeveral defaultWepKey;
    rpOneOfSeveral wepKeySize;
    Unsigned32 country_code;
    int band;
    Unsigned32 option_80211;
    char channel_string[20];
    Unsigned16          submit_type;
} WirelessNetPgCache;


static wln_country_code_t * supported_country_list = NULL;
static int                 country_count = 0;

#if BSP_WANT_WPA
static int encrypt_types[] = {
	WLN_ENCR_ANY, WLN_ENCR_OPEN, WLN_ENCR_WEP, WLN_ENCR_TKIP, WLN_ENCR_CCMP,
};

static int authen_types[] = {
	WLN_AUTH_ANY, WLN_AUTH_OPEN, WLN_AUTH_SHAREDKEY, WLN_AUTH_WEP_8021X,
	WLN_AUTH_WPA_PSK, WLN_AUTH_WPA_8021X, WLN_AUTH_LEAP,
};
#endif // BSP_WANT_WPA

//-------------------------------------
// Helper functions
//-------------------------------------

WirelessNetPgCache *getWirelessNetPageCache(void *theTaskDataPtr) {    
    SessionCache *sess = getSessionCache(theTaskDataPtr);
    if (isPageCacheEmpty(sess) || !isSessionCacheID(sess, WIRELESS_NET_CONFIG_ID))
        setPageCache(sess, malloc(sizeof(WirelessNetPgCache)), WIRELESS_NET_CONFIG_ID);
    return (WirelessNetPgCache *) getPageCache(sess, WIRELESS_NET_CONFIG_ID);
}

static int setWepKey(SessionCache *sess, char *wepkey, unsigned char *key, int idx) {
   	int len = strlen(wepkey);
   	int i, n;
   	char *buf;
    char errMsg[64];
   	
    if (len == 0) {
    	// clear the key
        for (i = 0; i < WLN_WEP104_SIZE; i++) {
            key[i] = 0;
        }
    	return 1;
    } else if ((len != (WLN_WEP104_SIZE * 2)) && (len != (WLN_WEP40_SIZE * 2))) {
        sprintf(errMsg, "WEP key %d must be 10 or 26 hex digits.", idx+1);
	    addSessionMsg(sess, WEBUI_ERROR_MSG, errMsg);
	    return 0;
    } else {    
        buf = wepkey;
        for (i = 0; i < len/2; i++) {
  	        if (isxdigit(buf[2*i]) && isxdigit(buf[2*i+1]) &&
	           sscanf(&buf[2*i], "%2x", &n) == 1) {
                key[i] = n;
            } else {
		        sprintf(errMsg, "WEP key %d must be hex digits.", idx+1);
			    addSessionMsg(sess, WEBUI_ERROR_MSG, errMsg);
			    return 0;
	        }
  	    }
    }
    return 1;
}

#if BSP_WANT_WPA
static void setAuthenticationType(rpOneOfSeveral selectedAuthen, unsigned int *authen) {
	if (selectedAuthen < sizeof(authen_types)) {
		*authen = authen_types[selectedAuthen];
	} else {
		*authen = authen_types[0];
	}
}

static rpOneOfSeveral getAuthenticationType(unsigned int authen) {
	rpOneOfSeveral result = 0;
	int i;
	
	// Find the first authentication type match. Use it's index as our value.
	for (i=0; i<sizeof(authen_types); ++i) {
		if ((authen_types[i] & authen) == authen_types[i]) {
			result = i;
			break;
		}
	}
	
	return result;
}

static void setEncryptionType(rpOneOfSeveral selectedEncrypt, unsigned int *encrypt) {
	if (selectedEncrypt < sizeof(encrypt_types)) {
		*encrypt = encrypt_types[selectedEncrypt];
	} else {
		*encrypt = encrypt_types[0];
	}
}


static rpOneOfSeveral getEncryptionType(unsigned int encrypt) {
	rpOneOfSeveral result = 0;
	int i;
	
	// Find the first encryption type match. Use it's index as our value.
	for (i=0; i<sizeof(encrypt_types); ++i) {
		if ((encrypt_types[i] & encrypt) == encrypt_types[i]) {
			result = i;
			break;
		}
	}
	
	return result;
}

#endif // BSP_WANT_WPA
#endif // BSP_WANT_WIRELESS



/* ******************************************************* */
/* *    Built from "html\wireless_network_config.htm"    * */
/* ******************************************************* */


extern Unsigned16 getSubmitType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
Unsigned16 getSubmitType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	Unsigned16  theResult = 0;
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    theResult = cache->submit_type;
#endif
	return theResult;
}

extern void setSubmitType(void *theTaskDataPtr, Unsigned16 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setSubmitType(void *theTaskDataPtr, Unsigned16 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
    cache->submit_type = theValue;
#endif
	return;
}

extern rpOneOfSeveral getWirelessNetworkType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getWirelessNetworkType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->bss_type;
#else
	return 0;
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessNetworkType(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessNetworkType(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	cache->bss_type = theValue;
#endif	
}

extern char *getSSID(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getSSID(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->ssid;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setSSID(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setSSID(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->ssid, theValuePtr, sizeof(cache->ssid)-1);
#endif // BSP_WANT_WIRELESS	
}


extern void *getWirelessCountryName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
void *getWirelessCountryName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr) {
	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
    wln_country_code_t country_code;

    theResult = (void *)NULL;  /* setup no more options */
    *theOptionSelectedFlag = False;

//    country_code_list = wln_get_supported_country_list(&country_count);
    if (supported_country_list != NULL)
    {
        if (theItemNumber < country_count)
        {
            country_code = supported_country_list[theItemNumber];
            theResult = (char *)wln_get_country_string_from_code(country_code);
            if (supported_country_list[theItemNumber] == (Unsigned32)cache->country_code)
            {
                *theOptionSelectedFlag = True;
            }
            *theValuePtr = (Unsigned32)country_code;
        }
    }
#endif // BSP_WANT_WIRELESS	

    return (void *)theResult;
}

extern void setWirelessCountryName(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessCountryName(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    if (wln_is_country_valid((wln_country_code_t)theValue))
    {
        cache->country_code = (Unsigned32)theValue;
    }
#endif
	return;
}


#if BSP_WANT_WIRELESS
static char *wireless_80211_option_strings[] = {
    "None",
    "Multi Domain Capability (802.11d)",
    "Spectrum Management Capability (802.11h)"
};

static Unsigned32 wireless_80211_option_values[] = {
    0, 
    WLN_OPT_MULTI_DOMAIN,
    WLN_OPT_MULTI_DOMAIN | WLN_OPT_SPECTRUM_MGMT,
};
#endif

extern void *get80211Options(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
void *get80211Options(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr) {
	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
    int                 option_limit = asizeof(wireless_80211_option_values);

#if (AIROHA_TRANSCEIVER == 1 )
    /* adjust the option.
     * skip last one if AIROHA_7230 is not supported.
     */
    if (!isRF_AIROHA_7230()) 
    {
        option_limit--;
	}
#else
    /* AIRONA_7230 not supported */
     option_limit--;
#endif

    *theOptionSelectedFlag = False;
    if (theItemNumber < option_limit)
    {
        theResult = wireless_80211_option_strings[theItemNumber];
        *theValuePtr = wireless_80211_option_values[theItemNumber];
    }

    while (--option_limit >= 0)
    {
        if ((cache->option_80211 & wireless_80211_option_values[theItemNumber]) == 
                                    wireless_80211_option_values[theItemNumber])
        {
            *theOptionSelectedFlag = True;
            break;
        }
    }
#endif /* BSP_WANT_WIRELESS */
    return (void *) theResult;
}

extern void set80211Options(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void set80211Options(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    /* mask of the 802.11d and 802.11h bits */
    cache->option_80211 &= ~(WLN_OPT_MULTI_DOMAIN | WLN_OPT_SPECTRUM_MGMT);
    cache->option_80211 |= theValue;

#endif /* BSP_WANT_WIRELESS */
	return;
}


int isValidWirelessChannel(unsigned long country_code, int band, int sys_channel)
{
    int isValid = 0;
#if BSP_WANT_WIRELESS
    int max, min;
    int disp_channel, channel;
    unsigned long mask[2];
    BOOL chan_11a = FALSE;

    if (sys_channel == WLN_CHAN_SCAN)
    {
        isValid =1;
        goto _ret;
    }
    if (wln_get_channel_mask(country_code, mask) < 0)
    {
        goto _ret;
    }
        
    max = ((band == WLN_BAND_B) || (band == WLN_BAND_BG)) ? 14 : WLN_CHAN_NUM;
    min = (band == WLN_BAND_A) ? 17 : 1;
        
    if (band == WLN_BAND_A) 
    {
        chan_11a = TRUE;
    }
            
    disp_channel = wln_get_chan_info(sys_channel, NULL);
    channel = wln_is_channel_supported( (uint32 *)mask, disp_channel, chan_11a);
      
    if (channel > min && channel <= max && mask[(channel-1)/32] & (1 << (channel-1)%32))
    {   /* good channel */
        isValid = 1;
    }
_ret:
#endif
    return isValid;
}
extern void *getWirelessChannel(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
void *getWirelessChannel(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr) {
	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    int max, min;
    int chan_index, item_index;
    unsigned long mask[2];

    *theOptionSelectedFlag = False;

    if (theItemNumber == 0)
    {
        theResult = "0 (Auto-Scan)";
        *theValuePtr = (Unsigned32)WLN_CHAN_SCAN;
        if (cache->channel == WLN_CHAN_SCAN)
        {
            *theOptionSelectedFlag = True;

        }
    }
    else
    {

        if (wln_get_channel_mask(cache->country_code, mask) < 0)
        {
            goto _ret;
        }
        
        max = ((cache->band == WLN_BAND_B) || (cache->band == WLN_BAND_BG)) ? 14 : WLN_CHAN_NUM;
        min = ((cache->band == WLN_BAND_A)) ? 17 : 1;
        
        for (item_index = 1, chan_index = min; chan_index <= max; chan_index++)
        {
            if (mask[(chan_index-1)/32] & (1 << (chan_index-1)%32))
            {
                if (item_index == theItemNumber)
                {
                    if (chan_index > 14 && ((cache->band == WLN_BAND_A) || 
                       (cache->band == WLN_BAND_DEFAULT))) 
                        sprintf(cache->channel_string, "%d on Band-A", wln_get_chan_info(chan_index, NULL));
                    else
                        sprintf(cache->channel_string, "%d", wln_get_chan_info(chan_index, NULL));

                    if (cache->channel == chan_index)
                    {
                        *theOptionSelectedFlag = True;
                    }
                    theResult = cache->channel_string;
                    *theValuePtr = (Unsigned32)chan_index;
                    break;
                }
                item_index++;
            }
        }
    }

_ret:
#endif

    return (void *)theResult;
}

extern void setWirelessChannel(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessChannel(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    cache->channel = (Unsigned8)theValue;
#endif

	return;
}


/* ARM7 only supports B band */
extern char *displayWirelessBandB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *displayWirelessBandB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	char * theResult = NULL;

#if BSP_WANT_WIRELESS
    theResult = "Band b Only";
#endif

	return theResult;
}

extern void *getWirelessBand(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
void *getWirelessBand(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr) {
	char * theResult = NULL;

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    char *supported_bands[] = {
        "All", "B", "BG", "A"
    };
    int band_values[] =  {
        WLN_BAND_DEFAULT, WLN_BAND_B, WLN_BAND_BG, WLN_BAND_A
    } ;

    int band_count = asizeof(supported_bands);

#if (AIROHA_TRANSCEIVER == 1 )
    /* adjust the band_count */
    if (!isRF_AIROHA_7230()) 
    {
        /* skip the A band */
        band_count--;
    }
#endif
    *theOptionSelectedFlag = False;
    if (theItemNumber < band_count)
    {
        *theValuePtr = (Unsigned32) band_values[theItemNumber];
        theResult = supported_bands[theItemNumber];
        if (cache->band == band_values[theItemNumber])
        {
            *theOptionSelectedFlag = True;
        }
    }
#endif

    return (void *) theResult;
}

extern void setWirelessBand(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessBand(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);

    cache->band = (int)theValue;
#endif
	return;
}

extern Unsigned8 IS_ARM9(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 IS_ARM9(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	Unsigned8  theResult = 0;

#ifdef BSP_ARM9       
    theResult = 1;
#endif
	return theResult;

}

extern rpOneOfSeveral getWirelessAuthentication(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getWirelessAuthentication(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->authen;
#else
	return 0;
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessAuthentication(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessAuthentication(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	cache->authen = theValue;
#endif // BSP_WANT_WIRELESS		
}

extern rpOneOfSeveral getWirelessEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getWirelessEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->encrypt;
#else
	return 0;
#endif // BSP_WANT_WIRELESS		
}

extern void setWirelessEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	cache->encrypt = theValue;
#endif // BSP_WANT_WIRELESS		
}

extern char *getWPAPassphrase(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWPAPassphrase(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wpaPass;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWPAPassphrase(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWPAPassphrase(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->wpaPass, theValuePtr, sizeof(cache->wpaPass)-1);
#endif // BSP_WANT_WIRELESS	
}

extern char *getWirelessLogin(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessLogin(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->login;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWirelessLogin(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessLogin(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->login, theValuePtr, sizeof(cache->login)-1);
#endif // BSP_WANT_WIRELESS	
}

extern char *getWirelessPassword(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWirelessPassword(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->password;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWirelessPassword(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessPassword(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->password, theValuePtr, sizeof(cache->password)-1);
#endif // BSP_WANT_WIRELESS	
}

extern Unsigned8 IS_WPA_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
Unsigned8 IS_WPA_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {

#if BSP_WANT_WPA
	return 0;
#else
	return 1;
#endif
}

extern rpOneOfSeveral getWirelessWEPEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getWirelessWEPEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wepKeySize;
#else
	return 0;
#endif // BSP_WANT_WIRELESS	
}

extern void setWirelessWEPEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWirelessWEPEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	cache->wepKeySize = theValue;
#endif
}

extern rpOneOfSeveral getDefaultWEPKey(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpOneOfSeveral getDefaultWEPKey(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->defaultWepKey;
#else
	return 0;
#endif // BSP_WANT_WIRELESS		
}

extern void setDefaultWEPKey(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setDefaultWEPKey(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS			
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	cache->defaultWepKey = theValue;
#endif	
}

extern char *getWEPKey0(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWEPKey0(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wepkey0;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWEPKey0(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWEPKey0(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->wepkey0, theValuePtr, sizeof(cache->wepkey0));
#endif	
}

extern char *getWEPKey1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWEPKey1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wepkey1;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWEPKey1(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWEPKey1(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->wepkey1, theValuePtr, sizeof(cache->wepkey1));
#endif	
}

extern char *getWEPKey2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWEPKey2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wepkey2;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWEPKey2(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWEPKey2(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->wepkey2, theValuePtr, sizeof(cache->wepkey2));
#endif	
}

extern char *getWEPKey3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWEPKey3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	return cache->wepkey3;
#else
	return "";
#endif // BSP_WANT_WIRELESS
}

extern void setWEPKey3(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWEPKey3(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	strncpy(cache->wepkey3, theValuePtr, sizeof(cache->wepkey3));
#endif	
}

//extern void wirelessNetworkSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void wirelessNetworkSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS
	SessionCache *sess = getSessionCache(theTaskDataPtr);
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	int status = 0;
	int n = 0;
	int writeParms = 1;
	

    if (cache->submit_type == SUBMIT_TYPE_UPDATE_COUNTRY ||
        cache->submit_type == SUBMIT_TYPE_UPDATE_BAND)
    {
        addSessionMsg(sess, WEBUI_STATUS_MSG, "Press Sumbit for changes to take effect.");
        setCacheValid(sess);
        goto _ret;
    }


	n = strlen(cache->ssid);
	memcpy(cache->nvParams.wlnParams.ssid, cache->ssid, n);
	cache->nvParams.wlnParams.ssid_len = n;
	cache->nvParams.wlnParams.bss_type = cache->bss_type;

    if (isValidWirelessChannel(cache->country_code, cache->band, cache->channel) == 1)
	    cache->nvParams.wlnParams.channel = cache->channel;
    else
        writeParms = 0; /* error */

    cache->nvParams.wlnExtParams.wlnCountryCode = cache->country_code;
    cache->nvParams.wlnParams.options = cache->option_80211;

    cache->nvParams.wlnParams.band = cache->band;


#if BSP_WANT_WPA

	// WPA not supported in ad-hoc modes. (NOTE: encryption type still set)
	if ((cache->nvParams.wlnParams.bss_type != WLN_BSS_IBSS) &&
		(cache->nvParams.wlnParams.bss_type != WLN_BSS_IBSS_JOIN)) {
		
		setAuthenticationType(cache->authen, &cache->nvParams.wlnParams.authen);
		
		// EAP is currently only configurable through the CLI. If, however,
		// LEAP authentication is selected then the outer EAP must be set to
		// LEAP as well. If it is not enabled then outer EAP must have LEAP off.
		if (cache->nvParams.wlnParams.authen & WLN_AUTH_LEAP) {
			cache->nvParams.wlnParams.eap_methods |= WLN_EAP_LEAP;
		} else {
			cache->nvParams.wlnParams.eap_methods &= ~WLN_EAP_LEAP;
		}
		
		if (cache->nvParams.wlnParams.authen & WLN_AUTH_WPA_PSK) {
			// Generate a PSK only if a passphrase was specified.
			int passphraseLen = strlen(cache->wpaPass);
			if (passphraseLen > 0) {
				if (cache->nvParams.wlnParams.ssid_len == 0) {
				    addSessionMsg(sess, WEBUI_ERROR_MSG, "SSID required to use WPA Personal.");
				    writeParms = 0;
				} else if (passphraseLen < 8 || passphraseLen > 63) {
				    addSessionMsg(sess, WEBUI_ERROR_MSG, "Passphrase must be 8 to 63 characters.");
				    writeParms = 0;
				} else {
					// Generate the PSK from the passphrase and ssid.
					wpa_passphrase_to_psk(
						cache->wpaPass,
						(char*)cache->nvParams.wlnParams.ssid,
						cache->nvParams.wlnParams.ssid_len,
						cache->nvParams.wlnParams.wpa_psk);
				}
			}
		}
		
		// Login/password only supported for WEP 802.1x, WPA 802.1x, and LEAP
		if ((cache->nvParams.wlnParams.authen & WLN_AUTH_WEP_8021X) ||
			(cache->nvParams.wlnParams.authen & WLN_AUTH_WPA_8021X) ||
			(cache->nvParams.wlnParams.authen & WLN_AUTH_LEAP)) {
			strncpy(cache->nvParams.wlnParams.identity, cache->login, WLN_IDENT_SIZE);
			strncpy(cache->nvParams.wlnParams.password, cache->password, WLN_PWD_SIZE);
		}
		
		setEncryptionType(cache->encrypt, &cache->nvParams.wlnParams.encrypt);
		
	   	// Only Open and Shared Key authentication require WEP keys. Shared key always requires
	   	// WEP keys because they are used for the authentication (not just data encryption).
		// The other authentication types obtain keys dynamically.
		if ((cache->nvParams.wlnParams.authen & WLN_AUTH_SHAREDKEY) ||
			((cache->nvParams.wlnParams.authen & WLN_AUTH_OPEN) &&
			 (cache->nvParams.wlnParams.encrypt & WLN_ENCR_WEP))) {
		   	cache->nvParams.wlnParams.wep_key_len = cache->wepKeySize;
	    	cache->nvParams.wlnParams.wep_key_id = cache->defaultWepKey;
	        writeParms &= setWepKey(sess, cache->wepkey0, cache->nvParams.wlnParams.wep_key[0], 0);
	        writeParms &= setWepKey(sess, cache->wepkey1, cache->nvParams.wlnParams.wep_key[1], 1);
	        writeParms &= setWepKey(sess, cache->wepkey2, cache->nvParams.wlnParams.wep_key[2], 2);
	        writeParms &= setWepKey(sess, cache->wepkey3, cache->nvParams.wlnParams.wep_key[3], 3);        
	    }
	} else {
		// Ad-hoc mode
		setEncryptionType(cache->encrypt, &cache->nvParams.wlnParams.encrypt);

		if (cache->nvParams.wlnParams.encrypt & WLN_ENCR_WEP) {
		   	cache->nvParams.wlnParams.wep_key_len = cache->wepKeySize;
	    	cache->nvParams.wlnParams.wep_key_id = cache->defaultWepKey;
	        writeParms &= setWepKey(sess, cache->wepkey0, cache->nvParams.wlnParams.wep_key[0], 0);
	        writeParms &= setWepKey(sess, cache->wepkey1, cache->nvParams.wlnParams.wep_key[1], 1);
	        writeParms &= setWepKey(sess, cache->wepkey2, cache->nvParams.wlnParams.wep_key[2], 2);
	        writeParms &= setWepKey(sess, cache->wepkey3, cache->nvParams.wlnParams.wep_key[3], 3);        
	    }
	}
	
#else
	// No WPA. Set appropriate authen and encrypt flags.
	// Always save the WEP key size. A size of 0 disbles WEP.
   	cache->nvParams.wlnParams.wep_key_len = cache->wepKeySize;
	if (cache->wepKeySize != 0) {
		cache->nvParams.wlnParams.authen = WLN_AUTH_OPEN | WLN_AUTH_SHAREDKEY | WLN_AUTH_WEP_8021X;
		cache->nvParams.wlnParams.encrypt = WLN_ENCR_WEP;
    	cache->nvParams.wlnParams.wep_key_id = cache->defaultWepKey;
        writeParms &= setWepKey(sess, cache->wepkey0, cache->nvParams.wlnParams.wep_key[0], 0);
        writeParms &= setWepKey(sess, cache->wepkey1, cache->nvParams.wlnParams.wep_key[1], 1);
        writeParms &= setWepKey(sess, cache->wepkey2, cache->nvParams.wlnParams.wep_key[2], 2);
        writeParms &= setWepKey(sess, cache->wepkey3, cache->nvParams.wlnParams.wep_key[3], 3);        
	} else {
		cache->nvParams.wlnParams.authen = WLN_AUTH_OPEN;
		cache->nvParams.wlnParams.encrypt = WLN_ENCR_OPEN;
	}
	
#endif // BSP_WANT_WPA


	
	if (writeParms) {
	  status = customizeWriteDevBoardParams(&cache->nvParams);

	  if (status != BP_SUCCESS) {
		  setCacheValid(sess); // Leave user entered data in form
		  addSessionMsg(sess, WEBUI_ERROR_MSG, "Critical error saving wireless settings!");
	  } else {
		  setCacheInvalid(sess);
		  addSessionMsg(sess, WEBUI_STATUS_MSG, "Settings have been saved. You must reboot for changes to take effect.");
	  }
	} else {
		setCacheValid(sess);
	}
_ret:
    return;
#endif // BSP_WANT_WIRELESS
}
//#endif


extern void wirelessNetworkConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void wirelessNetworkConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
#if BSP_WANT_WIRELESS

	SessionCache *sess = getSessionCache(theTaskDataPtr);
	WirelessNetPgCache *cache = getWirelessNetPageCache(theTaskDataPtr);
	NaIamParams_t *interface;
	int i, n;
	
    if (supported_country_list == NULL)
    {
        supported_country_list = wln_get_supported_country_list(&country_count);
    }
	if (!isPageCacheValid(sess, WIRELESS_NET_CONFIG_ID)) {
		/* Set defaults. */
		memset(cache, 0, sizeof(WirelessNetPgCache));
				    	
		/* Read current settings */
		customizeReadDevBoardParams(&cache->nvParams);
		
       /* Extract the configuration structure from the NVRAM structure.  There should be a configuration */
       /* settings for the wln0 interface.  If not, then reinitialize the structure. */
       interface = customizeIamFindInterfaceConfig(BP_WLN_INTERFACE, &cache->nvParams.iamParamsInfo);
	   if (interface == NULL) {
		  interface = customizeIamFindUnusedInterfaceConfig(&cache->nvParams.iamParamsInfo, FALSE);
		  if (interface) {
			customizeIamGetDefaultInterfaceConfig(BP_WLN_INTERFACE, interface);
		  }
	    }

		n = cache->nvParams.wlnParams.ssid_len;
		memcpy(cache->ssid, cache->nvParams.wlnParams.ssid, n);
		cache->ssid[n] = '\0';
		cache->bss_type = cache->nvParams.wlnParams.bss_type;
		cache->channel = cache->nvParams.wlnParams.channel;
        cache->band = cache->nvParams.wlnParams.band;
        cache->country_code = cache->nvParams.wlnExtParams.wlnCountryCode;
        cache->option_80211 = cache->nvParams.wlnParams.options;
        cache->submit_type = SUBMIT_TYPE_APPLY;

#if BSP_WANT_WPA
		cache->authen = getAuthenticationType(cache->nvParams.wlnParams.authen);
		
		cache->wpaPass[0] = '\0';
		strncpy(cache->login, cache->nvParams.wlnParams.identity, WLN_IDENT_SIZE);
		strncpy(cache->password, cache->nvParams.wlnParams.password, WLN_PWD_SIZE);


		cache->encrypt = getEncryptionType(cache->nvParams.wlnParams.encrypt);
#endif // BSP_WANT_WPA

		cache->wepKeySize = cache->nvParams.wlnParams.wep_key_len;
		cache->defaultWepKey = cache->nvParams.wlnParams.wep_key_id;
		
		char *keyPtr = cache->wepkey0;
		char *bytePtr = (char *)cache->nvParams.wlnParams.wep_key[0];
		for (i=0; i < cache->nvParams.wlnParams.wep_key_len;i++) {
		    sprintf(keyPtr+(i*2), "%02X", *(bytePtr+i));
		}
		
		keyPtr = cache->wepkey1;
		bytePtr = (char *)cache->nvParams.wlnParams.wep_key[1];
		for (i=0; i < cache->nvParams.wlnParams.wep_key_len;i++) {
		    sprintf(keyPtr+(i*2), "%02X", *(bytePtr+i));
		}
		
		keyPtr = cache->wepkey2;
		bytePtr = (char *)cache->nvParams.wlnParams.wep_key[2];
		for (i=0; i < cache->nvParams.wlnParams.wep_key_len;i++) {
		    sprintf(keyPtr+(i*2), "%02X", *(bytePtr+i));
		}
		
		keyPtr = cache->wepkey3;
		bytePtr = (char *)cache->nvParams.wlnParams.wep_key[3];
		for (i=0; i < cache->nvParams.wlnParams.wep_key_len;i++) {
		    sprintf(keyPtr+(i*2), "%02X", *(bytePtr+i));
		}

	}

	setCacheInvalid(sess);
	
#endif // BSP_WANT_WIRELESS
}


#endif	/* RomPagerServer */

