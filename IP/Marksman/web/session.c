#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "session.h"

// for now, there is just one static cache.
// TODO: add logic for session scoped caching.
static SessionCache sessCache;

SessionCache *getSessionCache(void * theTaskDataPtr)
{
    return &sessCache;
}

Boolean addSessionMsg(SessionCache *cache, unsigned char type, char * inMsg)
{
    if(cache->msg.isActive == True) return False;
    else {
        cache->msg.type = type;
        strncpy(cache->msg.msg, inMsg, MAX_SESSION_MSG);
        cache->msg.msg[MAX_SESSION_MSG] = '\0';
        cache->msg.isActive = True;
        return True;
    } 
}

Boolean addStdSuccessMsg(SessionCache *cache)
{
	//TODO: localizable status msg
    return addSessionMsg(cache, WEBUI_STATUS_MSG, "Changes have been saved successfully."); 
}

SessionMsg *readAndClearSessionMsg(SessionCache *cache)
{
    if(cache->msg.isActive == True) {
        cache->msg.isActive = False;
        return &(cache->msg);
    } else return NULL;
}

SessionMsg *getSessionMsg(SessionCache *cache)
{
    if(cache->msg.isActive == True) {
        return &(cache->msg);
    } else return NULL;
}

Boolean isSessionMsgFull(SessionCache *cache)
{
    if(cache->msg.isActive==True) return True;
    else return False;
}

Boolean isPageCacheValid(SessionCache *cache, CACHE_ID_TYPE id)
{
    if( isPageCacheEmpty(cache) ) return False;
    if( ! (isSessionCacheID(cache, id) ) ) return False;
    else {
        if(cache->isValid == True) return True;
        else return False;
    }
}

Boolean isPageCacheEmpty(SessionCache *cache)
{
    if (cache->pageCache == NULL) return True;
    else return False;
}

Boolean isSessionCacheID(SessionCache *cache, CACHE_ID_TYPE id)
{
    if(cache->id == id) return True;
    else return False;
}
       
void * getPageCache(SessionCache *cache, CACHE_ID_TYPE id)
{
    if(cache->id != id) {
        cache->id = id;
        cache->isValid=False;
        if (cache->pageCache) free(cache->pageCache);
        cache->pageCache = NULL;
    }
    
    return cache->pageCache;
}

void setPageCache(SessionCache *sess, void *cache, CACHE_ID_TYPE id)
{
    if (sess->pageCache) free(sess->pageCache);
    sess->pageCache = cache;
    sess->id = id;
}

void setCacheInvalid(SessionCache *cache)
{
    cache->isValid = False;
}

void setCacheValid(SessionCache *cache)
{
    cache->isValid = True;
}

void repeatWhileMoreStatusMessages(void *theTaskDataPtr, Signed16Ptr theIndexPtr, 
                                   void **theRepeatGroupValuePtr, void * theRquestPtr)
{

    SessionCache *s = getSessionCache(theTaskDataPtr);
    SessionMsg *msg = getSessionMsg(s);

    if(msg == NULL) *theRepeatGroupValuePtr = NULL;
    else if(msg->type == WEBUI_STATUS_MSG) *theRepeatGroupValuePtr = theTaskDataPtr;
    else *theRepeatGroupValuePtr = NULL;

}

// Called by all pages (see xsl tranform).
void repeatWhileMoreErrorMessages(void *theTaskDataPtr, Signed16Ptr theIndexPtr, 
                                  void **theRepeatGroupValuePtr, void * theRquestPtr)
{
    
    SessionCache *s = getSessionCache(theTaskDataPtr);
    SessionMsg *msg = getSessionMsg(s);

    if(msg == NULL) *theRepeatGroupValuePtr = NULL;
    else if(msg->type == WEBUI_ERROR_MSG) *theRepeatGroupValuePtr = theTaskDataPtr;
    else *theRepeatGroupValuePtr = NULL;
}

// Called by all pages (see xsl tranform).
// Retrieves message text of current message. Repeat functions above are used to determine
// which type of message is active.
char *getMessageText(void *theTaskDataPtr, char * theNamePtr, Signed16Ptr theIndexValuesPtr)
{
    SessionCache *s = getSessionCache(theTaskDataPtr);
    SessionMsg *msg = readAndClearSessionMsg(s);

    if(msg==NULL) return "";
    else return msg->msg;

}

// Called by all pages (see xsl tranform).
char *clearMessages() {
    return "";
}
