#ifndef WEBMENU_H_
#define WEBMENU_H_

#if RomPagerServer

#include "AsExtern.h"


#define MAX_HTML_MENU_ITEM_NAME_LEN    32

typedef struct {
	rpObjectDescription *page;
	const char 			name[MAX_HTML_MENU_ITEM_NAME_LEN+1];
} html_menu_item;


// Returns the menu item for the given index, or null
html_menu_item* getHtmlMenuItem(int index);


#endif	/* RomPagerServer */

#endif /*WEBMENU_H_*/
