#include <stdio.h>
#include <string.h>
#include <Npttypes.h>
#include "AsExtern.h"
#include "webutil.h"


/* Takes an unsinged long IP address and converts it to a text string. */
void ipaddr_to_str(char* ipStr, unsigned long ipAddr) {
#ifndef _LITTLE_ENDIAN
	sprintf(ipStr, "%d.%d.%d.%d",
			(int)(ipAddr >> 24) & 0xFF,
			(int)(ipAddr >> 16) & 0xFF,
			(int)(ipAddr >> 8)  & 0xFF,
			(int)(ipAddr) & 0xFF);
#else
	sprintf(ipStr, "%d.%d.%d.%d",
			(int)(ipAddr) & 0xFF,
			(int)(ipAddr >> 8)  & 0xFF,
			(int)(ipAddr >> 16) & 0xFF,
			(int)(ipAddr >> 24) & 0xFF);
#endif
}

/* Changes from Rompager IP array format to ulong IP format. */
unsigned long rompager_to_ipaddr(char* rompagerIpAddr) {
    unsigned long ipAddr;
    unsigned long a, b, c, d;

    a = (unsigned char)rompagerIpAddr[0];
    b = (unsigned char)rompagerIpAddr[1];
    c = (unsigned char)rompagerIpAddr[2];
    d = (unsigned char)rompagerIpAddr[3];

    memset(&ipAddr, 0, sizeof(ipAddr));

#ifndef _LITTLE_ENDIAN
    ipAddr |= (a <<= 24);
    ipAddr |= (b <<= 16);
    ipAddr |= (c <<= 8);
    ipAddr |= (d);
#else
    ipAddr |= (d <<= 24);
    ipAddr |= (c <<= 16);
    ipAddr |= (b <<= 8);
    ipAddr |= (a);
#endif

    return ipAddr;
}

/* Takes an unsigned long IP address and converts it to the RomPager 4 byte char array format. */
void ipaddr_to_rompager(char* rompagerIpAddr, unsigned long ipAddr) {
    unsigned int a,b,c,d;

#ifndef _LITTLE_ENDIAN
    a = (ipAddr&0xff000000) >> 24;
    b = (ipAddr&0x00ff0000) >> 16;
    c = (ipAddr&0x0000ff00) >> 8;
    d = (ipAddr&0x000000ff);
#else
    d = (ipAddr&0xff000000) >> 24;
    c = (ipAddr&0x00ff0000) >> 16;
    b = (ipAddr&0x0000ff00) >> 8;
    a = (ipAddr&0x000000ff);
#endif

    rompagerIpAddr[0] = a;
    rompagerIpAddr[1] = b;
    rompagerIpAddr[2] = c;
    rompagerIpAddr[3] = d;
}
