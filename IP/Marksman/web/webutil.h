#ifndef WEBUTIL_H
#define WEBUTIL_H

#ifdef __cplusplus
extern "C"
{
#endif


#define ROMPAGER_IP_ARRAY_LENGTH        4

#define TO_ONE_BASED(x) ((x) + 1)
#define TO_ZERO_BASED(x) ((x) - 1)


/* Takes a ulong IP address and converts it to a text string. */
void ipaddr_to_str(char* ipStr, unsigned long ipAddr);

/* Changes from Rompager 4 byte char array format to ulong IP address. */
unsigned long rompager_to_ipaddr(char* rompagerIpAddr);

/* Takes a ulong IP address and converts it to the RomPager 4 byte char array format. */
void ipaddr_to_rompager(char* rompagerIpAddr, unsigned long ipAddr);


#ifdef __cplusplus
}
#endif

#endif // WEBUTIL_H

