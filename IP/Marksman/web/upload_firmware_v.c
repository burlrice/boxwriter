/* Created with PageBuilder version 4.04 on Fri Jun 23 14:25:18 2006 */

#include "AsExtern.h"
#include <fs_api.h>

#include <fileinit.h>
#include <httpsvr.h>
#include <firmware.h>
#include "session.h"

#if RomPagerServer



/* *********************************************** */
/* *    Built from "html\upload_firmware.htm"    * */
/* *********************************************** */

extern char *getWebUpgradeType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
char *getWebUpgradeType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr) {
	// This is only needed so that Page has some value in it
	return "firmware";
}

extern void setWebUpgradeType(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
void setWebUpgradeType(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr) {

	RpHSSetCurrentDirectory(RpGetCurrentConnection(theTaskDataPtr), FS_FLASH_VOLUME_ROOT_DIR);
	RpHSSetStorageLocation(RpGetCurrentConnection(theTaskDataPtr), NAFS_LOCATION_FIRMWARE);
}

extern void submitUpgradeFirmware(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void submitUpgradeFirmware(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	int theStatus = 0;    
	SessionCache *sess = getSessionCache(theTaskDataPtr);
	
	setCacheInvalid(sess);

	if (RpHSGetErrorStatus(RpGetCurrentConnection(theTaskDataPtr), &theStatus) != eRpNoError) {
		addSessionMsg(sess, WEBUI_ERROR_MSG, "Internal error while uploading image file.");
		return;
	}
	
	if (theStatus == FW_SUCCESS) {
		addSessionMsg(sess, WEBUI_STATUS_MSG, "Image file uploaded successfully. You must reboot for changes to take effect.");
	} else {
		char errorMsg[MAX_SESSION_MSG];
		sprintf(errorMsg, "Error uploading image file (%d).", theStatus);
		addSessionMsg(sess, WEBUI_ERROR_MSG, errorMsg);
	}

	return;
}

extern void initFirmwareUpload(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void initFirmwareUpload(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {

    RpHSSetCurrentDirectory(RpGetCurrentConnection(theTaskDataPtr), FS_FLASH_VOLUME_ROOT_DIR);
    RpHSSetStorageLocation(RpGetCurrentConnection(theTaskDataPtr), NAFS_LOCATION_FIRMWARE);
	return;
}


#endif	/* RomPagerServer */
