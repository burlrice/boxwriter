/* Created with PageBuilder version 4.04 on Fri Jul 30 13:18:19 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ***************************************** */
/* *    Built from "css\stylesheet.css"    * */
/* ***************************************** */

extern rpObjectDescription Pgstylesheet;

static char Pgstylesheet_Item_1[] =
	"BODY\n"
	"{\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n"
	"BACKGROUND-COLOR: white;\n"
	"COLOR: black;\n"
	"}\n"
	"SELECT\n"
	"{\n"
	"FONT-SIZE: 11px;\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif\n"
	"}\n"
	"INPUT\n"
	"{\n"
	"FONT-SIZE: 11px;\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif\n"
	"}\n"
	"TABLE, TR, TD, P, BR {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n"
	"}\n"
	".PAGEHEADER {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 16px;\n"
	"FONT-WEIGHT: bold;\n"
	"COLOR: black;\n";

static char Pgstylesheet_Item_2[] =
	"BACKGROUND-COLOR: white;\n"
	"}\n"
	".CONTENTHEADER {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 16px;\n"
	"FONT-WEIGHT: bold;\n"
	"COLOR: white;\n"
	"BACKGROUND-COLOR: #00008B;\n"
	"PADDING: 10px;\n"
	"MARGIN: 0px;\n"
	"}\n"
	".PAGETAB {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n"
	"FONT-WEIGHT: normal;\n"
	"COLOR: #00008B;\n"
	"BACKGROUND-COLOR: #C5D6FC;\n"
	"BORDER-STYLE: solid;\n"
	"BORDER-COLOR: #C5D6FC;\n"
	"PADDING: 5px;\n"
	"}\n"
	".PAGETAB-SELECTED {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n";

static char Pgstylesheet_Item_3[] =
	"FONT-WEIGHT: bold;\n"
	"COLOR: #00008B;\n"
	"BACKGROUND-COLOR: #C5D6FC;\n"
	"BORDER-STYLE: solid;\n"
	"BORDER-COLOR: #C5D6FC;\n"
	"PADDING: 5px;\n"
	"}\n"
	".PAGECONTENT {\n"
	"BACKGROUND-COLOR: #E8E8E8;\n"
	"}\n"
	".COPYRIGHT {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 10px;\n"
	"}\n"
	".BORDERCOLOR {\n"
	"BACKGROUND-COLOR: #00008B;\n"
	"}\n"
	".BORDER {\n"
	"PADDING: 0px;\n"
	"BORDER: 2px solid #00008B;\n"
	"}\n"
	"HR.SEPARATORLINE {\n"
	"COLOR: #00008B;\n"
	"}\n"
	"A:LINK, A:VISITED, A:ACTIVE {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n";

static char Pgstylesheet_Item_4[] =
	"COLOR: #0033CC;\n"
	"TEXT-DECORATION: underline;\n"
	"}\n"
	"A:HOVER {\n"
	"FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;\n"
	"FONT-SIZE: 12px;\n"
	"COLOR: #00008B;\n"
	"TEXT-DECORATION: underline;\n"
	"}\n"
	".ERRORFIELD {\n"
	"FONT-WEIGHT: bold;\n"
	"BACKGROUND-COLOR: #FFDDDD;\n"
	"BORDER-COLOR: #E80000;\n"
	"BORDER-STYLE: solid;\n"
	"}\n"
	".STATUSBOX {\n"
	"FONT-SIZE: 12px;\n"
	"FONT-WEIGHT: bold;\n"
	"COLOR: #00008B;\n"
	"BACKGROUND-COLOR: #C5D6FC;\n"
	"BORDER-COLOR: #00008B;\n"
	"BORDER-STYLE: solid;\n"
	"BORDER-WIDTH: thin;\n"
	"}\n"
	".ERRORBOX {\n"
	"FONT-SIZE: 12px;\n"
	"FONT-WEIGHT: bold;\n";

static char Pgstylesheet_Item_5[] =
	"COLOR: #E80000;\n"
	"BACKGROUND-COLOR: #FFDDDD;\n"
	"BORDER-COLOR: #E80000;\n"
	"BORDER-STYLE: solid;\n"
	"BORDER-WIDTH: thin;\n"
	"}\n"
	".ACTIVE {\n"
	"COLOR: #FFFFFF;\n"
	"BACKGROUND-COLOR: #228B22;\n"
	"}\n"
	".INACTIVE {\n"
	"COLOR: #FFFFFF;\n"
	"BACKGROUND-COLOR: #00008B;\n"
	"}\n";


static rpItem Pgstylesheet_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgstylesheet_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgstylesheet_Item_2 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgstylesheet_Item_3 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgstylesheet_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgstylesheet_Item_5 }, 
	{ eRpItemType_LastItemInList } 
};


rpObjectDescription Pgstylesheet = {
	"/stylesheet.css",
	Pgstylesheet_Items,
	(rpObjectExtensionPtr) 0,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeCss,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
