/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer

extern rpObjectDescription Pgindex;
extern rpObjectDescription Pgnetwork_config;
extern rpObjectDescription Pgnetwork_config_Form_1;
extern rpObjectDescription Pgwireless_network_config;
extern rpObjectDescription Pgwireless_network_config_Form_1;
extern rpObjectDescription Pgwireless_ip_config;
extern rpObjectDescription Pgwireless_ip_config_Form_1;
extern rpObjectDescription Pgupload_firmware;
extern rpObjectDescription Pgupload_firmware_Form_1;
extern rpObjectDescription Pgreboot;
extern rpObjectDescription Pgreboot_Form_1;
extern rpObjectDescription Pgreboot_status;
extern rpObjectDescription Pgfilelist;
extern rpObjectDescription Pgfilelist_Form_1;
extern rpObjectDescription Pgstylesheet;
extern rpObjectDescription Pglogo;
extern rpObjectDescription Pgerrhandler;
extern rpObjectDescription Pgutils;
extern rpObjectDescription Pgvalidation;

rpObjectDescriptionPtr gRpObjectList[] = {
	&Pgindex,
	&Pgnetwork_config,
	&Pgnetwork_config_Form_1,
	&Pgwireless_network_config,
	&Pgwireless_network_config_Form_1,
	&Pgwireless_ip_config,
	&Pgwireless_ip_config_Form_1,
	&Pgupload_firmware,
	&Pgupload_firmware_Form_1,
	&Pgreboot,
	&Pgreboot_Form_1,
	&Pgreboot_status,
	&Pgfilelist,
	&Pgfilelist_Form_1,
	&Pgstylesheet,
	&Pglogo,
	&Pgerrhandler,
	&Pgutils,
	&Pgvalidation,
	(rpObjectDescriptionPtr) 0
};

rpObjectDescPtrPtr gRpMasterObjectList[] = {
	(rpObjectDescriptionPtr *) gRpObjectList,
	(rpObjectDescPtrPtr) 0
};


#endif	/* RomPagerServer */
