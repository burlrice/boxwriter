#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AsExtern.h"
#include <bsp.h>
#include <boardParams.h>
#include "webmenu.h"

#if RomPagerServer


// External page references.
extern rpObjectDescription Pgindex;
#if BSP_WANT_ETHERNET
extern rpObjectDescription Pgnetwork_config;
#endif
#if BSP_WANT_WIRELESS
extern rpObjectDescription Pgwireless_network_config;
#endif
extern rpObjectDescription Pgfilelist;
extern rpObjectDescription Pgupload_firmware;
extern rpObjectDescription Pgreboot;


// Add entries to this table for menu items.
static html_menu_item menuItems[] = {
	{&Pgindex,						"Home"},
#if BSP_WANT_ETHERNET
	{&Pgnetwork_config,				"Network"},
#endif	
#if BSP_WANT_WIRELESS
	{&Pgwireless_network_config,	"Wireless"},
#endif	
	{&Pgfilelist,					"File Management"},
	{&Pgupload_firmware,			"Upload Firmware"},
	{&Pgreboot,						"Reboot"},
};



html_menu_item* getHtmlMenuItem(int index) {
	if (index >= 0 && index < (sizeof(menuItems)/sizeof(html_menu_item))) {
		return &menuItems[index];
	} else {
		return NULL;
	}
}


#endif	/* RomPagerServer */
