/* Created with PageBuilder version 4.04 on Mon Mar  6 11:18:39 2006 */

#include <stdio.h>
#include <stdlib.h>
#include <tx_api.h>
#include <bsp_api.h>
#include <gpiomux_def.h>
#include "AsExtern.h"
#include "webutil.h"

#if RomPagerServer

#define NOT_USED                0
#define DELAY_BEFORE_RESET      (2*NABspTicksPerSecond)

static TX_TIMER resetTimer;

static void reset(ULONG notUsed)
{
    NA_UNUSED_PARAM(notUsed);
    customizeReset();   /* reset*/
}


extern rpObjectDescription Pgreboot_status;

/* ************************************** */
/* *    Built from "html\reboot.htm"    * */
/* ************************************** */

extern void rebootSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
void rebootSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr) {
	RpSetNextPage(theTaskDataPtr, &Pgreboot_status);
    printf("Resetting system...");
	tx_timer_create (&resetTimer, "Reset Timer", reset, NOT_USED, DELAY_BEFORE_RESET, NOT_USED, TX_AUTO_ACTIVATE);
}


#endif	/* RomPagerServer */
