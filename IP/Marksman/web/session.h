#ifndef _SESSION_H
#define _SESSION_H

#include "AsExtern.h"

#ifdef __cplusplus
extern "C"
{
#endif


#define WEBUI_STATUS_MSG	1
#define WEBUI_ERROR_MSG		2

#define MAX_SESSION_MSG 255

typedef struct SessionMsg {
    Boolean isActive;
    unsigned char type;
    char msg[MAX_SESSION_MSG + 1];
} SessionMsg;

// largest cache size needed by any page or set of pages
#define MAX_PAGE_CACHE 2048 
#define CACHE_ID_TYPE rpObjectDescriptionPtr

typedef struct SessionCache {
    // free form id used only to validate cache state.  A page or set of pages share an
    // id to share the cache.  Typically a page just sets its page id  
    CACHE_ID_TYPE id; 
    SessionMsg msg;
    // page managed flag to indicate if cache contains readable data 
    Boolean isValid; 
    void *pageCache;
} SessionCache;

// Retrieve the session cache for the current session.  The input paramater is
// an rpObjectPtr, but is left void * here so that it matches what 
// functions will have available to them.
SessionCache * getSessionCache(void *theTaskDataPtr);

// add a session message.  If there was no room for the message
// false is returned
Boolean addSessionMsg(SessionCache *cache, unsigned char type, char * inMsg);

// Used by pages to add the usual "Changes saved.."
// status message. Just a little helper
// Returns same as addSessionMsg()
Boolean addStdSuccessMsg(SessionCache *cache);

// Used to retrieve messages for display.  Also clears active flag
// to indicate the msg has been displayed (therefore caller will 
// always see isActive FALSE).
// If there are no messages for display, NULL is returned
SessionMsg *readAndClearSessionMsg(SessionCache *cache);

// Same as readAndClearSessionMsg(), except the message is not 
// made inactive.
SessionMsg *getSessionMsg(SessionCache *cache);

// return true iff one more message cannot be added.
Boolean isSessionMsgFull(SessionCache *cache);

// returns the state of the page cache.  Valid means 
// the cache can be retrieved and valid data read (or written)
// invalid indicates the cache contains no valid data (but is 
// ready to be written to).  Id's must match, else FALSE is returned.  
Boolean isPageCacheValid(SessionCache *cache, CACHE_ID_TYPE id);
Boolean isPageCacheEmpty(SessionCache *cache);


// returns true iff the session cache id matches passed in ID.  Used 
// to determine if the session cache is set with data for this page.
Boolean isSessionCacheID(SessionCache *cache, CACHE_ID_TYPE id);
                                                                    
// Returns a pointer to the cache space for use by this page.  If
// the cache does not match the passed in id type, the current cache
// is set to this type and the page cache is marked invalid. 
// Messges are not affected.
// Callers must be careful to only use MAX_PAGE_CACHE bytes.
// Callers must be careful to only read from the cache 
// if isPageCacheValid() == TRUE
void * getPageCache(SessionCache *cache, CACHE_ID_TYPE id);
void setPageCache(SessionCache *sess, void *cache, CACHE_ID_TYPE id);

// Marks the cache invalid so that subsequent pages do not read from
// the cache.  This is the normal call that each page should make in
// their page pre-processing, unless they need to propagate the cache
// to another page, such as a wizard might
void setCacheInvalid(SessionCache *cache);

// This marks the cache valid so that the next page served that
// is of this page's type will read values from this cache.  Typically
// used by form submit and in stateful pages (like wizards).
void setCacheValid(SessionCache *cache);


#ifdef __cplusplus
}
#endif

#endif // _SESSION_H
