#ifndef FJ_SERIAL_H_
#define FJ_SERIAL_H_

#include "Npttypes.h"
#include "gpio.h"
#include "termios.h"

#define SERIAL_STACK_SIZE      (3*4096)

#define _8BIT_1STOPBIT_EVENPARITY_HW { (CREAD | CS8 | PARENB | CRTSCTS), (0) }
#define _8BIT_1STOPBIT_EVENPARITY_NO { (CREAD | CS8 | PARENB ), (0) }
#define _8BIT_1STOPBIT_EVENPARITY_SW { (CREAD | CS8 | PARENB ), (IXON | IXOFF) }
#define _8BIT_1STOPBIT_ODDPARITY_HW  { (CREAD | CS8 | PARENB | PARODD | CRTSCTS), (0) }
#define _8BIT_1STOPBIT_NOPARITY_HW   { (CREAD | CS8 | CRTSCTS), (0) }
#define _8BIT_1STOPBIT_NOPARITY_NO   { (CREAD | CS8), (0) }
#define _8BIT_2STOPBIT_EVENPARITY_HW { (CREAD | CS8 | CSTOPB | PARENB | CRTSCTS), (0) }
#define _8BIT_2STOPBIT_ODDPARITY_HW  { (CREAD | CS8 | CSTOPB | PARENB | PARODD | CRTSCTS), (0) }
#define _8BIT_2STOPBIT_NOPARITY_HW   { (CREAD | CS8 | CSTOPB | CRTSCTS), (0) }
#define _8BIT_2STOPBIT_EVENPARITY_SW { (CREAD | CS8 | CSTOPB | PARENB ), (IXON | IXOFF) }

struct ioflags_t {
    tcflag_t cflags;
    tcflag_t iflags;
};

void SerialFct (ULONG thread_input);
int WriteSerial (const unsigned char * lpsz, int nLen);
int ReadSerial (unsigned char * lpsz, int nLen);

extern TX_SEMAPHORE semaSerial;

#endif /*FJ_SERIAL_H_*/
