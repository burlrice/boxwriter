#include "fj.h"
#include <stdio.h>
#include "fj_misc.h"
#include "fj_system.h"

// the RT clock will not use DST.
// the fj_mktime function will be used in place of the standard mktime function.
// fj_mktime will use the C library version of mktime.
// if mktime adjusts for DST and DST is not desired,
// fj_mktime will undo the DST.

FJDLLExport time_t fj_mktime( struct tm *pTM )
{
#ifdef NET_OS
	CLPCFJSYSTEM pfsys = fj_CVT.pfsys;	// this routine is called a few times before pfsys is set up
#else
	CLPCFJSYSTEM pfsys = NULL;			// just to get through first compile
#endif
	time_t tRet;
	BOOL   bDSTUSA = FALSE;				// set default if pfsys is NULL

	if ( NULL != pfsys )
	{
		if ( FALSE == pfsys->bInternalClock )
			bDSTUSA = pfsys->bDSTUSA;	// set value if pfsys is not NULL
	}

	// mktime in this C library will automatically detect USA DST, set the dst flag and adjust the time.
	// mktime assumes the time is standard - it ignores the tm_isdst flag.
	// I have found no setting to bypass this.
	// so it must be done with explicit code here.
	if ( 0 != pTM->tm_isdst )			// if dst time,
	{
		pTM->tm_hour--;					// take the hour off
		pTM->tm_isdst = 0;
	}
	// pass standard time to mktime()
	tRet = mktime( pTM );
	// if USA DST is not wanted, it must be compensated for.
	if ( FALSE == bDSTUSA )				// if not USA DST,
	{
		if ( 0 != pTM->tm_isdst )		// and mktime set it,
		{
			pTM->tm_hour--;				// take the hour off,
			mktime( pTM );				// and re-do it.
			pTM->tm_isdst = 0;
		}
	}
	return ( tRet );
}
//
// convert FLOAT from inches to proper units
// input is in inches. everything in ip is in inches.
// if lUnits is negative, convert to inches
//
FJDLLExport FLOAT fj_FloatUnits( FLOAT fInput, LONG lUnitsIn )
{
	double dm, dd, dr;
	LONG lUnits;

	lUnits = abs(lUnitsIn);
	if      ( FJSYS_UNITS_ENGLISH_INCHES == lUnits ){ return(fInput); }
	else if ( FJSYS_UNITS_ENGLISH_FEET   == lUnits ){ dm = 1.0;    dd = 12.0; }
	else if ( FJSYS_UNITS_METRIC_MM      == lUnits ){ dm = 1000.0; dd = 39.37007874015748; }
	else if ( FJSYS_UNITS_METRIC_CM      == lUnits ){ dm = 100.0;  dd = 39.37007874015748; }
	else if ( FJSYS_UNITS_METRIC_METERS  == lUnits ){ dm = 1.0;    dd = 39.37007874015748; }
	if ( 0 < lUnitsIn ) dr = (double)fInput * dm / dd;
	else                dr = (double)fInput * dd / dm;
	return( (FLOAT)dr );
}
//
// convert DOUBLE to string and strip trailing zeros
//
FJDLLExport VOID fj_DoubleToStr( LPSTR pStr, double dInput)
{
	CHAR   spec[8] = "%.6f";
	double dRound;
	static int    max = 6;				// max number of decimal digits
	static int    min = 1;				// min number of decimal digits - location for rounding
	static int    shift = 0;			// divisor to 'shift' fixed number down to max decimal digit
	static int    limit;				// rounding limit
	int    limitw;						// rounding limit - working copy that changes
	int    index;						// index into string of second decimal deigit - first for rounding
	int    test;						// amount to be tested
	int    delta;						// difference to 'round' target
	int    len;
	int    i;
	int    j;

	if ( 0 == shift )
	{
		shift = 1; for ( j = 0 ; j < max;       j++ ) { shift *= 10; }
		limit = 1; for ( j = 0 ; j < (max-min); j++ ) { limit *= 10; }
	}
	dRound = 0.0;
	limitw = limit;
	spec[2] = '0' + max;
	sprintf( pStr, spec, dInput);
	len = strlen(pStr);
	for ( i = max; i > 2; i-- )			// start at minimum number of decimals; stop with last 2
	{
		index = len - i + min;			// index to first digit to look at
		test = atoi(pStr+index);		// convert possible round 'value' to decimal integer
		delta = limitw - test;			// get difference to target
		if ( delta < limitw/100 )		// within 1% of max?
		{
			dRound = delta;				// make floating point
										// 'shift' it down to correct decimal position
			dRound = dRound / (double)shift;
			break;
		}
		else if ( test < limitw/100 )	// within 1% of 0?
		{
			dRound = -test;				// make floating point
										// 'shift' it down to correct decimal position
			dRound = dRound / (double)shift;
			break;
		}
		limitw /= 10;
	}

	spec[2] = '0' + max;
	sprintf( pStr, spec, dInput+dRound);// now we should have a nice round number
	len = strlen(pStr) - 1;

	for ( i = (max-min); i > 0; i-- )	// strip off trailing zeros
	{
		if ( '0' == pStr[len] )
		{
			pStr[len] = 0;
			len--;
		}
	}
	return;
}
//
// convert LONG to string
//
FJDLLExport VOID fj_LongToStr( LPSTR pStr, LONG lInput)
{
	sprintf( pStr, "%d", lInput);
	return;
}
//
// convert BOOL to string - "T" or "F"
//
FJDLLExport VOID fj_BoolToStr( LPSTR pStr, BOOL bInput)
{
	*pStr = (bInput == TRUE ? 'T' : 'F');
	pStr++;
	*pStr = 0;
	return;
}
//
// convert ULONG ip address to string - "nnn.nnn.nnn.nnn"
//
FJDLLExport VOID fj_IPToStr( ULONG lInput, LPSTR pStr )
{
	sprintf( pStr, "%u.%u.%u.%u", PUSH_IPADDR(lInput) );
	return;
}
//
// convert string "nnn.nnn.nnn.nnn" to ip adress
//
FJDLLExport ULONG fj_StrToIP( LPSTR addrp )
{
	unsigned long l;
	unsigned int val;
	unsigned char c;
	int i;
	l = 0;
	for (i = 0; i < 4; i++)
	{
		l <<= 8;
		c = *addrp;
		if (c == '\0')
		{
			return 0;
		}
		else
		{
			val = 0;
			while (c >= '0' && c <= '9')
			{
				val *= 10;
				val += c - '0';
				addrp++;
				c = *addrp;
			}
			if (val > 255)
			{
				return 0;
			}
			l |= val;
			if (c != '\0' && c != '.')
			{
				return 0;
			}
			addrp++;
		}
	}
	return ( l );
}
//
//  FUNCTION: fj_SetParameter()
//
//  PURPOSE:  set one parameter from a string value
//
// return true if keyword processed
//
FJDLLExport BOOL fj_SetParameter( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pKey, LPSTR pValue )
{
	struct fjtable *pParm;
	int   j;
	BOOL  bRet;

	bRet = FALSE;

	pParm = &(pTable[lTableFirst]);
	for ( j = lTableFirst; j <= lTableLast; j++, pParm++ )
	{
		if ( 0 == strcmp(pKey, pParm->pStr) )
		{
			//{ char sz [1024]; sprintf (sz, "[%d] [%d, %d] %s: %s", j, lTableFirst, lTableLast, pKey, pValue); TRACEF (sz); }
			if ( NULL != pParm->pSet) pParm->pSet( pStruct, pValue );
			bRet = TRUE;
			break;
		}
	}
	return( bRet );
}
//
//  FUNCTION: fj_MakeParameterString()
//
//  PURPOSE:  make a string with print head parameters
//
FJDLLExport VOID fj_MakeParameterString( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pString )
{
	struct fjtable *pParm;
	LPSTR pCat;
	int   j;

	pCat = pString;
	pParm = &(pTable[lTableFirst]);

	for ( j = lTableFirst; j <= lTableLast; j++, pParm++ )
	{
		strcpy( pCat, pParm->pStr );
		pCat = pCat + strlen(pCat);
		*(pCat++) = '=';
		pParm->pGet( pStruct, pCat );
		pCat = pCat + strlen(pCat);
		*(pCat++) = ';';
		*(pCat  ) = 0;
	}
	return;
}
//
//  FUNCTION: fj_FindEndQuotedString()
//
//  PURPOSE:  Find the end of a double-quoted string.
//            First character is checked for a double-quote; if not double-quote, NULL is returned.
//
//  RETURN:   Pointer to the closing double-quote
//
FJDLLExport LPSTR fj_FindEndQuotedString( LPSTR pStr )
{
	LPSTR pRet = NULL;
	BOOL bQuote = FALSE;
	BOOL bSlash = FALSE;
	CHAR ch;

	if ( '"' == *pStr )
	{
		while ( 0 != (ch = *pStr) )
		{
			if ( '/' == ch )
			{
				if ( TRUE == bQuote ) bSlash = !bSlash;
			}
			if ( '"' == ch )
			{
				if ( TRUE == bSlash ) bSlash = FALSE;
				else
				{
					if ( FALSE == bQuote ) bQuote = TRUE;
					else
					{
						pRet = pStr;
						break;
					}
				}
			}
			pStr++;
		}
	}

	return( pRet );
}
//
//  FUNCTION: fj_ParseKeywordsFromStr()
//
//  PURPOSE:  Return pointers to parameters in string..
//            Parameters are delineated with the specified character.
//            The delimiter characters are replaced with 0. All the parameters become little strings.
//            A {} Curly brace at the front and end of the input string are stripped.
//            A delimiter inside a pair of double quotes is ignored.
//
FJDLLExport LONG fj_ParseKeywordsFromStr( LPSTR pStr, CHAR chDelim, LPSTR *pList, LONG lListSize )
{
	LPSTR pEndQuote;
	CHAR  ch;
	int   i;
	LONG  lCount;

	lCount = 0;
	if ( (NULL != pStr) && (NULL != pList) && (0 < lListSize) )
	{
		for ( i = 0; i < lListSize; i++ ) *(pList+i) = 0;
		i = strlen(pStr)-1;
		if ( ('{' == *pStr) && ('}' == *(pStr+i)) )
		{
			*(pStr+i) = '\0';
			pStr++;
		}
		if ( 0 != *pStr )
		{
			*pList = pStr;
			lCount = 1;
			while ( 0 != (ch = *pStr) )
			{
				pEndQuote = NULL;
				if ( '"' == ch )
				{
					pEndQuote = fj_FindEndQuotedString( pStr );
				}
				if ( NULL != pEndQuote )
				{
					pStr = pEndQuote;
				}
				else
				{
					if ( chDelim == ch )
					{
						*(pStr) = 0;	// change delimeter to 0
										// stop before adding new pointer into list
						if ( lCount < lListSize )
						{
							*(pList+lCount) = pStr+1;
							lCount++;
						}
						else
						{
							break;
						}
					}
				}
				pStr++;
			}
		}
	}
	return( lCount );
}
//
//  FUNCTION: fj_processParameterStringInput()
//
//  PURPOSE:  process special characters in a parameter string
//
//  NOTE: If a parameter string starts with a double quote("),
//                strip the double quotes off both ends.
//                convert any slash-double quote(/") to a double quote.
//                convert any slash-slash(//) to a single slash.
//
FJDLLExport VOID fj_processParameterStringInput( LPSTR pInput )
{
	LPSTR pOutput;
	CHAR  c;
	CHAR  cNext;

	if ( (NULL != pInput) && ('"' == *pInput) )
	{
		pOutput = pInput;
		pInput++;
		cNext = *pInput;
		while ( 0 != cNext )
		{
			c = cNext;
			pInput++;
			cNext = *pInput;
			if ( (('/' == c) && (('"' == cNext) ||  ('/' == cNext))) ||
				((c == '\\') && (cNext == '{' || cNext == '}'))) // burl
			{
				*pOutput++ = cNext;
				pInput++;
				cNext = *pInput;
			}
			else
			{
				*pOutput++ = c;
			}
		}
		pOutput--;
		if ( '"' == *pOutput ) *pOutput = 0;
	}
}
//
//  FUNCTION: fj_processParameterStringOutput()
//
//  PURPOSE:  process special characters in a parameter string
//
//  NOTE: Wrap a string in double quotes(")If a parameter string contains a double quote("),
//                Any double quote or slash(/) in the string will have a slash added in front of it.
//
FJDLLExport VOID fj_processParameterStringOutput( LPSTR pOutput, LPCSTR pInput )
{
	CHAR  c;

	if ( NULL != pOutput )
	{
		if ( (NULL != pInput) && (0 != *pInput) )
		{
			*pOutput++ = '"';
			c = *pInput;
			while ( 0 != c )
			{
				if ( ('"' == c) || ('/' == c) )
				{
					*pOutput++ = '/';
				}
				else if	(c == '{' || c == '}') // burl
				{
					*pOutput++ = '\\';
				}
				*pOutput++ = c;
				pInput++;
				c = *pInput;
			}
			*pOutput++ = '"';
		}
		*pOutput = 0;
	}
}
//
//  FUNCTION: fj_SetParametersFromString()
//
//  PURPOSE:  parse a string of parameters
//
// return true if all parameters processed
//
FJDLLExport BOOL fj_SetParametersFromString( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pString )
{
	LPSTR pList[140];
	LPSTR pList2[2];
	LONG lCount;
	LONG lCount2;
	int  j;
	BOOL bRet;

	bRet = TRUE;

	//60 );
	lCount = fj_ParseKeywordsFromStr( pString, ';', pList, 140 );
	for ( j = 0; j < lCount; j++ )
	{
		// break keyword=value up into 2 strings
		lCount2 = fj_ParseKeywordsFromStr( pList[j], '=', pList2, 2 );
		if ( 2 == lCount2 )
		{
			bRet &= fj_SetParameter( pTable, lTableFirst, lTableLast, pStruct, pList2[0], pList2[1] );
		}
		else if ( 0 != lCount2 )
		{
			bRet = FALSE;
		}
	}
	return( bRet );
}
