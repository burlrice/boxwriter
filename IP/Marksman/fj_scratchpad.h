#ifndef FJ_SCRATCHPAD_H_
#define FJ_SCRATCHPAD_H_

#include "fj_defines.h"

void fj_scratchpadWriteCount (long lCount);
long fj_scratchpadReadCount ();

void fj_scratchpadWriteLabelName (const char * lpsz);
void fj_scratchpadReadLabelName (char * lpsz, int nLen);

void fj_scratchpadWriteInkUsage (double dInk);
double fj_scratchpadReadInkUsage ();

void fj_scratchpadWritePrintState (BOOL bPrintIdle);
BOOL fj_scratchpadReadPrintState ();

#endif /*FJ_SCRATCHPAD_H_*/
