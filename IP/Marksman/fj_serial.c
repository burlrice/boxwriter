
#include <stdio.h>
#include <string.h>
#include <netosIo.h>
#include <serParam.h>
#include <tx_api.h>
#include "utils.h"
#include "appconf.h"
#include "sockapi.h" /* for getErrno */
#include "ind_io.h"
#include "netosIo.h"
#include "termios.h"
#include "fj.h"
#include "fj_serial.h"
#include <ncc_init.h>

#define BUFFERSIZE 445000
#define SERIALTIMEOUT 100
#define COM0           "/com/1"
#define COM1           "/com/3"

TX_SEMAPHORE semaSerial;
BOOL bSerialFct = FALSE;

extern TX_THREAD thrSerial;

static BOOL bSerialDataReceived = FALSE;
static char rcvBuffer[BUFFERSIZE];
static char sndBuffer[BUFFERSIZE];
static int numBytesRcvd = 0;
static int numBytesSend = 0;

static char dbgBuffer[BUFFERSIZE];

void OutputDebugString (const char * lpsz, const char * lpszFile, ULONG lLine)
{
    const BOOL bDebug = are_we_in_debugger ();
    
    if (bDebug) {
		sprintf (dbgBuffer, "%s%s(%d): %s\r\n", bDebug ? "[JTAG]: " : "", lpszFile, lLine, lpsz);
		//fj_socketSendAll (IPC_STATUS, (LPBYTE)dbgBuffer, strlen (dbgBuffer)); 
		WriteSerial (dbgBuffer, strlen (dbgBuffer));
    }
}

int ReadSerial (unsigned char * lpsz, int nLen)
{
	int nResult = 0;
	
	if (bSerialDataReceived) {
		if (tx_semaphore_get (&semaSerial, NS_MILLISECONDS_TO_TICKS (SERIALTIMEOUT)) == TX_SUCCESS) {
			nResult = min (numBytesRcvd, min (nLen, BUFFERSIZE));
			
			bSerialDataReceived = FALSE;
			memcpy (lpsz, rcvBuffer, nResult);
			lpsz [nLen] = 0;
			numBytesRcvd = 0;
			tx_semaphore_put (&semaSerial);
		}
	}
	
	return nResult;
}

int WriteSerial (const unsigned char * lpsz, int nLen)
{
	int nResult = 0;

	if (bSerialFct) {
		if (tx_semaphore_get (&semaSerial, NS_MILLISECONDS_TO_TICKS (SERIALTIMEOUT)) == TX_SUCCESS) {
			memcpy (&sndBuffer [numBytesSend], lpsz, min (nLen, BUFFERSIZE));
			numBytesSend += nLen;
			tx_semaphore_put (&semaSerial);
			tx_thread_resume (&thrSerial);
			nResult = nLen - numBytesSend;
		}
	}
	
	return nResult;
}

static int OpenSerial ()
{
    int result, baudrate = fj_CVT.pfph->lBaudRate;
    struct ioflags_t iOptions = { _8BIT_1STOPBIT_NOPARITY_NO };
    struct termios tios;
    int fd = open (COM1, O_RDWR | O_NONBLOCK);

    if (fd < 0) {
        printf ("Can't open %s [%d].\n", COM1, getErrno());
        return -1;
    }
    
    tcgetattr(fd, &tios);
    cfsetospeed(&tios, baudrate);

    tios.c_cflag = iOptions.cflags;
    tios.c_iflag = iOptions.iflags;

    result = tcsetattr( fd, TCSANOW, &tios );
    
    if (result < 0) {
        printf ("ioctl(Set attributes) failure: errno[%d]\n",  getErrno());
        return -1;
    }
    if (cfgetospeed(&tios) != baudrate) {
        printf ("ioctl(Baud set) errno[%d]\n", getErrno());
        return -1;
    }
    if ((tios.c_cflag != iOptions.cflags) || (tios.c_iflag != iOptions.iflags)) {
        printf("ioctl(Flag settings) failure errno[%d]\n", getErrno());
        return -1;
    }
    
    return fd;
}

void SerialFct (ULONG thread_input)
{
	int fd = -1;
	int baudrate = fj_CVT.pfph->lBaudRate;
	TX_THREAD * pMain = (TX_THREAD *)thread_input;
	TX_THREAD * pThis = tx_thread_identify ();
	
    tx_semaphore_get(&semaSerial, TX_WAIT_FOREVER);
    memset (rcvBuffer, 0, ARRAYSIZE (rcvBuffer));
    memset (sndBuffer, 0, ARRAYSIZE (sndBuffer));
    bSerialFct = TRUE;
	fd = OpenSerial ();
	tx_semaphore_put (&semaSerial);
		
	while (bSerialFct) {
		tx_semaphore_get (&semaSerial, TX_WAIT_FOREVER);

		if (baudrate != fj_CVT.pfph->lBaudRate) {
			close (fd);
			fd = OpenSerial ();
			baudrate = fj_CVT.pfph->lBaudRate;
		}

		if (fd > 0) {
			if (numBytesSend) { // write
				int nSent = 0;
				
				while (numBytesSend > 0) {
					int nWrite = write (fd, (sndBuffer + nSent), numBytesSend);
					
					if (nWrite > 0) 
						numBytesSend -= nWrite;
				}
			}
			else {
				if (!bSerialDataReceived) { // read
				    int nRead = read (fd, (rcvBuffer + numBytesRcvd), (BUFFERSIZE - numBytesRcvd));
			        
			        if (nRead > 0) {
			        	int i;
		
			        	numBytesRcvd += nRead;
			        	
			        	for (i = 0; i < nRead; i++) {
			        		int nIndex = numBytesRcvd + i - nRead;
			        		char c = rcvBuffer [nIndex];
			        		
			        		if ((c == 0) || (c == 0x0A) || (c == 0x0D)) {
								rcvBuffer [nIndex] = 0;
								bSerialDataReceived = TRUE;
			        		}
			        	}
			        }
				}
			}
		}
		
		tx_semaphore_put (&semaSerial);
		
		//tx_thread_resume (pMain);		
		tx_thread_suspend (pThis);		
	}
	
	bSerialFct = FALSE;
	close (fd);
}
