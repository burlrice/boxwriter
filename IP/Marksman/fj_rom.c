/*
 *            Copyright (c) 2000 by FoxJet Inc.
 *
 */

#include "fj.h"
//#include "fj_memstorage.h"
#include "fj_mem.h"
#include "fj_netsilicon.h"
#include "fj_printhead.h"
#include "fj_defines.h"
#include "fj_file.h"
#include "fj_rom.h"
#include <bsp_sys.h>
//#include <ncc_io.h>
#include <narmled.h>
#include <netoserr.h>

/*
//
// fj_writeROM in fj_writeROM.c does the actual write into the hardware ROM sectors.
//      It calculates and sets the ROM checksum.
//

WORD32 fj_checksumRomEntry( LPCROMENTRY pRomEntry, CLPCBYTE pBuffer )
{
	WORD32 *pCheckSum;
	WORD32  wCheckSum;
	LONG    lLength;
	int     i;

	if ( (int)pRomEntry < 100 )
	{
		pRomEntry = *(fj_CVT.pfj_RomTable) + (int)pRomEntry;
	}

	if ( NULL == pBuffer )
	{
		pCheckSum = (WORD32 *)(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset);
	}
	else
	{
		pCheckSum = (WORD32 *)(pBuffer);
	}
	lLength   = pRomEntry->lMaxLength - 4;
	wCheckSum = 0;
	for ( i = 0; i < lLength; i += 4, pCheckSum++ )
	{
		wCheckSum ^= *pCheckSum;
	}

	return( 0xffffffff - wCheckSum );
}
*/

int fj_setDefaultFJRomParams( LPFJ_PARAM pFJ_PARAM )
{
	int iRet;

	if (pFJ_PARAM == NULL)
	{
		iRet = -1;
	}
	else
	{
		fj_SystemInit( &(pFJ_PARAM->FJSys) );
		pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
		pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
		pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);
		fj_PrintHeadInit( &(pFJ_PARAM->FJPh) );
		pFJ_PARAM->FJPh.pfsys = &(pFJ_PARAM->FJSys);
		iRet = 0;
	}
	return( iRet );
}
/*
 *
 *  Function: int fj_ReadFJRomParams (LPFJ_PARAM pFJ_PARAM)
 *
 *  Description:
 *
 *      This routine reads the contents of the section of ROM that contains
 *      the FoxJet startup parameters.
 *
 *  Parameters:
 *
 *      pFJ_PARAM          pointer to buffer to read parameters into
 *
 *  Return Values:
 *
 *      0       success
 *      -1      unable to read parameters. default parameters set.
 */
#define MAXFILELEN 0x0FFF

int fj_ReadFJRomParams( LPFJ_PARAM pFJ_PARAM )
{
	//struct tm tm;

	//fj_print_head_GetRTClockTMRegisters( &tm );
	char * lpszSystem [] = 
	{
		"System_Syst.txt", 
		"System_Time.txt", 
		"System_Date.txt", 
		"System_Inet.txt", 
		"System_Pass.txt", 
		"System_BarC.txt", 
	};
	char * lpszGroup [] = 
	{
		"Group.txt", 
	};
	char * lpszPrinter [] = 
	{
		"Printer_Prt.txt", 
		"Printer_Pkg.txt", 
		"Printer_Phy.txt", 
		"Printer_Ope.txt", 
		"Printer_Edt.txt", 
	};
	LPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	char szFile [FJSYS_MAX_FILENAME];
	int i, ret = -1;
	BOOL bDebug = FALSE;

	//bDebug = TRUE;
	
	if (fj_CVT.pFJglobal->semaROMLock.tx_semaphore_count != 0) {
		fj_writeInfo("ROM read FoxJet params - semaphore not locked\n" );
		return -1;
	}
	
	char * sz = fj_malloc (MAXFILELEN); //char sz [MAXFILELEN];
	
	if (pFJ_PARAM != NULL) {
		fj_SystemInit (&(pFJ_PARAM->FJSys));
		pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
		pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
		pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);
		fj_PrintHeadInit (&(pFJ_PARAM->FJPh));
		pFJ_PARAM->FJPh.pfsys = &(pFJ_PARAM->FJSys);

		for (i = 0; i < ARRAYSIZE (lpszSystem); i++) {
			int nLen = MAXFILELEN;
			sprintf (szFile, "%s/%s", FJ_DIR_PARAMS, lpszSystem [i]);
			memset (sz, 0, nLen);
			
			if (bDebug) { printf ("%s(%d): %s: ", __FILE__, __LINE__, szFile); }
			
			if (ReadFile (&nLen, sz, szFile)) {
				if (bDebug) { printf (sz); printf ("\n"); }
				BOOL bFromString = fj_SystemFromString( &(pFJ_PARAM->FJSys), sz);

				ret = 0;
				
				if (!bFromString) 
					sprintf ("(%s)%d: fj_SystemFromString failed: %s: %s\n", __FILE__, __LINE__, szFile, sz);
			}
		}
		
		for (i = 0; i < ARRAYSIZE (lpszGroup); i++) {
			int nLen = MAXFILELEN;
			sprintf (szFile, "%s/%s", FJ_DIR_PARAMS, lpszGroup [i]);
			memset (sz, 0, nLen);
			
			if (bDebug) { printf ("%s(%d): %s: ", __FILE__, __LINE__, szFile); }

			if (ReadFile (&nLen, sz, szFile)) {
				if (bDebug) { printf (sz); printf ("\n"); }
				BOOL bFromString = fj_SystemGroupFromString( &(pFJ_PARAM->FJSys), sz);

				ret = 0;

				if (!bFromString) 
					sprintf ("(%s)%d: fj_SystemGroupFromString failed: %s: %s\n", __FILE__, __LINE__, szFile, sz);
			}
		}

		for (i = 0; i < ARRAYSIZE (lpszPrinter); i++) {
			int nLen = MAXFILELEN;
			sprintf (szFile, "%s/%s", FJ_DIR_PARAMS, lpszPrinter [i]);
			memset (sz, 0, nLen);
			
			if (bDebug) { printf ("%s(%d): %s: ", __FILE__, __LINE__, szFile); }
			
			if (ReadFile (&nLen, sz, szFile)) {
				if (bDebug) { printf (sz); printf ("\n"); }

				BOOL bFromString = fj_PrintHeadFromString( &(pFJ_PARAM->FJPh), sz);

				ret = 0;

				if (!bFromString) 
					sprintf ("(%s)%d: fj_PrintHeadFromString failed: %s: %s\n", __FILE__, __LINE__, szFile, sz);
			}
		}

		{
			int nLen = sizeof (fj_CVT.pfph->lBaudRate);
			ULONG lBaudRate = 9600;

			sprintf (szFile, "%s/serial.txt", FJ_DIR_PARAMS);
			//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
			ReadFile (&nLen, &lBaudRate, szFile);
			fj_CVT.pfph->lBaudRate = pFJ_PARAM->FJPh.lBaudRate = lBaudRate;
		}
		/* 
		if (ret != 0) 
			fj_setDefaultFJRomParams (pFJ_PARAM);

		pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
		pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
		pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);
		*/
		
		// check SE params
//		LPFJSYSTEM pfsys = &(pFJ_PARAM->FJSys);
//		pfsys->fEncoder = pfsys->fEncoderWheel * 2 / pfsys->lEncoderDivisor;
	}

	/*
	{
		char szDyn [FJMESSAGE_DYNAMIC_SEGMENTS] [FJTEXT_SIZE+1] = { 0 };
		int nLen = FJMESSAGE_DYNAMIC_SEGMENTS * (FJTEXT_SIZE + 1);
		
		sprintf (szFile, "%s/DynamicSegments.txt", FJ_DIR_PARAMS);
		//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);

		if (ReadFile (&nLen, szDyn, szFile)) {
			for (i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
				strncpy (pGlobal->DynamicSegments [i].strDynData, szDyn [i], FJTEXT_SIZE + 1);
				//printf ("%s(%d): [%d] %s\n", __FILE__, __LINE__, i, pGlobal->DynamicSegments [i].strDynData);
			}
		}
	}
	*/
	
	fj_free (sz);
	//pCVT->fj_print_head_SetRTClockTM( &tm );
	
	return( ret );
}
/*
 *
 *  Function: VOID fj_WriteFJRomParams (LPFJ_PARAM pFJ_PARAM)
 *
 *  Description:
 *
 *      This function writes FoxJet parameters to ROM.
 *
 *  Parameters:
 *
 *      pFJ_PARAM      pointer to structure with parameters
 *
 *
 */

VOID fj_WriteFJRomParams( LPFJ_PARAM pFJ_PARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue )
{
	LPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	char * sz = fj_malloc (MAXFILELEN); //char sz [MAXFILELEN];
	//char sz [MAXFILELEN];
	char szFile [FJSYS_MAX_FILENAME];
	int nWrite = 0;
	int i;
	
	sprintf (szFile, "%s", FJ_DIR_PARAMS);
	mkdir (szFile, 0);
	
	//////////////////////////////////////////////////////////////////////
	memset (sz, 0, MAXFILELEN);
	fj_SystemToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_Syst.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);
	
	memset (sz, 0, MAXFILELEN);
	fj_SystemRomTimeToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_Time.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_SystemDateStringsToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_Date.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_SystemInternetToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_Inet.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_SystemPasswordToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_Pass.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_SystemBarCodesToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/System_BarC.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);
	
	//////////////////////////////////////////////////////////////////////
	memset (sz, 0, MAXFILELEN);
	fj_SystemGroupToString( &(pFJ_PARAM->FJSys), sz);
	sprintf (szFile, "%s/Group.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);
	
	//////////////////////////////////////////////////////////////////////
	memset (sz, 0, MAXFILELEN);
	fj_PrintHeadToString( &(pFJ_PARAM->FJPh), sz);
	sprintf (szFile, "%s/Printer_Prt.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_PrintHeadPackageToString( &(pFJ_PARAM->FJPh), sz);
	sprintf (szFile, "%s/Printer_Pkg.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_PrintHeadPhysicalToString( &(pFJ_PARAM->FJPh), sz);
	sprintf (szFile, "%s/Printer_Phy.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_PrintHeadOperationalToString( &(pFJ_PARAM->FJPh), sz);
	sprintf (szFile, "%s/Printer_Ope.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);

	memset (sz, 0, MAXFILELEN);
	fj_PrintHeadGetEditStatus( &(pFJ_PARAM->FJPh), sz);
	sprintf (szFile, "%s/Printer_Edt.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (strlen (sz), sz, szFile);
	
	sprintf (szFile, "%s/serial.txt", FJ_DIR_PARAMS);
	//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
	nWrite += WriteFile (sizeof (fj_CVT.pfph->lBaudRate), &fj_CVT.pfph->lBaudRate, szFile);

	/*
	{
		char szDyn [FJMESSAGE_DYNAMIC_SEGMENTS] [FJTEXT_SIZE+1] = { 0 };
		int nLen = FJMESSAGE_DYNAMIC_SEGMENTS * (FJTEXT_SIZE + 1);
		
		for (i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
			strncpy (szDyn [i], pGlobal->DynamicSegments [i].strDynData, FJTEXT_SIZE + 1);
			//printf ("%s(%d): [%d] %s\n", __FILE__, __LINE__, i, pGlobal->DynamicSegments [i].strDynData);
		}
		
		sprintf (szFile, "%s/DynamicSegments.txt", FJ_DIR_PARAMS);
		//printf ("%s(%d): %s: %s\n", __FILE__, __LINE__, szFile, sz);
		nWrite += WriteFile (nLen, szDyn, szFile);
	}
	*/
	
	fj_free (sz);
}
/*
 *
 *  Function: int fj_ReadNETOSRomParams (PFJ_NETOSPARAM pFJ_NETOSPARAM)
 *
 *  Description:
 *
 *      This routine reads the contents of the section of ROM that contains
 *      the NETOS startup parameters.
 *
 *  Parameters:
 *
 *      pFJ_NETOSPARAM          pointer to buffer to read parameters into
 *
 *  Return Values:
 *
 *      0       success
 *      -1      unable to read parameters. default parameters set.
 */
/*
int fj_ReadNETOSRomParams( LPFJ_NETOSPARAM pFJ_NETOSPARAM )
{
	int nLen = sizeof(FJ_NETOSPARAM);
	char szFile [FJSYS_MAX_FILENAME];

	sprintf (szFile, "%s/NETOSRomParams.txt", FJ_DIR_PARAMS);

	if (!ReadFile (&nLen, pFJ_NETOSPARAM, szFile)) 
		fj_setDefaultFJRomParams (pFJ_NETOSPARAM);

}
*/
/*
 *
 *  Function: VOID fj_WriteNETOSRomParams (LPFJ_NETOSPARAM pFJ_NETOSPARAM)
 *
 *  Description:
 *
 *      This function writes NETOS parameters to ROM.
 *
 *  Parameters:
 *
 *      pFJ_NETOSPARAM      pointer to structure with parameters
 *
 *
 */

/*
VOID fj_WriteNETOSRomParams( VOID *pFJ_NETOSPARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue )
{
//	LPFJ_BRAM pBRAM = fj_CVT.pBRAM;
	//ROMQUEUE q;
	int iRet;
	char szFile [FJSYS_MAX_FILENAME];

	// call old routine, now modified, to compute checksum and move to RAM save area
	iRet = NAWriteDevBoardParams ((devBoardParamsType *)pFJ_NETOSPARAM);
// TODO //	memcpy(&(pBRAM->nvParamsBRAM), pFJ_NETOSPARAM, sizeof(devBoardParamsType));

	sprintf (szFile, "%s/NETOSRomParams.txt", FJ_DIR_PARAMS);
	
	if (!WriteFile (sizeof(FJ_NETOSPARAM), pFJ_NETOSPARAM, szFile))
		printf ("%s(%d): failed to write: %s\n", __FILE__, __LINE__, szFile);
}
*/
//
// convert 8 byte NETsilicon serial number to FJ string.
// strip leading zeros
//
VOID fj_NETtoFJSerial( LPCHAR pNET, LPSTR pFJ )
{
	int i, j;
	BOOL bAll = FALSE;

	pNET++;
	
	for (i = 0; i < 8; i++) {
		if (bAll || (pNET [i] != '0')) {
			* pFJ++ = pNET [i];
			bAll = TRUE;
		}
	}
	
	* pFJ++ = 0;
}
//
// convert FJ string to 8 byte NETsilicon serial number
// add leading zeros
//
VOID fj_FJtoNETSerial( LPSTR pFJ, LPCHAR pNET )
{
	int  i,j,l;

	l = 8 - strlen( pFJ );
	j=0;
	for ( i = 0; i < 8; i++ )
	{
		if ( i < l )
		{
			pNET[i] = '0';
		}
		else
		{
			pNET[i] = pFJ[j];
			j++;
		}
	}
}
VOID fj_threadRomStart(ULONG in)
{
/*
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	ROMQUEUE    q;
	LPCROMENTRY pRomEntry;
	//int  i;
	LONG lWritten;
	CHAR str[88];
	int  iRet;

	while( TRUE )
	{
		tx_queue_receive( &(pGlobal->qROM), &q, TX_WAIT_FOREVER );

		iRet = -1;
		pRomEntry = *(fj_CVT.pfj_RomTable) + q.lRomEntry;
		if ( NULL != pRomEntry )
		{
			if ( NULL != q.pBuffer )
			{
				if ( (int)pRomEntry < 100 )
				{
					pRomEntry = *(fj_CVT.pfj_RomTable) + (int)pRomEntry;
				}

				if ( NULL != pRomEntry )
				{
					strcpy( str, "ROM write started");
					if ( 0 != fj_CVT.pFJglobal->semaROMLock.tx_semaphore_count )
					{
						strcat( str, " - semaphore not locked\n" );
					}
					else
					{
						strcat( str, " - semaphore locked\n" );
					}
					if ( TRUE == pfph->bInfoROM )
					{
						fj_writeInfo(str);
					}
					lWritten = fj_writeROM( pRomEntry, q.pBuffer, q.lBufferSize );

										// flash written successfully??
					if ( lWritten < q.lBufferSize )
					{
						if ( TRUE == pfph->bInfoROM )
						{
							sprintf(str,"ROM not written correctly - %i bytes out of %i\n",(int)lWritten,(int)q.lBufferSize);
							fj_writeInfo(str);
						}
					}
					else
					{
						if ( TRUE == pfph->bInfoROM )
						{
							sprintf(str,"ROM written correctly - %i bytes\n",(int)q.lBufferSize);
							fj_writeInfo(str);
						}
						iRet = 0;
					}
				}
			}
		}
		if ( NULL != q.pCallBack )
		{
			q.pCallBack( q.lCallBackValue, iRet );
		}
		if ( 0 == iRet )
		{
										// if reboot needed,
			if( TRUE == pRomEntry->bReboot )
			{
				pGlobal->bReset = TRUE;	// stop the watchdog code to let the chip reset
				if ( TRUE == pfph->bInfoROM )
				{
					fj_writeInfo( "Chip Reset\n" );
				}
				fj_print_head_SetStatus();
				fj_print_head_SendStatus();

				printf ("%s(%d): TODO: fj_threadRomStart\n", __FILE__, __LINE__);
										// give final messages time to display
				tx_thread_sleep( 1*BSP_TICKS_PER_SECOND );
										// shortest watchdog timer interval
				(*NCC_GEN).scr.bits.swt = 0;
				while ( TRUE )			// blink LED until reset occurs
				{
					NALedBlinkRed(100,1);
				}
			}
			else
			{
				if ( 0 == strcmp(pRomEntry->strFileName,"ga.bin" ))
					pGlobal->bGAUpgrade = TRUE;
			}
			fj_print_head_SetStatus();
			fj_print_head_SendStatus();

										// if executable,
			if( TRUE == pRomEntry->bExecutable )
			{
				// declare address as a function pointer and link to it
				((void (*)())(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset))();
			}
		}
		if ( TRUE == q.bFreeBuffer )
		{
			fj_free( q.pBuffer );
		}
		if ( TRUE == q.bFreeLock )
		{
			if ( 0 == pGlobal->semaROMLock.tx_semaphore_count )
				tx_semaphore_put( &(pGlobal->semaROMLock) );
		}
	}
	return;
*/
}
//
// Create rom queue.
// Start rom thread.
//
VOID fj_romInit( VOID )
{
	/* TODO
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	UINT  iRet;
	LONG  lSize;
	VOID  *pBuffer;
	LPBYTE pStack;

	lSize = 8 * sizeof(ROMQUEUE);
	pBuffer = fj_memory_getOnce( lSize );
	iRet = tx_queue_create( &(fj_CVT.pFJglobal->qROM), "ROM_QUEUE", TX_8_ULONG, pBuffer, lSize );

	pStack = fj_memory_getStack(FJ_THREAD_ROM_STACK_SIZE);
	// create the thread to get/send external time.
										// control block for ROM thread
	iRet = tx_thread_create (&(pGlobal->thrROM),
		"ROM Thread",					// thread name
		fj_threadRomStart,				// entry function
		0,								// parameter
		pStack,							// start of stack
		FJ_THREAD_ROM_STACK_SIZE,		// size of stack
		FJ_THREAD_ROM_PRIORITY,			// priority
		FJ_THREAD_ROM_PRIORITY,			// preemption threshold
		1,								// time slice threshold
		TX_AUTO_START);					// start immediately

	if (iRet != TX_SUCCESS)				// if couldn't create thread
	{
		netosFatalError ("Unable to create ROM thread", 1, 1);
	}
*/
}

void fj_ReadDynData ()
{
	char szFile [FJSYS_MAX_FILENAME];
	int i;
	
	for (i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
		int nLen = sizeof (DYNSEGMENT);
		
		sprintf (szFile, "%s/DynData%d.txt", FJ_DIR_PARAMS, i);
		
		if (!ReadFile (&nLen, (char *)&fj_CVT.pFJglobal->DynamicSegments [i], szFile)) {
			/*
			char sz [FJSYS_MAX_FILENAME + 128];
			sprintf (sz, "Failed to read: %s", szFile);
			TRACEF (sz);
			*/
		}
	}
}

void fj_ReadSerialData ()
{
	char szFile [FJSYS_MAX_FILENAME];
	int i = fj_CVT.pfph->lSerialVarDataID;
	int nLen = sizeof (DYNSEGMENT);
	
	sprintf (szFile, "%s/DynData%d.txt", FJ_DIR_PARAMS, i);
	
	if (!ReadFile (&nLen, (char *)&fj_CVT.pFJglobal->DynamicSegments [i], szFile)) {
		/*
		char sz [FJSYS_MAX_FILENAME + 128];
		sprintf (sz, "Failed to read: %s", szFile);
		TRACEF (sz);
		*/
	}
}
