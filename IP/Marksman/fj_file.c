//#include <tx_api.h>
#include <fs_api.h>
//#include <fs.h>
#include <sysAccess.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <netosIo.h>
#include <stdio.h>
#include <time.h>

#include "fj_mem.h"
#include "fj_defines.h"

#define NAFS_SUCEEDED(rc) NAFS_succeded ((rc), __FILE__, __LINE__)

static BOOL NAFS_succeded (int rc, LPCSTR lpszFile, int nLine)
{
	switch (rc) {
	case NAFS_SUCCESS:	
		return TRUE;
	case NAFS_NULL_POINTER:						printf ("%s(%d): NAFS_NULL_POINTER\n", 					lpszFile, nLine);
	case NAFS_INVALID_DIR_PATH_LENGTH:			printf ("%s(%d): NAFS_INVALID_DIR_PATH_LENGTH\n", 		lpszFile, nLine);
	case NAFS_INVALID_FILE_NAME_LENGTH:			printf ("%s(%d): NAFS_INVALID_FILE_NAME_LENGTH\n", 		lpszFile, nLine);
	case NAFS_INVALID_PATH_NAME:				printf ("%s(%d): NAFS_INVALID_PATH_NAME\n", 			lpszFile, nLine);
	case NAFS_INVALID_VOLUME_NAME:				printf ("%s(%d): NAFS_INVALID_VOLUME_NAME\n", 			lpszFile, nLine);
	case NAFS_IO_REQUEST_CB_BUSY:				printf ("%s(%d): NAFS_IO_REQUEST_CB_BUSY\n", 			lpszFile, nLine);
	case NAFS_IO_REQUEST_CB_NOT_INITIALIZED:	printf ("%s(%d): NAFS_IO_REQUEST_CB_NOT_INITIALIZED\n", lpszFile, nLine);
	case NAFS_SHUTDOWN_MODE:					printf ("%s(%d): NAFS_SHUTDOWN_MODE\n", 				lpszFile, nLine);
	}
	
	return FALSE;
}

FJDLLExport ULONG ReadFileSize (char *file_name)
{
	FILE *file = fopen(file_name, "rb");
	
	if (file == NULL) 
		return 0;

	fseek(file, 0, SEEK_END);
	ULONG lSize = ftell(file);
	fclose(file);
	
	return lSize;
}

FJDLLExport BOOL ReadFile(int *file_length, char *file_data, char *file_name)
{
	size_t bytes_read = 0;
	FILE *file = NULL;
	
	// Open the file
	file = fopen(file_name, "rb");
	
	if (file == NULL) {
		//printf ("%s(%d): fopen (%s): NULL\n", __FILE__, __LINE__, file_name);
		return FALSE;
	}
	
	// Determine size of file	
	fseek(file, 0, SEEK_END);
	*file_length = ftell(file);
	fseek(file, 0, SEEK_SET);
	if (*file_length < 0) {
		printf ("%s(%d): file_length: %d\n", __FILE__, __LINE__, * file_length);
		fclose(file);
		return FALSE;
	}
	
	// Read data from the file.
	bytes_read = fread(file_data, sizeof(char), *file_length, file);
	
	fclose(file);
	
	if ((int)bytes_read != *file_length) {
		printf ("%s(%d): bytes_read [%d] != *file_length [%d]\n", __FILE__, __LINE__, bytes_read, * file_length);
		return FALSE;
	}
	
	return TRUE;
}

FJDLLExport BOOL WriteFile (int file_length, char *file_data, char *file_name)
{
	size_t bytes_written = 0;
	FILE *file = NULL;
	
	// Open the file
	file = fopen(file_name, "wb");
	
	if (file == NULL) {
		//printf ("%s(%d): fopen (%s): NULL\n", __FILE__, __LINE__, file_name);
		return FALSE;
	}

	//printf ("%s(%d): fwrite: %s [%d bytes]\n", __FILE__, __LINE__, file_name, file_length);	
	
	//bytes_written = fwrite(file_data, sizeof(char), file_length, file);
	{
		const int nBlockSize = 1024;
		int n = file_length;
		char * p = file_data;
		
		while (n > 0) {
			int nLen = (n < nBlockSize) ? n : nBlockSize;
			//printf ("%s(%d): fwrite: %p [%d] (%d/%d) %s\n", __FILE__, __LINE__, p, nLen, file_length - n, file_length, file_name);
			int nWrite = fwrite (p, sizeof(char), nLen, file);
			
			if (nWrite == 0)
				break;
				
			n -= nWrite;
			p += nWrite;
			bytes_written += nWrite;
		}
	}
	
	fclose(file);
	
	if ((int)bytes_written != file_length) {
		printf ("%s(%d): bytes_written [%d] != file_length [%d]\n", __FILE__, __LINE__, bytes_written ,file_length);
		return FALSE;
	}
	
	return TRUE;
}

FJDLLExport int GetFileNames (const char * lpszDir, const char * pKeyword, char * lpsz, int nLen)
{
	NAFS_IO_REQUEST_CB io;
	int rc;
	UINT i, nFiles = 0;
	char szDir [NAFS_FILE_NAME_SIZE + 1];
	int nTotal = 0;
	
	sprintf (szDir, "COUNT=99999;%s=", pKeyword);
	nTotal = strlen (szDir);
	
	strlen ("COUNT=NNNN;X=;") + strlen (pKeyword);
	
	strncpy (szDir, lpszDir, ARRAYSIZE (szDir));
	memset (&io, 0, sizeof (io));
	
	if (lpsz)
		memset (lpsz, 0, nLen);
	
	rc = NAFSinit_io_request_cb (&io, NULL, 0);

	if (rc == NAFS_NULL_POINTER) {
		printf ("%s(%d): NAFSinit_io_request_cb: NAFS_NULL_POINTER [%s]\n", __FILE__, __LINE__, szDir);
		return nTotal;
	}

	rc = NAFSdir_entry_count (szDir, NASYSACC_LEVEL_ROOT, &nFiles, &io);

	if (lpsz && nFiles == 0) 
		sprintf (lpsz, "COUNT=0;%s=;", pKeyword);
	
	if (!NAFS_SUCEEDED (rc)) 
		return nTotal;

	NAFS_DIR_ENTRY_INFO * pInfo = (NAFS_DIR_ENTRY_INFO *)fj_malloc (nFiles * sizeof (NAFS_DIR_ENTRY_INFO));
	memset (pInfo, 0, nFiles * sizeof (NAFS_DIR_ENTRY_INFO));

	rc = NAFSlist_dir (szDir, NASYSACC_LEVEL_ROOT, pInfo, nFiles * sizeof (NAFS_DIR_ENTRY_INFO), &io);
	
	if (!NAFS_SUCEEDED (rc)) 
		return nTotal;

	for (i = 0; i < nFiles; i++) 
		nTotal += strlen (pInfo [i].entry_name) + 1;

	if (lpsz && nLen > 0) {
		sprintf (lpsz, "COUNT=%d;%s=", nFiles, pKeyword);
		
		for (i = 0; i < nFiles; i++) {
			int nNeeded = strlen (lpsz) + strlen (pInfo [i].entry_name) + 1;

			if (nNeeded > nLen)
				break;

			strcat (lpsz, pInfo [i].entry_name);
			
			if (i < (nFiles - 1))
				strcat (lpsz, ",");
		}

		strcat (lpsz, ";");
	}

	fj_free (pInfo);

	return nTotal;
}

FJDLLExport BOOL DeleteFile (const char * lpszDir, const char * lpszFile)
{
	if (!strcmp (lpszFile, "*"))
		return DeleteAllFiles (lpszDir);

	{		
		char sz [NAFS_FILE_NAME_SIZE + 1];
		
		sprintf (sz, "%s/%s", lpszDir, lpszFile);
		
		FILE * fp = fopen (sz, "rb");

		if (fp)
			fclose (fp);
		else
			return TRUE;
	}
	
	BOOL bResult = FALSE;
	NAFS_IO_REQUEST_CB io;
	int rc;
	char szDir [NAFS_FILE_NAME_SIZE + 1];
	char szFile [NAFS_FILE_NAME_SIZE + 1];

	memset (&io, 0, sizeof (io));
	strncpy (szDir, lpszDir, ARRAYSIZE (szDir));
	strncpy (szFile, lpszFile, ARRAYSIZE (szFile));
	
	rc = NAFSinit_io_request_cb (&io, NULL, 0);

	if (rc == NAFS_NULL_POINTER) {
		printf ("%s(%d): NAFSinit_io_request_cb: NAFS_NULL_POINTER [%s]\n", __FILE__, __LINE__, szDir);
		return FALSE;
	}
	
	rc = NAFSdelete_file (szDir, szFile, NASYSACC_LEVEL_ROOT, &io);

	bResult = NAFS_SUCEEDED (rc);
	
	printf ("%s(%d): DeleteFile [%d]: %s/%s\n", __FILE__, __LINE__, bResult, szDir, szFile);

	return bResult;
}

FJDLLExport BOOL DeleteAllFiles (const char * lpszDir)
{
	NAFS_IO_REQUEST_CB io;
	int rc;
	UINT i, nDelete = 0, nFiles = 0;
	char szDir [NAFS_FILE_NAME_SIZE + 1];
	
	strncpy (szDir, lpszDir, ARRAYSIZE (szDir));
	memset (&io, 0, sizeof (io));
	
	rc = NAFSinit_io_request_cb (&io, NULL, 0);

	if (rc == NAFS_NULL_POINTER) {
		printf ("%s(%d): NAFSinit_io_request_cb: NAFS_NULL_POINTER [%s]\n", __FILE__, __LINE__, szDir);
		return FALSE;
	}

	rc = NAFSdir_entry_count (szDir, NASYSACC_LEVEL_ROOT, &nFiles, &io);
	
	if (!NAFS_SUCEEDED (rc)) 
		return FALSE;

	if (nFiles == 0)
		return FALSE;
		
	NAFS_DIR_ENTRY_INFO * pInfo = (NAFS_DIR_ENTRY_INFO *)fj_malloc (nFiles * sizeof (NAFS_DIR_ENTRY_INFO));
	memset (pInfo, 0, nFiles * sizeof (NAFS_DIR_ENTRY_INFO));

	rc = NAFSlist_dir (szDir, NASYSACC_LEVEL_ROOT, pInfo, nFiles * sizeof (NAFS_DIR_ENTRY_INFO), &io);
	
	if (!NAFS_SUCEEDED (rc)) 
		return FALSE;
	
	for (i = 0; i < nFiles; i++) 
		nDelete += DeleteFile (lpszDir, pInfo [i].entry_name) ? 1 : 0;

	fj_free (pInfo);

	return nDelete == nFiles;
}

FJDLLExport BOOL FileExists (const char * lpszDir, const char * lpszFile)
{
/*
	FILE *file = fopen (lpszFile, "rb");
	
	if (file == NULL) 
		return FALSE;

	fclose(file);
	
	return TRUE;
*/
	NAFS_IO_REQUEST_CB io;
	int rc;
	UINT i, nFiles = 0;
	char szDir [NAFS_FILE_NAME_SIZE + 1] = { 0 };
	
	if (strlen (lpszFile) == 0)
		return FALSE;
		
	strncpy (szDir, lpszDir, ARRAYSIZE (szDir));
	memset (&io, 0, sizeof (io));
	
	rc = NAFSinit_io_request_cb (&io, NULL, 0);

	if (rc == NAFS_NULL_POINTER) {
		printf ("%s(%d): NAFSinit_io_request_cb: NAFS_NULL_POINTER [%s]\n", __FILE__, __LINE__, szDir);
		return FALSE;
	}

	rc = NAFSdir_entry_count (szDir, NASYSACC_LEVEL_ROOT, &nFiles, &io);

	if (!NAFS_SUCEEDED (rc)) 
		return FALSE;

	if (nFiles == 0)
		return FALSE;
		
	NAFS_DIR_ENTRY_INFO * pInfo = (NAFS_DIR_ENTRY_INFO *)fj_malloc (nFiles * sizeof (NAFS_DIR_ENTRY_INFO));
	memset (pInfo, 0, nFiles * sizeof (NAFS_DIR_ENTRY_INFO));

	rc = NAFSlist_dir (szDir, NASYSACC_LEVEL_ROOT, pInfo, nFiles * sizeof (NAFS_DIR_ENTRY_INFO), &io);
	
	if (!NAFS_SUCEEDED (rc)) 
		return FALSE;
	
	BOOL bResult = FALSE;
	
	for (i = 0; (i < nFiles) && !bResult; i++) {
		if (strncmp (lpszFile, pInfo [i].entry_name, FJSYS_MAX_FILENAME) == 0)
			bResult = TRUE;
	}

	fj_free (pInfo);
	
	return bResult;
}