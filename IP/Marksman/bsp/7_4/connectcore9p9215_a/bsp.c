/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *  Description.
 *  =======================================================================
 *  This file defines the ROM based (const) chip select initialization table.
 *  The table contains the chip select settings for the NET+ARM development
 *  board, this only supports the ARM9 based CPU's using the ARM PrimeCell
 *  MultiPort Memory Controller (PL172).
 *
 *  This table can be modified for a customized BSP.  See bsp.h for the
 *  definition of all the NetarmInitData data members.
 *
 *	 Edit History
 *  Date        Initials        Change Desription
 *
 *  07/06/05    JZW             Vantive 16443. The comments representing the interrupt numbers for
 *                              TIMER5_INTERRUPT and TIMER6_INTERRUPT are inverted. Correct.
 *                              TIMER5_INTERRUPT = 21   TIMER6_INTERRUPT = 22
 */


/*
 * Do not attempt to trace registers before the C library is
 * setup.  The global variables used by the trace library will
 * be in an undefined state until after the C library has
 * initilized the data segement.
 */
#ifdef NA_REG_TRACE_ENABLED
    #ifdef _REG_DEF_H
        #error Trace code compiled into init code.
    #endif
    #undef NA_REG_TRACE_ENABLED
#endif

#include <sysClock.h>
#include <ncc_init.h>
#include <mcreg_def.h>
#include <mcpll.h>
#include <reg_def.h>
#include <bsp_refs.h>
#include <bsp_drivers.h>
#include <mc_isr.h>
#include <gpio.h>


/*ad*
 * This board support package (BSP) supports the connectcore9p9215_a platform.  See the Programmer's
 * Guide for more details. If you are porting to a new platform, you must review
 * the contents of the files in this directory. The constants in the bsp.h file
 * (in the src/bsp/platforms/connectcore9p9215_a directory), which includes bsp_refs.h, bsp_defs.h, bsp_sys.h,
 * bsp_serial.h, bsp_drivers.h, bsp_net.h, bsp_cli.h, bsp_fs.h and bspbldr.h, define the
 * components that are included in the bsp. Review the following files 
 * carefully if you are porting to a new platform;
 * all these files are located in the platforms directory.
 * <list type="unordered">
 * <item>bsp.h            - Include a couple of header files which define for setting up the BSP.</item>
 * <item>bsp_sys.h 	  - This file defines BSP system level features.</item>
 * <item>bsp_serial.h 	  - This file defines BSP serial port settings.</item>
 * <item>bsp_drivers.h 	  - This file enable/disable drivers.</item>
 * <item>bsp_net.h 	  - This file defines BSP network interfaces.</item>
 * <item>bsp_cli.h 	  - This file defines BSP cli support.</item>
 * <item>bsp_fs.h 	  - This file defines BSP file system support.</item>
 * <item>bsp_bldr.h 	  - This file defines BSP feature values for the bootloader.</item>
 * <item>init_settings.h  - SDRAM memory settings and SPI boot setings.</item>
 * <item>bsp.c            - SRAM (flash) memory settings</item>
 * <item>CustomizeCache.c - Cache settings.</item>
 * <item>Sysclock.h       - Clock settings.</item>
 * </list>
 * For a detailed description of all the BSP platforms files, read the
 * Programmer's Guide. For a description of the defines used to enable
 * and disable the drivers in NET+OS, look under the Miscellaneous section.
 *
 * @since 6.2
 * @external
 * @category BSP
 * @overview BSP
 * @library bsp
 * @platform all
 * @include <bsp.h>
 *
*ad*/


/*ad*
 * The following table contains the registers settings for 
 * static memory (such as flash), this includes cs0-cs3, cs1 is the 
 * memory from which the code boots, some of the cs1 parameters
 * are set by strapping pins so the memory controller has enough
 * information about the SRAM so we can boot. For a detailed description 
 * of these registers, refer to the document "PrimeCell MultiPort Memory 
 * Controller (PL172) Technical Reference Manual" Section 3.1.
 *
 * @since 6.1
 * @external
 * @category BSP
 *
 * @see @link NAStaticMemoryType
 *
 * @note You will have to change these settings to match your flash type if
 * you will be using a different flash part than the one installed on the developement
 * board.
*ad*/
NAStaticMemoryType const NAStaticMemoryTable[] =
{
	{1, 0x180, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,   0x40000000, 0xFF700001},  //ADDED for CS0
	{1, 0x81, 0x00, 0x01, 0x06, 0x00, 0x05, 0x02,   0x0, 0x0}, /* cs1 */
	{0, 0x181, 0x00, 0x00, 0x1f, 0x1f, 0x1f, 0x0f, 0x03100000, 0xffff8001}, /* cs2 */
	{0, 0x82, 0x02, 0x02, 0x09, 0x02, 0x09, 0x02, 0x0, 0x0}  /* cs3 */
/*
#ifdef DOTNETMF
    // .NET has an LCD connected to cs0 
	{1, 0x180, 0x04, 0x00, 0x00, 0x00, 0x08, 0x00,   0x40000000, 0xF0000001}, 
#else
	{0, 0x82, 0x02, 0x02, 0x09, 0x02, 0x09, 0x02, 0x0, 0x0}, // cs0 
#endif
	{1, 0x81, 0x00, 0x01, 0x06, 0x00, 0x05, 0x02,   0x0, 0x0}, // cs1 
	{0, 0x181, 0x00, 0x00, 0x1f, 0x1f, 0x1f, 0x0f, 0x03100000, 0xffff8001}, // cs2 
	{0, 0x82, 0x02, 0x02, 0x09, 0x02, 0x09, 0x02, 0x0, 0x0}  // cs3 
*/
};


/*ad*
 * The following table contains the serial flash settings on NETOS 6.2 development
 * board.
 *
 * @since 6.2
 * @external
 * @category BSP
 *
 * @see @link NASpiDeviceConfigType
 *
 * @note You will have to change these settings to match your serial flash type if
 * you are using a different serial flash part than the one installed on the NETOS 6.2 developement
 * board.
*ad*/
NASpiDeviceConfigType NADefaultSflashDevice = {
	1,
	NULL, 
	NULL,
	NA_SPI_MODE0,
	NA_SPI_MSB,
	14054400,
	10,
	BSP_DEFAULT_FLASH_DEVICE
};


/*ad*
 *  The NAAhbPriorityTab table contains the priority of each interrupt in the AHB Bus.
 *  The NAAhbPriorityTab allows flexible prioritization for all the AHB interrupts 
 *  in the NET+ARM that drive the ARM processor IRQ.  The table is
 *  configured with interrupts of highest priority at the beginning
 *  and interrupts of lowest priority towards the end of the table.  
 *
 *  For instance, the interrupt for TIMER2_INTERRUPT has the highest priority.
 *
 *  Adjust the values in this array as appropriate for your platform. 
 *  Each level must be used once and only once.
 *
 *  Warning:    If this list is filled out incorrectly, for example, a 
 *              level is left out, or a level is duplicated, NAInitIsr
 *              will call customizeErrorHandler with ERROR_IRQ_TABLE blinks.
 *
 * @since 6.1
 * @external
 * @category BSP 
*ad*/
int unsigned NAAhbPriorityTab[32] = 
{
    /* High Priority Interrupts */
    /* 15 */ EARLY_POWER_LOSS_INTERRUPT,   
    /* 19 */ TIMER1_INTERRUPT,             
    /* 18 */ TIMER0_INTERRUPT,             
    /* 20 */ TIMER2_INTERRUPT,             
    /* 21 */ TIMER3_INTERRUPT,             
    /* 22 */ TIMER4_INTERRUPT,             
    /* 23 */ TIMER5_INTERRUPT,             
    /* 24 */ TIMER6_INTERRUPT,             
    /* 25 */ TIMER7_INTERRUPT,             
    /* 26 */ TIMER8_INTERRUPT,             
    /* 27 */ TIMER9_INTERRUPT,             
    /* 00 */ WATCHDOG_INTERRUPT,           
    /* 01 */ AHB_BUS_ERROR_INTERRUPT,      
    /* 02 */ EXTERNAL_DMA_INTERRUPT,       
    /* 03 */ CPU_WAKE_INTERRUPT,           
    /* 04 */ ETH_RECEIVE_INTERRUPT,        
    /* 05 */ ETH_TRANSMIT_INTERRUPT,       
    /* 06 */ ETH_PHY_INTERRUPT,            
    /* 07 */ UARTA_INTERRUPT,              
    /* 08 */ UARTB_INTERRUPT,              
    /* 09 */ UARTC_INTERRUPT,              
    /* 10 */ UARTD_INTERRUPT,              
    /* 11 */ SPI_INTERRUPT,                
    /* 14 */ ADC_INTERRUPT,                
    /* 16 */ I2C_INTERRUPT,                
    /* 17 */ RTC_INTERRUPT,                
    /* 28 */ EXTERNAL0_INTERRUPT,          
    /* 29 */ EXTERNAL1_INTERRUPT,          
    /* 30 */ EXTERNAL2_INTERRUPT,          
    /* 31 */ EXTERNAL3_INTERRUPT,          
    /* 12 */ IOP0_INTERRUPT,          
    /* 13 */ IOP1_INTERRUPT,
    /* Low Priority Interrupts */
};
          

/*ad*
 * Return the oscillator type for this chip stored in NANetarmInitData.  The
 * NANetarmInitData array is indexed by the chip ID.  This function
 * reads the chip ID from H/W and uses it to look up the oscillator
 * type stored in the table.
 *
 * @since 4.0
 * @external
 * @category BSP 
 * @return NA_SELECT_THE_CRYSTAL_INPUT  internal oscillator used
 * @return NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT               external oscillator used
*ad*/
unsigned int NAusesTheInternalOscillator(void)
{
    unsigned int BP;

    NAReadPLLBypassStatus(&BP);

    if (BP == 0)
      return NA_SELECT_THE_CRYSTAL_INPUT;
    else
      return NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT;
}







