/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp.h This file defines BSP wide constants, data structures and prototypes.
 * Please refer to the BSP porting guide for a description of this file.
 *
 *
 * Edit History
 *
 * Date      INitials       Change Description
 * 02/13/04  JZW            removed definition of BSP_PRTENG_THREAD_PRIORITY
 *                          Changed BSP_SIMPLE_SERIAL_PORT's definition
 *                          from ....PORTB to ....PORTA
 * 07/01/05	 JZW            vantive 16441 - Remove restrictive code that requires PORT A to
 *                          be enabled if PORTB is enabled.
 */

#ifndef BSP_H
#define BSP_H

#include "bsp_refs.h"	/* spi_master_api.h (-> Npttypes.h) */
#include "bsp_defs.h"
#include "bsp_sys.h"	/* init_settings.h */
#include "bsp_serial.h"
#include "bsp_drivers.h"
#include "bsp_net.h"
#include "bsp_cli.h"
#include "bsp_fs.h"		/* fs.h */
#include "bsp_bldr.h"

#endif


