/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * This file contains RTC device table that allows user to add
 * multiple RTC devices.
 * Each RTC device must be added in this table with a unique device id.
 */

#include "rtc.h"
#include "rtc_def.h"

/*ad*
 * Real Time Clock Device Function Table.
 *
 * It contains a list of real time clock devices and its interfaces.
 * When RTC API is called, it searches this table for specific RTC
 * device and executes its function.
 * This table allows user to add addition Real Time Clock devices.
 * Each RTC device must be given unique device id. The device id 
 * is used for executing which RTC's interfaces.
 *  
 * The device id 0 in the table is initialized and
 * registered to the Greenhills or GNU standard time functions if  
 * BSP_INCLUDE_RTC_DRIVER is TRUE.
 *
 * @since 6.2
 * @external
 * @category RealTimeClockDriver
 *
 * @see @link NaRtcTable_t 
 * @see @link BSP_INCLUDE_RTC_DRIVER
 *
*ad*/
NaRtcTable_t naRtcDeviceTable[] = {

    {0, naRtcDrvOpen, naRtcDrvGetInfo, naRtcDrvGetTime, naRtcDrvSetTime, 
     naRtcDrvSetAlarmAbsolute, naRtcDrvSetAlarmRelative, naRtcDrvDisableAlarm}  /* NS9215 RTC device */
    
};    

int const naRtcDeviceCount = sizeof(naRtcDeviceTable) / sizeof(naRtcDeviceTable[0]);


