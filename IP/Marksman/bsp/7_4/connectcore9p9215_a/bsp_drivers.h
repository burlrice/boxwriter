/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_driveres.h This file defines BSP feature variables for driver inclusion.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_DRIVERS_H
#define BSP_DRIVERS_H

#include <Npttypes.h>
#include "bsp_defs.h"
#include "gpio.h"


#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * Set this constant to TRUE to build the IEEE-1284 parallel port driver.
 *
 * @since 1.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_PARALLEL_DRIVER     FALSE

/*ad*
 * Set this constant to TRUE to include the NUL driver.
 *
 * @since 1.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_NUL_DRIVER          FALSE

/*ad*
 * Set this constant to TRUE to include the loopback driver.
 *
 * @since 1.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_LOOPBACK_DRIVER     FALSE

/*ad*
 * Set this constant to TRUE to include the UPD debug driver.
 *
 * @since 1.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_UDP_DEBUG_DRIVER    FALSE


/*ad*
 * Set this constant to TRUE to include the LCD driver.
 *
 * @since 6.1
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_LCD_DRIVER			FALSE


/*ad*
 * Set this constant to TRUE to include the Real Time Clock driver.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_RTC_DRIVER          FALSE


/*ad*
 * Sets this constant to TRUE to include the I2C_EXPANDER APIs in
 * the functional name based GPIO API functions.
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration
*ad*/

#define BSP_INCLUDE_I2C_EXPANDER        TRUE

 
/*ad*
 * This constant determines the USB device driver API used in the system.
 * Set this constant to one of the values in @link UsbDeviceDriverVersion
 *
 * @since 7.1
 * @external
 * @category BSP:BSP_Configuration
 *
*ad*/
#define BSP_USB_DEVICE_VERSION          BSP_USB_DRIVER_VERSION_NA


/*ad*
 * This constant determines which NVRAM driver will be built.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_NVRAM_DRIVER    BSP_NVRAM_LAST_FLASH_SECTOR


/*ad*
 * This constant determines the name of default serial flash device name.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_DEFAULT_FLASH_DEVICE		"defaultSFlash"


/*ad*
 * If BSP_NVRAM_DRIVER is set to BSP_NVRAM_EEPROM.  This constant
 * determines where the part should be mapped to in memory.
 *
 * @since 3.0
 * @external
 * @deprecated 6.1
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_EEPROM_BASE     0x3000000

/*ad*
 * If BSP_NVRAM_DRIVER is set to BSP_NVRAM_EEPROM.  This constant
 * determines the size of the part.
 *
 * @since 3.0
 * @external
 * @deprecated 6.1
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_EEPROM_SIZE     0x2000

/*ad*
 * BSP_FLASH_BANKS sets the number of flash memory banks.  The number of
 * flash banks is the number of flash chips located in memory where one
 * chip follows another in the address map.
 * For example, 2 16-bit chips wired up as 16-bit flash would count as 2
 * Flash banks.  2 16-bit chips wired up as 32-bit flash (in parallel)
 * would count as 1 bank.  4 16-bit chips wired up as 32-bit flash would
 * count as 2 banks (2 pairs of chips where the pairs follow each other in
 * the address map).
 *
 * @since 5.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_FLASH_BANKS  1

/*ad*
 * Maximum number of PWM channels supported
 *
 * @since 6.2
 * @include 	<bsp.h>
 * @external
 * @category BSP:BSP_Configuration
 *
*ad*/
#define BSP_PWM_MAXIMUM_CHANNELS     5     


/*ad*
 * Setting the BSP_QUAD_DECODER_ENABLE flag to TRUE enables the
 * quadrature decoder driver.
 *
 * @since 7.3
 * @include     <bsp_drivers.h>
 * @external
 * @category BSP:BSP_Configuration
 *
 *ad*/

#define	BSP_QUAD_DECODER_ENABLE	TRUE


/*ad*
 * Setting the BSP_QUAD_FLOAT flag to TRUE enables the
 * quadrature decoder floating point function, and causes
 * the floating point libraries to be linked in to the application.
 *
 * @since 7.3
 * @include     <bsp_drivers.h>
 * @external
 * @category BSP:BSP_Configuration
 *
 *ad*/

#define	BSP_QUAD_FLOAT	FALSE



/*ad*
 * Set this constant to TRUE to build in support for the NET+OS
 * SDIO driver.  This driver is only supported on the connectcore9p9215_a
 * platform.
 *
 * @since 7.3
 * @external 
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_SDIO_DRIVER         TRUE



/*ad*
 * Set this constant to TRUE to build in support for the NET+OS 
 * AES driver.  This will allow software that uses AES to take
 * advantage of the AES encryption/decryption engine built into
 * the NS9215 and NS9210 processors.
 *
 * The AES encryption/decryption engine uses one of the external 
 * DMA channels.
 *
 * @since 7.3
 * @external 
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_WANT_HW_AES            TRUE


#ifdef DOTNETMF
#undef  BSP_INCLUDE_RTC_DRIVER
#define BSP_INCLUDE_RTC_DRIVER   FALSE
#undef  BSP_WANT_HW_AES
#define BSP_WANT_HW_AES          FALSE
#undef  BSP_INCLUDE_SDIO_DRIVER 
#define BSP_INCLUDE_SDIO_DRIVER  FALSE
#endif


#if (BSP_WANT_AES) && (BSP_GPIO_MUX_DMA_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
    #error External DMA channel 0 and the AES encryption/decryption engine cannot be used simultaneously.
#endif





#ifdef __cplusplus
}
#endif

#endif /* BSP_DRIVERS_H */

