/*****************************************************************************
* Copyright (c) 2007 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/

#include "fwtypes.h"
#include "sdioTable.h"
#include "bsp_drivers.h"
#include "iop_api.h"
#include "sdioIopDriver.h"
#include "sdioIopDriver2.h"
#include "sdioNetos.h"

#define NA_SDIO_3_POINT_0_TO_3_POINT_1_VOLTS        (BIT_18)
#define NA_SDIO_3_POINT_1_TO_3_POINT_2_VOLTS        (BIT_19)
#define NA_SDIO_3_POINT_2_TO_3_POINT_3_VOLTS        (BIT_20)
#define NA_SDIO_3_POINT_3_TO_3_POINT_4_VOLTS        (BIT_21)
#define NA_SDIO_3_POINT_4_TO_3_POINT_5_VOLTS        (BIT_22)
#define NA_SDIO_3_POINT_5_TO_3_POINT_6_VOLTS        (BIT_23)

/*
 * The NS9215 and NS9210 chips will operate in this range of voltages.  So, the
 * voltage supplied to the card should be within this range too.  If you know 
 * exactly what voltage range your hardware will use, then you should narrow down
 * this range accordingly.
 */
#define VOLTAGE_RANGE_SUPPORTED_BY_NS9215       NA_SDIO_3_POINT_0_TO_3_POINT_1_VOLTS | \
                                                NA_SDIO_3_POINT_1_TO_3_POINT_2_VOLTS | \
                                                NA_SDIO_3_POINT_2_TO_3_POINT_3_VOLTS | \
                                                NA_SDIO_3_POINT_3_TO_3_POINT_4_VOLTS | \
                                                NA_SDIO_3_POINT_4_TO_3_POINT_5_VOLTS | \
                                                NA_SDIO_3_POINT_5_TO_3_POINT_6_VOLTS 

NaSdioTable_t naSdioTable[] = 
{
    {
        NA_IOP_PROCESSOR_PIC0,
        (NaIopProgram_t *) &naSdioIopDriver,
        VOLTAGE_RANGE_SUPPORTED_BY_NS9215,
        naSdioGetSemaphore,
        naSdioReleaseSemaphore,
        naSdioCreateSemaphore
    }
};

const unsigned naSdioTableSize = sizeof(naSdioTable) / sizeof(naSdioTable[0]);


