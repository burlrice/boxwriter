
/*****************************************************************************
* Copyright (c) 2004 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/

#ifndef NA_GPIO_H_
#define NA_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

/* old carrier board has 4 PCA9500 I2C parts */
/* #define NA_OLD_CARRIER */

/* These are the names of GPIO functions. */
#define GPIO_MFGO	(-1)	    /* Manufacturing Output */
#define GPIO_MFGI	(-2)	    /* Manufacturing Input */

#define GPIO_PROCESSOR_0      (-3)       /* Processor GPIO 0 */
#define GPIO_PROCESSOR_1      (-4)       /* Processor GPIO 1 */
#define GPIO_PROCESSOR_2      (-5)       /* Processor GPIO 2 */
#define GPIO_PROCESSOR_3      (-6)       /* Processor GPIO 3 */
#define GPIO_PROCESSOR_4      (-7)       /* Processor GPIO 4 */
#define GPIO_PROCESSOR_5      (-8)       /* Processor GPIO 5 */
#define GPIO_PROCESSOR_6      (-9)       /* Processor GPIO 6 */
#define GPIO_PROCESSOR_7      (-10)      /* Processor GPIO 7 */
#define GPIO_PROCESSOR_8      (-11)      /* Processor GPIO 8 */
#define GPIO_PROCESSOR_9      (-12)      /* Processor GPIO 9 */
#define GPIO_PROCESSOR_10     (-13)      /* Processor GPIO 10 */
#define GPIO_PROCESSOR_11     (-14)      /* Processor GPIO 11 */
#define GPIO_PROCESSOR_12     (-15)      /* Processor GPIO 12 */
#define GPIO_PROCESSOR_13     (-16)      /* Processor GPIO 13 */


#define GPIO_I2C_0        (-17)       /* I2C GPIO IO Expander */
#define GPIO_I2C_1        (-18)       /* I2C GPIO IO Expander */
#define GPIO_I2C_2        (-19)       /* I2C GPIO IO Expander */
#define GPIO_I2C_3        (-20)       /* I2C GPIO IO Expander */
#define GPIO_I2C_4        (-21)       /* I2C GPIO IO Expander */
#define GPIO_I2C_5        (-22)       /* I2C GPIO IO Expander */
#define GPIO_I2C_6        (-23)       /* I2C GPIO IO Expander */
#define GPIO_I2C_7        (-24)       /* I2C GPIO IO Expander */

/* Here is a list of the different LEDs.  Each is on when the pin is low.
 *
 * GPIO_ERROR_LED
 *  This is the LED that reports failures by blinking a 3 digit
 *  code.  
 * 
 */
#define GPIO_ERROR_LED    (-25)       /* error LED           */
#define GPIO_USER_LED1    (-25)       /* user LED 1 */
#define GPIO_USER_LED2    (-26)       /* user LED 2 */
#define GPIO_USER_BUTTON1 (-27)       /* user push button 1 */
#define GPIO_USER_BUTTON2 (-28)       /* user push button 2 */
#define GPIO_WAKEUP_BUTTON (-29)      /* user wakeup button */

#define GPIO_UARTA_RTS	  (-30)    /* RTS for UARTA (ns9210/ns9215 errata fix) */
#define GPIO_UARTB_RTS	  (-31)    /* RTS for UARTB (ns9210/ns9215 errata fix) */
#define GPIO_UARTC_RTS	  (-32)    /* RTS for UARTC (ns9210/ns9215 errata fix) */
#define GPIO_UARTD_RTS	  (-33)    /* RTS for UARTD (ns9210/ns9215 errata fix) */

#define GPIO_UARTA_CTS	  (-34)    /* RTS for UARTC */
#define GPIO_UARTB_CTS	  (-35)    /* RTS for UARTC */
#define GPIO_UARTC_CTS	  (-36)    /* RTS for UARTC */
#define GPIO_UARTD_CTS	  (-37)    /* RTS for UARTC */


/* This function returns TRUE if the specified function exists on the
 * hardware.
 */
extern int naGpioFunctionExists(int function);

/* Set this gpio pin to be an input.
 * Returns TRUE if successful.
 */
extern int naGpioSetInput(int function);

/* Set this gpio pin to be an output.
 * Returns TRUE if successful.
 */
extern int naGpioSetOutput(int function, unsigned int default_value);

/* Set this gpio pin to be an interrupt.
 * level is 1 for high, 0 for low.
 * edge_trigger is 1 for edge-triggered, 0 for level-triggered.
 * Returns TRUE if successful.
 */
extern int naGpioSetInterrupt(int function, int level, int edge_trigger);

/* Returns 0 or non-zero value of pin.
 * Returns (unsigned long)-1 if unable to get value.
 */
extern unsigned long naGpioGetValue(int function);

/* GPIO pin will be set based on value being 0 or non-zero.  Will be ignored
 * if pin is set as input or interrupt.
 * Returns TRUE if successful.
 */
extern int naGpioSetOutputValue(int function, unsigned long value);

/* GPIO pin will be set to an output with an initial value based on value being
 * 0 or non-zero.  Will atomically change to an output with the desired value
 * if possible.  This attempts to avoid a glitch as the pin becomes an output.
 *
 * Obsoletes GpioSetOutput.
 */
extern int naGpioSetOutputWithValue(int function, unsigned long value);

/* Set a gpio pin to a particular function in the gpio mux.  This is only really
 * useful for processor gpio pins, but calling this function abstracts processor
 * differences.
 * Returns TRUE if successful, or FALSE otherwise.
 */
extern int naGpioSetFunction(int function, int gpio_function);

/*
 * Returns the GPIO control register address and pin associated with the 
 * function. Powersave mode requires GPIO bit to be set in cache, so it 
 * is not easy to use existing naGpioSetOutputValue() function directly.
 *
 */
extern int naGpioGetAddress(int function, unsigned long *addr, unsigned long *pin);


#ifdef __cplusplus
}
#endif

#endif /* GPIO_H_ */

