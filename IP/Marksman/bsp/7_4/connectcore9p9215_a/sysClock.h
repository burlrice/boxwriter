/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 * Description.
 * =======================================================================
 * This file contains the public API for the SYSCLK and XTAL1 clock.  These
 * values should be updated and defined for each peculiar board support 
 * package.  Be aware that the values for the Crystal or XTAL input have 
 * huge ramifications on the system timing, in particular, serial data 
 * transfer rates.
 *                                                                        
 *                                                                        
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 * PMJ  04/30/01   Set PLLTST_SELECT to the NA_SELECT_THE_CRYSTAL_INPUT
 *                 (the oscillator input).
 * PMJ  06/06/01   Redefined PLLTST_SELECT and PLL_CONTROL_REGISTER_N_VALUE to
 *                 use values based within the chip select system settings.  This
 *                 allows auto-sensing of the chip type, for those systems using
 *                 multiple configurations of hardware. (see Settings.h)
 *
 */
#ifndef __SYSCLOCK_H
#define __SYSCLOCK_H

#ifdef __cplusplus
extern "C"
{
#endif

// #include <bsp_api.h>
// #include <bsp.h>
#include <sysClockApi.h>

/*ad*
 *
 * Selecting the NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT bypasses the PLL.
 * Selecting the NA_SELECT_THE_CRYSTAL_INPUT uses the PLL and reads the 
 * PLL register to determine the frequency at which the chip is running
 * based on the input frequency specified below.
 *
 * @name SYSCLOCK_SOURCE_DEFINES "Sysclock source defines"
 *
 * @param NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT  Use this define if you are using an external oscillator
 * @param NA_SELECT_THE_CRYSTAL_INPUT Select this define if you are using a crystal as input
 *
 * @external
 * @category Clock_and_Timer
 * @since 6.1
*ad*/
#define NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT  0
#define NA_SELECT_THE_CRYSTAL_INPUT              1


/*ad*
 * This directive defines the input frequency to the Digi International
 * ARM9 processor.  This input can be either a crystal or an
 * external oscillator.  
 *
 * This input clock can route to the internal PLL, or bypass
 * it, based on strapping options.
 *
 * @external
 * @category Clock_and_Timer
 * @since 6.1
 *
*ad*/
#define NA_ARM9_INPUT_FREQUENCY             29491200



/*ad*
 *      This directive determines the clock source to be used.  It is 
 *      the address input to the SYSCLK signal multiplexer.  The SYSCLK 
 *      has two possible sources, one from the TTL clock input applied 
 *      to the XTAL1 pin, and the other from the Crystal Oscillator and 
 *      Phase Lock Loop circuit.
 *
 *
 *      When PLLTST_SELECT = NA_SELECT_THE_EXTERNAL_OSCILLATOR_INPUT
 *
 *          SYSCLK uses the XTAL1 its source.
 *
 *
 *      When PLLTST_SELECT = NA_SELECT_THE_CRYSTAL_INPUT
 *
 *          SYSCLK uses the Crystal Ocsillator and PLL as its source.
 *
 * @external
 * @category Clock_and_Timer
 * @since 6.0
 *
*ad*/
#define PLLTST_SELECT                       NAusesTheInternalOscillator()



 


#ifdef __cplusplus
}
#endif
#endif


