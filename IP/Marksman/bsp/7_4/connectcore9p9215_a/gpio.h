/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *  Description.
 *
 *  This file contains the public API code that sets up the GPIO ports.  THE
 *  DEFINITIONS IN THIS FILE AND THE SOURCE CODE FOR THIS INTERFACE SHOULD BE 
 *  CUSTOMIZED FOR YOUR PARTICULAR HARDWARE.  The version of the file that is 
 *  shipped with the NET+OS BSP is written to support NET+OS development boards.  
 *  Your hardware will be different and the code in this file must be 
 *  customized to support it.
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  =======================================================
 *
 *
 */


#ifndef __GPIO_H_
#define __GPIO_H_

#include <gpiomux_def.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * Set this constant to indicate the data bus width on your board.  The data bus
 * width is selected in hardware through strapping pins.  You must set this 
 * constant to match the data bus width selected by the hardware strapping
 * pins.
 *
 * Set BSP_GPIO_MUX_DATA_BUS_WIDTH to BSP_GPIO_MUX_DATA_BUS_WIDTH_16 if a 16-bit
 * data bus width has been selected, or to BSP_GPIO_MUX_DATA_BUS_WIDTH_32 if a
 * 32-bit bus width has been selected.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215
*ad*/
#define BSP_GPIO_MUX_DATA_BUS_WIDTH BSP_GPIO_MUX_DATA_BUS_WIDTH_16

#if (BSP_GPIO_MUX_DATA_BUS_WIDTH != BSP_GPIO_MUX_DATA_BUS_WIDTH_16) && \
    (BSP_GPIO_MUX_DATA_BUS_WIDTH != BSP_GPIO_MUX_DATA_BUS_WIDTH_32) 
#error Invalid data bus width
#endif






/*ad*
 * The NS9215 has 108 pins are multiplexed with various functions including
 * general purpose input and output (GPIO).  The functions supported by these
 * pins include:
 * <list type="unordered">
 * <item>4 UARTs</item>
 * <item>inputs and outputs for 10 timers</item>
 * <item>control and data signals for 16 or 32-bit data bus</item>
 * <item>SPI bus</item>
 * <item>I2C bus</item>
 * <item>Quadrature decoder</item>
 * <item>4 external interrupt trigers</item>
 * <item>4 extended PWM outpus</item>
 * <item>2 external DMA channels</item>
 * <item>Ethernet MII</item>
 * </list>
 *
 * Select options other than @link BSP_GPIO_MUX_INTERNAL_USE_ONLY to have the
 * BSP configure groups of pins for specific functions when the system starts.
 * An example of a multiplexer group is @link BSP_GPIO_MUX_SPI_0 , which can be 
 * assigned the following possible 
 * values: @link BSP_GPIO_MUX_INTERNAL_USE_ONLY , @link BSP_GPIO_MUX_USE_PRIMARY_PATH , 
 * or @link BSP_GPIO_MUX_USE_ALTERNATE_PATH .  
 *
 * When @link BSP_GPIO_MUX_SPI_0 is set to @link BSP_GPIO_MUX_USE_PRIMARY_PATH , the 
 * multiplexing will be routed using the primary connection, as
 * defined in the Piniout/GPIO section of the NS9215 Hardware Reference Manual.
 *
 * Alternatively, the group can be set to @link BSP_GPIO_MUX_USE_ALTERNATE_PATH ,
 * which also sets multiplexer pins to use SPI, however the alternate 
 * paths are used inplace of the primary paths.  The NS9215 Hardware 
 * Reference Manual describes the pins that have alternate and primary
 * functions.  Lastly, when @link BSP_GPIO_MUX_SPI_0  is set 
 * to @link BSP_GPIO_MUX_INTERNAL_USE_ONLY , the multiplexer does not route the
 * pins to SPI.  Instead, the pins are available to some other 
 * multiplexing functions, or, if all functions are using 
 * the @link BSP_GPIO_MUX_INTERNAL_USE_ONLY , the pin can be used as a GPIO.
 *
 * Also included are GPIO access functions for setting and sensing the 
 * state of the pins.  See @link NAgetGPIOpin and @link NAsetGPIOpin for more
 * details.
 *
 *
 * @note Only one multiplexer group can claim the functionality on a pin.  If
 * multiple groups claim use on a pin, a compile time error will occur.  This
 * should save time when configuring a BSP.
 *
 * When a multiplexer pin is left unspecified, it defaults to a GPIO
 * receiver.  That means all possible functions that can be routed out a 
 * multiplexer pin have been assigned the BSP_GPIO_MUX_INTERNAL_USE_ONLY 
 * setting.
 *
 * To change a multiplexer pin to a GPIO output, see the section on
 * "Altering the BSP_GPIO_INITIAL_STATE_PINx".  For convenience, all 108 
 * pins have been included.  However, only pins that specify a GPIO output
 * are necessary.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215
 * @overview GPIO NS9215
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/




/*ad*
 * The NS9215 has the ability to multiplex four IRQ signals to trigger the
 * fast call to interrupt service routines.  Routing of these signals 
 * can use a primary path or alternate.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @overview IRQ
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the IRQ0 signal is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary or 
 * 3 alternate GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IRQ_0                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_IRQ_0 is not set correctly.
#endif


/*ad*
 * This directive controls how the IRQ1 signal is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary or 
 * 2 alternate GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IRQ_1                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IRQ_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IRQ_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IRQ_1 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IRQ_1 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
    (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_IRQ_1 is not set correctly.
#endif


/*ad*
 * This directive controls how the IRQ2 signal is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary or 
 * 2 alternate GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IRQ_2                          BSP_GPIO_MUX_USE_ALTERNATE_PATH // BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IRQ_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IRQ_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IRQ_2 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_IRQ_2 is not set correctly.
#endif


/*ad*
 * This directive controls how the IRQ3 signal is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the one pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IRQ_3                          BSP_GPIO_MUX_USE_ALTERNATE_PATH  //BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_USE_3RD_ALTERNATE_PATH)
    #error BSP_GPIO_MUX_IRQ_3 is not set correctly.
#endif



/*ad*
 * This directive controls how the IRQ0 signal is configured.
 * It can be configured to be an active high or active low 
 * level sensitive interrupt, or a rising or falling edge
 * sensitive interrupt.
 *
 * @note    This directive is ignored if BSP_GPIO_MUX_IRQ_0
 *          is set to BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * @since 6.1
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_LOW
 * @see @link BSP_GPIO_MUX_IRQ_RISING_EDGE
 * @see @link BSP_GPIO_MUX_IRQ_FALLING_EDGE
*ad*/
#define BSP_GPIO_MUX_IRQ_0_CONFIG BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
#if (BSP_GPIO_MUX_IRQ_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
    #if (BSP_GPIO_MUX_IRQ_0_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_HIGH) && \
        (BSP_GPIO_MUX_IRQ_0_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_LOW) && \
        (BSP_GPIO_MUX_IRQ_0_CONFIG != BSP_GPIO_MUX_IRQ_RISING_EDGE) && \
        (BSP_GPIO_MUX_IRQ_0_CONFIG != BSP_GPIO_MUX_IRQ_FALLING_EDGE) 
        #error BSP_GPIO_MUX_IRQ_0 is not set correctly.
    #endif
#endif

/*ad*
 * This directive controls how the IRQ1 signal is configured.
 * It can be configured to be an active high or active low 
 * level sensitive interrupt, or a rising or falling edge
 * sensitive interrupt.
 *
 * @note    This directive is ignored if BSP_GPIO_MUX_IRQ_1
 *          is set to BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * @since 6.1
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_LOW
 * @see @link BSP_GPIO_MUX_IRQ_RISING_EDGE
 * @see @link BSP_GPIO_MUX_IRQ_FALLING_EDGE
*ad*/
#define BSP_GPIO_MUX_IRQ_1_CONFIG BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
#if (BSP_GPIO_MUX_IRQ_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
    #if (BSP_GPIO_MUX_IRQ_1_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_HIGH) && \
        (BSP_GPIO_MUX_IRQ_1_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_LOW) && \
        (BSP_GPIO_MUX_IRQ_1_CONFIG != BSP_GPIO_MUX_IRQ_RISING_EDGE) && \
        (BSP_GPIO_MUX_IRQ_1_CONFIG != BSP_GPIO_MUX_IRQ_FALLING_EDGE) 
        #error BSP_GPIO_MUX_IRQ_1 is not set correctly.
    #endif
#endif


/*ad*
 * This directive controls how the IRQ2 signal is configured.
 * It can be configured to be an active high or active low 
 * level sensitive interrupt, or a rising or falling edge
 * sensitive interrupt.
 *
 * @note    This directive is ignored if BSP_GPIO_MUX_IRQ_2
 *          is set to BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * @since 6.1
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_LOW
 * @see @link BSP_GPIO_MUX_IRQ_RISING_EDGE
 * @see @link BSP_GPIO_MUX_IRQ_FALLING_EDGE
*ad*/
#define BSP_GPIO_MUX_IRQ_2_CONFIG   BSP_GPIO_MUX_IRQ_FALLING_EDGE
#if (BSP_GPIO_MUX_IRQ_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
    #if (BSP_GPIO_MUX_IRQ_2_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_HIGH) && \
        (BSP_GPIO_MUX_IRQ_2_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_LOW) && \
        (BSP_GPIO_MUX_IRQ_2_CONFIG != BSP_GPIO_MUX_IRQ_RISING_EDGE) && \
        (BSP_GPIO_MUX_IRQ_2_CONFIG != BSP_GPIO_MUX_IRQ_FALLING_EDGE) 
        #error BSP_GPIO_MUX_IRQ_2 is not set correctly.
    #endif
#endif


/*ad*
 * This directive controls how the IRQ3 signal is configured.
 * It can be configured to be an active high or active low 
 * level sensitive interrupt, or a rising or falling edge
 * sensitive interrupt.
 *
 * @note    This directive is ignored if BSP_GPIO_MUX_IRQ_3
 *          is set to BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * @since 6.1
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:IRQ
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
 * @see @link BSP_GPIO_MUX_IRQ_ACTIVE_LOW
 * @see @link BSP_GPIO_MUX_IRQ_RISING_EDGE
 * @see @link BSP_GPIO_MUX_IRQ_FALLING_EDGE
*ad*/
#define BSP_GPIO_MUX_IRQ_3_CONFIG	BSP_GPIO_MUX_IRQ_FALLING_EDGE  //BSP_GPIO_MUX_IRQ_ACTIVE_HIGH
#if (BSP_GPIO_MUX_IRQ_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
    #if (BSP_GPIO_MUX_IRQ_3_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_HIGH) && \
        (BSP_GPIO_MUX_IRQ_3_CONFIG != BSP_GPIO_MUX_IRQ_ACTIVE_LOW) && \
        (BSP_GPIO_MUX_IRQ_3_CONFIG != BSP_GPIO_MUX_IRQ_RISING_EDGE) && \
        (BSP_GPIO_MUX_IRQ_3_CONFIG != BSP_GPIO_MUX_IRQ_FALLING_EDGE) 
        #error BSP_GPIO_MUX_IRQ_3 is not set correctly.
    #endif
#endif


/*ad*
 * The NS9215 supports 5 standard mode PWM outputs.  These PWM outputs
 * operate in the same way as PWM operated on the NS9360.  In addition 
 * to the standard mode PWM outputs, the NS9215 also supports 4 extended
 * mode PWM ouputs.
 *
 * Set the BSP_GPIO_MUX_PWM_X constants to enable route the standard PWM
 * outputs through the GPIO MUX.  Extended mode PWM uses the timer event
 * output signals.  Use the BSP_GPIO_MUX_TIMER_X_OUTPUT for timers 6-9 
 * on the NS9215 to route the timer event output signals used for extended
 * mode PWM.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @overview PWM
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/


/*ad*
 * This directive controls how the standard mode PWM 0 output is routed through the 
 * GPIO MUX.  Set this constant to BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * if your application does not use PWM 0.  This will leave the
 * GPIO pin available for other applications.  Set this constant
 * to BSP_GPIO_MUX_USE_PRIMARY_PATH to route the PWM signal to
 * the GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_PWM_0                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_PWM_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_PWM_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
    #error BSP_GPIO_MUX_PWM_0 is not set correctly.
#endif



/*ad*
 * This directive controls how the standard mode PWM 1 output is routed through the 
 * GPIO MUX.  Set this constant to BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * if your application does not use PWM 1.  This will leave the
 * GPIO pin available for other applications.  Set this constant
 * to BSP_GPIO_MUX_USE_PRIMARY_PATH to route the PWM signal to
 * the GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_PWM_1                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_PWM_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_PWM_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
    #error BSP_GPIO_MUX_PWM_1 is not set correctly.
#endif



/*ad*
 * This directive controls how the standard mode PWM 2 output is routed through the 
 * GPIO MUX.  Set this constant to BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * if your application does not use PWM 2.  This will leave the
 * GPIO pin available for other applications.  Set this constant
 * to BSP_GPIO_MUX_USE_PRIMARY_PATH to route the PWM signal to
 * the GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_PWM_2                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_PWM_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_PWM_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
    #error BSP_GPIO_MUX_PWM_2 is not set correctly.
#endif



/*ad*
 * This directive controls how the standard mode PWM 3 output is routed through the 
 * GPIO MUX.  Set this constant to BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * if your application does not use PWM 3.  This will leave the
 * GPIO pin available for other applications.  Set this constant
 * to BSP_GPIO_MUX_USE_PRIMARY_PATH to route the PWM signal to
 * the GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_PWM_3                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_PWM_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_PWM_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
    #error BSP_GPIO_MUX_PWM_3 is not set correctly.
#endif


/*ad*
 * This directive controls how the standard mode PWM 4 output is routed through the 
 * GPIO MUX.  Set this constant to BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * if your application does not use PWM 4.  This will leave the
 * GPIO pin available for other applications.  Set this constant
 * to BSP_GPIO_MUX_USE_PRIMARY_PATH to route the PWM signal to
 * the GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:PWM
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_PWM_4                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_PWM_4 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_PWM_4 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
    #error BSP_GPIO_MUX_PWM_4 is not set correctly.
#endif



/*ad*
 * The NS9215 can support one Serial Peripheral Interface (SPI) bus.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SPI
 * @overview SPI
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the SPI bus is routed through
 * the GPIO multiplexor.  
 * These signals can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary or 
 * alternate GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SPI
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_SPI_0                          BSP_GPIO_MUX_INTERNAL_USE_ONLY

#if (BSP_GPIO_MUX_SPI_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_SPI_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_SPI_0 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_SPI_0 is not correctly set up
#endif




/*ad*
 * The NS9215 has the ability to multiplex the 2 DMA channels.  These  
 * channels are used to transfer data to (and from) memory space to 
 * an external peripheral.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:DMA
 * @overview DMA
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls whether the external DMA channel 0 
 * signals are routed out the GPIO multiplexer.  These signals 
 * can stay internal (allowing GPIO or other special functions 
 * to multiplex), or can be routed to the primary or alternate 
 * GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:DMA
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_DMA_0                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_DMA_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_DMA_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_DMA_0 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_DMA_0 is not correctly configured
#endif


/*ad*
 * This directive controls how the external DMA channel 1 
 * signals are routed out the GPIO multiplexer.  These signals 
 * can stay internal (allowing GPIO or other special functions 
 * to multiplex), or can be routed to the primary or alternate 
 * GPIO pins.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:DMA
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_DMA_1                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_DMA_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_DMA_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_DMA_1 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
    #error BSP_GPIO_MUX_DMA_1 is not correctly configured
#endif





/*ad*
 * The physical I2C bus consists of two open-drain signal lines: serial 
 * data (SDA) and serial clock (SCL). Pullup resistors are required; see 
 * the standard I2C bus specification for the correct value for the 
 * application. Each device connected to the bus is software addressable
 * by a unique 7- or 10-bit address, and a simple master/slave relationship
 * exists at all times.
 *
 * The internal NS9215 GPIO/Multiplexer has two optional ports to switch 
 * the two I2C lines: a primary and alternate connection.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:I2C
 * @overview I2C
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the I2C is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary or 
 * alternate GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:I2C
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_I2C                          BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
#if (BSP_GPIO_MUX_I2C != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_I2C != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_I2C != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_I2C != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH)
#error Invalid configuration for BSP_GPIO_MUX_I2C
#endif




/*ad*
 * NS9215 has one Quadrature Decoder/Coder.  It is used to determine the direction
 * and rate of rotation of a wheel such as an electric motor or a control knob.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:QDC
 * @overview Quadrature Decoder/Coder
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the quadrature decoder/coder is multiplexed.
 * These signals can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:QDC
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_QDC                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_QDC != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_QDC != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_QDC != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_QDC
#endif





/*ad*
 * NS9215 can be configured to support a reset_done signal.  The RESET_DONE
 * signal is asserted when the processor comes out of reset.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:RESET_DONE
 * @overview RESET_DONE Signal
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the reset_done signal is multiplexed.
 * This signal can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary,
 * or alternate GPIO pin.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:RESET_DONE
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_RESET_DONE                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_RESET_DONE != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_RESET_DONE != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_RESET_DONE != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for RESET_OUT
#endif




/*ad*
 * The NS9215 can emulate the CS0 functionality of the NS7520.  
 * Two GPIO pins are used to generate the signals CS0OE_ and 
 * CS0WE_.  When this feature is enabled, the signals are used
 * to maximize the read access timing for external memory 
 * peripherals attached to CS0.  Refer to the Hardware
 * Reference manuals for the NS9210, NS9215, and NS7520 for
 * details.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:CS0
 * @overview Chip Select 0
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * This directive controls how the CS0 signals CS0OE_ and CS0WE_ are multiplexed.
 * These signals can stay internal (allowing GPIO or other special 
 * functions to multiplex) or can be routed to the primary GPIO pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:CS0
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_CS0                          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_CS0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_CS0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_CS0 
#endif




/*ad*
 * The NS9210 and NS9215 have 10 timers.  The GPIO MUX can be configured to supply
 * inputs for all 10 timers on the NS9215, but only for timers 5 - 9 on the NS9210.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @overview Timer
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/



/*ad*
 * This directive controls how the Timer 0 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_0_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_0_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_0_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 0 input
#endif
#if (BSP_GPIO_MUX_TIMER_0 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 0 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 0 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_0_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_0_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_0_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 0 output
#endif
#if (BSP_GPIO_MUX_TIMER_0_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 0 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 1 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_1_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_1_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_1_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 1 input
#endif
#if (BSP_GPIO_MUX_TIMER_1 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 5 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 1 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_1_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_1_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_1_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 1 output
#endif
#if (BSP_GPIO_MUX_TIMER_1_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 1 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 2 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_2_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_2_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_2_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 2 input
#endif
#if (BSP_GPIO_MUX_TIMER_2 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 2 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 2 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_2_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_2_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_2_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 2 output
#endif
#if (BSP_GPIO_MUX_TIMER_2_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 2 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 3 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_3_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_3_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_3_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 3 input
#endif
#if (BSP_GPIO_MUX_TIMER_3 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 3 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 3 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_3_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_3_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_3_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 3 output
#endif
#if (BSP_GPIO_MUX_TIMER_3_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 3 is not available on the alternate path
#endif



/*ad*
 * This directive controls how the Timer 4 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_4_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_4_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_4_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 4 input
#endif
#if (BSP_GPIO_MUX_TIMER_4 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 4 is not available on the alternate path
#endif



/*ad*
 * This directive controls how the Timer 4 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_4_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_4_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_4_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH)
#error Invalid configuration for timer 4 output
#endif
#if (BSP_GPIO_MUX_TIMER_4_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 4 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 5 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_5_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_5_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_5_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH)
#error Invalid configuration for timer 5 input
#endif
#if (BSP_GPIO_MUX_TIMER_5 == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 5 is not available on the alternate path
#endif



/*ad*
 * This directive controls how the Timer 5 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_5_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_5_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_5_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for timer 5 output
#endif
#if (BSP_GPIO_MUX_TIMER_5_OUTPUT == BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Timer 5 is not available on the alternate path
#endif


/*ad*
 * This directive controls how the Timer 6 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_6_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_6_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_6_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_6_INPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 6 input
#endif


/*ad*
 * This directive controls how the Timer 6 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_6_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_6_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_6_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_6_OUTPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 6 output
#endif


/*ad*
 * This directive controls how the Timer 7 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_7_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_7_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_7_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_7_INPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 7 input
#endif


/*ad*
 * This directive controls how the Timer 7 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_7_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_7_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_7_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_7_OUTPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 7 output
#endif


/*ad*
 * This directive controls how the Timer 8 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_8_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_8_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_8_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_8_INPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 8 input
#endif


/*ad*
 * This directive controls how the Timer 8 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_8_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_8_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_8_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_8_OUTPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 8 output
#endif


/*ad*
 * This directive controls how the Timer 9 input signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_9_INPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_9_INPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_9_INPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_9_INPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 9 input
#endif


/*ad*
 * This directive controls how the Timer 9 output signal is routed
 * through the GPIO multiplexer.  This signal can stay internal 
 * (allowing GPIO or other special functions to multiplex), or 
 * can be routed to the primary GPIO pin.
 *
 * Note this function has no alternate path available through 
 * the GPIO multiplexer
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Timer
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_TIMER_9_OUTPUT                        BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_TIMER_9_OUTPUT != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_TIMER_9_OUTPUT != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_TIMER_9_OUTPUT != BSP_GPIO_MUX_USE_ALTERNATE_PATH)
#error Invalid configuration for timer 9 output
#endif








/*ad*
 * The NS9215 contains 4 serial channels that can be multiplexed.  Only ports
 * B and D are fully supported on the Connect Core 9P 9215 jump start board.  
 * Ports A and C are available, but do not have H/W drivers or regular serial
 * port connectors.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface
 * @overview Serial
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * The NS9215 has 4 serial ports, 2 of which are fully supported on the
 * Connect Core 9P 9215 development board.
 * 
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface:SerialChannels
 * @overview Serial Channels
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/



/*ad*
 * This directive controls how Serial Port A is multiplexed.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins based on the particular serial configuration.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface:SerialChannels
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_SERIAL_2_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_4_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_6_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_8_WIRE_UART
*ad*/
#define BSP_GPIO_MUX_SERIAL_A                       BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_SERIAL_A != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_SERIAL_A != BSP_GPIO_MUX_SERIAL_2_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_A != BSP_GPIO_MUX_SERIAL_4_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_A != BSP_GPIO_MUX_SERIAL_6_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_A != BSP_GPIO_MUX_SERIAL_8_WIRE_UART) 
#error Invalid Serial Port A configuration
#endif



/*ad*
 * This directive controls how Serial Port B is multiplexed.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins based on the particular serial configuration.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface:SerialChannels
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_SERIAL_2_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_4_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_6_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_8_WIRE_UART
*ad*/
#define BSP_GPIO_MUX_SERIAL_B                          BSP_GPIO_MUX_SERIAL_8_WIRE_UART_USE_ALTERNATE_PATH
#if (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_2_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_4_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_6_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_8_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_2_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_4_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_6_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_B != BSP_GPIO_MUX_SERIAL_8_WIRE_UART_USE_ALTERNATE_PATH) 
#error Invalid Serial Port B configuration
#endif





/*ad*
 * This directive controls how Serial Port C is multiplexed.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins based on the particular serial configuration.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface:SerialChannels
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_SERIAL_2_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_4_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_6_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_8_WIRE_UART
*ad*/
#define BSP_GPIO_MUX_SERIAL_C                BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_SERIAL_C != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_SERIAL_C != BSP_GPIO_MUX_SERIAL_2_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_C != BSP_GPIO_MUX_SERIAL_4_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_C != BSP_GPIO_MUX_SERIAL_6_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_C != BSP_GPIO_MUX_SERIAL_8_WIRE_UART) 
#error Invalid Serial Port C configuration
#endif






/*ad*
 * This directive controls how Serial Port D is multiplexed.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins based on the particular serial configuration.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:SerialInterface:SerialChannels
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_SERIAL_2_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_4_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_6_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_8_WIRE_UART
 * @see @link BSP_GPIO_MUX_SERIAL_SPI_APP
*ad*/
#define BSP_GPIO_MUX_SERIAL_D                          BSP_GPIO_MUX_SERIAL_8_WIRE_UART_USE_ALTERNATE_PATH
#if (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_2_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_4_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_6_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_8_WIRE_UART) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_2_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_4_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_6_WIRE_UART_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_SERIAL_D != BSP_GPIO_MUX_SERIAL_8_WIRE_UART_USE_ALTERNATE_PATH) 
#error Invalid Serial Port D configuration
#endif


/*ad*
 * The NS9215 and NS9210 processors support the industry standard MII
 * Ethernet for communication with an Ethernet PHY.  Unless your 
 * application does not use Ethernet, you should configure the GPIO
 * pins to support the MII interface.  The NS9215 and NS9210 also allow
 * you to choose whether to support the TXER and RXER signals,
 * which are often not supported by lower cost PHYs.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Ethernet
 * @overview Ethernet
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/



/*ad*
 * This directive controls how Ethernet is configured.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins based on the particular Ethernet configuration.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Ethernet
 * @see @link BSP_GPIO_MUX_ETHERNET_MII
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
*ad*/

#define BSP_GPIO_MUX_ETHERNET_PHY_INTERFACE    BSP_GPIO_MUX_ETHERNET_MII
#if (BSP_GPIO_MUX_ETHERNET_PHY_INTERFACE != BSP_GPIO_MUX_ETHERNET_MII) && \
    (BSP_GPIO_MUX_ETHERNET_PHY_INTERFACE != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
#error Invalid Ethernet PHY Interface configuration
#endif

#if (BSP_GPIO_MUX_ETHERNET_PHY_INTERFACE != BSP_GPIO_MUX_INTERNAL_USE_ONLY)
/*ad*
 * This directive controls how the Ethernet PHY TXER signal is multiplexed.
 * This signal is often not implemented on certain low cost MII PHYs.
 *
 * The signals can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.
 *
 * The Primary path will route the internal Ethernet module to the external 
 * PHY.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Ethernet
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_ETHERNET_PHY_TXER                BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_ETHERNET_PHY_TXER != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_ETHERNET_PHY_TXER != BSP_GPIO_MUX_USE_PRIMARY_PATH)
#error Invalid Ethernet PHY TXER configuration
#endif

/*ad*
 * This directive controls how the Ethernet PHY RXER signal is multiplexed.
 * This signal is often not implemented on certain low cost MII PHYs.
 *
 * The signals can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * The Primary path will route the internal Ethernet module to the external 
 * PHY.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Ethernet
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_ETHERNET_PHY_RXER                BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_ETHERNET_PHY_RXER != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_ETHERNET_PHY_RXER != BSP_GPIO_MUX_USE_PRIMARY_PATH)
#error Invalid Ethernet PHY RXER configuration
#endif

#else /* if Ethernet interface disabled */
    #define BSP_GPIO_MUX_ETHERNET_PHY_TXER      BSP_GPIO_MUX_INTERNAL_USE_ONLY
    #define BSP_GPIO_MUX_ETHERNET_PHY_RXER      BSP_GPIO_MUX_INTERNAL_USE_ONLY
#endif




/*ad*
 * The NS9215 has two I/O co-processors.  These co-processors can each
 * support up to 8 general purpose I/O signals, CAN receive and transmit
 * signals, a 16-bit and a 24-bit data bus, and clocks and control signals
 * to support the data buses.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @overview IOP
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/


/*ad*
 * This directive controls how general I/O signal 0 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_0             BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_0 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_0 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_0
#endif



/*ad*
 * This directive controls how general I/O signal 1 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_1             BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_1 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_1 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_1
#endif



/*ad*
 * This directive controls how general I/O signal 2 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_2             BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_2 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_2 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_2
#endif


/*ad*
 * This directive controls how general I/O signal 3 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_3             BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_3 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_3 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_3
#endif


/*ad*
 * This directive controls how general I/O signal 4 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_4             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_4 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_4 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_4 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_4
#endif



/*ad*
 * This directive controls how general I/O signal 5 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_5             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_5 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_5 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_5 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_5
#endif



/*ad*
 * This directive controls how general I/O signal 6 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_6             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_6 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_6 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_6 != BSP_GPIO_MUX_USE_ALTERNATE_PATH)  
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_6
#endif


/*ad*
 * This directive controls how general I/O signal 7 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_GEN_IO_7             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_GEN_IO_7 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_7 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_GEN_IO_7 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_GEN_IO_7
#endif


/*ad*
 * This directive controls how control signal 0 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CTL_IO_0             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_0_CTL_IO_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CTL_IO_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CTL_IO_0
#endif


/*ad*
 * This directive controls how control signal 1 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CTL_IO_1             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_0_CTL_IO_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CTL_IO_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CTL_IO_1
#endif


/*ad*
 * This directive controls how control signal 2 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CTL_IO_2             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_CTL_IO_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CTL_IO_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CTL_IO_2
#endif


/*ad*
 * This directive controls how control signal 3 for I/O processor 0 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CTL_IO_3             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_CTL_IO_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CTL_IO_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CTL_IO_3
#endif


/*ad*
 * This directive controls how the CAN receive signal on I/O processor 0
 * is routed through the GPIO multiplexer
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CAN_RXD              BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_CAN_RXD != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CAN_RXD != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_CAN_RXD != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CAN_RXD
#endif


/*ad*
 * This directive controls how the CAN transmit signal on I/O processor 0
 * is routed through the GPIO multiplexer
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_CAN_TXD              BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_CAN_TXD != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_CAN_TXD != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_0_CAN_TXD != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_CAN_RXD
#endif


/*ad*
 * Configures a group of 16 GPIO pins to act as a 16-bit bus
 * for I/O processor 0.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_16_BIT_BUS           BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_16_BIT_BUS != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_16_BIT_BUS != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_16_BIT_BUS
#endif


/*ad*
 * Configures a group of 24 GPIO pins to act as a 16-bit bus
 * for I/O processor 0.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_24_BIT_BUS           BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_24_BIT_BUS != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_24_BIT_BUS != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_24_BIT_BUS
#endif


/*ad*
 * This directive controls how the input clock signal for I/O processor 0
 * is routed through the GPIO multiplexer.  This signal is used for bus
 * oriented operations.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_O_INPUT_CLOCK          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_INPUT_CLOCK != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_INPUT_CLOCK != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_INPUT_CLOCK
#endif


/*ad*
 * This directive controls how the output clock signal for I/O processor 0
 * is routed through the GPIO multiplexer.  This signal is used for bus
 * oriented operations.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_0_OUTPUT_CLOCK         BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_0_OUTPUT_CLOCK != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_0_OUTPUT_CLOCK != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_0_OUTPUT_CLOCK
#endif








/*ad*
 * This directive controls how general I/O signal 0 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_0             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_0 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_0 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_0
#endif



/*ad*
 * This directive controls how general I/O signal 1 for I/O processor 1
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_1             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_1 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_1 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_1
#endif



/*ad*
 * This directive controls how general I/O signal 2 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_2             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_2 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_2 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_2
#endif


/*ad*
 * This directive controls how general I/O signal 3 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
 * @see @link BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_3             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_3 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_3 != BSP_GPIO_MUX_USE_2ND_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_3
#endif



/*ad*
 * This directive controls how general I/O signal 4 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_4             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_4 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_4 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_4 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_4
#endif



/*ad*
 * This directive controls how general I/O signal 5 for I/O processor 1
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_5             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_5 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_5 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_5 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_5
#endif



/*ad*
 * This directive controls how general I/O signal 6 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_6             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_6 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_6 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_6 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_6
#endif


/*ad*
 * This directive controls how general I/O signal 7 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_GEN_IO_7             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_GEN_IO_7 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_7 != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_GEN_IO_7 != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_GEN_IO_7
#endif





/*ad*
 * This directive controls how control signal 0 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CTL_IO_0             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_CTL_IO_0 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CTL_IO_0 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CTL_IO_0
#endif


/*ad*
 * This directive controls how control signal 1 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CTL_IO_1             BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_CTL_IO_1 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CTL_IO_1 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CTL_IO_1
#endif


/*ad*
 * This directive controls how control signal 2 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CTL_IO_2             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_CTL_IO_2 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CTL_IO_2 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CTL_IO_2
#endif


/*ad*
 * This directive controls how control signal 3 for I/O processor 1 
 * is multiplexed.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CTL_IO_3             BSP_GPIO_MUX_USE_PRIMARY_PATH
#if (BSP_GPIO_MUX_IOP_1_CTL_IO_3 != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CTL_IO_3 != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CTL_IO_3
#endif


/*ad*
 * This directive controls how the CAN receive signal on I/O processor 1
 * is routed through the GPIO multiplexer
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CAN_RXD              BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_CAN_RXD != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CAN_RXD != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_CAN_RXD != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CAN_RXD
#endif


/*ad*
 * This directive controls how the CAN transmit signal on I/O processor 1
 * is routed through the GPIO multiplexer
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
 * @see @link BSP_GPIO_MUX_USE_ALTERNATE_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_CAN_TXD              BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_CAN_TXD != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_CAN_TXD != BSP_GPIO_MUX_USE_PRIMARY_PATH) && \
    (BSP_GPIO_MUX_IOP_1_CAN_TXD != BSP_GPIO_MUX_USE_ALTERNATE_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_CAN_RXD
#endif


/*ad*
 * Configures a group of 16 GPIO pins to act as a 16-bit bus
 * for I/O processor 1.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_16_BIT_BUS           BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_16_BIT_BUS != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_16_BIT_BUS != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_16_BIT_BUS
#endif


/*ad*
 * Configures a group of 24 GPIO pins to act as a 16-bit bus
 * for I/O processor 1.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_24_BIT_BUS           BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_24_BIT_BUS != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_24_BIT_BUS != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_24_BIT_BUS
#endif


/*ad*
 * This directive controls how the input clock signal for I/O processor 1
 * is routed through the GPIO multiplexer.  This signal is used for bus
 * oriented operations.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_INPUT_CLOCK          BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_INPUT_CLOCK != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_INPUT_CLOCK != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_INPUT_CLOCK
#endif


/*ad*
 * This directive controls how the output clock signal for I/O processor 1
 * is routed through the GPIO multiplexer.  This signal is used for bus
 * oriented operations.
 *
 * The signal can stay internal (allowing GPIO or other special functions 
 * to multiplex) or can be routed out to the GPIO pin.  
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:IOP
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_PRIMARY_PATH
*ad*/
#define BSP_GPIO_MUX_IOP_1_OUTPUT_CLOCK         BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_IOP_1_OUTPUT_CLOCK != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_IOP_1_OUTPUT_CLOCK != BSP_GPIO_MUX_USE_PRIMARY_PATH) 
#error Invalid configuration for BSP_GPIO_MUX_IOP_1_OUTPUT_CLOCK
#endif







/*ad*
 * Many hardware designs do not use the highest address bits.  The NS9215 and NS9210
 * allow you to reconfigure these pins so they can be used for other functions.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Memory
 * @overview High Level Address Bits
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/


/*ad*
 * This directive controls high order address memory.
 * The signals can stay internal (allowing GPIO or other 
 * special functions to multiplex) or can be routed to GPIO 
 * pins.
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Memory
 * @see @link BSP_GPIO_MUX_INTERNAL_USE_ONLY
 * @see @link BSP_GPIO_MUX_USE_ADDR_BIT_24_AND_LOWER
 * @see @link BSP_GPIO_MUX_USE_ADDR_BIT_25_AND_LOWER
 * @see @link BSP_GPIO_MUX_USE_ADDR_BIT_26_AND_LOWER
 * @see @link BSP_GPIO_MUX_USE_ADDR_BIT_27_AND_LOWER
*ad*/
#define BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING  BSP_GPIO_MUX_INTERNAL_USE_ONLY
#if (BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING != BSP_GPIO_MUX_INTERNAL_USE_ONLY) && \
    (BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING != BSP_GPIO_MUX_USE_ADDR_BIT_24_AND_LOWER) && \
    (BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING != BSP_GPIO_MUX_USE_ADDR_BIT_25_AND_LOWER) && \
    (BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING != BSP_GPIO_MUX_USE_ADDR_BIT_26_AND_LOWER) && \
    (BSP_GPIO_MUX_HIGH_ORDER_ADDRESSING != BSP_GPIO_MUX_USE_ADDR_BIT_27_AND_LOWER) 
#error Invalid High Order Memory Address configuration
#endif





/*ad*
 * Discrete signals can be controlled or sensed using a General Purpose 
 * Input/Output (GPIO) pin.  This pin is able to drive logic or read 
 * levels.  This functionality is available on all Signal Multiplexed pins.
 *
 * When a MUX pin is not configured (to a serial, or Timer, etc), then
 * the functionality defaults to GPIO.  In other words, to setup a pin
 * to have GPIO functionality, all of the associated multiplexer groups 
 * must be defined as BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * When the MUX pin is set to GPIO, three initial states are available:
 * one for receiver, one for a driver initially set to logic 0, and
 * another for a driver set to logic 1.
 *
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:GPIOInterface
 * @overview GPIO
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*
 * Altering the BSP_GPIO_INITIAL_STATE_PINx
 *
 * The following BSP_GPIO_INITIAL_STATE_PINx directives control the 
 * I/O configuration if and only if the MUX has not been configured
 * to some other available function.  
 *
 * When a MUX pin is not configured (to a serial, or Timer, etc), then
 * the functionality multiplexed is GPIO.  In other words, to setup a pin
 * to have GPIO functionality, all of the associated multiplexer groups 
 * must be defined as BSP_GPIO_MUX_INTERNAL_USE_ONLY.
 *
 * Addtionally, if the MUX is configured to use something other than the 
 * GPIO functionality, then the definitions below will be ignored on that 
 * particalur multiplexer pin.
 *
 * When the MUX pin is set to GPIO, three initial states are available:
 * one for receiver, one for a driver initially set to logic 0, and
 * another for a driver set to logic 1.
 *
 * @name GPIO_INITIAL_STATE_PINx "GPIO Initial Pin State"
 *
 * @note The safe setting for this is BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER,
 * which programs the pin to a receive configuration.
 *
 * @param BSP_GPIO_INITIAL_STATE_PIN0  "Initial state of GPIO pin 0" 
 * @param BSP_GPIO_INITIAL_STATE_PIN1  "Initial state of GPIO pin 1" 
 * @param BSP_GPIO_INITIAL_STATE_PIN2  "Initial state of GPIO pin 2" 
 * @param BSP_GPIO_INITIAL_STATE_PIN3  "Initial state of GPIO pin 3" 
 * @param BSP_GPIO_INITIAL_STATE_PIN4  "Initial state of GPIO pin 4" 
 * @param BSP_GPIO_INITIAL_STATE_PIN5  "Initial state of GPIO pin 5" 
 * @param BSP_GPIO_INITIAL_STATE_PIN6  "Initial state of GPIO pin 6" 
 * @param BSP_GPIO_INITIAL_STATE_PIN7  "Initial state of GPIO pin 7" 
 * @param BSP_GPIO_INITIAL_STATE_PIN8  "Initial state of GPIO pin 8" 
 * @param BSP_GPIO_INITIAL_STATE_PIN9  "Initial state of GPIO pin 9" 
 * @param BSP_GPIO_INITIAL_STATE_PIN10  "Initial state of GPIO pin 10" 
 * @param BSP_GPIO_INITIAL_STATE_PIN11  "Initial state of GPIO pin 11" 
 * @param BSP_GPIO_INITIAL_STATE_PIN12  "Initial state of GPIO pin 12" 
 * @param BSP_GPIO_INITIAL_STATE_PIN13  "Initial state of GPIO pin 13" 
 * @param BSP_GPIO_INITIAL_STATE_PIN14  "Initial state of GPIO pin 14" 
 * @param BSP_GPIO_INITIAL_STATE_PIN15  "Initial state of GPIO pin 15" 
 * @param BSP_GPIO_INITIAL_STATE_PIN16  "Initial state of GPIO pin 16" 
 * @param BSP_GPIO_INITIAL_STATE_PIN17  "Initial state of GPIO pin 17" 
 * @param BSP_GPIO_INITIAL_STATE_PIN18  "Initial state of GPIO pin 18" 
 * @param BSP_GPIO_INITIAL_STATE_PIN19  "Initial state of GPIO pin 19" 
 * @param BSP_GPIO_INITIAL_STATE_PIN20  "Initial state of GPIO pin 20" 
 * @param BSP_GPIO_INITIAL_STATE_PIN21  "Initial state of GPIO pin 21" 
 * @param BSP_GPIO_INITIAL_STATE_PIN22  "Initial state of GPIO pin 22" 
 * @param BSP_GPIO_INITIAL_STATE_PIN23  "Initial state of GPIO pin 23" 
 * @param BSP_GPIO_INITIAL_STATE_PIN24  "Initial state of GPIO pin 24" 
 * @param BSP_GPIO_INITIAL_STATE_PIN25  "Initial state of GPIO pin 25" 
 * @param BSP_GPIO_INITIAL_STATE_PIN26  "Initial state of GPIO pin 26" 
 * @param BSP_GPIO_INITIAL_STATE_PIN27  "Initial state of GPIO pin 27" 
 * @param BSP_GPIO_INITIAL_STATE_PIN28  "Initial state of GPIO pin 28" 
 * @param BSP_GPIO_INITIAL_STATE_PIN29  "Initial state of GPIO pin 29" 
 * @param BSP_GPIO_INITIAL_STATE_PIN30  "Initial state of GPIO pin 30" 
 * @param BSP_GPIO_INITIAL_STATE_PIN31  "Initial state of GPIO pin 31" 
 * @param BSP_GPIO_INITIAL_STATE_PIN32  "Initial state of GPIO pin 32" 
 * @param BSP_GPIO_INITIAL_STATE_PIN33  "Initial state of GPIO pin 33" 
 * @param BSP_GPIO_INITIAL_STATE_PIN34  "Initial state of GPIO pin 34" 
 * @param BSP_GPIO_INITIAL_STATE_PIN35  "Initial state of GPIO pin 35" 
 * @param BSP_GPIO_INITIAL_STATE_PIN36  "Initial state of GPIO pin 36" 
 * @param BSP_GPIO_INITIAL_STATE_PIN37  "Initial state of GPIO pin 37" 
 * @param BSP_GPIO_INITIAL_STATE_PIN38  "Initial state of GPIO pin 38" 
 * @param BSP_GPIO_INITIAL_STATE_PIN39  "Initial state of GPIO pin 39" 
 * @param BSP_GPIO_INITIAL_STATE_PIN40  "Initial state of GPIO pin 40" 
 * @param BSP_GPIO_INITIAL_STATE_PIN41  "Initial state of GPIO pin 41" 
 * @param BSP_GPIO_INITIAL_STATE_PIN42  "Initial state of GPIO pin 42" 
 * @param BSP_GPIO_INITIAL_STATE_PIN43  "Initial state of GPIO pin 43" 
 * @param BSP_GPIO_INITIAL_STATE_PIN44  "Initial state of GPIO pin 44" 
 * @param BSP_GPIO_INITIAL_STATE_PIN45  "Initial state of GPIO pin 45" 
 * @param BSP_GPIO_INITIAL_STATE_PIN46  "Initial state of GPIO pin 46" 
 * @param BSP_GPIO_INITIAL_STATE_PIN47  "Initial state of GPIO pin 47" 
 * @param BSP_GPIO_INITIAL_STATE_PIN48  "Initial state of GPIO pin 48" 
 * @param BSP_GPIO_INITIAL_STATE_PIN49  "Initial state of GPIO pin 49" 
 * @param BSP_GPIO_INITIAL_STATE_PIN50  "Initial state of GPIO pin 50" 
 * @param BSP_GPIO_INITIAL_STATE_PIN51  "Initial state of GPIO pin 51" 
 * @param BSP_GPIO_INITIAL_STATE_PIN52  "Initial state of GPIO pin 52" 
 * @param BSP_GPIO_INITIAL_STATE_PIN53  "Initial state of GPIO pin 53" 
 * @param BSP_GPIO_INITIAL_STATE_PIN54  "Initial state of GPIO pin 54" 
 * @param BSP_GPIO_INITIAL_STATE_PIN55  "Initial state of GPIO pin 55" 
 * @param BSP_GPIO_INITIAL_STATE_PIN56  "Initial state of GPIO pin 56" 
 * @param BSP_GPIO_INITIAL_STATE_PIN57  "Initial state of GPIO pin 57" 
 * @param BSP_GPIO_INITIAL_STATE_PIN58  "Initial state of GPIO pin 58" 
 * @param BSP_GPIO_INITIAL_STATE_PIN59  "Initial state of GPIO pin 59" 
 * @param BSP_GPIO_INITIAL_STATE_PIN60  "Initial state of GPIO pin 60" 
 * @param BSP_GPIO_INITIAL_STATE_PIN61  "Initial state of GPIO pin 61" 
 * @param BSP_GPIO_INITIAL_STATE_PIN62  "Initial state of GPIO pin 62" 
 * @param BSP_GPIO_INITIAL_STATE_PIN63  "Initial state of GPIO pin 63" 
 * @param BSP_GPIO_INITIAL_STATE_PIN64  "Initial state of GPIO pin 64" 
 * @param BSP_GPIO_INITIAL_STATE_PIN65  "Initial state of GPIO pin 65" 
 * @param BSP_GPIO_INITIAL_STATE_PIN66  "Initial state of GPIO pin 66" 
 * @param BSP_GPIO_INITIAL_STATE_PIN67  "Initial state of GPIO pin 67" 
 * @param BSP_GPIO_INITIAL_STATE_PIN68  "Initial state of GPIO pin 68" 
 * @param BSP_GPIO_INITIAL_STATE_PIN69  "Initial state of GPIO pin 69" 
 * @param BSP_GPIO_INITIAL_STATE_PIN70  "Initial state of GPIO pin 70" 
 * @param BSP_GPIO_INITIAL_STATE_PIN71  "Initial state of GPIO pin 71" 
 * @param BSP_GPIO_INITIAL_STATE_PIN72  "Initial state of GPIO pin 72" 
 * @param BSP_GPIO_INITIAL_STATE_PIN73  "Initial state of GPIO pin 73" 
 * @param BSP_GPIO_INITIAL_STATE_PIN74  "Initial state of GPIO pin 74" 
 * @param BSP_GPIO_INITIAL_STATE_PIN75  "Initial state of GPIO pin 75" 
 * @param BSP_GPIO_INITIAL_STATE_PIN76  "Initial state of GPIO pin 76" 
 * @param BSP_GPIO_INITIAL_STATE_PIN77  "Initial state of GPIO pin 77" 
 * @param BSP_GPIO_INITIAL_STATE_PIN78  "Initial state of GPIO pin 78" 
 * @param BSP_GPIO_INITIAL_STATE_PIN79  "Initial state of GPIO pin 79" 
 * @param BSP_GPIO_INITIAL_STATE_PIN80  "Initial state of GPIO pin 80" 
 * @param BSP_GPIO_INITIAL_STATE_PIN81  "Initial state of GPIO pin 81" 
 * @param BSP_GPIO_INITIAL_STATE_PIN82  "Initial state of GPIO pin 82" 
 * @param BSP_GPIO_INITIAL_STATE_PIN83  "Initial state of GPIO pin 83" 
 * @param BSP_GPIO_INITIAL_STATE_PIN84  "Initial state of GPIO pin 84" 
 * @param BSP_GPIO_INITIAL_STATE_PIN85  "Initial state of GPIO pin 85" 
 * @param BSP_GPIO_INITIAL_STATE_PIN86  "Initial state of GPIO pin 86" 
 * @param BSP_GPIO_INITIAL_STATE_PIN87  "Initial state of GPIO pin 87" 
 * @param BSP_GPIO_INITIAL_STATE_PIN88  "Initial state of GPIO pin 88" 
 * @param BSP_GPIO_INITIAL_STATE_PIN89  "Initial state of GPIO pin 89" 
 * @param BSP_GPIO_INITIAL_STATE_PIN90  "Initial state of GPIO pin 90" 
 * @param BSP_GPIO_INITIAL_STATE_PIN91  "Initial state of GPIO pin 91" 
 * @param BSP_GPIO_INITIAL_STATE_PIN92  "Initial state of GPIO pin 92" 
 * @param BSP_GPIO_INITIAL_STATE_PIN93  "Initial state of GPIO pin 93" 
 * @param BSP_GPIO_INITIAL_STATE_PIN94  "Initial state of GPIO pin 94" 
 * @param BSP_GPIO_INITIAL_STATE_PIN95  "Initial state of GPIO pin 95" 
 * @param BSP_GPIO_INITIAL_STATE_PIN96  "Initial state of GPIO pin 96" 
 * @param BSP_GPIO_INITIAL_STATE_PIN97  "Initial state of GPIO pin 97" 
 * @param BSP_GPIO_INITIAL_STATE_PIN98  "Initial state of GPIO pin 98" 
 * @param BSP_GPIO_INITIAL_STATE_PIN99  "Initial state of GPIO pin 99" 
 * @param BSP_GPIO_INITIAL_STATE_PIN100  "Initial state of GPIO pin 100" 
 * @param BSP_GPIO_INITIAL_STATE_PIN101  "Initial state of GPIO pin 101" 
 * @param BSP_GPIO_INITIAL_STATE_PIN102  "Initial state of GPIO pin 102" 
 * @param BSP_GPIO_INITIAL_STATE_PIN103  "Initial state of GPIO pin 103" 
 * @param BSP_GPIO_INITIAL_STATE_PINA24  "Initial state of GPIO pin A24" 
 * @param BSP_GPIO_INITIAL_STATE_PINA25  "Initial state of GPIO pin A25" 
 * @param BSP_GPIO_INITIAL_STATE_PINA26  "Initial state of GPIO pin A26" 
 * @param BSP_GPIO_INITIAL_STATE_PINA27  "Initial state of GPIO pin A27" 
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:GPIOInterface
 * @see @link BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC0
 * @see @link BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
 * @see @link BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
*ad*/
#define BSP_GPIO_INITIAL_STATE_PIN0                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN1                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN2                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN3                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN4                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN5                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN6                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN7                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN8                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN9                 BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN10                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN11                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN12                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN13                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN14                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN15                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN16                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN17                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN18                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN19                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN20                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN21                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN22                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN23                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN24                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN25                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN26                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN27                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN28                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN29                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN30                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN31                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN32                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN33                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN34                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN35                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN36                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN37                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN38                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN39                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN40                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN41                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN42                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN43                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN44                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN45                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN46                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN47                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN48                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN49                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN50                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN51                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN52                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN53                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN54                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN55                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN56                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN57                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN58                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN59                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN60                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN61                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN62                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN63                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN64                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN65                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN66                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN67                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN68                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN69                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN70                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN71                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN72                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN73                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN74                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN75                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN76                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN77                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN78                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN79                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN80                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER

/*
 * On this board, pins 81 and 84 are inputs for the push buttons, and pins 82 and 85
 * control the LEDs.  The LED is turned on by setting the GPIO pin to a 0.
 */
#define BSP_GPIO_INITIAL_STATE_PIN81                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN82                BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
#define BSP_GPIO_INITIAL_STATE_PIN83                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN84                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN85                BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1

#define BSP_GPIO_INITIAL_STATE_PIN86                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN87                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN88                BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
#define BSP_GPIO_INITIAL_STATE_PIN89                BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
/*
 * GPIO 90 is connected to the PHY.  It needs to be set high to take the PHY out of reset.
 */
#define BSP_GPIO_INITIAL_STATE_PIN90                BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
#define BSP_GPIO_INITIAL_STATE_PIN91                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN92                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN93                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN94                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN95                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN96                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN97                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN98                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN99                BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN100               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN101               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN102               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PIN103               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER

#define BSP_GPIO_INITIAL_STATE_PINA24               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PINA25               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PINA26               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
#define BSP_GPIO_INITIAL_STATE_PINA27               BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER




/*ad*
 * The GPIO pins on the NS9215 have internal pullup resisters.  These can be enabled 
 * or disabled for each pin.
 *
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Pullups
 * @overview GPIO
 * @include <gpio.h>
 * @library bsp
 * @platform all
*ad*/

/*ad*
 * These constant control the settings for the internal pullup resisters on each
 * GPIO pin.  Set a constant to TRUE to enable the pullup, or to FALSE to disable
 * the pullup.
 *
 * @name GPIO_PULLUP_PINx "GPIO Pullup Enable/Disable"
 *                 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN0  "Configures pullup on pin 0" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN1  "Configures pullup on pin 1" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN2  "Configures pullup on pin 2" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN3  "Configures pullup on pin 3" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN4  "Configures pullup on pin 4" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN5  "Configures pullup on pin 5" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN6  "Configures pullup on pin 6" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN7  "Configures pullup on pin 7" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN8  "Configures pullup on pin 8" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN9  "Configures pullup on pin 9" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN10  "Configures pullup on pin 10" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN11  "Configures pullup on pin 11" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN12  "Configures pullup on pin 12" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN13  "Configures pullup on pin 13" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN14  "Configures pullup on pin 14" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN15  "Configures pullup on pin 15" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN16  "Configures pullup on pin 16" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN17  "Configures pullup on pin 17" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN18  "Configures pullup on pin 18" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN19  "Configures pullup on pin 19" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN20  "Configures pullup on pin 20" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN21  "Configures pullup on pin 21" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN22  "Configures pullup on pin 22" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN23  "Configures pullup on pin 23" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN24  "Configures pullup on pin 24" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN25  "Configures pullup on pin 25" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN26  "Configures pullup on pin 26" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN27  "Configures pullup on pin 27" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN28  "Configures pullup on pin 28" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN29  "Configures pullup on pin 29" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN30  "Configures pullup on pin 30" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN31  "Configures pullup on pin 31" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN32  "Configures pullup on pin 32" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN33  "Configures pullup on pin 33" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN34  "Configures pullup on pin 34" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN35  "Configures pullup on pin 35" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN36  "Configures pullup on pin 36" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN37  "Configures pullup on pin 37" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN38  "Configures pullup on pin 38" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN39  "Configures pullup on pin 39" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN40  "Configures pullup on pin 40" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN41  "Configures pullup on pin 41" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN42  "Configures pullup on pin 42" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN43  "Configures pullup on pin 43" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN44  "Configures pullup on pin 44" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN45  "Configures pullup on pin 45" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN46  "Configures pullup on pin 46" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN47  "Configures pullup on pin 47" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN48  "Configures pullup on pin 48" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN49  "Configures pullup on pin 49" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN50  "Configures pullup on pin 50" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN51  "Configures pullup on pin 51" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN52  "Configures pullup on pin 52" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN53  "Configures pullup on pin 53" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN54  "Configures pullup on pin 54" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN55  "Configures pullup on pin 55" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN56  "Configures pullup on pin 56" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN57  "Configures pullup on pin 57" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN58  "Configures pullup on pin 58" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN59  "Configures pullup on pin 59" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN60  "Configures pullup on pin 60" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN61  "Configures pullup on pin 61" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN62  "Configures pullup on pin 62" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN63  "Configures pullup on pin 63" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN64  "Configures pullup on pin 64" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN65  "Configures pullup on pin 65" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN66  "Configures pullup on pin 66" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN67  "Configures pullup on pin 67" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN68  "Configures pullup on pin 68" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN69  "Configures pullup on pin 69" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN70  "Configures pullup on pin 70" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN71  "Configures pullup on pin 71" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN72  "Configures pullup on pin 72" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN73  "Configures pullup on pin 73" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN74  "Configures pullup on pin 74" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN75  "Configures pullup on pin 75" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN76  "Configures pullup on pin 76" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN77  "Configures pullup on pin 77" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN78  "Configures pullup on pin 78" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN79  "Configures pullup on pin 79" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN80  "Configures pullup on pin 80" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN81  "Configures pullup on pin 81" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN82  "Configures pullup on pin 82" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN83  "Configures pullup on pin 83" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN84  "Configures pullup on pin 84" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN85  "Configures pullup on pin 85" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN86  "Configures pullup on pin 86" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN87  "Configures pullup on pin 87" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN88  "Configures pullup on pin 88" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN89  "Configures pullup on pin 89" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN90  "Configures pullup on pin 90" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN91  "Configures pullup on pin 91" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN92  "Configures pullup on pin 92" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN93  "Configures pullup on pin 93" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN94  "Configures pullup on pin 94" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN95  "Configures pullup on pin 95" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN96  "Configures pullup on pin 96" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN97  "Configures pullup on pin 97" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN98  "Configures pullup on pin 98" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN99  "Configures pullup on pin 99" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN100  "Configures pullup on pin 100" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN101  "Configures pullup on pin 101" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN102  "Configures pullup on pin 102" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PIN103  "Configures pullup on pin 103" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PINA24  "Configures pullup on pin A24" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PINA25  "Configures pullup on pin A25" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PINA26  "Configures pullup on pin A26" 
 * @param BSP_GPIO_ENABLE_PULLUP_ON_PINA27  "Configures pullup on pin A27" 
 *
 * @since 7.3
 * @external
 * @category BSP:Device_Drivers:Mux_and_GPIO:GPIO9215:Pullups
 * @see @link BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC0
 * @see @link BSP_GPIO_INITIAL_STATE_OUTPUT_DRIVER_LOGIC1
 * @see @link BSP_GPIO_INITIAL_STATE_INPUT_RECEIVER
*ad*/
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN0                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN1                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN2                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN3                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN4                 FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN5                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN6                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN7                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN8                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN9                 TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN10                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN11                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN12                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN13                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN14                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN15                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN16                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN17                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN18                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN19                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN20                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN21                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN22                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN23                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN24                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN25                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN26                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN27                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN28                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN29                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN30                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN31                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN32                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN33                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN34                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN35                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN36                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN37                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN38                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN39                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN40                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN41                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN42                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN43                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN44                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN45                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN46                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN47                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN48                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN49                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN50                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN51                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN52                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN53                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN54                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN55                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN56                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN57                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN58                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN59                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN60                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN61                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN62                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN63                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN64                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN65                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN66                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN67                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN68                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN69                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN70                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN71                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN72                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN73                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN74                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN75                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN76                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN77                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN78                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN79                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN80                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN81                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN82                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN83                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN84                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN85                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN86                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN87                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN88                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN89                FALSE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN90                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN91                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN92                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN93                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN94                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN95                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN96                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN97                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN98                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN99                TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN100               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN101               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN102               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PIN103               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PINA24               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PINA25               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PINA26               TRUE
#define BSP_GPIO_ENABLE_PULLUP_ON_PINA27               TRUE



#ifdef DOTNETMF
#undef  BSP_GPIO_MUX_SERIAL_A                       
#define BSP_GPIO_MUX_SERIAL_A BSP_GPIO_MUX_SERIAL_4_WIRE_UART
#undef  BSP_GPIO_MUX_SERIAL_B                       
#define BSP_GPIO_MUX_SERIAL_B BSP_GPIO_MUX_SERIAL_4_WIRE_UART_USE_ALTERNATE_PATH
#undef  BSP_GPIO_MUX_SERIAL_C
#define BSP_GPIO_MUX_SERIAL_C BSP_GPIO_MUX_SERIAL_4_WIRE_UART
#undef  BSP_GPIO_MUX_SERIAL_D                       
#define BSP_GPIO_MUX_SERIAL_D BSP_GPIO_MUX_SERIAL_4_WIRE_UART_USE_ALTERNATE_PATH
#endif

#ifdef __cplusplus
}
#endif


#endif








