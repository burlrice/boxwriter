/*****************************************************************************
* Copyright (c) 2007 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/

#ifndef BSP_LOW_POWER_H
#define BSP_LOW_POWER_H

#include <Npttypes.h>
#include "bsp_defs.h"
#include <init_settings.h>
#include "mcreg_def.h"
#include "powersave.h"


#ifdef __cplusplus
extern "C"
{
#endif


/*ad*
 * Set this constant to TRUE to automatically enable low power mode.
 * Low power mode will start just before applicationStart is 
 * executed.  This will cause the kernel to reduce the CPU clock
 * rate whenever the thread scheduler is waiting for a thread
 * to become ready to execute.  The CPU clock resume normal speed
 * whenever any of the devices selected by BSP_LOW_POWER_MODE_TRIGGERS
 * needs to be serviced, or whenever a thread executes.  The CPU
 * clock will be lowered again when servicing is complete.
 *
 * You must add libtx_lp.a into the list of libraries in your application
 * makefile if you set BSP_LOW_POWER_MODE_ENABLE TRUE.  This
 * will cause the low power version of the NET+OS kernel to be linked
 * into your application.  This kernel runs slightly slower than
 * the standard version of the kernel.
 *
 * There is no reason to use low power mode unless your application
 * hardware has to conserve power (uses batteries).  Low power mode
 * may cause some Ethernet receive packets to be lost if the value of 
 * BSP_LOW_POWER_MODE_LOW_SPEED_DIVISOR is greater than 4.
 *
 * Low power mode is often refered to as catnap mode.
 *
 *
 * @note Low power mode interferes with JTAG debuggers.  It will cause
 *       the debugger to lose connection to the processor or to crash.
 *       For this reason, the BSP will NOT start low power mode if a
 *       JTAG debugger is detected, even if BSP_LOW_POWER_MODE_ENABLE
 *       is set.
 *       
 * @since 7.4
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LOW_POWER_MODE_TRIGGERS
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_DIVISOR
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_CPU_CLOCK_RATIO
 *
*ad*/
#define BSP_LOW_POWER_MODE_ENABLE       FALSE


/*ad*
 * This constant selects which devices can bring the CPU out of low power
 * mode.  Once low power mode starts, the kernel will lower the clock rate
 * of the CPU whenever there are no threads ready to execute.  The processor
 * automatically restores the CPU clock to full speed when any of the
 * devices indicated by this constant need to be serviced.  Low power mode
 * is resumed after the servicing is complete.
 *
 * Usually you should set BSP_LOW_POWER_MODE_TRIGGERS to NAPD_ANY_TRIGGER so that
 * full speed is restored whenever any device needs to be serviced.
 * 
 *
 * @since 7.4
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LOW_POWER_MODE_ENABLE
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_DIVISOR
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_CPU_CLOCK_RATIO
 *
*ad*/
#define BSP_LOW_POWER_MODE_TRIGGERS    NAPD_ANY_TRIGGER


/*ad*
 * This constant determines the divisor for the CPU clock when operating in
 * low power mode.  The BSP will start low power mode just before starting
 * the applicatino code if BSP_LOW_POWER_MODE_ENABLE is set to TRUE.  When the
 * system is idle, the CPU clock is divided down by this constant to 
 * conserve power.  This constant can be set to a value between 1 and 16 
 * on the NS9215 and NS9210 processors.
 *
 * @note    Setting this constant to more than 4 may cause some Ethernet 
 *          receive packets to be lost.
 * 
 *
 * @since 7.4
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LOW_POWER_MODE_ENABLE
 * @see @link BSP_LOW_POWER_MODE_TRIGGERS
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_CPU_CLOCK_RATIO
 *
*ad*/
#define BSP_LOW_POWER_MODE_LOW_SPEED_DIVISOR        (16)


/*ad*
 * This constant determines the ratio of the CPU clock to the AHB
 * clock when operating in low power mode.  On the NS9215 and NS9210
 * processors, the CPU clock can run at twice the rate of the AHB clock, 
 * or at the same rate as the AHB clock.  Set this constant to either
 * NAPD_CPU_CLOCK_IS_AHB or NAPD_CPU_CLOCK_IS_2XAHB.
 *
 *
 * @since 7.4
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LOW_POWER_MODE_ENABLE
 * @see @link BSP_LOW_POWER_MODE_TRIGGERS
 * @see @link BSP_LOW_POWER_MODE_LOW_SPEED_DIVISOR
 *
*ad*/
#define BSP_LOW_POWER_MODE_LOW_SPEED_CPU_CLOCK_RATIO    NAPD_CPU_CLOCK_IS_AHB

 


#ifdef __cplusplus
}
#endif


#endif /* BSP_SYS_H */

