/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_bldr.h This file defines BSP feature values for the bootloader
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_BLDR_H
#define BSP_BLDR_H

#include <Npttypes.h>
#include "bsp_defs.h"

#ifdef __cplusplus
extern "C"
{
#endif


/*ad*
 * Set this constant to TRUE to let the bootloader to download application
 * image from network only.
 *
 * @note     After the bootloader successfully downloads
 *           the application image, it reads the header of the image to determine if
 *           the image needs to be written to flash (or sflash) or stays in RAM. User
 *           needs to customize the field "WriteToFlash" in bootldr.dat in the platform 
 *           directory to suit their application.
 * 
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link blFlags
*ad*/
#define BSP_BOOTLOADER_BOOT_FROM_NETWORK_ONLY        FALSE


/*ad*
 * Bootloader flash recovery mode. The default is set to BSP_BOOTLOADER_TRY_ALL
 *
 * @since 6.3
 * @include 	<bsp.h>
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_BOOTLOADER_RECOVERY_METHOD  BSP_BOOTLOADER_TRY_ALL

#ifdef DOTNETMF
#undef  BSP_BOOTLOADER_RECOVERY_METHOD
#define BSP_BOOTLOADER_RECOVERY_METHOD BSP_BOOTLOADER_SERIAL_RECOVERY 
#endif

#ifdef __cplusplus
}
#endif

#endif /* BSP_BLDR_H */
