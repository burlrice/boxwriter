/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_sys.h This file defines BSP wide constants and data structures.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_SERIAL_H
#define BSP_SERIAL_H

#include <Npttypes.h>
#include "bsp_defs.h"


#ifdef __cplusplus
extern "C"
{
#endif

/*
 * This constant determines whether the serial driver uses the internal fast
 * interrupt and TIMER2 to provide better servicing characteristics of the 
 * FIFO at high baud rates.  It is recommended that this be set to TRUE if
 * the port will be run at 115200 baud or higher.
 *
 * @since 6.0
 * @external
 * @include <bsp.h>
 * @category BSP:BSP_Configuration:ARM7
 * @index BSP_SERIAL_FAST_INTERRUPT
 */
#define BSP_SERIAL_FAST_INTERRUPT FALSE 

/*ad*
 * This constant controls which serial driver is built in
 * the BSP for serial port 1.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 * @see @link SerialPortConfigurations
*ad*/
#define BSP_SERIAL_PORT_1       BSP_SERIAL_NO_DRIVER

/*ad*
 * This constant controls which serial driver is built in
 * the BSP for serial port 2.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 * @see @link SerialPortConfigurations
*ad*/
#define BSP_SERIAL_PORT_2       BSP_SERIAL_UART_DRIVER

/*ad*
 * This constant controls which serial driver is built in
 * the BSP for serial port 3.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 * @see @link SerialPortConfigurations
*ad*/
#define BSP_SERIAL_PORT_3       BSP_SERIAL_NO_DRIVER

/*ad*
 * This constant controls which serial driver is built in
 * the BSP for serial port 4.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 * @see @link SerialPortConfigurations
*ad*/
#define BSP_SERIAL_PORT_4       BSP_SERIAL_UART_DRIVER 

/*ad*
 * This constant controls which serial port the simple serial driver
 * is connected too.
 *
 * @since 6.1
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_SIMPLE_SERIAL_PORT          BSP_SIMPLE_SERIAL_ON_PORTD

#ifdef DOTNETMF
#undef  BSP_SERIAL_PORT_1
#define BSP_SERIAL_PORT_1       BSP_SERIAL_UART_DRIVER
#undef  BSP_SERIAL_PORT_3
#define BSP_SERIAL_PORT_3       BSP_SERIAL_UART_DRIVER
#endif


#ifdef __cplusplus
}
#endif


#endif /* BSP_SERIAL_H */
