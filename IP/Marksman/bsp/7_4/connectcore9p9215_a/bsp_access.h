/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_sys.h This file defines BSP wide constants and data structures.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#include "sysAccess.h"

/*ad*
 * Set this constant to TRUE to enable ldap v3 authentication in
 * the system security access. LDAP will be used to verify
 * uername and password. If ldap fails to authenticated, the system
 * security access database will be used to validate the username
 * and password.
 *
 * @since 7.4
 * @external
 * @include  <bsp_access.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LDAP_SYSTEM_ACCESS_PORT 
 * @see @link BSP_LDAP_SYSTEM_ACCESS_IP_ADDR 
 * @see @link SystemSWServices:SystemAccess
*ad*/
#define BSP_LDAP_SYSTEM_ACCESS         FALSE

/*ad*
 * Define the IP address for the LDAP server which
 * will be used to verify the username and password
 * if BSP_LDAP_SYSTEM_ACCESS is set to TRUE.
 *
 * @since 7.4
 * @external
 * @include  <bsp_access.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LDAP_SYSTEM_ACCESS 
 * @see @link BSP_LDAP_SYSTEM_ACCESS_PORT 
*ad*/
#define BSP_LDAP_SYSTEM_ACCESS_IP_ADDR  "10.9.8.7"

/*ad*
 * Define the port which will be used to contact the LDAP server
 * if BSP_LDAP_SYSTEM_ACCESS is set to TRUE.
 *
 * @since 7.4
 * @external
 * @include  <bsp_access.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LDAP_SYSTEM_ACCESS 
 * @see @link BSP_LDAP_SYSTEM_ACCESS_IP_ADDR 
 *
*ad*/
#define BSP_LDAP_SYSTEM_ACCESS_PORT     389

/*ad*
 * Define the access level
 *
 * @since 7.4
 * @external
 * @include  <bsp_access.h>
 * @category BSP:BSP_Configuration
 *
 * @see @link BSP_LDAP_SYSTEM_ACCESS 
 * @see @link BSP_LDAP_SYSTEM_ACCESS_IP_ADDR 
 *
*ad*/
#define BSP_LDAP_SYSTEM_ACCESS_LEVEL     NASYSACC_LEVEL_ROOT
