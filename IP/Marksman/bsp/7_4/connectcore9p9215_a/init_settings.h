/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * init_settings.h This file defines constant used for init.arm and for spi 
 * boot header's data structures for NS9750.
 * 
 *
 */


#ifndef init_settings_h
#define init_settings_h

/* 
 * Define the size of the stack for each mode
 * NOTE: Both user and system mode use the same stack space.
 */
#define USER_STK_SZ         1024
#define SYS_STK_SZ         USER_STK_SZ
#define FIQ_STK_SZ         256
#define IRQ_STK_SZ         8192
#define SVC_STK_SZ         1024 /* 256    for initialization only */
#define ABT_STK_SZ         256 
#define UND_STK_SZ         1024 /* 256 */

 
/*ad*
 * Chip Select 4 Dynamic Memory Configuration
 *
 * CS4 is the first available chip select for dynamic
 * memory. Define here what type 
 * of memory you have on chip select four (CS4).
 *
 * When we are in debugger, CS4 is setup by the debug script;
 * when boot from flash, the code in INIT.arm will be able to
 * automatic memory sizing SDRAM, and CS_WIDTH must be set to 32-bits 
 * to enable automatic memory sizing; when boot from SPI boot engine, 
 * CS4's parameters must be specified definitively. Sensing size on CS4 
 * is not possible, so CS_WIDTH MUST BE SET TO 16-BITS ON THIS PLATFORM 
 * TO MATCH THE PHYSICAL SDRAM CONFIGURATION.
 *
 * @param MEM_TYPE_x8			8 bit memory part
 * @param MEM_TYPE_x16			16 bit memory part
 * @param MEM_TYPE_x32			32 bit memory part
 * @param MEM_SIZE_16Mb			16 Mega bits
 * @param MEM_SIZE_64Mb			64 Mega bits
 * @param MEM_SIZE_128Mb		128 Mega bits
 * @param MEM_SIZE_256Mb		256 Mega bits
 * @param MEM_SIZE_512Mb		512 Mega bits
 * @param MEM_WIDTH_16			Data width defines for 16 bit memory
 * @param MEM_WIDTH_32			Data width defines for 32 bit memory
 * @param MEM_POWER_LEVEL_NORMAL  Normal-power SDRAM
 * @param MEM_POWER_LEVEL_LOW     Low-power SDRAM
 * @param CS_MEM_TYPE    16-bit memory parts on Connect Core 9P9215
 * @param CS_MEM_SIZE    We have an 8-Mbyte part on the Connect Core 9P9215, this 
 *                       is actually determined by the automatic memory sizing though
 * @param CS_WIDTH       Must be set to 32-bits to enable automatic memory sizing
 * @param CS_POWERLEVEL  Connect Core 9P9215 uses normal power SDRAM
 * @param CS_tAPR				data loaded in Dynamic Memory last Data Out to Active Time Register in Memory Controller
 * @param CS_tDAL				data loaded in Dynamic Memory Data-in to Active Command Time Register in Memory Controller 
 * @param CS_tWR				data loaded in Dynamic Memory Write Recovery Time Register in Memory Controller 
 * @param CS_tRC				data loaded in Dynamic Memory Active to Active Command Period Register in Memory Controller
 * @param CS_tRFC				data loaded in Dynamic Memory Auto Refresh Period and Auto Refresh to Active Command Period Register in Memory Controller 
 * @param CS_tRRD				data loaded in Dynamic Memory Active Bank A to Active Bank B Time Register in Memory Controller
 * @param CS_tMRD				data loaded in Dynamic Memory Load Mode Register to Active Command Time Register in Memory Controller
 * @param  CS_DYNAMICRASCAS     data loaded in Dynamic Memory RAS and CAS Delay Register 0 in Memory Controller
 * @param MPMCConfig			data loaded in Configuration Register in Memory Controller
 * @param MPMCDynamicRefresh	data loaded in Dynamic Memory Refresh Timer Register in Memory Controller
 * @param MPMCDynamicReadConfig data loaded in Dynamic Memory Read Confoguration Register in Memory Controller
 * @param MPMCDynamicRP			data loaded in Dynamic Memory Precharge Command Period Register in Memory Controller
 * @param MPMCDynamicRAS		data loaded in Dynamic Memory Active to Precharge Command Period Register in Memory Controller
 * @param MPMCDynamicSREX       data loaded in Dynamic Memory Self Refreash Exit Time Register in Memory Controller
 * @param MPMCDynamicXSR		data loaded in Dynamic Memory Exit Self Refresh to Active Command Register in Memory Controller
 * @param FIRSTSECTORRAM		Defines for the reads which occur to initialize the SDRAM
 *                              chips, this corresponds to the mode register which is on the
 *                              SDRAM part (not a NS9750 register), these correspond to 
 *                              CAS setting above, see the technical manual for the SDRAM
 *                               part for a description of this register
 * @param SECONDSECTORRAM      Same as above
 * @param THIRDSECTORRAM       Same as above
 * @param FOURTHSECTORRAM      Same as above
 *
 *
 * @category  BSP:BootLoader:SPIBootLoader
 * @external
 * @since 6.2
 * @name NACS4DynamicMemoryConfig  "Chip Select 4 Dynamic Memory Configuration"
 *
*ad*/
#define MEM_TYPE_x8			0x0 
#define MEM_TYPE_x16		0x1  
#define MEM_TYPE_x32		0x2   
#define MEM_SIZE_16Mb		0x0
#define MEM_SIZE_64Mb		0x1
#define MEM_SIZE_128Mb		0x2
#define MEM_SIZE_256Mb		0x3
#define MEM_SIZE_512Mb		0x4
#define MEM_WIDTH_16		0x0
#define MEM_WIDTH_32		0x1
#define MEM_POWER_LEVEL_NORMAL  0x0
#define MEM_POWER_LEVEL_LOW     0x1
#define CS_MEM_TYPE    MEM_TYPE_x16
#define CS_MEM_SIZE    MEM_SIZE_64Mb
#define CS_WIDTH       MEM_WIDTH_32
#define CS_POWERLEVEL  MEM_POWER_LEVEL_NORMAL
#define CS_tAPR  0x0
#define CS_tDAL  0x4
#define CS_tWR   0x1
#define CS_tRC   0x5
#define CS_tRFC  0x5
#define CS_tRRD  0x1
#define CS_tMRD  0x1
#define  CS_DYNAMICRASCAS        0x202
#define MPMCConfig  0x1
#define MPMCDynamicRefresh  0x49
#define MPMCDynamicReadConfig  0x1
#define MPMCDynamicRP  0x1
#define MPMCDynamicRAS  0x3
#define MPMCDynamicSREX  0x5
#define MPMCDynamicXSR  0x5
#define FIRSTSECTORRAM       0x00011800
#define SECONDSECTORRAM      0x10011800
#define THIRDSECTORRAM       0x20011800
#define FOURTHSECTORRAM      0x30011800

/*ad*
 * Defines the interface data rate for the boot-over-SPI operation after the initial 16-bytes. 
 * A data rate of about 375 Kbps fetches the 16-byte header. The data rate for the bytes after
 * the first 16 byte is determined by the PLL clock rate and the divisor.
 * See the SPI Clock Generation register for more details.
 * 
 * Example:
 * The PLL clock is 224870400 Hz, with divisor be 0x10, the data rate is 224870400/16, which is
 * 14054400 Hz. Make sure your sflash part does support this data rate.
 *
 * Refer to the documentation "NS9215 Hardware Manual" on SPI/SPI-EEPROM Boot Logic section 
 *
 * @category  BSP:BootLoader:SPIBootLoader
 * @external
 * @since 7.3
*ad*/
#define SPIBOOT_DIVISOR  0x20

/*ad*
 * A "1" indicates the external device supports high-speed read operation. Serial FLASH devices 
 * operating above 20MHz generally support this feature. Make sure your sflash part does support 
 * higher data rate, and make sure changing the divior above accordingly..
 *
 * Refer to the documentation "NS9215 Hardware Manual" on SPI/SPI-EEPROM Boot Logic section 
 *
 * @category  BSP:BootLoader:SPIBootLoader
 * @external
 * @since 7.3
*ad*/
#define SPIBOOT_HS_READ  0x0

/* 
 * The following define specifies the SDRAM initialization
 * delay, this must last for at least two refresh cycles, which
 * is 64 AHB clock cycles.  We need this delay to give the SDRAM 
 * time to stabilize. 
 */
#define CS_WAIT_TIME  64 


#define USB_HOST    5
#define USB_DEVICE  7 

/* 
 * Define whether the device is going to be configured as
 * a host or device 
 */
#define USB_CONFIG  USB_DEVICE

#endif /* #ifndef init_settings_h */


