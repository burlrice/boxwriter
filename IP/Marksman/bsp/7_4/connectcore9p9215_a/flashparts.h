/*****************************************************************************
* Copyright (c) 2005 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/

/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29DL323D Top 
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 * 
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29DL323DT FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29DL323D Bottom 
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29DL323DTB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV128M
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV128M FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV160B
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV160B TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV160BB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV160BB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV160T
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV160T TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV320DB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV320DB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV641MH
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV641MH FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV641DH
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV641 TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV800BB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV800BB TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29LV800BT
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29LV800BT FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29f800BB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29F800BB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AM29F800BT
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AM29F800BT FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * AMD AT49BV1614A
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AT49BV1614A FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Atmel AT49BV8011
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AT49BV8011 FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Atmel AT49BV8011T
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_AT49BV8011T FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Sharp LH28F800SG
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_LH28F800SG FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Numonyx M29W640GL
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W640GL TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29DW641F
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29DW641F TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W160B
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W160B FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W160DB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W160DB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W160T
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W160T FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W320DB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W320DB   FALSE

/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W320EB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W320EB   TRUE

/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29DW323DB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29DW323DB FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * ST Micro M29W800AB
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_M29W800AB TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Fujitsu Microelectronics MBM29LV160B
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_MBM29LV160B FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Fujitsu Microelectronics MBM29LV160T
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_MBM29LV160T FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Fujitsu Microelectronics MBM29LV800BE
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_MBM29LV800BE TRUE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Macronix MX28F4000
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_MX28F4000 FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Silicon Storage Technology SST28SF040
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_SST28SF040 FALSE


/*ad*
 * Set this constant to TRUE to compile in support for the
 * Silicon Storage Technology SST39VF800
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_SST39VF800 FALSE

/*ad*
 * Set this constant to TRUE to compile in support for the
 * Spansion S29GL128P
 * flash part.  Set this constant to FALSE to reduce the memory
 * requirements of the flash driver if you do not need to 
 * support this flash part.
 *
 * You set this constant by editing the file flashparts.h in 
 * the platform directory.
 *
 * @external
 * @category BSP:Device_Drivers:Flash_Memory
 * @since 7.0
*ad*/
#define NAFLASH_WANT_TO_SUPPORT_S29GL128P TRUE
