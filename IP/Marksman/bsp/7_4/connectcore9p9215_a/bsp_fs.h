
/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_fs.h This file defines BSP feature variable settings for the file system.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_FS_H
#define BSP_FS_H

#include <Npttypes.h>
#include "bsp_defs.h"
#include <fs.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * Set this constant to TRUE to include the native file system in C library.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_FILESYSTEM_FOR_CLIBRARY          TRUE

/*ad*
 * When the BSP creates a native file system volume, this constant specifies the
 * percentage of the maximum number of inode blocks that can be allocated to
 * store inodes for a volume.  This constant allows specifying the upper limit of
 * the number of blocks reserved to store inodes.  Valid values are from 1 to 100.
 * Refer to NAFSinit_volume_cb for more information.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NAFSinit_volume_cb
*ad*/
#define BSP_NATIVE_FS_INODE_BLOCK_LIMIT              100

/*ad*
 * When the BSP creates a native file system volume, this constant specifies the
 * maximum number of open directories that the file system will track.  A directory
 * is considered open if there are open files in the directory.  Valid values are
 * from 1 to 64.  Refer to NAFSinit_volume_cb for more information.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NAFSinit_volume_cb
*ad*/
#define BSP_NATIVE_FS_MAX_OPEN_DIRS                  20

/*ad*
 * When the BSP creates a native file system volume, this constant specifies the
 * the maximum number of open files per directory that the file system will track.  
 * Valid values are from 1 to 64.  Refer to NAFSinit_volume_cb for more information.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NAFSinit_volume_cb
*ad*/
#define BSP_NATIVE_FS_MAX_OPEN_FILES_PER_DIR         20

/*ad*
 * When the BSP creates a native file system volume, this constant specifies the
 * block size used for the volume.  Valid values are:
 *
 * <list type = "unordered">
 *      <item> NAFS_BLOCK_SIZE_512 </item>
 *      <item> NAFS_BLOCK_SIZE_1K </item>
 *      <item> NAFS_BLOCK_SIZE_2K </item>
 *      <item> NAFS_BLOCK_SIZE_4K </item>
 * </list>
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_NATIVE_FS_BLOCK_SIZE                     NAFS_BLOCK_SIZE_512

/*ad*
 * When the BSP creates the native file system RAM volume, this constant specifies the
 * size of the RAM volume in bytes.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_NATIVE_FS_RAM0_VOLUME_SIZE               256 * 1024

/*ad*
 * When the BSP creates the native file system Flash volume, this constant specifies the
 * advanced options to use.  Valid values are the bitwise ORing of the following:
 *
 * <list type = "unordered">
 *      <item> NAFS_MOST_DIRTY_SECTOR </item>
 *      <item> NAFS_RANDOM_DIRTY_SECTOR </item>
 *      <item> NAFS_TRACK_SECTOR_ERASES </item>
 *      <item> NAFS_BACKGROUND_COMPACTING </item>
 * </list>
 *
 * Refer to NAFSinit_volume_cb for more information.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NAFSinit_volume_cb
*ad*/
#define BSP_NATIVE_FS_FLASH0_OPTIONS                 NAFS_MOST_DIRTY_SECTOR | NAFS_TRACK_SECTOR_ERASES

/*ad*
 * If the BSP_NATIVE_FS_FLASH0_OPTIONS constant includes NAFS_BACKGROUND_COMPACTING, then this
 * constant specifies the percentage of erased blocks in a flash sector to gain in order
 * to trigger the sector compacting process.  Valid values are from 1 to 100.  Refer to
 * NAFSinit_volume_cb for more information.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NAFSinit_volume_cb
*ad*/
#define BSP_NATIVE_FS_FLASH0_COMPACTING_THRESHOLD    100

/*ad*
 * Set this constant to TRUE to include the FAT file system in C library.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_INCLUDE_FAT_FILESYSTEM_FOR_CLIBRARY          TRUE

/*ad*
 * When BSP_YAFFS_DRIVER_ENABLE is set to TRUE, the yaffs nand flash
 * file system driver is built into the bsp. The flash partitioning is
 * applied in the yaffscfg2k.c file.
 *
 * @since 7.2
 * @include     <bsp.h>
 * @external
 * @category BSP:BSP_Configuration
 *ad*/

#define	BSP_YAFFS_DRIVER_ENABLE	FALSE

#ifdef __cplusplus
}
#endif


#endif /* BSP_FS_H */

