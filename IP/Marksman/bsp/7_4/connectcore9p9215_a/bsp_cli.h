/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp.h This file defines BSP wide constants, data structures and prototypes.
 * Please refer to the BSP porting guide for a description of this file.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_CLI_H
#define BSP_CLI_H

#include <Npttypes.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * Builds the CLI as part of the BSP library when set to TRUE.
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all                     
 * @library  libbsp
 * @external
 *
*ad*/
#define BSP_CLI_INCLUDE_IN_BSP  TRUE


/*ad*
 * Builds Telnet capability as part of the CLI when set to TRUE.
 * libtelnsvr.a must be linked in with this option. 
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_INCLUDE_IN_BSP
 * @see @link BSP_CLI_ENABLE
*ad*/
#define BSP_CLI_TELNET_ENABLE   TRUE


/*ad*
 * Builds serial capability as part of the CLI when set to TRUE.
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_INCLUDE_IN_BSP
 * @see @link BSP_CLI_ENABLE
*ad*/
#define BSP_CLI_SERIAL_ENABLE   FALSE


/*ad*
 * Starts the CLI before applicationStart() is called from the ROOT task.
 * The CLI api function @link naCliOpen cannot be called from 
 * the application if set to TRUE. A maximum of five Telnet sessions is permitted
 * if the Telnet server is enabled.
 *
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_TELNET_ENABLE
 * @see @link BSP_CLI_SERIAL_ENABLE
*ad*/
#define BSP_CLI_ENABLE  TRUE


/*ad*
 * When BSP_CLI_ENABLE is set to TRUE, this macros determines the
 * Telnet server port.
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_TELNET_ENABLE
*ad*/
#define BSP_CLI_TELNET_PORT     23 

/*ad*
 * When BSP_CLI_ENABLE is set to TRUE, this macros determines the
 * com port on which the CLI will be accessed. Make sure 
 * this port is not in conflict with printf's, which is usually
 * "/com/0".  
 *
 * @category SystemSWServices:CLI
 * @since 7.0
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_SERIAL_ENABLE
*ad*/
#define BSP_CLI_COM_PORT        "/com/1"

/*ad*
 * When BSP_CLI_ENABLE is set to TRUE, this macro determines if
 * a SSH server will be created for the CLI.  Use the 
 * BSP_CLI_SSH_KEY_FILENAME and BSP_CLI_SSH_KEY_PASSWORD macros
 * to set the SSH key.
 *
 * @category SystemSWServices:CLI
 * @since 7.3
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_INCLUDE_IN_BSP
 * @see @link BSP_CLI_ENABLE
 * @see @link BSP_CLI_SSH_KEY_FILENAME
 * @see @link BSP_CLI_SSH_KEY_PASSWORD
*ad*/
#define BSP_CLI_SSH_ENABLE          FALSE

/*ad*
 * When BSP_CLI_SSH_ENABLE is set to TRUE, this macro determines the
 * name of the file that contains the host key for SSH.
 *
 * @category SystemSWServices:CLI
 * @since 7.3
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_SSH_ENABLE
*ad*/
#define BSP_CLI_SSH_KEY_FILENAME    "FLASH0/svrkey.pem"

/*ad*
 * When BSP_CLI_SSH_ENABLE is set to TRUE, this macro determines the
 * password associated with the SSH host key.
 *
 * @category SystemSWServices:CLI
 * @since 7.3
 * @include "cliapi.h"
 * @platform all
 * @library  libbsp
 * @external
 *
 * @see @link BSP_CLI_SSH_ENABLE
*ad*/
#define BSP_CLI_SSH_KEY_PASSWORD    "Digi sslsvr key"


#ifdef DOTNETMF
#undef  BSP_CLI_INCLUDE_IN_BSP
#define BSP_CLI_INCLUDE_IN_BSP  TRUE
#undef  BSP_CLI_SERIAL_ENABLE
#define BSP_CLI_SERIAL_ENABLE   FALSE
#undef  BSP_CLI_ENABLE
#define BSP_CLI_ENABLE          TRUE
#endif

#ifdef __cplusplus
}
#endif


#endif /* BSP_CLI_H */

