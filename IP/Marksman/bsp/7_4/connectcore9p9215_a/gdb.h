/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 */

#ifndef _GDB_H
#define _GDB_H

// #include <bsp.h>


/*
 * Bootloader menu configuration selections. This selections allow
 * bootloader to run or debug an image if no selection is made
 * within ~5 second. 
 * 
 * @since 6.3
 * @external
 * @name bootloader_menu_selection    "Bootloader  Menu Selections"
 * @category BSP:BSP_Configuration
 *
 * @param NA_GDB_BOOTLOADER_NONE                Waiting for valid key selection.
 *
 * @param NA_GDB_BOOTLOADER_RUN_IMAGE           Loading and running an image from flash onto RAM.
 *
 * @param NA_GDB_BOOTLOADER_LOAD_DEBUG_IMAGE    Loading debug image over TFTP
 *
 * @param NA_GDB_BOOTLOADER_RECOVER_TFTP        Recover an image from TFTP (currently not supported).
 * @param NA_GDB_BOOTLOADER_RECOVER_SERIAL      Recover an image form SERIAL (currently not supported).
 *
 */
#define NA_GDB_BOOTLOADER_NONE             0
#define NA_GDB_BOOTLOADER_RUN_IMAGE        1
#define NA_GDB_BOOTLOADER_LOAD_DEBUG_IMAGE 2
#define NA_GDB_BOOTLOADER_RECOVER_TFTP     3
#define NA_GDB_BOOTLOADER_RECOVER_SERIAL   4

/*
 * Bootloader menu default configuration.  
 *
 * The menu allows user to load and run an image from flash onto on RAM or to load a debug
 * image over tftp onto RAM.
 * 
 * This configuration specifies which bootloader is going to perform if no 
 * selection is made with ~5 seconds.
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration
 *
 */
#define NA_GDB_BOOTLOADER_START        NA_GDB_BOOTLOADER_LOAD_DEBUG_IMAGE

/*
 * Bootloader default debug image name.
 *
 * This is used to specify the name of the debug image that
 * will be retrieved over TFTP server.
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration
 *
 */
#define NA_GDB_TFTP_IMAGE          "image.bin"

#endif /* _GDB_H */


