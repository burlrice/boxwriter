/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_net.h This file defines BSP feature variables for network configuration
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_NET_H
#define BSP_NET_H

#include <Npttypes.h>


#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * Set this constant to TRUE to enable IP packet forwarding.
 * When this feature is enabled, the TCP/IP stack will retransmit packets
 * received on one network interface
 * out another if the packets are addressed to a device that is reachable
 * from the other interface.  You must enable this feature to allow the TCP/IP
 * stack to route packets from one network to another.
 * Disable this feature to improve security if it is not needed.
 *
 * @since 7.0
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *ad*/
#define BSP_ENABLE_IP_FORWARDING        FALSE


/*ad*
 * Set this constant to TRUE to add the PPP devices into the TCP/IP
 * stack's table of devices.  Set it to FALSE if you do not intend
 * to use PPP.
 *
 * @category BSP:BSP_Configuration
 * @since 6.0
 *
 * @external
 * @see @link Networking:PPP
*ad*/
#define BSP_WANT_PPP                        FALSE

/*ad*
 * Set this constant to TRUE to add the ethernet device into the TCP/IP
 * stack's table of devices.  Set it to FALSE if you do not intend
 * to use ethernet.
 *
 * @category BSP:BSP_Configuration
 * @since 6.0
 *
 * @external
*ad*/
#define BSP_WANT_ETHERNET              TRUE 


/*ad*
 * Set this constant to TRUE to add the 802.11 wireless device into the TCP/IP
 * stack's table of devices.  Set it to FALSE if you do not intend
 * to use wireless. You must set the interrupt line for your wireless adaptor
 * in pci.c in your platform directory.
 *
 * @category BSP:Device_Drivers:wireless
 * @since 6.3
 * @note For cardbus cards, make sure SW10-5 is on, for PCI cards, SW10-5 is off.
 *
 * @external
*ad*/
#define BSP_WANT_WIRELESS               FALSE


/*ad*
 * Set this constant to TRUE to add the start the Wireless Protect Access (WPA)
 * application from the BSP.  Set it to FALSE if you do not intend to use WPA, 
 * or will start it later.  BSP_WANT_WIRELESS must be TRUE to use WPA.
 *
 * @category BSP:Device_Drivers:wireless
 * @since 6.3
 *
 * @external
*ad*/
#define BSP_WANT_WPA	               FALSE


/*ad*
 * This constant determines how the Ethernet link is monitored.
 * Set BSP_USE_PHY_INTERRUPT to TRUE to use PHY interupt to monitor the Ethernet link.
 * Set BSP_USE_PHY_INTERRUPT to FALSE to use 500ms ThreadX timer to monitor the link.
 *
 * BSP_USE_PHY_INTERRUPT should be set to TRUE only on platforms that support PHY interupt.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver
*ad*/
#define BSP_USE_PHY_INTERRUPT   FALSE

/*ad*
 * This constant sets increased MII drive strength for LXT971A PHY.
 *
 * A higher Media Independent Interface (MII) drive strength may be desired in some designs 
 * to drive signals over longer PCB trace lengths, or over high-capacitive loads, through 
 * multiple vias, or through a connector. 
 *
 * BSP_INCREASED_MII_DRIVE_STRENGTH must be set to TRUE for NS9750 development board.
 *
 * @since 6.1
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver
*ad*/
#define BSP_INCREASED_MII_DRIVE_STRENGTH   TRUE

/*ad*
 * This constant determines whether the BSP waits for the stack to be configured before
 * starting the application by calling the applicationStart() function.  Previous versions
 * of NET+OS always waited for the stack to be configured.
 *
 * Your application should not use any network resources until the stack has been
 * configured by setting an IP address on at least one interface.  You can use the
 * @link NAGetIPConfigureState function to determine if an IP address has
 * been assigned to an interface.
 *
 * Set BSP_WAIT_FOR_IP_CONFIG to TRUE to cause the BSP to wait for an IP address to be
 * configured on at least one interface before calling applicationStart().  Also see
 * the @link BSP_AUTOIP_DELAY symbol to hold off start up when Auto IP is the only 
 * configured IP address.
 *
 * Set BSP_WAIT_FOR_IP_CONFIG to FALSE to call applicationStart() without waiting for
 * an IP address to be assigned.
 *
 * @since 6.1
 * @external
 * @include 	"bsp.h"
 * @see @link NAGetIPConfigureState 
 * @see @link BSP_AUTOIP_DELAY
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_WAIT_FOR_IP_CONFIG  TRUE



/*ad*
 * Set this constant to the number of seconds the BSP should delay calling 
 * applicationStart when the only IP version 4 address available was derived from Auto-IP.
 *
 * NET+OS uses two methods for acquiring IP version 4 addresses from the network:  
 * DHCP and Auto-IP.  Addresses acquired with DHCP are better than addresses acquired
 * with Auto-IP because packets with Auto-IP addresses will be discarded by routers.
 * Auto-IP is used mainly on very simple networks with no routers, or with Ethernet 
 * cross cable connections between two devices.
 *
 * NET+OS will use the DHCP address by default when it has both a DHCP address and an 
 * Auto-IP address.  However, if your application opens a socket when there is only 
 * an Auto-IP address, then that socket will be bound to the Auto-IP address, 
 * even if a DHCP address is acquired later.  The application will not be able to use 
 * the socket to communicate with a device located across a router.
 *
 * You can disable Auto-IP by updating the IAM configuration settings stored in NVRAM.
 *
 * If you use both DHCP and Auto-IP, then you may want wait for a DHCP address before
 * letting your application start.  Some DHCP servers are slow to respond.  Set
 * BSP_AUTOIP_DELAY to the number of seconds NET+OS should wait for DHCP to acquire
 * an IP address after Auto-IP has an address.  Set this constant to 0 if NET+OS
 * should immediately call applicationStart as soon as any IP version 4 address is
 * available.  Set the constant to a large value if a DHCP address is prefered.  
 * Disable Auto-IP entirely if you do not want to use Auto-IP addresses.
 *
 * If both Ethernet and wireless interfaces are present, then NET+OS will wait for
 * at least one IPv4 address, which was not acquired via Auto-IP, to be available on 
 * one of them. 
 *
 * @since 7.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_AUTOIP_DELAY        (30)


/*ad*
 * This constant enables IAM's initial IPv4 address conflict detection(ACD) for static addresses.
 * Initial ACD is performed by sending @link BSP_DEFAULT_ETH_NUM_ADDR_PROBES ARP probes at  
 * @link BSP_DEFAULT_ETH_PROBE_INTERVAL_MS intervals. An ARP probe is an ARP request with source protocol
 * address equal to zero. IAM always performs initial address conflict detection
 * for DHCP, Auto IP. Stateless auto-configured IPv6 addresses are tested for initial conflicts using
 * duplicate address detection(DAD), which is always enabled as well. 
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_ENABLE_ADDR_CONFLICT_DETECTION          TRUE

/*ad*
 * This constant enables IAM's ongoing IPv4 address conflict detection(ACD) after an interface has been
 * configured with a static or DHCP IPv4 address. Ongoing ACD is performed by checking for
 * for ARP requests, or replies coming from any interface, not originated by this host, 
 * whose ARP sender addresses are the same as the configured IPv4 address. IAM's ongoing ACD does
 * not send ARP probes to check for conflicts. An ARP probe is an ARP request with source protocol
 * address equal to zero. Ongoing ACD is always performed for Auto IP addresses. Stateless auto-configured
 * IPv6 addresses are tested for ongoing conflicts using duplicate address detection(DAD), which is
 * always enabled as well. 
 *
 * @since 7.0
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_ENABLE_ONGOING_ADDR_CONFLICT_DETECTION  FALSE


/*ad*
 * Define this constant to TRUE for IPv4 ACD to use the constant values, 
 * defined in RFC 5227. Otherwise IPv4 ACD constants are set to the values,
 * used in previous releases for backward compatibility.
 *
 * @since 7.5
 * @external
 * @include "bsp_net.h"
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_ENABLE_RFC_5227_SUPPORT	FALSE

#if BSP_ENABLE_RFC_5227_SUPPORT == TRUE

#define BSP_DEFAULT_ETH_NUM_ADDR_PROBES             3
#define BSP_DEFAULT_ETH_PROBE_WAIT_MS				1000
#define BSP_DEFAULT_ETH_PROBE_INTERVAL_MS           1000
#define BSP_DEFAULT_ETH_PROBE_INTERVAL_MAX_MS       2000
#define BSP_DEFAULT_ETH_NUM_ADDR_ANNOUNCEMENTS      2
#define BSP_DEFAULT_ETH_ANNOUNCEME_WAIT_MS			2000
#define BSP_DEFAULT_ETH_ANNOUNCE_INTERVAL_MS        2000
#define BSP_MAX_ADDR_CONFLICTS                      10
#define BSP_DEFAULT_ETH_RATE_LIMIT_INTERVAL_MS		60000
#define BSP_DEFAULT_ETH_DEFEND_INTERVAL_MS			10000

#else

/*ad*
 * This constant specifies the default number of ARP address probes, sent by IAM to detect IP address 
 * conflict on Ethernet and wireless interfaces. This constant is used for static, DCHP, and Auto IP
 * IPv4 addresses, however, @link BSP_ENABLE_ADDR_CONFLICT_DETECTION must be set to TRUE for static 
 * addresses to use it. When this constant is set to zero, four ARP probes are sent by default.  
 *
 * An ARP probe is an ARP request with source protocol address equal to zero.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_NUM_ADDR_PROBES             4

/*ad*
 * This constant specifies the interval, in milliseconds, between ARP address probes, sent by IAM to detect
 * initial IPv4 address conflicts on Ethernet and wireless interfaces. This constant is used for static 
 * DCHP, and  Auto IP IPv4 addresses, however, @link BSP_ENABLE_ADDR_CONFLICT_DETECTION must be set to TRUE for static 
 * addresses to use it. For Auto IP this constant is used as the minimum interval before the repeated ARP probe, 
 * according to RFC 3927. @link BSP_DEFAULT_ETH_PROBE_INTERVAL_MAX_MS 
 * specifies the maximum interval. 
 *
 * An ARP probe is an ARP request with source protocol address equal to zero.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_PROBE_INTERVAL_MS           200

/*ad*
 * This constant specifies the maximum interval, in milliseconds, before the repeated ARP address probe, 
 * sent by IAM to detect initial IPv4 address conflicts on Ethernet and wireless interfaces. 
 * @link BSP_DEFAULT_ETH_PROBE_INTERVAL_MS specifies the minimum interval.
 * This constant is used for and Auto IP IPv4 addresses, according to RFC 3927.
 *
 * An ARP probe is an ARP request with source protocol address equal to zero.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_PROBE_INTERVAL_MAX_MS		(BSP_DEFAULT_ETH_PROBE_INTERVAL_MS * 2)

/*ad*
 * This constant specifies the initial random delay, in milliseconds, before IAM sends the first ARP probe 
 * to detect initial IPv4 address conflicts on Ethernet and wireless interfaces.
 * This constant is used for and Auto IP IPv4 addresses, according to RFC 3927.
 *
 * An ARP probe is an ARP request with source protocol address equal to zero.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_PROBE_WAIT_MS				BSP_DEFAULT_ETH_PROBE_INTERVAL_MS

/*ad*
 * This constant specifies the default number of ARP address announcements, sent by IAM after 
 * an IPv4 address has been configured on an interface. This constant is used for static,
 * DHCP, and Auto IP addresses. 
 *
 * An ARP announcement is an ARP request with both source and destination protocol address 
 * equal the interface IP address.
 * 
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_NUM_ADDR_ANNOUNCEMENTS      2

/*ad*
 * This constant specifies the delay, in milliseconds, before IAM sends the first ARP address announcement, 
 * after an IPv4 address has been configured. This constant is used for and Auto IP IPv4 addresses, 
 * according to RFC 3927.
 *
 * An ARP announcement is an ARP request with both source and destination protocol address 
 * equal the interface IP address.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_ANNOUNCEME_WAIT_MS			2000

/*ad*
 * This constant specifies the interval, in milliseconds, between ARP address announcements, sent 
 * by IAM after an IPv4 address has been configured. This constant is used for static, DHCP, and Auto IP
 * addresses.
 *
 * An ARP announcement is an ARP request with both source and destination protocol address 
 * equal the interface IP address.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_DEFAULT_ETH_ANNOUNCE_INTERVAL_MS        2000

/*ad*
 * This constant specifies the number of IPv4 address conflicts before rate limiting. 
 * This constant is used for and Auto IP IPv4 addresses, according to RFC 3927.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/
#define BSP_MAX_ADDR_CONFLICTS                      10

/*ad*
 * This constant specifies the delay, in milliseconds, delay between successive attempts to aquire the  
 * IP address, after the number of IP address conflicts reached @link BSP_MAX_ADDR_CONFLICTS.
 * This constant is used for and Auto IP IPv4 addresses, according to RFC 3927.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/		
#define BSP_DEFAULT_ETH_RATE_LIMIT_INTERVAL_MS		60000

/*ad*
 * This constant specifies the minimum interval between defensive ARPS in milliseconds.
 * At present IAM does not defend an IP address and this constant is not used.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:IPv4ACD
*ad*/		
#define BSP_DEFAULT_ETH_DEFEND_INTERVAL_MS			10000
#endif		


/*ad*
 * Set this constant to TRUE to enable support for other protocols besides
 * TCP/IP.
 *
 * If this constant is set to FALSE, then the Ethernet driver will pass up
 * all received packets to the TCP/IP stack.  This is the recommended setting
 * for all TCP/IP applications.
 *
 * Set this constant to TRUE if your application needs to support other 
 * protocols.  This will enable code in the Ethernet driver that examines the
 * protocol ID of each received packet to determine which protocol it is 
 * addressed to.  The Ethernet driver will send packets to the protocol stack
 * that has been registered to receive them with @link NAEthAddPacketType.
 * TCP/IP is automatically registered to receive IP, ARP, and RARP packets.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver
 * @see @link NAEthAddPacketType
 * @see @link BSP_WANT_2ND_ETHERNET_CHECKSUM
*ad*/
#define BSP_ENABLE_ETHERNET_PACKET_TYPES        FALSE

/*ad*
 * Set this constant to TRUE to force the Ethernet driver to perform a second
 * CRC checksum check of incoming packets.  
 *
 * Under very rare conditions, the NET50 and NS7520 chips will corrupt data in
 * Ethernet receive packets.  This error is not detected by the Ethernet H/W.
 * See the errata on these parts for more information.  Set BSP_WANT_2ND_ETHERNET_CHECKSUM
 * to TRUE to have the Ethernet driver check received packets by performing a second 
 * CRC check.  The driver will discard packets that fail the CRC check.
 *
 * Note that UDP and TCP both perform their own checksums on received data, so this
 * check is NOT necessary for TCP/IP based applications.
 *
 * This constant is only valid for NET50 and NS7520 platforms.  It is ignored on all
 * others.
 *
 * If @link BSP_ENABLE_ETHERNET_PACKET_TYPES and BSP_WANT_2ND_ETHERNET_CHECKSUM are 
 * both set, the Ethernet driver will determine whether it should checksum packets 
 * by determining which protocol the packet is addressed to and then examining the 
 * want2ndCrcCheck field of the @link ethPacketType structure passed to @link 
 * NAEthAddPacketType when the packet type and protocol were registered.  
 * If @link BSP_ENABLE_ETHERNET_PACKET_TYPES is not set, but BSP_WANT_2ND_ETHERNET_CHECKSUM
 * is, then the Ethernet driver will perform a CRC checksum on all received packets.
 *
 * @note    There is a performance penalty when this feature is enabled.
 *          You do not need to enable this feature if your application only uses
 *          TCP/IP since TCP and UDP both perform checksums of incoming packets.
 * 
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver
 * @see @link NAEthAddPacketType
 * @see @link BSP_ENABLE_ETHERNET_PACKET_TYPES
*ad*/
#define BSP_WANT_2ND_ETHERNET_CHECKSUM       FALSE

/*ad*
 * Set this constant to TRUE to queue received Ethernet TCP/IP packets and 
 * pass them up to the TCP/IP stack in a lower priority thread. The priority of
 * the receive queue thread is determined by the @link BSP_ETHERNET_RECV_QUEUE_THREAD_PRIORITY.
 *
 * This mode is recommended when another protocol stack is running in parallel 
 * with TCP/IP and the bypass packets need higher priority over TCP/IP packets.
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver:EthernetBypass
 * @see @link NAEthRegisterCustomPacketFn
 * @see @link NAEthAddPacketType
*ad*/
#define BSP_USE_ETHERNET_RECV_QUEUE  FALSE

/*ad*
 * This constant defines the priority of the Ethernet receive queue thread,
 * if @link BSP_USE_ETHERNET_RECV_QUEUE is set to TRUE. 
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver:EthernetBypass
*ad*/
#define BSP_ETHERNET_RECV_QUEUE_THREAD_PRIORITY  9

/*ad*
 * Set this constant to TRUE to enable the Ethernet transmit bypass of the TCP/IP stack. 
 * Bypass packets are sent using the @link naEthBypassTransmit routine. 
 *
 * To avoid dependancy on TCP/IP stack recources for bypass packets, different queues are used
 * for bypass and TCP/IP packets. Total number of transmit DMA buffers is split to be used by
 * the Ethernet bypass and the TCP/IP stack. 
 *
 * The @link BSP_ETHERNET_BYPASS_SEND_BUFFERS number of DMA buffers is used for bypass. 
 * The rest of the DMA buffers - 1 are used for TCP/IP. 
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver:EthernetBypass
*ad*/
#define BSP_ENABLE_ETHERNET_BYPASS            FALSE

/*ad*
 * The number of Ethernet transmit DMA buffer descriptors, allocated for bypass packets,
 * if @link BSP_ENABLE_ETHERNET_BYPASS is enabled.
 *
 * @since 6.3
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver:EthernetBypass
 * @see @link naEthBypassTransmit
*ad*/
#define BSP_ETHERNET_BYPASS_SEND_BUFFERS       54

/*ad*
 * Set this constant to TRUE to enable the ability to filter packets directly 
 * in the Ethernet receive interrupt.
 *
 * The @link BSP_ENABLE_ETHERNET_BYPASS does not need to be enabled to use 
 * a receive interrupt filter, but must be enabled to send packets directly from
 * a receive interrupt filter.
 *
 * @since 7.5
 * @external
 * @category BSP:BSP_Configuration:EthernetDriver:EthernetBypass
 * @see @link naEthRegisterInterruptFilterFn
*ad*/
#define BSP_ENABLE_ETHERNET_INTERRUPT_FILTER	FALSE

/*ad*
 * ENABLE_ADDP_SERVER is used to activate the ADDP server.
 * 
 * Set ENABLE_ADDP_SERVER to FALSE to disable the ADDP server.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/
#define ENABLE_ADDP_SERVER TRUE

#if (ENABLE_ADDP_SERVER == TRUE) && (BSP_WANT_WIRELESS == FALSE) && (BSP_WANT_ETHERNET == FALSE)
#error ADDP Server can be used only when Ethernet or wireless is enabled
#endif

/*ad*
 * ADDP_HARDWARE_REV Is the hardware revision of the device.
 * 
 * The value should be modified by the user to represent the hardware revision
 * for this device.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/

#define ADDP_HARDWARE_REV	1

#if	0
		/* move this to addpconf_api.h when all bsp are updated */
/*ad*
 * This structure defines the internals of the GUID data structure.
 *
 * @since 6.2
 * @external
 * @category Networking:ADDPConfiguration
 * @param Data1	Specifies the first 8 hexadecimal digits of the GUID. 
 * @param Data2	Specifies the first group of 4 hexadecimal digits of the GUID. 
 * @param Data3	Specifies the second group of 4 hexadecimal digits of the GUID. 
 * @param Data4	Array of eight elements. The first two elements contain the
 * third group of 4 hexadecimal digits of the GUID. The remaining six elements
 * contain the final 12 hexadecimal digits of the GUID. 
 *
*ad*/
typedef struct _GUID{
   unsigned long	Data1;
   unsigned short	Data2;
   unsigned short	Data3;
   unsigned char	Data4[8];
} GUID, UUID;
#endif

/*ad*
 * ADDP_OEM_GUID Is the unique GUID assigned to this user.  This value is used
 * by ADDP to communicate with devices made by this user.  Currently set to 
 * Digi International GUID.
 * 
 * This value should be modified by the user to reflect their own GUID.
 * The GUID value is broken up based on the GUID structure defined above.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/

#define ADDP_OEM_GUID  {0xbf6db409, 0xc83d, 0x44a3, {0xa3, 0x6d, 0x21, 0x79, 0x7d, 0x2f, 0x73, 0xf9}}

/*ad*
 * ADDP_COOKIE Is the magic cookie.  This value is used by ADDP to communicate
 * with devices made by this user.  Currently set to DVKT.
 * 
 * This value should match the value defined in the ADDP client library.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/
#define ADDP_COOKIE	0x44564B54

/*ad*
 * ADDP_HARDWARE_NAME Is the name for the hardware.  
 * 
 * This value should be modified by the user to reflect their hardware name.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/

#define ADDP_HARDWARE_NAME  "Connect Core 9P 9215 A"

/*ad*
 * ADDP_FEP_REV Is the FEP revision value.
 * 
 * The value should be modified by the user to represent the FEP revision for 
 * this application.
 *
 * @external
 * @category Networking:ADDPConfiguration
 * @since 6.2
 *ad*/

#define ADDP_FEP_REV		"Unknown"

#ifdef DOTNETMF
#define BSP_DHCP_STARTUP_TIMEOUT 30
#undef  ENABLE_ADDP_SERVER
#define ENABLE_ADDP_SERVER       FALSE
#endif


#ifdef __cplusplus
}
#endif


#endif /* BSP_NET_H */

