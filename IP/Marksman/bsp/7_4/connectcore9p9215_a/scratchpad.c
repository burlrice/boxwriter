/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 */
#include <string.h>
#include "Npttypes.h"
#include "scratchpad.h"

/* location of the Battery backed NvRAM */
#define RTC_BACKEDUP_RAM_START         0x900600C0
#define RTC_BACKEDUP_RAM_STOP          0x90060100

extern NaStatus naRtcBatteryBackedRam_Init(void * context);
extern NaStatus naRtcBatteryBackedRam_Read(void * context, unsigned int offset, 
                                            unsigned char * buffer, unsigned int bytesToRead);
extern NaStatus naRtcBatteryBackedRam_Write(void * context, unsigned int offset, 
                                            const unsigned char * buffer, unsigned int bytesToWrite);
/* Scratchpad table that will be initialized via naScratchpadInit */

NaScratchpad_t naScratchpadTable[] = {
    {
    /* .busType = */      NA_SCRATCHPAD_ON_CHIP, 
    /* .location = */     {{0, RTC_BACKEDUP_RAM_START}}, 
    /* .context = */      &naScratchpadTable[0], 
    /* .size = */         (RTC_BACKEDUP_RAM_STOP - RTC_BACKEDUP_RAM_START), 
    /* .init = */         (NaScratchpadInit)naRtcBatteryBackedRam_Init, 
    /* .read = */         (NaScratchpadRead)naRtcBatteryBackedRam_Read, 
    /* .write = */        (NaScratchpadWrite)naRtcBatteryBackedRam_Write
    }
};
/* Size of the scratchpad table */
const unsigned int naScratchpadTableSize = asizeof(naScratchpadTable);
