/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *  Description.
 *
 *  This file contains code that sets up the GPIO driven LEDS.  THIS CODE MUST
 *  BE CUSTOMIZED FOR YOUR PARTICULAR HARDWARE.  The version of the file that
 *  is shipped with the NET+OS BSP is written to support NET+OS development
 *  boards.  Your hardware will be different and the code in this file
 *  must be customized to support it.
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  =======================================================
 *
 *
 */

#include <stdlib.h>
#include <Npttypes.h>
#include <narmled.h>
#include <gpio_def.h>
#include <gpio.h>
#include "io_ctrl_mod.h"


/*
 * Bit definitions for LEDs on the NET+OS development board.  Unless your
 * board happens to have the LEDs on the same pins, you will have to change
 * these for your board.
 */
#define GREEN_LED_GPIO_PIN          (89)
#define YELLOW_LED_GPIO_PIN         (88)
#define USER1_LED_GPIO_PIN          (82)
#define USER2_LED_GPIO_PIN          (85)

/*
 * The NET+OS LED driver uses values in the table NALedTable
 * to determine how to turn LEDs on and off.  See the BSP
 * Reference guide for information on how to modify this
 * table for your custom hardware.  
 *
 * This table sets up the LEDs on the development board.  Pin
 * C1 is connected to the green LED, and pin C2 is connected
 * to the yellow LED.  Setting a pin HIGH turns the LED OFF,
 * and setting it LOW turns the LED ON.
 *
 * The LED driver has to be able to work even when RAM is not
 * setup.  It is the only way to get status information out
 * during initialization.  Therefore, this table should be
 * declared as a constant so that it is stored in ROM.  If you
 * need to support multiple boards that have the LEDs connected
 * to different pins, then use #ifdef statements to create
 * different tables for different boards.
 */
const NALedTableType NALedTable[] = 
{
    {                       /* setup for the yellow LED*/
        (WORD32*)NA_GPIO_CONTROL_getAddress(GREEN_LED_GPIO_PIN),
        0,
        ~(1 << NA_GPIO_CONTROL_getSubindex(GREEN_LED_GPIO_PIN)),
        (1 << NA_GPIO_CONTROL_getSubindex(GREEN_LED_GPIO_PIN)),
        0xffffffff,
        TRUE
    },
    {                       /* setup for the green LED*/
        (WORD32*)NA_GPIO_CONTROL_getAddress(YELLOW_LED_GPIO_PIN),
        0,
        ~(1 << NA_GPIO_CONTROL_getSubindex(YELLOW_LED_GPIO_PIN)),
        (1 << NA_GPIO_CONTROL_getSubindex(YELLOW_LED_GPIO_PIN)),
        0xffffffff,
        TRUE
    },
    {                       /* No ethernet LED  */
        (WORD32*)NA_GPIO_CONTROL_getAddress(YELLOW_LED_GPIO_PIN),
        0,
        ~(1 << NA_GPIO_CONTROL_getSubindex(YELLOW_LED_GPIO_PIN)),
        (1 << NA_GPIO_CONTROL_getSubindex(YELLOW_LED_GPIO_PIN)),
        0xffffffff,
        FALSE
    },
    {                       /* setup for the yellow LED*/
        (WORD32*)NA_GPIO_CONTROL_getAddress(USER1_LED_GPIO_PIN),
        0,
        ~(1 << NA_GPIO_CONTROL_getSubindex(USER1_LED_GPIO_PIN)),
        (1 << NA_GPIO_CONTROL_getSubindex(USER1_LED_GPIO_PIN)),
        0xffffffff,
        TRUE
    },
    {                       /* setup for the green LED*/
        (WORD32*)NA_GPIO_CONTROL_getAddress(USER2_LED_GPIO_PIN),
        0,
        ~(1 << NA_GPIO_CONTROL_getSubindex(USER2_LED_GPIO_PIN)),
        (1 << NA_GPIO_CONTROL_getSubindex(USER2_LED_GPIO_PIN)),
        0xffffffff,
        TRUE
    }
};




/*
 * This constant should be set to the number of LEDs supported by
 * the H/W by customizeSetupLedTable().  The LED driver uses it 
 * to determine the size of the table.
 */
const int NA_LED_COUNT = sizeof NALedTable / sizeof(NALedTableType);
