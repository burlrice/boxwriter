/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 * Description.
 * =======================================================================
 * These routines are as the BSP is initializing.
 * Please note also that the routine is called from nccInit(), which is also
 * called from the bootloader. So DON'T use any routines that are in the C
 * library.
 */

/*
 * Do not attempt to trace registers before the C library is
 * setup.  The global variables used by the trace library will
 * be in an undefined state until after the C library has
 * initilized the data segement.
 */
#ifdef NA_REG_TRACE_ENABLED
    #ifdef _REG_DEF_H
        #error Trace code compiled into init code.
    #endif
    #undef NA_REG_TRACE_ENABLED
#endif

#include <stdlib.h>
#include <string.h>
#include "Npttypes.h"
#include "nareset.h"
#include "io_ctrl_mod.h"



/*
 *
 *  Function: void customizeReadPowerOnButtons(unsigned char *initData)
 *
 *  Description:
 *
 *      This function stores the values of any buttons or switches that the user may
 *      be holding down at power up to RAM.  It is called during the initial ROM
 *      startup after the GPIO ports and RAM have been setup.  
 *
 *      This version of the function has been written for the NET+OS development
 *      board.  It reads the GEN_ID field of the System Status Register, and the 
 *      values of the GPIO ports and stores them to RAM.  Generally, switches 
 *      and buttons will be connected to GPIO pins, so this function may be
 *      usable "as is" for your application.
 *
 *  Notes:
 *
 *      THIS CODE MUST BE CUSTOMIZED FOR YOUR BOARD.  The implementation
 *      below is for NET+OS development boards only.
 *
 *      This function will be called by ncc_init() before RAM has been
 *      configured.  The stack will be located on internal RAM in the
 *      NET+ARM.  You can only use stack variables.  Do not use global
 *      or static variables.
 *
 *  Parameters:
 *
 *      chipType        set to indicate the base type of the processor
 *      initData        pointer to 1K of memory.  
 *
 *  Return Values:
 *
 *      none
 *
 */
void customizeReadPowerOnButtons(int chipType, unsigned char *initData)
{
    WORD32 gpioState;
/*
 * THIS CODE MUST BE CUSTOMIZED FOR YOUR BOARD.  This implementation
 * is for NET+OS development boards only.
 */
    NA_UNUSED_PARAM(chipType);
    NA_UNUSED_PARAM(initData);
    
    if (initData != NULL)
    {
        /*
         * Read the current state of the GPIO pins into the buffer pointed to by
         * initData.  The pins (most of them anyway) power up in the GPIO input
         * configuration, so we can just read them.
         */
        gpioState = *NA_GPIO_STATUS_getAddress(0);
        memcpy(initData, &gpioState, 4);
        gpioState = *NA_GPIO_STATUS_getAddress(32);
        memcpy(&initData[4], &gpioState, 4);
        gpioState = *NA_GPIO_STATUS_getAddress(64);
        memcpy(&initData[8], &gpioState, 4);
        gpioState = *NA_GPIO_STATUS_getAddress(96);
        memcpy(&initData[12], &gpioState, 4);
    }
    

}

