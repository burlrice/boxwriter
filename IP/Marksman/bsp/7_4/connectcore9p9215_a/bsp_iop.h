/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_IOP_H
#define BSP_IOP_H

#ifdef __cplusplus
extern "C"
{
#endif

/*ad*
 * 
 * The BSP configuration for the FIM driver allows you to configure folowing 
 * device drivers:
 *
 * <list type="unordered">
 *  <item> SDIO Interface </item>
 *  <item> UART Interface </item>
 *  <item> 1-Wire Interface </item>
 *  <item> CAN Interface </item>
 * </list>
 *
 * @note Above device interface configuration are subject to the pin 
 * availability in the main processor. That means to say, we may not be able
 * to configure FIM 0 control IO lines in NS9210 based devices.
 *
 * @category BSP:BSP_Configuration:FIM
 * @since 7.4
 * @overview FIM Configuration
 * @library bsp
 * @platform    All_9210_Platforms, All_9215_Platforms 
 * @include "bsp_iop.h"
 * @external
 *
*ad*/

/*ad*
 *
 * This section explains how FIM can be configured to use as serial driver.
 *
 * ARM9 <=> GPIO lines <=> FIM <=> FIM GIO lines <=> serial lines/device  
 *
 * These are the steps you need to follow to use FIM serial interface.
 *
 * <list type="unordered">
 *  <item> You will need 4 FIM general IO lines for 4-Wire UART or just two
 *         FIM general IO lines for 2-Wire UART </item>
 *  <item> Based on the available GPIO pins configure BSP_GPIO_MUX_IOP_n_GEN_IO_x 
 *         defines in platform specific gpio.h </item>
 *  <item> Based on selected GPIO pins, configure FIM IO lines using defines
 *         in bsp_iop.h. For example if you want FIM IO line 2 as TXD line
 *         then set BSP_IOP_FIM0_UART_TXD as BSP_IOP_GEN_IO_2 </item>
 *  <item> User can use /com/4 and com/5 to open serial channel on FIM0 and
 *         FIM1 respectively </item>
 * </list>
 *
 * <example>
 * #include <netosIo.h>
 * #include <termios.h>
 *
 * void sampleFunction(void)
 * {
 *    int bytes_read, handle = -1;
 *    struct termios tios;
 *    static char buffer[64]; 
 *
 *    handle = open("/com/4", O_RDWR | O_NONBLOCK, 0);    
 *    if (handle <= 0)
 *    {
 *        printf("Failed to open FIM serial port\n");
 *        return;
 *    }
 *
 *    // get the current serial attributes
 *    if (tcgetattr(handle, &tios) < 0)
 *    {
 *        printf("tcgetattr failed\n");
 *        close(handle);
 *        goto init_done;
 *    }
 *
 *    // turn on hardware handshake 
 *    tios.c_cflag |= CRTSCTS;
 *    tios.c_iflag &= ~(IXON | IXOFF);
 *
 *    // set the baudrate
 *    cfsetospeed(&tios, 115200);
 *    if (tcsetattr(handle, TCSANOW, &tios) < 0)
 *    {
 *        printf("tcsetattr failed\n");
 *        close(handle);
 *        return;
 *    }
 *   
 *    // read the data from serial port
 *    bytes_read = read(handle, buffer, 64);
 *    if (bytes_read < 0)
 *    {
 *        if (getErrno() != EAGAIN)
 *        {
 *            printf("Serial read failed, errno[%d]\n", getErrno());
 *            close(handle);
 *            return;
 *        }
 *    }
 *    else
 *    {
 *        // echo the sent data back to sender     
 *        if (write(handle, buffer, bytes_read) < 0)
 *        {
 *            if (getErrno() != EAGAIN)
 *            {
 *                printf("Serial write failed, errno[%d]\n", getErrno());
 *                close(handle);
 *                return;
 *            }
 *        }
 *    }
 *
 *    close(handle);
 *
 * }
 * </example>
 *
 * @category BSP:BSP_Configuration:FIM:FIM_Serial
 * @since 7.4
 * @overview Serial Interface
 * @library bsp
 * @platform    All_9210_Platforms, All_9215_Platforms 
 * @include "bsp_iop.h"
 * @external
 *
*ad*/


/*ad*
 * Defines for FIM General IO pins. Supported only under NS9210 and NS9215 
 * based platforms. Both FIMs have following set of pins.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 * @name IopFimPins   "FIM General IO Ports"
 *
 * @param BSP_IOP_GEN_IO_0   FIM general IO port 0
 * @param BSP_IOP_GEN_IO_1   FIM general IO port 1 
 * @param BSP_IOP_GEN_IO_2   FIM general IO port 2
 * @param BSP_IOP_GEN_IO_3   FIM general IO port 3 
 * @param BSP_IOP_GEN_IO_4   FIM general IO port 4 
 * @param BSP_IOP_GEN_IO_5   FIM general IO port 5 
 * @param BSP_IOP_GEN_IO_6   FIM general IO port 6 
 * @param BSP_IOP_GEN_IO_7   FIM general IO port 7 
 *
 *ad*/
#define BSP_IOP_GEN_IO_0    0
#define BSP_IOP_GEN_IO_1    1
#define BSP_IOP_GEN_IO_2    2
#define	BSP_IOP_GEN_IO_3    3
#define BSP_IOP_GEN_IO_4    4
#define BSP_IOP_GEN_IO_5    5	
#define BSP_IOP_GEN_IO_6    6
#define BSP_IOP_GEN_IO_7    7	


/*ad*
 * Defines which FIM general IO pin to use for TXD line when FIM is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM0_UART_TXD           BSP_IOP_GEN_IO_0

/*ad*
 * Defines which FIM general IO pin to use for RXD line when FIM 0 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM0_UART_RXD           BSP_IOP_GEN_IO_1

/*ad*
 * Defines which FIM general IO pin to use for RTS line when FIM 0 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM0_UART_RTS           BSP_IOP_GEN_IO_2

/*ad*
 * Defines which FIM general IO pin to use for CTS line when FIM 0 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM0_UART_CTS           BSP_IOP_GEN_IO_3

/*ad*
 * Defines which FIM general IO pin to use for TXD line when FIM 1 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM1_UART_TXD           BSP_IOP_GEN_IO_4

/*ad*
 * Defines which FIM general IO pin to use for RXD line when FIM 1 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM1_UART_RXD           BSP_IOP_GEN_IO_5

/*ad*
 * Defines which FIM general IO pin to use for RTS line when FIM 1 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM1_UART_RTS           BSP_IOP_GEN_IO_6

/*ad*
 * Defines which FIM general IO pin to use for CTS line when FIM 1 is 
 * configured as UART.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM1_UART_CTS           BSP_IOP_GEN_IO_7

/*ad*
 * Defines which FIM general IO pin to use for 1-Wire line when FIM 0 is 
 * configured as 1-Wire.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM0_1WIRE            BSP_IOP_GEN_IO_0

/*ad*
 * Defines which FIM general IO pin to use for 1-Wire line when FIM 1 is 
 * configured as 1-Wire.
 *
 * @since 7.4
 * @external
 * @category BSP:BSP_Configuration:FIM
 *
 * @see @link IopFimPins
*ad*/
#define BSP_IOP_FIM1_1WIRE            BSP_IOP_GEN_IO_0


#ifdef __cplusplus
}
#endif


#endif /* BSP_IOP_H */
