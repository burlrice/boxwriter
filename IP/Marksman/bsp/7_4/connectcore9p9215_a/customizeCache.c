/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *  Description
 *  ====================================================================
 *  This file contains mmuTable which is a table developers should edit
 *  to reflect the memory layout of their application.
 *
 */

#include <string.h>
#include "Npttypes.h"
#include "cache.h"
#include "mmu_ttb.h"
#include "mmu.h"
#include "nammu.h"


/*ad*
 * MMU Definition Table
 *
 * You should edit this table to reflect the address map of your application.  
 * The BSP processes this table at startup to build the MMU address
 * translation table.  This table determines the access level and caching
 * options for each section of memory. 
 *
 * Each entry in this table specifies a section of the processor's address space
 * and sets the MMU options for it.  Any sections of memory that do not have 
 * an entry in this table are defined as nonaccessable.  Any attempt to access
 * such a region will cause an abort exception.
 *
 * The NASetCacheAccessModes() function can be used to modify the cache and user
 * access settings for a region of memory after the MMU address translation table
 * has been setup.  The page size for regions set in this table cannot be 
 * modified at runtime.
 *
 * Refer to the documentation on mmuTableType for details on how to create
 * entries.
 *
 * @external
 * @category MMU
 * @since 6.1 
 *
 * @note  Use large page sizes to improve performance.  
 *        The MMU has a small internal cache of MMU table
 *        entries.  As long as the table entry for the memory being accessed is
 *        inside the MMU's internal cache, the MMU does not have to scan the
 *        MMU table in RAM.  However, whenever the CPU accesses memory whose
 *        table entry is not already in the MMU's cache, the MMU has to look up the 
 *        table entry in RAM.  This incurs at least one extra memory access 
 *        (more for coarse sections) for the access.  The more table entries 
 *        there are, the more likely this is to happen.
 *
 * @see @link mmuCacheModeValues
 * @see @link mmuUserAccessValues
 * @see @link mmuPageSizes
 * @see @link mmuTableType
 * @see @link NASetCacheAccessModes
*ad*/
mmuTableType mmuTable[] =
{
/*   Start       End          Page Size          Cache Mode          User Access     Physical Address */
/*   ==========  ==========   ================   =============       ===========     =============== */
    {0x40000000, 0x40000200,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,    MMU_CLIENT_RW,  0x40000000, 0x40000200}, /* CS0 - MPHC */

    {0x00000000, 0x00ffffff,  MMU_PAGE_SIZE_1M,  MMU_WRITE_BACK,  MMU_CLIENT_RW,  0x00000000, 0x00ffffff}, /* 16 Megs of RAM                      */
    {0x50000000, 0x57ffffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x50000000, 0x57ffffff}, /* 8 Megs of flash                     */
    {0x03100000, 0x0310ffff,  MMU_PAGE_SIZE_64K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x03100000, 0x0310ffff}, /* 64k of compact flash               */
    {0x80000000, 0x8fffffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x80000000, 0x8fffffff}, /* PCI Memory                          */
/*
    {0x00000000, 0x00ffffff,  MMU_PAGE_SIZE_1M,  MMU_WRITE_BACK,     MMU_CLIENT_RW,  0x00000000, 0x00ffffff}, // 16 Megs of RAM                      
#ifdef DOTNETMF
    // .NET has an LCD connected to cs0 
    {0x40000000, 0x47ffffff,  MMU_PAGE_SIZE_1M,  MMU_NONBUFFERED,    MMU_CLIENT_RW,  0x40000000, 0x47ffffff}, // CS0 - LCD 
#endif
    {0x50000000, 0x57ffffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x50000000, 0x57ffffff}, // 8 Megs of flash                     
    {0x03100000, 0x0310ffff,  MMU_PAGE_SIZE_64K, MMU_BUFFERED,       MMU_CLIENT_RW,  0x03100000, 0x0310ffff}, // 64k of compact flash                
    {0x80000000, 0x8fffffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x80000000, 0x8fffffff}, // PCI Memory                          
*/
    {0x90000000, 0x900001d3,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90000000, 0x900001d3}, /* BBUS DMA Engine 1                   */
    {0x90001000, 0x90003fff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90001000, 0x90003fff}, /* IOP processor 0 pass thru CSR space */
    {0x90004000, 0x90007fff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90004000, 0x90007fff}, /* IOP processor 0 instruction space   */

    {0x90008000, 0x90008030,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90008000, 0x90008030}, /* IOP processor 1 CSR space           */
    {0x90009000, 0x9000bfff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90009000, 0x9000bfff}, /* IOP processor 1 pass thru CSR space */
    {0x9000c000, 0x9000ffff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x9000c000, 0x9000ffff}, /* IOP processor 1 instruction space   */

    {0x90010000, 0x9001002f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90010000, 0x9001002f}, /* UARTA IOHUB Registers               */
    {0x90011000, 0x9001111f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90011000, 0x9001111f}, /* UARTA Registers                     */

    {0x90018000, 0x9001802f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90018000, 0x9001802f}, /* UARTB IOHUB Registers               */ 
    {0x90019000, 0x9001911f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90019000, 0x9001911f}, /* UARTB Registers                     */ 

    {0x90020000, 0x9002002f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90020000, 0x9002002f}, /* UARTC IOHUB Registers               */ 
    {0x90021000, 0x9002111f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90021000, 0x9002111f}, /* UARTC Registers                     */ 

    {0x90028000, 0x9002802f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90028000, 0x9002802f}, /* UARTD IOHUB Registers               */ 
    {0x90029000, 0x9002911f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90029000, 0x9002911f}, /* UARTD Registers                     */ 

    {0x90030000, 0x9003002f,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90030000, 0x9003002f}, /* SPI Registers               */ 
    {0x90031000, 0x90037fff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,    MMU_CLIENT_RW,  0x90031000, 0x90037fff}, /* SPI CSR Space               */ 

    {0x90039000, 0x90039027,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90039000, 0x90039027}, /* A/D                                 */
    {0x90050000, 0x9005000F,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90050000, 0x9005000F}, /* I2C                                 */
    {0x90060000, 0x90067FFF,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90060000, 0x90067FFF}, /* RTC + Battery Backed up RAM               */
    {0x90068000, 0x900680ff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90068000, 0x900680FF}, /* IOP H/W Assist Config 0             */
    {0x90070000, 0x900700ff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90070000, 0x900700FF}, /* IOP H/W Assist Config 1             */
    {0x90080000, 0x9008ffff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90080000, 0x9008ffff}, /* IOP 0 I/O space                     */
    {0x90090000, 0x9009ffff,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90090000, 0x9009ffff}, /* IOP 1 I/O space                     */
/*    {0x90110000, 0x901101d3,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90110000, 0x901101d3}, */ /* BBUS DMA Engine 2              */
    {0x90200000, 0x9020007b,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90200000, 0x9020007b}, /* Serial ports 1 and 2                */
    {0x90300000, 0x9030007b,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90300000, 0x9030007b}, /* Serial ports 3 and 4                */
    {0x90400000, 0x9040017b,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90400000, 0x9040017b}, /* IEEE 1284                           */
    {0x90600000, 0x90600093,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90600000, 0x90600093}, /* BBUS utility                        */
    {0x90700000, 0x90700034,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90700000, 0x90700034}, /* RTC                                 */
    {0x90800000, 0x90801FFF,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90800000, 0x90801FFF}, /* USB Host                            */
    {0x90900000, 0x9091FFFF,  MMU_PAGE_SIZE_1M,  MMU_NONBUFFERED,       MMU_CLIENT_RW,  0x90900000, 0x9091FFFF}, /* USB Device                          */
    {0xA0000000, 0xA00fffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0000000, 0xA00fffff}, /* PCI IO                              */
    {0xA0100000, 0xA01fffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0100000, 0xA01fffff}, /* PCI CONFIG_ADDR                     */
    {0xA0200000, 0xA02fffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0200000, 0xA02fffff}, /* PCI CONFIG_DATA                     */

    {0xA0300000, 0xA0301FFF,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0300000, 0xA0301FFF}, /* PCI Abiter                          */
    {0xA0400000, 0xA040002B,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0400000, 0xA040002B}, /* AHB general purpose DMA             */
    {0xA0401000, 0xA0401007,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0401000, 0xA0401007}, /* BBUS Bridge Interrupt               */
    {0xA0600000, 0xA0601400,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0600000, 0xA0601400}, /* Ethernet Communications Module      */
    {0xA0700000, 0xA070027c,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0700000, 0xA070027c}, /* Memory Controller                   */
    {0xA0800000, 0xA08003FF,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0800000, 0xA08003FF}, /* LCD Controller                      */
    {0xA0900000, 0xA0900224,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0900000, 0xA0900224},  /* System Control Module               */
    {0xA0902000, 0xA090208b,  MMU_PAGE_SIZE_4K,  MMU_BUFFERED,       MMU_CLIENT_RW,  0xA0902000, 0xA090208b},  /* GPIO config, control, status regs  */
                                                                                                          
   {0xB0000000, 0xBfffffff,  MMU_PAGE_SIZE_1M,  MMU_WRITE_BACK,     MMU_CLIENT_RW,  0x80000000, 0x8fffffff}, /* PCI Memory                          */
   {0xC0000000, 0xC0ffffff,  MMU_PAGE_SIZE_1M,  MMU_BUFFERED,       MMU_CLIENT_RW,  0x00000000, 0x00ffffff} /* Uncached SDRAM */
};

int const mmuTableSize = asizeof(mmuTable);







