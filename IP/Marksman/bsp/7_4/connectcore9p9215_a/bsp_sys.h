/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 *
 * bsp_sys.h This file defines BSP wide constants and data structures.
 *
 *
 * Edit History
 *
 * Date      Initials       Change Description
 *
 */

#ifndef BSP_SYS_H
#define BSP_SYS_H

#include <Npttypes.h>
#include "bsp_defs.h"
#include <init_settings.h>
#include "mcreg_def.h"


#ifdef __cplusplus
extern "C"
{
#endif


/*ad*
 * Set this constant to TRUE to include the GDB debug.
 * This enables software debugging. Once this is enabled,
 * Bootloader starts TFTP to load image.bin for debugging 
 * and bsp starts GDB by waiting for gdb debugger.
 * 
 *
 * @since 7.0
 * @external
 * @include  <bsp.h>
 * @category BSP:BSP_Configuration
 *
 * @note gdb server library must be linked in your application.
 * @see @link SystemSWServices:gdbserver
 * @see @link NA_GDB_TFTP_SERVER
 * @see @link NA_GDB_COMPORT
 *
*ad*/
#define BSP_GDB_SERVER_ENABLE          FALSE
 
/*ad*
 * Bootloader default TFTP server ip address. 
 * This is used to specify default TFTP server ip address
 * to load debug image.
 * This BSP_GDB_SERVER_ENABLE must be enabled to include GDB.
 *
 * @since 7.0
 * @include     <bsp.h>
 * @external
 * @category BSP:BSP_Configuration
 * @see @link NA_GDB_COMPORT
 * @see @link BSP_GDB_SERVER_ENABLE
 *
 *ad*/
#define NA_GDB_TFTP_SERVER     "10.52.32.193" 

/*ad*
 * GDB communication port. 
 *
 * This is used for gdb debug. This serial port is used to communicate
 * between the board and the gdb debugger.
 * See BSP_GDB_SERVER_ENABLE to enable GDB debug.
 *
 * @since 7.0
 * @include     <bsp.h>
 * @external
 * @category BSP:BSP_Configuration
 * @see @link BSP_GDB_SERVER_ENABLE
 * @see @link NA_GDB_TFTP_SERVER
 *
 *ad*/
#define NA_GDB_COMPORT         "/com/3"


/*ad*
 * This constant sets the UDP port on the UDP debug device.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 *ad*/
#define BSP_UDP_DEBUG_PORT              12345


/*ad*
 * BSP_TICKS_PER_SECOND determines the system clock rate.  
 *
 * 
 * Use the constant NABspTicksPerSecond to access this value
 * in application code.
 *
 * @since 1.0
 * @external
 * @see @link NABspTicksPerSecond
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_TICKS_PER_SECOND        100                /* 10ms tick */

/*ad*
 * This constant defines the chip type and rev used in the BSP,
 * the values for this variable are defined in the header
 * file mcreg_def.h.
 *
 * @since 6.1
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_CHIP_TYPE NCC_NS9215_A


/*ad*
 * BSP_BAUD_RATE determines the baud rate that will be used for the configuration
 * dialog if APP_USE_NVRAM is 0, or if NVRAM is uninitialized.
 *
 * @external
 * @category BSP:BSP_Configuration 
 * @since 7.1
*ad*/
#define BSP_BAUD_RATE	        38400

/*ad*
 * Set this to TRUE to run the dialog code during startup.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_ENABLE_DIALOG          TRUE

/*
 * BSP_STDIO_PORT determines which serial port stdin, stdout, and stderr should be redirected to.
 * It should be set to the device name of the port or NULL.
 *
 * @external
 * @category BSP:BSP_Configuration 
 * @since 7.1 
 */
#define BSP_STDIO_PORT              NULL

/*
 * BSP_DIALOG_PORT determines whether or not the initialization code will prompt for configuration
 * parameters.  It should be set to a string that contains the serial port to prompt with.  If it
 * is not set, or if it contains an invalid port, then the initialization code will not prompt for
 * configuration parameters.
 *
 * @external
 * @category BSP:BSP_Configuration 
 * @since 7.1 
 */
#define BSP_DIALOG_PORT             "/com/3"


/*ad*
 * These manifest constants determine the ThreadX priority levels for
 * various server processes created by the API libraries.  ThreadX
 * priority levels range from 0 to 31 with 0 being the highest.  Threads
 * with greater priority can preempt threads
 * with lower priority.
 *
 * @since 1.0
 * @external
 * @category BSP:BSP_Configuration
 * @name ThreadPriorityLevels           "Thread Priority Levels"
 *
 * @param BSP_HIGH_PRIORITY             priority level for threads that need to be able to preempt
 *                                      most other threads
 * @param BSP_MEDIUM_PRIORITY           normal priority level for threads
 * @param BSP_LOW_PRIORITY              priority level for threads that should run at a low
 *                                      priority level (and be preemptable by most other threads)
 * @param BSP_DEFAULT_PRIORITY          default priority level for threads
 * @param BSP_FTP_SERVER_PRIORITY       priority level for FTP server thread
 * @param BSP_SNMP_AGENT_PRIORITY       priority level for SNMP agent thread
 * @param BSP_TELNET_SERVER_PRIORITY    priority level for Telnet server thread
 * @param BSP_SNTP_THREAD_PRIORITY      priority level for SNTP client thread
 * @param BSP_SERLMON_THREAD_PRIORITY   priority level for the serial monitor thread
*ad*/
#define BSP_HIGH_PRIORITY               8
#define BSP_MEDIUM_PRIORITY             16
#define BSP_LOW_PRIORITY                24
#define BSP_DEFAULT_PRIORITY            BSP_MEDIUM_PRIORITY
#define BSP_FTP_SERVER_PRIORITY         BSP_MEDIUM_PRIORITY
#define BSP_SNMP_AGENT_PRIORITY         BSP_MEDIUM_PRIORITY
#define BSP_TELNET_SERVER_PRIORITY      BSP_MEDIUM_PRIORITY
#define BSP_SNTP_THREAD_PRIORITY        BSP_MEDIUM_PRIORITY
#define BSP_SERLMON_THREAD_PRIORITY     15

/*ad*
 * This constant determines how long the watchdog timer waits before resetting the unit.
 * The unit of time is in seconds.
 *
 * @since 6.1
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_WATCHDOG_TIMEOUT_IN_SECONDS 30


/*
 * Offset from the beginning of RAM which we are using for
 * the top of the stack for calling ncc_init, this memory is tested
 * in init.s.
 */
#define BSP_RAM_OFFSET 0x4000

/*ad*
 * If BSP_AUTOMATICALLY_ENABLE_INSTRUCTION_CACHE is set, then the BSP startup code will
 * enable instruction cache if it is present on the processor.  This constant is useful
 * if you will (or may in the future) run your firmware on different chips
 * that have or don't have cache.  Setting it TRUE will cause the BSP to
 * turn cache on if it detects it in the chip.  Setting it FALSE will cause
 * it leave cache off whether the chip supports it or not.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_AUTOMATICALLY_ENABLE_INSTRUCTION_CACHE  TRUE

 
/*ad*
 * Set BSP_HANDLE_FATAL_ERROR to this value to trigger a reset when fatal
 * errors are encountered.
 *
 * Set BSP_HANDLE_UNEXPECTED_EXCEPTION to this value to trigger a reset
 * when an unexpected exception occurs.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
 * @see @link BSP_HANDLE_FATAL_ERROR
 * @see @link BSP_HANDLE_UNEXPECTED_EXCEPTION
*ad*/
// #define BSP_ERROR_RESET_UNIT        FALSE

/*ad*
 * This constant determines how fatal errors are handled.  The possible values for
 * BSP_HANDLE_FATAL_ERROR are:
 *
 *      BSP_ERROR_RESET_UNIT    Unit is reset unless a debugger ICE was detected during
 *                              initialization.  If an ICE was detected, then the LEDs
 *                              are blinked in a pattern that indicates the error
 *                              condition, and it is possible to continue execution in
 *                              the debugger.
 *      BSP_ERROR_REPORT_ERROR  BSP blinks LEDs in a pattern that indicates the type of
 *                              error that occurred.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_HANDLE_FATAL_ERROR              BSP_ERROR_REPORT_ERROR

/*ad*
 * This constant determines how unexpected exceptions are handled.  The possible
 * values for BSP_HANDLE_UNEXPECTED_EXCEPTION are:
 *
 *      BSP_ERROR_RESET_UNIT    Unit is reset unless a debugger ICE was detected during
 *                              initialization.  If an ICE was detected, then the LEDs
 *                              are blinked in a pattern that indicates the error
 *                              condition, and it is possible to continue execution in
 *                              the debugger.
 *      BSP_ERROR_REPORT_ERROR  BSP blinks LEDs in a pattern that indicates the type of
 *                              error that occurred.
 *      BSP_ERROR_CONTINUE      the exception handler attempts to continue execution.  The
 *                              recovery procedure is to adjust the return address of the
 *                              exception to restart execution on the next instruction
 *                              after the one that causes the exception.
 *
 * @since 6.0
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_HANDLE_UNEXPECTED_EXCEPTION     BSP_ERROR_REPORT_ERROR


/*ad*
 *  BSP_STARTUP_DELAY defines number of additional seconds the system
 *  will wait, before starting the application. Used on NS9750 chip to
 *  let some network activity be received to use as a ramdom seed.
 *
 * @since 5.1
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_STARTUP_DELAY       2

/*ad*
 * Set this constant TRUE to inform NETOS startup process to setup SNTP for retreiving time. 
 * 
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
 *
 * @note This constant must be TRUE and APP_USE_NETWORK_TIME_PROTOCOL must be defined in the application to enable SNTP.
*ad*/
#define BSP_INCLUDE_SNTP    FALSE

/*ad*
 * This is used to define a callback function for SNTP. BSP initializes
 * SNTP with this callback definition.
 *
 * @since 6.0
 * @external
 * @category Networking:SNTP
 *
 * @note BSP_INCLUDE_SNTP must be TRUE and APP_USE_NETWORK_TIME_PROTOCOL must be defined in the application to enable SNTP.
*ad*/
#define BSP_SNTP_CALLBACK   NULL


/*ad*
 * This enables the deep sleep mode. Before setting this to true make sure one 
 * GPIO pin is configured to trigger external reset and another GPIO pin is
 * configured to hold the status and pin 68 is configured to function mode
 * 1 so that the SDRAM CKE signal is under direct control of the memory 
 * controller.
 * 
 * @since 7.0
 * @include 	<bsp.h>
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_ENABLE_SLEEP_EXTERNAL_RESET    FALSE


/*ad*
 * Set this to TRUE if you want to bypass the auto-negotiation during sleep 
 * wakeup mode. This will sped up the initialization by around 2 seconds. 
 * Ethernet may not configure correctly if you switch the device in sleep mode 
 * to the network whose duplex mode is fixed and is different from the 
 * device's previously configured duplex mode. 
 *
 * @since 7.0
 * @include 	<bsp.h>
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_WAKEUP_DISABLE_AUTO_NEGOTIATION   FALSE

/*ad*
 * BSP_START_OF_DEV_BOARD_PARAMETERS is used to set the offset into
 * EEPROM space for the start of the board parameters structures. This may
 * be done to share the EEPROM with another element on the board.
 * The value set is the byte offset into the EEPROM where NET+OS will
 * write its parameters.
 * This replaces START_OF_DEV_BOARD_PARAMETERS previously defined
 * in boardParams.h.
 *
 * @since 7.2
 * @include     <bsp.h>
 * @external
 * @category BSP:BSP_Configuration
 *ad*/
 
#define	BSP_START_OF_DEV_BOARD_PARAMETERS 0


/*ad*
 * Set this to TRUE to run the system POST tests during startup.
 *
 * @since 6.2
 * @external
 * @category BSP:BSP_Configuration
*ad*/
#define BSP_POST_TEST          FALSE


/* These settings are defined in init_settings.h */
#define     mem_power_level_normal	MEM_POWER_LEVEL_NORMAL
#define     mem_power_level_low		MEM_POWER_LEVEL_LOW


/* These settings are defined in init_settings.h */
#define    mem_width_16				MEM_WIDTH_16
#define    mem_width_32				MEM_WIDTH_32


/* These settings are defined in init_settings.h */
#define    x8						MEM_TYPE_x8
#define    x16						MEM_TYPE_x16
#define    x32						MEM_TYPE_x32



/* Index into static memory table */
#define CS_0_INDEX 0
#define CS_1_INDEX 1
#define CS_2_INDEX 2
#define CS_3_INDEX 3

/* Index into dynamic memory table */
#define CS_4_INDEX 0
#define CS_5_INDEX 1
#define CS_6_INDEX 2
#define CS_7_INDEX 3

// extern const NAStaticMemoryType  NAStaticMemoryTable[];

/*
 * Offset into RAM which we write/read to determine if RAM is
 * present.
 */
#define MEMC_READ_LOCATION 4096

/* Number of 4-byte words in the memory controller cache */
#define MEMC_CTRL_BUF_LEN  64

#define BSP_RAM_BASE 0x00000000

#ifdef DOTNETMF
#undef  BSP_TICKS_PER_SECOND
#define BSP_TICKS_PER_SECOND 1000 /* 1KHz clock*/
#undef  BSP_WATCHDOG_TIMEOUT_IN_SECONDS
#define BSP_WATCHDOG_TIMEOUT_IN_SECONDS 10
#endif


#ifdef __cplusplus
}
#endif


#endif /* BSP_SYS_H */

