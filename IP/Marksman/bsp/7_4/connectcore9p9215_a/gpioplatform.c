
/*****************************************************************************
* Copyright (c) 2004 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/


#include <assert.h>
#include <fwtypes.h>
#include <nagpio.h>
#include <gpioplatform.h>
#include <processorgpio.h>
#include <i2c_gpio.h>
#include <i2c_rawapi.h>


/*
 * This is a 7-bit address.  Bits 0-2 are hardware wired on the board
 * to be 0.  Bits 3-7 are determined by the part itself, which wants
 * bit 5 to be set and the others zero.  So, our address for this part
 * is 0x20.
 */
#define I2CGPIO0 0x20	


#define PROCESSOR_I2C_GPIO0 (NA_I2C_MAKE_ADDRESS(1, NA_I2C_ADDRMODE_7BIT, I2CGPIO0))

/*   gpio_type			addr		gpio_pin */
const GpioPlatformTable_t GpioPlatformNone[] = {

    {GPIO_TYPE_NONE,		0,		0},	/* Manufacturing Output */
    {GPIO_TYPE_NONE,		0,		0},	/* Manufacturing Input */

    /* Processor GPIO */
    {GPIO_TYPE_PROCESSOR_PIN,   2,           3},     /* Processor GPIO 0 */
    {GPIO_TYPE_PROCESSOR_PIN,   2,           4},     /* Processor GPIO 1*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           5},     /* Processor GPIO 2*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           6},     /* Processor GPIO 3*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           7},     /* Processor GPIO 4*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           8},     /* Processor GPIO 5*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           9},     /* Processor GPIO 6*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           10},     /* Processor GPIO 7*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           11},     /* Processor GPIO 8*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           12},     /* Processor GPIO 9*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           13},     /* Processor GPIO 10*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           14},     /* Processor GPIO 11*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           15},     /* Processor GPIO 12*/
    {GPIO_TYPE_PROCESSOR_PIN,   2,           16},     /* Processor GPIO 13*/

    /* I2C GPIO */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	0},	/* GPIO_I2C_GPIO_0 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	1},	/* GPIO_I2C_GPIO_1 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	2},	/* GPIO_I2C_GPIO_2 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	3},	/* GPIO_I2C_GPIO_3 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	4},	/* GPIO_I2C_GPIO_4 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	5}, /* GPIO_I2C_GPIO_5 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	6},	/* GPIO_I2C_GPIO_6 */
    {GPIO_TYPE_I2C,		PROCESSOR_I2C_GPIO0,	7},	/* GPIO_I2C_GPIO_7 */

    {GPIO_TYPE_PROCESSOR_PIN,   2,      18}, /* GPIO_ERROR_LED and user LED 1 */
    {GPIO_TYPE_PROCESSOR_PIN,   2,      21}, /* user LED 2 */
    {GPIO_TYPE_PROCESSOR_PIN,   2,      17}, /* GPIO_USER_BUTTON1 */
    {GPIO_TYPE_PROCESSOR_PIN,   2,      20}, /* GPIO_USER_BUTTON2 */
    {GPIO_TYPE_PROCESSOR_PIN,   0,      4},   /* GPIO_WAKEUP_BUTTON */
    
    {GPIO_TYPE_NONE, 0,   0},  		/* GPIO_UARTA_RTS */
    {GPIO_TYPE_PROCESSOR_PIN, 1,   24}, /* GPIO_UARTB_RTS, 8-wire UART */
    {GPIO_TYPE_NONE, 0,   0}, 		/* GPIO_UARTC_RTS */
    {GPIO_TYPE_PROCESSOR_PIN, 2,   0},  /* GPIO_UARTD_RTS, 8-wire UART*/

    {GPIO_TYPE_NONE, 0,   0},  		/* GPIO_UARTA_CTS */
    {GPIO_TYPE_PROCESSOR_PIN, 1,   20}, /* GPIO_UARTB_CTS, 8-wire UART */
    {GPIO_TYPE_NONE, 0,   0}, 		/* GPIO_UARTC_CTS */
    {GPIO_TYPE_PROCESSOR_PIN, 1,   28},  /* GPIO_UARTD_CTS, 8-wire UART*/
};

const GpioPlatformTable_t * const GpioPlatformTable[1] = {
    GpioPlatformNone,
};

const int GpioPlatformTableSize[1] = {
    sizeof(GpioPlatformNone)/sizeof(GpioPlatformNone[0]),
};

static int GpioPlatformMode(void)
{
    /* TO DO: If there are multiple modes in this platform, you should add
       codes here to either read from a static memory device or switch to
       decide which mode it is.

       For this platform, we only put in one mode.  One can update the table
       above and to add logic to find out what mode the hardware is set at.
       */
    return 0;
}

int GpioPlatformFunction(int function, unsigned long *gpio_type, unsigned long *addr, unsigned long *gpio_pin)
{
    int index = -1 - function;
    int mode = GpioPlatformMode();

    if (index < 0 || index >= GpioPlatformTableSize[mode])
        return FALSE;
    *gpio_type  = GpioPlatformTable[mode][index].gpio_type;
    *addr	= GpioPlatformTable[mode][index].addr;
    *gpio_pin   = GpioPlatformTable[mode][index].gpio_pin % 32;
    if (*gpio_type == GPIO_TYPE_NONE)
        return FALSE;
    return TRUE;
}

void GpioPlatform_epoch(void)
{
}
