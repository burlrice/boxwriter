/*****************************************************************************
* Copyright (c) 2004 Digi International Inc., All Rights Reserved
* 
* This software contains proprietary and confidential information of Digi
* International Inc.  By accepting transfer of this copy, Recipient agrees
* to retain this software in confidence, to prevent disclosure to others,
* and to make no use of this software other than that for which it was
* delivered.  This is an unpublished copyrighted work of Digi International
* Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
* prohibited.
* 
* Restricted Rights Legend
*
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
* Technical Data and Computer Software clause at DFARS 252.227-7031 or
* subparagraphs (c)(1) and (2) of the Commercial Computer Software -
* Restricted Rights at 48 CFR 52.227-19, as applicable.
*
* Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
*
*****************************************************************************/

#ifndef _1284_H

#define _1284_H

#include <mcdma_def.h>

/*
 * This header file defines constants that are used by the client parallel
 * driver.  You must customize these constants to suit the requirements
 * of your application.
 */



/*ad*
 * This constant determines the size of the DMA descriptor ring the parallel
 * driver will create to hold receive data buffers.  Buffers in this ring 
 * are used to hold data sent from the host.  Set this constant to a value
 * between 1 and 64.  Larger values usually improve performance.
 *
 *
 * @external
 * @category paraClient
 * @since 6.2
 *
*ad*/
#define PARA_RX_RING_SIZE               16

#if ((PARA_RX_RING_SIZE < 1) || (PARA_RX_RING_SIZE > DMA_BBUS_MAX_RING_SIZE))
    #error PARA_RX_RING_SIZE is invalid
#endif

/*ad*
 * This constant determines the size of the receive data buffers the parallel
 * driver will allocate.  Set this constant to a value which is divisible by
 * 4 and between 4 and 65532.  This constant must be at least as large as the
 * device ID (if a device ID is set).  Larger values usually improve performance.
 *
 * @external
 * @category paraClient
 * @since 6.2
 *
*ad*/
#define PARA_RX_BUFFER_SIZE             (16*1024)

#if ((PARA_RX_BUFFER_SIZE < DMA_BBUS_MIN_BUFFER_SIZE) || (PARA_RX_BUFFER_SIZE > DMA_BBUS_MAX_BUFFER_SIZE) || (PARA_RX_BUFFER_SIZE & 0x3))
    #error PARA_RX_BUFFER_SIZE is invalid
#endif


/*ad*
 * This constant determines the size of the DMA descriptor ring the parallel
 * driver will create to hold transmit data buffers.  Buffers in this ring 
 * are used to hold data sent to the host.  Set this constant to a value
 * between 1 and 64.  
 *
 *
 * @external
 * @category paraClient
 * @since 6.2
 *
*ad*/
#define PARA_TX_RING_SIZE               4

#if ((PARA_TX_RING_SIZE < 1) || (PARA_TX_RING_SIZE > DMA_BBUS_MAX_RING_SIZE))
    #error PARA_TX_RING_SIZE is invalid
#endif

/*ad*
 * This constant determines the size of the transmit data buffers the parallel
 * driver will allocate.  Set this constant to a value divisible by 4, 
 * and between 4 and 65532.  
 *
 * @external
 * @category paraClient
 * @since 6.2
 *
*ad*/
#define PARA_TX_BUFFER_SIZE             256

#if ((PARA_TX_BUFFER_SIZE < DMA_BBUS_MIN_BUFFER_SIZE) || (PARA_TX_BUFFER_SIZE > DMA_BBUS_MAX_BUFFER_SIZE) || (PARA_TX_BUFFER_SIZE & 0x3))
    #error PARA_TX_BUFFER_SIZE is invalid
#endif


#endif


