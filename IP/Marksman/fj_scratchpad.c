#include <stdio.h>
#include <string.h>
#include <tx_api.h>
#include "fj.h"
#include "fj_scratchpad.h"
#include "fj_printhead.h"

#define FJ_SCRATCHPAD_BASE			(0x900600C0)
#define FJ_SCRATCHPAD_COUNT			0
#define FJ_SCRATCHPAD_LABEL_NAME	4 	// FJLABEL_NAME_SIZE // 32
#define FJ_SCRATCHPAD_INK_USAGE		36 	// 8
#define FJ_SCRATCHPAD_PRINT_STATE	44 	// 4

static BOOL fj_scratchpadRead (int nOffset, unsigned char * lpData, int nSize);
static BOOL fj_scratchpadWrite (int nOffset, unsigned char * lpData, int nSize);

static BOOL fj_scratchpadRead (int nOffset, unsigned char * lpData, int nSize)
{
	volatile unsigned long * pSrc = (volatile unsigned long *)(FJ_SCRATCHPAD_BASE + nOffset);
	unsigned long * pDest = (unsigned long *)lpData;
	int i;
	
	if ((nOffset + nSize) >= 64) {
		printf ("%s(%d): fj_scratchpadRead failed: nOffset [%d] + nSize [%d] is out of bounds\n", __FILE__, __LINE__, nOffset, nSize);
		return FALSE;
	}
	
	if ((nSize % 4) != 0) {
		printf ("%s(%d): fj_scratchpadRead failed: nSize [%d] is not a multiple of 4\n", __FILE__, __LINE__, nSize);
		return FALSE;
	}
	
	int n = (nSize / 4);
	
	for (i = 0; i < n; i++) 
		* pDest++ = * pSrc++;
		
	//printf ("%s(%d): [%p - %p] [%d]\n", __FILE__, __LINE__, lpData, pDest, (ULONG)pDest - (ULONG)lpData);
		
	return TRUE;
}

static BOOL fj_scratchpadWrite (int nOffset, unsigned char * lpData, int nSize)
{
	volatile unsigned long * pDest = (volatile unsigned long *)(FJ_SCRATCHPAD_BASE + nOffset);
	unsigned long * pSrc = (unsigned long *)lpData;
	int i;
	
	if ((nOffset + nSize) >= 64) {
		printf ("%s(%d): fj_scratchpadRead failed: nOffset [%d] + nSize [%d] is out of bounds\n", __FILE__, __LINE__, nOffset, nSize);
		return FALSE;
	}
	
	if ((nSize % 4) != 0) {
		printf ("%s(%d): fj_scratchpadWrite failed: nSize [%d] is not a multiple of 4\n", __FILE__, __LINE__, nSize);
		return FALSE;
	}
	
	for (i = 0; i < (nSize / 4); i++) 
		* pDest++ = * pSrc++;
		
	return TRUE;
}

void fj_scratchpadWriteCount (long lCount)
{
	fj_scratchpadWrite (FJ_SCRATCHPAD_COUNT, (unsigned char *)&lCount, sizeof (lCount));
	//printf ("%s(%d): fj_scratchpadWriteCount: %d\n", __FILE__, __LINE__, lCount);
}

long fj_scratchpadReadCount ()
{
	long lCount = 0;
	
	fj_scratchpadRead (FJ_SCRATCHPAD_COUNT, (unsigned char *)&lCount, sizeof (lCount));
	
	return lCount;
}

void fj_scratchpadWriteLabelName (const char * lpsz)
{
	char strLabel [FJLABEL_NAME_SIZE + 4] = { 0 };
	int i;
	
	for (i = 0; i < FJLABEL_NAME_SIZE; i++) {
		char c = lpsz [i];
		
		if (c >= 0x20 && c <= 0x7e)
			strLabel [i] = c;
		else 
			strLabel [i] = 0;
	}
	
	fj_scratchpadWrite (FJ_SCRATCHPAD_LABEL_NAME, (unsigned char *)strLabel, FJLABEL_NAME_SIZE);
	//printf ("%s(%d): fj_scratchpadWriteLabelName: %s\n", __FILE__, __LINE__, strLabel);
}

void fj_scratchpadReadLabelName (char * lpsz, int nLen)
{
	char strLabel [FJLABEL_NAME_SIZE + 4] = { 0 };
	int i;
	
	fj_scratchpadRead (FJ_SCRATCHPAD_LABEL_NAME, (unsigned char *)strLabel, FJLABEL_NAME_SIZE);
	
	for (i = 0; i < nLen; i++) {
		char c = strLabel [i];
		
		if (c >= 0x20 && c <= 0x7e)
			lpsz [i] = c;
		else {
			lpsz [i] = 0;
			break;
		}
	}
}

void fj_scratchpadWriteInkUsage (double dInk)
{
	ULONG lInk [2] = { 0 };
	
	memcpy (&lInk, &dInk, sizeof (dInk));
//	fj_scratchpadWrite (FJ_SCRATCHPAD_INK_USAGE, lInk, sizeof (dInk));		
	fj_scratchpadWrite (FJ_SCRATCHPAD_INK_USAGE, &dInk, sizeof (dInk));		
	//printf ("%s(%d): %.09f [%.09f]\n", __FILE__, __LINE__, fj_scratchpadReadInkUsage (), dInk);
}

double fj_scratchpadReadInkUsage ()
{
	ULONG lInk [2] = { 0 };
	double dInk = 0.0;
	double dMax = fj_PrintHeadGetInkLowVolume (fj_CVT.pfph);
	
	//fj_scratchpadRead (FJ_SCRATCHPAD_INK_USAGE, &lInk, sizeof (dInk));
	//memcpy (&dInk, lInk, sizeof (dInk));
	fj_scratchpadRead (FJ_SCRATCHPAD_INK_USAGE, &dInk, sizeof (dInk));
	
	if (dInk < 0.0)			dInk = 0.0;
	else if (dInk > dMax)	dInk = dMax;
		
	return dInk;
}

void fj_scratchpadWritePrintState (BOOL bPrintIdle)
{
	ULONG lIdle = bPrintIdle;
	
	fj_scratchpadWrite (FJ_SCRATCHPAD_PRINT_STATE, &lIdle, sizeof (lIdle));		
	//printf ("%s(%d): fj_scratchpadWritePrintState: %d\n", __FILE__, __LINE__, lIdle);
}

BOOL fj_scratchpadReadPrintState ()
{
	ULONG lIdle = 0;
	
	fj_scratchpadRead (FJ_SCRATCHPAD_PRINT_STATE, &lIdle, sizeof (lIdle));		
	
	return lIdle ? TRUE : FALSE;
}
