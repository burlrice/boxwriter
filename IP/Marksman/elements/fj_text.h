#ifndef FJ_TEXT_H_
#define FJ_TEXT_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjfont;

typedef struct fjtext
{
	LONG     lBoldValue;						// amount of boldness. 0 origin.
	LONG     lWidthValue;						// amount of width stretch. 1 origin.
	LONG     lGapValue;							// amount of gap between characters
	CHAR	 cAlign;							// alignment (L, R, C)
	STR      strFontName[1+FJFONT_NAME_SIZE];	// font name
	STR      strText[1+FJTEXT_SIZE];			// text string
	struct fjfont FAR * pff;					// pointer to font structure
} FJTEXT, FAR *LPFJTEXT, FAR * const CLPFJTEXT;
typedef const struct fjtext FAR *LPCFJTEXT, FAR * const CLPCFJTEXT;

#endif /*FJ_TEXT_H_*/
