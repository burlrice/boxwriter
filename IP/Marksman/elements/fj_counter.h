#ifndef FJ_COUNTER_H_
#define FJ_COUNTER_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjelement;

typedef struct fjcounter
{
	LONG   lBoldValue;					// amount of boldness. 0 origin.
	LONG   lWidthValue;					// amount of width stretch. 1 origin.
	LONG   lGapValue;					// amount of gap between characters
	LONG   lChange;						// Value for change of counter
	LONG   lRepeat;						// Repeating value of counter
	LONG   lStart;						// Start value of counter
	LONG   lLimit;						// Limit value of counter
	LONG   lCurValue;					// Current value of counter
	LONG   lOldValue;					// Previous value of counter
	LONG   lCurRepeat;					// Current value of repeat of counter
	LONG   lLength;						// length of string output. 0 = 'natural' ( no alteration )
	CHAR   cAlign;						// alignment of value in larger length - L/R
	CHAR   cFill;						// fill character. 0 is no fill.
										// font name
	STR    strFontName[1+FJFONT_NAME_SIZE];
	struct fjelement FAR *pfeText;		// element for text generation
} FJCOUNTER, FAR *LPFJCOUNTER, FAR * const CLPFJCOUNTER;
typedef const struct fjcounter FAR *LPCFJCOUNTER, FAR * const CLPCFJCOUNTER;

#endif /*FJ_COUNTER_H_*/
