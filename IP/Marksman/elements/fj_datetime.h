#ifndef FJ_DATETIME_H_
#define FJ_DATETIME_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjelement;

enum FJDTTYPE
{
	DT_TIME_NORMAL = 0x00,
	DT_TIME_EXPIRATION,
	DT_TIME_ROLLOVER,
	DT_TIME_ROLL_EXP
};

enum FJDTFORMAT
{
	DT_DATE_FIRST     =  1,
	DT_DATE_YYYYMMDD  =  1,
	DT_DATE_DDMMYYYY,
	DT_DATE_MMDDYYYY,
	DT_DATE_YYYY,
	DT_DATE_MM,
	DT_DATE_DD,
	DT_DATE_YEAR2     = 10,				// date codes equal or greater have 2 digit year
	DT_DATE_YYMMDD,
	DT_DATE_DDMMYY,
	DT_DATE_MMDDYY,
	DT_DATE_YY,
	DT_DATE_DAYWK,						// day of week. 1-7. 1 = Sunday.
	DT_DATE_WK,
	DT_DATE_JULIAN,
	DT_DATE_MONTHA,						// alphabetic month
	DT_DATE_DAYWKA,						// alphabetic day of week
	DT_DATE_LAST      = 30,
	DT_TIME_FIRST,
	DT_TIME_HHMMSS24,
	DT_TIME_HHMMSS12,
	DT_TIME_HHMMSS12M,
	DT_TIME_HHMM24,
	DT_TIME_HHMM12,
	DT_TIME_HHMM12M,
	DT_TIME_HHMMLAST,					// time codes equal or lower all have hours and minutes
	DT_TIME_HH,
	DT_TIME_MM,
	DT_TIME_SS,
	DT_TIME_AMPM,						// for AM/PM only
	DT_TIME_HOURA,						// alphabetic hour
	DT_TIME_SHIFT,						// shift code
	DT_TIME_LAST
};

typedef struct fjdatetime
{
	LONG     lBoldValue;				// amount of boldness. 0 origin.
	LONG     lWidthValue;				// amount of width stretch. 1 origin.
	LONG     lGapValue;					// amount of gap between characters
	enum FJDTFORMAT lFormat;			// format of the date/time
	enum FJDTTYPE   lTimeType;			// which date/time
	LONG     lLength;					// length of string output. 0 = 'natural' ( no alteration )
	CHAR     cAlign;					// alignment of value in larger length - L/R
	CHAR     cFill;						// fill character. 0 is no fill.
	CHAR     cDelim;					// delimiter to use between date/time parts
	BOOL     bStringNormal;				// which set of strings to use for alphabetic formats
	LONG     lHourACount;				// number of time periods in 24 hours
										// font name
	STR      strFontName[1+FJFONT_NAME_SIZE];
	struct fjelement FAR *pfeText;		// element for text generation
} FJDATETIME, FAR *LPFJDATETIME, FAR * const CLPFJDATETIME;
typedef const struct fjdatetime FAR *LPCFJDATETIME, FAR * const CLPCFJDATETIME;

#endif /*FJ_DATETIME_H_*/
