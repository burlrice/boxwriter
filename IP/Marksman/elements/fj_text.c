#include "fj.h"
#include "fj_mem.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

// used for MemStor identification
const  CHAR fj_TextID[]   = "Text";

//
//  FUNCTION: fj_TextNew()
//
//  PURPOSE:  Create a new FJTEXT structure
//
FJDLLExport LPFJTEXT fj_TextNew( VOID )
{
	LPFJTEXT pft;

	pft = (LPFJTEXT)fj_calloc( 1, sizeof (FJTEXT) );

	pft->lBoldValue     =  0;			// amount of boldness. 0 origin.
	pft->lWidthValue    =  1;			// amount of width stretch. 1 origin.
	pft->lGapValue      =  0;			// amount of gap. 0 = vertical
	pft->strFontName[0] = 0;			// font name
	pft->strText[0]     = 0;			// text string
	pft->pff            = NULL;			// pointer to font structure
	pft->cAlign			= 'L';
	return( pft );
}
//
//  FUNCTION: fj_TextDestroy()
//
//  PURPOSE:  Destroy a FJTEXT structure
//
FJDLLExport VOID fj_TextDestroy( LPFJELEMENT pfe )
{
	LPFJTEXT pft;

	pft = (LPFJTEXT)pfe->pDesc;
	if (NULL != pft)
	{
		fj_free( pft );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_TextDup()
//
//  PURPOSE:  Duplicate a FJTEXT structure
//
FJDLLExport LPFJTEXT fj_TextDup( CLPCFJTEXT pftIn )
{
	LPFJTEXT pftDup;

	pftDup = (LPFJTEXT)fj_malloc( sizeof(FJTEXT) );
	memcpy( pftDup, pftIn, sizeof(FJTEXT) );

	return( pftDup );
}

static LPCSTR fj_TextGetLine (LPCSTR lpsz, int nLine)
{
	if (nLine > 1) {
		UINT i;
		int nCurrentLines = 1;

		for (i = 0; lpsz [i] != '\0'; i++) {
			if (lpsz [i] == '\n')
				nCurrentLines++;
			else if (nCurrentLines == nLine)
				return &lpsz [i];
		}
	}

	return lpsz;
}

static int fj_TextCountLines (LPCSTR lpsz)
{
	UINT i;
	int nLines;

	nLines = 1;

	for (i = 0; lpsz [i] != '\0'; i++)
		if (lpsz [i] == '\n')
			nLines++;

	return nLines;
}

static int fj_TextCalcWidth (LPCSTR lpsz, int nLine, LPFJFONT pff)
{
	UINT i;
	int nCurrentLine;
	LONG lLen;
	LONG lMaxLen;

	lLen			= 0;
	lMaxLen			= 0;
	nCurrentLine	= 1;

	if (pff) {
		if (pff->pBuffer != NULL && pff->pWBuffer != NULL) {
			for (i = 0; lpsz [i] != '\0'; i++) {
				if (lpsz [i] == '\n') {
					if (nCurrentLine == nLine)
						return lLen;

					nCurrentLine++;
					lMaxLen = max (lLen, lMaxLen);
					lLen = 0;
				}
				else {
					int nIndex;
					LPUCHAR pRast;

					nIndex = lpsz [i] & 0xff;

					if (nIndex < pff->lFirstChar) nIndex = pff->lFirstChar;
					if (nIndex > pff->lLastChar ) nIndex = pff->lLastChar;

					nIndex -= pff->lFirstChar; // position char in font
					pRast = pff->pBuffer + pff->pWBuffer [nIndex];
					lLen += (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
				}
			}
		}
	}

	if (nLine == -1)
		return max (lLen, lMaxLen);

	return lLen;
}

static int fj_TextCountChars (LPCSTR lpsz, int nLine)
{
	UINT i;
	int nChars;
	int nMaxChars;
	int nCurrentLine;

	nChars			= 0;
	nMaxChars		= 0;
	nCurrentLine	= 1;

	for (i = 0; lpsz [i] != '\0'; i++) {
		if (lpsz [i] == '\n') {
			if (nCurrentLine == nLine)
				return nChars;

			nCurrentLine++;
			nMaxChars = max (nChars, nMaxChars);
			nChars = 0;
		}
		else {
			nChars++;
		}
	}

	if (nLine == -1)
		return max (nChars, nMaxChars);
	
	return nChars;
}


FJDLLExport VOID fj_TextCreateImage( CLPFJELEMENT pfe )
{
	CLPFJTEXT     pft = (CLPFJTEXT)pfe->pDesc;
	LPFJIMAGE     pfi = pfe->pfi;
	LPFJPRINTHEAD pfph;
	LPFJIMAGE     pfiOld;
	LPBYTE        pBuffer;
	LONG ret;
	//static int size = 8 *sizeof(int);
	LONG   lBufferBase;
	LPUCHAR  font_byte;
	LPFJFONT pff;
	LONG     lHeight;
	LONG     lBytes;
	LONG     lBits;
	int nLines;
	ULONG    sOffset;
	LPUCHAR  pRast;
	int i, j;

	ret = 0;

	// pre-process
	// get what we need to build FJIMAGE

	pfph = pfe->pfm->pfph;
	pft->pff = fj_PrintHeadBuildFont( pfph, pft->strFontName );

	// if there is no requested font try to use defaul one
	if (pft->pff == NULL) 
		pft->pff = fj_PrintHeadGetDefaultFont( pfph );

	// if there is no fonts at all - exit
	if (pft->pff == NULL) 
		return;

	// build FJIMAGE

	pff = pft->pff;
	font_byte = pff->pBuffer;
	nLines = fj_TextCountLines (pft->strText);
	lHeight = pff->lHeight * nLines;
	lBytes  = (pff->lHeight + 7) / 8;
	lBits = 0;
	lBufferBase = 0;

	// is the font OK? could be parsing error or out of memory
	if ( (NULL != pff->pBuffer) && (NULL != pff->pWBuffer) )
	{
		LONG lMaxLen	= fj_TextCalcWidth (pft->strText, -1, pft->pff);
		int nMaxChars	= fj_TextCountChars (pft->strText, -1);
		LONG lBufferLen = (lMaxLen + (nMaxChars * pft->lGapValue)) * lBytes * nLines;

		pfi = fj_ImageNew( lBufferLen );

		if (pfi != NULL) {
			int nCurrentLine;

			pfi->lHeight = lHeight;
			pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);

			if (nLines == 1) {
				for (i = 0; i < nMaxChars; i++) {
					int nIndex = (int)pft->strText [i] & 0xff;
					int nCharBufferSize;

					if (nIndex < pff->lFirstChar) (nIndex = pff->lFirstChar);
					if (nIndex > pff->lLastChar ) (nIndex = pff->lLastChar );
										
					nIndex -= pff->lFirstChar; // position char in font
					sOffset = pff->pWBuffer[nIndex]; // size of char in font

					pRast = pff->pBuffer + sOffset;
					nCharBufferSize = (((*(pRast))<<8)&0x0000ff00) | *(pRast+1); // set real position in buf
										
					nIndex = pff->pWBuffer [nIndex];
					nIndex += 2;			// skip Width val
					
					{ // special case for one line of text
						UCHAR * pSrc = &font_byte [nIndex];
						UCHAR * pDest = &pBuffer [lBufferBase];
						int nLen = nCharBufferSize * lBytes;

						//copy entire char from font file with one memcpy
						memcpy (pDest, pSrc, nLen);

						lBufferBase += nLen;
						lBufferBase += (pft->lGapValue * nLines * lBytes); // + gap
					}
				}
			}
			else {
				for (nCurrentLine = 1; nCurrentLine <= nLines; nCurrentLine++) {
					int nChars = fj_TextCountChars (pft->strText, nCurrentLine);
					LPCSTR lpsz = fj_TextGetLine (pft->strText, nCurrentLine);
					LONG lLenDiff = lMaxLen - fj_TextCalcWidth (pft->strText, nCurrentLine, pft->pff);

					switch (pft->cAlign) {
					default:
					case 'L': case 'l':	lBufferBase = 0;									break;
					case 'C': case 'c':	lBufferBase = ((lLenDiff / 2) * nLines * lBytes);	break;
					case 'R': case 'r':	lBufferBase = ((lLenDiff) * nLines * lBytes);		break;
					}

					for (i = 0; i < nChars; i++) {
						int nIndex = (int)lpsz [i] & 0xff;
						int nCharBufferSize;

						if (nIndex < pff->lFirstChar) (nIndex = pff->lFirstChar);
						if (nIndex > pff->lLastChar ) (nIndex = pff->lLastChar );
											
						nIndex -= pff->lFirstChar; // position char in font
						sOffset = pff->pWBuffer[nIndex]; // size of char in font

						pRast = pff->pBuffer + sOffset;
						nCharBufferSize = (((*(pRast))<<8)&0x0000ff00) | *(pRast+1); // set real position in buf
											
						nIndex = pff->pWBuffer [nIndex];
						nIndex += 2;			// skip Width val
						
						for (j = 0; j < nCharBufferSize; j++) {
							int nFontOffset = (j * lBytes);
							int nBufferOffset = ((nLines - nCurrentLine) * lBytes) + (j * nLines * lBytes);
							UCHAR * pSrc = &font_byte [nIndex + nFontOffset];
							UCHAR * pDest = &pBuffer [lBufferBase + nBufferOffset];

							memcpy (pDest, pSrc, lBytes);
						}

						lBufferBase += (nCharBufferSize * nLines * lBytes);
						lBufferBase += (pft->lGapValue * nLines * lBytes); // + gap
					}
				}
			}

			{
				LPBYTE pBuffer = (LPBYTE)pfi + sizeof (FJIMAGE);
				int nByte;

				for (nByte = 0; nByte < pfi->lLength; nByte++ )
					lBits += fj_ImageCountBits[*(pBuffer + nByte)];
			}

			pfi->lBits = lBits;
			// bold and width are done in each element's routines.
			// they are sometimes omitted, depending on the element.
			// slant and transforms are usually 'global' and usually part of the 'environment',
			//    and they are not wanted for some special cases; e.g., text in a barcode.
			// bold first
			if ( 0 < pft->lBoldValue  ) pfi = fj_ImageBold(  pfi, pft->lBoldValue );
			if ( 1 < pft->lWidthValue ) pfi = fj_ImageWidth( pfi, pft->lWidthValue );
		}
	}

	pfiOld = pfe->pfi;					// do not reuse old image
	pfe->pfi = pfi;						// put new image into pfe
	if ( NULL != pfiOld )				// release old one
	{
		fj_ImageRelease( pfiOld );
	}
	return;
}
/*
FJDLLExport VOID fj_TextCreateImage( CLPFJELEMENT pfe )
{
	CLPFJTEXT     pft = (CLPFJTEXT)pfe->pDesc;
	LPFJIMAGE     pfi = pfe->pfi;
	LPFJPRINTHEAD pfph;
	LPFJIMAGE     pfiOld;
	LPBYTE        pBuffer;
	LONG ret;
	int i,j,jj;
	static int size = 8 *sizeof(int);
	LONG   sRBufLen;
	UCHAR    ch;
	LPSTR    in_char;
	LPUCHAR  font_byte;
	LPUCHAR  pRast;
	LPFJFONT pff;
	LONG     lHeight;
	LONG     lBytes;
	LONG     lLength;
	LONG     lBits;
	LONG     in_length;
	LONG     index;
	LONG     gap, gap_fl;
	ULONG    sOffset;

	ret = 0;

	// pre-process
	// get what we need to build FJIMAGE

	pfph = pfe->pfm->pfph;
	pft->pff = fj_PrintHeadBuildFont( pfph, pft->strFontName );

	// if there is no requested font try to use defaul one
	if( NULL == pft->pff ) pft->pff = fj_PrintHeadGetDefaultFont( pfph );

	// if there is no fonts at all - exit
	if(  NULL == pft->pff ) return;

	// build FJIMAGE

	in_char = pft->strText;
	gap_fl = 0;							// 0 for skip gap len for 1st char

	in_length = strlen(in_char);

	pff = pft->pff;
	font_byte = pff->pBuffer;
	lHeight = pff->lHeight;
	lBytes  = (pff->lHeight + 7) / 8;
	lBits = 0;
	sRBufLen = 0;
#ifdef TRUETYPE
	if ((!strcmp (pff->pFontFileName + strlen (pff->pFontFileName) -3,"TTF")) ||
		(!strcmp (pff->pFontFileName + strlen (pff->pFontFileName) -3,"ttf")))
	{
		FT_Library  library;			// handle to library     
		FT_Face     face;				// handle to face object 
		FT_BBox  bbox;
		FT_Glyph    glyph;
		ULONG FWidth;
		unsigned char* NewChar;			// = (unsigned char*)calloc((FntWidth+2) * FntByte, 1);
		unsigned char* pNewChar;		// = NewChar + FntByte*bbox.xMax;
		unsigned char* pSrcBuf;
		unsigned char* ptr_prev;
		ULONG prev_size;
		int a,b,a_bit,b_bit,sft_src,sft_dst;

		if (0 != FT_Init_FreeType( &library ))
			exit (-1);					// Couldn't init library
		if (0 !=  FT_New_Memory_Face( library, pft->pFont->pBuffer, pft->pFont->lBufLen, 0, &face ))
			exit (-1);
										// handle to face object 
		if (0 != FT_Set_Pixel_Sizes( face,
										// pixel_width   
			prf->lHeight * (pfi->lBoldValue + 1),
			prf->lHeight ))				// pixel_height 
			exit (-1);					// Couldn't set pixel size

		gap = pfi->lGapValue * lBytes;

		ptr_prev = NULL;
		prev_size = 0;
		sRBufLen = 0;
		pfi->lLength = 0;

		for ( i=0; i<in_length; i++ )
		{
			index = in_char[i] & 0xff;
			if (index < prf->lFirstChar) (index = prf->lFirstChar);
			if (index > prf->lLastChar ) (index = prf->lLastChar );
			if (0 != FT_Load_Char( face, index, FT_LOAD_RENDER  | FT_LOAD_MONOCHROME))
				continue;
			if (0 != FT_Get_Glyph( face->glyph, &glyph ))
				continue;
			FT_Glyph_Get_CBox(glyph, ft_glyph_bbox_pixels, &bbox );

			//			if (face->glyph->metrics.horiAdvance/64 > face->glyph->bitmap.width )
			//				FWidth = face->glyph->metrics.horiAdvance/64;
			//			else
			//				FWidth = face->glyph->bitmap.width;
			if (face->glyph->metrics.horiAdvance/64 > bbox.xMax)
				FWidth = face->glyph->metrics.horiAdvance/64;
			else
				FWidth = bbox.xMax;

			pfi->lLength = FWidth * lBytes + prev_size;
										// Calculate size of buf.
			pfi->lLength += pfi->lGapValue * lBytes;
			// Allocate buf.+ gap
			pfi->pBuffer = (LPUCHAR)fj_calloc( pfi->lLength + pfi->lHeight * lBytes, 1);
			if (ptr_prev != NULL)
			{
				memcpy (pfi->pBuffer, ptr_prev, prev_size);
				fj_free (ptr_prev);
			}
			ptr_prev = pfi->pBuffer;

			a_bit = pfi->lHeight - pfi->lHeight/4 - bbox.yMax;
			a_bit += lBytes * 8 - pfi->lHeight;
			b_bit = 0;
			sft_src =0;
			sft_dst = a_bit/8;
			a_bit = a_bit - sft_dst * 8 - 1;

			NewChar = pfi->pBuffer + prev_size;

			for (a = 0; a < face->glyph->bitmap.rows; a++)
			{
				if (a_bit == 8)
				{
					a_bit = 0;
					sft_dst++;
				}
				// set pointer to 1st row & to current byte
				pNewChar = NewChar + lBytes - 1 - sft_dst;
				// Add X shift
				pNewChar += bbox.xMin <= 0 ? 0 : bbox.xMin * lBytes;

				pSrcBuf = face->glyph->bitmap.buffer + a * face->glyph->bitmap.pitch;

				for (b = 0, b_bit = 0; b < face->glyph->bitmap.width; b++)
				{
					*pNewChar |= ((*pSrcBuf <<b_bit) & 0x80) >> (7-a_bit);
					b_bit++;
					if (b_bit == 8)
					{
						b_bit = 0;
						pSrcBuf++;
					}
					pNewChar += lBytes;
				}
				a_bit++;
			}
			prev_size = pfi->lLength;
			FT_Done_Glyph (glyph);
		}
		FT_Done_FreeType( library );
	}
	else
#endif TRUETYPE
	{
		// is the font OK? could be parsing error or out of memory
		if ( (NULL != pff->pBuffer) && (NULL != pff->pWBuffer) )
		{
			for (i=0; i< in_length; i++)
			{
				index = in_char[i] & 0xff;
				if (index < pff->lFirstChar) (index = pff->lFirstChar);
				if (index > pff->lLastChar ) (index = pff->lLastChar );
										// position char in font
				index -= pff->lFirstChar;
				pRast = pff->pBuffer + pff->pWBuffer[index];
				sRBufLen += (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
			}

			// Calculate size of buf.
			lLength = (sRBufLen + (in_length * pft->lGapValue)) * lBytes;
			pfi = fj_ImageNew( lLength );
			if ( NULL != pfi )
			{
				pfi->lHeight = lHeight;
				pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);

				sRBufLen = 0;
				for ( i=0; i<in_length; i++ )
				{
					gap = pft->lGapValue * lBytes;
					index = in_char[i] & 0xff;
					if (index < pff->lFirstChar) (index = pff->lFirstChar);
					if (index > pff->lLastChar ) (index = pff->lLastChar );
										// position char in font
					index -= pff->lFirstChar;
					// size of char in font
					sOffset = pff->pWBuffer[index];

					pRast = pff->pBuffer + sOffset;
					jj = (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
					jj = jj * lBytes;
										// set real position in buf
					index = pff->pWBuffer [index];
					index += 2;			// skip Width val
					for ( j=0; j<jj; j++ )
					{
						ch = font_byte[ index + j ];
						pBuffer[ sRBufLen + j + gap_fl] = ch;
										// count the number of bits/drops
						lBits += fj_ImageCountBits[ch];
					}					// end for
					while (gap > 0)
					{
						pBuffer[ sRBufLen + j + gap_fl + gap-1] = 0;
						gap--;
					}
					gap_fl += pft->lGapValue * lBytes;
					sRBufLen += jj;
				}
				pfi->lBits = lBits;
				// bold and width are done in each element's routines.
				// they are sometimes omitted, depending on the element.
				// slant and transforms are usually 'global' and usually part of the 'environment',
				//    and they are not wanted for some special cases; e.g., text in a barcode.
				// bold first
				if ( 0 < pft->lBoldValue  ) pfi = fj_ImageBold(  pfi, pft->lBoldValue );
				if ( 1 < pft->lWidthValue ) pfi = fj_ImageWidth( pfi, pft->lWidthValue );
			}
		}
	}

	pfiOld = pfe->pfi;					// do not reuse old image
	pfe->pfi = pfi;						// put new image into pfe
	if ( NULL != pfiOld )				// release old one
	{
		fj_ImageRelease( pfiOld );
	}
	return;
}
*/

//
//   Text does not change
//
FJDLLExport VOID fj_TextPhotocellBuild( CLPFJELEMENT pfe )
{
	return;
}
//
//  FUNCTION: fj_TextToStr()
//
//  PURPOSE:  Convert a FJTEXT structure into a descriptive string
//
FJDLLExport VOID fj_TextToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJTEXT pft = (CLPCFJTEXT)pfe->pDesc;
	STR sName[FJTEXT_NAME_SIZE*2];

										// text element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	if (NULL != pStr) sprintf (pStr,"{%s,%s,%f,%d,%c,%d,%d,%d,%c,%c,%c,",
			fj_TextID,
			sName,						// text element Name
			pfe->fLeft,					// distance from left edge of box
			pfe->lRow,					// row number to start pixels - 0 = bottom of print head
			(CHAR)pfe->lPosition,		// type of positioning
			pft->lBoldValue,			// amount of boldness. 0 origin.
			pft->lWidthValue,			// amount of width stretch. 1 origin.
			pft->lGapValue,				// amount of gap. 0 = vertical
										// true for invert
			(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F') );
	// font name
	fj_processParameterStringOutput( pStr+strlen(pStr), pft->strFontName );
	strcat( pStr, "," );
	// add text string
	fj_processParameterStringOutput( pStr+strlen(pStr), pft->strText );

	switch (pft->cAlign) {
	default:
	case 'L': case 'l':	strcat( pStr, ",L" );	break;
	case 'C': case 'c':	strcat( pStr, ",C" );	break;
	case 'R': case 'r':	strcat( pStr, ",R" );	break;
	}

	strcat( pStr, "}" );
	return;
}
//
//  FUNCTION: fj_TextFromStr()
//
//  PURPOSE:  Convert a descriptive string into a FJTEXT structure
//
FJDLLExport VOID fj_TextFromStr( LPFJELEMENT pfe, LPCSTR pStr )
{
#define TEXT_PARAMETERS 14
	LPFJTEXT pft;
	LPSTR  pParams[TEXT_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pft = (LPFJTEXT)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJTEXT_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJTEXT_MEMSTOR_SIZE );
	*(pStrM+FJTEXT_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, TEXT_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_TextID ) ) res = 0;

	if ( res >= (TEXT_PARAMETERS - 1))	
	{
		pfe->lTransform  =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft       = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow        = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition   =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pft->lBoldValue  = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pft->lWidthValue = atol(pParams[ 6]);
										// amount of gap. 0 = vertical
		pft->lGapValue   = atol(pParams[ 7]);
		// true for inverse
		pfe->lTransform |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[10]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
		fj_processParameterStringInput( pParams[11] );
		// font name
		strncpy( pft->strFontName, pParams[11], FJFONT_NAME_SIZE ); pft->strFontName[FJFONT_NAME_SIZE] = 0;
		fj_processParameterStringInput( pParams[12] );
		// text string
		strncpy( pft->strText      , pParams[12], FJTEXT_SIZE ); pft->strText[FJTEXT_SIZE] = 0;

		pft->cAlign = 'L';
	}

	if ( res >= TEXT_PARAMETERS) // for backward compatibility
		pft->cAlign = *(pParams[13]);

	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_TextGetType
//
//	Return: Text type
//
//
FJDLLExport enum FJELETYPE fj_TextGetType( VOID )
{

	return( FJ_TYPE_TEXT );
}
//
//	FUNCTION: fj_TextGetTypeString
//
//	Return: Pointer to string for Text type
//
//
FJDLLExport LPCSTR fj_TextGetTypeString( VOID )
{

	return( fj_TextID );
}
//
//	FUNCTION: fj_TextGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_TextGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJTEXT pft;

	pft = (LPFJTEXT)pfe->pDesc;
	strncpy( pStr, pft->strText, lMaxCount );
	*(pStr+lMaxCount-1) = 0;
	return;
}
FJCELEMACTION fj_TextActions =
{
	(LPVOID  (*)( VOID ))fj_TextNew,
	(LPVOID  (*)( LPVOID ))fj_TextDup,
	fj_TextDestroy,
	fj_TextCreateImage,
	fj_TextPhotocellBuild,
	fj_TextFromStr,
	fj_TextToStr,
	fj_TextGetTextString,
	fj_TextGetType,
	fj_TextGetTypeString
};
