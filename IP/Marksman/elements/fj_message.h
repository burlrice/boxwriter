#ifndef FJ_MESSAGE_H_
#define FJ_MESSAGE_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjmemstor;
struct fjelement;
struct fjprinthead;
struct fjlabel;

typedef struct fjmessage
{
	FLOAT   fLeft;						// summary info for all segments
	FLOAT   fRight;						// summary info for all segments
	LONG    lRow;						// summary info for all segments
	LONG    lHeight;					// summary info for all segments
	LONG    lBits;						// summary info for all segments
	LONG    lCount;						// count of segments
										// object name
	STR     strName[1+FJMESSAGE_NAME_SIZE];
	struct fjelement   FAR **papfe;		// pointer to array of pointers to FJELEMENT structures
	struct fjprinthead FAR  *pfph;		// pointer to owning FJPRINTHEAD structure
	struct fjlabel     FAR  *pfl;		// pointer to owning FJLABEL structure
} FJMESSAGE, FAR *LPFJMESSAGE, FAR * const CLPFJMESSAGE;
typedef const struct fjmessage FAR *LPCFJMESSAGE, FAR * const CLPCFJMESSAGE;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	// globals
	FJDLLExport LPFJMESSAGE fj_MessageNew( VOID );
	FJDLLExport LPFJMESSAGE fj_MessageDup( LPCFJMESSAGE pfmIn );
	FJDLLExport VOID        fj_MessageDestroy( LPFJMESSAGE pfm );
	FJDLLExport VOID        fj_MessageReset( LPFJMESSAGE pfm );
	FJDLLExport VOID        fj_MessagePhotocellBuild( LPFJMESSAGE pfm );
	FJDLLExport VOID        fj_MessageGetText( CLPCFJMESSAGE pfm, LPSTR pString, LONG lStringLength );
	FJDLLExport LONG        fj_MessageParseParameterStr( LPSTR pStr, LPSTR *pFirst );
	FJDLLExport LPFJMESSAGE fj_MessageBuildFromName( LPCSTR pName );
	FJDLLExport LPFJMESSAGE fj_MessageBuildFromString( LPCSTR pMessage );
#ifdef  __cplusplus
}
#endif

#endif /*FJ_MESSAGE_H_*/
