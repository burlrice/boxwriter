#include "fj.h"
#include "fj_mem.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_dyntext.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

#ifdef _WINDOWS
char lpszWinDynData [FJMESSAGE_DYNAMIC_SEGMENTS][FJTEXT_SIZE + 1];

FJDLLExport VOID fj_initWinDynData ()
{
	int i, j;

	memset (lpszWinDynData, 0, sizeof (char) * FJMESSAGE_DYNAMIC_SEGMENTS * FJTEXT_SIZE);

	for (i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
		for (j = 0; j < FJTEXT_SIZE; j++) {
			#ifdef _DEBUG
				lpszWinDynData [i][j] = 'W' - i;
			#else
				lpszWinDynData [i][j] = 'W';
			#endif
		}
	}
}

FJDLLExport BOOL fj_setWinDynData (int nIndex, LPCSTR lpsz)
{
	if (nIndex > 0 && nIndex <= FJMESSAGE_DYNAMIC_SEGMENTS) {
		strncpy (lpszWinDynData [nIndex - 1], lpsz, FJTEXT_SIZE);
		return TRUE;
	}

	return FALSE;
}

FJDLLExport BOOL fj_getWinDynData (int nIndex, LPSTR lpsz)
{
	if (nIndex > 0 && nIndex <= FJMESSAGE_DYNAMIC_SEGMENTS) {
		strncpy (lpsz, lpszWinDynData [nIndex - 1], FJTEXT_SIZE);
		return TRUE;
	}

	return FALSE;
}

#endif //_WINDOWS

// used for MemStor identification
const  CHAR fj_DynTextID[]   = "Dyntext";

//
//  FUNCTION: fj_DynTextNew()
//
//  PURPOSE:  Create a new FJDYNTEXT structure
//
FJDLLExport LPFJDYNTEXT fj_DynTextNew( VOID )
{
	LPFJDYNTEXT pfdt;

	pfdt = (LPFJDYNTEXT)fj_calloc( 1, sizeof (FJDYNTEXT) );

	pfdt->lBoldValue     = 0;			// amount of boldness. 0 origin.
	pfdt->lWidthValue    = 1;			// amount of width stretch. 1 origin.
	pfdt->lGapValue      = 0;			// amount of gap. 0 = vertical
	pfdt->lDynamicID     = 0;			// ID of dinamic text segment
	pfdt->lLength        = 0;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->lFirstChar     = 0;			// start pos in dinamic field
	pfdt->lNumChar       = 0;			// number of charecters to use in dinamic field. 0 = 'natural' ( no alteration )
	pfdt->cAlign         = 'R';			// alignment of value in larger length - L/R
	pfdt->cFill          = 0;			// fill character. 0 is no fill.
	pfdt->strFontName[0] = 0;			// font name
	pfdt->pff            = NULL;		// pointer to font structure
	return( pfdt );
}
//
//  FUNCTION: fj_DynTextDestroy()
//
//  PURPOSE:  Destroy a FJDYNTEXT structure
//
FJDLLExport VOID fj_DynTextDestroy( LPFJELEMENT pfe )
{
	LPFJDYNTEXT pfdt;

	pfdt = (LPFJDYNTEXT)pfe->pDesc;
	if (NULL != pfdt)
	{
		fj_free( pfdt );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_DynTextDup()
//
//  PURPOSE:  Duplicate a FJDYNTEXT structure
//
FJDLLExport LPFJDYNTEXT fj_DynTextDup( CLPCFJDYNTEXT pfdtIn )
{
	LPFJDYNTEXT pfdtDup;

	pfdtDup = (LPFJDYNTEXT)fj_malloc( sizeof(FJDYNTEXT) );
	memcpy( pfdtDup, pfdtIn, sizeof(FJDYNTEXT) );

	return( pfdtDup );
}
FJDLLExport VOID fj_DynTextCreateImage( CLPFJELEMENT pfe )
{
	CLPFJDYNTEXT     pfdt = (CLPFJDYNTEXT)pfe->pDesc;
#ifndef _WINDOWS
	LPFJ_GLOBAL        pGlobal = fj_CVT.pFJglobal;
#endif								//_WINDOWS
	LPFJIMAGE     pfi = pfe->pfi;
	LPFJPRINTHEAD pfph;
	LPFJIMAGE     pfiOld;
	LPBYTE        pBuffer;
	LONG ret;
	int i,j,jj;
	//static int size = 8 *sizeof(int);
	LONG   sRBufLen;
	UCHAR    ch;
	LPSTR    in_char;
	STR      pTempStr[1+FJTEXT_SIZE];
	STR      pTempStr1[1+FJTEXT_SIZE];
	LPUCHAR  font_byte;
	LPUCHAR  pRast;
	LPFJFONT pff;
	LONG     lHeight;
	LONG     lBytes;
	LONG     lLength;
	LONG     lBits;
	LONG     in_length;
	LONG     strLen;
	LONG     index;
	LONG     gap, gap_fl;
	ULONG    sOffset;

	ret = 0;

	// pre-process
	// get what we need to build FJIMAGE

	pfph = pfe->pfm->pfph;
	pfdt->pff = fj_PrintHeadBuildFont( pfph, pfdt->strFontName );

	// if there is no requested font try to use defaul one
	if( NULL == pfdt->pff ) pfdt->pff = fj_PrintHeadGetDefaultFont( pfph );

	// if there is no fonts at all - exit
	if(  NULL == pfdt->pff ) return;

	// build FJIMAGE

	memset(pTempStr, 0, FJTEXT_SIZE);
	memset(pTempStr1,0, FJTEXT_SIZE);
	gap_fl = 0;							// 0 for skip gap len for 1st char

#ifndef _WINDOWS
	in_length = strlen(pGlobal->DynamicSegments[pfdt->lDynamicID].strDynData);
	if ( in_length > pfdt->lFirstChar) strcpy(pTempStr, pGlobal->DynamicSegments[pfdt->lDynamicID].strDynData + pfdt->lFirstChar);
#else								// Burl
	in_length = strlen(lpszWinDynData [pfdt->lDynamicID]);
	if ( in_length > pfdt->lFirstChar)
		strncpy(pTempStr, (lpszWinDynData [pfdt->lDynamicID]) + pfdt->lFirstChar, pfdt->lNumChar);
#endif								//_WINDOWS

	if ( FJTEXT_SIZE > pfdt->lNumChar && 0 != pfdt->lNumChar ) pTempStr[pfdt->lNumChar] = 0;
	strLen = strlen( pTempStr );
	if ( strLen < pfdt->lLength && 0 != pfdt->cFill)
	{
		for ( i = 0; i < pfdt->lLength; i++ ) pTempStr1[i] = pfdt->cFill;
		in_char = pTempStr1;
		if ( 'R' == pfdt->cAlign ) in_char += (pfdt->lLength - strLen);
		for ( i = 0; i < strLen; i++ ) in_char[i] = pTempStr[i];
		in_char = pTempStr1;
	}
	else
	{
		in_char = pTempStr;
	}

	//TRACEF (pTempStr);
	
	in_length = strlen(in_char);

	pff = pfdt->pff;
	font_byte = pff->pBuffer;
	lHeight = pff->lHeight;
	lBytes  = (pff->lHeight + 7) / 8;
	lBits = 0;
	sRBufLen = 0;
#ifdef TRUETYPE
	if ((!strcmp (pff->pFontFileName + strlen (pff->pFontFileName) -3,"TTF")) ||
		(!strcmp (pff->pFontFileName + strlen (pff->pFontFileName) -3,"ttf")))
	{
		FT_Library  library;			/* handle to library     */
		FT_Face     face;				/* handle to face object */
		FT_BBox  bbox;
		FT_Glyph    glyph;
		ULONG FWidth;
		unsigned char* NewChar;			// = (unsigned char*)calloc((FntWidth+2) * FntByte, 1);
		unsigned char* pNewChar;		// = NewChar + FntByte*bbox.xMax;
		unsigned char* pSrcBuf;
		unsigned char* ptr_prev;
		ULONG prev_size;
		int a,b,a_bit,b_bit,sft_src,sft_dst;

		if (0 != FT_Init_FreeType( &library ))
			exit (-1);					// Couldn't init library
		if (0 !=  FT_New_Memory_Face( library, pft->pFont->pBuffer, pft->pFont->lBufLen, 0, &face ))
			exit (-1);
										/* handle to face object  */
		if (0 != FT_Set_Pixel_Sizes( face,
										/* pixel_width   */
			prf->lHeight * (pfi->lBoldValue + 1),
			prf->lHeight ))				/* pixel_height */
			exit (-1);					// Couldn't set pixel size

		gap = pfi->lGapValue * lBytes;

		ptr_prev = NULL;
		prev_size = 0;
		sRBufLen = 0;
		pfi->lLength = 0;

		for ( i=0; i<in_length; i++ )
		{
			index = in_char[i] & 0xff;
			if (index < prf->lFirstChar) (index = prf->lFirstChar);
			if (index > prf->lLastChar ) (index = prf->lLastChar );
			if (0 != FT_Load_Char( face, index, FT_LOAD_RENDER  | FT_LOAD_MONOCHROME))
				continue;
			if (0 != FT_Get_Glyph( face->glyph, &glyph ))
				continue;
			FT_Glyph_Get_CBox(glyph, ft_glyph_bbox_pixels, &bbox );

			//			if (face->glyph->metrics.horiAdvance/64 > face->glyph->bitmap.width )
			//				FWidth = face->glyph->metrics.horiAdvance/64;
			//			else
			//				FWidth = face->glyph->bitmap.width;
			if (face->glyph->metrics.horiAdvance/64 > bbox.xMax)
				FWidth = face->glyph->metrics.horiAdvance/64;
			else
				FWidth = bbox.xMax;

			pfi->lLength = FWidth * lBytes + prev_size;
										// Calculate size of buf.
			pfi->lLength += pfi->lGapValue * lBytes;
			// Allocate buf.+ gap
			pfi->pBuffer = (LPUCHAR)fj_calloc( pfi->lLength + pfi->lHeight * lBytes, 1);
			if (ptr_prev != NULL)
			{
				memcpy (pfi->pBuffer, ptr_prev, prev_size);
				fj_free (ptr_prev);
			}
			ptr_prev = pfi->pBuffer;

			a_bit = pfi->lHeight - pfi->lHeight/4 - bbox.yMax;
			a_bit += lBytes * 8 - pfi->lHeight;
			b_bit = 0;
			sft_src =0;
			sft_dst = a_bit/8;
			a_bit = a_bit - sft_dst * 8 - 1;

			NewChar = pfi->pBuffer + prev_size;

			for (a = 0; a < face->glyph->bitmap.rows; a++)
			{
				if (a_bit == 8)
				{
					a_bit = 0;
					sft_dst++;
				}
				// set pointer to 1st row & to current byte
				pNewChar = NewChar + lBytes - 1 - sft_dst;
				// Add X shift
				pNewChar += bbox.xMin <= 0 ? 0 : bbox.xMin * lBytes;

				pSrcBuf = face->glyph->bitmap.buffer + a * face->glyph->bitmap.pitch;

				for (b = 0, b_bit = 0; b < face->glyph->bitmap.width; b++)
				{
					*pNewChar |= ((*pSrcBuf <<b_bit) & 0x80) >> (7-a_bit);
					b_bit++;
					if (b_bit == 8)
					{
						b_bit = 0;
						pSrcBuf++;
					}
					pNewChar += lBytes;
				}
				a_bit++;
			}
			prev_size = pfi->lLength;
			FT_Done_Glyph (glyph);
		}
		FT_Done_FreeType( library );
	}
	else
#endif //TRUETYPE
	{
		// is the font OK? could be parsing error or out of memory
		if ( (NULL != pff->pBuffer) && (NULL != pff->pWBuffer) )
		{
			for (i=0; i< in_length; i++)
			{
				index = in_char[i] & 0xff;
				if (index < pff->lFirstChar) (index = pff->lFirstChar);
				if (index > pff->lLastChar ) (index = pff->lLastChar );
										// position char in font
				index -= pff->lFirstChar;
				pRast = pff->pBuffer + pff->pWBuffer[index];
				sRBufLen += (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
			}

			// Calculate size of buf.
			lLength = (sRBufLen + (in_length * pfdt->lGapValue)) * lBytes;
			pfi = fj_ImageNew( lLength );
			if ( NULL != pfi )
			{
				pfi->lHeight = lHeight;
				pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);

				sRBufLen = 0;
				for ( i=0; i<in_length; i++ )
				{
					gap = pfdt->lGapValue * lBytes;
					index = in_char[i] & 0xff;
					if (index < pff->lFirstChar) (index = pff->lFirstChar);
					if (index > pff->lLastChar ) (index = pff->lLastChar );
										// position char in font
					index -= pff->lFirstChar;
					// size of char in font
					sOffset = pff->pWBuffer[index];

					pRast = pff->pBuffer + sOffset;
					jj = (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
					jj = jj * lBytes;
										// set real position in buf
					index = pff->pWBuffer [index];
					index += 2;			// skip Width val
					for ( j=0; j<jj; j++ )
					{
						ch = font_byte[ index + j ];
						pBuffer[ sRBufLen + j + gap_fl] = ch;
										// count the number of bits/drops
						lBits += fj_ImageCountBits[ch];
					}					// end for
					while (gap > 0)
					{
						pBuffer[ sRBufLen + j + gap_fl + gap-1] = 0;
						gap--;
					}
					gap_fl += pfdt->lGapValue * lBytes;
					sRBufLen += jj;
				}
				pfi->lBits = lBits;
				// bold and width are done in each element's routines.
				// they are sometimes omitted, depending on the element.
				// slant and transforms are usually 'global' and usually part of the 'environment',
				//    and they are not wanted for some special cases; e.g., text in a barcode.
				// bold first
				if ( 0 < pfdt->lBoldValue  ) pfi = fj_ImageBold(  pfi, pfdt->lBoldValue );
				if ( 1 < pfdt->lWidthValue ) pfi = fj_ImageWidth( pfi, pfdt->lWidthValue );
#ifndef _WINDOWS
				// we just rebuild image, this flag will be update by web/socket/comm interface
				pGlobal->DynamicSegments[pfdt->lDynamicID].bChanged = FALSE;
#endif					//_WINDOWS
			}
		}
	}

	pfiOld = pfe->pfi;					// do not reuse old image
	pfe->pfi = pfi;						// put new image into pfe
	if ( NULL != pfiOld )				// release old one
	{
		fj_ImageRelease( pfiOld );
	}
	return;
}
//
//   DynText can change
//
FJDLLExport VOID fj_DynTextPhotocellBuild( CLPFJELEMENT pfe )
{
	fj_DynTextCreateImage( pfe );
	return;
}
//
//  FUNCTION: fj_DynTextToStr()
//
//  PURPOSE:  Convert a FJDYNTEXT structure into a descriptive string
//
FJDLLExport VOID fj_DynTextToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	LPFJDYNTEXT pfdt;
	STR sName[FJTEXT_NAME_SIZE*2];
	STR sFill[3];
	STR sFillQuote[5];

	pfdt = (LPFJDYNTEXT)pfe->pDesc;

	sFill[0]  = pfdt->cFill;
	sFill[1]  = 0;
										// fill character
	fj_processParameterStringOutput( sFillQuote,  sFill  );
										// text element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	if (NULL != pStr) sprintf (pStr,"{%s,%s,%f,%d,%c,%d,%d,%d,%d,%d,%d,%d,%c,%s,%c,%c,%c,",
			fj_DynTextID,
			sName,						// text element Name
			pfe->fLeft,					// distance from left edge of box
			pfe->lRow,					// row number to start pixels - 0 = bottom of print head
			(CHAR)pfe->lPosition,		// type of positioning
			pfdt->lBoldValue,			// amount of boldness. 0 origin.
			pfdt->lWidthValue,			// amount of width stretch. 1 origin.
			pfdt->lGapValue,			// amount of gap. 0 = vertical
			pfdt->lDynamicID,			// ID of dinamic text segment
			pfdt->lLength,				// length of string output. 0 = 'natural' ( no alteration )
			pfdt->lFirstChar,			// start pos in dinamic text segment
			pfdt->lNumChar,				// number of charecters to use. 0 = 'natural' ( no alteration )
			pfdt->cAlign,				// alignment of value in larger length - L/R
			sFillQuote,					// fill character. 0 is no fill.
										// true for invert
			(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F') );
	// font name
	fj_processParameterStringOutput( pStr+strlen(pStr), pfdt->strFontName );
	strcat( pStr, "}" );
	return;
}
//
//  FUNCTION: fj_DynTextFromStr()
//
//  PURPOSE:  Convert a descriptive string into a FJDYNTEXT structure
//
FJDLLExport VOID fj_DynTextFromStr( LPFJELEMENT pfe, LPCSTR pStr )
{
#define DYNTEXT_PARAMETERS 18
	LPFJDYNTEXT pfdt;
	LPSTR  pParams[DYNTEXT_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfdt = (LPFJDYNTEXT)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJTEXT_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJTEXT_MEMSTOR_SIZE );
	*(pStrM+FJTEXT_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, DYNTEXT_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_DynTextID ) ) res = 0;

	if ( res == DYNTEXT_PARAMETERS )
	{
		pfe->lTransform  =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft       = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow        = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition   =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfdt->lBoldValue  = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfdt->lWidthValue = atol(pParams[ 6]);
										// amount of gap. 0 = vertical
		pfdt->lGapValue   = atol(pParams[ 7]);
										// ID of dinamic text segment
		pfdt->lDynamicID  = atol(pParams[ 8]);
										// length of string output. 0 = 'natural' ( no alteration )
		pfdt->lLength     = atol(pParams[ 9]);
										// start pos in dinamic text segment
		pfdt->lFirstChar  = atol(pParams[10]);
										// number of charecters to use. 0 = 'natural' ( no alteration )
		pfdt->lNumChar    = atol(pParams[11]);
										// alignment of value in larger length - L/R
		pfdt->cAlign      = *(pParams[12]);
		fj_processParameterStringInput( pParams[13] );
										// fill character. 0 is no fill.
		pfdt->cFill       = *(pParams[13]);

		// true for inverse
		pfe->lTransform |=   (*(pParams[14]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[15]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[16]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
		fj_processParameterStringInput( pParams[17] );
		// font name
		strncpy( pfdt->strFontName, pParams[17], FJFONT_NAME_SIZE ); pfdt->strFontName[FJFONT_NAME_SIZE] = 0;
	}

	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_DynTextGetType
//
//	Return: DynText type
//
//
FJDLLExport enum FJELETYPE fj_DynTextGetType( VOID )
{

	return( FJ_TYPE_DYNTEXT );
}
//
//	FUNCTION: fj_DynTextGetTypeString
//
//	Return: Pointer to string for DynText type
//
//
FJDLLExport LPCSTR fj_DynTextGetTypeString( VOID )
{

	return( fj_DynTextID );
}
//
//	FUNCTION: fj_DynTextGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_DynTextGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJDYNTEXT pfdt;
#ifndef _WINDOWS
	LPFJ_GLOBAL        pGlobal = fj_CVT.pFJglobal;
#endif

	pfdt = (LPFJDYNTEXT)pfe->pDesc;
#ifndef _WINDOWS
	strncpy( pStr, pGlobal->DynamicSegments[pfdt->lDynamicID].strDynData, lMaxCount );
#else
	strncpy (pStr, lpszWinDynData [pfdt->lDynamicID], lMaxCount);
#endif								//_WINDOWS

	*(pStr+lMaxCount-1) = 0;
	return;
}
FJCELEMACTION fj_DynTextActions =
{
	(LPVOID  (*)( VOID ))fj_DynTextNew,
	(LPVOID  (*)( LPVOID ))fj_DynTextDup,
	fj_DynTextDestroy,
	fj_DynTextCreateImage,
	fj_DynTextPhotocellBuild,
	fj_DynTextFromStr,
	fj_DynTextToStr,
	fj_DynTextGetTextString,
	fj_DynTextGetType,
	fj_DynTextGetTypeString
};
