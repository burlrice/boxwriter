#include "fj.h"
#include "fj_mem.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_dynimage.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

// used for MemStor identification
const  CHAR fj_DynimageID[]   = "Dynimage";

//
//  FUNCTION: fj_DynimageNew()
//
//  PURPOSE:  Create a new FJDYNIMAGE structure
//
FJDLLExport LPFJDYNIMAGE fj_DynimageNew( VOID )
{
	LPFJDYNIMAGE pfd;

	pfd = (LPFJDYNIMAGE)fj_calloc( 1, sizeof (FJDYNIMAGE) );

	pfd->lBoldValue     = 8;			// amount of boldness. 0 origin.
	pfd->lWidthValue    = 1;			// amount of width stretch. 1 origin.
	pfd->lDynimageCount = 0;			// version of dynimage
	return( pfd );
}
//
//  FUNCTION: fj_DynimageDestroy()
//
//  PURPOSE:  Destroy a FJDYNIMAGE structure
//
FJDLLExport VOID fj_DynimageDestroy( LPFJELEMENT pfe )
{
	LPFJDYNIMAGE pfd;

	pfd = (LPFJDYNIMAGE)pfe->pDesc;
	if ( NULL != pfd)
	{
		fj_free( pfd );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_DynimageDup()
//
//  PURPOSE:  Duplicate a FJDYNIMAGE structure
//
FJDLLExport LPFJDYNIMAGE fj_DynimageDup( CLPCFJDYNIMAGE pfdIn )
{
	LPFJDYNIMAGE pfdDup;

	pfdDup = (LPFJDYNIMAGE)fj_malloc( sizeof(FJDYNIMAGE) );
	memcpy( pfdDup, pfdIn, sizeof(FJDYNIMAGE) );

	return( pfdDup );
}
FJDLLExport VOID fj_DynimageCreateImage( CLPFJELEMENT pfe )
{
	LPFJDYNIMAGE  pfd;
	LPFJIMAGE     pfi;
	LPFJIMAGE     pfiOld;
	LPFJIMAGE     pfiDyn;

	pfd = (LPFJDYNIMAGE)pfe->pDesc;

	pfiDyn = pfe->pfm->pfph->pfiDynimage;

	if ( NULL != pfiDyn )
	{
		pfi = pfiDyn;
		fj_ImageUse( pfi );
		pfd->lDynimageCount = pfe->pfm->pfph->lDynimageCount;

		// bold and width are done in each element's routines.
		// they are sometimes omitted, depending on the element.
		// slant and transforms are usually 'global' and usually part of the 'environment',
		//    and they are not wanted for some special cases; e.g., text in a barcode.
		if ( 0 < pfd->lBoldValue  ) pfi = fj_ImageBold(  pfi, pfd->lBoldValue );
		if ( 1 < pfd->lWidthValue ) pfi = fj_ImageWidth( pfi, pfd->lWidthValue );

		pfiOld = pfe->pfi;				// do not reuse image
		pfe->pfi = pfi;					// set new one
		if ( NULL != pfiOld )
		{
			fj_ImageRelease( pfiOld );
		}
	}

	return;
}
//
//   Dynimage does not change
//
FJDLLExport VOID fj_DynimagePhotocellBuild( CLPFJELEMENT pfe )
{
	CLPCFJDYNIMAGE pfd = (CLPCFJDYNIMAGE)pfe->pDesc;

	if ( pfd->lDynimageCount != pfe->pfm->pfph->lDynimageCount ) fj_DynimageCreateImage( pfe );
	return;
}
//
//  FUNCTION: fj_DynimageToString()
//
//  PURPOSE:  Convert a FJDYNIMAGE structure into a descriptive string with binary buffer
//
FJDLLExport VOID fj_DynimageToString( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJDYNIMAGE pfd = (CLPCFJDYNIMAGE)pfe->pDesc;
	STR  sName[FJDYNIMAGE_NAME_SIZE*2];

										// dynimage element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	sprintf ((CHAR*)pStr, "{%s,%s,%f,%d,%c,%d,%d,%c,%c,%c}",
		fj_DynimageID,
		sName,
		pfe->fLeft,						// distance from left edge of box
		pfe->lRow,						// row number to start pixels - 0 = bottom of print head
		(CHAR)pfe->lPosition,			// type of positioning
		pfd->lBoldValue,				// amount of boldness. 0 origin.
		pfd->lWidthValue,				// amount of width stretch. 1 origin.
										// true for invert
		(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F')
		);
	return;
}
//
//  FUNCTION: fj_DynimageFromString
//
//  PURPOSE:  Convert a descriptive string into a FJDYNIMAGE structure
//
FJDLLExport VOID fj_DynimageFromString( LPFJELEMENT pfe, LPBYTE pBuff )
{
#define DYNIMAGE_PARAMETERS 10
	LPFJDYNIMAGE pfd;
	LPSTR  pParams[DYNIMAGE_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfd = (LPFJDYNIMAGE)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJDYNIMAGE_MEMSTOR_SIZE+1 );
	strncpy( pStrM, (LPSTR)pBuff, FJDYNIMAGE_MEMSTOR_SIZE );
	*(pStrM+FJDYNIMAGE_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, DYNIMAGE_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_DynimageID ) ) res = 0;

	if ( res == DYNIMAGE_PARAMETERS )	// FJ_DYNIMAGE string must have 10 field delimiters!!!!!
	{
		pfe->lTransform   =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft        = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow         = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition    =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfd->lBoldValue   = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfd->lWidthValue  = atol(pParams[ 6]);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 7]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
	}
	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_DynimageGetType
//
//	Return: Dynimage type
//
//
FJDLLExport enum FJELETYPE fj_DynimageGetType( VOID )
{
	return( FJ_TYPE_DYNIMAGE );
}
//
//	FUNCTION: fj_DynimageGetTypeString
//
//	Return: Pointer to string for Dynimage type
//
//
FJDLLExport LPCSTR fj_DynimageGetTypeString( VOID )
{
	return( fj_DynimageID );
}
//
//	FUNCTION: fj_DynimageGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_DynimageGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	strncpy( pStr, "Dynamic Image", lMaxCount );
	*(pStr+lMaxCount-1) = 0;
	return;
}
FJCELEMACTION fj_DynimageActions =
{
	(LPVOID  (*)( VOID ))fj_DynimageNew,
	(LPVOID  (*)( LPVOID ))fj_DynimageDup,
	fj_DynimageDestroy,
	fj_DynimageCreateImage,
	fj_DynimagePhotocellBuild,
	fj_DynimageFromString,
	fj_DynimageToString,
	fj_DynimageGetTextString,
	fj_DynimageGetType,
	fj_DynimageGetTypeString
};

struct fjdyntable
{
	LPSTR pStr;
	VOID (*pGet)( CLPCFJIMAGE pfi, LPSTR  pString );
	VOID (*pSet)( CLPFJIMAGE  pfi, LPCSTR pString );
};

FJDLLExport VOID fj_DynSetHeight  (CLPFJIMAGE pfi, LPCSTR pStr){pfi->lHeight = (LONG)atoi(pStr);}
FJDLLExport VOID fj_DynSetColumns (CLPFJIMAGE pfi, LPCSTR pStr){pfi->lLength = pfi->lHeight*atoi(pStr);}
FJDLLExport VOID fj_DynSetLength  (CLPFJIMAGE pfi, LPCSTR pStr){pfi->lLength = (LONG)atoi(pStr);}

struct fjdyntable fj_DynParams[] =
{
#define DYN_PARAM_FIRST  0
	{ "IMAGE_HEIGHT",   NULL, fj_DynSetHeight,	},
	{ "IMAGE_COLUMNS",  NULL, fj_DynSetColumns,	},
	{ "IMAGE_LENGTH",   NULL, fj_DynSetLength,	},
#define DYN_PARAM_LAST  2
};
LONG fj_DynParamCount = sizeof(fj_DynParams)/sizeof(struct fjdyntable);

//
//	FUNCTION: fj_DynimagePutData
//
//	store the Dynimage image data
//
//
FJDLLExport VOID fj_DynimagePutData( LPFJPRINTHEAD pfph, LPBYTE pBuff )
{
	register LONG lLength;
	register LONG lBits;
	register LPBYTE pBufIn;
	register LPBYTE pBufOut;
	register LPBYTE pBufOutEnd;

	LPFJIMAGE pfi;
	LPFJIMAGE pfiOld;
	LPFJIMAGE pfiDummy;
	LPSTR pStr;
	LONG  lLen;
	BOOL bRet;

	pStr = (LPSTR)pBuff;
	lLen = strlen( pStr );
	pBuff += lLen + 1;

	pfiDummy = fj_ImageNew( 0 );
	bRet = fj_SetParametersFromString( (struct fjtable *)fj_DynParams, 0, DYN_PARAM_LAST, (LPVOID)pfiDummy, pStr );

	lLength = pfiDummy->lLength;

	pfi = fj_ImageNew( lLength );

	if ( NULL != pfi )
	{
		pBufIn  = pBuff;
		pBufOut = (LPBYTE)pfi + sizeof( FJIMAGE );
		pBufOutEnd = pBufOut + lLength;
		while ( pBufOut < pBufOutEnd )
		{
			lBits += fj_ImageCountBits[(*(pBufOut++)=*(pBufIn++))];
		}
		pfi->lBits = lBits;
		pfi->lHeight = pfiDummy->lHeight;
	}

	pfiOld = pfph->pfiDynimage;
	pfph->pfiDynimage = pfi;
	pfph->lDynimageCount++;
	if ( NULL != pfiOld )
	{
		fj_ImageRelease( pfiOld );
	}
	fj_ImageRelease( pfiDummy );

	return;
}
