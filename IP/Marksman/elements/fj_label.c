#include "fj.h"

#include <stdio.h>
#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_label.h"

// used for MemStor identification
const  CHAR fj_LabelID[]   = "Label";

FJDLLExport BOOL fj_IsValidLabelName (const char * psz)
{
	const char * strRestrict [] = 
	{
		"\\",
		"/",
		".",
		"'",
		"\"",
		"*",
	};
	int i;
	int nLen = strlen (psz);

	if (nLen <= 0 || nLen > FJMESSAGE_NAME_SIZE)
		return FALSE;

	for (i = 0; i < nLen; i++) {
		char c = psz [i];

		if (!isalnum (c))
			return FALSE;
	}

	for (i = 0; i < ARRAYSIZE (strRestrict); i++) 
		if (strstr (psz, strRestrict [i]) != NULL)
			return FALSE;
			
	
	return TRUE;
}

//
//  FUNCTION: fj_LabelNew()
//
//  PURPOSE:  Create a new FJLABEL structure
//
FJDLLExport LPFJLABEL fj_LabelNew( VOID )
{
	LPFJLABEL pfl;

	pfl = (LPFJLABEL)fj_calloc( 1, sizeof(FJLABEL) );
	pfl->fBoxWidth  = 10.0;				// width of box
	pfl->fBoxHeight = 10.0;				// height of box
	pfl->lExpValue  = 0;				// Datetime - expiration value
										// Datetime - expiration units
	pfl->lExpUnits		= FJ_LABEL_EXP_MONTHS;
	pfl->bExpRoundUp	= FALSE;			// Datetime - expiration should be round up to top of unit
	pfl->bExpRoundDown	= FALSE;			// Datetime - expiration should be round up to top of unit

	return( pfl );
}
//
//  FUNCTION: fj_LabelDup()
//
//  PURPOSE:  Duplicate a FJLABEL structure
//
FJDLLExport LPFJLABEL fj_LabelDup( CLPCFJLABEL pflIn )
{
	LPFJLABEL pflDup;

	pflDup = NULL;
	if ( NULL != pflIn )
	{
		pflDup = (LPFJLABEL)fj_malloc( sizeof(FJLABEL) );
		memcpy( pflDup, pflIn, sizeof(FJLABEL) );
	}
	return( pflDup );
}
//
//  FUNCTION: fj_LabelDestroy()
//
//  PURPOSE:  Destroy a FJLABEL structure
//
FJDLLExport VOID fj_LabelDestroy( LPFJLABEL pfl )
{
	if ( NULL != pfl )
	{
		fj_free( pfl );
	}
	return;
}
//  FUNCTION: fj_LabelToStr()
//
//  PURPOSE:  Convert a FJLABEL structure into a descriptive string
//
FJDLLExport VOID fj_LabelToStr( CLPCFJLABEL pfl, LPSTR pStr )
{
	STR sName[FJLABEL_NAME_SIZE*2];
	int i,n;
	char cRound;

	cRound = (pfl->bExpRoundUp == TRUE ? 'T' : 'F');

	if (pfl->bExpRoundDown)
		cRound = 'D';
										// label Name
	fj_processParameterStringOutput( sName, pfl->strName );
	sprintf( pStr, "{%s,%s,%f,%f,%d,%d,%c",
		fj_LabelID,
		sName,
		pfl->fBoxHeight,
		pfl->fBoxWidth,
		pfl->lExpValue,					// Datetime - expiration value
		pfl->lExpUnits,					// Datetime - expiration units
		cRound);

	for ( n = FJSYS_NUMBER_PRINTERS-1; n >= 0; n-- )
	{
		if ( 0 != pfl->fmMessageIDs[n][0] ) break;
	}
	for ( i = 0; i <= n; i++ )
	{
		strcat( pStr, "," );
		// message Name
		fj_processParameterStringOutput( sName, pfl->fmMessageIDs[i] );
		strcat( pStr, sName );
	}
	strcat( pStr, "}" );

	return;
}
//
//  FUNCTION: fj_LabelFromStr()
//
//  PURPOSE:  Convert a descriptive string into a FJLABEL structure
//
//
FJDLLExport LPFJLABEL fj_LabelFromStr( LPCSTR pStr )
{
#define LABEL_PARAM_BASE 7
#define LABEL_PARAMETERS (LABEL_PARAM_BASE+FJSYS_NUMBER_PRINTERS)
	LPFJLABEL pfl;
	LPSTR  pParams[LABEL_PARAMETERS];
	LPSTR  pStrM;
	int res;
	int i;

	printf ("%s(%d): fj_LabelFromStr: %s\n", __FILE__, __LINE__, pStr);
	pfl = NULL;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJLABEL_MEMSTOR_SIZE );
	strcpy( pStrM, pStr );

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, LABEL_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_LabelID ) ) res = 0;

	if ( res >= LABEL_PARAM_BASE )		// FJ_LABEL string must have 7 field delimiters!!!!!
	{
		pfl = fj_LabelNew();
		fj_processParameterStringInput( pParams[1] );
		strcpy( pfl->strName,pParams[1] );
		pfl->fBoxHeight = (FLOAT)atof(pParams[2]);
		pfl->fBoxWidth  = (FLOAT)atof(pParams[3]);
		pfl->lExpValue  = atoi(pParams[4]);
		pfl->lExpUnits  = atoi(pParams[5]);
		pfl->bExpRoundUp  = (*(pParams[6]) == 'T' ? TRUE : FALSE);
		pfl->bExpRoundDown = FALSE;

		if (*(pParams[6]) == 'D') {
			pfl->bExpRoundUp = FALSE;
			pfl->bExpRoundDown = TRUE;
		}

		res -= LABEL_PARAM_BASE;
		for ( i = 0; i < res; i++ )
		{
			fj_processParameterStringInput( pParams[LABEL_PARAM_BASE+i] );
			strcpy( pfl->fmMessageIDs[i], pParams[LABEL_PARAM_BASE+i] );
			printf ("%s(%d): pfl->fmMessageIDs [%d]: %s\n", __FILE__, __LINE__, i, pfl->fmMessageIDs [i]);
		}
	}

	fj_free( pStrM );
	return( pfl );
}
//
//	FUNCTION: fj_LabelBuildFromString
//
//	Return: TRUE if success.
//
//  Extract and build a FJLABEL from FJMEMSTOR
//
FJDLLExport LPFJLABEL fj_LabelBuildFromString( LPCSTR pLabel )
{
	LPFJLABEL pfl = NULL;
	CHAR      str[FJLABEL_MEMSTOR_SIZE];
	
	if (pLabel != NULL) {
		strcpy (str, pLabel);
		pfl = fj_LabelFromStr (str);
	}

	return( pfl );
}
//
//	FUNCTION: fj_LabelBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJLABEL from FJMEMSTOR
//
FJDLLExport LPFJLABEL fj_LabelBuildFromName( LPCSTR pName )
{
	LPFJLABEL pfl = NULL;
	char szFile [FJSYS_MAX_FILENAME] = { 0 };
	char szLabel [FJSYS_MAX_FILENAME] = { 0 };

	strcpy (szLabel, pName);
	
	if (szLabel [0] == '"')
		fj_processParameterStringInput (szLabel);
					
	sprintf (szFile, "%s/%s", FJ_DIR_LABELS, szLabel);
	int nLen = ReadFileSize (szFile);
	
	if (nLen > 0) {
		char * psz = fj_malloc (nLen);
		
		memset (psz, 0, nLen);
		
		if (ReadFile (&nLen, psz, szFile)) {
			char * pEnd = strchr (psz, '}');
			
			printf ("%s(%d): %s\n", __FILE__, __LINE__, psz);
			
			if (pEnd)
				* (pEnd + 1) = 0;
				
			printf ("%s(%d): %s\n", __FILE__, __LINE__, psz);
			
			pfl = fj_LabelBuildFromString (psz);
		}
		
		fj_free (psz);
	}
	
	return( pfl );
}
//
//	FUNCTION: fj_LabelAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJLABEL to FJMEMSTOR
//
FJDLLExport BOOL fj_LabelAddToMemstor( CLPCFJLABEL pfl )
{
	CHAR   buff[FJLABEL_MEMSTOR_SIZE];
	BOOL   bRetL = FALSE;
/* TODO

	bRetL = TRUE;

	if ( NULL != pfl )
	{
		fj_LabelToStr( pfl, buff );
		bRetL = fj_MemStorAddElementString( pfms, buff );
	}
*/
	return( bRetL );
}
