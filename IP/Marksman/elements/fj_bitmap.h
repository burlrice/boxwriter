#ifndef FJ_BITMAP_H_
#define FJ_BITMAP_H_

#include "fj_base.h"
#include "fj_defines.h"

#define REVERSEWORD32(word) ( ((word&0x000000ff)<<24) | ((word&0x0000ff00)<<8) | ((word&0x00ff0000)>>8) | ((word&0xff000000)>>24) )
#define REVERSEWORD16(word) ( ((word&0x000000ff)<<8) | ((word&0x0000ff00)>>8) )

#define LOADREVERSEWORD32(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) | (((*(((BYTE*)pointer)+2)))<<16) | (((*(((BYTE*)pointer)+3)))<<24) )
#define LOADREVERSEWORD16(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) )

//
// A bitmap element uses one color bitmaps that are independently stored in MemStor.
//

typedef struct fjbitmap
{
	LONG    lBoldValue;					// amount of boldness. 0 origin.
	LONG    lWidthValue;				// amount of width stretch. 1 origin.
	LONG    lbmpSize;					// size of bmp in bytes
	LPBYTE  pbmp;						// pointer to bmp
	STR     strName[1+FJBMP_NAME_SIZE];	// bmp name
} FJBITMAP, FAR *LPFJBITMAP, FAR * const CLPFJBITMAP;
typedef const struct fjbitmap FAR *LPCFJBITMAP, FAR * const CLPCFJBITMAP;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport BOOL fj_bmpCheckFormat( LPBYTE pbmp );
	//FJDLLExport fj_BitmapLoadFromMemory (LPFJBITMAP pfb, LPSTR pElement);
#ifdef  __cplusplus
}
#endif

//
// the following bitmap structures are copied from Windows
//
#ifdef NET_OS
//typedef unsigned short  WORD;
//typedef unsigned long  DWORD;

typedef struct tagBITMAPFILEHEADER		// bmfh
{
	WORD    bfType;
	DWORD   bfSize;
	WORD    bfReserved1;
	WORD    bfReserved2;
	DWORD   bfOffBits;
} __attribute__((__packed__)) BITMAPFILEHEADER;
typedef struct tagBITMAPINFOHEADER		// bmih
{
	DWORD  biSize;
	LONG   biWidth;
	LONG   biHeight;
	WORD   biPlanes;
	WORD   biBitCount;
	DWORD  biCompression;
	DWORD  biSizeImage;
	LONG   biXPelsPerMeter;
	LONG   biYPelsPerMeter;
	DWORD  biClrUsed;
	DWORD  biClrImportant;
} __attribute__((__packed__)) BITMAPINFOHEADER;

#endif

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport int sizeofBITMAPFILEHEADER (BITMAPFILEHEADER * p);
	FJDLLExport int sizeofBITMAPINFOHEADER (BITMAPINFOHEADER * p);
#ifdef  __cplusplus
}
#endif

#endif /*FJ_BITMAP_H_*/
