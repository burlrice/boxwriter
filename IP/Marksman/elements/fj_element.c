#include "fj.h"
#include <stdio.h>
#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

// globals
extern FJCELEMACTION fj_BarCodeActions;
extern FJCELEMACTION fj_BitmapActions;
extern FJCELEMACTION fj_CounterActions;
extern FJCELEMACTION fj_DatetimeActions;
extern FJCELEMACTION fj_DynimageActions;
extern FJCELEMACTION fj_TextActions;
extern FJCELEMACTION fj_DynTextActions;
extern FJCELEMACTION fj_DynBarCodeActions;
extern FJCELEMACTION fj_DataMatrixActions;

extern const CHAR fj_FontID[];
extern const CHAR fj_TextID[];
extern const CHAR fj_DynTextID[];
extern const CHAR fj_BarCodeID[];
extern const CHAR fj_DynBarCodeID[];
extern const CHAR fj_BitmapID[];
extern const CHAR fj_CounterID[];
extern const CHAR fj_DatetimeID[];
extern const CHAR fj_DynimageID[];
extern const CHAR fj_MessageID[];
extern const CHAR fj_LabelID[];
extern const CHAR fj_bmpID[];
extern const CHAR fj_DataMatrixID[];

//
//  FUNCTION: fj_ElementNew()
//
//  PURPOSE:  Create a new FJELEMENT structure
//
FJDLLExport LPFJELEMENT fj_ElementNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = (LPFJELEMENT)fj_calloc( 1, sizeof (FJELEMENT) );

	pfe->lRow         = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft        = 0.0;			// distance from left edge of box
										// type of positioning
	pfe->lPosition    = FJ_POSITION_DISTANCE;
	pfe->bEdit        = FALSE;			// element is being edited. used for different display.
	pfe->lTransform   = FJ_TRANS_NORMAL;// 'user' transforms to use

	pfe->strName[0]   = 0;
	pfe->pActions     = NULL;			// pointer to actions structure
	pfe->pDesc        = NULL;			// pointer to individual element descriptor
	pfe->pfi          = NULL;			// pointer to FJIMAGE structure
	pfe->pfm          = NULL;			// pointer to owning FJMESSAGE structure
	return( pfe );
}
//
//  FUNCTION: fj_ElementTextNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJTEXT structure
//
FJDLLExport LPFJELEMENT fj_ElementTextNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_TextActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementDynTextNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJDYNTEXT structure
//
FJDLLExport LPFJELEMENT fj_ElementDynTextNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_DynTextActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementBarCodeNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJBARCODE structure
//
FJDLLExport LPFJELEMENT fj_ElementBarCodeNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_BarCodeActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementDynBarCodeNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJDYNBARCODE structure
//
FJDLLExport LPFJELEMENT fj_ElementDynBarCodeNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_DynBarCodeActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}


FJDLLExport LPFJELEMENT fj_ElementDataMatrixNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_DataMatrixActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}

//
//  FUNCTION: fj_ElementBitmapNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJBITMAP structure
//
FJDLLExport LPFJELEMENT fj_ElementBitmapNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_BitmapActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementDynimageNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJDYNIMAGE structure
//
FJDLLExport LPFJELEMENT fj_ElementDynimageNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_DynimageActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementDatetimeNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJDATETIME structure
//
FJDLLExport LPFJELEMENT fj_ElementDatetimeNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_DatetimeActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementCounterNew()
//
//  PURPOSE:  Create a new FJELEMENT structure with a FJCOUNTER structure
//
FJDLLExport LPFJELEMENT fj_ElementCounterNew( VOID )
{
	LPFJELEMENT pfe;

	pfe = fj_ElementNew();

	pfe->pActions = &fj_CounterActions;
	pfe->pDesc = pfe->pActions->fj_DescNew();
	return( pfe );
}
//
//  FUNCTION: fj_ElementGetActionsFromTypeString()
//
//  PURPOSE:  Find the requested Action table
//
FJDLLExport LPCFJELEMACTION fj_ElementGetActionsFromTypeString( LPCSTR pType )
{
	LPCFJELEMACTION pActions;

	pActions = &fj_BarCodeActions;
	if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
	{
		pActions = &fj_BitmapActions;
		if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
		{
			pActions = &fj_CounterActions;
			if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
			{
				pActions = &fj_DatetimeActions;
				if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
				{
					pActions = &fj_DynimageActions;
					if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
					{
						pActions = &fj_TextActions;
						if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
						{
							pActions = &fj_DynTextActions;
							if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
							{
								pActions = &fj_DynBarCodeActions;
								if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
								{
									pActions = &fj_DataMatrixActions;
									if ( 0 != strcmp(pType, pActions->fj_DescGetTypeString()) )
									{
										pActions = NULL;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return( pActions );
}
//
//  FUNCTION: fj_ElementGetActionsFromType()
//
//  PURPOSE:  Find the requested Action table
//
FJDLLExport LPCFJELEMACTION fj_ElementGetActionsFromType( LONG lType )
{
	LPCFJELEMACTION pActions;

	pActions = &fj_BarCodeActions;
	if ( lType != pActions->fj_DescGetType() )
	{
		pActions = &fj_BitmapActions;
		if ( lType != pActions->fj_DescGetType() )
		{
			pActions = &fj_CounterActions;
			if ( lType != pActions->fj_DescGetType() )
			{
				pActions = &fj_DatetimeActions;
				if ( lType != pActions->fj_DescGetType() )
				{
					pActions = &fj_DynimageActions;
					if ( lType != pActions->fj_DescGetType() )
					{
						pActions = &fj_TextActions;
						if ( lType != pActions->fj_DescGetType() )
						{
							pActions = &fj_DynTextActions;
							if ( lType != pActions->fj_DescGetType() )
							{
								pActions = &fj_DynBarCodeActions;
								if ( lType != pActions->fj_DescGetType() )
								{
									pActions = &fj_DataMatrixActions;
									if ( lType != pActions->fj_DescGetType() )
									{
										pActions = NULL;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return( pActions );
}
//
//  FUNCTION: fj_ElementNewFromType()
//
//  PURPOSE:  Create a new FJELEMENT structure with the requested descriptor structure
//
FJDLLExport LPFJELEMENT fj_ElementNewFromType( LONG lType )
{
	LPFJELEMENT     pfe;
	LPCFJELEMACTION pActions;

	pfe = NULL;

	pActions = fj_ElementGetActionsFromType( lType );
	if ( NULL != pActions )
	{
		pfe = fj_ElementNew();
		pfe->pActions = pActions;
		pfe->pDesc = pfe->pActions->fj_DescNew();
	}
	return( pfe );
}
//
//  FUNCTION: fj_ElementNewFromTypeString()
//
//  PURPOSE:  Create a new FJELEMENT structure with the requested descriptor structure
//
FJDLLExport LPFJELEMENT fj_ElementNewFromTypeString( LPCSTR pType )
{
	LPFJELEMENT     pfe;
	LPCFJELEMACTION pActions;

	pfe = NULL;

	pActions = fj_ElementGetActionsFromTypeString( pType );
	if ( NULL != pActions )
	{
		pfe = fj_ElementNew();
		pfe->pActions = pActions;
		pfe->pDesc = pfe->pActions->fj_DescNew();
	}
	return( pfe );
}
//
//  FUNCTION: fj_ElementDup()
//
//  PURPOSE:  Duplicate a FJELEMENT structure and its descriptor
//
FJDLLExport LPFJELEMENT fj_ElementDup( CLPCFJELEMENT pfeIn )
{
	LPFJELEMENT pfeDup;

	pfeDup = NULL;
	if ( NULL != pfeIn )
	{
		pfeDup = fj_ElementNew();
		memcpy( pfeDup, pfeIn, sizeof(FJELEMENT) );
		pfeDup->pfm    = NULL;			// pointer to owning FJMESSAGE structure
		// build a new descriptor
		pfeDup->pDesc  = pfeDup->pActions->fj_DescDup( pfeIn->pDesc );
		// bump the pImage Usage Counter
		if ( NULL != pfeDup->pfi ) fj_ImageUse( pfeDup->pfi );
	}
	return( pfeDup );
}
//
//  FUNCTION: fj_ElementDestroy()
//
//  PURPOSE:  Destroy a FJELEMENT structure
//
FJDLLExport VOID fj_ElementDestroy( LPFJELEMENT pfe )
{
	if ( NULL != pfe )
	{
		if ( NULL != pfe->pfi ) fj_ImageRelease( pfe->pfi );
		pfe->pActions->fj_DescDestroy( pfe );
		fj_free( pfe );
	}
	return;
}










//
//  FUNCTION: fj_ElementReset()
//
//  PURPOSE:  Reset a FJELEMENT structure after a FJIMAGE change
//
FJDLLExport VOID fj_ElementReset( LPFJELEMENT pfe )
{
	LPFJIMAGE     pfi;
	LPFJMESSAGE   pfm;
	LPFJPRINTHEAD pfph;
	LONG  lSlant;
	LONG  lTransforms;
	LONG   lTestRow;

	lSlant = 0;
	lTransforms = pfe->lTransform;

										// make new rasters
	pfe->pActions->fj_DescCreateImage( pfe );

	pfi = pfe->pfi;

	if ( NULL != pfi )
	{
		pfm = pfe->pfm;
		if ( NULL != pfm )
		{
			pfph = pfm->pfph;
			if ( NULL != pfph )
			{

				// bold and width are done in each element's routines.
				// they are sometimes omitted, depending on the element.
				// slant and transforms are usually 'global' and usually part of the 'environment',
				//    and they are not wanted for some special cases; e.g., text in a barcode.
				pfi = fj_ImageRasterTransform( pfi, pfph->lTransformEnv^lTransforms );

				lSlant = pfph->lSlant;
				if ( 0 < lSlant  )
				{
					if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) lSlant = -lSlant;
					// TODO: rem //pfi = fj_ImageSlant( pfi, lSlant );
				}

				/* TODO: rem
				lTestRow = (pfe->lRow/8)*8;
				if ( lTestRow != pfe->lRow)
				{
					lTestRow = pfe->lRow-lTestRow;
					pfe->lRow -= lTestRow;
					pfi = fj_ImageShift(pfi,lTestRow);
				}
				*/

				// store the returned pfi.
				// if any transform changed the original pfi, it was released.
				pfe->pfi = pfi;			// put new image into pfe
			}
		}
	}
	return;
}
//
//  FUNCTION: fj_ElementPhotocellBuild()
//
//  PURPOSE:  Rebuild a FJELEMENT structure for a photocell event
//
FJDLLExport VOID fj_ElementPhotocellBuild( LPFJELEMENT pfe )
{
	LPFJIMAGE pfi;
	LPFJIMAGE pfiOrg;
	LONG  lSlant;
	LONG  lTransforms;
	LONG   lTestRow;

	lSlant = 0;
	lTransforms = pfe->lTransform;
	if ( (NULL != pfe->pfm) && (NULL != pfe->pfm->pfph) ) lSlant = pfe->pfm->pfph->lSlant;

	pfiOrg = pfe->pfi;
	if ( NULL != pfiOrg ) fj_ImageUse ( pfiOrg );
										// make new rasters if necessary
	pfe->pActions->fj_DescPhotocellBuild( pfe );

	pfi = pfe->pfi;

	if ( (NULL != pfi) && (pfi != pfiOrg) )
	{

		// bold and width are done in each element's routines.
		// they are sometimes omitted, depending on the element.
		// slant and transforms are usually 'global' and usually part of the 'environment',
		//    and they are not wanted for some special cases; e.g., text in a barcode.
		// TODO: rem //if ( 0 < lSlant  ) pfi = fj_ImageSlant( pfi, lSlant );

		pfi = fj_ImageRasterTransform( pfi, pfe->pfm->pfph->lTransformEnv^lTransforms );

		/* TODO: rem
		lTestRow = (pfe->lRow/8)*8;
		if ( lTestRow != pfe->lRow)
		{
			lTestRow = pfe->lRow-lTestRow;
			pfe->lRow -= lTestRow;
			pfi = fj_ImageShift(pfi,lTestRow);
		}
		*/

		// store the returned pfi.
		// if any transform changed the original pfi, it was released.
		pfe->pfi = pfi;					// put new image into pfe
	}

	if ( NULL != pfiOrg ) fj_ImageRelease( pfiOrg );

	return;
}
/* TODO: rem // burl
//
//  FUNCTION: fj_ElementReset()
//
//  PURPOSE:  Reset a FJELEMENT structure after a FJIMAGE change
//
FJDLLExport VOID fj_ElementReset( LPFJELEMENT pfe )
{
	LPFJIMAGE     pfi;
	LPFJMESSAGE   pfm;
	LPFJPRINTHEAD pfph;
	LONG  lSlant;
	LONG  lTransforms;
	LONG   lTestRow;

	lSlant = 0;
	lTransforms = pfe->lTransform;

										// make new rasters
	pfe->pActions->fj_DescCreateImage( pfe );

	pfi = pfe->pfi;

	if ( NULL != pfi )
	{
		pfm = pfe->pfm;
		if ( NULL != pfm )
		{
			pfph = pfm->pfph;
			if ( NULL != pfph )
			{
				lTestRow = (pfe->lRow/8)*8;
				if ( lTestRow != pfe->lRow)
				{
					lTestRow = pfe->lRow-lTestRow;
					pfi = fj_ImageShift(pfi,lTestRow);
				}

				// bold and width are done in each element's routines.
				// they are sometimes omitted, depending on the element.
				// slant and transforms are usually 'global' and usually part of the 'environment',
				//    and they are not wanted for some special cases; e.g., text in a barcode.
				pfi = fj_ImageRasterTransform( pfi, pfph->lTransformEnv^lTransforms );

				lSlant = pfph->lSlant;
				if ( 0 < lSlant  )
				{
					if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) lSlant = -lSlant;
					pfi = fj_ImageSlant( pfi, lSlant );
				}

				// store the returned pfi.
				// if any transform changed the original pfi, it was released.
				pfe->pfi = pfi;			// put new image into pfe
			}
		}
	}
	return;
}
//
//  FUNCTION: fj_ElementPhotocellBuild()
//
//  PURPOSE:  Rebuild a FJELEMENT structure for a photocell event
//
FJDLLExport VOID fj_ElementPhotocellBuild( LPFJELEMENT pfe )
{
	LPFJIMAGE pfi;
	LPFJIMAGE pfiOrg;
	LONG  lSlant;
	LONG  lTransforms;
	LONG   lTestRow;

	lSlant = 0;
	lTransforms = pfe->lTransform;
	if ( (NULL != pfe->pfm) && (NULL != pfe->pfm->pfph) ) lSlant = pfe->pfm->pfph->lSlant;

	pfiOrg = pfe->pfi;
	if ( NULL != pfiOrg ) fj_ImageUse ( pfiOrg );
										// make new rasters if necessary
	pfe->pActions->fj_DescPhotocellBuild( pfe );

	pfi = pfe->pfi;

	if ( (NULL != pfi) && (pfi != pfiOrg) )
	{
		lTestRow = (pfe->lRow/8)*8;
		if ( lTestRow != pfe->lRow)
		{
			lTestRow = pfe->lRow-lTestRow;
			//		    pfe->lRow -= lTestRow;
			pfi = fj_ImageShift(pfi,lTestRow);
		}

		// bold and width are done in each element's routines.
		// they are sometimes omitted, depending on the element.
		// slant and transforms are usually 'global' and usually part of the 'environment',
		//    and they are not wanted for some special cases; e.g., text in a barcode.
		pfi = fj_ImageRasterTransform( pfi, pfe->pfm->pfph->lTransformEnv^lTransforms );

		if ( 0 < lSlant  ) pfi = fj_ImageSlant( pfi, lSlant );

		// store the returned pfi.
		// if any transform changed the original pfi, it was released.
		pfe->pfi = pfi;					// put new image into pfe
	}
#ifndef _WINDOWS
	if ( pfe->pActions == &fj_CounterActions )
	{
		fj_CVT.fj_SaveBRAMCounter( pfe );
	}
#endif
	if ( NULL != pfiOrg ) fj_ImageRelease( pfiOrg );
	return;
}
*/












//
//  FUNCTION: fj_ElementListAdd()
//
//  PURPOSE:  Add a FJELEMENT structure to the array of pointers
//
FJDLLExport LONG fj_ElementListAdd( LPFJELEMENT **ppapfeIn, LONG lCount, LPCFJELEMENT  pfe )
{
	LPFJELEMENT *papfeOld;
	LPFJELEMENT *papfeNew;
	LONG  lCountNew;
	int   i;

	papfeOld  = *ppapfeIn;
	lCountNew = lCount + 1;
	papfeNew = (LPFJELEMENT *)fj_malloc( lCountNew * sizeof (LPFJELEMENT) );
	for ( i = 0; i < lCount; i++ )
	{
		papfeNew[i] = papfeOld[i];
	}
	papfeNew[lCount] = (LPFJELEMENT)pfe;

	if ( NULL != papfeOld ) fj_free( papfeOld );
	*ppapfeIn = papfeNew;
	return( lCountNew );
}
//
//  FUNCTION: fj_ElementListRemove()
//
//  PURPOSE:  Remove a FJELEMENT structure from the array of pointers
//
FJDLLExport LONG fj_ElementListRemove( LPFJELEMENT **ppapfeIn, LONG lCount, LPCFJELEMENT  pfe )
{
	LPFJELEMENT *papfe;
	LONG  lCountNew;
	int   i;

	papfe = *ppapfeIn;
	lCountNew = lCount;
	for ( i = 0; i < lCount; i++ )
	{
		if ( pfe == papfe[i] )
		{
			for ( i++; i < lCount; i++ )
			{
				papfe[i-1] = papfe[i];
			}
			lCountNew = lCount - 1;
			papfe[lCountNew] = NULL;
			break;
		}
	}
	return( lCountNew );
}
//
//  FUNCTION: fj_ElementListDestroyAll()
//
//  PURPOSE:  Destroy all FJELEMENT structures and their list
//
FJDLLExport VOID fj_ElementListDestroyAll( LPFJELEMENT **ppapfeIn, LONG lCount )
{
	LPFJELEMENT *papfeIn;
	LPFJELEMENT  pfe;
	int i;

	papfeIn   = *ppapfeIn;
	if ( NULL != papfeIn )
	{
		*ppapfeIn = NULL;
		for ( i = 0; i < lCount; i++ )
		{
			pfe = papfeIn[i];
			if ( NULL != pfe ) fj_ElementDestroy( pfe );
		}
		fj_free( papfeIn );
	}

	return;
}
//
//  FUNCTION: fj_ElementMakeMemstorName()
//
//  PURPOSE:  Make an element name derived from message name
//
//  NOTE: originally, the IP printer code was developed with the capability of the segments
//        and elements to have their own names, their own lists and the ability to be
//        used in any message.
//        currently, segments and elements exist only in their containing message.
//
FJDLLExport VOID fj_ElementMakeMemstorName( LPFJELEMENT pfe, LPSTR pName )
{
	LPCSTR pID;
	STR   sTemp[FJELEMENT_NAME_SIZE*2];
	int   i;

	pID = pfe->pActions->fj_DescGetTypeString();
	for ( i = 0; i <10000; i++ )
	{
		sprintf( pName, "%s-%s-%d", pfe->pfm->strName, pID, i );
		fj_processParameterStringOutput( sTemp, pName );
		// TODO // if ( NULL == fj_MemStorFindElement( pfe->pfm->pfph->pMemStorEdit, pID, sTemp ) ) break;
	}

	return;
}

FJDLLExport LONG fj_ElementGetType( LPCSTR pType )
{
	if (strcmp (fj_TextID, 				pType) == 0)
		return FJ_TYPE_TEXT;
	else if (strcmp (fj_BarCodeID, 		pType) == 0)
		return FJ_TYPE_BARCODE;
	else if (strcmp (fj_BitmapID, 		pType) == 0)
		return FJ_TYPE_BITMAP;
	else if (strcmp (fj_DynimageID, 	pType) == 0)
		return FJ_TYPE_DYNIMAGE;
	else if (strcmp (fj_DatetimeID, 	pType) == 0)
		return FJ_TYPE_DATETIME;
	else if (strcmp (fj_CounterID, 		pType) == 0)
		return FJ_TYPE_COUNTER;
	else if (strcmp (fj_DynTextID, 		pType) == 0)
		return FJ_TYPE_DYNTEXT;
	else if (strcmp (fj_DynBarCodeID, 	pType) == 0)
		return FJ_TYPE_DYNBARCODE;
	else if (strcmp (fj_DataMatrixID, 	pType) == 0)
		return FJ_TYPE_DATAMATRIX;

	return 0;
}
