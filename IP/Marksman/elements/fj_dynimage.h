#ifndef FJ_DYNIMAGE_H_
#define FJ_DYNIMAGE_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjprinthead;

//
// A 'dynamic' image element is one where the image is received dynamically and not stored in ROM.
// The dynamic image is stored in a global location.
//

typedef struct fjdynimage
{
	LONG    lBoldValue;					// amount of boldness. 0 origin.
	LONG    lWidthValue;				// amount of width stretch. 1 origin.
	LONG    lDynimageCount;				// version of dynimage
} FJDYNIMAGE, FAR *LPFJDYNIMAGE, FAR * const CLPFJDYNIMAGE;
typedef const struct fjdynimage FAR *LPCFJDYNIMAGE, FAR * const CLPCFJDYNIMAGE;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport VOID fj_DynimagePutData( struct fjprinthead FAR *pfph, LPBYTE pBuff );
#ifdef  __cplusplus
}
#endif

#endif /*FJ_DYNIMAGE_H_*/
