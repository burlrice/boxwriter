#ifndef FJ_DYNBARCODE_H_
#define FJ_DYNBARCODE_H_

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct Barcode_Item;
struct fjelement;
struct fjsysbarcode;

typedef struct fjdynbarcode
{
	LONG    lSysBCIndex;				// index of global barcode definition
	LONG    lBoldValue;					// amount of boldness. 0 origin.
	LONG    lWidthValue;				// amount of width stretch. 1 origin.
	BOOL    bHROutput;					// true to text output into barcode
	CHAR    cHRAlign;					// text output alignment ( L, R, C)
	LONG    lHRBold;					// text output Bold value
	LONG    lFirstChar;					// start pos in dinamic field
	LONG    lNumChar;					// number of charecters to use in dinamic field
	LONG    lDynamicID;					// ID of dinamic text segment
	LPBYTE  pImageHRText;				// pointer to image location for Human Readable Text
	struct Barcode_Item * pBC;			// pointer to BC Lib structure
	struct fjelement FAR *pfeText;		// element for text generation
} FJDYNBARCODE, FAR *LPFJDYNBARCODE, FAR * const CLPFJDYNBARCODE;
typedef const struct fjdynbarcode FAR *LPCFJDYNBARCODE, FAR * const CLPCFJDYNBARCODE;

#define BARCODE_FONT  "barcode"
#define BARCODE_FONT256 "barcode256"

#endif /*FJ_DYNBARCODE_H_*/
