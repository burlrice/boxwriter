#include <tx_api.h>
#include <netoserr.h>
#include "fj.h"

/*
static LPBYTE    fj_mempoolHigh;		// high address of fixed block pools
static LPBYTE    fj_mempoolLow;			//  low address of fixed block pools

typedef struct mempool
{
	ULONG lSize;						// size of blocks in this pool
	ULONG lCount;						// number of blocks in this pool
	ULONG lMax;							// maximum block used
	TX_BLOCK_POOL BlockPool;			// block pool
} MEMPOOL, *PMEMPOOL, FAR *LPMEMPOOL;

// the following pools are set up to represent -
//      64   64 small control blocks
//     320  192 FJTEXT, FJIMAGE, etc.
//   1,024   64      tiny raster images and misc small control blocks
//   4,096   64     small raster images -  16 high, 300 dpi,  6 inches
//  16,384   16    medium raster images -  32 high, 300 dpi, 12 inches  - or medium bitmapped fonts   + FJSYSTEM, temp buffers, etc.
//  65,536   16     large raster images -  32 high, 300 dpi, 50 inches  - or large bitmapped fonts    + ROM segment
// 262,144    8      huge raster images - 256 high, 300 dpi, 25 inches  - or TrueType fonts
// 524,288    2  enormous 256-high fonts or TrueType fonts
//
// as of 10/10/02 the total memory used was 0xc7e000 out of 0xffffff, leaving 3.5 meg avaiable(decimal).
// this will grow as code is added and as NetSilicon inflates their libraries for options that we don't want anyway.
// this mempool has been reorganized on 02/26/04 for heavy duty labels, leaving 1.2 meg avaiable(decimal).
static ULONG  lPoolSizes[]  = {     64,     320,     1024,     4096,  8192,  4*4096,  8*4096,  16*4096,  32*4096,  64*4096,   128*4096 };
static ULONG  lPoolCounts[] = {     64,     192,       64,       64,    32,    24,     20,     16,        6,       6,         2 };
static STRING *lPoolNames[] = {"FJ_64","FJ_320","FJ_1024","FJ_4096","FJ_8192","FJ_16K", "FJ_32K", "FJ_64K", "FJ_128K", "FJ_256K", "FJ_512K" };

static LONG lPoolCount = sizeof(lPoolSizes) / sizeof(ULONG);
static MEMPOOL  mempools[sizeof(lPoolSizes) / sizeof(ULONG)];
*/

// permanent memory allocation
// allocate memory from top
// return low address of memory
BYTE *fj_memory_getOnce( LONG lSize )
{
	return nonCachedMalloc (lSize);
/*
 	BYTE *pRet;
	int boundary = 64;
	fj_CVT.pFJglobal->pRAMUnused = (LPBYTE)((((ULONG)fj_CVT.pFJglobal->pRAMUnused + boundary - 1) / boundary) * boundary);
	//    lSize = ((lSize + boundary - 1) / boundary) * boundary;
	//    return( malloc(lSize) );
	pRet = fj_CVT.pFJglobal->pRAMUnused;
	fj_CVT.pFJglobal->pRAMUnused += lSize;
	return ( pRet );
*/
}

/*
//
// return a stack
BYTE *fj_memory_getStack( LONG lSize )
{
	return( fj_memory_getOnce( lSize ) );
}
*/

void fj_memory_init(void)
{
/*
	LPMEMPOOL pPool;
	ULONG  lMemsize;
	ULONG  lAllsize;
	int    boundary = 64;
	LPBYTE pBuffer;
	STRING str[16];
	LONG   i;
	UINT   ret;

	lAllsize = 0;
	for ( i = 0; i < lPoolCount; i++ )
	{
		pPool = &(mempools[i]);
		pPool->lSize  = lPoolSizes [i];
		pPool->lCount = lPoolCounts[i];
		lMemsize = (pPool->lSize + sizeof(void*)) * pPool->lCount;
		lMemsize = ((lMemsize + boundary - 1) / boundary) * boundary;
		lAllsize += lMemsize;
	}
	pBuffer = fj_memory_getOnce( lAllsize );
	fj_mempoolLow  = pBuffer;			// save low address
	fj_mempoolHigh = pBuffer + lAllsize;// save high address
	for ( i = 0; i < lPoolCount; i++ )
	{
		pPool = &(mempools[i]);
		pPool->lSize  = lPoolSizes [i];
		pPool->lCount = lPoolCounts[i];
		lMemsize = (pPool->lSize + sizeof(void*)) * pPool->lCount;
		ret = tx_block_pool_create( &(pPool->BlockPool), lPoolNames[i], pPool->lSize, pBuffer, lMemsize);
		if ( TX_SUCCESS != ret )
		{
			netosFatalError ("Unable to create block pool", 1, 1);
		}
		lMemsize = ((lMemsize + boundary - 1) / boundary) * boundary;
		pBuffer += lMemsize;
	}
*/
}

//static int bob = 2;
VOID *fj_malloc( LONG lSize )
{
	return malloc (lSize);
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	LPMEMPOOL pPool;
	VOID  *pAllocated;
	LONG   lBlockSize;
	ULONG  lUsed;
	STRING str[80];
	int    i, j;
	UINT   ret;

	lBlockSize = lSize;
	pAllocated = NULL;
	for ( i = 0; (i < lPoolCount) && (NULL == pAllocated); i++ )
	{
		if ( lBlockSize <= mempools[i].lSize )
		{
			pPool = &(mempools[i]);
			if ( i == bob )
			{
				ret = 43;
			}
			if ( lSize > 64*4096 )
			{
				ret = 43;
			}
			ret = tx_block_allocate( &(pPool->BlockPool), &pAllocated, TX_NO_WAIT );
			if ( TX_NO_MEMORY == ret )
			{
				if ( (NULL != pfph) && (TRUE == pfph->bInfoRAM) )
				{
					fj_writeInfo(pPool->BlockPool.tx_block_pool_name);
					fj_writeInfo(" fully allocated\n");
				}
				if ( i < (lPoolCount-1) )lBlockSize = mempools[i+1].lSize;
			}
			if ( TX_SUCCESS == ret )
			{
				//memset( pAllocated, 0x55, pPool->lSize );
				lUsed = pPool->BlockPool.tx_block_pool_total - pPool->BlockPool.tx_block_pool_available;
				if ( pPool->lMax < lUsed )
				{
					pPool->lMax = lUsed;
					if ( (NULL != pfph) && (TRUE == pfph->bInfoRAM) )
					{
						sprintf(str, "%s max allocated = %d\n", pPool->BlockPool.tx_block_pool_name, lUsed);
						fj_writeInfo(str);
					}
				}
			}
		}
	}
	// if there is no memory,
	// and if the block is large for a font or image, the code can handle a NULL return, and we do not want to exhaust the heap.
	// but if it is for a small block, allocate from the heap in C.
	if ( NULL == pAllocated )
	{
		if ( lSize < 4096 ) pAllocated = malloc(lSize);
	}
	else
	{
	}
	return( pAllocated );
*/
}
VOID *fj_calloc( LONG lCount, LONG lSize )
{
	return calloc (lCount, lSize);
/*
	VOID  *pAllocated;
	LONG   lBlockSize;

	lBlockSize = lCount * lSize;
	pAllocated = fj_malloc( lBlockSize );
	if ( NULL != pAllocated ) memset( pAllocated, 0, lBlockSize );
	return( pAllocated );
*/
}
//static int fret;
void fj_free( VOID* pBlock )
{
	free (pBlock);
/*
	if ( NULL != pBlock )
	{
		if ( (pBlock <= fj_mempoolHigh) && (pBlock >= fj_mempoolLow) )
		{
			tx_block_release( pBlock );
		}
		else
		{
			free( pBlock );
		}
	}
*/	
}
