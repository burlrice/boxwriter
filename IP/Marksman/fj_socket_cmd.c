#include "math.h"
#include "fj.h"

#include <stdio.h>
#include <time.h>
#include <fs.h>
#include <fservapi.h>

#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_bitmap.h"
#include "fj_dynimage.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"
#include "fj_socket.h"
#include "fj_element.h"					// Burl v 1.1014
#include "fj_counter.h"					// Burl v 1.1014
#include "fj_file.h"
#include "fj_scratchpad.h"
#include "mphc.h"
#include "fj_image.h"
#include "fj_rom.h"

extern const CHAR fj_FontID[];
extern const CHAR fj_TextID[];
extern const CHAR fj_DynTextID[];
extern const CHAR fj_BarCodeID[];
extern const CHAR fj_DynBarCodeID[];
extern const CHAR fj_BitmapID[];
extern const CHAR fj_CounterID[];
extern const CHAR fj_DatetimeID[];
extern const CHAR fj_DynimageID[];
extern const CHAR fj_MessageID[];
extern const CHAR fj_LabelID[];
extern const CHAR fj_bmpID[];
extern const CHAR fj_DataMatrixID[];

extern ROMENTRY        fj_RomTable[];

void fj_ftp_ROM_callback( LONG l1, LONG l2 );

static int GetBoldFactor (BITMAPINFOHEADER * pbmInfoHeader)
{
	int i, nResult = 0;
	const double dRes = (double)fj_CVT.pfsys->fEncoder;
	double dWidth = (double)REVERSEWORD32 (pbmInfoHeader->biWidth);
	double dDPI = (double)REVERSEWORD32 (pbmInfoHeader->biXPelsPerMeter);
	
	if (dRes != 0.0) {
		const double dTargetWidth = dWidth / dDPI;
		double dMin = abs ((dWidth / dRes) - dTargetWidth);
	
		for (i = 0; i < 10; i++) {
			double dBoxWidth = ((dWidth * (double)(i + 1)) / dRes);
			double dDiff = abs (dBoxWidth - dTargetWidth);
	
			if (dDiff < dMin) {
				nResult = i;
				dMin = dDiff;
			}
		}
	}
	
	{ char sz [128]; sprintf (sz, "GetBoldFactor: %d: width: %f, dpi: %f [%f], dRes: %f, divisor: %d", nResult, dWidth, dDPI, dWidth / dDPI, dRes, fj_CVT.pfsys->lEncoderDivisor);  TRACEF (sz); }
	
	return nResult;
}

static int IsBinaryCmd (LONG lCmd) // burl 1.2026
{
    switch (lCmd) {
	case IPC_PUT_BITMAP:
	case IPC_PUT_FONT:
	    return 1;
    }

    return 0;
}

// NOTE: this routine to process socket commands is used in both
//       the print head chip and the external editor.
//       the first parameter is different in the two systems.
//       the socketSend routines must be implemented in both systems.

//
// process a command - received or requested
//
// NOTE: pVoid is different for different systems.
//
FJDLLExport VOID fj_socketCmd( LPFJPRINTHEAD pfph, LPVOID pVoid, ULONG lIP, _FJ_SOCKET_MESSAGE *psmsgIn, _FJ_SOCKET_MESSAGE *psmsgOut, VOID((*socketSend)( LPVOID pVoid, LPBYTE pByte, LONG lLength)) )
{
/*
	ROMQUEUE      q;
	LPFJMEMSTOR pfms;
*/
	_FJ_SOCKET_MESSAGE *psmsgBig;
	LPSTR  *papList;
	LPSTR  *papListSave;
	ULONG  lCmd;
	ULONG  lBatchID;
	ULONG  lDataLength;
	LPBYTE pData;
	BOOL   bIsEdit;
	BOOL   bInvalid;
	BOOL   bSendString;
	BOOL   bSendBinary;
	LPCSTR pID;
	LPCSTR pKeyword;
	LPSTR  pElement;
	LPSTR  apList[10];
	LPSTR  apListCmd[2];
	LPSTR  apListKeyVal[2];
	STR    strLabelName[FJLABEL_NAME_SIZE*2];
	CHAR   parm[88];
	CHAR   str[88];
	LONG   lRet;
	LONG   lParmLen;
	LONG   lParmCount;
	int    i;
	int    index;
	int    retrom;
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal; //pCVT->pFJglobal;
	LPFJ_PARAM    pParam  = fj_CVT.pFJparam; //pCVT->pFJparam;
	LPFJSYSTEM    pfsys   = fj_CVT.pfsys; //pCVT->pfsys;
	BOOL   bRet;

	
	// psmsgIn might not be on a LONG boundary
	// move ULONG values from header.
	memcpy( &lCmd,        &(psmsgIn->Header.Cmd),     sizeof(ULONG) );
	memcpy( &lBatchID,    &(psmsgIn->Header.BatchID), sizeof(ULONG) );
	memcpy( &lDataLength, &(psmsgIn->Header.Length),  sizeof(ULONG) );
	pData       = (LPBYTE)&(psmsgIn->Data);
	psmsgOut->Header.BatchID = lBatchID;
	bInvalid    = FALSE;
	bSendString = FALSE;
	bSendBinary = FALSE;

  	bIsEdit = fj_PrintHeadIsEdit( pfph, lIP );

	// do some common parsing of the beginning of any parameters.
	// it might not be needed.
	// but it will be done correctly in one location.
	//
	// copy first part of parameters into local buffer so that original string is not modified.
	// parse/find first 2 semi-colon delimited keywords.
	// parse find keyword=value of first keyword.

	if ( 0 != lDataLength )
	{
		strncpy( parm, (LPSTR)pData, sizeof(parm) );
		parm[sizeof(parm)-1] = 0;

		if (!IsBinaryCmd (lCmd)) // burl 1.2026
		    parm [lDataLength] = 0; // burl 1.2025
		
		lParmLen = strlen( parm ) - 1;
		if ( ';' == parm[lParmLen] ) parm[lParmLen] = 0;
		lParmCount = fj_ParseKeywordsFromStr( parm, ';', apListCmd, 2 );
		if ( lParmCount > 0 )
		{
			i = fj_ParseKeywordsFromStr( apListCmd[0], '=', apListKeyVal, 2 );
		}
	}

	//TRACEF (fj_socketCommandString (lCmd));

	switch ( lCmd )
	{
		// these cases, and others, that have no code are not implemented in the common section.
		// look in the socket control modules to find their imlementations.
		// some cases are imlemented in both this common code and the local socket control modules.
		// they are here for completeness - as a check that they were considered and not erroneosly ignored.
		case IPC_COMPLETE:
		case IPC_CONNECTION_CLOSE:
		case IPC_INVALID:
		case IPC_ECHO:
		case IPC_STATUS:
		case IPC_GET_STATUS:
		case IPC_PHOTO_TRIGGER:
			break;

 		case IPC_EDIT_STATUS:
			pData[lDataLength] = 0;
			fj_PrintHeadFromString( pfph, (LPSTR)pData );
			break;

		case IPC_GET_EDIT_STATUS:
			psmsgOut->Header.Cmd = IPC_EDIT_STATUS;
			fj_PrintHeadGetEditStatus( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;
		case IPC_EDIT_START:
		case IPC_EDIT_SAVE:
		// case IPC_EDIT_CANCEL: // no longer used
			break;
		case IPC_GET_LABEL_IDS:
			{
				pID = fj_LabelID;
				pKeyword = "LABELIDS";
				int nBufferSize = GetFileNames (FJ_DIR_LABELS, pKeyword, NULL, 0) + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
				unsigned char * pBuffer = fj_malloc (nBufferSize);

				if (pBuffer) {
					_FJ_SOCKET_MESSAGE_HEADER * pHeader = (_FJ_SOCKET_MESSAGE_HEADER *)pBuffer;
					unsigned char * p = (unsigned char *)(pBuffer + sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					memset (pBuffer, 0, nBufferSize);
					GetFileNames (FJ_DIR_LABELS, pKeyword, (char *)p, nBufferSize - sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					lDataLength			= strlen ((LPSTR)p);
					pHeader->Cmd		= IPC_SET_LABEL_IDS;
					pHeader->BatchID	= lBatchID;
					pHeader->Length		= lDataLength;

					socketSend (pVoid, (LPBYTE)pBuffer, sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lDataLength);
					fj_free (pBuffer);
				}

				return;
			}
			break;
		case IPC_GET_LABEL_ID_SELECTED:
			psmsgOut->Header.Cmd = IPC_SET_LABEL_ID_SELECTED;
			strcpy( (LPSTR)psmsgOut->Data, "LABELID=" );
			if ( NULL != pfph->pfsys->pfl )
			{
				// label Name
				fj_processParameterStringOutput( strLabelName, pfph->pfsys->pfl->strName );
				strcat( (LPSTR)psmsgOut->Data, strLabelName );
			}
			strcat( (LPSTR)psmsgOut->Data, ";" );
			bSendString = TRUE;
			break;
		case IPC_SET_LABEL_ID_SELECTED:
		case IPC_SELECT_LABEL:
			{
				char szFile [FJSYS_MAX_FILENAME] = { 0 };
				
				pData[lDataLength] = 0;
				str[0] = 0;					// select no label if parameter is bad
				if ( 0 == strcmp( apListKeyVal[0], "LABELID" ) )
				{
					if ( '"' == apListKeyVal[1][0] )
					{
						strcpy( strLabelName, apListKeyVal[1] );
						fj_processParameterStringInput( apListKeyVal[1] );
					}
					else
					{
						// label Name
						fj_processParameterStringOutput( strLabelName, apListKeyVal[1] );
					}
	
					sprintf (szFile, "%s/%s", FJ_DIR_LABELS, apListKeyVal[1]);
					
					if (!FileExists (FJ_DIR_LABELS, apListKeyVal[1]))										
						apListKeyVal[1][0] = 0; // select no label if label does not exist
				}
				else
					bInvalid = TRUE;
	
				printf ("%s(%d): IPC_SET_LABEL_ID_SELECTED: %s\n", __FILE__, __LINE__, szFile);
				
				fj_ResetBRAMCounters (pCVT->pBRAM);
				fj_SystemBuildSelectedLabel (pfph->pfsys, apListKeyVal [1]);
			}
			break;
		case IPC_GET_FONT:
			{
				char szFile [FJSYS_MAX_FILENAME];
				
				sprintf (szFile, "%s/%s", FJ_DIR_FONTS, apListKeyVal [1]);
				lDataLength = ReadFileSize (szFile);
				printf ("%s(%d): szFile: %s [%d bytes]\n", __FILE__, __LINE__, szFile, lDataLength);
				
				psmsgOut->Header.Cmd = IPC_PUT_FONT;
				pID = fj_FontID;
				pKeyword = "FONTID";
				
				LONG lPacketSize = sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1;
				printf ("%s(%d): lPacketSize: %d\n", __FILE__, __LINE__, lPacketSize);

				_FJ_SOCKET_MESSAGE * p = (_FJ_SOCKET_MESSAGE *)fj_malloc (lPacketSize);
				
				printf ("%s(%d): p: %p\n", __FILE__, __LINE__, p);
				
				if (p) {
					memset (p, 0, lPacketSize);
					p->Header.Cmd     = p->Header.Cmd;
					p->Header.BatchID = lBatchID;
					p->Header.Length  = lDataLength;
					
					int nReadFile = ReadFile ((int *)&lDataLength, (char *)&p->Data, szFile);
					//memcpy (psmsgBig->Data, pElement, lSize );
					printf ("%s(%d): nReadFile: %d [lSize: %d]\n", __FILE__, __LINE__, nReadFile, lDataLength);
	
					socketSend (pVoid, (LPBYTE)p, sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength);
					fj_free (p);
					bInvalid = FALSE;
				}
			}
			break;
		case IPC_GET_FONT_IDS:
			{
				pID = fj_FontID;
				pKeyword = "FONTIDS";
				int nBufferSize = GetFileNames (FJ_DIR_FONTS, pKeyword, NULL, 0) + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
				unsigned char * pBuffer = fj_malloc (nBufferSize);

				if (pBuffer) {
					_FJ_SOCKET_MESSAGE_HEADER * pHeader = (_FJ_SOCKET_MESSAGE_HEADER *)pBuffer;
					unsigned char * p = (unsigned char *)(pBuffer + sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					memset (pBuffer, 0, nBufferSize);
					GetFileNames (FJ_DIR_FONTS, pKeyword, (char *)p, nBufferSize - sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					lDataLength			= strlen ((LPSTR)p);
					pHeader->Cmd		= IPC_SET_FONT_IDS;
					pHeader->BatchID	= lBatchID;
					pHeader->Length		= lDataLength;

					socketSend (pVoid, (LPBYTE)pBuffer, sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lDataLength);
					fj_free (pBuffer);
				}

				return;
			}
			break;

		case IPC_PUT_FONT:
			bInvalid = TRUE;
	
			if (bIsEdit && lDataLength) {
				LPFJFONT pff = fj_FontFromBuffer (pData);
				
				if (pff) {
					char szFile [FJSYS_MAX_FILENAME];

					sprintf (szFile, "%s", FJ_DIR_FONTS);
					int nMkdir = mkdir (szFile, 0);
					//printf ("%s(%d): mkdir (%s): %d\n", __FILE__, __LINE__, szFile, nMkdir);
					
					sprintf (szFile, "%s/%s", FJ_DIR_FONTS, pff->strName);
					int nWrite = WriteFile (lDataLength, (char *)pData, szFile);
					printf ("%s(%d): nWrite: %d [%s]\n", __FILE__, __LINE__, nWrite, szFile);
					
					fj_FontDestroy( pff );

					// burl 1.2026
					bSendString = TRUE;
					strcpy ((char *)psmsgOut->Data, "IPC_PUT_FONT");
					psmsgOut->Header.Cmd = IPC_PUT_FONT;
					psmsgOut->Header.BatchID = lBatchID;
					psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
					// burl 1.2026
		
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_DELETE_FONT:
			pID = fj_FontID;
			pKeyword = "FONTID";
			bInvalid = TRUE;
			
			if (bIsEdit) {
				char szFile [FJFONT_NAME_SIZE] = { 0 };
				
				if ('"' == apListKeyVal[1][0]) {
					strcpy (szFile, &apListKeyVal [1][1]);
					szFile [strlen (szFile) - 1] = 0;
				}
				else
					strcpy (szFile, &apListKeyVal [1][0]);
				
				printf ("%s(%d): DeleteFile: %s\n", __FILE__, __LINE__, szFile);
				
				if (!DeleteFile (FJ_DIR_FONTS, szFile))
					bInvalid = FALSE;
			}
			break;
  		case IPC_PUT_BMP:
			if (bIsEdit && lDataLength) {
				LPSTR lpszParams [3] = { NULL };
				int nParams = (int)fj_ParseKeywordsFromStr (apListCmd [0], ',', lpszParams, ARRAYSIZE (lpszParams));
				
				if (nParams >= 1) {
					pElement = (LPSTR)pData;
					pElement += strlen (pElement) + 1;
					
					if (fj_bmpCheckFormat ((LPBYTE)pElement)) {
						char szFile [FJSYS_MAX_FILENAME];
						char szTitle [FJBITMAP_NAME_SIZE];
						BITMAPFILEHEADER * pbmfh = (BITMAPFILEHEADER *)pElement;
						BITMAPINFOHEADER * pbmih = (BITMAPINFOHEADER *)(pElement + sizeofBITMAPFILEHEADER (pbmfh));
						
						if (lpszParams [1][0] != '\"') 
							strncpy (szTitle, lpszParams [1], FJBITMAP_NAME_SIZE);
						else {
							strncpy (szTitle, &lpszParams [1][1], FJBITMAP_NAME_SIZE);
							szTitle [strlen (szTitle) - 1] = NULL;
						}
	
						sprintf (szFile, "%s", FJ_DIR_BITMAPS);
						int nMkdir = mkdir (szFile, 0);
						//printf ("%s(%d): mkdir (%s): %d\n", __FILE__, __LINE__, szFile, nMkdir);
						
						sprintf (szFile, "%s/%s", FJ_DIR_BITMAPS, szTitle);
						int nWrite = WriteFile (lDataLength, (char *)pData, szFile);
						printf ("%s(%d): nWrite: %d [%s]\n", __FILE__, __LINE__, nWrite, szFile);
												
						bSendString = TRUE;
						strcpy ((char *)psmsgOut->Data, "IPC_PUT_BMP");
						psmsgOut->Header.Cmd = IPC_PUT_BMP;
						psmsgOut->Header.BatchID = lBatchID;
						psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
			
						bInvalid = FALSE;
					}
				}
			}
			break;
		case IPC_PUT_BMP_DRIVER:
			if (!bIsEdit && lDataLength) {
				LPSTR lpszParams [3] = { NULL };
				int nParams = (int)fj_ParseKeywordsFromStr (apListCmd [0], ',', lpszParams, ARRAYSIZE (lpszParams));
				
				if (nParams >= 1) {
					pElement = (LPSTR)pData;
					pElement += strlen (pElement) + 1;
					
					if (fj_bmpCheckFormat ((LPBYTE)pElement)) {
						BITMAPFILEHEADER * pbmfh = (BITMAPFILEHEADER *)pElement;
						BITMAPINFOHEADER * pbmih = (BITMAPINFOHEADER *)(pElement + sizeofBITMAPFILEHEADER (pbmfh));

						fj_StatusModeFromString (fj_CVT.pfsys, "STATUS_MODE=OFF;");
						fj_print_head_EditStart (lIP);
						fj_SysSetPrintMode (fj_CVT.pfsys, "STOP");
						fj_SysSetPrintDriverMode (fj_CVT.pfsys, "T");
						
						{
							const int nBold = GetBoldFactor (pbmih);
							const double dRes = (double)fj_CVT.pfsys->fEncoder;
							STR szLabel [512];
							double dLength = (double)REVERSEWORD32 (pbmih->biWidth) / dRes;

							//sprintf (szLabel, "width: %f", (double)REVERSEWORD32 (pbmih->biWidth) / (double)REVERSEWORD32 (pbmih->biXPelsPerMeter)); TRACEF (szLabel);
							//sprintf (szLabel, "dLength: %f", dLength); 	TRACEF (szLabel);
							//sprintf (szLabel, "dRes: %f", dRes); 		TRACEF (szLabel);
							sprintf (szLabel,
								"{Label,\"PrintDriver\",12.0,%.03f,0,3,F,\"PrintDriver\"}"
								"{Message,\"PrintDriver\",1,\"PrintDriver-Bitmap-1\"}"
								"{Bitmap,\"PrintDriver-Bitmap-1\",0.000000,0,D,%d,1,F,F,F,\"PrintDriver\"}",
								dLength, nBold);
							TRACEF (szLabel);
							LPFJLABEL pfl = fj_SystemBuildSelectedLabelFromString (szLabel);
							
							if (pfl) {
								LPFJMESSAGE pfm = pfph->pfmSelected;
								
								if (pfm) {
									int nElement;

									for (nElement = 0; nElement < pfm->lCount; nElement++) {
										LPFJELEMENT pfe = pfm->papfe [nElement];
										
										if (pfe) {
										    switch (pfe->pActions->fj_DescGetType ()) {
											case FJ_TYPE_BITMAP:
												{
													LPFJBITMAP pfb = (LPFJBITMAP)pfe->pDesc;
													
													ClearOnBoardRam ();
													fj_BitmapLoadFromMemory (pfb, (LPSTR)pData);
													fj_ProgramSetupRegs ();	
													fj_MessageReset (pfm);
													//fj_MessagePhotocellBuild (pfm);
													//fj_SystemBuildImage (pfm);
													//fj_SysSetPrintMode (fj_CVT.pfsys, "RESUME");		
													//fj_scratchpadWritePrintState (FALSE);

													LPFJIMAGE pfi = pfe->pfi;
													
													if (pfi) {
														int cx = (int)((double)pfi->lLength / ceil ((double)pfi->lHeight / 8.0));
														double dDPI = (double)REVERSEWORD32 (pbmih->biXPelsPerMeter);
														
														pfl->fBoxWidth = (double)cx / dRes;
														
														
														if (fj_CVT.pfsys->bPrintDriverDebug) {
															const char * lpszFile = "FLASH0/pfe.FJIMAGE";
															char sz [512]; 
															
															sprintf (sz, "pfb->lBoldValue: %d", pfb->lBoldValue); TRACEF (sz); 
															sprintf (sz, "dRes: %f, dDPI: %f", dRes, dDPI); TRACEF (sz);
															sprintf (sz, "pfi: lLength: %d, lHeight: %d", pfi->lLength, pfi->lHeight); TRACEF (sz); 
															sprintf (sz, "cx: %d, fBoxWidth: %f in", cx, pfl->fBoxWidth); TRACEF (sz); 
															
															if (WriteFile (sizeof (pfi) + pfi->lLength, (char *)pfi, lpszFile))
																TRACEF (lpszFile);
														}
													}
													
													break;
												}
										    }
										}
									}
								}
							}
							
							fj_print_head_EditSave (lIP);
							fj_SysSetPrintMode (fj_CVT.pfsys, "RESUME");
							fj_CVT.pfsys->bPrintDriverPending = FALSE;
														
							bSendString = TRUE;
							strcpy ((char *)psmsgOut->Data, "IPC_PUT_BMP_DRIVER");
							psmsgOut->Header.Cmd = IPC_PUT_BMP_DRIVER;
							psmsgOut->Header.BatchID = lBatchID;
							psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
							bInvalid = FALSE;
						}
					}
				}
			}
			break;
		case IPC_GET_BMP_IDS:
			{
				pID = fj_bmpID;
				pKeyword = "BMPIDS";
				int nBufferSize = GetFileNames (FJ_DIR_BITMAPS, pKeyword, NULL, 0) + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
				unsigned char * pBuffer = fj_malloc (nBufferSize);

				if (pBuffer) {
					_FJ_SOCKET_MESSAGE_HEADER * pHeader = (_FJ_SOCKET_MESSAGE_HEADER *)pBuffer;
					unsigned char * p = (unsigned char *)(pBuffer + sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					memset (pBuffer, 0, nBufferSize);
					GetFileNames (FJ_DIR_BITMAPS, pKeyword, (char *)p, nBufferSize - sizeof (_FJ_SOCKET_MESSAGE_HEADER));

					lDataLength			= strlen ((LPSTR)p);
					pHeader->Cmd		= IPC_SET_BMP_IDS;
					pHeader->BatchID	= lBatchID;
					pHeader->Length		= lDataLength;

					socketSend (pVoid, (LPBYTE)pBuffer, sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lDataLength);
					fj_free (pBuffer);
				}

				return;
			}
			break;

		case IPC_SET_BMP_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_BMPS:
			psmsgOut->Header.Cmd = IPC_PUT_BMP;
			pID = fj_bmpID;
			// TODO // goto BinaryElementsCommon;
			break;

		case IPC_GET_BMP:
			psmsgOut->Header.Cmd = IPC_PUT_BMP;
			pID = fj_bmpID;
			pKeyword = "BMPID";
			// TODO // goto GetBinaryElementCommon;
			break;

		case IPC_DELETE_BMP:
			pID = fj_bmpID;
			pKeyword = "BMPID";
			bInvalid = TRUE;
			
			if (bIsEdit) {
				char szFile [FJFONT_NAME_SIZE] = { 0 };
				
				if ('"' == apListKeyVal[1][0]) {
					strcpy (szFile, &apListKeyVal [1][1]);
					szFile [strlen (szFile) - 1] = 0;
				}
				else
					strcpy (szFile, &apListKeyVal [1][0]);
				
				printf ("%s(%d): Delete bitmap: %s\n", __FILE__, __LINE__, szFile);
				
				if (!DeleteFile (FJ_DIR_BITMAPS, szFile))
					bInvalid = FALSE;
			}
			break;

		case IPC_DELETE_ALL_BMPS:
			bInvalid = TRUE;
			
			if (bIsEdit) {
				if (!DeleteAllFiles (FJ_DIR_BITMAPS)) {
					printf ("%s(%d): Delete all bitmaps\n", __FILE__, __LINE__);
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_DELETE_ALL_LABELS:
			bInvalid = TRUE;
			
			if (bIsEdit) {
				if (!DeleteAllFiles (FJ_DIR_LABELS)) {
					printf ("%s(%d): Delete all labels\n", __FILE__, __LINE__);
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_GET_SYSTEM_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_INTERNET_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemInternetToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_TIME_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemTimeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_DATE_STRINGS:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemDateStringsToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_PASSWORDS:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemPasswordToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_BARCODES:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemBarCodesToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_DATAMATRIX:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemDataMatrixToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_SYSTEM_INFO:
			pData[lDataLength] = 0;
			fj_SystemFromString( pfph->pfsys, (LPSTR)pData );
			break;

		case IPC_GET_PRINTER_ID:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadIDToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_PKG_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadPackageToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_PHY_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadPhysicalToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_OP_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadOperationalToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_PRINTER_INFO:
			pData[lDataLength] = 0;
			fj_PrintHeadFromString( pfph, (LPSTR)pData );
			fj_PrintHeadReset( pfph );
			break;

		case IPC_PUT_PRINTER_ELEMENT:
			if ( TRUE == bIsEdit )
			{
				LPSTR pElementTest;
				LPSTR pElementEnd;
				LPSTR pQuoteEnd;
				CHAR  c;
				
				pElement = (LPSTR)pData;
				i = 0;
				
				while ( (i < (LONG)lDataLength) && (FALSE == bInvalid) )
				{
					pElementEnd = NULL;
					pElementTest = pElement;
					while ( i < (LONG)lDataLength )
					{
						c = *pElementTest;
						if ( '"' == c )
						{
							pQuoteEnd = fj_FindEndQuotedString( pElementTest );
							if ( NULL != pQuoteEnd )
							{
								pElementTest = pQuoteEnd;
								i = pElementTest - (LPSTR)pData;
							}
						}
						else if ( '}' == c )
						{
							pElementEnd = pElementTest;
							break;
						}
						pElementTest++;
						i++;
					}

					//					pElementEnd = strchr( pElement, '}' );
					if ( NULL != pElementEnd )
					{
						strncpy( str, pElement, sizeof(str) );
						i = 1 + (pElementEnd - pElement);
						if ( i < sizeof(str) )
						{
							str[i] = 0;
						}
						else
						{
										// guarantee end for partial scan
							str[sizeof(str)-2] = '}';
							str[sizeof(str)-1] = 0;
						}
						
						/*
						fj_ParseKeywordsFromStr( str, ',', apList, 3 );
						
						if ((0 == strcmp(fj_FontID, apList[0])) ||
							(0 == strcmp(fj_bmpID,  apList[0])) )
						{
							break;		// end the while statement
						}
						else if (0 == strcmp (fj_LabelID, apList[0])) {
						*/
							LPFJLABEL pLabel = fj_LabelFromStr (str);
							
							if (pLabel) {
								char szFile [FJSYS_MAX_FILENAME];
								
								sprintf (szFile, "%s", FJ_DIR_LABELS);
								int nMkdir = mkdir (szFile, 0);
								
								sprintf (szFile, "%s/%s", FJ_DIR_LABELS, pLabel->strName);
								int nWrite = WriteFile (lDataLength, (char *)pData, szFile);
								printf ("%s(%d): nWrite: %d [%s]: %s\n", __FILE__, __LINE__, nWrite, szFile, str);
								
								fj_LabelDestroy (pLabel);
								bInvalid = !(nWrite == 1);
																
								if (!bInvalid) {
									bSendString = TRUE;
									strcpy ((char *)psmsgOut->Data, "IPC_PUT_PRINTER_ELEMENT");
									psmsgOut->Header.Cmd = IPC_PUT_PRINTER_ELEMENT;
									psmsgOut->Header.BatchID = lBatchID;
									psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
								}
								break;
							}
							
							pElementEnd++;
							*pElementEnd = 0;
							
							pElement = pElementEnd;
							i = pElement - (LPSTR)pData;
							if ( i < (LONG)lDataLength )
								*pElement = '{';
						/*
						}
						else bInvalid = TRUE;
						*/
					}
					else
						break;			// end the while statement
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_DELETE_LABEL:
			bInvalid = TRUE;
			
			if (bIsEdit) {
				char szFile [FJLABEL_NAME_SIZE] = { 0 };
				
				if ('"' == apListKeyVal[1][0]) {
					strcpy (szFile, &apListKeyVal [1][1]);
					szFile [strlen (szFile) - 1] = 0;
				}
				else
					strcpy (szFile, &apListKeyVal [1][0]);
				
				printf ("%s(%d): Delete label: %s\n", __FILE__, __LINE__, szFile);
				
				if (!DeleteFile (FJ_DIR_LABELS, szFile))
					bInvalid = FALSE;
			}
			break;

		case IPC_DELETE_ALL_PRINTER_ELEMENTS:
			bInvalid = TRUE;
			
			if (bIsEdit) {
				if (!DeleteAllFiles (FJ_DIR_LABELS)) {
					printf ("%s(%d): Delete all labels\n", __FILE__, __LINE__);
					bInvalid = FALSE;
				}
				if (!DeleteAllFiles (FJ_DIR_BITMAPS)) {
					printf ("%s(%d): Delete all bitmaps\n", __FILE__, __LINE__);
					bInvalid = FALSE;
				}
				if (!DeleteAllFiles (FJ_DIR_FONTS)) {
					printf ("%s(%d): Delete all fonts\n", __FILE__, __LINE__);
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_FIRMWARE_UPGRADE:
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					char * lpszFile = "image.bin";
					ULONG lHandle = 0;
					
					/*
					// set lock to write ROM
					tx_semaphore_get( &(fj_CVT.pFJglobal->semaROMLock), TX_WAIT_FOREVER );
					q.lRomEntry   = ROM_RAMBIN;
					q.pBuffer     = pData;
					q.lBufferSize = lDataLength;
					q.bFreeBuffer = FALSE;
					q.bFreeLock   = TRUE;
					q.pCallBack   = fj_ftp_ROM_callback;
					tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );
					strcpy( (LPSTR)psmsgOut->Data, "ROM write strarted");
					bSendString = FALSE;
					*/

					/*
					printf ("%s(%d): FSStoreImageToFlash (%p, %d, %s, %d)...\n", __FILE__, __LINE__, pData, lDataLength, lpszFile, lHandle);

		            int nWrite = FSStoreImageToFlash ((char *)pData, lDataLength, lpszFile, lHandle);

					switch (nWrite) {
					case NAFTPS_SUCCESS:		printf ("%s(%d): FSStoreImageToFlash: NAFTPS_SUCCESS\n", __FILE__, __LINE__);		break;
					case NAFTPS_GENERROR:		printf ("%s(%d): FSStoreImageToFlash: NAFTPS_GENERROR\n", __FILE__, __LINE__);		break;
					case NAFTPS_FILESYSTEMERR:	printf ("%s(%d): FSStoreImageToFlash: NAFTPS_FILESYSTEMERR\n", __FILE__, __LINE__);	break;
					default:					printf ("%s(%d): FSStoreImageToFlash: (unknown error)\n", __FILE__, __LINE__);		break;
					}
					 
					pGlobal->bReset	= TRUE;
					*/
				}
			}
			else bInvalid = TRUE;
			break;
		
		case IPC_SET_PH_TYPE:
			pData[lDataLength] = 0;
			index = atoi(pData);
			if ( index < pCVT->lFJPhOpCount )
			{
				// get ROM structure
				tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
				retrom = 0;//fj_ReadFJRomParams( pParam );
				
				if (retrom != 0) {
					printf ("%s(%d): fj_ReadFJRomParams failed\n", __FILE__, __LINE__);
				}
				else {
					pfph->lFJPhOpIndex = index;
					pParam->FJPh.lFJPhOpIndex = index;
					pfph->bHeadSelected = TRUE;
					pParam->FJPh.bHeadSelected = TRUE;
					memcpy( &(pParam->FJPhOp ), &(*(fj_CVT.paFJPhOp ))[index], sizeof(FJPRINTHEAD_OP ) );
					index = pParam->FJPhOp.lPhyTable;
					memcpy( &(pParam->FJPhPhy), &(*(fj_CVT.paFJPhPhy))[index], sizeof(FJPRINTHEAD_PHY) );
					index = pParam->FJPhOp.lPkgTable;
					memcpy( &(pParam->FJPhPkg), &(*(fj_CVT.paFJPhPkg))[index], sizeof(FJPRINTHEAD_PKG) );

					// write and free ROM lock
					//fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );

					memcpy( pfph->pfop , &(pParam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
					memcpy( pfph->pfphy, &(pParam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
					memcpy( pfph->pfpkg, &(pParam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );
					fj_PrintHeadReset( pfph );
					//fj_print_head_KillENI();
					//fj_print_head_SetGAValues();
					//fj_print_head_InitENI();
					fj_print_head_SetStatus();
					fj_print_head_SendInfoAll();

					{
						char strLabel [FJLABEL_NAME_SIZE + 1] = { 0 };
						long lCount = fj_scratchpadReadCount ();

						fj_scratchpadReadLabelName (strLabel, FJLABEL_NAME_SIZE);
						
						//fj_ResetBRAMCounters( pCVT->pBRAM );
						fj_SystemBuildSelectedLabel (pfsys, strLabel);
						fj_SetCounts (pfph->pfmSelected, lCount);
					}
				}

				tx_semaphore_put( &(pGlobal->semaROMLock) );
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_PH_TYPE:
			psmsgOut->Header.Cmd = IPC_GET_PH_TYPE;
			sprintf ((LPSTR)psmsgOut->Data, "HeadType=%d;", pfph->lFJPhOpIndex);
			psmsgOut->Header.Length = strlen ((LPSTR)psmsgOut->Data);
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_MODE:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_MODE;
			fj_PrintModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_PRINTER_MODE:
			pData[lDataLength] = 0;
			printf ("%s(%d): IPC_SET_PRINTER_MODE [%s]\n", __FILE__, __LINE__, (LPSTR)pData);
			fj_PrintModeFromString( pfph->pfsys, (LPSTR)pData );
			break;

		case IPC_SET_SELECTED_COUNT:
			printf ("%s(%d): IPC_SET_SELECTED_COUNT: %s\n", __FILE__, __LINE__, pData);
			
			if ( 0 == strcmp(apListKeyVal[0],"CountSel"))
			{
				long lCount = atoi(apListKeyVal[1]);
				fj_SetCounts (pfph->pfmSelected, lCount);
			}
			//break;
		case IPC_GET_SELECTED_COUNT:
			fj_print_head_SendSelectedCNT ();
			break;

		case IPC_SELECT_VARDATA_ID:
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{
				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)  pfph->lSerialVarDataID = index;
				else bInvalid = TRUE;
			} else bInvalid = TRUE;
			break;
		case IPC_GET_VARDATA_ID:
			bInvalid = TRUE;
			
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{
				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)
				{
					psmsgOut->Header.Cmd = IPC_SET_VARDATA_ID;
					sprintf( (LPSTR)psmsgOut->Data, "ID=%d;DATA=\"%s\";",
						index+1,
						pGlobal->DynamicSegments[index].strDynData );
					bSendString = TRUE;
					bInvalid = FALSE;
				}
			}
			break;
		case IPC_SET_VARDATA_ID:
			bInvalid = TRUE;
			
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{

				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)
				{
				    memset (pGlobal->DynamicSegments[index].strDynData, 0, FJTEXT_SIZE); // 1.2029
				    
					i = fj_ParseKeywordsFromStr( apListCmd[1], '=', apListKeyVal, 2 );
					if ( 0 == strcmp(apListKeyVal[0],"DATA"))
					{
						lDataLength = strlen( apListKeyVal[1] );
						if ( lDataLength > FJTEXT_SIZE ) lDataLength = FJTEXT_SIZE;
										// empty str should be ""
						if ( 2 < lDataLength )
						{
							pData = apListKeyVal[1];
							*(pData + lDataLength - 1) = 0;
							strcpy( pGlobal->DynamicSegments[index].strDynData, (pData+1));
							fj_writeInfo ((pData+1)); 
							pGlobal->DynamicSegments[index].bChanged = TRUE;
							bInvalid = FALSE;
							
							if (pfph->bSaveDynData) {
								char szFile [FJSYS_MAX_FILENAME];
								int nLen = sizeof (DYNSEGMENT);
								
								sprintf (szFile, "%s/DynData%d.txt", FJ_DIR_PARAMS, index);
			
								if (!WriteFile (nLen, (char *)&fj_CVT.pFJglobal->DynamicSegments [index], szFile)) {
									char sz [FJSYS_MAX_FILENAME + 128];
									sprintf (sz, "Failed to write: %s", szFile);
									TRACEF (sz);
								}
							}
						}
					}
				}
			}
			break;

		case IPC_SAVE_ALL_PARAMS:
			{
				LPFJ_PARAM pParam = fj_CVT.pFJparam;
				LPFJSYSTEM         pfsys;
				LPFJSYSTEM         pfsysNew;
				LPFJPRINTHEAD      pfph;
				LPFJPRINTHEAD_OP   pfop;
				LPFJPRINTHEAD_PHY  pfphy;
				LPFJPRINTHEAD_PKG  pfpkg;

				pfsys = fj_CVT.pfsys;
				pfph  = fj_CVT.pfph;
				pfop  = pfph->pfop;
				pfphy = pfph->pfphy;
				pfpkg = pfph->pfpkg;


				memcpy( &(fj_CVT.pFJparam->FJSys ), pfsys, sizeof(FJSYSTEM) );
				memcpy( &(fj_CVT.pFJparam->FJPhOp ), pfop, sizeof(FJPRINTHEAD_OP ) );
				memcpy( &(fj_CVT.pFJparam->FJPhPhy), pfphy, sizeof(FJPRINTHEAD_PHY) );
				memcpy( &(fj_CVT.pFJparam->FJPhPkg), pfpkg, sizeof(FJPRINTHEAD_PKG) );
				memcpy( &(fj_CVT.pFJparam->FJPh   ), pfph, sizeof(FJPRINTHEAD    ) );

				//if ( TRUE == bIsEdit )
				//{
				pfsysNew = &(fj_CVT.pFJparam->FJSys );
				pfsysNew->lActiveCount = 0;
				tx_semaphore_get( &(pCVT->pFJglobal->semaROMLock), TX_WAIT_FOREVER );
				// write and free ROM lock
				fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
				//fj_WriteNETOSRomParams (fj_CVT.pNETOSparam, TRUE, NULL, 0);
				/*
				fj_PrintHeadReset( pfph );
				//fj_print_head_KillENI();
				//fj_print_head_SetGAEncoder();
				// set encoder switch
				//fj_print_head_SetGAWriteSwitches();
				//fj_print_head_InitENI();
				//fj_print_head_SetGAValues();
				fj_print_head_SetStatus();
				fj_print_head_SendInfoAll();
				//fj_ResetBRAMCounters( pCVT->pBRAM );
				//fj_SystemBuildSelectedLabel( pCVT->pfsys, pCVT->pBRAM->strLabelName );
				*/
				tx_semaphore_put( &(pGlobal->semaROMLock) );
				//}
				//else bInvalid = TRUE;

				psmsgOut->Header.Cmd = IPC_SAVE_ALL_PARAMS;
				sprintf((LPSTR)psmsgOut->Data, "IPC_SAVE_ALL_PARAMS;");
				bSendString = TRUE;
			}
			break;

		case IPC_GET_SW_VER_INFO:
			psmsgOut->Header.Cmd = IPC_STATUS;
			sprintf((LPSTR)psmsgOut->Data, "SW_VER=%s;", fj_getVersionNumber());
			bSendString = TRUE;
			
			if (lDataLength) {
				fj_CVT.pfsys->bPrintDriverPending = (strstr ((LPSTR)pData, "PRINT_DRIVER=T") != NULL);
				
				if (strstr ((LPSTR)pData, "PRINT_DRIVER_DEBUG=T"))
					fj_CVT.pfsys->bPrintDriverDebug = TRUE;
				else if (strstr ((LPSTR)pData, "PRINT_DRIVER_DEBUG=F"))
					fj_CVT.pfsys->bPrintDriverDebug = FALSE;
			}
				
			break;
		case IPC_GET_GA_VER_INFO:
		{
			FPGA_LOCK ();
			WriteByte (LCR, 0x2C);
			UCHAR GAver = ReadByte (CMD_REG);
			printf ("%s(%d): IPC_GET_GA_VER_INFO: 0x%02X\n", __FILE__, __LINE__, GAver);
			BYTE GA_Mver = ((GAver>>4)& 0x0F);
			BYTE GA_Lver = (GAver & 0x0F);
			WriteByte (LCR, 03);
			FPGA_UNLOCK ();
			
			GA_Mver = revfield (GA_Mver, 4);
			GA_Lver = revfield (GA_Lver, 4);
			
			psmsgOut->Header.Cmd = IPC_STATUS;
			sprintf((LPSTR)psmsgOut->Data, "GA_VER=%d.%04d;", GA_Mver, GA_Lver);
			bSendString = TRUE;
		}
		break;
		case IPC_GA_UPGRADE:
			bInvalid = TRUE;
			
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					char szFile [FJSYS_MAX_FILENAME];
					
					// add length of file as last word of rom space
					//BYTE *pByte = pData + (&(fj_RomTable[3]))->lMaxLength - 4;
					//*((LONG *)(pByte- 4)) = (LONG)lDataLength;

					sprintf (szFile, "%s", FJ_DIR_FIRMWARE);
					mkdir (szFile, 0);
					
					//DeleteAllFiles (szFile);
					
					sprintf (szFile, "%s/%s", FJ_DIR_FIRMWARE, "ga.bin");
					printf ("%s(%d): WriteFile : %d bytes [%s]\n", __FILE__, __LINE__, lDataLength, szFile);
					int nWrite = WriteFile (lDataLength, (char *)pData, szFile);
					printf ("%s(%d): nWrite: %d [%s]\n", __FILE__, __LINE__, nWrite, szFile);
					
					//fj_ProgramFPGA (pData, lDataLength);
					
					bInvalid = FALSE;					
					psmsgOut->Header.Cmd = IPC_STATUS;
					sprintf((LPSTR)psmsgOut->Data, "IPC_GA_UPGRADE complete");
					bSendString = TRUE;
					pGlobal->bGAUpgrade = TRUE;
				}
			}
			break;

		case IPC_GET_STATUS_MODE:
			psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
			fj_StatusModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_STATUS_MODE:
			psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
			pData[lDataLength] = 0;
			fj_StatusModeFromString( pfph->pfsys, (LPSTR)pData );
			fj_StatusModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_DISK_USAGE:
			psmsgOut->Header.Cmd = IPC_GET_DISK_USAGE;
			fj_GetDiskUsage ((LPSTR)psmsgOut->Data);
			psmsgOut->Header.Length = strlen ((LPSTR)psmsgOut->Data);
			bSendString = TRUE;
			break;

		default:
			bInvalid = TRUE;
			break;
	}
	if ( TRUE == bInvalid )
	{
		psmsgOut->Header.Cmd = IPC_INVALID;
		*(psmsgOut->Data) = 0;
		bSendString = TRUE;
	}
	if ( TRUE == bSendString )
	{
		lDataLength = strlen( (LPSTR)psmsgOut->Data );
		psmsgOut->Header.Length = lDataLength;
		socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
	}
	if ( TRUE == bSendBinary )
	{
		memcpy( &lDataLength, (pElement-sizeof(LONG)), sizeof(LONG) );
		psmsgBig = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1);
		psmsgBig->Header.Cmd     = psmsgOut->Header.Cmd;
		psmsgBig->Header.BatchID = lBatchID;
		psmsgBig->Header.Length  = lDataLength;
		memcpy( psmsgBig->Data, pElement, lDataLength );
		socketSend( pVoid, (LPBYTE)psmsgBig, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
		fj_free( psmsgBig );
	}

	return;
}
//
// send the high voltage info
//
VOID fj_socketCmdSendVoltageInfo( LPFJPRINTHEAD pfph, LPVOID pVoid, _FJ_SOCKET_MESSAGE *psmsgOut, VOID((*socketSend)( LPVOID pVoid, LPBYTE pByte, LONG lLength))  )
{
	LONG  lDataLength;
	LONG  lVoltage = 145;

	psmsgOut->Header.Cmd = IPC_STATUS;
	//    if ( (TRUE == fj_CVT.pfph->bHeadSelected) &&
	//	     (TRUE == fj_CVT.pFJglobal->bADReady )  )
	if ( (TRUE == pfph->bHeadSelected) )
	{
		sprintf( (LPSTR)psmsgOut->Data, "VoltReady=ON;Voltage=%d;", (int)lVoltage );
		//		if ( TRUE == fj_CVT.pFJglobal->bHeater1Present )
		{
			lDataLength = strlen( (LPSTR)psmsgOut->Data );
			psmsgOut->Header.Length = lDataLength;
			socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
		}
	}

	return;
}
