#ifndef FJ_ROM_H_
#define FJ_ROM_H_

#include "fj.h"

int fj_ReadFJRomParams( LPFJ_PARAM pFJ_PARAM );
void fj_ReadDynData ();
void fj_ReadSerialData ();

#endif /*FJ_ROM_H_*/
