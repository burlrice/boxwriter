#include "fj.h"
#include "fj_scratchpad.h"
#include "typedefs.h"

extern volatile tagPHC_Flags gPHCFlags;

//
// send the photocell light status
//
VOID fj_print_head_SendPhotocellInfo(BOOL bPhotocellOn)
{
	LPSTRING pstr;

	if (bPhotocellOn)					pstr = "PhotoCell=ON;";
	else                                pstr = "PhotoCell=OFF;";
	fj_socketSendAll( IPC_STATUS, (LPBYTE)pstr, strlen(pstr) );
}
//
// send the photocell detect message to outside observers
//
VOID fj_print_head_SendPhotocellDetect( VOID )
{
/* TODO
	CHAR   str[256];

	strcpy( str, "DATETIME=");
	fj_SystemGetDateTimeString( (str+strlen(str) ), &(fj_CVT.pfsys->tmPhotoCell) );
	strcat( str, ";" );
	fj_socketSendAll( IPC_PHOTO_TRIGGER, (LPBYTE)str, strlen(str) );
	fj_print_head_SendSelectedCNT();
*/
}
//
// send the photocell mode message to outside observers
//
VOID fj_print_head_SendPhotocellMode( VOID )
{
/* TODO
	CHAR   str[256];
	LPSTR  pStat;
	pStat = "D;";
	if      ( FJ_PC_MODE_ENABLED == fj_CVT.pFJglobal->lPhotocellMode ) pStat = "E;";
	else if ( FJ_PC_MODE_TEST    == fj_CVT.pFJglobal->lPhotocellMode ) pStat = "T;";
	sprintf(str, "PhotoCellMode=%s",pStat);
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
VOID fj_print_head_SendSelectedCNT( VOID )
{
	char str [FJLABEL_NAME_SIZE + 64] 		= { 0 };
	char strLabel [FJLABEL_NAME_SIZE + 1] 	= { 0 };
	ULONG lCount = fj_scratchpadReadCount ();

	fj_scratchpadReadLabelName (strLabel, FJLABEL_NAME_SIZE);
	
	if (!fj_IsValidLabelName (strLabel) || strLabel [0] == 0) {
		strcpy (strLabel, "(none)");
		lCount = 0;
	}
		
	sprintf(str, "LabelSel=%s;CountSel=%d;", strLabel, lCount);
	//printf ("%s(%d): %s\n", __FILE__, __LINE__, str);
	fj_socketSendAll (IPC_STATUS, (LPBYTE)str, strlen(str));
}
//
// send the printer data ready light status
//
VOID fj_print_head_SendDataReadyInfo( VOID )
{
/* TODO
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	LPSTRING pstr;

	if ( (TRUE == pfph->bHeadSelected) &&
		(TRUE == fj_CVT.pFJglobal->bADReady )  )
	{
		if ( TRUE == fj_CVT.pFJglobal->bDataReady ) pstr = "PrintData=T;";
		else                                        pstr = "PrintData=F;";
		fj_socketSendAll( IPC_STATUS, (LPBYTE)pstr, strlen(pstr) );
	}
*/
}
//
// send the print head status
//
VOID fj_print_head_SendStatus( VOID )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	STRING str [256 + 16] = { 0 };
	int i;

	if (pGlobal->bStatusReport) {
		sprintf (str, "HeadStatus=%s;", fj_CVT.pFJglobal->strHeadStatus);
		
		if ( TRUE == pfph->bInfoStatus )
		{
			fj_writeInfo( str );
			fj_writeInfo( "\n" );
		}
		//if ( pGlobal->lIOBoxSocketIndex != -1) fj_print_head_IOBoxInfo();

		fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
	}
}
//
// send the print head edit status
//
VOID fj_print_head_SendEditStatus( VOID )
{
	LPFJPRINTHEAD pfph = fj_CVT.pfph;
	STRING str[256];

	fj_PrintHeadGetEditStatus( pfph, str );
	fj_socketSendAll( IPC_EDIT_STATUS, (LPBYTE)str, strlen(str) );
}
//
// send the board temperature info
//
VOID fj_print_head_SendBoardTempInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	STRING str[180];

	sprintf( str, "BoardTemperature=%d;", (int)pGlobal->lBoardTemp );
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
//
// send the IOBox info
//
VOID fj_print_head_IOBoxInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPFJ_BRAM   pBRAM   = fj_CVT.pBRAM;
	LPSTR  pLevel;
	STRING strLow[32];
	STRING strHigh[32];
	STRING str[180];

	if ( TRUE == pBRAM->bInkLow )
	{
		if ( fj_PrintHeadGetInkLowVolume (fj_CVT.pfph) > pBRAM->dInkLowML) pLevel = "LOW";
		else pLevel = "EMPTY";
	}
	else pLevel = "FULL";

	if (TRUE == pGlobal->bReady)		//( TRUE == pBRAM->bInkLow )
	{
		if ( TRUE == pBRAM->bInkLow )
		{
			if ( fj_PrintHeadGetInkLowVolume (fj_CVT.pfph) > pBRAM->dInkLowML) fj_socket_IOBoxRedOn();
			else fj_socket_IOBoxRedFlash();
		}
		else fj_socket_IOBoxGreenOn();
	}
	else fj_socket_IOBoxRedFlash();		// Not Ready

	fj_SystemGetDateTimeString( strHigh, &(pBRAM->tmInkFull) );
	fj_SystemGetDateTimeString( strLow,  &(pBRAM->tmInkLow) );

	sprintf( str, "InkLevel=%s;InkLowTime=%s;InkFullTime=%s;", pLevel, strLow, strHigh );
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
//
// send the battery info
//
VOID fj_print_head_SendBatteryInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPSTR  pLevel;
	STRING str[180];

	if ( TRUE == pGlobal->bBatteryOK ) pLevel = "T";
	else                               pLevel = "F";

	sprintf( str, "BatteryOK=%s;", pLevel );
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
//
// send the clock info
//
VOID fj_print_head_SendClockInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPSTR  pLevel;
	STRING str[180];

	if ( TRUE == pGlobal->bClockOK ) pLevel = "T";
	else                             pLevel = "F";

	sprintf( str, "ClockOK=%s;", pLevel );
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
//
// send the shaft encoder info
//
VOID fj_print_head_SendShaftEncoderInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPSTR  pLevel;
	STRING str[180];

	if ( TRUE == pGlobal->bShaftEncoderRunning ) pLevel = "T";
	else                                         pLevel = "F";

	sprintf( str, "SERunning=%s;", pLevel );
	fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
*/
}
//
// send the high voltage info
//
VOID fj_print_head_SendVoltageInfo( VOID )
{
/* TODO
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	STRING str[180];
	LPSTR pStr;

	if (TRUE == fj_CVT.pfph->bHeadSelected)
	{
		if ( TRUE == pGlobal->bVoltageReady ) pStr = "T";
		else                                  pStr = "F";

		sprintf( str, "VoltReady=%s;", pStr );
		fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
	}
*/
}
//
// send the heater info
//
VOID fj_print_head_SendHeaterInfo( LPHEATER pHeater )
{
/* TODO
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;
	STRING str[180];

	if ( (TRUE == pfph->bHeadSelected) &&
		(TRUE == pHeater->bHeaterPresent)  )
	{
		sprintf( str, "%s", pHeater->pNameJava );
		if ( TRUE == pHeater->bHeaterOn ) strcat(str,"On=T;");
		else                              strcat(str,"On=F;");
		strcat(str, pHeater->pNameJava);
		if ( TRUE == wStatus.bits.AtTemp   ) strcat(str,"Ready=T;");
		else                            strcat(str,"Ready=F;");
		if ( TRUE == pfph->bInfoStatus )
		{
			fj_writeInfo( str );
			fj_writeInfo( "\n" );
		}
		fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
	}
*/
}
//
// send the available storage
//
VOID fj_print_head_SendAvailStorage( VOID )
{
/* TODO
	LPFJMEMSTOR pfms = fj_CVT.pfph->pMemStor;
	STRING str[180];

	if ( NULL != pfms )
	{
		sprintf( str, "AvailableStorage=%d;", (int)(pfms->lSize - pfms->lNext) );
		fj_socketSendAll( IPC_STATUS, (LPBYTE)str, strlen(str) );
	}
*/
}
//
// send all the print head info
//
VOID fj_print_head_SendInfoAll(VOID)
{
	
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;

	fj_print_head_SendStatus();
	fj_print_head_SendSelectedCNT();

	if (pfph->bHeadSelected && pGlobal->bADReady) {
		printf ("%s(%d): TODO: impl: fj_print_head_SendInfoAll\n", __FILE__, __LINE__);
/*
		fj_print_head_SendBoardTempInfo();
		fj_print_head_SendVoltageInfo();
		fj_print_head_SendBatteryInfo();
		fj_print_head_SendClockInfo();
		fj_print_head_SendShaftEncoderInfo();
		fj_print_head_IOBoxInfo();
		fj_print_head_SendHeaterInfo( &(pGlobal->heater1) );
		fj_print_head_SendPhotocellMode();
		fj_print_head_SendAvailStorage();
*/
	}
}
//
//  FUNCTION: fj_print_head_LabelCountsZero()
//
//  PURPOSE:  Zero the label counters in one entry
//
static VOID fj_print_head_LabelCountsZero( LPLABELCOUNTER pLC )
{
	memset( pLC, 0, sizeof(LABELCOUNTER) );
	return;
}
//
//  FUNCTION: fj_print_head_LabelCountsShift()
//
//  PURPOSE:  Shift the label counters down by one entry and zero the first one
//
static VOID fj_print_head_LabelCountsShift( VOID )
{
/* TODO
	LPFJ_BRAM pBRAM = fj_CVT.pBRAM;
	int i;

	for ( i = FJSYS_NUMBER_LABEL_COUNTERS-1; i > 0; i-- )
	{
		memcpy( &(pBRAM->LabelCounters[i]), &(pBRAM->LabelCounters[i-1]), sizeof(LABELCOUNTER) );
	}
	fj_print_head_LabelCountsZero( &(pBRAM->LabelCounters[0]) );
*/
}
//
//  FUNCTION: fj_print_head_CountsInc()
//
//  PURPOSE:  Increment the statistic counters in one structure
//
static VOID fj_print_head_CountsInc( LPRASTERCOUNTS pRC, LONG lMessages, LONG lDots )
{
/* TODO
	pRC->lMessages     += lMessages;
	pRC->lDotsLo       += lDots;
	if ( pRC->lDotsLo > 1000000 )
	{
		pRC->lDotsLo -= 1000000;
		pRC->lDotsHi++;
	}
*/
}
//
//  FUNCTION: fj_print_head_CountsZero()
//
//  PURPOSE:  Zero the statistic counters in one structure
//
static VOID fj_print_head_CountsZero( LPRASTERCOUNTS pRC )
{
	pRC->lMessages     = 0;
	pRC->lDotsHi       = 0;
	pRC->lDotsLo       = 0;
	return;
}
//
//  FUNCTION: fj_print_head_CountsCopy()
//
//  PURPOSE:  copy the statistic counters from one structure to another
//
static VOID fj_print_head_CountsCopy( LPRASTERCOUNTS pRCTo, LPRASTERCOUNTS pRCFrom )
{
	pRCTo->lMessages     = pRCFrom->lMessages;
	pRCTo->lDotsHi       = pRCFrom->lDotsHi;
	pRCTo->lDotsLo       = pRCFrom->lDotsLo;
	return;
}
//
//  FUNCTION: fj_print_head_CountsAdd()
//
//  PURPOSE:  Add the statistic counters in two structures
//
static VOID fj_print_head_CountsAdd( LPRASTERCOUNTS pRCTotal, LPRASTERCOUNTS pRCAdd )
{
/* TODO
	pRCTotal->lDotsHi       += pRCAdd->lDotsHi;
	fj_print_head_CountsInc( pRCTotal, pRCAdd->lMessages, pRCAdd->lDotsLo );
*/
}
//
//  FUNCTION: fj_print_head_Counters()
//
//  PURPOSE:  Increment all the statistic counters
//
VOID fj_print_head_Counters( LONG lMessages, LONG lDots )
{
/* TODO
	CLPFJ_BRAM      pBRAM = fj_CVT.pBRAM;
	CLPCFJPRINTHEAD pfph  = fj_CVT.pfph;
	LPFJMESSAGE     pfm;
	LPFJELEMENT     pfe;
	LPFJCOUNTER     pfc;
	time_t tThisPrint;
	time_t tLastPrint;
	struct tm tmThis;
	STR    sLabelName[1+FJLABEL_NAME_SIZE];
	LONG   lThisMin;
	LONG   lLastMin;
	LONG   lThisHour;
	LONG   lLastHour;
	LONG   lThisDay;
	LONG   lLastDay;
	RASTERCOUNTS rc;
	LONG   lDelta;
	int    i;
	int    j;

	if ( 0 != lMessages )
	{
		memcpy( &tmThis, &(fj_CVT.pfsys->tmPhotoCell), sizeof(tmThis) );
	}
	else
	{
		pCVT->fj_SystemGetTime( &tmThis );
	}
	tThisPrint = fj_mktime( &tmThis );
	tLastPrint = fj_mktime( &(pBRAM->tmLastPrint) );
	lThisMin = tThisPrint / 60;
	lLastMin = tLastPrint / 60;
	if ( lThisMin > lLastMin  )
	{
		lDelta = lThisMin - lLastMin;
		fj_print_head_CountsZero( &rc );
		for ( i = 59; i >= 0; i-- )
		{
			j = i - lDelta;
			if ( j >= 0 )
			{
				fj_print_head_CountsCopy( &(pBRAM->countMinutes[i]), &(pBRAM->countMinutes[j]) );
				fj_print_head_CountsAdd ( &rc                      , &(pBRAM->countMinutes[j]) );
			}
			else
			{
				fj_print_head_CountsZero( &(pBRAM->countMinutes[i]) );
			}
		}
		fj_print_head_CountsCopy( &(pBRAM->count60Minutes), &rc );

	}
	lThisHour = tThisPrint / (60*60);
	lLastHour = tLastPrint / (60*60);
	if ( lThisHour > lLastHour  )
	{
		lDelta = lThisHour - lLastHour;
		fj_print_head_CountsZero( &rc );
		for ( i = 23; i >= 0; i-- )
		{
			j = i - lDelta;
			if ( j >= 0 )
			{
				fj_print_head_CountsCopy( &(pBRAM->countHours[i]), &(pBRAM->countHours[j]) );
				fj_print_head_CountsAdd ( &rc                    , &(pBRAM->countHours[j]) );
			}
			else
			{
				fj_print_head_CountsZero( &(pBRAM->countHours[i]) );
			}
		}
		fj_print_head_CountsCopy( &pBRAM->count24Hours, &rc );
		fj_print_head_CountsZero( &pBRAM->countClockHour );
	}
	lThisDay = tThisPrint / (24*60*60);
	lLastDay = tLastPrint / (24*60*60);
	if ( lThisDay > lLastDay  )
	{
		lDelta = lThisDay - lLastDay;
		fj_print_head_CountsZero( &rc );
		for ( i = 29; i >= 0; i-- )
		{
			j = i - lDelta;
			if ( j >= 0 )
			{
				fj_print_head_CountsCopy( &(pBRAM->countDays[i]), &(pBRAM->countDays[j]) );
				fj_print_head_CountsAdd ( &rc                     , &(pBRAM->countDays[j]) );
			}
			else
			{
				fj_print_head_CountsZero( &(pBRAM->countDays[i]) );
			}
		}
		fj_print_head_CountsCopy( &(pBRAM->count30Days), &rc );
		fj_print_head_CountsZero( &(pBRAM->countCalDay) );
	}

	if ( lThisDay != lLastDay  )
	{
		if ( ( tmThis.tm_mon  == pBRAM->tmLastPrint.tm_mon  ) &&
			( tmThis.tm_year == pBRAM->tmLastPrint.tm_year ) )
		{
		}
		else
		{
			fj_print_head_CountsZero( &(pBRAM->countCalMonth) );
		}
	}

	if ( 0 != lMessages )
	{
		fj_print_head_CountsZero( &rc );
		rc.lMessages     = lMessages;
		rc.lDotsLo       = lDots;

		fj_print_head_CountsAdd ( &(pBRAM->countInService) , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countCalMonth)  , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countCalDay)    , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countClockHour) , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->count30Days)    , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->count24Hours)   , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->count60Minutes) , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countCustomer)  , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countInkFull)   , &rc );
		if ( TRUE == pBRAM->bInkLow )
		{
			DOUBLE d;

			fj_print_head_CountsAdd ( &(pBRAM->countInkLow)   , &rc );

			d  = (DOUBLE)pBRAM->countInkLow.lDotsHi * (DOUBLE)1000000.0;
			d += (DOUBLE)pBRAM->countInkLow.lDotsLo;
			d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;

			pBRAM->dInkLowML = d;
		}
		fj_print_head_CountsAdd ( &(pBRAM->countMinutes[0]) , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countHours  [0]) , &rc );
		fj_print_head_CountsAdd ( &(pBRAM->countDays   [0]) , &rc );

		memcpy( &(pBRAM->tmLastPrint), &tmThis, sizeof(struct tm) );

		// process label counters
		// by the time we get here, the counters have been imaged and bumped.
		// so for the display of the individual counters, we must use the old values.
		sLabelName[0] = 0;
		if ( NULL != fj_CVT.pfsys->pfl ) strcpy( sLabelName, fj_CVT.pfsys->pfl->strName );
		if ( (0 != sLabelName[0]) && (0 != strcmp(pBRAM->LabelCounters[0].cLabelName, sLabelName)) )
		{
			fj_print_head_LabelCountsShift();
			strcpy(pBRAM->LabelCounters[0].cLabelName, fj_CVT.pfsys->pfl->strName );
			memcpy( &(pBRAM->LabelCounters[0].tmFirst), &tmThis, sizeof(struct tm) );
			j = 0;
			i = 0;
			pfm = pfph->pfmSelected;
			if ( NULL != pfm )
			{
				while ( (j < 2) && (i < pfm->lCount) )
				{
					pfe = pfm->papfe[i];
					if ( FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType() )
					{
						pfc = (LPFJCOUNTER)pfe->pDesc;
						if ( 0 == j ) pBRAM->LabelCounters[0].lCount1First = pfc->lOldValue;
						if ( 1 == j ) pBRAM->LabelCounters[0].lCount2First = pfc->lOldValue;
						j++;
					}
					i++;
				}
			}
		}
		memcpy( &(pBRAM->LabelCounters[0].tmLast), &tmThis, sizeof(struct tm) );
		pBRAM->LabelCounters[0].lCount++;

		j = 0;
		i = 0;
		pfm = pfph->pfmSelected;
		if ( NULL != pfm )
		{
			while ( (j < 2) && (i < pfm->lCount) )
			{
				pfe = pfm->papfe[i];
				if ( FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType() )
				{
					pfc = (LPFJCOUNTER)pfe->pDesc;
					if ( 0 == j ) pBRAM->LabelCounters[0].lCount1Last = pfc->lOldValue;
					if ( 1 == j ) pBRAM->LabelCounters[0].lCount2Last = pfc->lOldValue;
					j++;
				}
				i++;
			}
		}
	}
*/
}
//
//  FUNCTION: fj_print_head_CountsCustomerReset()
//
//  PURPOSE:  Zero the customer statistic counters and reset the saved time
//
VOID fj_print_head_CountsCustomerReset(VOID)
{
/* TODO
	fj_SystemGetTime( &(fj_CVT.pBRAM->tmCustomerReset) );
	fj_print_head_CountsZero( &(fj_CVT.pBRAM->countCustomer) );
*/
}
//
//  FUNCTION: fj_print_head_CountsResetAll()
//
//  PURPOSE:  Zero all statistic counters
//
VOID fj_print_head_CountsResetAll( VOID )
{
/* TODO
	LPFJ_BRAM   pBRAM   = fj_CVT.pBRAM;
	int i;

	fj_print_head_CountsZero( &(pBRAM->countInService) );
	fj_print_head_CountsZero( &(pBRAM->countCalMonth)  );
	fj_print_head_CountsZero( &(pBRAM->countCalDay)    );
	fj_print_head_CountsZero( &(pBRAM->countClockHour) );
	fj_print_head_CountsZero( &(pBRAM->count30Days)    );
	fj_print_head_CountsZero( &(pBRAM->count24Hours)   );
	fj_print_head_CountsZero( &(pBRAM->count60Minutes) );
	fj_print_head_CountsZero( &(pBRAM->countCustomer)  );
	fj_print_head_CountsZero( &(pBRAM->countInkFull)   );
	fj_print_head_CountsZero( &(pBRAM->countInkLow)    );

	for ( i = 0; i < 60 ; i++ )
	{
		fj_print_head_CountsZero( &(pBRAM->countMinutes[i]) );
	}
	for ( i = 0; i < 24 ; i++ )
	{
		fj_print_head_CountsZero( &(pBRAM->countHours[i]) );
	}
	for ( i = 0; i < 30 ; i++ )
	{
		fj_print_head_CountsZero( &(pBRAM->countDays[i]) );
	}

	for ( i = 0; i < FJSYS_NUMBER_LABEL_COUNTERS; i++ )
	{
		fj_print_head_LabelCountsZero( &(pBRAM->LabelCounters[i]) );
	}
*/
}
