#include "fj.h"
#include "fj_netsilicon.h"

/*
extern WORD16 LGetSectorNumber(WORD16 id, WORD16 segment_number, WORD32 segment_offset);
// FoxJet
extern WORD32 LGetSectorOffset(WORD16 id, WORD16 segment_number, WORD32 segment_offset);
extern WORD32 LGetSectorSize(WORD32 sectorNumber);
extern WORD8  identify_flash( void );
extern WORD8  flash_id;
*/

//
// actually write the data into ROM.
// the end of the RomEntry is cleared out.
// any data in the sector, before or after the RomEntry, is copied from current ROM.
//
// NOTE: zero lengths may occur on some of the calls to memcpy and memset.
//       this is OK
//       the code would be harder to read if these conditions were tested.
//
LONG fj_writeROM( CLPCROMENTRY pRomEntry,
CLPCBYTE pRamBuffer,
LONG lCurrentLength )
{
	printf ("%s(%d): fj_writeROM: TODO\n", __FILE__, __LINE__);
	return 0;
/* TODO
	LONG    lSectorNumber;				// hardware sector number
	LONG    lSectorOffset;				// sector offset from start of ROM
	LONG    lSectorSize;				// hardware sector size
	LONG    lSector32Number;			// 32k software sector number
	LONG    lSector32Offset;			// offset into 32k sector
	LONG    lRomSectorMax;				// max size of a ROM sector on this machine
	LPBYTE  pRomBuffer;					// pointer to buffer for a ROM sector
	WORD8   wFlashID;					// flash type

	LONG    lWorkOffset;
	LONG    lOffsetDelta;
	LONG    lWorkLength;
	LONG    lRemainder;
	LONG    lWorkRemainder;
	LONG    lWorkRamOffset;
	LONG    lWritten;
	WORD32  wCheckSum;
	WORD32  *pCheckSum;
	BOOL    bWriteGood;
	int     iRetFlash;
	int     i;
	CHAR    str[88];

	bWriteGood = TRUE;
	pRomBuffer = NULL;
	lWritten = 0;
	wCheckSum = 0;

	if ( NULL != pRomEntry )
	{
		// get the flash ROM type
		wFlashID = identify_flash();
		if ( 0xFF == wFlashID ) wFlashID = 0;

		// flash_id in naflash.c must be set for some of its programs to work
		flash_id = wFlashID;

		//  64k
		if (((*(NARM_MEM)).cs0.csor.reg & 0x0000000C) == 0x4) lRomSectorMax =  64*1024;
		// 128k
		else                                                  lRomSectorMax = 128*1024;
		pRomBuffer = (LPBYTE)fj_malloc( lRomSectorMax );
										// out of memory (!!!)
		if ( NULL == pRomBuffer ) return (0);

		lWorkOffset = pRomEntry->lOffset;
		lRemainder = pRomEntry->lMaxLength - lCurrentLength;
		lWorkRamOffset = 0;
		while ( ((lWorkRamOffset < lCurrentLength)||(lRemainder > 0)) && (TRUE == bWriteGood) )
		{
			// this offset's starting sector number, sector offset, and size
			// must give input as if we already had all 32k sectors
			lSector32Number = lWorkOffset / (32*1024);
			lSector32Offset = lWorkOffset - (lSector32Number*(32*1024));
			lSectorNumber = LGetSectorNumber( wFlashID, lSector32Number, lSector32Offset );
			lSectorOffset = LGetSectorOffset( wFlashID, lSector32Number, lSector32Offset );
			lSectorSize   = LGetSectorSize( lSectorNumber );
			lOffsetDelta = lWorkOffset-lSectorOffset;
			// fill front of sector buffer from current ROM, if necessary
			if ( 0 != lOffsetDelta )
			{
				memcpy( pRomBuffer,
					fj_CVT.pFJglobal->pROMLow+lSectorOffset,
					lOffsetDelta );
			}
			// move new data
			pCheckSum = NULL;
			lWorkLength = lCurrentLength - lWorkRamOffset;
			if ( lWorkLength > 0 )
			{
				if ( lWorkLength > (lSectorSize-lOffsetDelta) ) lWorkLength = (lSectorSize-lOffsetDelta);
				memcpy( pRomBuffer+lOffsetDelta,
					pRamBuffer+lWorkRamOffset,
					lWorkLength );		// move new data
				pCheckSum = (WORD32 *)(pRomBuffer+lOffsetDelta);
				for ( i = 0; i < lWorkLength; i += 4, pCheckSum++ )
				{
					wCheckSum ^= *pCheckSum;
				}
				if ( i == lWorkLength )
				{
					pCheckSum = NULL;	// ended on a full word. nothing to complete.
				}
				else
				{
					pCheckSum--;		// back up to last WORD32.
										// undo it until buffer is cleared.
					wCheckSum ^= *pCheckSum;
				}
				lWorkRamOffset += lWorkLength;
			}
			else
			{
				lWorkLength = 0;
			}

			// clear end of RomTable entry
			// fill end of sector buffer from current ROM, if necessary
			if ( lWorkRamOffset >= lCurrentLength )
			{
				lWorkRemainder = lRemainder;
				if ( 0 != lRemainder )	// clear ROMENTRY area that was not downloaded
				{
					if ( lWorkRemainder > (lSectorSize-lOffsetDelta-lWorkLength) ) lWorkRemainder = (lSectorSize-lOffsetDelta-lWorkLength);
					memset( pRomBuffer+lOffsetDelta+lWorkLength,
						0,
						lWorkRemainder );
					lRemainder -= lWorkRemainder;
					// finish last word of non-zero
					if ( NULL != pCheckSum ) wCheckSum ^= *pCheckSum;
				}
				// store checksum at end of RomEntry
				if ( 0 == lRemainder )
				{
					pCheckSum = (WORD32 *)(pRomBuffer+lOffsetDelta+lWorkLength+lWorkRemainder-4);
										// undo where checksum goes
					wCheckSum ^= *pCheckSum;
										// store checksum
					*pCheckSum = 0xffffffff - wCheckSum;
				}
				// fill end of sector past the RomEntry
				if ( 0 == lRemainder )	// copy data past this ROMENTRY
				{
					memcpy( pRomBuffer+lOffsetDelta+lWorkLength+lWorkRemainder,
						fj_CVT.pFJglobal->pROMLow+lSectorOffset+lOffsetDelta+lWorkLength+lWorkRemainder,
						lSectorSize - (lOffsetDelta+lWorkLength) - lWorkRemainder);
				}
			}

			iRetFlash = NAflash_write_fj( lSectorNumber, pRomBuffer );
			if ( 0 == iRetFlash )
			{
				lWritten += lSectorSize;
			}
			else
			{
				bWriteGood = FALSE;
			}
			lWorkOffset = lSectorOffset + lSectorSize;
		}

		if ( NULL != pRomBuffer ) fj_free( pRomBuffer );
	}

	return( lWritten );
*/
}
