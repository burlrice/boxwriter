#include "fj.h"
#include "fj_netsilicon.h"
#include <appconf.h>					// for DEFINEd application options
//#include <narmsrln.h>					// for nvparams & dialog port output
#include "nacache.h"
#include "bsp.h"
//#include <ncc_io.h>
#include <netosIo.h>					// for OPEN options
#include <netoserr.h>
#include <nareset.h>
#include <narmled.h>
#include <na_isr.h>
#include <sysClock.h>
#include <mailcapi.h>
#include <hservapi.h>
#include <ncc_init.h>
#include "reg_def.h"
#include "watchdog.h"
#include "bsptimer.h"
#include "fj_misc.h"
#include "noncache.h"
#include "nagpio.h"
#include "sockapi.h"

TX_THREAD thrSerial;

//static char cThreadRaster [FJ_THREAD_NAMES_STACK_SIZE];
static char cSerialStack [FJ_THREAD_NAMES_STACK_SIZE] = { 0 };

#include <stdio.h>

#include "fj.h"
#include "fj_mem.h"
#include "fj_scratchpad.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_file.h"
#include "mphc.h"
#include "fj_serial.h"

extern LPBYTE pImageBuffer;
extern TX_MUTEX gMutexFPGA;
extern BOOL bSerialFct;

BOOL gbPrintCycle 			= FALSE;
BOOL gbIdleOnEOP 			= FALSE;
BOOL gbIdledOnError 		= FALSE;

STROBE_SETTING_STRUCT strobe;
UCHAR gnErrDrv 		= 0x00;
UCHAR gnStatusReg 	= 0x00;

// used in marmsrln.c to keep the NETOS parameters in RAM instead of NVRAM
// we do not have a NVRAM.
// used in fj_init.c to write them to ROM
extern    BOOL    bDialogParamChange;
extern    BOOL    fj_bNetosParamsChanged;
extern    ULONG   ldhcpAddr;
extern    ULONG   ldhcpMask;
extern    ULONG   ldhcpGateway;

// __ghsend_free_mem is set up by the Green Hills initialization software to point to
// the end of what is linked and loaded.
// the NET+OS standard link files pads out the free_mem segment to 64k.
//extern      BYTE __ghsend_free_mem;

//
// global info
//
BOOL      fj_watchDog    = FALSE;		//TRUE;			// if true, use watchdog timer, if other conditions are OK
BOOL      fj_noPrintDev  = FALSE;		// if true, do not use any FoxJet chips and/or devices
BOOL      fj_writeROMMsg = FALSE;		// if true, always reset ROM messages on boot up
BOOL      fj_rasterLoop  = FALSE;		// if true, have a background loop running to generate a load and measure throughput
BOOL      fj_bootInfo    = TRUE;		// if true, writes boot-time info to the serial port
struct tm fj_tmStandby;
struct tm fj_tmAPS;

VOID fj_print_head_OnPhotocell (unsigned char uStatus)
{
	LPFJ_GLOBAL   pGlobal	= fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph		= fj_CVT.pfph;

	if (pfph->pfmSelected) {
		fj_MessagePhotocellBuild (pfph->pfmSelected);
		ULONG lPixels = fj_SystemBuildImage (pfph->pfmSelected);
		tagIndicators status;
		
		DecodeIndicators (&status, uStatus);
		/*
		printf ("%s(%d): status.InkLow: %d\n", __FILE__, __LINE__, status.InkLow);
		printf ("%s(%d): status.HVOK: %d\n", __FILE__, __LINE__, status.HVOK);
		printf ("%s(%d): status.AtTemp: %d\n", __FILE__, __LINE__, status.AtTemp);
		printf ("%s(%d): status.HeaterOn: %d\n", __FILE__, __LINE__, status.HeaterOn);
		printf ("%s(%d): uStatus: %d%d%d%d %d%d%d%d\n", __FILE__, __LINE__, 
			((uStatus & (1 << 0)) ? 1 : 0),
			((uStatus & (1 << 1)) ? 1 : 0),
			((uStatus & (1 << 2)) ? 1 : 0),
			((uStatus & (1 << 3)) ? 1 : 0),
			((uStatus & (1 << 4)) ? 1 : 0),
			((uStatus & (1 << 5)) ? 1 : 0),
			((uStatus & (1 << 6)) ? 1 : 0),
			((uStatus & (1 << 7)) ? 1 : 0));
		*/
		
		if (status.InkLow == FALSE) {
			double dInk = fj_PrintHeadGetInkLowVolume (pfph);
	
			//printf ("%s(%d): dInk: %f\n", __FILE__, __LINE__, dInk);
			fj_scratchpadWriteInkUsage (dInk);
		}
		else {
			double dInk = fj_scratchpadReadInkUsage ();
			double dUsage = fj_CalcInkUsage (lPixels);
			
			if (dInk < dUsage) {
				//gbIdledOnError = TRUE;
				//fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
				gbIdleOnEOP = TRUE;
				dInk = 0.0;
			}
			else 
				dInk -= dUsage;
				
			//printf ("%s(%d): dInk: %.09f, dUsage: %.09f\n", __FILE__, __LINE__, dInk, dUsage);
			
			fj_scratchpadWriteInkUsage (dInk);
		}
			
	    fj_SystemGetTime (&fj_tmStandby);
	    
		{
			ULONG lCount = fj_scratchpadReadCount () + 1;

			fj_scratchpadWriteCount (lCount);
			
			if (pGlobal->bStatusReport)
				fj_print_head_SendSelectedCNT ();
		}
	}
}

void fj_EndPrintDriverJob ()
{
	gbIdleOnEOP = FALSE;
	fj_SysSetPrintDriverMode (fj_CVT.pfsys, "F");
	fj_StatusModeFromString (fj_CVT.pfsys, "STATUS_MODE=ON;");
	fj_print_head_SendSelectedCNT ();
	TRACEF ("PrintDriver: IDLE");	
	fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
}

extern BOOL gbFPGA;
extern volatile tagPHC_Flags gPHCFlags;
void fj_photocell_check (UCHAR uData)
{
	if (gbFPGA) {
		BOOL bSendStatus = FALSE;

		if (FPGA_LOCK ()) {
			if (gPHCFlags.PhotocellActivated) {
				gPHCFlags.PhotocellActivated = FALSE;
				gbPrintCycle = TRUE;

				if (fj_CVT.pfph->pfmSelected) {
					bSendStatus = TRUE;
					printf ("%s(%d): photocell detected: %f fpm\n", __FILE__, __LINE__, ReadLineSpeed ());
					fj_SystemGetTime (&fj_CVT.pfsys->tmPhotoCell);
					fj_print_head_OnPhotocell (uData);
					fj_print_head_SendPhotocellInfo (TRUE);
				}
			}
			else if (gPHCFlags.EndOfPrint) {
				gPHCFlags.EndOfPrint = FALSE;
				gbPrintCycle = FALSE;

				if (fj_CVT.pfsys->bPrintDriver) {
					ULONG lCount = fj_scratchpadReadCount ();

					{ char sz [128]; sprintf (sz, "count: %d [%d]", lCount, fj_CVT.pfsys->nPrintDriverCount); TRACEF (sz); }
					
					if (lCount >= fj_CVT.pfsys->nPrintDriverCount) 
						fj_EndPrintDriverJob ();
				}

				if (gbIdleOnEOP) {
					gbIdleOnEOP = FALSE;
					gbIdledOnError = TRUE;
					//TRACEF ("IDLE");
					fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
				}
				
				if (fj_CVT.pfph->pfmSelected) {
					bSendStatus = TRUE;
					printf ("%s(%d): eop detected: %f fpm\n", __FILE__, __LINE__, ReadLineSpeed ());
					fj_print_head_SendPhotocellInfo (FALSE);
				}
			}
		}
						
		if (bSendStatus) {
			fj_print_head_SetStatus ();
			fj_print_head_SendInfoAll ();
		}
	}
}

STROBESTATE SetStrobeState (STROBESTATE s1, STROBESTATE s2)
{
	STROBESTATE s = s2;
	
	if (s1 == STROBESTATE_ON || s2 == STROBESTATE_ON)		return STROBESTATE_ON;
	else													return STROBESTATE_OFF;
}

void DecodeStrobeState (ULONG lTickCount, tagIndicators * pStatus)
{
	STROBESTATE green 	= STROBESTATE_OFF;
	STROBESTATE yellow 	= STROBESTATE_OFF;
	STROBESTATE red 	= STROBESTATE_OFF;
	UCHAR nErrDrv = 0x00;
					
 	if (pStatus->HVOK && pStatus->AtTemp && !pStatus->InkLow) {
		green 	= strobe.m_Ready.m_green;
		yellow 	= strobe.m_Ready.m_yellow;
		red 	= strobe.m_Ready.m_red;
	}
	else {
		if (!pStatus->HVOK) {
			green 	= SetStrobeState (green, 	strobe.m_HighVoltage.m_green);
			yellow 	= SetStrobeState (yellow, 	strobe.m_HighVoltage.m_yellow);
			red 	= SetStrobeState (red, 		strobe.m_HighVoltage.m_red);
		}
			
		if (!pStatus->AtTemp) {
			green 	= SetStrobeState (green, 	strobe.m_LowTemp.m_green);
			yellow 	= SetStrobeState (yellow, 	strobe.m_LowTemp.m_yellow);
			red 	= SetStrobeState (red, 		strobe.m_LowTemp.m_red);
		}
		
		if (pStatus->InkLow) {
			double dInk = fj_scratchpadReadInkUsage ();
			
			if (dInk <= 0.0) {
				green 	= SetStrobeState (green, 	strobe.m_OutOfInk.m_green);
				yellow 	= SetStrobeState (yellow, 	strobe.m_OutOfInk.m_yellow);
				red 	= SetStrobeState (red, 		strobe.m_OutOfInk.m_red);
			}
			else {
				green 	= SetStrobeState (green, 	strobe.m_LowInk.m_green);
				yellow 	= SetStrobeState (yellow, 	strobe.m_LowInk.m_yellow);
				red 	= SetStrobeState (red, 		strobe.m_LowInk.m_red);
			}
		}
	}
   
	if (gPHCFlags.AmsActive) {
		green 	= SetStrobeState (green, 	strobe.m_APS.m_green);
		yellow 	= SetStrobeState (yellow, 	strobe.m_APS.m_yellow);
		red 	= SetStrobeState (red, 		strobe.m_APS.m_red);
	}
	
	if (fj_CVT.pfsys->bPrintDriverPending) {
		green 	= SetStrobeState (green, 	strobe.m_Driver.m_green);
		yellow 	= SetStrobeState (yellow, 	strobe.m_Driver.m_yellow);
		red 	= SetStrobeState (red, 		strobe.m_Driver.m_red);
		
		if (yellow == STROBESTATE_ON || red == STROBESTATE_ON)
			green = STROBESTATE_OFF;
	}

	if (red == STROBESTATE_ON)		nErrDrv |= 0x01;
	if (yellow == STROBESTATE_ON)	nErrDrv |= 0x04;
	if (green == STROBESTATE_ON)	nErrDrv |= 0x02;
	
	{ //if (gnErrDrv != nErrDrv) {
		tagInterruptCtrlReg t;

		///*
		if (gnErrDrv != nErrDrv) {
			char sz [128];
			char szTime [128];
			struct tm tmStamp;
			
			fj_SystemGetTime (&tmStamp);
			strftime (szTime, ARRAYSIZE (szTime), "%H:%M:%S", &tmStamp);
			
			sprintf (sz, "[%s] strobe: %s%s%s [0x%02X]", 
				szTime,
				(nErrDrv & 0x01) ? "R" : "",
				(nErrDrv & 0x04) ? "Y" : "",
				(nErrDrv & 0x02) ? "G" : "",
				nErrDrv);
			TRACEF (sz);
		}
		//*/
		
		memset (&t, 0, sizeof (t));
		t._Channel 	= 2;
		t._Mode 	= 1;
		t._ErrDrv 	= gnErrDrv = nErrDrv;
		
		WriteRegister (INTR_CONTROL_REG, &t, sizeof (t));
		
		BOOL bPrintIdle = fj_scratchpadReadPrintState ();
		
		if (red == STROBESTATE_ON) {
			if (!bPrintIdle) {
				char strLabelName [FJLABEL_NAME_SIZE + 1] = { 0 };
			
				fj_scratchpadReadLabelName (strLabelName, FJLABEL_NAME_SIZE);
				
				if (strLabelName [0] != 0) 
					gbIdleOnEOP = TRUE;
			}
		}
		else {
			if (gbIdledOnError) {
				gbIdledOnError = FALSE;
				fj_SysSetPrintMode (fj_CVT.pfsys, "RESUME");
				//TRACEF ("RESUME");
			}
		}
	}
}					

void fj_CheckPrintDriverCancel ()
{
	if (fj_CVT.pfsys->bPrintDriver) {
		tagInterfaceStatusReg s;

		memset (&s, 0, sizeof (s));
		ReadRegister (0x04, &s, sizeof (s));

		if (s._OTBIP) {
			char str [64] = { 0 };
			
			TRACEF ("PrintDriver: CANCEL");
			fj_EndPrintDriverJob ();
			sprintf(str, "CancelPrintJob=1;");
			fj_socketSendAll (IPC_STATUS, (LPBYTE)str, strlen(str));
		}
	}
}

void fj_run(void)
{
	//UINT    iOldPriority;
	BOOL bLoop = TRUE;
	ULONG lTickStatusCheck = tx_time_get ();
	ULONG lTickStatusSend = tx_time_get ();
	ULONG lUserLED = 0;
	UCHAR uState = 0;
	double dInk = fj_scratchpadReadInkUsage ();
	double dMax = fj_PrintHeadGetInkLowVolume (fj_CVT.pfph);
    const BOOL bDebug = are_we_in_debugger ();
    double dTimeout = 60.0;

    fj_SystemGetTime (&fj_tmStandby);
    fj_SystemGetTime (&fj_tmAPS);

	if (dInk < dMax) {
		uState |= 0x20;
		TriggerLowInk ();
		
		if (dInk <= 0) {
			gbIdledOnError = TRUE;
			fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
		}
	}
	
//	WATCHDOG_CCODE code = watchdogServicePeriodically ();
//	printf ("%s(%d): watchdogServicePeriodically: %d\n", __FILE__, __LINE__, code);

	//tx_thread_priority_change(tx_thread_identify(), FJ_THREAD_LOW_PRIORITY, &iOldPriority);

	while (bLoop) 	{
		BOOL bSendStatus = FALSE;
	
		if (!fj_CVT.pfph->bEdit && fj_bKeepAlive) {
		    struct tm tmNow;
			time_t now;
			time_t last;
			double dDiff;

		    fj_SystemGetTime (&tmNow);
		    now = mktime (&tmNow);
		    last = mktime (&fj_tmKeepAlive);
		    dDiff = difftime (now, last);
	
		    if (dDiff >= dTimeout) {
				fj_reboot ();
				return;
		    }
		}
		
 		if (fj_bStrokeWatchdog && !fj_CVT.pFJglobal->bReset)
			watchdogServiceOnce ();			
		
		fj_photocell_check (uState);
		fj_print_head_Check();
//		fj_nameCheck();
//		fj_timeCheck();
		fj_socketCheck();
		fj_checkSerialPort();

		if ((tx_time_get () - lTickStatusCheck) > NS_MILLISECONDS_TO_TICKS (1000)) {
			UCHAR uData = 0;
			UCHAR uMask = 0xF0;
			tagIndicators status;

			lTickStatusCheck = tx_time_get ();
			
			if (fj_CVT.pfsys->bPrintDriver) 
				fj_CheckPrintDriverCancel ();
				
			ReadRegister (IFACE_STAUS_REG, &uData, sizeof (uData));
			naGpioSetOutputValue (GPIO_USER_LED1, lUserLED++ % 2);
			gnStatusReg = uData;
			uData = uData & uMask;
			
			DecodeIndicators (&status, uData);	
			DecodeStrobeState (lUserLED, &status);
			
			if (uState != uData) {
				/*
				char sz [128];
				char szTime [128];
				struct tm tmStamp;
				UCHAR uTmp = EncodeIndicators (&status);
				
				fj_SystemGetTime (&tmStamp);
				strftime (szTime, ARRAYSIZE (szTime), "%H:%M:%S", &tmStamp);
				
				sprintf (sz, "[%s] HVOK=%s;InkLow=%s;AtTemp=%s;HeaterOn=%s; [%d%d%d%d][%d%d%d%d]",
					szTime,
					status.HVOK		? "T" : "F",		// InkLevel
					status.InkLow	? "T" : "F", 		// VoltReady
					status.AtTemp	? "T" : "F",		// Heater1Ready
				 	status.HeaterOn	? "T" : "F",		// Heater1On
					((uData & (1 << 4)) ? 1 : 0),
					((uData & (1 << 5)) ? 1 : 0),
					((uData & (1 << 6)) ? 1 : 0),
					((uData & (1 << 7)) ? 1 : 0),
					((uTmp & (1 << 4)) ? 1 : 0),
					((uTmp & (1 << 5)) ? 1 : 0),
					((uTmp & (1 << 6)) ? 1 : 0),
					((uTmp & (1 << 7)) ? 1 : 0));
				TRACEF (sz);
		 		*/
		 		
				uState = uData;
				bSendStatus = TRUE;
			}
		}

		if ((tx_time_get () - lTickStatusSend) > NS_MILLISECONDS_TO_TICKS (5000)) 
			bSendStatus = TRUE;

		if (fj_CVT.pFJglobal->bStatusReport && bSendStatus) {
			fj_print_head_SetStatus ();
			fj_print_head_SendInfoAll ();
			lTickStatusSend = tx_time_get ();
		}
	
		tx_thread_sleep(1);				// short delay
	}
}

unsigned char bm[] =								// FoxJet logo in dynimage/raster image format
{
	0x18,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x7E,0x00,0x00,0x00,
	0x7E,0x00,0x00,0x00,
	0x7E,0x7F,0xFF,0x80,
	0x7E,0x7F,0xFF,0xC0,
	0x7E,0x7F,0xFF,0xE0,
	0x7E,0x7F,0xFF,0xE0,
	0xFF,0x7F,0xFF,0xF0,
	0xFF,0x7F,0xFF,0xF0,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFE,
	0xFF,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x00,0x07,0xBE,
	0x7E,0x00,0x07,0xBE,
	0x7E,0x00,0x07,0xBE,
	0x3C,0x00,0x07,0xBE,
	0x3C,0x00,0x07,0xBF,
	0x3C,0x00,0x07,0xBF,
	0x18,0x00,0x07,0xBF,
	0x00,0x00,0x07,0x9F,
	0x00,0x00,0x07,0x9F,
	0x40,0x00,0x07,0x9F,
	0x40,0x00,0x0F,0x9F,
	0x64,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x3E,0x00,0x0F,0x9F,
	0x3E,0x00,0x0F,0x9F,
	0x3E,0x00,0x1F,0x9F,
	0x3E,0x00,0x1F,0x80,
	0x3E,0x00,0x1F,0x80,
	0x3F,0x00,0x1F,0x80,
	0x3F,0x00,0x1F,0x80,
	0x1F,0x00,0x3F,0x80,
	0x1F,0x80,0x3F,0x80,
	0x1F,0x80,0x3F,0x80,
	0x1F,0xC0,0x7F,0x80,
	0x1F,0xE0,0xFF,0x80,
	0x0F,0xF1,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x01,0xFF,0xFF,0x80,
	0x01,0xFF,0xFF,0x80,
	0x00,0xFF,0xEF,0x80,
	0x00,0xFF,0xEF,0x80,
	0x00,0x7F,0xCF,0x80,
	0x00,0x7F,0xCF,0x80,
	0x00,0x3F,0x8F,0x80,
	0x00,0x1F,0x0F,0x80,
	0x00,0x04,0x00,0x00,
	0x00,0x00,0x00,0x00
};

void fj_init(void)
{
    const BOOL bDebug = are_we_in_debugger ();
    
	pImageBuffer = (LPBYTE)nonCachedMalloc (FJ_IMAGE_BUFFER_SIZE);
	printf ("%s(%d): pImageBuffer: %p [%d]\n", __FILE__, __LINE__, pImageBuffer, FJ_IMAGE_BUFFER_SIZE);
	
	//LPFJ_CVT           pCVTrw = (LPFJ_CVT)&fj_CVT;
	LPFJ_GLOBAL        pGlobal = fj_CVT.pFJglobal;

										// invalid pointer until set up
	LPFJSYSTEM         pfsys   = (LPFJSYSTEM)0xfffff000;
	LPFJSYSGROUPMEM    pfsm;
										// invalid pointer until set up
	LPFJPRINTHEAD      pfph    = (LPFJPRINTHEAD)0xfffff000;
	LPFJPRINTHEAD_OP   pfop;
	LPFJPRINTHEAD_PHY  pfphy;
	LPFJPRINTHEAD_PKG  pfpkg;
	devBoardParamsType nvParams;
	struct tm tm;
	//LONG    lRet;
	int     retromNet;
	int     retromFJ;
	int     i;
	int     j;
	STRING  str[80];
	
	customizeReadDevBoardParams (&nvParams);
	
	strobe.m_Ready.m_green 			= STROBESTATE_ON;
	strobe.m_Ready.m_yellow			= STROBESTATE_OFF;
	strobe.m_Ready.m_red 			= STROBESTATE_OFF;
	
	strobe.m_APS.m_green 			= STROBESTATE_OFF;
	strobe.m_APS.m_yellow			= STROBESTATE_OFF;
	strobe.m_APS.m_red 				= STROBESTATE_OFF;
	
	strobe.m_LowTemp.m_green 		= STROBESTATE_OFF;
	strobe.m_LowTemp.m_yellow 		= STROBESTATE_OFF;
	strobe.m_LowTemp.m_red 			= STROBESTATE_ON;

	strobe.m_HighVoltage.m_green 	= STROBESTATE_OFF;
	strobe.m_HighVoltage.m_yellow	= STROBESTATE_OFF;
	strobe.m_HighVoltage.m_red 		= STROBESTATE_ON;

	strobe.m_LowInk.m_green 		= STROBESTATE_OFF;
	strobe.m_LowInk.m_yellow 		= STROBESTATE_ON;
	strobe.m_LowInk.m_red 			= STROBESTATE_OFF;

	strobe.m_OutOfInk.m_green 		= STROBESTATE_OFF;
	strobe.m_OutOfInk.m_yellow 		= STROBESTATE_OFF;
	strobe.m_OutOfInk.m_red 		= STROBESTATE_ON;
	
	strobe.m_Driver.m_green 		= STROBESTATE_OFF;
	strobe.m_Driver.m_yellow 		= STROBESTATE_ON;
	strobe.m_Driver.m_red 			= STROBESTATE_OFF;

	BOOL bInitBRAMprams = FALSE;
	
	WATCHDOG_CCODE code = watchdogStopServicing ();
	printf ("%s(%d): watchdogStopServicing: %d\n", __FILE__, __LINE__, code == WATCHDOG_SUCCESS ? "WATCHDOG_SUCCESS" : "failed");
	
										// just for looking
	NALedGreenOff();					// turn off the green LED
	NALedRedOff();						// turn off the red/yellow LED

	pGlobal->fj_ftp_lRamBuffer = NULL;	// buffer for ftp download into ram
										// set from start of read until end of write into ROM

	fj_bKeepAlive = FALSE;
	fj_bStrokeWatchdog = !bDebug;
	
	pGlobal->fj_ftp_bBufferInUse = FALSE;
	pGlobal->fj_ftp_lRamBufferSize = 0;

	/* TODO
	// init some global values with info from NETsilicon
	// low address of ROM
	pGlobal->pROMLow  = (LPBYTE)((*(NARM_MEM)).cs0.csar.bits.base << 12);
	// size of ROM
	pGlobal->lROMSize = physical_size((*(NARM_MEM)).cs0.csor.bits.mask << 12);
	// high address of ROM
	pGlobal->pROMHigh = pGlobal->pROMLow + pGlobal->lROMSize - 1;
	// low address of RAM
	pGlobal->pRAMLow  = (LPBYTE)((*(NARM_MEM)).cs1.csar.bits.base << 12);
	// size of RAM
	pGlobal->lRAMSize = physical_size((*(NARM_MEM)).cs1.csor.bits.mask << 12);
	// high address of RAM
	pGlobal->pRAMHigh = pGlobal->pRAMLow + pGlobal->lRAMSize - 1;
	*/
	
	pGlobal->bPrint = FALSE;			// print the current message
	pGlobal->bPrintTestLoop = FALSE;	// buld the current message, but don't print it
	pGlobal->bSendDataReady = FALSE;	// send a DataReady status message
	pGlobal->bSendPhotocell = FALSE;	// send a Photocell status message
										// send a Photocell Detect message
	pGlobal->bSendPhotocellDetect = FALSE;
	pGlobal->bBuildFailure  = FALSE;	// set when a message can not be built between the photocell and the print head
	pGlobal->bBufferFailure = FALSE;	// set when a new ENI FIFO buffer can not be filled before the active one is finished
	pGlobal->bDataReady = FALSE;		// TRUE when data queued for printing
	pGlobal->bPhotocell = FALSE;		// TRUE when photocell detected
	pGlobal->bPhotocellInt = FALSE;		// TRUE when photocell interrupt wanted from GA. set when ENI is initialized.
										// enabled/disabled/test - test will print but not change date/time/counters
	pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
	pGlobal->bStatusReport = TRUE;
	pGlobal->bInitComplete = FALSE;		// FALSE until end of this routine
	pGlobal->bReset = FALSE;			// TRUE = reset the chip. stops the WatchDog strobe to cause a chip reset.
	pGlobal->bGAUpgrade = FALSE;		// TRUE = GA code has been upgrade. Need to notify everybody.
	pGlobal->bReady = FALSE;			// FALSE until everything ready
	pGlobal->bADReady = FALSE;			// FALSE until AD average values computed
	pGlobal->bPrintHeadPresent = FALSE;	// FALSE until print head detected
	pGlobal->bBatteryOK = FALSE;		// FALSE until battery reported OK by RTC chip
	pGlobal->bClockOK = FALSE;			// FALSE unless clock OK at poewr on - or clock set after poewr on
										// FALSE until shaft encoder is generating pulses
	pGlobal->bShaftEncoderRunning = FALSE;
	pGlobal->bHighVoltageOn = FALSE;	// FALSE until we want the high voltage on
	pGlobal->bStandByOn = FALSE;		// FALSE until user wants the StandBy mode on
	pGlobal->lHighVoltageWanted = 0;	// 0 until head detected
	pGlobal->lHighVoltageActual = 0;	// 0 until head detected
	pGlobal->lPWMHV = 0;				// 0 until head detected
	pGlobal->lBoardTemp = 0;			// Board temperature
	pGlobal->tmNewManu.tm_year = 0;		// new manufacture date
	pGlobal->tmNewInservice.tm_year = 0;// new inservice date
	pGlobal->tmGA.tm_year = 0;			// FPGA program date
										// save first memory that we can use
	//pGlobal->pRAMUnused = &__ghsend_free_mem;
	//pGlobal->pRAMUnused += 0x40000;		// NetSilicon uses more than they say
										// internal chip frequency for GEN and SER modules
	pGlobal->lXtalFreq = NAgetXtalFreq();
										// system clock value
	pGlobal->lCPUClockFreq = NAgetSysClkFreq();
	// gate array clock is divided by scale factor
	pGlobal->lGAClockFreq1 = pGlobal->lCPUClockFreq / GA_CLOCK1_SCALE;

	/* TODO
	// the loader sets location 0x3ffc to a zero
	// it is incremented after this test
	// assuming that the ROM version of this app ran this far before the debugger took control,
	// a non-zero means that this app was loaded by the debugger
	pGlobal->bDebug = FALSE;			// TRUE = debug in RAM. FALSE = ROM.
	if ( 0 != *pwBootFlag )
	{
		pGlobal->bDebug = TRUE;
	}
	(*pwBootFlag)++;					// change it for next debug execution
	*/
	
	// do not use watchdog unless 50-1 bug is fixed
	//	if ( NETA50_1 >= (*(NARM_GEN)).ssr.bits.rev ) fj_watchDog = FALSE;
	// do not use watchdog if in debug mode
	if ( TRUE == pGlobal->bDebug ) fj_watchDog = FALSE;

	
	for ( i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++ )
	{
		memset (pGlobal->DynamicSegments [i].strDynData, 0, ARRAYSIZE (pGlobal->DynamicSegments [i].strDynData));
		pGlobal->DynamicSegments [i].bChanged = FALSE;
	}
	
	
	// create global semaphores
	tx_semaphore_create( &(pGlobal->semaROMLock), "FJ ROM access lock", 1 );
	tx_semaphore_create( &(pGlobal->semaEditLock), "FJ Edit lock", 1 );
	tx_semaphore_create( &(pGlobal->semaPrintChainLock), "FJ Print Chain lock", 1 );
	tx_mutex_create(&gMutexFPGA, "FJ FPGA mutex", TX_INHERIT);
    tx_semaphore_create (&semaSerial, "serial_semaphore", 1);

/*	TODO
	if (fj_bStrokewatch) {
		//watchdogServiceOnce ();
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
*/
	
/* TODO
	if (fj_bStrokeWatchdog) {
		//watchdogServiceOnce ();
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
*/

	
	int iOldPriority = 0;
	tx_thread_priority_change(tx_thread_identify(), FJ_THREAD_LOW_PRIORITY, &iOldPriority);

	{
		fj_CVT.pfph->lBaudRate = nvParams.baudrate;
	
	    int ccode = tx_thread_create (&thrSerial, "Serial Thread", SerialFct, tx_thread_identify (), &cSerialStack, ARRAYSIZE (cSerialStack), 
	    	FJ_THREAD_MEDIUM_PRIORITY, FJ_THREAD_MEDIUM_PRIORITY, 
	    	//TX_NO_TIME_SLICE, TX_AUTO_START);
	    	1, TX_AUTO_START);
	    
	    ULONG lTime = tx_time_get ();
	    	
	    while (!bSerialFct) {
	    	tx_thread_sleep (1);
	    	
			if ((tx_time_get () - lTime) > NS_MILLISECONDS_TO_TICKS (5000)) {
				printf ("%s(%d): failed to start serial thread\n", __FILE__, __LINE__);
				break;
			}
	    }
	}
	
	// set lock to avoid error message
	tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	// the param routines need the ROM address that was just set into the GLOBAL area
	// read NETsilicon parameters
	// NOTE: this uses a modified NetSilicon routine to get parameters possibly changed by the Dialog()
	//retromNet = NAReadDevBoardParams( &nvParams );
	// read FoxJet NV parameters
	fj_setDefaultFJRomParams (fj_CVT.pFJparam);
	retromFJ = fj_ReadFJRomParams( fj_CVT.pFJparam );
	tx_semaphore_put( &(pGlobal->semaROMLock) );

	// one of the two above routines destroys optimized register settings.
	// reload some pointers
	pGlobal = fj_CVT.pFJglobal;			// set local pointer
	
	{
		NaIamIfAddrInfo_t addrInfo;
		
		NaIamStatus_t Status = customizeIamGetIfAddrInfo (BP_ETH_INTERFACE, &addrInfo);
		
		pGlobal->ipaddress 		= addrInfo.v4Info.IPV4_ADDR(ipAddress);
		pGlobal->ipsubnet       = addrInfo.v4Info.subnetMask;
		pGlobal->ipdefgateway   = addrInfo.v4Info.IPV4_ADDR(defaultGateway);
		
		printf ("%s(%d): ip address:      %d.%d.%d.%d\n", __FILE__, __LINE__, PUSH_IPADDR(pGlobal->ipaddress));
		printf ("%s(%d): subnet mask:     %d.%d.%d.%d\n", __FILE__, __LINE__, PUSH_IPADDR(pGlobal->ipsubnet));
		printf ("%s(%d): default gateway: %d.%d.%d.%d\n", __FILE__, __LINE__, PUSH_IPADDR(pGlobal->ipdefgateway));
	}

	/*
	if ( 0 == nvParams.useDhcp )
	{
										// get IP address
		pGlobal->ipaddress          = nvParams.ipAddress;
										// get subnet mask
		pGlobal->ipsubnet           = nvParams.subnetMask;
										// get default gateway
		pGlobal->ipdefgateway       = nvParams.gateway;
	}
	else
	{
										// get IP address
		pGlobal->ipaddress          = ldhcpAddr;
										// get subnet mask
		pGlobal->ipsubnet           = ldhcpMask;
										// get default gateway
		pGlobal->ipdefgateway       = ldhcpGateway;
	}
	*/
										// get IP address for loopback
	pGlobal->iploopback         = 0x7f000001;
										// NETsilicon bug
	pGlobal->iploopback         = pGlobal->ipaddress;
										// get subnet broadcast address
	pGlobal->ipbroadcastsubnet  = 0xffffffff;
	// get net broadcast address
	pGlobal->ipbroadcastnet     = pGlobal->ipaddress | (~pGlobal->ipsubnet);

	/*
	// save MAC address
	NASerialnum_to_mac( nvParams.serialNumber, (char*)pGlobal->macaddr );
	NAInitEthAddress ( (char*)pGlobal->macaddr );
	*/	

	// copy FJSYSTEM from parameters into RAM
	pfsys = fj_CVT.pfsys;
	memcpy( pfsys, &(fj_CVT.pFJparam->FJSys ), sizeof(FJSYSTEM) );
	//  set manufacturing password
	strcpy( pfsys->aPassword[FJSYS_PRIORITY_MANUFACTURING], "71043bob68" );

	// copy FJPRINTHEAD from parameters into RAM
	pfph  = fj_CVT.pfph;
	pfop  = pfph->pfop;
	pfphy = pfph->pfphy;
	pfpkg = pfph->pfpkg;
	memcpy( pfop,  &(fj_CVT.pFJparam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
	memcpy( pfphy, &(fj_CVT.pFJparam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
	memcpy( pfpkg, &(fj_CVT.pFJparam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );
	memcpy( pfph,  &(fj_CVT.pFJparam->FJPh   ), sizeof(FJPRINTHEAD    ) );
	pfph->pfop  = pfop;
	pfph->pfphy = pfphy;
	pfph->pfpkg = pfpkg;

	// link the FJPRINTHEAD to the FJSYSTEM
	pfph->pfsys = pfsys;

	pfph->lIPaddr = pGlobal->ipaddress; //nvParams.ipAddress;	// get IP address

	fj_PrintHeadReset( pfph );

	// this must be done here in case the serial number was changed by the boot dialog.
	// convert serial number
	fj_NETtoFJSerial( nvParams.serialNumber, pfph->strSerial );
	pfph->lSerial = atoi(pfph->strSerial);
	fj_setID();

	pfph->bEdit = FALSE;

	// add print head to group
	// temp until we add a group to permanent storage
	pfsys->lActiveCount = 0;
	pfsm  = &(pfsys->gmMembers[pfsys->lActiveCount]);
	pfsys->lActiveCount++;
	pfsm->pfph = pfph;
	strcpy( pfsm->strID, pfph->strID );
	pfsm->lIPaddr = pfph->lIPaddr;
	
	strcpy (pfsys->strID, pfph->strID);

	for (i = 0; i < min (pfsys->lActiveCount, FJSYS_NUMBER_PRINTERS); i++)
		strncpy (pfsys->gmMembers[i].strID, pfsys->strID, FJPH_ID_SIZE);
	
	// need to set group number here. default is zero - correct for one head system.

	pfph->lEditID      = 0;				// IP address of connection with lock

	// pfsys and pfph are now set up with info from the system memstor in ROM

	// get the time once
	fj_SystemGetTime( &tm );
	fj_SystemGetTime( &(pfsys->tmPhotoCell) );

/*
	if (fj_bStrokeWatchdog) {
		//watchdogServiceOnce ();
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
*/
	
	strcpy(str,"FoxJet started. V");
	strcat(str,fj_getVersionNumber());
	strcat(str," ");
	strcat(str,fj_getVersionDate());
	strcat(str,"\n");
	fj_writeInfo(str);

	/*
	fj_writeInfo("\nActual IP values being used - \n");
	sprintf( str, "IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipaddress));
	fj_writeInfo(str);
	sprintf( str, "Subnet mask = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipsubnet));
	fj_writeInfo(str);
	sprintf( str, "Def Gateway = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipdefgateway));
	fj_writeInfo(str);
	sprintf( str, "Network broadcast IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipbroadcastnet));
	fj_writeInfo(str);
	sprintf( str, "Sub Network broadcast IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipbroadcastsubnet));
	fj_writeInfo(str);
	*/
	
	/* TODO
	fj_writeInfo("\nLast Reset - ");
	if ( TRUE == pGlobal->bDebug ) fj_writeInfo("Debug\n");
	else
	{
		if ( 0 != (*NCC_GEN).ssr.bits.ext ) fj_writeInfo("External Reset\n");
		if ( 0 != (*NCC_GEN).ssr.bits.pow ) fj_writeInfo("Power On\n");
		if ( 0 != (*NCC_GEN).ssr.bits.sw  ) fj_writeInfo("Watch-Dog Timer\n");
	}

	for ( i = BOOT_NUMBER-1; i > 0; i-- )
	{
		memcpy( &(pBRAM->aBoots[i]), &(pBRAM->aBoots[i-1]), sizeof(BOOTINFO) );
	}
	memcpy( &(pBRAM->aBoots[0].tmBoot), &(tm), sizeof(struct tm) );
	pBRAM->aBoots[0].bBatt = pGlobal->bBatteryOK;
	pBRAM->aBoots[0].bClock = ( 100 < tm.tm_year );
	if ( TRUE == pGlobal->bDebug ) pBRAM->aBoots[0].lReason = BOOT_REASON_DEBUG;
	else
	{
		if ( 0 != (*NCC_GEN).ssr.bits.ext ) pBRAM->aBoots[0].lReason = BOOT_REASON_EXTERAL;
		if ( 0 != (*NCC_GEN).ssr.bits.sw  ) pBRAM->aBoots[0].lReason = BOOT_REASON_WATCHDOG;
	}
	*/
	
	/* TODO
	i = mii_check_speed();
	fj_writeInfo("\nEthernet speed is ");
	if      ( 0 == i ) fj_writeInfo("10Mbit\n");
	else if ( 1 == i ) fj_writeInfo("100Mbit\n");
	else               fj_writeInfo("unknown\n");
	i = mii_check_duplex();
	fj_writeInfo("Ethernet mode is ");
	if      ( 0 == i ) fj_writeInfo("half duplex\n");
	else if ( 1 == i ) fj_writeInfo("full duplex\n");
	else               fj_writeInfo("unknown\n");
	*/
	
	/* TODO
	// the following code is copied from NetSilicon
	// ask chip to do something
	//(*NCC_EFE).miiar.bits.madr  = 0x401;
	phy_addr = 0x100;					// assume address 1 change if different on your board 
	narm_write_reg (NARM_EMAR_REG, NARM_EMAR_ADDR, madr, phy_addr + 0x001);
	//(*NCC_EFE).miicr.bits.rstat = 1;
	narm_write_reg (NARM_EMIC_REG, NARM_EMIC_ADDR, rstat, 1);
	// wait for the chip to do it
	mii_poll_busy();
	// read the result
	fj_writeInfo("Ethernet link is ");
	if ((narm_read_reg (NARM_EMRD_REG, NARM_EMRD_ADDR, mrdd) & 0x0004) == 0)
		fj_writeInfo("DOWN\n");
	//	if (((*NCC_EFE).miirdr.bits.mrdd & 0x0004) == 0) fj_writeInfo("DOWN\n");
	else fj_writeInfo("UP\n");
	*/
	
	fj_romInit();						// create queue and thread for writing ROM
	
	// initialize everything
	fj_socketInit();
	// TODO //fj_timeInit();
	// TODO //fj_print_head_InitEmail();
	// TODO //fj_print_head_Init();
	// TODO //fj_nameInit();

	// TODO //fj_print_head_ScheduleStandBy();

	pGlobal->bInitComplete = TRUE;		// FALSE until end of this routine
	fj_writeInfo("Init complete\n");
	fj_writeInfo("\n");

	//	fj_print_head_SendEmailService( sMailMsg, "Service: up and running" );

	char szFPGA [FJSYS_MAX_FILENAME];
	
	mkdir (FJ_DIR_FONTS, 	O_RDWR);
	mkdir (FJ_DIR_LABELS, 	O_RDWR);
	mkdir (FJ_DIR_BITMAPS, 	O_RDWR);
	mkdir (FJ_DIR_PARAMS, 	O_RDWR);
	mkdir (FJ_DIR_FIRMWARE, O_RDWR);
	
    SetMphcBaseAddress ((unsigned short *)0x40000000);

	sprintf (szFPGA, "%s/%s", FJ_DIR_FIRMWARE, "ga.bin");
	unsigned long lLen = ReadFileSize (szFPGA);
	
	if (lLen > 0) {
		char * psz = fj_malloc (lLen);
		
		memset (psz, 0, lLen);
		
		if (ReadFile (&lLen, psz, szFPGA)) {
			tagSetupRegs tSetup;
			tagFireVibPulse tPulseRegs;
			tagInterruptCtrlReg t;

			//printf ("%s(%d): %s [%d bytes]\n", __FILE__, __LINE__, szFile, lLen);
	    	fj_ProgramFPGA (psz, lLen);
	    	TRACEF ("FPGA loaded");

			memset (&t, 0, sizeof (t));
			memset (&tSetup, 0, sizeof (tSetup));
			memset (&tPulseRegs, 0, sizeof (tPulseRegs));
	
			ToRegisters (&tSetup, &tPulseRegs);

			t._Channel 						= 2;
			t._Mode 						= 1;
			t._ErrDrv 						= 0x01;
			tSetup._CtrlReg._PrintEnable 	= 0;	

			WriteRegister (0x00, &tSetup._CtrlReg, sizeof (tSetup._CtrlReg));
			//WriteRegister (INTR_CONTROL_REG, &t, sizeof (t));
		}
		
		fj_free (psz);
	}

	if (!gbFPGA) {
		char sz [512];
		
		sprintf (sz, "Failed to load: %s", szFPGA);
		TRACEF (sz);
	}
	else {
		char strLabel [FJLABEL_NAME_SIZE + 1] = { 0 };
		char sz [256] = { 0 };

		fj_scratchpadReadLabelName (strLabel, FJLABEL_NAME_SIZE);

		if (fj_IsValidLabelName (strLabel) && FileExists (FJ_DIR_LABELS, strLabel)) {
			long lCount = fj_scratchpadReadCount ();
			double dInk = fj_scratchpadReadInkUsage ();
			BOOL bPrintIdle = fj_scratchpadReadPrintState ();

			sprintf (sz, "ink usage:   %.09f", dInk);		TRACEF (sz);
			sprintf (sz, "build label: %s", strLabel);		TRACEF (sz);
			sprintf (sz, "idle:        %d", bPrintIdle);	TRACEF (sz);
			fj_SystemBuildSelectedLabel (pfph->pfsys, strLabel);
			
			if (pfph->pfmSelected) {
				sprintf (sz, "lCount:      %d", lCount);	TRACEF (sz);
				fj_SetCounts (pfph->pfmSelected, lCount);
			}
			
			if (bPrintIdle)
				fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
		}
		else {
			pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
			memset (strLabel, 0, ARRAYSIZE (strLabel));
			fj_scratchpadWriteLabelName (strLabel);
			fj_scratchpadWriteCount (0);
			fj_scratchpadWritePrintState (TRUE);
			//fj_scratchpadWriteInkUsage (fj_PrintHeadGetInkLowVolume (fj_CVT.pfph));
			fj_ProgramHeadConfig (1);
			SetProductLen (1);
			SetPrintDelay (1);
		}
	}

	TRACEF (fj_getVersionNumber ());
	
	if (pfph->bSaveDynData)
		fj_ReadDynData ();
		
	if (pfph->bSaveSerialData)
		fj_ReadSerialData ();
		
	fj_run();
}
