
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <tx_api.h>
#include <Npttypes.h>
#include <sysAccess.h>

#include "fs.h"
#include "rss.h"
#include "fs_api.h"
#include "fileinit.h"
#include "bsp_api.h"


int FileSystem_InitVolume(void)
{
	// We require the CLib support for the file system
    if (NABspFilesystemForCLib == 0)
    {
        printf("CLib file system not enabled!\n");
    	return NAFS_VOLUME_NOT_INITIALIZED;
    }
    
    // Create the directory that will hold configuration files.
    //TODO: how do we set authorities to this directory.
    mkdir(FS_FLASH_VOLUME_CONFIG_DIR, O_RDWR);
    
    return NAFS_SUCCESS;
}
