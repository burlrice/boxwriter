
#ifndef _FILEINIT_H
#define _FILEINIT_H

#ifdef __cplusplus
extern "C"
{
#endif


/* Enable printf */
#define DEBUG_PRINTF   0

#define FS_FLASH_VOLUME				"FLASH0"
#define FS_FLASH_VOLUME_ROOT_DIR	FS_FLASH_VOLUME "/"
#define FS_FLASH_VOLUME_CONFIG_DIR	FS_FLASH_VOLUME_ROOT_DIR ".cfg"

/* these bit offsets are used to calculate access bit (in sysAccess.h) 
 * from a group-id (group_id << READ_BIT_OFFSET)
 */
#define READ_BIT_OFFSET    		2 
#define WRITE_BIT_OFFSET   		16

#define FILE_TYPE_FILE          1    
#define FILE_TYPE_DIR         	2    
#define IS_FILE_TYPE(a)        ((a) == FILE_TYPE_FILE)   
#define IS_DIRECTORY_TYPE(a)   ((a) == FILE_TYPE_DIR)   


int FileSystem_InitVolume(void);

#ifdef __cplusplus
}
#endif

#endif /*  _FILEINIT_H */
