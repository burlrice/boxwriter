
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <Npttypes.h>
#include <fservapi.h>
#include <fclntapi.h>
#include <fs_api.h>
#include <fs.h>
#include <errno.h>
#include <sockapi.h>

#include <ftpsvrfs.h>
#include <naflash.h>
#include <Flash.h>
#include <bsp_api.h>
#include <reg_def.h>
#ifdef BSP_ARM9
    #include <cs.h>
    #include <gpiomux_def.h>
#endif

#include <fileinit.h>
#include <firmware.h>


int FTPServer_init()
{
    int status = -1;

    /* initialize ftp server with number of users */
    status = FSInitialize(5);
    if (status != NAFTPS_SUCCESS)
    {
        printf("FTP server: FSInitialize failed [%d]\n", status);
        goto _ret;
    }
 
    /* install file system for FTP server */
    status = FSInstallFileSystem(FS_FLASH_VOLUME_ROOT_DIR);
    if (status != NAFTPS_SUCCESS)
    {
        printf("FTP server: FSInstallFileSystem failed [%d]\n", status);
        goto _ret;
    }
    
    /* start ftp server */
    status = FSStartServer();
    if (status != NAFTPS_SUCCESS)
    {
        printf("FTP server: FSStartServer failed [%d]\n", status);
        goto _ret;
    }
    
    printf ("FTP server started.\n");

_ret:
    return status;
}
