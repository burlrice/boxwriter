#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tx_api.h>
#include "utils.h"
#include "nsuptime.h"


static unsigned long last_ticks = 0;
static unsigned long ticks_overflow = 0;
static unsigned long uptime_secs = 0;
static int using_timer = 0;
static int initialized = 0;
static TX_TIMER uptime_update_timer;
static char uptime_thread_stack[4*1024];
static TX_THREAD  uptime_thread;


static unsigned long ns_seconds_from_ticks(void);


/*********************************************************************
 * ns_get_uptime_sec()
 *
 * This function is used to obtain the system uptime in seconds.
 *********************************************************************/
unsigned long ns_get_uptime_sec(void)
{
    if (!using_timer)
    {
        uptime_secs = ns_seconds_from_ticks();
    }
    return uptime_secs;
}


static unsigned long ns_seconds_from_ticks(void)
{
    unsigned long ticks;
    unsigned long secs;
 
    ticks = tx_time_get();
    if (ticks < last_ticks)
    {
        ++ticks_overflow; /* rollover of ticks counter */
    }
    last_ticks = ticks;
    if (ticks_overflow == 0)
    {
        secs = NS_TICKS_TO_MILLISECONDS(ticks) / 1000UL;
    }
    else
    {
		secs = (unsigned long) ((ticks_overflow *
						(NS_TICKS_TO_MILLISECONDS(0xFFFFFFFF) / 1000UL)) +
						       	(NS_TICKS_TO_MILLISECONDS(ticks) / 1000UL));
    }
    return secs;
}

static void ns_system_uptime_update_alarm(unsigned long unused_param)
{
    (void)unused_param;

    ++uptime_secs; /* count one more second */
}

static void ns_system_uptime_thread(unsigned long unused_param)
{
    unsigned int tx_retval;

    (void)unused_param;

    memset(&uptime_update_timer, 0, sizeof(TX_TIMER));
    tx_retval = tx_timer_create(&uptime_update_timer,
                    "System Uptime Update Timer",
                    ns_system_uptime_update_alarm,
                    0, /* dummy parameter */
                    NS_MILLISECONDS_TO_TICKS(1000),
                    NS_MILLISECONDS_TO_TICKS(1000),
                    TX_NO_ACTIVATE);
    if (tx_retval == TX_SUCCESS)
    {
        using_timer = 1;
    }

    uptime_secs = ns_seconds_from_ticks();
    if (using_timer)
    {
        tx_timer_activate(&uptime_update_timer); 
    }
    while (1)
    {
        /*
         * Every so often, synchronize the uptime value being maintained by
         * the timer, with the system's tick counter. In the event that there
         * is drift, this should keep the two within a second of one another.
         * 
         * If we are not using the timer, the implementation of our API
         * ns_get_uptime_sec() computes the uptime on every call. By calling
         * ns_seconds_from_ticks() periodically from this loop, we update the
         * tick rollover counter, which is necessary for "long" uptimes.
         *
         * Note: I do not believe that playing with the timer is necessary
         * in this loop. Once it is activated, just let it run.
         */
        tx_thread_sleep(NS_MILLISECONDS_TO_TICKS(60*1000UL+5));
//        tx_timer_deactivate(&uptime_update_timer);
        uptime_secs = ns_seconds_from_ticks();
//        tx_timer_change(&uptime_update_timer,
//                NS_MILLISECONDS_TO_TICKS(1000),
//                NS_MILLISECONDS_TO_TICKS(1000));
//        tx_timer_activate(&uptime_update_timer);
    }
}

int ns_start_uptime_thread(void)
{
    unsigned int tx_retval;

    if (initialized)
    {
        return 0;
    }

    /* Create the system uptime update thread. */
    tx_retval = tx_thread_create(&uptime_thread,
                    "System Uptime Thread",
                    ns_system_uptime_thread,
                    0, /* dummy parameter */
                    (VOID *) uptime_thread_stack,
                    sizeof(uptime_thread_stack),
                    15,
                    15,
                    NS_MILLISECONDS_TO_TICKS(10),
                    TX_AUTO_START);
    if (tx_retval != TX_SUCCESS)
    {
    	printf("Error creating system uptime thread (%d)\n", tx_retval);
        return tx_retval;
    }

    initialized = 1;
    return 0;
}

int UpTime_Init(void)
{
    return ns_start_uptime_thread();
}
