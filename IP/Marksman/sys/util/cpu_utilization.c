#include <stdio.h>
#include <stdlib.h>
#include "tx_api.h"
#include "bsp_api.h"
#include "cpu_utilization.h"


#if CPU_LOAD_MEASURING

/* 1ms BSP timer tick  */
//#define CPU_LOAD_MEASUREMENT_INTERVAL			1277
#define CPU_LOAD_MEASUREMENT_INTERVAL			500

/* Flag indicating timer has fired */
static volatile UINT TimerFlag = 0;

/* Percent of cpu load */
static volatile int cpuLoad = 0;


static char appIdleThreadStack[0x1000];
static TX_THREAD  sIdleThread;


/*
 * Get the current CPU load (percentage in use).
 *
 * @return  The percentage of the CPU currently in use.
 */
int getCpuLoad()
{
	return cpuLoad;
}


/*
 * timer function
 */
static void IdleMeasure(ULONG Param)
{
    if (!TimerFlag)
    {
        TimerFlag = 1;
    }
    else
    {
    	/* Never got through an idle loop. Must be at 100% load. */
    	cpuLoad = 100; 
        //printf("CPU Load: 100%% *\n");
    }
}


/*
 * idle thread, used for CPU load measuring
 */
static void IdleThread(ULONG thread_input)
{
    TX_TIMER        IdleTimer;
    unsigned int    retVal;
    unsigned int    loopCtr = 0;
    unsigned int    firstCount = 0;

    TimerFlag = 0;

	memset(&IdleTimer, 0, sizeof IdleTimer);

    /* create timer */
    retVal = tx_timer_create(
    				&IdleTimer,                    /* pointer to timer */
                    "CPU Util Idle Timer",         /* name             */
                    IdleMeasure,                   /* expiration funct */
                    0,                             /* expiration input */
                    CPU_LOAD_MEASUREMENT_INTERVAL, /* initial ticks    */
                    CPU_LOAD_MEASUREMENT_INTERVAL, /* reschedule ticks */
                    TX_AUTO_ACTIVATE);             /* activate now     */

    if (retVal != TX_SUCCESS)
    {
        printf("tx_timer_create returned error: 0x%04x", retVal);
    }

    while (1)
    {
        if (TimerFlag)
        {  /* a new measurement must be captured now */
            if (firstCount == 0)
            {
                //printf("Reference Counts: %d\n\n", loopCtr);
                firstCount = loopCtr;
            }
            else
            {
                cpuLoad = 100 - ((float)(100.0*loopCtr) / (float)firstCount);
                //printf("Count:%10d ", loopCtr);
                //printf("CPU Load: %2d%%\n", cpuLoad);

                TimerFlag = 0;
                loopCtr = 0;
            }
        }

        loopCtr++;
    }
}


int CPUUtilization_Init(void)
{
	int rc = TX_SUCCESS;
	
	/*
	 * print message and sleep a little bit so that print task is over when we do the
	 * reference measuring begins
	*/
	printf("Initializing CPU utilization monitor. This takes approximately 10 seconds...\n");
	tx_thread_sleep(CPU_LOAD_MEASUREMENT_INTERVAL);
	
	/* first create the idle thread */
	rc = tx_thread_create(
						&sIdleThread,                /* pointer to thread              */
						"CPU Util Idle Thread",      /* name                           */
						IdleThread, 0,               /* entry function and input       */
						(VOID *) appIdleThreadStack, /* pointer to thread stack        */
						sizeof(appIdleThreadStack),  /* stack size                     */
						31, 31,                      /* priority and preempt threshold */
						1, TX_AUTO_START);           /* time slice and auto start      */
	if (rc == TX_SUCCESS)
	{
		/* sleep a while to let the Idle thread determine the reference count value */
		tx_thread_sleep(10*NABspTicksPerSecond);
		printf("CPU utilization monitor initialized.\n");
	}
	else
	{
		printf("failed to create CPU utilization monitor! (%d)\n", rc);
	}
	
	return rc;
}

#endif /* CPU_LOAD_MEASURING */
