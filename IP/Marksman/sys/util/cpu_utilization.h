#ifndef _CPU_UTILIZATION_H
#define _CPU_UTILIZATION_H

#ifdef __cplusplus
extern "C"
{
#endif


#include "appservices.h"

#if CPU_LOAD_MEASURING

int getCpuLoad();
int CPUUtilization_Init(void);

#endif /* CPU_LOAD_MEASURING */


#ifdef __cplusplus
}
#endif

#endif /* _CPU_UTILIZATION_H */
