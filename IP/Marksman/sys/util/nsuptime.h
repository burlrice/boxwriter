#ifndef _NSUPTIME_H
#define _NSUPTIME_H

#ifdef __cplusplus
extern "C"
{
#endif


/*
 * ns_get_uptime_sec()
 *
 * This function is used to obtain the system uptime in seconds.
 */
unsigned long ns_get_uptime_sec(void);

int UpTime_Init(void);


#ifdef __cplusplus
}
#endif

#endif /* _NSUPTIME_H */
