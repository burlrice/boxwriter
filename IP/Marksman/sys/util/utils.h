#ifndef _UTILS_H
#define _UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif


#include <bsp.h>
#include "bsp_api.h"

/******************************************************************
 * milliseconds to ticks macro
 ******************************************************************/
#ifndef BSP_TICKS_PER_SECOND
#error "BSP_TICKS_PER_SECOND NOT DEFINED"
#endif



/******************************************************************
 * time comparison macros
 ******************************************************************/

/* These macros are used to correctly (i.e. handle wrap around) compare 
 * two 32bit counters. */

/* true if time1 >= time2 */
#define NS_TIME_GREATER_OR_EQUAL(t1, t2) (((t1) - (t2)) <= 0x7fffffff)

/* true if time1 > time2 */
#define NS_TIME_GREATER(t1, t2) (((t2) - (t1)) > 0x7fffffff)

/* true if time1 <= time2 */
#define NS_TIME_LESS_OR_EQUAL(t1, t2) (((t2) - (t1)) <= 0x7fffffff) 

/* true if time1 < time2 */
#define NS_TIME_LESS(t1, t2) (((t1) - (t2)) > 0x7fffffff)



/******************************************************************
 * bitmap manipulation macros
 *
 * These macros are used for manipulating bitmaps that are comprised
 * of array of 32bit integers.  Note the bitmap_size is the size of
 * the bitmap array and not the size of the bitmap itself.
 *******************************************************************/

/* macro for converting the number of bits in a bitmap to the bitmap 
 * array  size */
#define NS_BITMAP_SIZE(bitcount) (((bitcount) % 32) ? (((bitcount) / 32) + 1) : ((bitcount) / 32))

/* used to set a bit in the bitmap */
#define NS_SET_BITMAP(bitmap, index) \
{ \
    (bitmap)[(index)/32] |= (1 << ((index) & 0x1f)); \
}

/* used to clear a bit in the bitmap */
#define NS_UNSET_BITMAP(bitmap, index) \
{ \
    (bitmap)[(index)/32] &= ~(1 << ((index) & 0x1f)); \
}

/* used to test a bit in the bitmap */
#define NS_ISSET_BITMAP(bitmap, index) \
    ((bitmap)[(index)/32] & (1 << ((index) & 0x1f)))

/* used to clear an entire bitmap */
#define NS_CLEAR_BITMAP(bitmap, bitmap_size) \
{ \
    unsigned int bitmap_index; \
    for (bitmap_index=0; bitmap_index < (bitmap_size); bitmap_index++) \
    { \
	(bitmap)[bitmap_index] = 0; \
    } \
}

/* used to copy an entire bitmap */
#define NS_COPY_BITMAP(bitmap_destination, bitmap_source, bitmap_size) \
{ \
    unsigned int bitmap_index; \
    for (bitmap_index=0; bitmap_index < (bitmap_size); bitmap_index++) \
    { \
	(bitmap_destination)[bitmap_index] = (bitmap_source)[bitmap_index]; \
    } \
}

/* used to set an entire bitmap */
#define NS_SETALL_BITMAP(bitmap, bitmap_size) \
{ \
    unsigned int bitmap_index; \
    for (bitmap_index=0; bitmap_index < (bitmap_size); bitmap_index++) \
    { \
	(bitmap)[bitmap_index] = 0xffffffff; \
    } \
}


#ifdef __cplusplus
}
#endif

#endif /* _UTILS_H */
