#ifndef FIRMWARE_H_
#define FIRMWARE_H_

#define FW_IMAGE_TYPE_NONE          0
#define FW_IMAGE_TYPE_FIRMWARE      1
#define FW_IMAGE_TYPE_ROM           2

#define FW_SUCCESS           0
#define FW_GENERROR          -1
#define FW_INIT_ERROR        -2
#define FW_INVALID_HANDLE    -3
#define FW_FLASH_WRITE_ERROR -4

int fw_open_flash_image_file(char *file, unsigned long handle);
int fw_write_flash_image_file(char *bufferp, unsigned long buflen, unsigned long handle);
int fw_close_flash_image_file(unsigned long handle);


#endif /*FIRMWARE_H_*/
