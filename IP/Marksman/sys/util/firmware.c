#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <Npttypes.h>
#include <naflash.h>
#include <Flash.h>
#include <bsp_api.h>
#include <reg_def.h>
#include <cs.h>
#ifdef BSP_ARM9
    #include <gpiomux_def.h>
#endif
#include "firmware.h"


#define FW_FLASH_IMAGE_BUFFER_SIZE    128 * 1024

typedef struct {
	unsigned long	fi_handle;
	int				successful;
	int				currentLength;
	unsigned int	currentSector;
	unsigned int	writeIndex;
	int				sectors;
	unsigned long	*sectorSizes;
	unsigned int	romsize;
	unsigned int	imageType;
	char sectorDatap[FW_FLASH_IMAGE_BUFFER_SIZE];
} FW_FLASH_IMAGE_T;    

//TODO: semaphore protect access to this
static FW_FLASH_IMAGE_T *gFWFlashImage = NULL;



static int fw_flash_image_init()
{
	int rc = FW_SUCCESS;
	
	gFWFlashImage = (FW_FLASH_IMAGE_T *)malloc(sizeof(FW_FLASH_IMAGE_T));
	if (gFWFlashImage == NULL)
	{
		return FW_INIT_ERROR;
	}
	memset(gFWFlashImage, 0, sizeof(FW_FLASH_IMAGE_T));
	
#ifdef BSP_ARM9
	gFWFlashImage->romsize = customizeGetCSSize ((WORD32 *)NA_SCM_CS1_BASE_ADDR, (WORD32 *)NA_SCM_CS1_BASE_ADDR);
#else
	gFWFlashImage->romsize = customizeGetCSSize ((WORD32 *)NARM_CSAR0_ADDR, (WORD32 *)NARM_CSOR0_ADDR);
#endif

    /* determine the flash physical sectors and sector sizes */
    gFWFlashImage->sectors = NAFlashSectors();
    gFWFlashImage->sectorSizes =
    	(unsigned long *)malloc(sizeof(unsigned long) * gFWFlashImage->sectors);
    rc = NAFlashSectorSizes(gFWFlashImage->sectorSizes);
    if (rc)
    {
       printf ("Unable to get Flash sector size\nCheck if flash is enabled\n");
       rc = FW_INIT_ERROR;
    }
    
    return rc;
}

static unsigned int fw_get_image_type(char *file)
{
   	//TODO: can we determine the image type from header information in the file?
   	if (strcmp (file, "image.bin") == 0) {
    	return FW_IMAGE_TYPE_FIRMWARE;
   	}
   	
   	if ((strcmp (file, "rom.bin") == 0) || (strcmp (file, "spi_rom.bin") == 0) 
       || (strcmp (file, "romzip.bin") == 0)) {
       	return FW_IMAGE_TYPE_ROM;
    }
    
    return FW_IMAGE_TYPE_NONE;
}

int fw_open_flash_image_file(char *file, unsigned long handle)
{
    int result;
	FW_FLASH_IMAGE_T *fi = gFWFlashImage;

	// Check for one time initialization
	if (fi == NULL) {
		result = fw_flash_image_init();
		if (result != FW_SUCCESS) {
			return (result);
		}
		fi = gFWFlashImage;
	}
	
    if (fi->fi_handle == 0) {
		fi->fi_handle = handle;
		fi->imageType = fw_get_image_type(file);
		printf("Writing %s to flash...\n", file);
    } else {
        printf ("ERROR: Another flash image upload is already in progress.\n");
        return (FW_GENERROR);
    }
    
    return FW_SUCCESS;
}

int fw_write_flash_image_file(char *bufferp, unsigned long buflen, unsigned long handle)
{
    unsigned long templen;
    int k;
    int result;
	int flashFull = 0;
	FW_FLASH_IMAGE_T *fi = gFWFlashImage;

	// Check if initialized
	if (fi == NULL) {
		return (FW_INIT_ERROR);
	}
	
	// Validate handle is the same one we opened with.
    if (fi->fi_handle == 0 || fi->fi_handle != handle)
    {
        printf ("ERROR: Can't store flash image. Invalid handle.\n");
        return FW_INVALID_HANDLE;
    }

    if ((fi->imageType == FW_IMAGE_TYPE_FIRMWARE) && (fi->currentSector == 0))
    {
       fi->writeIndex = NAAppOffsetInFlash;
       for (k = fi->currentSector; k < fi->sectors; k++)
       {
          if (fi->sectorSizes[k] <= fi->writeIndex)
          {
             fi->writeIndex -= fi->sectorSizes[k];
             if (k == (fi->sectors - 1))
             {
                 /* No more flash memory to write, so set flag */
                 flashFull = 1;
             }
          }
          else
          {
             fi->currentSector = k;
             break;
          }
       }
       if (fi->writeIndex != 0)
       {
            printf ("_NAAppOffsetInFlash in the linker customization file is set incorrectly.\n");
            printf ("The application image must start on a sector boundary.\n");
            fi->successful = 0;
            return (FW_GENERROR);
       }
    }

    if (flashFull)
    {
      /* If all flash memory has been written,
         clear flag and return -2 */
      printf("Image size exceeds flash memory size, and flash memory has been overwritten.\n");
      fi->successful = 0;
      return (FW_GENERROR);
    }
    if ((fi->currentLength + buflen) > fi->romsize)
    {
      /* If data received exceeds available flash memory,
         clear flag and return -2 */
      printf("Image size exceeds flash memory size, but flash memory has not been overwritten.\n");
      fi->successful = 0;
      return (FW_GENERROR);
    }
    /*  copy this file into our buffer  */
    if (fi->currentLength + buflen > FW_FLASH_IMAGE_BUFFER_SIZE)
    {
        templen = FW_FLASH_IMAGE_BUFFER_SIZE - fi->currentLength;
    } 
    else
    {
        templen = buflen;
    }

    memcpy ((char *)&fi->sectorDatap[fi->currentLength], bufferp, templen);
    fi->currentLength += templen;

    /* Write the data to Flash in 128K chunks */
    /* Smaller data chunks decrease performance but require a smaller buffer */
    while (fi->currentLength == FW_FLASH_IMAGE_BUFFER_SIZE)
    {
        /* Write to a valid Flash sector */
        if (fi->currentSector < fi->sectors)
        {
           result = NAFlashWrite(fi->currentSector, fi->writeIndex, FW_FLASH_IMAGE_BUFFER_SIZE, fi->sectorDatap, ERASE_AS_NEEDED);
           if (result)
           {
               printf ("ftp_flash_stor NAFlashWrite() error %d.\n", result);
               fi->successful = 0;
               return FW_FLASH_WRITE_ERROR;
           }

       /*  save the extra data not written to flash -- for the next NAFlashWrite() */
           fi->currentLength = 0;
           if (buflen > templen)
           {
              if ((buflen - templen) >= FW_FLASH_IMAGE_BUFFER_SIZE)
              {
                 memcpy ((char *)&fi->sectorDatap[fi->currentLength], (char *)&bufferp[templen], FW_FLASH_IMAGE_BUFFER_SIZE);
                 templen += FW_FLASH_IMAGE_BUFFER_SIZE;
                 fi->currentLength = FW_FLASH_IMAGE_BUFFER_SIZE;
              }
              else
              {
                 memcpy ((char *)&fi->sectorDatap[fi->currentLength], (char *)&bufferp[templen], buflen - templen);
                 fi->currentLength = buflen - templen;
              }
           }		
           fi->writeIndex += FW_FLASH_IMAGE_BUFFER_SIZE;

           /* Compute the sector and sector offset to start writing next */
           for (k = fi->currentSector; k < fi->sectors; k++)
           {
              if (fi->sectorSizes[k] <= fi->writeIndex)
              {
                 fi->writeIndex -= fi->sectorSizes[k];
                 if (k == (fi->sectors - 1))
                 {
                     /* No more flash memory to write, so set flag */
                     flashFull = 1;
                 }
              }
              else
              {
                 fi->currentSector = k;
                 break;
              }
           }
        }
    }
    fi->successful = 1;
        
    return FW_SUCCESS;
}

int fw_close_flash_image_file(unsigned long handle)
{
	int rc = FW_SUCCESS;
	FW_FLASH_IMAGE_T *fi = gFWFlashImage;
	
	// Check if initialized
	if (fi == NULL) {
		return FW_INIT_ERROR;
	}
	
	// Validate handle is the same one we opened with.
    if (fi->fi_handle == 0 || fi->fi_handle != handle)
    {
        printf ("ERROR: Can't close flash image. Invalid handle.\n");
        return FW_INVALID_HANDLE;
    }
	
    if (fi->successful)  /* the whole file is downloaded successfully */
    {
    	/* Do we need to write a last bit of data? */
        if (fi->currentLength)
        {
            /* Write to a valid Flash sector */
            if (fi->currentSector < fi->sectors)
            {
               rc = NAFlashWrite(fi->currentSector, fi->writeIndex, fi->currentLength, fi->sectorDatap, ERASE_AS_NEEDED);
               if (rc)
               {
                   printf ("ftp_flash_run NAFlashWrite() error %d.\n", rc);
                   rc = FW_FLASH_WRITE_ERROR;
               }
            }
            else
            {
				printf("Image size exceeds flash memory size, and flash memory has been overwritten.\n");
				rc = FW_GENERROR;
            }
        }
        
        if (rc == 0)
        {
			printf("Image successfully written to flash.\n");
        }
    }
    
    /* Reset variables to allow downloading again */
    fi->currentSector = 0;
    fi->writeIndex = 0;
    fi->currentLength = 0;
    fi->successful = 0;
    fi->imageType = FW_IMAGE_TYPE_NONE;
    fi->fi_handle = 0;
    
    return rc;
}

