#ifndef CLIWIRELESS_H_
#define CLIWIRELESS_H_
#ifdef __cplusplus
extern "C"
{
#endif

#if (BSP_CLI_INCLUDE_IN_BSP && (BSP_CLI_SERIAL_ENABLE || BSP_CLI_TELNET_ENABLE || BSP_CLI_SSH_ENABLE))

#if BSP_WANT_WIRELESS 
int cli_set_wireless_help(int fd, int n, char *args[]);
int cli_set_wireless(int fd, int n, char *args[]);

enum {
    CLI_SET_WIRELESS_OPTION_TYPE  = 10,
    CLI_SET_WIRELESS_OPTION_SSID,
    CLI_SET_WIRELESS_OPTION_CHANNEL,
    CLI_SET_WIRELESS_OPTION_AUTHENTICATION,
    CLI_SET_WIRELESS_OPTION_MATXRATE,
    CLI_SET_WIRELESS_OPTION_TXPOWER,
    CLI_SET_WIRELESS_OPTION_WEPKEY1,
    CLI_SET_WIRELESS_OPTION_WEPKEY2,
    CLI_SET_WIRELESS_OPTION_WEPKEY3,
    CLI_SET_WIRELESS_OPTION_WEPKEY4,
    CLI_SET_WIRELESS_OPTION_WEPINDEX,
    CLI_SET_WIRELESS_OPTION_WEPMODE,
    CLI_SET_WIRELESS_OPTION_ENCRYPTION,
#if BSP_WANT_WPA
    CLI_SET_WIRELESS_OPTION_LOGIN,
    CLI_SET_WIRELESS_OPTION_PASSWORD,
    CLI_SET_WIRELESS_OPTION_PSK,
    CLI_SET_WIRELESS_OPTION_OUTER_EAP,
    CLI_SET_WIRELESS_OPTION_INNER_EAP,
#endif
    CLI_SET_WIRELESS_OPTION_COUNTRY_NAME,
    CLI_SET_WIRELESS_OPTION_BAND,
    CLI_SET_WIRELESS_OPTION_80211,
    CLI_SET_WIRELESS_OPTION_80211_D,

    /* add any index above this line
     */
    CLI_SET_WIRELESS_OPTION_MAX
};



#define CLI_SET_WIRELESS_OPTION_INFRA 0
#define CLI_SET_WIRELESS_OPTION_CREATEJOIN 1
#define CLI_SET_WIRELESS_OPTION_JOIN 2
#define CLI_SET_WIRELESS_OPTION_ALL 3

#define CLI_SET_WIRELESS_OPTION_AUTH_OPEN 0
#define CLI_SET_WIRELESS_OPTION_AUTH_WEP_AUTH 1
#define CLI_SET_WIRELESS_OPTION_AUTH_ANY 2
#define CLI_SET_WIRELESS_OPTION_AUTH_SHAREDKEY 3
#define CLI_SET_WIRELESS_OPTION_AUTH_WPA_PSK 4
#define CLI_SET_WIRELESS_OPTION_AUTH_WPA_AUTH 5
#define CLI_SET_WIRELESS_OPTION_AUTH_LEAP 6

#define CLI_SET_WIRELESS_OPTION_ENCRYPT_OPEN 0
#define CLI_SET_WIRELESS_OPTION_ENCRYPT_WEP 1
#define CLI_SET_WIRELESS_OPTION_ENCRYPT_ANY 2
#define CLI_SET_WIRELESS_OPTION_ENCRYPT_TKIP 3
#define CLI_SET_WIRELESS_OPTION_ENCRYPT_CCMP 4

#define CLI_SET_WIRELESS_OPTION_OUTER_EAP_PEAP 0
#define CLI_SET_WIRELESS_OPTION_OUTER_EAP_TLS 1
#define CLI_SET_WIRELESS_OPTION_OUTER_EAP_TTLS 2
#define CLI_SET_WIRELESS_OPTION_OUTER_EAP_ANY 3

#define CLI_SET_WIRELESS_OPTION_INNER_EAP_GTC 0
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_MD5 1
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_OTP 2
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_CHAP 3
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_MSCHAP 4
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_TTLS_MSCHAPV2 5
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_PAP 6
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_ANY 7
#define CLI_SET_WIRELESS_OPTION_INNER_EAP_NONE 8

#define CLI_SET_WIRELESS_OPTION_WEPMODE_64 0
#define CLI_SET_WIRELESS_OPTION_WEPMODE_128  1

#define CLI_SET_WIRELESS_TXRATE_OPTION_ANY 0
#define CLI_SET_WIRELESS_TXRATE_OPTION_1 1
#define CLI_SET_WIRELESS_TXRATE_OPTION_2 2
#define CLI_SET_WIRELESS_TXRATE_OPTION_5_5 3
#define CLI_SET_WIRELESS_TXRATE_OPTION_11 4
#define CLI_SET_WIRELESS_TXRATE_OPTION_6 5
#define CLI_SET_WIRELESS_TXRATE_OPTION_9 6
#define CLI_SET_WIRELESS_TXRATE_OPTION_12 7
#define CLI_SET_WIRELESS_TXRATE_OPTION_18 8
#define CLI_SET_WIRELESS_TXRATE_OPTION_24 9
#define CLI_SET_WIRELESS_TXRATE_OPTION_36 10
#define CLI_SET_WIRELESS_TXRATE_OPTION_48 11
#define CLI_SET_WIRELESS_TXRATE_OPTION_54 12

#ifdef __cplusplus
}
#endif

#endif /* BSP_WANT_WIRELESS  */
#endif /* (BSP_CLI_INCLUDE_IN_BSP && (BSP_CLI_SERIAL_ENABLE || BSP_CLI_TELNET_ENABLE || BSP_CLI_SSH_ENABLE)) */

#endif /*CLIWIRELESS_H_*/
