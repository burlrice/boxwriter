#include <fwtypes.h>
#include <tx_api.h>
#include <nastatus.h>
#include <stdio.h>

#if (GRAPHICS_PLATFORM == TRUE)
#include <gfxcustomize.h>
#include <wx/wx.h>

/* The wxEntry processing thread */
TX_THREAD txMyAppWxEntryThread;
/* Thread priority of the wxEntry processing thread */
#define MYAPP_WXENTRY_THREAD_PRIORITY 8
/* Stack size of the wxEntry processing thread */
#define MYAPP_WXENTRY_THREAD_STACK_SIZE   4*1024                
/* Stack of the wxEntry processing thread */
char myAppWxEntryThreadStack[MYAPP_WXENTRY_THREAD_STACK_SIZE];
static void myAppWxEntryThread(unsigned long param);

#endif


#include "wxroot.h"


/*
 * Initialize the graphics display.
 */
int WxRoot_init(void)
{
    NaStatus status = NASTATUS_SUCCESS; 
	
#if (GRAPHICS_PLATFORM == TRUE) 

    UINT txrc;
    // Configure platform specific graphics customizations
    status = graphicsInit();
    if (!NA_IS_SUCCESS(status)) {
        printf ("Graphics initialization failed: status=%d\n", status);
        return status;
    }
    
    // Now start wxWidgets. Normally applications will simply call wxEntry() as
    // the last step in their application start. However here we want to start
    // wxWidgets as one of our 'application services' and then return to 
    // applicationStart() in case they want to do something more. Therefore, lets 
    // spin a new thread that does nothing more than call wxEntry(). 
    // NOTE: This threads call stack must be large enough to initialize wxWidgets, 
    //       your widgets application, and the initially displayed application windows.
    txrc = tx_thread_create(
                         &txMyAppWxEntryThread,          /* Allow ThreadX to dynamically allocate thread */
                         (CHAR *)"MyApp WxEntry Thread",
                         myAppWxEntryThread,
                         0,                    
                         myAppWxEntryThreadStack,        
                         MYAPP_WXENTRY_THREAD_STACK_SIZE, /* Stack size */
                         MYAPP_WXENTRY_THREAD_PRIORITY,   /* Priority */
                         MYAPP_WXENTRY_THREAD_PRIORITY,   /* Threshhold (same) */
                         10,           /* Ticks before pre-emption in priority band */
                         TX_AUTO_START );

    if ( txrc != TX_SUCCESS ) {
        printf("Failed to start MyApp WxEntry thread. (rc = %d)\n", txrc);
        return NASTATUS_ERROR;
    }
    printf ("Graphics application service started.");
    
#else
    printf ("Graphics application service not supported on this platform.");
    status = NASTATUS_GFX_INVALID_DEVICE;
#endif           
    return status;
}



#if (GRAPHICS_PLATFORM == TRUE)
/**
 * An asynchronous thread that starts wxWidgets and runs the application
 */
static void myAppWxEntryThread(unsigned long param) {
    wxEntry(); // blocks till the application closes.
}
#endif           

