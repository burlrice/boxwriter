#ifndef _WXROOT_H
#define _WXROOT_H

#ifdef __cplusplus
extern "C"
{
#endif

int WxRoot_init(void);

#ifdef __cplusplus
}
#endif

#endif /* _WXROOT_H */
