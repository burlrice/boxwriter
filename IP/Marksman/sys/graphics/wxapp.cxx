/*
 *
 *     Module Name: wxapp.cxx
 *         Version: 1.00
 *        Language: C++
 * Compile Options: 
 * Compile defines: 
 *       Libraries: 
 *    Link Options: 
 *
 *    Entry Points: wxEntry()  : triggers call to wxApp::OnInit()
 *
 * Description.
 * =======================================================================
 * wxEntry() is the entry point for wxWidgets applications.  This
 * function is typically called from applicationStart() after the graphics
 * subsystem has been initialized. wxEntry will initialize wxWidgets,
 * activate the application declared below, and execute it by calling its
 * OnInit() method.
 *                                                                        
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 *
 */

// ============================================================================
// declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
#include <fwtypes.h>
#if (GRAPHICS_PLATFORM == TRUE)
// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"
#include "appconf.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------

// the application icon (under Windows and OS/2 it is in resources and even
// though we could still include the XPM here it would be unused)
#if !defined(__WXMSW__) && !defined(__WXPM__)
    #include "sample.xpm"
#endif

// ----------------------------------------------------------------------------
// private classes
// ----------------------------------------------------------------------------

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
    virtual bool OnInit();
};

// Define a new frame type: this is going to be our main frame
class MyFrame : public wxFrame
{
public:
    // ctor(s)
    MyFrame(const wxString& title);

    // event handlers (these functions should _not_ be virtual)
    void OnButton1(wxCommandEvent& event);
    void OnButton2(wxCommandEvent& event);
    void OnButton3(wxCommandEvent& event);
	void OnCtrlC(wxCommandEvent& event);

private:

    // output control that app writes results to
    wxTextCtrl *outTxt;
    // any class wishing to process wxWidgets events must use this macro
    DECLARE_EVENT_TABLE()
};

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

// IDs for the controls and actions - Note custom ID's start after the predefined wxID's
enum
{
    ID_BUTTON1 = wxID_HIGHEST+1,
    ID_BUTTON2,
    ID_BUTTON3,
    ID_CTRL_C,
};

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp)

// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple button and menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_BUTTON(ID_BUTTON1,  MyFrame::OnButton1)
    EVT_BUTTON(ID_BUTTON2,  MyFrame::OnButton2)
    EVT_BUTTON(ID_BUTTON3,  MyFrame::OnButton3)
    EVT_MENU(ID_CTRL_C, MyFrame::OnCtrlC)
END_EVENT_TABLE()


// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
    // call the base class initialization method, currently it only parses a
    // few common command-line options but it could be do more in the future
    if ( !wxApp::OnInit() )
        return false;

    // create the main application window
    MyFrame *frame = new MyFrame(_T(APP_DIALOG_APP_NAME));

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
    frame->Show(true);

    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
MyFrame::MyFrame(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title)
{

    // The applications main layout consists of a header and a body. 
    // The header is on top an contains the title of the application 
    // and an application image. 
    // The body has a button area on the left side and a text area on the
    // right side. Pressing the buttons on the left causes various output
    // to appear in the text area on the right.

    // First create the main content panel and the sizers
    wxPanel *cpanel = new wxPanel(this, wxID_ANY);
    wxBoxSizer *mainSizer = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *headerSizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *bodySizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer *buttonSizer = new wxBoxSizer(wxVERTICAL);

    // next create the controls and insert them into the appropriate sizers
    wxStaticText *label = new wxStaticText(cpanel, wxID_ANY, _T(APP_DIALOG_APP_NAME));
    headerSizer->Add(label, 0, wxALL, 5);
    wxBitmap bitmap(sample_xpm);
    wxStaticBitmap *bm = new wxStaticBitmap(cpanel, wxID_ANY, bitmap);
    headerSizer->Add(bm, 0, wxALL, 5);
    mainSizer->Add(headerSizer, 0, wxALL, 5);
    wxButton *b = new wxButton(cpanel, ID_BUTTON1, "Roses");
    buttonSizer->Add(b, 0, wxALL, 5);
    b = new wxButton(cpanel, ID_BUTTON2, "Violets");
    buttonSizer->Add(b, 0, wxALL, 5);
    b = new wxButton(cpanel, ID_BUTTON3, "WOW");
    buttonSizer->Add(b, 0, wxALL, 5);
    outTxt = new wxTextCtrl(cpanel, wxID_ANY, "Press any button to see its results!\n", wxDefaultPosition, wxSize(100, 100), wxTE_MULTILINE);
    bodySizer->Add(buttonSizer, 1, wxALIGN_TOP | wxLEFT);
    bodySizer->Add(outTxt, 3, wxEXPAND | wxFIXED_MINSIZE);
    mainSizer->Add(bodySizer, 0, wxEXPAND | wxFIXED_MINSIZE);
    label = new wxStaticText(cpanel, wxID_ANY, _T("Press Ctl+C to clear the results"));
    mainSizer->Add(label, 0, wxALL, 5);
    
    cpanel->SetSizer(mainSizer);

#if wxUSE_STATUSBAR
    // create a status bar just for fun (by default with 1 pane only)
    CreateStatusBar(2);
    SetStatusText(_T("Welcome to wxWidgets!"));
#endif // wxUSE_STATUSBAR

    // Define the keyboard accellerator table
    wxAcceleratorEntry accelEntries[1];
    accelEntries[0].Set(wxACCEL_CTRL, (int) 'C', ID_CTRL_C);
    wxAcceleratorTable accel(1, accelEntries);
    SetAcceleratorTable(accel);
}


// event handlers
void MyFrame::OnButton1(wxCommandEvent& WXUNUSED(event)) {
	outTxt->SetForegroundColour(*wxRED);
	outTxt->AppendText("Roses are RED!\n");
}

void MyFrame::OnButton2(wxCommandEvent& WXUNUSED(event)) {
	outTxt->SetForegroundColour(*wxBLUE);
	outTxt->AppendText("Violets are BLUE!\n");
}

void MyFrame::OnButton3(wxCommandEvent& WXUNUSED(event)) {
	outTxt->SetForegroundColour(*wxGREEN);
	outTxt->AppendText("You've just created your first wxWidgets application!\n");
}

void MyFrame::OnCtrlC(wxCommandEvent& WXUNUSED(event)) {
	outTxt->Clear();
}
#endif

