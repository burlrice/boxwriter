#ifndef appservices_h
#define appservices_h

#include <Npttypes.h>


#ifdef __cplusplus
extern "C"
{
#endif

#define HTTP_SERVER_ENABLED             TRUE  /* Web server */
#define HTTPS_SERVER_ENABLED            FALSE /* Secure web server */
#define FTP_SERVER_ENABLED              TRUE  /* FTP server */
#define FTP_CLIENT_ENABLED              FALSE  /* FTP client */
#define CLI_TELNET_SERVER_ENABLED       TRUE  /* CLI telnet server */
#define CLI_SERIAL_SERVER_ENABLED       FALSE  /* CLI serial server */
#define CLI_SSH_SERVER_ENABLED          FALSE  /* CLI SSHv2 server */
#define FLASH_FILESYSTEM_ENABLED        FALSE  /* Flash file system */
#define SSL_ENABLED                     FALSE  /* SSL */
#define SNTP_ENABLED                    FALSE  /* Simple network time protocol */
#define SMTP_ENABLED                    FALSE  /* Simple mail transfer protocol */
#define WATCHDOG_ENABLED                FALSE  /* Watchdog timer */

/*******
 * NOTE: The following services require the C++ runtime to be enabled. 
 * *****/
#define REALPORT_ENABLED                FALSE  /* RealPort server */
#define GRAPHICS_ENABLED                FALSE  /* WxWidgets graphics */

void initAppServices(void);



#define CPU_LOAD_MEASURING              FALSE  /* CPU load measuring idle thread */

#ifdef __cplusplus
}
#endif

#endif /* appservices_h */


