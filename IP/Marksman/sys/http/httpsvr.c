#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netosIo.h>
#include <tx_api.h>
#include <http_awsapi.h>
#include "httpsvr.h"
#include "fileinit.h"
#include "appservices.h"


#if HTTPS_SERVER_ENABLED

#define SSL_CERTIFICATE_FILE_PATH	FS_FLASH_VOLUME_CONFIG_DIR "/" SSL_CERTIFICATE_FILE_NAME
#define SSL_KEY_FILE_PATH			FS_FLASH_VOLUME_CONFIG_DIR "/" SSL_KEY_FILE_NAME

static int readSSLCert(int *certificate_len, char **certificate, int *cert_key_len, char **cert_key);
static int writeSSLCert(int certificate_len, char *certificate, int cert_key_len, char *cert_key);

static int certificate_len;
static char* certificate=NULL;
static unsigned char  cipher_list[] =	SSL_CIPHER_LIST_HTTPS_DEFAULT;
static int cipher_list_len=	SSL_CIPHER_LIST_HTTPS_DEFAULT_LEN;
static char* cert_key=NULL;
static int cert_key_len;
static RpHsSecureParams_t LocalHttp_Parameters;

#endif // HTTPS_SERVER_ENABLED


int HTTPServer_Init(void)
{
	int rc = 0;
	
#if HTTPS_SERVER_ENABLED
	// If a certificate already exists use it. Otherwise create a new
	// one and save it for future use.
	rc = readSSLCert(&certificate_len, &certificate, &cert_key_len, &cert_key);
	if (rc != 0)
	{
		/* Generate X.509 certificate for use by HTTPS server */
	    printf ("Generating X.509 certificate.  This may take a while...\n");
		certificate_len = NASSLX509Generate(
								&certificate, &cert_key, &cert_key_len,
								SSL_CERTIFICATE_KEY_TYPE,
								SSL_CERTIFICATE_KEY_SIZE,
								SSL_CERTIFICATE_SERIAL_NUM,
								SSL_CERTIFICATE_NAME,
								SSL_CERTIFICATE_START_DATE,
								SSL_CERTIFICATE_END_DATE);
	
		if(certificate_len <= 0 || cert_key_len <= 0)
		{
			printf("Error generating X.509 certificate.\n");
			return -1;
		}
		else
		{
			printf("X.509 certificate generated.\n");
		}
		
		// Save the certificate we just generated.
		writeSSLCert(certificate_len, certificate, cert_key_len, cert_key);
	}
	else
	{
		printf("Using existing X.509 certificate.\n");
	}
	
    /* set of the secure parameters */
    memset(&LocalHttp_Parameters, 0, sizeof(LocalHttp_Parameters));
    LocalHttp_Parameters.SecurityParams.cipher_list         = (unsigned char *)cipher_list;
    LocalHttp_Parameters.SecurityParams.cipher_list_len     = cipher_list_len;
    LocalHttp_Parameters.SecurityParams.certificate         = certificate;
    LocalHttp_Parameters.SecurityParams.certificate_len     = certificate_len;
    LocalHttp_Parameters.SecurityParams.key                 = cert_key;
    LocalHttp_Parameters.SecurityParams.key_len             = cert_key_len;

    /* the Lengthofstructure field defines the version of the protocol */
    LocalHttp_Parameters.WebServerParams.Lengthofstructure  = sizeof (HS_SERVER_PARAMETER);
	
#if HTTP_SERVER_ENABLED
	/* Start both HTTPS and HTTP server */
    LocalHttp_Parameters.SecurityParams.secure_only         = HTTP_AND_HTTPS;
    rc = NARpStartServerSecure(&LocalHttp_Parameters);
	if (rc == 0)
	{
	printf("HTTPS/HTTP server started.\n");
	}
	else
	{
		printf("Error starting HTTPS/HTTP server.\n");
	}
#else
	/* Start HTTPS server only */
    LocalHttp_Parameters.SecurityParams.secure_only         = HTTPS_ONLY;
    rc = NARpStartServerSecure(&LocalHttp_Parameters);
	if (rc == 0)
	{
		printf("HTTPS server started.\n");
	}
	else
	{
		printf("Error starting HTTPS server.\n");
	}
#endif
	
#else /* HTTPS_SERVER_ENABLED */

#if HTTP_SERVER_ENABLED
	/* Start the HTTP server only */
	rc = RpHSStartServer();
	if (rc == 0)
	{
		printf("HTTP server started.\n");
	}
	else
	{
		printf("Error starting HTTP server.\n");
	}
#endif	

#endif /* HTTPS_SERVER_ENABLED */

	return rc;
}


#if HTTPS_SERVER_ENABLED

static int readFile(int *file_length, char **file_data, char *file_name)
{
	size_t bytes_read = 0;
	FILE *file = NULL;
	
	// Open the file
	file = fopen(file_name, "rb");
	if (file == NULL) return -1;

	// Determine size of file	
	fseek(file, 0, SEEK_END);
	*file_length = ftell(file);
	fseek(file, 0, SEEK_SET);
	if (*file_length < 0) {
		fclose(file);
		return -1;
	}
	
	// Allocate memory to hold the file contents.
	*file_data = (char*) malloc(*file_length);
	if (*file_data == NULL) {
		fclose(file);
	}
	
	// Read data from the file.
	bytes_read = fread(*file_data, sizeof(char), *file_length, file);
	
	fclose(file);
	
	if (bytes_read != *file_length) {
		free(*file_data);
		*file_data = NULL;
		*file_length = 0;
		return -1;
	}
	
	return 0;
}


static int writeFile(int file_length, char *file_data, char *file_name)
{
	size_t bytes_written = 0;
	FILE *file = NULL;
	
	// Open the file
	file = fopen(file_name, "wb");
	if (file == NULL) return -1;

	// Write data to the file.
	bytes_written = fwrite(file_data, sizeof(char), file_length, file);
	
	fclose(file);
	
	if (bytes_written != file_length) {
		return -1;
	}
	
	return 0;
}


static int readSSLCert(int *certificate_len, char **certificate, int *cert_key_len, char **cert_key)
{
	int rc = 0;
	
	// Read the certificate file.
	rc = readFile(certificate_len, certificate, SSL_CERTIFICATE_FILE_PATH);
	if (rc != 0) {
		return rc;
	}
	
	// Read the key file.
	rc = readFile(cert_key_len, cert_key, SSL_KEY_FILE_PATH);
	if (rc != 0) {
		return rc;
	}

	return 0;	
}


static int writeSSLCert(int certificate_len, char *certificate, int cert_key_len, char *cert_key)
{
	int rc = 0;
	
	// Write the certificate file.
	rc = writeFile(certificate_len, certificate, SSL_CERTIFICATE_FILE_PATH);
	if (rc != 0) {
		return rc;
	}
	
	// Write the key file.
	rc = writeFile(cert_key_len, cert_key, SSL_KEY_FILE_PATH);
	if (rc != 0) {
		return rc;
	}

	return 0;	
}
#endif /* HTTPS_SERVER_ENABLED */
