/*
 *  Copyright (c) 1996-2007 Digi International Inc., All Rights Reserved
 *
 *  This software contains proprietary and confidential information of Digi
 *  International Inc.  By accepting transfer of this copy, Recipient agrees
 *  to retain this software in confidence, to prevent disclosure to others,
 *  and to make no use of this software other than that for which it was
 *  delivered.  This is an unpublished copyrighted work of Digi International
 *  Inc.  Except as permitted by federal law, 17 USC 117, copying is strictly
 *  prohibited.
 *
 *  Restricted Rights Legend
 *
 *  Use, duplication, or disclosure by the Government is subject to
 *  restrictions set forth in sub-paragraph (c)(1)(ii) of The Rights in
 *  Technical Data and Computer Software clause at DFARS 252.227-7031 or
 *  subparagraphs (c)(1) and (2) of the Commercial Computer Software -
 *  Restricted Rights at 48 CFR 52.227-19, as applicable.
 *
 *  Digi International Inc. 11001 Bren Road East, Minnetonka, MN 55343
 *
 * Description.
 * =======================================================================
 * Contains the function stubs for CGI functionality.
 *                                                                        
 *                                                                        
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 * SW   09/19/01   RpExtern.h --> AsExtern.h; void * to RpUserExitInit()
 * SW   10/01/01   Added include rpcgi.h
 * PMJ  10/04/01   Updated the prototype.
 *
 */

#include "AsExtern.h"
#include "RpCgi.h"

int RpUserExitInit(void *theTaskDataPtr) {

    return 0;
}

int RpUserExitDeInit() {

    return 0;
}

void RpExternalCgi(void *theDataPtr, rpCgiPtr theCgiPtr) {

    return;
}


