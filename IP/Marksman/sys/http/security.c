#include "AsExtern.h"
#include <http_awsapi.h>
#include <http_security.h>


/*
    The RpHSInitSecurityTable routine is called once at start up to
	allow setting of security databases. The serverdata is passed and
	can be used to perform other initialization as needed.
*/
void RpHSInitSecurityTable(void *serverdata)
{
    NAHttpSetRealmSecurity (0, "NA-HTTP AWS Realm",
#ifdef THIS_APPLICATION_USES_DIGEST_AUTHENTICATION
        /*  Set realm 1 to Single Access with MD5 Digest Authentication */
        NA_HTTP_SECURITY_SINGLE_ACCESS, NA_HTTP_SECURITY_DIGEST_AUTHENTICATION);
#else
        /*  Set realm 1 to Multiple Access with Basic Authentication    */
        NA_HTTP_SECURITY_MULTIPLE_ACCESS, NA_HTTP_SECURITY_BASIC_AUTHENTICATION);
#endif
    NAHttpSetRealmSecurity (1, "NA-HTTP AWS Realm",
        NA_HTTP_SECURITY_MULTIPLE_ACCESS, NA_HTTP_SECURITY_BASIC_AUTHENTICATION);
        
    NAHttpSetRealmSecurity (2, "NA-HTTP AWS Realm",
        NA_HTTP_SECURITY_MULTIPLE_ACCESS, NA_HTTP_SECURITY_BASIC_AUTHENTICATION);
}


