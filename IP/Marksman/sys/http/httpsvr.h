#ifndef _HTTPSVR_H
#define _HTTPSVR_H

#include "AsExtern.h"
#include "appservices.h"

#if HTTPS_SERVER_ENABLED
#include <ssl.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif


#if HTTPS_SERVER_ENABLED
#define SSL_CERTIFICATE_KEY_TYPE	SSL_CERTIFICATE_KEY_TYPE_RSA
#define SSL_CERTIFICATE_KEY_SIZE	1024
#define SSL_CERTIFICATE_SERIAL_NUM	1
#define SSL_CERTIFICATE_NAME		"Generated SSL Certificate"
#define SSL_CERTIFICATE_START_DATE	"020630120000Z"
#define SSL_CERTIFICATE_END_DATE	"120630120000Z"

#define SSL_CERTIFICATE_FILE_NAME	"ssl.cert"
#define SSL_KEY_FILE_NAME			"ssl.key"
#endif

// HTTP Server service initialization function (called only once).
int HTTPServer_Init(void);


// Storage Location Fields for HTTP file upload and download
#define NAFS_LOCATION_FILE_SYSTEM               0   // Store to Local File System
#define NAFS_LOCATION_FIRMWARE                  1   // Store/Update Firmware (image.bin, rom.bin, ...)

// Valid realm for firmware uploads.
#define NAFS_FIRMWARE_UPLOAD_REALM              kRpPageAccess_Realm1

// HTTP file system functions
RpErrorCode RpHSGetErrorStatus(int theFileNumber, int *theErrorStatus);
RpErrorCode RpHSSetCurrentDirectory(int theFileNumber, char *theDirectory);
RpErrorCode RpHSSetStorageLocation(int theFileNumber, int theLocation);


#ifdef __cplusplus
}
#endif

#endif /* _HTTPSVR_H */
