#include <string.h>
#include <stdlib.h>
#include <bsp.h>
#include <armgdb.h>
#include "appservices.h"
#include "ncc_init.h"
#include "watchdog.h"
#include <fileinit.h>
#include <ftpsvr.h>
#include <clisvr.h>
#include <httpsvr.h>
#include <cpu_utilization.h>
#include <nsuptime.h>
#include <realportsvr.h>
#include <wxroot.h>


/*
 *
 *  Function: void initAppServices(void)
 *
 *  Description:
 *
 *      This routine is responsible for starting system services for the application.
 *      Services such as the HTTP server, FTP server, etc. are initialized and
 *      started. These services are defined and enabled in appservices.h.
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */

void initAppServices(void)
{

	UpTime_Init();

#if WATCHDOG_ENABLED
    if (!are_we_in_debugger())
    {
        watchdogServicePeriodically();
    }
#endif

#if CPU_LOAD_MEASURING
	CPUUtilization_Init();
#endif	
	
#if FLASH_FILESYSTEM_ENABLED
	FileSystem_InitVolume();
#endif

#if FTP_SERVER_ENABLED
    FTPServer_init();
#endif    

#if HTTP_SERVER_ENABLED || HTTPS_SERVER_ENABLED
	HTTPServer_Init();
#endif

#if CLI_TELNET_SERVER_ENABLED || CLI_SERIAL_SERVER_ENABLED || CLI_SSH_SERVER_ENABLED
	CLIServer_init();
#endif

#if REALPORT_ENABLED
    RealPortServer_init();
#endif  

#if GRAPHICS_ENABLED
    WxRoot_init();
#endif  
}


