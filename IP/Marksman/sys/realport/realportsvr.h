#ifndef _REALPORTSVR_H
#define _REALPORTSVR_H

#ifdef __cplusplus
extern "C"
{
#endif

#define REAL_PORT_DEVICE              "/com/1"

int RealPortServer_init(void);


#ifdef __cplusplus
}
#endif

#endif /* _REALPORTSVR_H */
