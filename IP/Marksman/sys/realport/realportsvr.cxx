#include <realport.h>
#include <tx_api.h>
#include <sockapi.h>

#include "realportsvr.h"


int RealPortServer_init(void)
{
    int retval;
    REALPORT_SERVER_CONFIG_TYPE rsct;

    /* Register devices for use by RealPort */
    /*  
       Devices are enumerated to the client in the order in which they
       were registered. Defining BSP_DIALOG_PORT, or BSP_STDIO_PORT to "/com/1"
       in bsp.h, will prevent it from being used by
       RealPort in this example.
    */

    if((retval = RealPortRegister((char *) REAL_PORT_DEVICE)) != REALPORT_SUCCESS)
    {
        printf("RealPort: warning: RealPortRegister() returned %d.\n", retval);
        return retval;
    }

    /* Configure the RealPort server */
    rsct.tcp_port = 771;     /* Server TCP port */
    rsct.priority = 10;      /* Thread priority base */
    rsct.max_concurrent = 4; /* Maximum number of client connections */
    rsct.timeslice = 10;     /* Timeslice (ticks) for connection processing */
 
    /* Attempt to instantiate the RealPort server */
    if((retval = RealPortStartServer(&rsct)) != REALPORT_SUCCESS)
    {
        printf("RealPort: fatal: RealPortStartServer() returned %d.\n", retval);
        return retval;
    }
    printf ("Realport server started.\n");
    return retval;
}
