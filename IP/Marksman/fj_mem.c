#include "fj_base.h"

#ifdef _WINDOWS

FJDLLExport void * fj_malloc( long lSize )
{
	LPBYTE pBuf = malloc( lSize );
	/*
    if( (LPBYTE)0x007EE0A0 == pBuf )
	{
		pBuf++;
		pBuf -= 1;
	}
*/
	return( pBuf );
}

FJDLLExport void * fj_calloc( long lCount, long lSize )
{
	return( calloc( lCount, lSize ) );
}

FJDLLExport void  fj_free( void * pBuffer )
{
	free( pBuffer );
	return;
}

#endif //_WINDOWS
