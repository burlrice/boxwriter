/*
			  ANSI C Runtime Library

	Copyright (c) 1983-1999 Green Hills Software, Inc.

	This program is the property of Green Hills Software, Inc,
	its contents are proprietary information and no part of it
	is to be disclosed to anyone except employees of Green Hills
	Software, Inc., or as agreed in writing signed by the President
	of Green Hills Software, Inc.
*/
#ifndef _STRING_H
#define _STRING_H

#include "../fj.h"

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(__Chorus)
#include <c_types.h>
#else
#if defined(__cplusplus) && !defined(NULL)
#define NULL 0L											/* C++ requires this to be integral, not a pointer */
#elif !defined(NULL)
#define NULL ((void *)0)								/* locale.h stddef.h stdio.h stdlib.h string.h time.h */
#endif

#ifndef _SIZE_T
#define _SIZE_T
# if defined(__Ptr_Is_64) && !defined(__Int_Is_64)
	typedef unsigned long size_t;
# else
	//typedef unsigned int size_t;
# endif
#endif													/* _SIZE_T */
#endif													/* defined(__Chorus) */

#if defined(__STDC__) || defined(__cplusplus)
#define __ARGS(x)       x
#define __VOIDPTR       void
#elif defined(__PROTOTYPES__)
#define __ARGS(x)       x
#define __VOIDPTR       char
#else
#define __ARGS(x)       ()
#define __VOIDPTR       char
#endif

#ifdef __cplusplus
//	const __VOIDPTR *memchr __ARGS((const void *, int, size_t));
	const char *strchr __ARGS((const char *, int));
	const char *strpbrk __ARGS((const char *, const char *));
	const char *strrchr __ARGS((const char *, int));
	const char *strstr __ARGS((const char *, const char *));
	/*
	extern "C++"
	{
		inline __VOIDPTR *memchr(void *_S, int _C, size_t _N)
			{return ((__VOIDPTR *)memchr(_S, _C, _N)); }
		inline char *strchr(char *_S, int _C)
			{return ((char *)strchr((const char *)_S, _C)); }
		inline char *strpbrk(char *_S, const char *_P)
			{return ((char *)strpbrk((const char *)_S, _P)); }
		inline char *strrchr(char *_S, int _C)
			{return ((char *)strrchr((const char *)_S, _C)); }
		inline char *strstr(char *_S, const char *_P)
			{return ((char *)strstr((const char *)_S, _P)); }
	}
	*/
#else
	__VOIDPTR *memchr __ARGS((const void *, int, size_t));
	char *strchr __ARGS((const char *, int));
	char *strpbrk __ARGS((const char *, const char *));
	char *strrchr __ARGS((const char *, int));
	char *strstr __ARGS((const char *, const char *));
#endif

#ifdef _DEBUG
	/* __VOIDPTR *memchr  __ARGS((const void *, int, size_t)); */
	__VOIDPTR *memcpy  __ARGS((void *, const void *, size_t));
	__VOIDPTR *memmove __ARGS((void *, const void *, size_t));
	__VOIDPTR *memset  __ARGS((void *, int, size_t));
#endif


	char *strdup   __ARGS((const char *));
	char *strcat   __ARGS((char *, const char *));
	char *strncat  __ARGS((char *, const char *, size_t));
	char *strcpy   __ARGS((char *, const char *));
	char *strncpy  __ARGS((char *, const char *, size_t));
	char *strtok   __ARGS((char *, const char *));
	char *strtok_r __ARGS((char *s1, const char *s2, char **ppLast));

	int memcmp  __ARGS((const void *, const void *, size_t));
	int strcmp  __ARGS((const char *, const char *));
	int strncmp __ARGS((const char *, const char *, size_t));
	int strcoll __ARGS((const char *, const char *));

	size_t strlen  __ARGS((const char *));
	size_t strcspn __ARGS((const char *, const char *));
	size_t strspn  __ARGS((const char *, const char *));
	size_t strxfrm __ARGS((char *, const char *, size_t));

	char *strerror __ARGS((int));

#if __STDC__ == 0
	char *index  __ARGS((const char *, const char));
	char *rindex __ARGS((const char *, const char));
#endif

#undef __ARGS
#undef __VOIDPTR
#ifdef __cplusplus
}															/* extern "C" */
#endif
#endif														/* _STRING_H */
