#ifndef FJ_BASE_H_
#define FJ_BASE_H_

#ifndef _WINDOWS

#include "fwtypes.h"
//#include "tx_port.h" // this ARM version of fj_base.h is not the same as the Windows version

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

#define FAR
#define NEAR
#define PASCAL
#define FJDLLExport

//typedef unsigned  char WORD8;	// use these 'WORDn' types when hardware, software,
//typedef unsigned short WORD16;	//     or protocols with exertal interfaces
//typedef unsigned  long WORD32;	//     demand certain lengths
//typedef            int BOOL;
typedef           char STR, STRING;
//typedef           long LONG;
//typedef           void VOID;
//typedef          char CHAR;
//typedef unsigned  char UCHAR;
//typedef unsigned short USHORT;
//typedef unsigned char BYTE;
//typedef unsigned long ULONG;
//typedef unsigned char byte;
typedef float  FLOAT;
typedef double DOUBLE;

typedef  void * LPVOID;

typedef       FAR STRING *LPSTR,  *LPSTRING,  * const CLPSTR,  * const CLPSTRING;
typedef const FAR STRING *LPCSTR, *LPCSTRING, * const CLPCSTR, * const CLPCSTRING;

typedef       FAR   char *LPCHAR,  * const CLPCHAR;
typedef const FAR   char *LPCCHAR, * const CLPCCHAR;

typedef       FAR   unsigned char *LPBYTE,  * const CLPBYTE;
typedef const FAR   unsigned char *LPCBYTE, * const CLPCBYTE;

typedef FAR USHORT *LPUSHORT;
typedef FAR unsigned char *LPUCHAR;

typedef FAR unsigned long *LPULONG;

typedef unsigned long ip_addr;

#ifdef TRUE
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#define FALSE 0
#define TRUE (!(FALSE))

#ifdef NULL
#undef NULL
#endif
#ifndef NULL							// the real definition of null
#ifdef  __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#ifdef LITTLE_ENDIAN
#undef LITTLE_ENDIAN					// make sure LITTLE_ENDIAN is not defined. bug from ipport.h
#endif

#else									// _WINDOWS
#define     FJDLLExport   __declspec(dllexport)

typedef unsigned  char WORD8;			// use these 'WORDn' types when hardware, software,
typedef unsigned long ip_addr;
typedef char STR, STRING;
typedef FAR USHORT *LPUSHORT;
typedef FAR unsigned char *LPUCHAR;

typedef FAR ULONG *LPULONG;

#ifdef LITTLE_ENDIAN
#undef LITTLE_ENDIAN					// make sure LITTLE_ENDIAN is not defined. bug from ipport.h
#endif
#endif									//_WINDOWS

#endif /*FJ_BASE_H_*/
