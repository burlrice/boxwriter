#include <hservapi.h>
#include "fj.h"
#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_label.h"

// externals
//extern devBoardParamsType fj_nvparams;
extern const CHAR fj_FontID [];
extern const CHAR fj_LabelID [];
extern const CHAR fj_MessageID [];
extern const CHAR fj_bmpID [];
extern const struct fjsysunits fj_aUnits[];
// forward declarations
const VOLTINFO fj_aVoltInfo[];
const TEMPINFO fj_aTempInfo[];
// actual declarations of structures
static FJ_PARAM        fj_param;
static FJ_GLOBAL       fj_global;
static FJSYSTEM        fj_sys;
static FJPRINTHEAD_PKG fj_phpkg;
static FJPRINTHEAD_PHY fj_phphy;
static FJPRINTHEAD_OP  fj_phop;
static FJPRINTHEAD     fj_ph ={&fj_phpkg, &fj_phphy, &fj_phop};
// TODO // static FJMEMSTOR       fj_mem;


const  FJPRINTHEAD_PKG fj_PHPK[2]=		// UJ II 2" head
{
	1.37, 2.5-1.37, 4.37-.95, .95, FJPH_UNITS_ENGLISH_INCHES,
										// Graphics 4" head
	1.37, 2.5-1.37, 6.37-.95, .95, FJPH_UNITS_ENGLISH_INCHES
};
//	chan orif drop heig widt  angle  span-cent  span-out    ChCh-str     ChCh-ang  offset
const  FJPRINTHEAD_PHY fj_PHPH[11]=
{
	32, 352, 429, 0.4, 0.4, 90.00,   1.81288,  1.86700, (1.81288/31.),   0    ,    .040, FJPH_UNITS_ENGLISH_INCHES,
	32, 256, 312, 0.4, 0.4, 90.00,   1.81288,  1.86600, (1.81288/31.),   0    ,     0, FJPH_UNITS_ENGLISH_INCHES,
	32, 224, 392, 0.4, 0.4, 90.00,   1.81288,  1.86600, (1.81288/31.),   0    ,    .0, FJPH_UNITS_ENGLISH_INCHES, // AC
	32, 192, 234, 0.4, 0.4, 32.30,   1.81288,  1.86588, (1.81288/31.),   .0494,     0, FJPH_UNITS_ENGLISH_INCHES,
	32,  96, 189, 0.4, 0.4, 27.17,   1.81288,  1.86588, (1.81288/31.),   .0520,     0, FJPH_UNITS_ENGLISH_INCHES,
	32,  96, 189, 0.4, 0.4, 15.50,   1.81288,  1.86588, (1.81288/31.),   .0564,     0, FJPH_UNITS_ENGLISH_INCHES,
	256, 768, 132, 0.4, 0.4, 90.00,   4.0    ,  4.0    , (4.0/255.),      0    ,    .040, FJPH_UNITS_ENGLISH_INCHES,
	128, 384, 132, 0.4, 0.4, 90.00,   4.0    ,  4.0    , (4.0/255.),      0    ,    .040, FJPH_UNITS_ENGLISH_INCHES,
	32, 192, 360, 0.4, 0.4, 32.30,   1.81288,  1.86588, (1.81288/31.),   .0494,     0, FJPH_UNITS_ENGLISH_INCHES,
	256, 768, 132, 0.4, 0.4, 90.00,   4.0    ,  4.0    , (4.0/255.),      0    ,    .330, FJPH_UNITS_ENGLISH_INCHES, // 768 Jet-L330
	256, 768, 132, 0.4, 0.4, 90.00,   4.0    ,  4.0    , (4.0/255.),      0    ,    .310, FJPH_UNITS_ENGLISH_INCHES, // 768 Jet-L310
	256, 768, 132, 0.4, 0.4, 90.00,   4.0    ,  4.0    , (4.0/255.),      0    ,    0, FJPH_UNITS_ENGLISH_INCHES, // 768 Jet-0
};

// voltage table 1 = 70V max, 2 = 90V max, 3 = 150V max
//                                                                                    Force
//                                                                                    Head
//     name                      PHY PKG Vtab Volt Pres   cur1 cur2 cur3  SPon   SPln  SPfr  SPon    SPoff   SPdu SPin Invert ChipInv Ttab ThdR T1   T2   T3 Cal1  Cal2  Cal3  AMS   A1    A2   A3    A4    A5  A6  A7,  AutoPrint
const  FJPRINTHEAD_OP  fj_PHOP[]=
{
	"ProSeries 352"           , 0, 0,  3,   0, FALSE, 13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  1,   3,  65,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  10, 1000, 2500,  3, 20,  1800,
	"ProSeries 192"           , 3, 0,  2,   0, FALSE, 13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   3,  60,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  10, 1000, 2500,  3, 20,  1800,
	"ProSeries 96 "           , 4, 0,  1,   0, FALSE, 17.0, 0.0, 0.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   3,  60,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  10, 1000, 2500,  3, 20,  1800,
	"ProSeries 768"           , 6, 1,  3,  90, TRUE,  13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   0,   0,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  28, 1000, 5500,  2, 100, 1800,
	"ProSeries 384"           , 7, 1,  3,  90, TRUE,  13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   0,   0,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  28, 1000, 5500,  2, 100, 1800,
	"ProSeries NP192"         , 8, 0,  2,   0, FALSE, 17, 4, 4,       FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   3,  60,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  10, 1000, 2500,  3, 20,  1800,
	"ProSeries AC"            , 2, 0,  3,   0, FALSE, 17.0, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   3,  60,   0,  0,  0,    0,    0,  FALSE,  3, 1000,  10, 1000, 2500,  3, 20,  1800,
	"768 Jet-L330"      	  , 9, 1,  3,  90, TRUE,  13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   0,   0,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  28, 1000, 5500,  2, 100, 1800,
	"768 Jet-L310"      	  , 10,1,  3,  90, TRUE,  13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   0,   0,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  28, 1000, 5500,  2, 100, 1800,
	"768 Jet-0"	    	  	  , 10,1,  3,  90, TRUE,  13.5, 5.0, 6.0, FALSE,   0,    0,       0,      0,   0,   0,  FALSE, FALSE,  0,   0,   0,   0,  0,  0,    0,    0,   TRUE,  3, 1000,  28, 1000, 5500,  2, 100, 1800,
};


//
// NOTE: the first entries in the ROMENTRY table must agree with the #defines in fj.h
//

// normal ROMENTRy table used before modification for multiple languages
ROMENTRY fj_RomTable[] =
{
	"(no file)"   ,        0,        0, FALSE, FALSE,
	"loader.bin"  , 0x000000, 0x002000, FALSE, FALSE,
										// reboot
	"ram.bin"     , 0x002000, 0x17e000, FALSE, TRUE,
	"ga.bin"      , 0x180000, 0x080000, FALSE, FALSE,
										// place holder for memory dedicated to msgs and fonts
	"fontmsg"     , 0x200000, 0x1fc000, FALSE, FALSE,
	"logo.jpg"    , 0x3fc000, 0x001000, FALSE, FALSE,
	"parm_fj"     , 0x3fd000, 0x002e00, FALSE, FALSE,
	"parm_netos"  , 0x3ffe00, 0x000200, FALSE, FALSE
};

// the CVT is put into its own linker section.
// the linker directives force this section to be the first in RAM.
// the current location is at 0x4000. This is defined in fj.h.
// modules linked together with this CVT section can access it by -
// - having an extern for fj_CVT
// - using the pFJcvt pointer defined in fj.h
//
// modules linked separately for other downloads must use the pointer.
#pragma ghs section rodata=".fj_cvt"
const FJ_CVT  fj_CVT =
{
	//&fj_nvparams,
	&fj_param,
	NULL, // TODO //		&fj_mem,							// pointer to memstore for "permanent" parameters
	&fj_global,
	&fj_sys,							// pfsys
	&fj_ph,								// pfph
	(const FJPRINTHEAD_PKG (*)[])&fj_PHPK,
	(const FJPRINTHEAD_PHY (*)[])&fj_PHPH,
	(const FJPRINTHEAD_OP  (*)[])&fj_PHOP,
										// lFJPhOpCount
	sizeof(fj_PHOP) / sizeof(FJPRINTHEAD_OP),
	(LPGA)GA_BASE,
	(LPFJ_BRAM)BRAM_BASE,
	
	/* (const VOLTINFO (*)[])fj_aVoltInfo, */(const struct voltinfo (* const)[])fj_aVoltInfo,
	/* (const TEMPINFO (*)[])fj_aTempInfo, */(const struct tempinfo (* const)[])fj_aTempInfo,

	/* (const ROMENTRY (*)[])fj_RomTable, */(const struct rom_entry (*)[])&fj_RomTable,
	sizeof(fj_RomTable)/sizeof(ROMENTRY),

	&fj_FontID,
	&fj_LabelID,
	&fj_MessageID,
	&fj_bmpID,

	NULL,
	NULL,
	NULL,
	NULL,

	//		  &fj_writeInfo,
	//		  &fj_malloc,
	//		  &fj_free,

	// from fj_info_soft.c
	&fj_getVersionNumber,
	&fj_getVersionDate,
	&fj_setSerial,
	&fj_setID,
	&fj_setGroupID,
	// from fj_print_head.c
	&fj_print_head_SetStatus,
	&fj_print_head_EditStart,
	&fj_print_head_EditCancel,
	&fj_print_head_EditSave,

	// from fj_print_head_info.c
	&fj_print_head_SendPhotocellMode,
	&fj_print_head_SendSelectedCNT,
	&fj_print_head_SendInfoAll,
	&fj_print_head_CountsResetAll,
	&fj_print_head_CountsCustomerReset,
	// from fj_print_headGAENI.c
	&fj_print_head_SetRTClockTM,
	&fj_print_head_SetGAValues,
	&fj_print_head_SetGAEncoder,
	&fj_print_head_SetGAWriteSwitches,
/*
	&fj_print_head_PhotoCell,
	&fj_print_head_PrintStepPattern,
	&fj_print_head_PrintTestPattern,
	&fj_print_head_InitENI,
	&fj_print_head_KillENI,
	// from fj_memstorage.c
	&fj_MemStorDeleteElement,
	&fj_MemStorFindAllElements,
*/
	// from fj_misc.c
	&fj_mktime,
	&fj_FloatUnits,
	&fj_DoubleToStr,
	&fj_IPToStr,
	&fj_StrToIP,
	&fj_processParameterStringInput,
	&fj_processParameterStringOutput,
	&fj_ParseKeywordsFromStr,
	// from fj_font.c
	&fj_FontDestroy,
	&fj_FontFromBuffer,
	// from fj_element.c
	&fj_ElementTextNew,
	&fj_ElementBarCodeNew,
	&fj_ElementDynTextNew,
	&fj_ElementDynBarCodeNew,
	&fj_ElementDataMatrixNew,
	&fj_ElementBitmapNew,
	&fj_ElementCounterNew,
	&fj_ElementDatetimeNew,
	&fj_ElementDynimageNew,
	&fj_ElementNewFromTypeString,
	&fj_ElementDup,
	&fj_ElementListAdd,
	&fj_ElementListRemove,
	&fj_ElementListDestroyAll,
	&fj_ElementMakeMemstorName,
	// from fj_message.c
	&fj_MessageNew,
	&fj_MessageDestroy,
	&fj_MessageReset,
	&fj_MessagePhotocellBuild,
	&fj_MessageGetText,
	&fj_MessageBuildFromName,
	&fj_MessageBuildFromString,
	NULL, //&fj_MessageAddToMemstor,
	NULL, //&fj_MessageDeleteFromMemstor,
	// from fj_printhead.c
	&fj_PrintHeadSetSerial,
	&fj_PrintHeadSetNameShort,
	&fj_PrintHeadSetNameLong,
	&fj_PrintHeadReset,
	&fj_PrintHeadIsEdit,
	// from fj_label.c
	&fj_LabelNew,
	&fj_LabelDestroy,
	NULL, //&fj_LabelBuildFromString,
	NULL, //&fj_LabelBuildFromName,
	NULL, //&fj_LabelAddToMemstor,
	// from fj_system.c
	&fj_aUnits,
	&fj_SystemBuildSelectedLabel,
	&fj_SystemBuildScannerDefaultLabel,
	&fj_SystemGetPasswordForLevel,
	&fj_SystemGetPasswordLevel,
	&fj_SystemMakeLevelID,
	&fj_SystemGetLevelID,
	&fj_SystemGetTime,
	&fj_SystemGetDateTimeFullString,
	&fj_SystemGetDateTimeString,
	&fj_SystemGetDateString,
	&fj_SystemGetTimeNoSecString,
	&fj_SystemGetMonthString,
	&fj_SystemGetDayString,
	&fj_SystemGetDayDateString,
	&fj_SystemParseDateTime,
	NULL, //&fj_SaveBRAMCounter,
	&fj_ResetBRAMCounters,
	NULL, //&fj_RestoreBRAMCounters,
	// from fj_rom.c
	&fj_ReadFJRomParams,
	&fj_WriteFJRomParams,
	//&fj_ReadNETOSRomParams,
	//&fj_WriteNETOSRomParams,

/*
	// from url.c
	&AppSearchURL,

	// from NetSilicon hservapi.h
	&HSSend,
	&HSSendBinary,
	&HSGetValue,
	&HSRemoteAddress,
	&HSTypeHtml,
	&HSTypeApplet,
	&HSTypeJpeg,
*/
	
	// from tx
	&tx_semaphore_get,
	&tx_semaphore_put,
	&tx_thread_resume
};
#pragma ghs section rodata=default

const WORD16 awTempTable60[] =
{
	969,								// 0
	966,
	963,
	960,
	957,
	953,
	950,
	946,
	942,
	938,
	934,								// 10
	930,
	925,
	921,
	916,
	911,
	906,
	901,
	896,
	890,
	884,								//20
	879,
	873,
	866,
	860,
	853,
	847,
	840,
	833,
	825,
	818,								//30
	810,
	803,
	795,
	787,
	779,
	770,
	762,
	753,
	745,
	736,								// 40
	727,
	718,
	709,
	699,
	690,
	680,
	671,
	661,
	652,
	642,								// 50
	632,
	622,
	612,
	603,
	593,
	583,
	573,
	563,
	553,
	543,								// 60
	533,
	524,
	514,
	504,
	494,
	485,
	475,
	466,
	456,
	447,								// 70
	438,
	429,
	420,
	411,
	402,
	393,
	385,
	377,
	368,
	360,								//80
	352,
	344,
	336,
	328,
	321,
	313,
	306,
	299,
	292,
	285									//90
};

const WORD16 awTempTable65[] =
{
	969,								// 0
	966,
	963,
	960,
	957,
	954,
	950,
	947,
	943,
	939,
	935,								//10
	931,
	927,
	922,
	918,
	913,
	908,
	903,
	898,
	892,
	887,								//20
	881,
	875,
	869,
	863,
	857,
	851,
	844,
	837,
	830,								//30
	823,
	816,
	809,
	801,
	794,
	786,
	778,
	770,
	762,
	754,								//40
	746,
	738,
	729,
	721,
	712,
	703,
	695,
	686,
	677,
	668,								//50
	660,
	651,
	642,
	633,
	624,
	615,
	606,
	597,
	589,								//60
	580,
	571,
	562,
	554,
	545,
	536,
	528,
	519,
	511,								//70
	503,
	495,
	487,
	479,
	471,
	463,
	455,
	448,
	440,
	433,								//80
	426,
	419,
	412,
	405,
	398,
	392,
	385,
	379,
	372,
	366,								//90
	360,
	354,
	349
};

const WORD16 awTempTable55[] =
{
	758,								// 0
	756,
	754,
	752,
	750,
	748,
	746,
	744,
	741,
	739,
	736,								//10
	734,
	731,
	728,
	725,
	722,
	719,
	716,
	712,
	709,
	705,								//20
	702,
	698,
	694,
	690,
	685,
	681,
	677,
	672,
	667,
	662,								//30
	657,
	652,
	647,
	642,
	636,
	631,
	625,
	619,
	613,
	607,								//40
	601,
	595,
	589,
	582,
	576,
	569,
	563,
	556,
	549,
	542,								//50
	535,
	528,
	521,
	514,
	506,
	499,
	492,
	485,
	477,
	470,								//60
	462,
	455,
	448,
	440,
	433,
	426,
	418,
	411,
	403,
	396,								//70
	389,
	382,
	375,
	368,
	360,
	354,
	347,
	340,
	333,
	326,								//80
	320,
	313,
	306,
	300,
	294,
	288,
	281,
	275,
	269,								//90
	263
};

const WORD16 awTempTable35[] =
{
	578,								// 0
	577,
	576,
	575,
	574,
	573,
	571,
	570,
	568,
	567,
	566,								//10
	564,
	562,
	561,
	559,
	557,
	555,
	553,
	551,
	549,								//20
	547,
	545,
	542,
	540,
	537,
	535,
	532,
	530,
	527,								//30
	524,
	521,
	518,
	515,
	511,
	508,
	505,
	501,
	498,								//40
	494,
	490,
	486,
	482,
	478,
	474,
	470,
	466,
	461,								//50
	457,
	453,
	448,
	443,
	439,
	434,
	429,
	424,
	419,
	414,								//60
	409,
	404,
	399,
	394,
	389,
	383,
	378,
	373,
	368,
	362,								//70
	357,
	352,
	346,
	341,
	336,
	330,
	325,
	319,
	314,								//80
	309,
	303,
	298,
	293,
	288,
	283,
	277,
	272,
	267,
	262,								//90
	257,
	252,
	247,
	242,
	238
};

const TEMPINFO fj_aTempInfo[] =
{
	0, 90, (WORD16(*)[])&awTempTable60,
	0, 90, (WORD16(*)[])&awTempTable65,
	0, 90, (WORD16(*)[])&awTempTable55,
	0, 90, (WORD16(*)[])&awTempTable35
};

// each entry in the voltage tables are a pair of number that correspond to the voltage in the comment.
// first number is the pwm number to set to get this voltage out of the variable supply.
// second number is the A/D reading of the head resister that indicates the voltage required.
const WORD16 awVoltTable70[] =
{

	56,129,								// 22 volts
	58,135,								// 23
	59,143,								// 24
	60,150,								// 25
	61,158,								// 26
	63,166,								// 27
	64,175,								// 28
	65,184,								// 29
	66,193,								// 30
	67,201,								// 31
	69,210,								// 32
	70,221,								// 33
	71,231,								// 34
	72,244,								// 35
	73,256,								// 36
	75,265,								// 37
	76,277,								// 38
	77,289,								// 39
	78,302,								// 40
	79,315,								// 41
	81,328,								// 42
	82,344,								// 43
	83,361,								// 44
	84,375,								// 45
	86,389,								// 46
	87,407,								// 47
	88,425,								// 48
	89,443,								// 49
	91,461,								// 50
	92,479,								// 51
	93,500,								// 52
	94,521,								// 53
	96,543,								// 54
	97,568,								// 55
	98,592,								// 56
	99,619,								// 57
	101,648,							// 58
	102,676,							// 59
	103,712,							// 60
	104,749,							// 61
	106,786,							// 62
	107,819,							// 63
	108,862,							// 64
	109,912,							// 65
	111,963,							// 66
	112,996,							// 67
	113,1008							// 68
};

const WORD16 awVoltTable90[] =
{
	56,82,								// 22 volts
	58,86,								// 23
	59,89,								// 24
	60,94,								// 25
	61,100,								// 26
	63,104,								// 27
	64,109,								// 28
	65,115,								// 29
	66,121,								// 30
	67,126,								// 31
	69,131,								// 32
	70,137,								// 33
	71,143,								// 34
	72,149,								// 35
	73,155,								// 36
	75,160,								// 37
	76,165,								// 38
	77,171,								// 39
	78,178,								// 40
	79,186,								// 41
	81,193,								// 42
	82,199,								// 43
	83,204,								// 44
	84,212,								// 45
	86,221,								// 46
	87,229,								// 47
	88,238,								// 48
	89,246,								// 49
	91,256,								// 50
	92,263,								// 51
	93,270,								// 52
	94,279,								// 53
	96,289,								// 54
	97,299,								// 55
	98,309,								// 56
	99,320,								// 57
	101,331,							// 58
	102,341,							// 59
	103,351,							// 60
	104,363,							// 61
	106,375,							// 62
	107,386,							// 63
	108,398,							// 64
	109,410,							// 65
	111,425,							// 66
	112,440,							// 67
	113,452,							// 68
	114,467,							// 69
	116,482,							// 70
	117,498,							// 71
	118,513,							// 72
	119,528,							// 73
	121,547,							// 74
	122,565,							// 75
	123,586,							// 76
	124,606,							// 77
	126,624,							// 78
	127,642,							// 79
	128,662,							// 80
	129,684,							// 81
	131,708,							// 82
	132,734,							// 83
	133,761,							// 84
	134,785,							// 85
	136,811,							// 86
	137,842,							// 87
	138,873,							// 88
	139,903,							// 89
	141,828								// 90
};

const WORD16 awVoltTable150[] =
{
	78,139,								// 40 volts
	79,143,								// 41
	81,146,								// 42
	82,150,								// 43
	83,153,								// 44
	84,157,								// 45
	86,161,								// 46
	87,165,								// 47
	88,169,								// 48
	89,173,								// 49
	91,177,								// 50
	92,182,								// 51
	93,186,								// 52
	94,190,								// 53
	96,194,								// 54
	97,199,								// 55
	98,203,								// 56
	99,207,								// 57
	101,211,							// 58
	102,214,							// 59
	103,219,							// 60
	104,223,							// 61
	106,227,							// 62
	107,231,							// 63
	108,235,							// 64
	109,240,							// 65
	111,244,							// 66
	112,249,							// 67
	113,253,							// 68
	114,258,							// 69
	116,263,							// 70
	117,267,							// 71
	118,272,							// 72
	119,277,							// 73
	121,282,							// 74
	122,286,							// 75
	123,290,							// 76
	124,294,							// 77
	126,298,							// 78
	127,302,							// 79
	128,307,							// 80
	129,311,							// 81
	131,316,							// 82
	132,321,							// 83
	133,327,							// 84
	134,332,							// 85
	136,337,							// 86
	137,343,							// 87
	138,348,							// 88
	139,353,							// 89
	141,359,							// 90
	142,364,							// 91
	143,369,							// 92
	144,373,							// 93
	146,378,							// 94
	147,383,							// 95
	148,389,							// 96
	149,395,							// 97
	151,401,							// 98
	152,406,							// 99
	153,412,							// 100
	154,418,							// 101
	156,424,							// 102
	157,430,							// 103
	158,436,							// 104
	159,442,							// 105
	161,448,							// 106
	162,454,							// 107
	163,460,							// 108
	164,466,							// 109
	166,472,							// 110
	167,477,							// 111
	168,482,							// 112
	169,487,							// 113
	171,492,							// 114
	172,498,							// 115
	173,504,							// 116
	174,510,							// 117
	176,516,							// 118
	177,522,							// 119
	178,528,							// 120
	179,536,							// 121
	180,544,							// 122
	182,551,							// 123
	183,558,							// 124
	184,565,							// 125
	185,571,							// 126
	187,577,							// 127
	188,583,							// 128
	189,589,							// 129
	190,596,							// 130
	191,603,							// 131
	193,610,							// 132
	194,617,							// 133
	195,624,							// 134
	196,630,							// 135
	198,636,							// 136
	199,642,							// 137
	200,648,							// 138
	201,654,							// 139
	203,661,							// 140
	204,669,							// 141
	205,677,							// 142
	206,684,							// 143
	208,691,							// 144
	209,698								// 145
};

const VOLTINFO fj_aVoltInfo[] =
{
	0,   0, NULL,
	22,  68, (WORD16(*)[])&awVoltTable70,
	22,  90, (WORD16(*)[])&awVoltTable90,
	40, 145, (WORD16(*)[])&awVoltTable150
};

/*
// the following structures are here just to document the chip select values of our board
typedef struct
{
	union								//	chip select 0 - ROM 4 meg at 0x02000000
	{
		WORD32 reg;						// csar	0200 0003
		struct
		{
			unsigned v        : 1;		// D00	1	valid
			unsigned wp       : 1;		// D01	1	write protected
			unsigned burst    : 1;		// D02	0	burst memory cycle enabled
			unsigned drse     : 1;		// D03	0	dram select disabled
			unsigned idle     : 1;		// D04	0	idle = 0
			unsigned dmuxm    : 1;		// D05	0	balanced ras/cas
			unsigned          : 1;		// D06	0
			unsigned dmuxs    : 1;		// D07	0	internal dram multiplexer
			unsigned dmode    : 2;		// D09:8	00	fast page dram
			unsigned          : 2;		// D11:10	00
			unsigned base     : 20;		// D12:31	02000	base address
		} bits;
	} csar;								// Basic structure of the Chip Select (Base) Address Register
	union
	{
		WORD32 reg;						// csor	F3C0 0302
		struct
		{
			unsigned wsync    : 1;		// D00	0	sram write cycle - asynch
			unsigned rsync    : 1;		// D01	1	sram read  cycle - synch
			unsigned ps       : 2;		// D02:03 00	32-bit port size
			unsigned bsize    : 2;		// D04:05	00	2 system bus cycles in burst access
			unsigned bcyc     : 2;		// D06:07	00	burst cycles are 1 clocks
			unsigned wait     : 4;		// D08:11	0011	3 wait states
			unsigned mask     : 20;		// D12:31	F3C00	mask address	4 meg
		} bits;
	} csor;								// Basic structure of the Chip Select Option Register
} cs_0;
typedef struct
{
	union								//	chip select 1 - RAM 16 meg at 0x00000000
	{
		WORD32 reg;						// csar	0000 022D
		struct
		{
			unsigned v        : 1;		// D00	1	valid
			unsigned wp       : 1;		// D01	0	not write protected
			unsigned burst    : 1;		// D02	1	burst memory cycle enabled
			unsigned drse     : 1;		// D03	1	dram select enabled
			unsigned idle     : 1;		// D04	0	idle = 0
			unsigned dmuxm    : 1;		// D05	1	unbalanced ras/cas
			unsigned          : 1;		// D06	0
			unsigned dmuxs    : 1;		// D07	0	internal dram multiplexer
			unsigned dmode    : 2;		// D09:8	10	synchronous dram
			unsigned          : 2;		// D11:10	00
			unsigned base     : 20;		// D12:31	00000	base address
		} bits;
	} csar;								// Basic structure of the Chip Select (Base) Address Register
	union
	{
		WORD32 reg;						// csor	F300 0070
		struct
		{
			unsigned wsync    : 1;		// D00	0	sram write cycle - asynch
			unsigned rsync    : 1;		// D01	0	sram read  cycle - asynch
			unsigned ps       : 2;		// D02:03 00	32-bit port size
			unsigned bsize    : 2;		// D04:05	11	16 system bus cycles in burst access
			unsigned bcyc     : 2;		// D06:07	01	burst cycles are 2 clocks
			unsigned wait     : 4;		// D08:11	0000	0 wait states
			unsigned mask     : 20;		// D12:31	F3000	mask address	16 meg
		} bits;
	} csor;								// Basic structure of the Chip Select Option Register
} cs_1;
typedef struct
{
	union								//	chip select 2 - battery backed up static VRAM 512k at 0x03800000
	{
		WORD32 reg;						// csar	0300 0003
		struct
		{
			unsigned v        : 1;		// D00	1	valid
			unsigned wp       : 1;		// D01	1	write protected
			unsigned burst    : 1;		// D02	0	burst memory cycle disabled
			unsigned drse     : 1;		// D03	0	dram select disabled
			unsigned idle     : 1;		// D04	0	idle = 0
			unsigned dmuxm    : 1;		// D05	0	balanced ras/cas
			unsigned          : 1;		// D06	0
			unsigned dmuxs    : 1;		// D07	0	internal dram multiplexer
			unsigned dmode    : 2;		// D09:8	00	fast page dram
			unsigned          : 2;		// D11:10	00
			unsigned base     : 20;		// D12:31	03000	base address
		} bits;
	} csar;								// Basic structure of the Chip Select (Base) Address Register
	union
	{
		WORD32 reg;						// csor	FFFF CA0A
		struct
		{
			unsigned wsync    : 1;		// D00	0	sram write cycle - asynch
			unsigned rsync    : 1;		// D01	1	sram read  cycle - synch
			unsigned ps       : 2;		// D02:03 10	8-bit port size
			unsigned bsize    : 2;		// D04:05	00	2 system bus cycles in burst access
			unsigned bcyc     : 2;		// D06:07	00	burst cycles are 1 clocks
			unsigned wait     : 4;		// D08:11	1010	10 wait states
			unsigned mask     : 20;		// D12:31	FFFFC	mask address	16 k
		} bits;
	} csor;								// Basic structure of the Chip Select Option Register
} cs_3;
typedef struct
{
	union								//	chip select 4 - Gate Array 4k at 0x08000000 ( 256 bytes decoded )
	{
		WORD32 reg;						// csar	0300 0001
		struct
		{
			unsigned v        : 1;		// D00	1	valid
			unsigned wp       : 1;		// D01	0	not write protected
			unsigned burst    : 1;		// D02	0	burst memory cycle disabled
			unsigned drse     : 1;		// D03	0	dram select disabled
			unsigned idle     : 1;		// D04	0	idle = 0
			unsigned dmuxm    : 1;		// D05	0	balanced ras/cas
			unsigned          : 1;		// D06	0
			unsigned dmuxs    : 1;		// D07	0	internal dram multiplexer
			unsigned dmode    : 2;		// D09:8	00	fast page dram
			unsigned          : 2;		// D11:10	00
			unsigned base     : 20;		// D12:31	03000	base address
		} bits;
	} csar;								// Basic structure of the Chip Select (Base) Address Register
	union
	{
		WORD32 reg;						// csor	FFFF F108
		struct
		{
			unsigned wsync    : 1;		// D00	0	sram write cycle - asynch
			unsigned rsync    : 1;		// D01	0	sram read  cycle - asynch
			unsigned ps       : 2;		// D02:03 10	8-bit port size
			unsigned bsize    : 2;		// D04:05	00	0 system bus cycles in burst access
			unsigned bcyc     : 2;		// D06:07	00	burst cycles are 1 clocks
			unsigned wait     : 4;		// D08:11	0001	1 wait state
			unsigned mask     : 20;		// D12:31	FFFFF	mask address	4 k
		} bits;
	} csor;								// Basic structure of the Chip Select Option Register
} cs_4;
*/
