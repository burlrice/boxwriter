#ifndef FJ_MEM_H_
#define FJ_MEM_H_

#include "fj_base.h"

#ifdef  __cplusplus
extern "C"
{
#endif
	FJDLLExport void *  fj_malloc( long lSize );
	FJDLLExport void *  fj_calloc( long lCount, long lSize );
	FJDLLExport void    fj_free( void * pBuffer );
#ifdef  __cplusplus
}
#endif

#endif /*FJ_MEM_H_*/
