#ifndef FJ_MEM
#define FJ_MEM

#include "fj_base.h"

#ifdef  __cplusplus
extern "C"
{
#endif
	FJDLLExport VOID*  fj_malloc( LONG lSize );
	FJDLLExport VOID*  fj_calloc( LONG lCount, LONG lSize );
	FJDLLExport VOID   fj_free( VOID* pBuffer );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_MEM
