/*
 *            Copyright (c) 1998-2000 by NETsilicon Inc.
 *
 *  This software is copyrighted by and is the sole property of
 *  NETsilicon.  All rights, title, ownership, or other interests
 *  in the software remain the property of NETsilicon.  This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of NETsilicon.
 *
 *  NETsilicon, reserves the right to modify this software
 *  without notice.
 *
 *  NETsilicon
 *  411 Waverley Oaks Road                  USA 781.647.1234
 *  Suite 227                               http://www.netsilicon.com
 *  Waltham, MA 02452                       AmericaSales@netsilicon.com
 *
 *************************************************************************
 *
 *     Module Name: ftpsvr.c
 *         Version: 1.00
 *   Original Date: 11-Feb-12:13
 *          Author:
 *        Language: Ansi C
 * Compile Options:
 * Compile defines:
 *       Libraries:
 *    Link Options:
 *
 *    Entry Points:
 *
 * Description.
 * =======================================================================
 * This module implements the NET+ARM FTP server along with
 * the application programming interface.
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 *
 *
 */

/*
 * TO DO:
 *
 * Add API to set local port start for client connections.
 * Add API to retrieve the sequence number.
 * Add API for setting child task stack sizes.
 * Add API for setting the number of children.
 * Add code for saving errors and an API to access the errors.
 * Add call to determine send length and receive length based upon ethernet settings
 *     for 10 base T or 100 base T.
 *
 */

#define CHG_092998

#ifdef TRUE
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#define FALSE 0
#define TRUE (!(FALSE))

//#include <rtos.h>
#ifdef PSOS_OS
#include <psos.h>
#include <pna.h>
#include <prepc.h>
#endif
#ifdef VXWORKS_OS
#include <vxWorks.h>
#include <taskLib.h>
#include <sysLib.h>
#include <string.h>
#include <sockLib.h>
#include <inetLib.h>
#include <ioLib.h>
#include <netinet\tcp.h>
#include <ctype.h>
#endif
#ifdef NET_OS
#include <tx_api.h>
#include <pthread.h>
#include "sockapi.h"
#endif

#include <stdio.h>
#include "narmapi.h"
#include "fservapi.h"
#include "ftpsvr.h"

#ifndef NET_OS
#define socketclose close
int getErrno()  {return errno;}
#endif

/*#define _DEBUG_FTP */

/*
 * External Declarations
 */

extern unsigned long NAIpAddress ();
extern int naftp_start_timer ();
extern int naftp_check_timer ();
extern UINT bsp_default_api_priority;

/*
 * Global Variables
 */

char *g_ftpsvrp = __FILE__;

ftp_config_info_t *g_ftp_configp = NULL;

struct sockaddr_in gftp_address;
struct sockaddr_in gftp_data;
struct sockaddr_in gftp_socket;
int g_ftp_server;

int use_write_mask=0;

/*
 * Prototypes
 */
void naftp_client_start(int);
void naftp_record_error(int, char *, unsigned long, unsigned long);
int  naftp_init_socket(int, int, struct sockaddr_in *, int);
void na_init_sockaddr(struct sockaddr_in *, int, unsigned long);

/*
 * NAFTP_server
 *
 * This routine is the initial entry point for the NET+ARM ftp server.
 *
 */

void
NAFTP_server()
{
	int buffsize;
	int err;
	int one = 1, two = 2;
	int on = TRUE;
	struct sockaddr_in peer_addr;
	int peer_len = sizeof(peer_addr);
	int client_socket;
	int userindex;
	fd_set read_mask;
	fd_set except_mask;
	fd_set write_mask;
	int width;
	struct timeval servertime;
	struct linger test_linger;
	int optval = 0;
	/*
	 * Start timer for FTP connection timeouts.
	 */

	if (naftp_start_timer ())
	{
		return;
	}

	/* Initialize the servers socket */

	na_init_sockaddr (&gftp_address, FTP_SERVER_PORT, 0);
	g_ftp_server = naftp_init_socket (4096, 1400, &gftp_address, 1);

	if (g_ftp_server < 0)
	{
		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKET_ERR, getErrno());
		return;
	}

	/* Set to non-blocking since select is being used. */

#ifdef PSOS_OS
	if (ioctl(g_ftp_server, FIONBIO, (char *)&on) == -1)
#endif
#ifdef VXWORKS_OS
		if (ioctl(g_ftp_server, FIONBIO,on) == -1)
#endif
#ifdef NET_OS
			if (setsockopt(g_ftp_server, SOL_SOCKET, SO_NBIO, NULL,0) == -1)
#endif
	{
		/* Log error for API. */

		naftp_record_error (__LINE__, g_ftpsvrp, NA_IOCTL_ERR, getErrno());

		return;
	}

	listen(g_ftp_server, 10);

	for (;;)
	{
		servertime.tv_sec = g_ftp_configp->m_waittime;

		if (0 == servertime.tv_sec)
		{
			/* Don't allow the FTP server to consume the CPU. */

			servertime.tv_usec = 100;
		} else
		{
			servertime.tv_usec = 0;
		}

		use_write_mask = 0;

		width = naftp_init_select (&read_mask, &except_mask, &write_mask);

		/*
		 * Wait for incomming messages, we need to set the write mask
		 * if were waiting for a connection to finish.
		 */
		if (use_write_mask)
		{
			err = select (FD_SETSIZE, (fd_set *)&read_mask,
				(fd_set *)&write_mask, (fd_set *)&except_mask,
				&servertime);
		}
		else
		{
			err = select (FD_SETSIZE, (fd_set *)&read_mask,
				(fd_set *)0, (fd_set *)&except_mask,
				&servertime);
		}

		if (0 == err)
		{
#ifdef _DEBUG_FTP
			printf("FTP: select timeout\n");
#endif

			/* 
			 * Timeout condition, continue... Look for client connection
			 * timeouts.
			 */

			naftp_check_timer ();

			/* 
			 * A data connection could have set the wait time to 0. This
			 * makes sure the wait time is restored to the original setting
			 * if there are no connections.
			 */

			if (0 == naftp_data_connections())
			{
				g_ftp_configp->m_waittime = g_ftp_configp->m_waitorg;
			}

			naftp_resolve_user (&read_mask, &except_mask, &write_mask);

			continue;
		}else
		if (-1 == err)
		{
#ifdef _DEBUG_FTP
			printf("FTP: select error errno=[%lx]\n", getErrno());
#endif
			continue;
		} else
		{
			if (FD_ISSET(g_ftp_server, &read_mask))
			{
				/* Got a request on the FTP servers socket */

#if defined PSOS_OS || defined NET_OS
				client_socket = accept (g_ftp_server, &peer_addr, &peer_len);
#endif
#ifdef VXWORKS_OS
				client_socket = accept (g_ftp_server,(struct sockaddr *)&peer_addr, &peer_len);
#endif
				if (client_socket < 0)
				{
#ifdef _DEBUG_FTP
					printf("FTP: accept error errno = [%lx]\n", getErrno());
#endif
					continue;
				}

#ifdef _DEBUG_FTP
				printf("FTP: accepted [%d]\n", client_socket);
#endif
#ifdef PSOS_OS
				if (setsockopt (client_socket, IPPROTO_TCP, TCP_MSL,
					(char *)&one, sizeof (one)) == -1)
				{
					socketclose (client_socket);
					naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());
				}
#endif

				/* 
				 * Get a ftp_user_info_t data structure to store client info
				 */
				userindex = naftp_prepare_user(client_socket);

				if (userindex < 0)
				{
#ifdef _DEBUG_FTP
					printf("FTP: naftp_prepare_user failed\n");
#endif
					if (socketclose(client_socket) < 0)
					{
#ifdef _DEBUG_FTP
						printf("FTP: socketclose failed [%lx]\n");
#else
						;
#endif
					}

					/* TO DO add stats category */
					continue;
				}

				/*
				 * Send back a successfull reply and parse response.
				 */
				naftp_client_start (userindex);

			} else
			{
				/* A user connection requires servicing. */
				naftp_resolve_user (&read_mask, &except_mask, &write_mask);
			}

			/* Look for client connection timeouts. */

			naftp_check_timer ();
		}
	}
}
/*
 * naftp_client_start
 *
 * This routine starts a client task to handle the connection specified
 * by the socket parameter.
 *
 */

void
naftp_client_start (int userindex)
{
	ftp_user_info_t *userp;

	userp = &g_ftp_configp->m_usersp[userindex];

	naftp_send_reply (userp->cntlsocket, 220, "NET+ARM FTP Server 1.0 ready.");

	naftp_parse (userindex);
}
/*
 * naftp_record_error
 *
 * This routine saves an error in the global config structure.
 *
 * INPUT
 *
 *    line       Specifies line number where error occurred.
 *    file       Specifies file name where the error occurred.
 *    naerr      NET+ARM FTP server error code.
 *    oserror    OS SPECIFIC error code.
 *
 */
void
naftp_record_error (int line, char *file, unsigned long naerr, unsigned long oserror)
{
#ifdef _DEBUG_FTP
	printf("FTP: errno = [%lx]\n", oserror);
	printf("FTP: file  = [%s]\n",  file);
	printf("FTP: line  = [%d]\n",  line);
#endif
#ifndef VXWORKS_OS
	NAErrRecord (g_ftp_configp->m_lasterror, line, file, naerr, oserror);
#endif
}
/*
 * na_init_sockaddr
 *
 * This routine initializes a sockaddr_in structure.
 *
 * INPUTS
 *
 *    port   Specifies value that sin_port is initialized to.
 *    ipaddr Specifies value that sin_addr is initialized to.
 *
 */

void
na_init_sockaddr (struct sockaddr_in *addressp, int port, unsigned long ipaddr)
{
	memset ((char *)addressp, 0, sizeof (struct sockaddr_in));

	addressp->sin_family = AF_INET;
	addressp->sin_port   = htons (port);
	ipaddr = htonl (ipaddr);
	memcpy ((char *)&addressp->sin_addr, (char *)&ipaddr, sizeof (unsigned long));
}
/*
 * naftp_init_config
 *
 * This routine initializes the FTP server configuration prior to it being
 * started. This initialization is done just before the FTP server task
 * is started and it is assumed that FSProperties has already been called.
 *
 * RETURNS
 *
 *     -1   If the initialization failed due to lack of memory.
 *
 */

int
naftp_init_config (int numusers)
{
	int i;

	g_ftp_configp = (ftp_config_info_t *)malloc (sizeof (ftp_config_info_t));
	if (NULL == g_ftp_configp)
		return -1;

	memset ((char *)g_ftp_configp, 0, sizeof (ftp_config_info_t));

#ifndef VXWORKS_OS
	g_ftp_configp->m_lasterror = NAErrCreate ();
	if (0 == g_ftp_configp->m_lasterror)
	{
#ifdef _DEBUG_FTP
		printf("FTP: initialization error\n");
#endif
		free (g_ftp_configp);
		g_ftp_configp = NULL;
		return -1;
	}
#endif
	g_ftp_configp->m_datap = (char *)malloc (2 * FTP_PI_BUF_SIZE);
	if (NULL == g_ftp_configp->m_datap)
	{
#ifdef _DEBUG_FTP
		printf("FTP: initialization error\n");
#endif
		free (g_ftp_configp);
		g_ftp_configp = NULL;
		return -1;
	}

	g_ftp_configp->m_size = 2 * FTP_PI_BUF_SIZE;

	memset (g_ftp_configp->m_datap, 0, 2 * FTP_PI_BUF_SIZE);

	strcpy (g_ftp_configp->m_name, "FSV");
	g_ftp_configp->m_majversion           = 1;
	g_ftp_configp->m_minversion           = 0;
	g_ftp_configp->m_tid                  = -1;
	g_ftp_configp->m_waitorg              = 2;
	g_ftp_configp->m_waittime             = 2;
	g_ftp_configp->m_cltimeout            = 6000;
	g_ftp_configp->m_maxusers             = numusers;
#ifdef NET_OS
	g_ftp_configp->m_priority             = bsp_default_api_priority;
#else
	g_ftp_configp->m_priority             = LIB_PRIO;
#endif
	g_ftp_configp->m_sysstack             = 4096;
	g_ftp_configp->m_usrstack             = 2048;
	g_ftp_configp->m_mode                 = 0x2000;
	g_ftp_configp->m_localport            = 6000;

	g_ftp_configp->m_usertablep = (ftp_user_table_t *)
		malloc (sizeof (ftp_user_table_t) * g_ftp_configp->m_maxusers);

	if (NULL == g_ftp_configp->m_usertablep)
	{
#ifdef _DEBUG_FTP
		printf("FTP: initialization error\n");
#endif
		free (g_ftp_configp->m_datap);
		free (g_ftp_configp);
		g_ftp_configp = NULL;
		return -1;
	}

	memset (g_ftp_configp->m_usertablep, 0, sizeof (ftp_user_table_t) * g_ftp_configp->m_maxusers);

	g_ftp_configp->m_usersp = (ftp_user_info_t *)
		malloc (sizeof (ftp_user_info_t) * g_ftp_configp->m_maxusers);

	if (NULL == g_ftp_configp->m_usersp)
	{
#ifdef _DEBUG_FTP
		printf("FTP: initialization error\n");
#endif
		free (g_ftp_configp->m_usertablep);
		return -1;
	}

	memset (g_ftp_configp->m_usersp, 0, sizeof (ftp_user_info_t) * g_ftp_configp->m_maxusers);

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		g_ftp_configp->m_usersp[i].cntlsocket = -1;
		g_ftp_configp->m_usersp[i].datasocket = -1;
		g_ftp_configp->m_usersp[i].active = 1;
		g_ftp_configp->m_usersp[i].dataport = 0;
		g_ftp_configp->m_usersp[i].localport = htons(g_ftp_configp->m_localport+i);
	}

	return 0;
}
/*
 * naftp_init_socket
 *
 * This routine creates a new socket sets the appropriate socket options
 * and binds the socket.
 *
 * INPUT
 *
 *    sendsize     Specifies SNDBUF socket option size.
 *    recvsize     Specifies RECVBUF socket option size.
 *    addressp     Pointer to sockaddr_in data structure. This parameter is used
 *                     for output.
 *    isserver     1 for a server socket, 0 for a client or single connect socket.
 *
 * RETURNS
 *
 *    <0           An error occured.
 *    Other        New socket.
 *
 */

int
naftp_init_socket (int sendsize, int recvsize, struct sockaddr_in *addressp, int isserver)
{
	int newsocket;
	int buffsize;
	int err, i;
	int one = 1, two = 2;
	struct linger closeopt;
	static int count=0;

	closeopt.l_onoff = 0;
#ifdef _DEBUG_FTP
	printf("FTP: opening socket\n");
#endif
	newsocket = socket (AF_INET, SOCK_STREAM, 0);
#ifdef _DEBUG_FTP
	printf("FTP:socket opened\n");
#endif
	if (newsocket < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: Coulndn't open socket\n");
#endif
		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKET_ERR, getErrno());
		return -1;
	}

#ifdef _DEBUG_FTP
	printf("FTP: newsocket = [%d]\n", newsocket);
#endif

	buffsize = sendsize;				/* 1300 */
	err = setsockopt(newsocket, SOL_SOCKET, SO_SNDBUF, (char *)&buffsize,
		sizeof(buffsize));
	if (err == -1)
	{
		/* Log error for API. */
		socketclose (newsocket);

		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());

		return -2;
	}

	buffsize = recvsize;
	err = setsockopt(newsocket, SOL_SOCKET, SO_RCVBUF, (char *)&buffsize,
		sizeof(buffsize));
	if (err == -1)
	{
		/* Log error for API. */
		socketclose (newsocket);
		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());
		return -3;
	}

	if (isserver)
	{
		if (setsockopt(newsocket, SOL_SOCKET,SO_REUSEADDR,
			(char *)&one,sizeof one) == -1)
		{
			/* Log error for API. */

			if (socketclose (newsocket) < 0)
			{
#ifdef _DEBUG_FTP
				printf("FTP: socketclose failed [%lx]\n", getErrno());
#else
				;
#endif
			}

			naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());

			return -4;
		}
	}

#if defined PSOS_OS || defined NET_OS
	if (bind (newsocket, addressp, sizeof (struct sockaddr_in)) == -1)
#endif
#ifdef VXWORKS_OS
		if (bind (newsocket,(struct sockaddr *)addressp, sizeof (struct sockaddr)) == -1)
#endif
	{
		/* Log error for API. */
		if (socketclose (newsocket) < 0)
		{
#ifdef _DEBUG_FTP
			printf("FTP: socketclose failed [%lx]\n", getErrno());
#else
			;
#endif
		}

		naftp_record_error (__LINE__, g_ftpsvrp, NA_BIND_ERR, getErrno());
		return -5;
	}

	if (setsockopt (newsocket, SOL_SOCKET, SO_LINGER,
		(char *)&closeopt, sizeof (closeopt)) == -1)
	{
		socketclose (newsocket);
		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());
		return -6;
	}

	/*
	 * Set the socketclose wait state to two seconds..this is the time the socket
	 * hangs around after calling socketclose.
	 */
#ifdef PSOS
	if (setsockopt (newsocket, IPPROTO_TCP, TCP_MSL,
		(char *)&one, sizeof (one)) == -1)
	{
		socketclose (newsocket);
		naftp_record_error (__LINE__, g_ftpsvrp, NA_SOCKOPT_ERR, getErrno());
		return -7;
	}
#endif
	return newsocket;
}
/* 
 * naftp_send_reply
 *
 * Sends FTP reply codes to the FTP client.
 *
 * INPUT
 *
 *    fd      Specifies socket to send the reply.
 *    code    Specifies FTP protocol numeric code.
 *    info    Specifies FTP information string.
 *
 * RETURNS
 *
 *    >0      Specifies number of bytes sent.
 *    -1      Specifies an error took place.
 */

int
naftp_send_reply(int fd, int code, char *info)
{
	char sndbuf[256];
	int size, ret;

	size = sprintf(sndbuf, "%d %s\r\n", code, info);

	ret = send (fd, sndbuf, size, 0);

	if (ret < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: send failed errno = [%lx]\n", getErrno());
#endif
		return -1;
	}

	return ret;
}
/*
 * naftp_split_command
 *
 * This routine splits a buffer into the ftp command and the
 * arguments for the command.
 *
 * INPUTS:
 *
 *    ftp_cmd_bufp    On input points to the entire ftp command line.
 *                    On output points to the ftp command only.
 *    ftp_argp        On output points to the ftp arguments only.
 *    readlen         On input specifies length of entire ftp command line.
 *
 * RETURNS:
 *
 *    NULL            The command could not be split properly
 *    char *          Points to parameter start.
 *
 */

char *
naftp_split_command (char *ftp_cmd_bufp, int readlen)
{
	int n = 0;
	char *ftp_argp;

	/* Split the buffer into command and args. */

	while(isalpha(ftp_cmd_bufp[n]) && n < readlen)
	{
		ftp_cmd_bufp[n] = tolower(ftp_cmd_bufp[n]);
		n++;
	}

	if (!n)
	{
		return NULL;
	}

	while(isspace(ftp_cmd_bufp[n]) && n < readlen)
		ftp_cmd_bufp[n++] = '\0';

	if (n != readlen)
		ftp_argp = ftp_cmd_bufp+n;

	while(ftp_cmd_bufp[n] && n<readlen)
	{
		//        if (isalpha(ftp_cmd_bufp[n]))								// FOXJET
		//            ftp_cmd_bufp[n] = tolower(ftp_cmd_bufp[n]);			// FOXJET
		n++;
	}

	ftp_cmd_bufp[n] = '\0';

	n--;

	while(isspace(ftp_cmd_bufp[n]))
		ftp_cmd_bufp[n--] = '\0';

	/* Make sure the parameters will fit into the user structure. */

	if (strlen (ftp_argp) > FTP_MAXARGLEN-1)
	{
		ftp_argp[FTP_MAXARGLEN-1] = '\0';
	}

	return ftp_argp;
}
/*
 * naftp_reset_datasock
 *
 * This routine is used to socketclose and re-initialize the datasocket section
 * of a client connection.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 */

void
naftp_reset_datasock (ftp_user_info_t *userp)
{
	if (userp->datasocket == -1)
	{
		return;
	}

#ifdef _DEBUG_FTP
	printf("FTP: closing data [%d]\n", userp->datasocket);
#endif

	if (shutdown(userp->datasocket, 2) < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: shutdown failed [%lx]\n", getErrno());
#else
		;
#endif
	}
#ifdef PSOS_OS
	tm_wkafter(15);
#endif
#ifdef VXWORKS_OS
	/*taskDelay(15);*/
#endif
	if (socketclose (userp->datasocket) < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: socketclose failed [%lx]\n", getErrno());
#else
		;
#endif
	}

	/* Do not re-initialize the port or ip address since they can be re-used. */

	userp->connected  = 0;
	userp->datasocket = -1;
	userp->cntlstate = FTP_IDLE;
	userp->sequenceno = 0;
#ifdef CHG_092998
	if (g_ftp_configp->m_usr_dresetp)	/* if user registered socketclose function*/
	{									/* then call it*/
		g_ftp_configp->m_usr_dresetp(userp->username, (unsigned long)userp);
	}
#endif
}
/*
 * naftp_reset_cntlsock
 *
 * This routine is called to socketclose and re-initialize a user structure
 * previously allocated to a client connection.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 */

void
naftp_reset_cntlsock (ftp_user_info_t *userp)
{
	if (-1 == userp->cntlsocket)
		return;

#ifdef _DEBUG_FTP
	printf("FTP: closing cntl [%d]\n", userp->cntlsocket);
#endif

	naftp_reset_datasock (userp);

	if (shutdown (userp->cntlsocket, 2) < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: shutdown failed [%lx]\n", getErrno());
#else
		;
#endif
	}

	if (socketclose (userp->cntlsocket) < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP: socketclose failed [%lx]\n", getErrno());
#else
		;
#endif
	}

	if (g_ftp_configp->m_usr_cresetp)
	{
		(*g_ftp_configp->m_usr_cresetp)(userp->username, (unsigned long)userp);
	}

	userp->cntlsocket = -1;
	userp->state = FTP_IDLE;
	userp->username[0] = '\0';
	userp->password[0] = '\0';

	g_ftp_configp->m_curusers--;
}
/*
 * naftp_graceful_exit
 *
 * This routine is called to disconnect a client connection.
 * The exit code of 221 is delivered prior to closing the connection.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 */

void
naftp_graceful_exit (ftp_user_info_t *userp)
{
	naftp_send_reply(userp->cntlsocket, 221, "Goodbye.");

	naftp_reset_cntlsock (userp);
}
/*
 * naftp_process_user
 *
 * This routine is called in response to the "user" command delivered by
 * the FTP client.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *
 * RETURNS
 *
 *    0         User name is valid.
 *   -1         User name is NOT valid.
 *
 */

int
naftp_process_user (ftp_user_info_t *userp, char *argp)
{
	int i;
	char buffer[256];
	char username[FS_MAXUSERLEN];
	char password[FS_MAXUSERLEN];

	if (g_ftp_configp->m_usr_validationp)
	{
		if ((*g_ftp_configp->m_usr_validationp)(argp, password) == 0)
		{
			userp->state = FTP_PROCESS_CMD;
			strcpy (userp->username, argp);
			strcpy (userp->password, password);
		} else
		{
			sprintf (buffer, "The user name is not valid.");
			naftp_send_reply(userp->cntlsocket, 530, buffer);
			return -1;
		}
	}

	/* 
	 * It is possible to not have a username or password.
	 */

	if (strlen (userp->username) == 0 && argp != NULL && strcmp (argp, "(none)") != 0)
	{
		sprintf (buffer, "The user name is not valid.");
		naftp_send_reply(userp->cntlsocket, 530, buffer);
		return -1;
	} else
	if (strlen (userp->username) == 0)
	{
		strcpy (username, "none");
	} else
	{
		strcpy (username, userp->username);
	}

	if (strlen (userp->password) == 0)
	{
		userp->state = FTP_PROCESS_CMD;
		sprintf (buffer, "User %s logged in.", username);
		naftp_send_reply(userp->cntlsocket, 230, buffer);

	} else
	{
		userp->state = FTP_PASSWORD;
		sprintf (buffer, "User %s OK, send password.", username);
		naftp_send_reply(userp->cntlsocket, 331, buffer);
	}

	return 0;
}
/*
 * naftp_process_password
 *
 * This routine is called in response to the "pass" command delivered by
 * an FTP client. The application supplied password is checked against the
 * password contained within the command. The appropriate reply is sent based
 * upon the comparison results.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *
 */

int
naftp_process_password (ftp_user_info_t *userp, char *argp)
{
	if (strcmp (userp->password, argp) == 0)
	{
		userp->state = FTP_PROCESS_CMD;
		naftp_send_reply(userp->cntlsocket, 230, "Password OK.");
		return 0;
	} else
	{
		userp->state = FTP_IDLE;
		naftp_send_reply(userp->cntlsocket, 332, "Invalid password.");
		return -1;
	}
}
/*
 * naftp_process_port
 *
 * This routine is called in response to the "port" command delivered by
 * an FTP client. The application supplied port information is stored
 * in the user structure to be used for data related commands.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *
 * RETURNS
 *
 *    0         Port command was processed successfully.
 *   -1         The port command syntax could not be recognized.
 *
 */

int
naftp_process_port (ftp_user_info_t *userp, char *argp)
{
	unsigned int a1, a2, a3, a4, p1, p2;

	if (argp &&
		6 == sscanf(argp, "%d,%d,%d,%d,%d,%d",&a1, &a2, &a3, &a4, &p1, &p2) &&
		a1<256 && a2<256 && a3<256 && a4<256 && p1<256 && p2<256 &&
		a1 != 127 && a1 != 0 )
	{
#ifdef VXWORKS_OS
		userp->dataport = ((p1<<8)+p2);
		userp->data_ip_addr = ((a1<<24) + (a2<<16) + (a3<<8) + a4);;
#else
		userp->dataport = htons((p1<<8)+p2);
		userp->data_ip_addr = htonl((a1<<24) + (a2<<16) + (a3<<8) + a4);;
#endif

		naftp_send_reply(userp->cntlsocket, 200, "PORT command Ok.");

		userp->active = 1;

		return 0;
	}
	else
	{
		naftp_send_reply(userp->cntlsocket, 501, "PORT syntax error");
		return -1;
	}

	return 0;
}
/*
 * naftp_process_pasv
 *
 * This routine is called in response to the "pasv" command delivered by an
 * FTP client. This routine sets the current userp structure into passive mode
 * and opens a socket to be listened on.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *
 * RETURNS
 *
 *   -1         An error occurred.
 *    0         The command was processed successfully.
 *
 */

int
naftp_process_pasv (ftp_user_info_t *userp, char *argp)
{
	char portinfo[64], *a, *p;
	struct sockaddr_in address;
	struct sockaddr_in socketinfo;
	int length = sizeof (struct sockaddr_in);
	int one = 1, two=2;
	int on = TRUE;

#ifdef _DEBUG_FTP
	printf("FTP: opening data socket\n");
#endif

	na_init_sockaddr (&address, userp->dataport, NAIpAddress());

	userp->datasocket = naftp_init_socket (4096, 1024, &address, 0);
	if (userp->datasocket < 0)
	{
		/* TO DO tear down user. */
		naftp_send_reply (userp->cntlsocket, 502, "Command not implemented.");
		return -1;
	}

#ifdef PSOS_OS
	if (ioctl(userp->datasocket, FIONBIO, (char *)&on) == -1)
#endif
#ifdef VXWORKS_OS
		if (ioctl(userp->datasocket, FIONBIO, on) == -1)
#endif
#ifdef NET_OS
			if (setsockopt(userp->datasocket, SOL_SOCKET, SO_NBIO, NULL,0) == -1)
#endif
	{
		/* TO DO tear down user. */
		naftp_send_reply (userp->cntlsocket, 502, "Command not implemented.");
		return -1;
	}

#if defined PSOS_OS || defined NET_OS
	if (getsockname (userp->datasocket, &address, &length) < 0)
#endif
#ifdef VXWORKS_OS
		if (getsockname (userp->datasocket,(struct sockaddr *)&address, &length) < 0)
#endif
	{
		naftp_send_reply (userp->cntlsocket, 502, "Command not implemented.");
		return -1;
	}

#if 0
	setsockopt(userp->datasocket, IPPROTO_TCP, TCP_NODELAY,
		(char *)&one, sizeof (one));
#endif

	a = (char *)&(address.sin_addr);
	p = (char *)&(address.sin_port);

	sprintf(portinfo, "PORT %d,%d,%d,%d,%d,%d",
		UC(a[0]), UC(a[1]), UC(a[2]), UC(a[3]),
		UC(p[0]), UC(p[1]));

	userp->active = 0;
	userp->state = FTP_PASV_DATA;
	naftp_send_reply (userp->cntlsocket, 227, portinfo);

	if (listen (userp->datasocket, 2) < 0)
	{
#ifdef _DEBUG_FTP
		printf("FTP listen failed\n");
#else
		;
#endif

	}

	/* naftp_send_reply(userp->cntlsocket, 150, "Waiting for data connection.");    	*/

	return 0;
}
/*
 * naftp_user_data
 *
 * This routine calls the user's routine at the start of processing
 * a data request. A data request is defined as a command that requires
 * the FTP server to send back data to the client.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *    cmd       FTP command type as defined ftpsvr.h.
 *
 */

void
naftp_user_data (ftp_user_info_t *userp, int cmd, char *argp)
{
	userp->cntlstate = cmd;
	switch (cmd)
	{
		case FTP_CMD_STOR:
			break;

		case FTP_CMD_RETR:
		case FTP_CMD_LIST:
		case FTP_CMD_NLST:

			/* Set the wait time to zero since a retrieve depends upon the
			   server supplying data back to the client. */

			g_ftp_configp->m_waittime = 0;

			break;

		case FTP_CMD_CWD:
			naftp_cmd_cwd (userp);
			break;

		case FTP_CMD_SYST:

			naftp_cmd_syst (userp);
			break;

		case FTP_CMD_PWD:
			naftp_cmd_pwd (userp);
			break;
	}
}
/*
 * naftp_process_data
 *
 * This routine is called when ever a command is received that requires data
 * to be sent. This includes the STOR, RETR, and LST commands.
 *
 * INPUT
 *
 *    userp     Points to the current user data structure.
 *    argp      Points to parameters for the "user" command.
 *    cmd       FTP command type as defined ftpsvr.h.
 *
 * RETURN
 *
 *    -1        An error occurred while processing the command.
 *     0        The command was processed successfully.
 */

int
naftp_process_data (ftp_user_info_t *userp, char *argp, int cmd)
{
	struct sockaddr_in address;
	struct sockaddr_in remote;
	int on = TRUE;
	int one = 1, two=2;
	int err = 0;

	if (userp->state != FTP_PROCESS_CMD)
	{
		return -1;
	}

	/* Some commands use a data connection others use the control connection. */

	if (FTP_CMD_STOR == cmd || FTP_CMD_RETR == cmd ||
		FTP_CMD_LIST == cmd || FTP_CMD_NLST == cmd)
	{
		if (userp->active)
		{
			/* Have the OS supply an unused port number. */

			na_init_sockaddr (&address, 0, 0);
			userp->localport = address.sin_port;

			/* Multiple clients may use port 20 for the data connection.
			   So set the server flag in naftp_init_socket. */

			userp->datasocket = naftp_init_socket (4096, 1024, &address, 1);
			if (userp->datasocket < 0)
			{
				naftp_graceful_exit (userp);

				return -1;
			}

			/* Set non-blocking, issue connect. */

#ifdef PSOS_OS
			if (ioctl(userp->datasocket, FIONBIO, (char *)&on) == -1)
#endif
#ifdef VXWORKS_OS
				if (ioctl(userp->datasocket, FIONBIO, on) == -1)
#endif
#ifdef NET_OS
					if (setsockopt(userp->datasocket, SOL_SOCKET, SO_NBIO, NULL,0) == -1)
#endif
			{
#ifdef _DEBUG_FTP
				printf("FTP ioctl FIONBIO failed\n");
#endif
				naftp_graceful_exit (userp);

				return -1;
			}
#ifndef NET_OS

			setsockopt (userp->datasocket, IPPROTO_TCP, TCP_NODELAY,
				(char *)&one, sizeof (one));
#else
			setsockopt(userp->datasocket, SOL_SOCKET, SO_NBIO, NULL,0);
#endif

			na_init_sockaddr (&remote, userp->dataport, userp->data_ip_addr);

			naftp_send_reply(userp->cntlsocket, 150, "About to open data connection.");

			/* 
			 * Issue the connect here...were in nonblocking mode so that
			 * the connect will fail here...we need to wait for the response
			 * on select.
			 */

			/*   printf("FTP: connecting [%d]\n", userp->datasocket); */

#if defined PSOS_OS || defined NET_OS
			err = connect (userp->datasocket, &remote, sizeof (remote));
#endif
#ifdef VXWORKS_OS
			err = connect (userp->datasocket,(struct sockaddr *)&remote, sizeof (remote));
#endif

			if (-1 == err)
			{
				err = getErrno();
				if (err  != EINPROGRESS)
				{
					naftp_graceful_exit (userp);
					return -1;
				}
				userp->connected = 0;
			}

		} else
		{
			/* Issue listen and wait for connection attempt to arrive. */

			if (FTP_PASV_DATA == userp->state)
			{
				/* naftp_send_reply(userp->cntlsocket, 150, "Waiting for data connection.");    	*/

				if (listen (userp->datasocket, 2) < 0)
				{
#ifdef _DEBUG_FTP
					printf("FTP: listen fails [%lx]\n", getErrno());
#else
					;
#endif
				}
			}
		}
		strcpy (userp->curarg, argp);

		if (userp->connected == 0)
		{
			/* save the command until where connected */
			userp->cmd = cmd;
		}
		else
		{
			naftp_user_data (userp, cmd, argp);
		}
	} else								/* commands that the control connection. */
	{
		strcpy (userp->curarg, argp);
		naftp_user_data (userp, cmd, argp);
	}
	return 0;
}
/*
 * naftp_parse
 *
 * This routine parses an FTP command by first reading a command, splitting it
 * into command and parameter parts and then dispatching to a handler for the
 * command.
 *
 * INPUT
 *
 *    client_socket     Specifies FTP control socket.
 *
 * RETURNS
 *
 *    0 for success
 *   -1 for error
 *
 */

int
naftp_parse (int userindex)
{
	char ftp_cmd_buf[FTP_PI_BUF_SIZE];
	char *ftp_argp = NULL;
	int readlen;
	ftp_user_info_t *userp;

	userp = &g_ftp_configp->m_usersp[userindex];

	readlen = recv (userp->cntlsocket, ftp_cmd_buf, FTP_PI_BUF_SIZE, 0);
	if (readlen < 0)
	{
		return -1;
	}
	else if (readlen == 0)				/* probably got a reset */
	{
#ifdef _DEBUG_FTP
		printf("FTP: readlen == 0.....\n");
#endif
		/*naftp_reset_datasock (userp);*/
		naftp_reset_cntlsock (userp);
		return 0;
	}

	/* Reset the connection timeout since the connection is being used. */

	userp->timeout = g_ftp_configp->m_cltimeout;

	/* Split the buffer into command and parameters and then dispatch
	   to the command handling routine. */

	ftp_argp = naftp_split_command (ftp_cmd_buf, readlen);

	if (!strcmp (ftp_cmd_buf, "user"))
	{
#ifdef _DEBUG_FTP
		printf("user\n");
#endif
		naftp_process_user (userp, ftp_argp);

	} else
	if (!strcmp (ftp_cmd_buf, "pass"))
	{

		naftp_process_password (userp, ftp_argp);
#ifdef _DEBUG_FTP
		printf("pass\n");
#endif

	} else
	/* If the FTP client initially sends a password that is	rejected */
	/* subsequent attempts to send a password will use	the "acct"   */
	/* command instead of the "pass" command... J.W.				 */
	if (!strcmp (ftp_cmd_buf, "acct"))
	{

		naftp_process_password (userp, ftp_argp);
#ifdef _DEBUG_FTP
		printf("acct\n");
#endif

	} else
	if (!strcmp (ftp_cmd_buf, "port"))
	{

		naftp_process_port (userp, ftp_argp);
#ifdef _DEBUG_FTP
		printf("port\n");
#endif

	} else
	if (!strcmp (ftp_cmd_buf, "pasv"))
	{
		naftp_process_pasv (userp, ftp_argp);
#ifdef _DEBUG_FTP
		printf("pasv\n");
#endif
	} else
	if (!strcmp (ftp_cmd_buf, "stor"))
	{
		naftp_process_data (userp, ftp_argp, FTP_CMD_STOR);

	} else
	if (!strcmp (ftp_cmd_buf, "retr"))
	{
		naftp_process_data (userp, ftp_argp, FTP_CMD_RETR);

	} else
	if (!strcmp (ftp_cmd_buf, "quit"))
	{
#ifdef _DEBUG_FTP
		printf("quit\n");
#endif
		naftp_send_reply(userp->cntlsocket, 221, "Goodbye.");

		naftp_reset_cntlsock (userp);
	} else
	if (!strcmp (ftp_cmd_buf, "list"))
	{
#ifdef _DEBUG_FTP
		printf("list\n");
#endif
		naftp_process_data (userp, ftp_argp, FTP_CMD_LIST);
	} else
	if (!strcmp (ftp_cmd_buf, "nlst"))
	{
#ifdef _DEBUG_FTP
		printf("nlst\n");
#endif
		naftp_process_data (userp, ftp_argp, FTP_CMD_NLST);
	} else
	if (!strcmp (ftp_cmd_buf, "cwd"))
	{
		naftp_process_data (userp, ftp_argp, FTP_CMD_CWD);
	} else
	if (!strcmp (ftp_cmd_buf, "syst"))
	{
		naftp_process_data (userp, ftp_argp, FTP_CMD_SYST);
	} else
	if (!strcmp (ftp_cmd_buf, "pwd"))
	{
		naftp_process_data (userp, ftp_argp, FTP_CMD_PWD);
	} else
	if (!strcmp (ftp_cmd_buf, "type"))
	{
		naftp_send_reply (userp->cntlsocket, 200, "Type set to I.");
	} else
	{
		naftp_send_reply (userp->cntlsocket, 502, "Command not implemented.");
#ifdef _DEBUG_FTP
		printf("Command not implemented\n");
#endif
	}
}
/*
 * naftp_prepare_user
 *
 * This routine selects an unused user structure from the global configuration
 * structure and initializes it for a user session that has just started.
 *
 * INPUT
 *
 *    client_socket   Control socket for the new user session.
 *
 * RETURNS
 *
 *  >=0 Specifies index of selected user structure.
 *   -1 there are no more user structures available at this time.
 */

int
naftp_prepare_user (int client_socket)
{
	int i;
	ftp_user_info_t *usersp;
	struct sockaddr_in peeraddr;
	int length = sizeof (peeraddr);

	usersp = g_ftp_configp->m_usersp;

	if (NULL == usersp)
		return -1;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (-1 == usersp[i].cntlsocket)
		{
			usersp[i].cntlsocket = client_socket;
			usersp[i].state = FTP_IDLE;
			usersp[i].timeout = g_ftp_configp->m_cltimeout;
			usersp[i].active = 1;
			usersp[i].curarg[0] = '\0';
			usersp[i].sequenceno = 0;
			g_ftp_configp->m_curusers++;
#if defined PSOS_OS || defined NET_OS
			if (getpeername(client_socket, &peeraddr, &length) < 0)
#endif
#ifdef VXWORKS_OS
				if (getpeername (client_socket,(struct sockaddr *)&peeraddr, &length) < 0)
#endif
			{
#ifdef _DEBUG_FTP
				printf("FTP getpeername failed [%lx]\n", getErrno());
#else
				;
#endif
			}

			memcpy ((char *)&usersp[i].cntl_ip_addr, (char *)&peeraddr.sin_addr,
				sizeof (peeraddr.sin_addr));
			return i;
		}
	}

#ifdef _DEBUG_FTP
	printf("FTP: Too many users\n");
#endif

	return -1;
}
/*
 * naftp_init_select
 *
 * This routine initializes the proper select masks for the FTP server.
 *
 * INPUT
 *
 *    read_maskp    Points to a fd_set read mask array.
 *
 * RETURNS
 *
 *    Number of sockets added to the masks.
 */

int
naftp_init_select (fd_set *read_maskp, fd_set *except_maskp,
fd_set *write_maskp)
{
	int count = 1;						/* The server socket is always added to the masks. */
	int i;

	FD_ZERO (read_maskp);
	FD_ZERO (except_maskp);
	FD_ZERO (write_maskp);

	FD_SET (g_ftp_server, read_maskp);
	FD_SET (g_ftp_server, except_maskp);

	use_write_mask = 0;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (g_ftp_configp->m_usersp[i].cntlsocket != -1)
		{
			FD_SET (g_ftp_configp->m_usersp[i].cntlsocket, read_maskp);
			FD_SET (g_ftp_configp->m_usersp[i].cntlsocket, except_maskp);
			count++;
		}

		if (g_ftp_configp->m_usersp[i].datasocket != -1)
		{
			FD_SET (g_ftp_configp->m_usersp[i].datasocket, read_maskp);
			FD_SET (g_ftp_configp->m_usersp[i].datasocket, except_maskp);

			if (g_ftp_configp->m_usersp[i].connected == 0)
			{
#ifdef _DEBUG_FTP
				printf("setting write mask [%d]\n",
					g_ftp_configp->m_usersp[i].datasocket);
#endif

				/*
				 * If the socket isn't connected yet we need to wait for
				 * it to become readable.
				 */
				FD_SET (g_ftp_configp->m_usersp[i].datasocket, write_maskp);
				use_write_mask = 1;		/* flag for select loop */
			}

			count++;
		}
	}

	return count;
}
/*
 * naftp_cmd_stor
 *
 * This routine is called for continued processing of a STOR command.
 * Each instance data arrives on a data socket for the STOR command
 * the application's registered routine is called.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   -1       Abnormal completion
 */

int
naftp_cmd_stor (ftp_user_info_t *userp)
{
	char ftp_cmd_buf[FTP_PI_BUF_SIZE];
	int readlen = 0;
	int loopcount = 0;
	int status;
	int dbgerr = 0;

	/*
	 * Loop a couple of times reading data then return so that other
	 * client connections get processing time.
	 *
	 */

	while (readlen >= 0 && loopcount < 2)
	{
		readlen = recv (userp->datasocket, g_ftp_configp->m_datap, g_ftp_configp->m_size, 0);
		if (readlen < 0)
		{
			dbgerr = getErrno();
			if (dbgerr == EWOULDBLOCK)
			{
				/* Normal for no more data. */
				return 0;
			}
			else
			{
#ifdef _DEBUG_FTP
				printf("FTP: datasock recv error [%lx]\n", getErrno());
#endif

				/* Tear down data socket for normal socketclose */

				if (userp->sequenceno)
				{
					naftp_reset_datasock (userp);
					naftp_send_reply (userp->cntlsocket, 426, "Connection closed; transfer aborted.");
					return -1;
				} else
				{
					/* If the connection has not been established yet then the client timeout
					   applies, so merely return here on the hope that the connection will get
					   established or the attempted will be timed out. */

					return 0;
				}
			}
		} else
		if (0 == readlen)
		{
			/* Tear down connection. Send reply on control connection. */

#ifdef _DEBUG_FTP
			printf("datasock recv returned 0\n");
#endif
			naftp_reset_datasock (userp);

			naftp_send_reply (userp->cntlsocket, 226, "Transfer complete");

			return 0;
		} else
		{
			/* Call user registered function here. */
			if (g_ftp_configp->m_usr_storp)
			{
				userp->sequenceno++;

				status = (*g_ftp_configp->m_usr_storp)(g_ftp_configp->m_datap, readlen, userp->curarg,
					(unsigned long)userp);
				if (status < 0)
				{
					naftp_reset_datasock (userp);
					naftp_send_reply (userp->cntlsocket, 426, "Connection closed; transfer aborted.");
					return -1;
				}
			}
		}

		loopcount++;
	}

	return 0;
}
/*
 * naftp_cmd_retr
 *
 * This routine is called in order to continually handle a RETR request. The RETR command
 * is handled differently than the STOR command since the user routine informs the server
 * when all of the data has been transferred.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   <0       Abnormal completion
 *
 */

int
naftp_cmd_retr (ftp_user_info_t *userp)
{
	int status;
	char buffer[256];

	if (g_ftp_configp->m_usr_retrp)
	{
		userp->sequenceno++;

		status = (*g_ftp_configp->m_usr_retrp)(userp->curarg, (unsigned long)userp);
		if (0 == status)
		{

			naftp_reset_datasock (userp);
			naftp_send_reply (userp->cntlsocket, 226, "Transfer complete.");
		} else
		if (status < 0)
		{
			naftp_reset_datasock (userp);
			sprintf (buffer, "Cannot find the file %s.", userp->curarg);
			naftp_send_reply (userp->cntlsocket, 550, buffer);
		}

		return status;
	}

	naftp_reset_datasock (userp);
	naftp_send_reply (userp->cntlsocket, 226, "Transfer complete.");
	return -1;
}
/*
 * naftp_cmd_list
 *
 * This routine is called in order to continually handle a LIST request. The user
 * routine is only called once for a LIST command. This handler always completes
 * the transfer once the user routine returns.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   -1       Abnormal completion
 *
 */

int
naftp_cmd_list (ftp_user_info_t *userp, int cmd)
{
	int retval = 0;
	char buffer[256];

	if (userp->connected == 0)
	{
		printf("user not connected\n");
	}

	if (g_ftp_configp->m_usr_listp && FTP_CMD_LIST == cmd)
	{
		retval = (*g_ftp_configp->m_usr_listp)(userp->curarg, (unsigned long)userp);

	} else
	if (g_ftp_configp->m_usr_nlstp && FTP_CMD_NLST == cmd)
	{
		retval = (*g_ftp_configp->m_usr_nlstp)(userp->curarg, (unsigned long)userp);
	}

	if (retval < 0)
	{
		naftp_reset_datasock (userp);
		sprintf (buffer, "Could not find the file(s) %s.", userp->curarg);
		naftp_send_reply (userp->cntlsocket, 550, buffer);

	} else
	{
		naftp_reset_datasock (userp);
		naftp_send_reply (userp->cntlsocket, 226, "Transfer complete.");
	}

	return retval;
}
/*
 * naftp_cmd_pwd
 *
 *    This routine is called to handle the PWD command.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   -1       Abnormal completion
 */

int
naftp_cmd_pwd (ftp_user_info_t *userp)
{
	int retval = 0;
	char buffer[128];

	if (g_ftp_configp->m_usr_pwdp)
	{
		retval = (*g_ftp_configp->m_usr_pwdp)(userp->curarg, (unsigned long)userp, buffer);
		if (0 == retval)
		{
			naftp_send_reply (userp->cntlsocket, 257, buffer);
		} else
		{
			sprintf (buffer, "PWD command not supported.");

			naftp_send_reply (userp->cntlsocket, 550, buffer);
		}
	} else
	{
		naftp_send_reply (userp->cntlsocket, 257, "\"/\" is current directory.");
	}

	return retval;
}
/*
 * naftp_cmd_cwd
 *
 *    This routine is called to handle the CWD command.
 *
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   -1       Abnormal completion
 */

int
naftp_cmd_cwd (ftp_user_info_t *userp)
{
	int retval = 0;
	char buffer[128];

	if (g_ftp_configp->m_usr_cwdp)
	{
		retval = (*g_ftp_configp->m_usr_cwdp)(userp->curarg, (unsigned long)userp);
		if (0 == retval)
		{
			naftp_send_reply (userp->cntlsocket, 250, "CWD command successful");
		} else
		{
			sprintf (buffer, "Cannot find the %s directory.", userp->curarg);
			naftp_send_reply (userp->cntlsocket, 550, buffer);
		}
	} else
	{
		naftp_send_reply (userp->cntlsocket, 250, "CWD command successful");
	}

	return retval;
}
/*
 * naftp_cmd_syst
 *
 * This routine is called to handle the SYST command.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 * RETURNS
 *
 *    0       For normal completion
 *   -1       Abnormal completion
 */

int
naftp_cmd_syst (ftp_user_info_t *userp)
{
	int retval = 0;
	char buffer[256];

	buffer[0] = '\0';
	if (g_ftp_configp->m_usr_systp)
	{
		retval = (*g_ftp_configp->m_usr_systp)(userp->curarg, buffer, (unsigned long)userp);
	} else
	{
		sprintf (buffer, "NET+ARM version %d.%d", g_ftp_configp->m_majversion,
			g_ftp_configp->m_minversion);
	}

	naftp_send_reply (userp->cntlsocket, 215, buffer);

	return retval;
}
/*
 * naftp_resolve_cmd
 *
 * This routine is called to determine and handle the current command being
 * processed by the server on behalf of a client connection.
 *
 * INPUT
 *
 *    userp   Points to user structure for the current session.
 *
 */

int
naftp_resolve_cmd (ftp_user_info_t *userp)
{
	struct sockaddr_in datapeer;
	int    addrlen = sizeof (struct sockaddr_in);
	int    retval;
	int    dbgerr;

	if (FTP_PASV_DATA == userp->state)
	{
#if defined PSOS_OS || defined NET_OS
		retval = accept (userp->datasocket, &datapeer, &addrlen);
#endif
#ifdef VXWORKS_OS
		retval = accept (userp->datasocket,(struct sockaddr *)&datapeer, &addrlen);
#endif
		if (retval < 0)
		{
#ifdef _DEBUG_FTP
			printf("FTP:accept failed [%lx]\n", getErrno());
#endif
			dbgerr = getErrno();
		} else
		{
			userp->state = FTP_PROCESS_CMD;
		}
	}

	/* Reset the connection timeout since the connection is being used. */

	userp->timeout = g_ftp_configp->m_cltimeout;

	switch (userp->cntlstate)
	{
		case FTP_CMD_STOR:

			naftp_cmd_stor (userp);

			break;

		case FTP_CMD_RETR:

			naftp_cmd_retr (userp);

			break;

		case FTP_CMD_LIST:
		case FTP_CMD_NLST:

			naftp_cmd_list (userp, userp->cntlstate);

			break;

		case FTP_CMD_CWD:
		case FTP_CMD_PWD:
		case FTP_CMD_SYST:
			break;
	}

	return 0;
}
/*
 * naftp_resolve_user
 *
 * This routine determines the index of the user client which data
 * has arrived on. Each user socket is checked to determine additional
 * processing is required.
 *
 * INPUT
 *
 *    readmaskp    Points to read mask used in the prior select call.
 *
 */

int
naftp_resolve_user (fd_set *readmaskp, fd_set *exceptmaskp, fd_set *writemaskp)
{
	int i;
	ftp_user_info_t *usersp;

	usersp = g_ftp_configp->m_usersp;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (-1 != usersp[i].cntlsocket && FD_ISSET(usersp[i].cntlsocket, readmaskp))
		{
			naftp_parse (i);
		}

		if ((-1 != usersp[i].datasocket) && FD_ISSET(usersp[i].datasocket, writemaskp))
		{
#ifdef _DEBUG_FTP
			printf("FTP: data socket connected [%d]\n",
				usersp[i].datasocket);
#endif
			usersp[i].connected = 1;
#ifdef PSOS_OS
			tm_wkafter(10);
#endif
#ifdef VXWORKS_OS
			/*taskDelay(10);*/
#endif
#ifdef NET_OS
			tx_thread_sleep(10);
#endif
			/* Now handle the command we got before */
			naftp_user_data (&usersp[i], usersp[i].cmd, usersp[i].curarg);
		}

		if (-1 != usersp[i].datasocket && FD_ISSET(usersp[i].datasocket, readmaskp))
		{
#ifdef _DEBUG_FTP
			printf("FTP: got command on data socket [%d]\n",
				usersp[i].datasocket);
#endif
			naftp_resolve_cmd (&usersp[i]);
		}

		if (-1 != usersp[i].cntlsocket && FD_ISSET(usersp[i].cntlsocket, exceptmaskp))
		{
#ifdef _DEBUG_FTP
			printf("FTP: got exception on client socket\n");
#else
			;
#endif
		}

		if (-1 != usersp[i].datasocket && FD_ISSET(usersp[i].datasocket, exceptmaskp))
		{
			/* Let resolve command handle resetting the connection and user data. */
#ifdef _DEBUG_FTP
			printf("FTP: got exception on data socket\n");
#endif
			naftp_resolve_cmd (&usersp[i]);
		}

		/* Repeatedly call the user registered function depending upon the command. */

		if (-1 != usersp[i].datasocket && (usersp[i].cntlstate == FTP_CMD_RETR ||
			FTP_CMD_LIST == usersp[i].cntlstate ||
			FTP_CMD_NLST == usersp[i].cntlstate))
		{
			naftp_resolve_cmd (&usersp[i]);
		}
	}

	return 0;
}
/*
 * naftp_data_connections
 *
 * This routine counts and returns the number of data connections
 * currently in use.
 *
 * RETURNS
 *
 *    The number of open data sockets.
 */
int
naftp_data_connections ()
{
	int i;
	int count = 0;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (g_ftp_configp->m_usersp[i].datasocket != -1)
			count++;
	}

	return count;
}
/*
 * FSProperties
 *
 * This routine is used to set various OS parameters prior to starting
 * the FTP server task.
 *
 * This is an external API routine.
 *
 * INPUTS:
 *
 *    name       Name of Web server task.
 *    priority   Task priority of the web server.
 *    systack    Size of system stack.
 *    usrstack   Size of user stack.
 *    mode       Any os dependent task mode settings.
 *    flags      Any os dependent flags used in starting the task.
 *
 */

void
FSProperties (char *tnamep, int priority, int sysstack, int usrstack, int mode, int flags)
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return;

	strncpy (g_ftp_configp->m_name, tnamep, 3);
	g_ftp_configp->m_name[4] = '\0';

	if (priority != FS_NOT_DEFINED)
		g_ftp_configp->m_priority = priority;

	if (sysstack != FS_NOT_DEFINED)
		g_ftp_configp->m_sysstack = sysstack;

	if (usrstack != FS_NOT_DEFINED)
		g_ftp_configp->m_usrstack = usrstack;

	if (mode != FS_NOT_DEFINED)
		g_ftp_configp->m_mode = mode;

	if (flags != FS_NOT_DEFINED)
		g_ftp_configp->m_flags = flags;
}
/*
 * FSRegisterCWD
 *
 * This routine is used to register the user application CWD routine.
 *
 * This call is ignored if the server is running.
 * This is an external API routine.
 *
 * Parameter(s)
 *
 *    app_CWDp     Pointer to user application Change Working Directory routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 *
 */

int FSRegisterCWD (int (*app_CWDp)(char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_cwdp = app_CWDp;

	return 0;
}
/*
 * FSRegisterLIST
 *
 * This routine is used to register the user application LIST routine.
 *
 * This call is ignored if the server is running.
 * This is an external API routine.
 *
 * INPUT
 *
 *    app_LISTp    Pointer to user application LIST routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int FSRegisterLIST (int (*app_LISTp)(char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_listp = app_LISTp;

	return 0;
}
/*
 * FSRegisterNLST
 *
 * This routine is used to register the user application NLST routine.
 *
 * This call is ignored if the server is running.
 * This is an external API routine.
 *
 * INPUT
 *
 *    app_NLSTp    Pointer to user application NLST routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int FSRegisterNLST (int (*app_NLSTp)(char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_nlstp = app_NLSTp;

	return 0;
}
/*
 * FSRegisterRETR
 *
 * This routine is used to register the user application RETR routine.
 * This call is ignored if the server is running.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    app_RETRp    Pointer to user application RETR routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int FSRegisterRETR (int (*app_RETRp)(char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_retrp = app_RETRp;

	return 0;
}
/*
 * FSRegisterSTOR
 *
 * This routine is used to register the user application STOR routine.
 * This call is ignored if the server is running.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    app_STORp    Pointer to user application STOR routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int FSRegisterSTOR (int (*app_STORp)(char *, int, char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_storp = app_STORp;

	return 0;
}
/*
 * FSRegisterSYST
 *
 * This routine is used to register a user application SYST routine.
 * This call is ignored if the server is running.
 *
 * This is an external API routine.
 *
 * Parameter(s)
 *
 *    app_SYSTp    Pointer to user application SYST routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int FSRegisterSYST (int (*app_SYSTp)(char *, char *, unsigned long))
{
	if (NULL == g_ftp_configp || g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_systp = app_SYSTp;

	return 0;
}
/*
 * FSRegisterDataClose
 *
 * This routine is used to register a callback routine for when a data
 * connection is closed.
 *
 * This is an external api routine.
 *
 * INPUTS
 *
 *    app_dResetp    Pointer to user application data close routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 *
 */

int
FSRegisterDataClose (int (*app_dResetp)(char *, unsigned long))
{
	if (g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_dresetp = app_dResetp;

	return 0;
}
/*
 * FSRegisterControlClose
 *
 * This routine is used to register a callback routine for when a control
 * connection is closed.
 *
 * This is an external api routine.
 *
 * INPUTS
 *
 *    app_cResetp    Pointer to user application control close routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 *
 */
int
FSRegisterControlClose (int (*app_cResetp)(char *, unsigned long))
{
	if (g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_cresetp = app_cResetp;

	return 0;
}
/*
 * FSRegisterValidation
 *
 * This routine is used to register a callback routine that validates usernames
 * and passwords.
 *
 * This is an external api routine.
 *
 * INPUTS
 *
 *    app_validp   Pointer to user application validation routine.
 *
 * RETURNS
 *
 *    0            Success
 *   -1            Server is already started, the registration is ignored.
 */

int
FSRegisterValidation (int (*app_validp)(char *, char *))
{
	if (NULL == g_ftp_configp)
		return -1;

	if (g_ftp_configp->m_started)
		return -1;

	g_ftp_configp->m_usr_validationp = app_validp;

	return 0;
}
/*
 * FSInitialize
 *
 * This routine initializes the FTP data structures.
 *
 * INPUT
 *
 *    numusers      Specifies the number of concurrent sessions supported by
 *                  the server.
 *
 * RETURNS
 *
 *     0            The data structures were properly initialized.
 *    -1            An error occurred.
 */

int
FSInitialize (int numusers)
{
	if (naftp_init_config(numusers) < 0)
		return -1;
	return 0;
}
/*
 * FSAddUser
 *
 * This routine is used to add a user to the user table.
 *
 * This is an external API routine.
 *
 * INPUTS
 *
 *    userp     Points to the user name to be added.
 *    passp     Points to the user name password.
 *
 * RETURNS
 *
 *    <0        The user table is full or the name/password was too large.
 *     0 or >0  The user name/password was successfully added.
 */

int
FSAddUser (char *userp, char *passp)
{
	int i;

	if (strlen (userp) >= FS_MAXUSERLEN-1 ||
		strlen (passp) >= FS_MAXUSERLEN-1)
	{
		return -1;
	}

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (strlen (g_ftp_configp->m_usertablep[i].username) == 0)
		{
			strcpy (g_ftp_configp->m_usertablep[i].username, userp);
			strcpy (g_ftp_configp->m_usertablep[i].password, passp);
			return i;
		}
	}

	return -1;
}
/*
 * FSHandleToSocket
 *
 * This routine is used to retrieve the data socket for a client connection.
 * The data socket is used by FTP applications to send back large amounts of
 * data before returning control to the FTP server.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    handle       Used to retrieve the FTP server client connection object.
 *
 * RETURNS
 *
 *    >0           Valid data socket.
 *    <0           The handle was not valid, or
 *                 the client object does not have an open data socket.
 *
 */

int
FSHandleToSocket (unsigned long handle)
{
	ftp_user_info_t *userp;
	int i;

	userp = (ftp_user_info_t *)handle;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (userp == &g_ftp_configp->m_usersp[i])
		{
			return userp->datasocket;
		}
	}

	return -1;
}
/*
 * FSHandleToControlSocket
 *
 * This routine is used to retrieve the control socket for a client connection.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    handle       Used to retrieve the FTP server client connection object.
 *
 * RETURNS
 *
 *    >0           Valid control socket.
 *    <0           The handle was not valid.
 *
 */

int
FSHandleToControlSocket (unsigned long handle)
{
	ftp_user_info_t *userp;
	int i;

	userp = (ftp_user_info_t *)handle;

	for (i = 0; i < g_ftp_configp->m_maxusers; i++)
	{
		if (userp == &g_ftp_configp->m_usersp[i])
		{
			return userp->cntlsocket;
		}
	}

	return -1;
}
/*
 * FSSequenceNumber
 *
 * This routine returns the current sequence number of the data socket.
 * This routine is for callback routines that require an indication of how
 * many times the callback routine has been called.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    handle       Used to retrieve the FTP server client connection object.
 *
 * RETURNS
 *
 *    0 or >0      Current sequence number.
 *   -1            The supplied handle does not represent a valid data connection.
 */

int
FSSequenceNumber (unsigned long handle)
{
	ftp_user_info_t *userp;
	int i;

	userp = (ftp_user_info_t *)handle;

	if (userp)
	{
		return userp->sequenceno;

	} else
	{
		return -1;
	}
}
/*
 * FSUserName
 *
 * This routine returns a pointer to the null terminated string containing
 * the user name of the current session.
 *
 * This is an external API routine.
 *
 * INPUT
 *
 *    handle       Used to retrieve the FTP server client connection object.
 *
 * RETURNS
 *
 *    Non-Null     Points to user name string.
 *    NULL         The handle does not represent a valid session.
 *
 */

char *
FSUserName (unsigned long handle)
{
	ftp_user_info_t *userp;

	userp = (ftp_user_info_t *)handle;

	if (userp)
	{
		return userp->username;

	} else
	{
		return NULL;
	}
}
