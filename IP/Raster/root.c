/*
 *
 *     Module Name: start.c
 *	       Version: 1.00
 *   Original Date: 08/23/99
 *	        Author: Charles Gordon
 *	      Language: Ansi C
 * Compile Options:
 * Compile defines:
 *	     Libraries:
 *    Link Options:
 *
 *    Entry Points: tx_application_define()
 *
 *  Copyrighted (c) by NETsilicon, Inc.  All Rights Reserved.
 *
 * Description.
 * =======================================================================
 *  tx_application_define() is called just before the ThreadX kernel loads.
 *  It is responsible for creating at least one thread to drive the application.
 *  We create a thread that starts up NET+OS.
 *
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  =======================================================
 *
 *
 */

#include <fj.h>
#include <stdio.h>
#include <bspconf.h>
#include <tx_api.h>
#include <hservapi.h>
void applicationTcpDown (void);

/*
 * Set this to 1 to run the system POST tests during startup.
 */
const int APP_POST = 0;

/*
 * Set this to 1 to run the manufacturing burn in tests.
 */
int APP_BURN_IN_TEST = 0;

#ifdef FOXJET
VOID fj_init(VOID);
#endif

/*
 *
 *  Function: void applicationStart (void)
 *
 *  Description:
 *
 *      This routine is responsible for starting the user application.  It should
 *      create any threads or other resources the application needs.
 *
 *      ThreadX, the NET+OS device drivers, and the TCP/IP stack will be running
 *      when this function is called.
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */

void applicationStart (void)

{
#ifndef FOXJET
	void AppSearchURL (unsigned long, char*);
	void AppPreprocessURL (unsigned long, char*);

	/*
	 * Register the URL search and preprocess functions and start the
	 * HTTP server
	 */

	HSRegisterSearchFunction(AppSearchURL, AppPreprocessURL);

	HSStartServer();
#endif

#ifdef FOXJET
	fj_init();
#endif

	tx_thread_suspend(tx_thread_identify());

}
/*
 *
 *  Function: void applicationTcpDown (void)
 *
 *  Description:
 *
 *      This routine will be called by the NET+OS root thread once every
 *      clock tick while it is waiting for the TCP/IP stack to come up.
 *      This function can increment a counter everytime it's called to
 *      keep track of how long we've been waiting for the stack to start.
 *      If we've been waiting too long, then this function can do something
 *      to handle the error.
 *
 *      This function will not be called once the stack has started.
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */

void applicationTcpDown (void)

{
	static int ticksPassed = 0;
	int secs, rem;
	char str[80];

	ticksPassed++;
	/*
	 * Code to handle error condition if the stack doesn't come up goes here.
	 */
	secs = ticksPassed/BSP_TICKS_PER_SECOND;
	rem  = ticksPassed%BSP_TICKS_PER_SECOND;
	if ( 0 == rem )
	{
		sprintf(str, "TCP not initialized after %d seconds\n", secs );
		fj_writeInfo(str);
	}
	return;
}
