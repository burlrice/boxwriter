#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#endif
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_bitmap.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

// used for MemStor identification
const  CHAR fj_BitmapID[] = "Bitmap";
const  CHAR fj_bmpID[]    = "bmp";

extern const unsigned char fj_nTestPattern32; // Burl
extern const int fj_nTestPattern32Size; // Burl

//
// NOTE: fj_Image, which processes the raster images, was the first module written.
//       These structures and methods are called BITMAP in order to avoid renaming things.
//       The external information will refer to these as images or graphics.
//

//
//  FUNCTION: fj_BitmapNew()
//
//  PURPOSE:  Create a new FJBITMAP structure
//
FJDLLExport LPFJBITMAP fj_BitmapNew( VOID )
{
	LPFJBITMAP pfb;

	pfb = (LPFJBITMAP)fj_calloc( 1, sizeof (FJBITMAP) );

	pfb->lBoldValue    =  0;			// amount of boldness. 0 origin.
	pfb->lWidthValue   =  1;			// amount of width stretch. 1 origin.
	pfb->lbmpSize      =  0;			// size of bmp in bytes
	pfb->pbmp          =  NULL;			// pointer to bmp
	pfb->strName[0]    =  0;			// bmp name
	return( pfb );
}
//
//  FUNCTION: fj_BitmapDestroy()
//
//  PURPOSE:  Destroy a FJBITMAP structure
//
FJDLLExport VOID fj_BitmapDestroy( LPFJELEMENT pfe )
{
	LPFJBITMAP pfb;

	pfb = (LPFJBITMAP)pfe->pDesc;
	if ( NULL != pfb)
	{
		if ( NULL != pfb->pbmp ) fj_free( pfb->pbmp );
		fj_free( pfb );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_BitmapDup()
//
//  PURPOSE:  Duplicate a FJBITMAP structure
//
FJDLLExport LPFJBITMAP fj_BitmapDup( CLPCFJBITMAP pfbIn )
{
	LPFJBITMAP pfbDup;

	pfbDup = (LPFJBITMAP)fj_malloc( sizeof(FJBITMAP) );
	memcpy( pfbDup, pfbIn, sizeof(FJBITMAP) );

	if ( NULL != pfbIn->pbmp )
	{
		pfbDup->pbmp = fj_malloc( pfbDup->lbmpSize );
		if ( NULL != pfbDup->pbmp ) memcpy( pfbDup->pbmp, pfbIn->pbmp, pfbDup->lbmpSize );
	}

	return( pfbDup );
}
//
//	FUNCTION: fj_bmpCheckFormat
//
//	Return: TRUE if valid bmp
//
//  Check for useful bmp
//
FJDLLExport BOOL fj_bmpCheckFormat( LPBYTE pbmp )
{
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LONG lLength;						// size of buffer in bytes
	LONG lOffset;						// bmp offset to rows of bits
	LONG lHeight;						// bmp height in bits
	LONG lWidth;						// bmp width in bits
	LONG lHeightBytes;					// bmp height in bytes
	LONG lWidthBytes;					// bmp width in bytes
	LONG lLengthCalc;					// calculated length in bytes
	BOOL bRet;

	bRet = FALSE;
	if ( NULL != pbmp )
	{
		pbmfh = (BITMAPFILEHEADER *)pbmp;
		pbmih = (BITMAPINFOHEADER *)(pbmp+sizeof(BITMAPFILEHEADER));
										// bmp magic number
		if ( ('B' == *pbmp) && ('M' == *(pbmp+1)) )
		{
			lLength = LOADREVERSEWORD32(&pbmfh->bfSize);
			lOffset = LOADREVERSEWORD32(&pbmfh->bfOffBits);
			// Windows bmp header length
			if ( sizeof(BITMAPINFOHEADER) == LOADREVERSEWORD32(&pbmih->biSize) )
			{
				if ( (1 == LOADREVERSEWORD16(&pbmih->biPlanes)) && (1 == LOADREVERSEWORD16(&pbmih->biBitCount)) && (0 == LOADREVERSEWORD32(&pbmih->biCompression)) )
				{
					lHeight = LOADREVERSEWORD32(&pbmih->biHeight);
					lWidth  = LOADREVERSEWORD32(&pbmih->biWidth);
					lHeightBytes =  (lHeight+ 7)/8;
					lWidthBytes  = ((lWidth +31)/32)*4;
					lLengthCalc = lOffset + (lHeight * lWidthBytes);
										// header plus rowdata must equal size
					if ( lLength == lLengthCalc )
					{
						bRet = TRUE;
					}
					// actual size might be rounded up to a multiple of 4
					else if ( (lLength > lLengthCalc) && (lLength <= (lLengthCalc+4)))
					{
						bRet = TRUE;
					}
				}
			}
		}
	}
	return( bRet );
}
FJDLLExport VOID fj_BitmapCreateImage( CLPFJELEMENT pfe )
{
	register LONG lBits;
	register unsigned char inByte;
	register unsigned char hBit;
	register unsigned char wBit;
	register LPBYTE pBufIn;
	register LPBYTE pBufOut;
	register int  w;
	register int  h;

	LPFJBITMAP pfb;
	LPFJIMAGE  pfi;
	LPFJIMAGE  pfiOld;
	LPBYTE     pBuffer;

	LPBYTE pbmp;
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LONG lLength;						// size of buffer in bytes
	LONG lOffset;						// bmp offset to rows of bits
	LONG lHeight;						// bmp height in bits
	LONG lWidth;						// bmp width in bits
	LONG lHeightBytes;					// bmp height in bytes
	LONG lWidthBytes;					// bmp width in bytes
	LONG lWidthBytes4;					// bmp width in bytes round up to multiple of 4

	pfb = (LPFJBITMAP)pfe->pDesc;
	pbmp = NULL;
	lBits = 0;

	// pre-process
	// get what we need to build FJIMAGE

	{
#define BMP_PARAMETERS 3
		LPBYTE     pElement;
		STR   sName[FJBMP_NAME_SIZE*2];
		STR   str[FJBMP_MEMSTOR_SIZE];
		LPSTR pParams[BMP_PARAMETERS];
		int   res;

		if ( NULL == pfb->pbmp )
		{
			// bitmap Name
			fj_processParameterStringOutput( sName, pfb->strName );


			if (!strcmp (sName, "\"" FJSYS_TEST_PATTERN "\"" )) {
				pbmp = (LPBYTE)(LPVOID)&fj_nTestPattern32;
				pfb->lbmpSize = fj_nTestPattern32Size;

				pfb->pbmp = fj_malloc( pfb->lbmpSize );
				
				if (pfb->pbmp != NULL) 
					memcpy (pfb->pbmp, pbmp, pfb->lbmpSize );
			}
			else {
				pElement = fj_MemStorFindElement( pfe->pfm->pfph->pMemStor, fj_bmpID, sName );
				if ( NULL != pElement )
				{
					strcpy( str, (LPSTR)pElement );
					res = (int)fj_ParseKeywordsFromStr( str, ',', pParams, BMP_PARAMETERS );
					if ( 0 != strcmp( pParams[0], fj_bmpID ) ) res = 0;

											// FJ_BMP string must have 3 fields!!!!!
					if ( res == BMP_PARAMETERS )
					{
						pfb->lbmpSize = atoi(pParams[2]);
						pfb->pbmp = fj_malloc( pfb->lbmpSize );
						if ( NULL != pfb->pbmp ) memcpy( pfb->pbmp, pElement+strlen((LPSTR)pElement)+1, pfb->lbmpSize );
					}
				}
			}
		}

		pbmp = pfb->pbmp;
		if ( NULL != pbmp )
		{
			if ( FALSE == fj_bmpCheckFormat( pbmp ) )
			{
				pbmp = NULL;
			}
		}
	}

	// build FJIMAGE

	if ( NULL != pbmp )
	{
		pbmfh = (BITMAPFILEHEADER *)pbmp;
		pbmih = (BITMAPINFOHEADER *)(pbmp+sizeof(BITMAPFILEHEADER));
		lOffset = LOADREVERSEWORD32(&pbmfh->bfOffBits);
		lHeight = LOADREVERSEWORD32(&pbmih->biHeight);
		lWidth  = LOADREVERSEWORD32(&pbmih->biWidth);
		if ( lHeight > pfe->pfm->pfph->pfphy->lChannels ) lHeight = pfe->pfm->pfph->pfphy->lChannels;

		lHeightBytes =  (lHeight+ 7)/8;
		lWidthBytes  =  (lWidth + 7)/8;
		lWidthBytes4 = ((lWidth +31)/32)*4;
		lLength = lHeightBytes * lWidth;

		pfi = fj_ImageNew( lLength );	// get a new one
		if ( NULL != pfi )
		{
			pfi->lHeight = lHeight;
			pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);

			for ( h = 0; h < lHeight; h++ )
			{
				int hByte;
				hByte = h/8;
				hBit = 0x80 >> (h-(hByte*8));
				pBufOut = pBuffer + hByte;
				pBufIn  = pbmp+lOffset+h*lWidthBytes4;
				wBit = 0x80;
				inByte = *pBufIn;
				for ( w = 0; w < lWidth; w++ )
				{
					if ( 0 == inByte )
					{
						pBufIn++;
						inByte = *pBufIn;
						pBufOut += 8*lHeightBytes;
						w += 7;
						wBit = 0x80;
					}
					else
					{
						if ( 0 != (inByte & wBit) )
						{
							*pBufOut |= hBit;
							lBits++;
						}
						pBufOut += lHeightBytes;
						wBit = wBit >> 1;
						if ( 0 == wBit )
						{
							wBit = 0x80;
							pBufIn++;
							inByte = *pBufIn;
						}
					}
				}
			}
			pfi->lBits = lBits;

			// bold and width are done in each element's routines.
			// they are sometimes omitted, depending on the element.
			// slant and transforms are usually 'global' and usually part of the 'environment',
			//    and they are not wanted for some special cases; e.g., text in a barcode.
			fj_ImageNegative(pfi);

			if ( 0 < pfb->lBoldValue  ) pfi = fj_ImageBold(  pfi, pfb->lBoldValue );
			if ( 1 < pfb->lWidthValue ) pfi = fj_ImageWidth( pfi, pfb->lWidthValue );
		}

		pfiOld = pfe->pfi;				// do not reuse old image
		pfe->pfi = pfi;					// put new image into pfe
		if ( NULL != pfiOld )			// release old one
		{
			fj_ImageRelease( pfiOld );
		}
	}
	return;
}
//
//   Bitmap does not change
//
FJDLLExport VOID fj_BitmapPhotocellBuild( CLPFJELEMENT pfe )
{
	return;
}
//
//  FUNCTION: fj_BitmapToString()
//
//  PURPOSE:  Convert a FJBITMAP structure into a descriptive string with binary buffer
//
FJDLLExport VOID fj_BitmapToString( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJBITMAP pfb = (CLPCFJBITMAP)pfe->pDesc;
	STR  sName[FJTEXT_NAME_SIZE*2];
	STR  sNamebmp[FJBMP_NAME_SIZE*2];

										// bitmap element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	// bmp Name
	fj_processParameterStringOutput( sNamebmp, pfb->strName );
	if ( NULL != pStr ) sprintf (pStr, "{%s,%s,%f,%d,%c,%d,%d,%c,%c,%c,%s}",
			fj_BitmapID,
			sName,
			pfe->fLeft,					// distance from left edge of box
			pfe->lRow,					// row number to start pixels - 0 = bottom of print head
			(CHAR)pfe->lPosition,		// type of positioning
			pfb->lBoldValue,			// amount of boldness. 0 origin.
			pfb->lWidthValue,			// amount of width stretch. 1 origin.
										// true for invert
			(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F'),
		sNamebmp );						// bmp name

	return;
}
//
//  FUNCTION: fj_BitmapFromString
//
//  PURPOSE:  Convert a descriptive string into a FJBITMAP structure
//
FJDLLExport VOID fj_BitmapFromString( LPFJELEMENT pfe, LPBYTE pBuff )
{
#define BITMAP_PARAMETERS 11
	LPFJBITMAP pfb;
	LPSTR  pParams[BITMAP_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfb = (LPFJBITMAP)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJBITMAP_MEMSTOR_SIZE+1 );
	strncpy( pStrM, (LPSTR)pBuff, FJBITMAP_MEMSTOR_SIZE );
	*(pStrM+FJBITMAP_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, BITMAP_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_BitmapID ) ) res = 0;

	if ( res == BITMAP_PARAMETERS )		// FJ_BITMAP string must have 9 field delimiters!!!!!
	{
		pfe->lTransform   =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft        = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow         = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition    =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfb->lBoldValue   = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfb->lWidthValue  = atol(pParams[ 6]);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 7]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
		fj_processParameterStringInput( pParams[10] );
		strncpy( pfb->strName,pParams[10], FJBMP_NAME_SIZE  ); *(pfb->strName+FJBMP_NAME_SIZE) = 0;
	}
	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_BitmapBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJBITMAP from FJMEMSTOR
//
FJDLLExport VOID fj_BitmapBuildFromName( CLPCFJMEMSTOR pfms, LPFJELEMENT pfe, LPCSTR pName )
{
	LPBYTE     pBitmapStr;

	pBitmapStr = fj_MemStorFindElement( pfms, fj_BitmapID, pName );
	if ( NULL != pBitmapStr )
	{
		fj_BitmapFromString( pfe, pBitmapStr );
	}

	return;
}
//
//	FUNCTION: fj_BitmapAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJBITMAP element to FJMEMSTOR
//
FJDLLExport BOOL fj_BitmapAddToMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	BOOL   bRet;
	CHAR   buff[FJBITMAP_MEMSTOR_SIZE];

	fj_BitmapToString( pfe, buff );

	bRet = fj_MemStorAddElementString( pfms, buff );

	return( bRet );
}
//
//	FUNCTION: fj_BitmapDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Delete FJBITMAP element from FJMEMSTOR
//
FJDLLExport BOOL fj_BitmapDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	STR sName[FJBITMAP_NAME_SIZE*2];
										// bitmap element Name
	fj_processParameterStringOutput( sName, pfe->strName );

	return( fj_MemStorDeleteElement( pfms, fj_BitmapID, sName ) );
}
//
//	FUNCTION: fj_BitmapGetType
//
//	Return: Bitmap type
//
//
FJDLLExport enum FJELETYPE fj_BitmapGetType( VOID )
{
	return( FJ_TYPE_BITMAP );
}
//
//	FUNCTION: fj_BitmapGetTypeString
//
//	Return: Pointer to string for Bitmap type
//
//
FJDLLExport LPCSTR fj_BitmapGetTypeString( VOID )
{
	return( fj_BitmapID );
}
//
//	FUNCTION: fj_BitmaptGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_BitmapGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJBITMAP pfb;
	LONG lLen;
	LONG lLeft;

	pfb = (LPFJBITMAP)pfe->pDesc;
	lLen = strlen( pfb->strName );
	strncpy( pStr, pfb->strName, lMaxCount );
	lLeft = lMaxCount - lLen;
	if ( lLeft > 0 ) strncpy( pStr+lLen, ".bmp", lLeft );
	*(pStr+lMaxCount-1) = 0;
	return;
}
FJCELEMACTION fj_BitmapActions =
{
	(LPVOID  (*)( VOID ))fj_BitmapNew,
	(LPVOID  (*)( LPVOID ))fj_BitmapDup,
	fj_BitmapDestroy,
	fj_BitmapCreateImage,
	fj_BitmapPhotocellBuild,
	fj_BitmapBuildFromName,
	fj_BitmapAddToMemstor,
	fj_BitmapDeleteFromMemstor,
	fj_BitmapGetTextString,
	fj_BitmapGetType,
	fj_BitmapGetTypeString
};

//
//	FUNCTION: fj_bmpAddToMemstor
//
//	Return: TRUE if success.
//
//  Add a bmp to FJMEMSTOR
//
FJDLLExport BOOL fj_bmpAddToMemstor( LPFJMEMSTOR pfms, LPBYTE pbmp, LPCSTR pName )
{
	BITMAPFILEHEADER *pbmfh;
	LPBYTE pBuf;
	LONG   lSize;
	LONG   lSizebmp;
	LONG   lStrLen;
	BOOL   bRet;
	STR    sName[FJBMP_NAME_SIZE*2];

	bRet = FALSE;
	if ( NULL != pbmp )
	{
		bRet = fj_bmpCheckFormat( pbmp );
		if ( TRUE == bRet )
		{
			pbmfh = (BITMAPFILEHEADER *)pbmp;
			lSizebmp = LOADREVERSEWORD32(&pbmfh->bfSize);
			lSize = lSizebmp + FJBMP_MEMSTOR_SIZE;
			pBuf = fj_malloc( lSize );
			if ( NULL != pBuf )
			{
				//  Name
				fj_processParameterStringOutput( sName, pName );
				sprintf( (LPSTR)pBuf, "{%s,%s,%d}", fj_bmpID, sName, (int)lSizebmp );
				lStrLen = strlen( (LPSTR)pBuf );
				lSize = lSizebmp + lStrLen + 1;
				memcpy( pBuf+lStrLen+1, pbmp, lSizebmp );
				bRet = fj_MemStorAddElementBinary( pfms, pBuf, lSize );

				fj_free( pBuf );
			}
		}
	}
	return( bRet );
}
