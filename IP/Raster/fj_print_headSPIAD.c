#include "fj_netsilicon.h"
#include <na_isr.h>
#include "fj.h"

// temporary for debugging
static WORD16  wReadA[8];
static LONG    lLengthA[8];

//
// The second serial port runs in SPI mode to read the A/D converter.
// The A/D converter simultaneously transmits 2 bytes in both directions.
// While a 2 byte control word is written, the 2 byte result of the previous control word is read.
// The SPI FIFO usually operates on a 4-byte word-at-a-time.
// A character gap timer is used to "interrupt" the transmission after a "large" gap appears between characters.
// This gap is made large enough not go off between the 2 bytes - thus both bytes are read at the same interrupt.
// The gap timer is enabled when the control word is written and diabbled after the interrupt.
// Thus we will get no extra interrupts when there is no data on the interface.
//

//
// There is also a problem with the Chip Select transitioning between bytes.
// The A/D converter does not like this - it terminates the transmission and we get only one byte.
// To fix this, we have created our own CS using an output line that this code controls directly.
// This output line is used as the CS for the A/D converter.
// The CS is turned off in the interrurt routine after the 2 bytes are received.
//

//
// ISR to handle SPI Receive interrupt
//
static int fj_print_head_spiR_interrupt(void *pIn)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	volatile WORD32 SPIsr1;				// for looking
	volatile WORD32 SPIsr2;				// for looking
	volatile WORD32 SPIsr;				// for working
	LONG   lLength;
	WORD32 w32;

	SPIsr1  = (WORD32)((*(NARM_SER)).sc2.sr.reg);

										// get status register
	SPIsr = (WORD32)((*(NARM_SER)).sc2.sr.reg);
	while ( 0 != (SPIsr & 0x0C00FFF0) )
	{
										// receive buffer closed due to a gap timer
		if ( 1 ==(*(NARM_SER)).sc2.sr.bits.rxbc )
		{
										// must be reset before any data available
			(*(NARM_SER)).sc2.sr.bits.rxbc = 1;
		}
		// let's acknowledge any signal interrupt pending that came on.
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxdcdi ) (*(NARM_SER)).sc2.sr.bits.rxdcdi = 1;
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxrii  ) (*(NARM_SER)).sc2.sr.bits.rxrii  = 1;
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxdsri ) (*(NARM_SER)).sc2.sr.bits.rxdsri = 1;
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.txctsi ) (*(NARM_SER)).sc2.sr.bits.txctsi = 1;
		// and a break condition
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxbrk  ) (*(NARM_SER)).sc2.sr.bits.rxbrk  = 1;
		// check for 3 errors
										// receive framing error
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxfe )
		{
			(*(NARM_SER)).sc2.sr.bits.rxfe = 1;
			pGlobal->lSPIReadLength = -1;
		}
										// receive parity error
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxpe )
		{
			(*(NARM_SER)).sc2.sr.bits.rxpe = 1;
			pGlobal->lSPIReadLength = -1;
		}
										// receive FIFO overrun
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxover )
		{
			(*(NARM_SER)).sc2.sr.bits.rxover = 1;
			pGlobal->lSPIReadLength = -1;
		}

										// for testing
		if ( 0 == (*(NARM_SER)).sc2.sr.bits.rxrdy )
		{
			w32 = 1;
		}
										// look again for debugging
		SPIsr2  = (WORD32)((*(NARM_SER)).sc2.sr.reg);

		// OK. Ready to be serious about getting data
		if ( 1 == (*(NARM_SER)).sc2.sr.bits.rxrdy )
		{
			lLength = (*(NARM_SER)).sc2.sr.bits.rxfdb;
			if ( 0 == lLength ) lLength = 4;
			// if the length is greater or equal to 2,
			// disable the gap timer and turn off the chip select.
			// otherwise, reading the data register will give another gap timer interurpt immediately.
			if ( 2 <= lLength )
			{
										// disable receive character gap timer
				(*(NARM_SER)).sc2.rct.bits.trun = 0;
			}

										// read FIFO data
			w32  = (WORD32)((*(NARM_SER)).sc2.fifo.reg);

										// if no errors
			if ( -1 != pGlobal->lSPIReadLength )
			{
				if ( 2 == lLength )
				{
					pGlobal->wSPIReadBuffer = (WORD16)(w32>>16);
					pGlobal->lSPIReadLength = lLength;
				}
				else
				{
					w32 = 43;
				}
			}
		}
										// get status register again for loop control
		SPIsr = (WORD32)((*(NARM_SER)).sc2.sr.reg);
	}

	SPIsr2  = (WORD32)((*(NARM_SER)).sc2.sr.reg);
	return;
}
//
// Write a A/D control word to the SPI interface
//
VOID fj_print_head_WriteSPI( LONG lChannel )
{
										// clear read length
	fj_CVT.pFJglobal->lSPIReadLength = 0;

										// should not occur
	if ( 1 == (*(NARM_SER)).sc2.sr.bits.txbc )
	{
										// clear buffer closed
		(*(NARM_SER)).sc2.sr.bits.txbc = 1;
	}
	if ( 1 == (*(NARM_SER)).sc2.sr.bits.txrdy )
	{
		// write control word
		*((WORD16*)&((*(NARM_SER)).sc2.fifo)) = (WORD16)(0x0808 | ((lChannel&0x7)<<7));
	}
										// should not occur
	if ( 1 == (*(NARM_SER)).sc2.sr.bits.txbc )
	{
		(*(NARM_SER)).sc2.sr.bits.txbc = 1;
	}

	// enable gap timer so that 2 bytes of input get read
	(*(NARM_SER)).sc2.rct.bits.trun = 1;// enable receive character gap timer

	return;
}
//
// Read the A/D data from the SPI interface.
// write the next A/D control word.
//
// The routine depends on the overall check loop having a built in delay
// to allow the SPI interface to read the data and process the interrupts.
//
VOID fj_print_head_ADRead(VOID)
{
	CLPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph    = fj_CVT.pfph;
	LONG    lAverage;
	LONG    lPlus;
	LONG    lDelta;
	LONG    lDeltai;
	WORD16  wTotal;
	WORD16  wRead;
	LONG    lADCount;
	LONG    lLength;
	BOOL    bAverage;
	WORD16  *pwAD;
	WORD16  (*pwADRaw)[8][FJ_AD_AVERAGE];
	LONG    lChannel;
	LONG    i,j;
	STRING  str[180];

	// the data returned during the last write was for the previous channel
	lChannel = pGlobal->lChannel - 1;
	if ( lChannel < 0 ) lChannel = 7;
	pwADRaw = &(pGlobal->wADRaw);
	pwAD = (WORD16 *)&(pGlobal->adValues);
	pwAD += lChannel;

	// start of read of data for previous A/D channel
	wRead   = pGlobal->wSPIReadBuffer;
	lLength = pGlobal->lSPIReadLength;
	pGlobal->wSPIReadBuffer = 0;
	pGlobal->lSPIReadLength = 0;

	lLengthA[lChannel] = lLength;
	wReadA[lChannel] = wRead;
	if ( 2 == lLength )
	{
		lADCount = pGlobal->lADCount[lChannel];
		bAverage = FALSE;

		if ( FJ_AD_AVERAGE != lADCount )// add in if not full of values yet
		{
			bAverage = TRUE;
										// if 1 less than full,
			if ( (FJ_AD_AVERAGE-1) == lADCount )
			{							//   do a sanity check
				lAverage = (*pwAD);		// average value so far
				lDeltai = 0;
										// find furthest from average
				for ( j = 0; j < (FJ_AD_AVERAGE-1); j++ )
				{
					lDelta = lAverage - (*pwADRaw)[lChannel][j];
					if ( 0 > lDelta ) lDelta = -lDelta;
					if ( lDelta > lDeltai )
					{
						lDeltai = lDelta;
						i = j;
					}
				}
										// if large delta, clear the rejected value
				if ( lDeltai > FJ_AD_DELTA )
				{
					(*pwADRaw)[lChannel][i] = (*pwADRaw)[lChannel][FJ_AD_AVERAGE-2];
					(*pwADRaw)[lChannel][FJ_AD_AVERAGE-2] = 0;
					lADCount--;
				}
			}
		}
		else
		{
			// check for value to be close to current average
			if ( ( wRead <= ((*pwAD)+FJ_AD_DELTA))  &&
				( wRead >= ((*pwAD)-FJ_AD_DELTA)) )
			{
				bAverage = TRUE;		// yes, use it
										// reset secondary table count
				pGlobal->lADCount2[lChannel] = 0;
			}
			else
			{
				bAverage = FALSE;
				if ( TRUE == pfph->bInfoAD )
				{
					sprintf( str,"Previous A/D (%d) average:%d  Current reading:%d\n",(int)lChannel,(int)(*pwAD),(int)wRead);
					fj_writeInfo( str );
				}
				// if secondary table is not full, insert this value
				if ( FJ_AD_AVERAGE > pGlobal->lADCount2[lChannel] )
				{
					pGlobal->wADRaw2[lChannel][pGlobal->lADCount2[lChannel]] = wRead;
					pGlobal->lADCount2[lChannel]++;
				}
				// if secondary table is half full, check for slide up or down
				if ( (FJ_AD_AVERAGE/2) == pGlobal->lADCount2[lChannel] )
				{
					lPlus = 0;
					for ( j = 1; j < (FJ_AD_AVERAGE/2); j++ )
					{
						if      ( pGlobal->wADRaw2[lChannel][j] > pGlobal->wADRaw2[lChannel][j-1] ) lPlus++;
						else if ( pGlobal->wADRaw2[lChannel][j] < pGlobal->wADRaw2[lChannel][j-1] ) lPlus--;
					}
					if      ( wRead > pGlobal->wADRaw2[lChannel][(FJ_AD_AVERAGE/2)-1] ) lPlus++;
					else if ( wRead < pGlobal->wADRaw2[lChannel][(FJ_AD_AVERAGE/2)-1] ) lPlus--;
					if ( lPlus < 0 ) lPlus = -lPlus;
					if ( lPlus >= ((FJ_AD_AVERAGE/2)-1) )
					{
						for ( j = 0; j < (FJ_AD_AVERAGE-(FJ_AD_AVERAGE/2)); j++ )
						{
							pGlobal->wADRaw[lChannel][j] = pGlobal->wADRaw[lChannel][(FJ_AD_AVERAGE-(FJ_AD_AVERAGE/2))+j];
						}
						for ( j = (FJ_AD_AVERAGE-(FJ_AD_AVERAGE/2)); j < FJ_AD_AVERAGE; j++ )
						{
							pGlobal->wADRaw[lChannel][j] = pGlobal->wADRaw2[lChannel][j-(FJ_AD_AVERAGE-(FJ_AD_AVERAGE/2))];
						}
						bAverage = TRUE;// yes, use it
										// reset secondary table count
						pGlobal->lADCount2[lChannel] = 0;
						if ( TRUE == pfph->bInfoAD )
						{
							sprintf( str,"Sliding A/D (%d)\n",(int)lChannel);
							fj_writeInfo( str );
						}
					}
				}
				// if secondary table is full, check individual values
				if ( FJ_AD_AVERAGE == pGlobal->lADCount2[lChannel] )
				{
					wTotal = 0;
					for ( j = 0; j < FJ_AD_AVERAGE; j++ )
					{
						wTotal += pGlobal->wADRaw2[lChannel][j];
					}
					lAverage = wTotal / FJ_AD_AVERAGE;

					lDeltai = 0;
										// find furthest from average
					for ( j = 0; j < FJ_AD_AVERAGE; j++ )
					{
						lDelta = lAverage - pGlobal->wADRaw2[lChannel][j];
						if ( 0 > lDelta ) lDelta = -lDelta;
						if ( lDelta > lDeltai )
						{
							lDeltai = lDelta;
							i = j;
						}
					}
										// if large delta, clear the rejected value
					if ( lDeltai > FJ_AD_DELTA )
					{
						pGlobal->wADRaw2[lChannel][i] = pGlobal->wADRaw2[lChannel][FJ_AD_AVERAGE-1];
						pGlobal->wADRaw2[lChannel][FJ_AD_AVERAGE-1] = 0;
						pGlobal->lADCount2[lChannel]--;
					}
					else				// we have jumped to a new, good average
					{
						// move values to prime table
						for ( j = 0; j < FJ_AD_AVERAGE; j++ )
						{
							pGlobal->wADRaw[lChannel][j] = pGlobal->wADRaw2[lChannel][FJ_AD_AVERAGE-1-j];
						}
										// reset secondary
						pGlobal->lADCount2[lChannel] = 0;
						bAverage = TRUE;
					}
				}
			}
		}

		if ( TRUE == bAverage )
		{
			for ( j = (FJ_AD_AVERAGE-1); j > 0; j-- )
			{
				(*pwADRaw)[lChannel][j] = (*pwADRaw)[lChannel][j-1];
			}
			(*pwADRaw)[lChannel][0] = wRead;
			if ( lADCount < FJ_AD_AVERAGE )
			{
				lADCount++;
				(pGlobal->lADCount[lChannel])++;
			}
			wTotal = 0;
			for ( j = 0; j < lADCount; j++ )
			{
				wTotal += (*pwADRaw)[lChannel][j];
			}
			*pwAD = wTotal / lADCount;	// put average value in the AD table
		}

		if ( FALSE == pGlobal->bADReady )
		{
			if ( (FJ_AD_AVERAGE == pGlobal->lADCount[0]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[1]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[2]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[3]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[4]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[5]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[6]) &&
				(FJ_AD_AVERAGE == pGlobal->lADCount[7]) )
			{
				pGlobal->bADReady = TRUE;
				if ( TRUE == pfph->bInfoAD ) fj_writeInfo( "A/D ready\n" );
				fj_print_head_SetStatus();
				fj_print_head_SendStatus();
			}
		}
	}
	// end of read of data from previous A/D channel

	// start of write of control word for next A/D channel
	lChannel = pGlobal->lChannel + 1;
	if ( lChannel > 7 )
	{
		lChannel = 0;
	}
	pGlobal->lChannel = lChannel;
	fj_print_head_WriteSPI( lChannel );

	return;
}
//
// initialize the SPI interface and the PORT registers
//
VOID fj_print_head_InitSPI(VOID)
{
	ULONG   lXtal;
	ULONG   lBitRate;
	ULONG   lTemp;
	volatile    WORD32 SPIcrA1;
	volatile    WORD32 SPIcrA2;
	volatile    WORD32 SPIcrB1;
	volatile    WORD32 SPIcrB2;
	volatile    WORD32 SPIsr1;
	volatile    WORD32 SPIsr2;
	volatile    WORD32 SPIbrg1;
	volatile    WORD32 SPIbrg2;
	volatile    WORD32 SPIrbt1;
	volatile    WORD32 SPIrbt2;
	volatile    WORD32 SPIrct1;
	volatile    WORD32 SPIrct2;
	volatile    WORD32 SPIportb1;
	volatile    WORD32 SPIportb2;
	volatile    WORD32 SPIportc1;
	volatile    WORD32 SPIportc2;

	lXtal = fj_CVT.pFJglobal->lXtalFreq;// crytal speed / 5
	NADisableIsr( SER2RX_INT );			// disable Serial Port 2 Receive interrupts
	NADisableIsr( SER2TX_INT );			// disable Serial Port 2 Transmit interrupts
	SPIcrA1 = (*(NARM_SER)).sc2.crA.reg;
	SPIcrB1 = (*(NARM_SER)).sc2.crB.reg;
	SPIsr1  = (*(NARM_SER)).sc2.sr.reg;
	SPIbrg1 = (*(NARM_SER)).sc2.brg.reg;
	SPIrbt1 = (*(NARM_SER)).sc2.rbt.reg;
	SPIrct1 = (*(NARM_SER)).sc2.rct.reg;
	(*(NARM_SER)).sc2.crA.reg = 0;		// reset & disable serial port 2
	SPIcrA2 = (*(NARM_SER)).sc2.crA.reg;

	(*(NARM_SER)).sc2.brg.reg = 0;		// reset bit rate generator
	SPIbrg2 =(*(NARM_SER)).sc2.brg.reg;
										// brg - 1X - synchronous
	(*(NARM_SER)).sc2.brg.bits.tmode = 1;
										// brg - receive clock internal
	(*(NARM_SER)).sc2.brg.bits.rxsrc = 0;
										// brg - transmit clock internal
	(*(NARM_SER)).sc2.brg.bits.txsrc = 0;
										// brg - receive clock external disabled
	(*(NARM_SER)).sc2.brg.bits.rxext = 0;
										// brg - transmit clock external at PORTC/OUT2
	(*(NARM_SER)).sc2.brg.bits.txext = 1;
										// brg - receive clock inverted
	(*(NARM_SER)).sc2.brg.bits.rxcinv = 1;
										// brg - transmit clock normal - not inverted
	(*(NARM_SER)).sc2.brg.bits.txcinv = 0;
										// brg - input clock = external crystal / 5
	(*(NARM_SER)).sc2.brg.bits.clkmux = 00;
	// NOTE: the above bit for txcinv does not invert the clock as needed by our A/D converter.
	//       this bug will be fixed on the 50 mhz chip.
	//       for now, the signal is inverted in the GateArray.
	lBitRate =   9600;
	lBitRate =  57600;
	lBitRate = 115200;
	lBitRate = 230400;
	//  lBitRate = 460800;						// too high - noisy. do not use
										// brg - clock rate
	(*(NARM_SER)).sc2.brg.bits.nreg = lXtal/lBitRate/2-1;
	(*(NARM_SER)).sc2.brg.bits.ebit = 1;// brg - enable the clock
	SPIbrg2 = (*(NARM_SER)).sc2.brg.reg;

	(*(NARM_SER)).sc2.rbt.reg = 0;		// reset receive buffer gap timer
	SPIrbt2 = (*(NARM_SER)).sc2.rbt.reg;
	//    lTemp = (((4*lXtal*11)/10)/(lBitRate*512));	// receive buffer timer value
	//    if ( lTemp > 0 ) lTemp--;
	//    (*(NARM_SER)).sc2.rbt.bits.bt = lTemp;	// receive buffer timer value
	(*(NARM_SER)).sc2.rbt.bits.trun = 0;// disable receive buffer gap timer
	SPIrbt2 = (*(NARM_SER)).sc2.rbt.reg;

	(*(NARM_SER)).sc2.rct.reg = 0;		// reset receive character gap timer
	SPIrct2 = (*(NARM_SER)).sc2.rct.reg;
										// receive character timer value
	lTemp = (((10*lXtal))/(lBitRate*512));
	if ( lTemp > 0 ) lTemp--;
	lTemp += 1;							// make it a bit bigger to insure we get both bytes at once
										// receive character timer value
	(*(NARM_SER)).sc2.rct.bits.ct = lTemp;
	(*(NARM_SER)).sc2.rct.bits.trun = 1;// enable receive character gap timer
	SPIrct2 = (*(NARM_SER)).sc2.rct.reg;

	(*(NARM_SER)).sc2.crB.reg = 0;		// reset control register B
	SPIcrB2 = (*(NARM_SER)).sc2.crB.reg;
	(*(NARM_SER)).sc2.crB.bits.rbgt = 1;// enable receive buffer gap timer
	(*(NARM_SER)).sc2.crB.bits.rcgt = 1;// enable receive character gap timer
	(*(NARM_SER)).sc2.crB.bits.mode = 2;// SPI master mode
										// MSB first on read/write
	(*(NARM_SER)).sc2.crB.bits.bitordr = 1;
	SPIcrB2 = (*(NARM_SER)).sc2.crB.reg;

	(*(NARM_SER)).sc2.crA.bits.wls = 3;	// 8-bit operation
	SPIcrA2 = (*(NARM_SER)).sc2.crA.reg;
	(*(NARM_SER)).sc2.crA.bits.ce = 1;	// enable serial port 2

	SPIcrA2 = (*(NARM_SER)).sc2.crA.reg;
	SPIcrB2 = (*(NARM_SER)).sc2.crB.reg;
	SPIsr2  = (*(NARM_SER)).sc2.sr.reg;
	SPIbrg2 = (*(NARM_SER)).sc2.brg.reg;
	SPIrbt2 = (*(NARM_SER)).sc2.rbt.reg;
	SPIrct2 = (*(NARM_SER)).sc2.rct.reg;
	SPIportb1 = (*(NARM_GEN)).portb.reg;
	SPIportc1 = (*(NARM_GEN)).portc.reg;
										// PORTB7 - SPI TXD
	(*(NARM_GEN)).portb.bits.mode |=  0x80;
										// PORTB7 - SPI TXD as output
	(*(NARM_GEN)).portb.bits.dir  |=  0x80;
										// PORTB4 - SPI Enable
	(*(NARM_GEN)).portb.bits.mode |=  0x10;
										// PORTB4 - SPI Enable as output
	(*(NARM_GEN)).portb.bits.dir  |=  0x10;
										// PORTB3 - SPI RXD
	(*(NARM_GEN)).portb.bits.mode |=  0x08;
										// PORTB3 - SPI RXD as input
	(*(NARM_GEN)).portb.bits.dir  &= ~0x08;
										// PORTC5 - SPI CLK
	(*(NARM_GEN)).portc.bits.mode |=  0x20;
										// PORTC5 - SPI CLK as output
	(*(NARM_GEN)).portc.bits.dir  |=  0x20;
	SPIportb2 =(*(NARM_GEN)).portb.reg;
	SPIportc2 = (*(NARM_GEN)).portc.reg;

	NAInstallIsr( SER2RX_INT, fj_print_head_spiR_interrupt, 0 );
										// enable receive break interrupt
	(*(NARM_SER)).sc2.crA.bits.erxbrk  = 1;
										// enable framing error interrupt
	(*(NARM_SER)).sc2.crA.bits.erxfe   = 1;
										// enable parity error interrupt
	(*(NARM_SER)).sc2.crA.bits.erxpe   = 1;
										// enable overrun interrupt
	(*(NARM_SER)).sc2.crA.bits.erxorun = 1;
										// enable fifo register ready interrupt
	(*(NARM_SER)).sc2.crA.bits.erxdrdy = 1;
										// enable buffer closed interrupt
	(*(NARM_SER)).sc2.crA.bits.erxbc   = 1;
	SPIcrA2 = (*(NARM_SER)).sc2.crA.reg;
	return;
}
