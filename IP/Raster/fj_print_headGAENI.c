#include "fj_netsilicon.h"
#include <na_isr.h>
#include <eni_api.h>
#include <nccDma.h>
#include "fj.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"

extern BOOL fj_rasterLoop;				// if true, have a background loop running to generate a load and measure throughput
extern LONG fj_lHSClocksPerMillisecond;

// there is an 8k limit on the ENI/DMA interface
// so 8000 seems like a good round number
#define ENI_BLANK_RASTERS  20			// number of blank rasters to use when idle
#define ENI_BUFFER_SIZE    ((8000/FJSYS_NOZZLES)*FJSYS_NOZZLES)

typedef struct fjeni
{
	eniMessageType *peni;				// pointer to eniMessageType
	LONG            lRasters;			// number of rasters in buffer
	BOOL            bZero;				// buffer is zeros
	BOOL            bInUse;				// buffer is in use
} FJENI, FAR *LPFJENI;

LONG   lENIchannel;
LONG   lENIrasters;						// the number of rasters that will fit in the buffer
LONG   lENIbytes;						// the number of bytes in the above rasters
LONG   lENIzerorasters;					// the number of blank rasters used when idle
LONG   lENIzerobytes;					// the number of blank bytes used when idle
LPBYTE pENIbuff1 = NULL;
LPBYTE pENIbuff2 = NULL;
//LPBYTE pENIbuff3 = NULL;
//LPBYTE pENIbuff4 = NULL;
eniMessageType eniM1;
eniMessageType eniM2;
//eniMessageType eniM3;
//eniMessageType eniM4;

FJENI fjeni1;
FJENI fjeni2;
//FJENI fjeni3;
//FJENI fjeni4;
FJENI* pcfjeni = NULL;					// current fjeni
static LONG  alENICounters[100] = {0};

// redefine NETsilicon symbolic interrupt numbers to names meaningful to the FoxJet code
// original defines are in na_isr.h
#define FJ_FIFO_REC_INT      PC_PORT1_INT
#define FJ_FIFO_TRANS_INT    PC_PORT2_INT
#define FJ_ENI_INTERRUPT_INT PC_PORT3_INT
#define FJ_ENI_BUS_ERROR_INT PC_PORT4_INT

// save area for values used by both sides of the FIFO interface
static LONG  fj_lFIFONozzles     = 32;	// number of nozzles on selected printhead
static LONG  fj_lFIFONozzleBytes =  4;	// number of bytes to contain these nozzles
static LONG  fj_lFIFORasterBytes =  0;	// number of bytes used in current raster
static LONG  fj_lFIFORasterOffset;		// number of rasters in offset print head
//static LONG  fj_lENIbytesrem = 0;

// the RT clock will not use DST.
// the fj_mktime function will be used in place of the standard mktime function.
// fj_mktime will use the C library version of mktime.
// if mktime adjusts for DST and DST is not desired,
// fj_mktime will undo the DST.

//
// set a time into the RT Clock
//
VOID fj_print_head_SetRTClockTM( struct tm *pTM )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	volatile LPGA pga     = fj_CVT.pGA;	// get base address
	WORD8 b;

	if ( TRUE == pGlobal->bGAAvail )
	{
		// Oscillator on | no dividers | Bank 1
		//		pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_A;
		//		pga->rw.write.RTclockData = FJ_RT_REGISTER_A_DV1 | FJ_RT_REGISTER_A_DV0;
		// SET | DataBinary | 24-hour | no daylight savings | no interrupts
		pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
		pga->rw.write.RTclockData = FJ_RT_REGISTER_B_SET | FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;

		pga->rw.write.RTclockAdd  = FJ_RT_CENTURY;
		pga->rw.write.RTclockData = 20;
		pga->rw.write.RTclockAdd  = FJ_RT_YEAR;
										// tm is 1900 origin.
		pga->rw.write.RTclockData = pTM->tm_year - 100;
		pga->rw.write.RTclockAdd  = FJ_RT_MONTH;
										// tm month is 0 origin. RT clock is 1 origin.
		pga->rw.write.RTclockData = pTM->tm_mon + 1;
		pga->rw.write.RTclockAdd  = FJ_RT_DAY_OF_MONTH;
		pga->rw.write.RTclockData = pTM->tm_mday;
		pga->rw.write.RTclockAdd  = FJ_RT_HOURS;
		pga->rw.write.RTclockData = pTM->tm_hour;
		pga->rw.write.RTclockAdd  = FJ_RT_MINUTES;
		pga->rw.write.RTclockData = pTM->tm_min;
		pga->rw.write.RTclockAdd  = FJ_RT_SECONDS;
		pga->rw.write.RTclockData = pTM->tm_sec;
		pga->rw.write.RTclockAdd  = FJ_RT_DAY_OF_WEEK;
										// tm wday is 0 origin. RT clock is 1 origin.
		pga->rw.write.RTclockData = pTM->tm_wday + 1;

		// start the clock
		b = FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;
		pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
		pga->rw.write.RTclockData = b;

		fj_CVT.pFJglobal->gaWrite.rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
		fj_CVT.pFJglobal->gaWrite.rw.write.RTclockData = b;
	}
	else
	{
		memcpy( &(pGlobal->tmGA), pTM, sizeof(struct tm) );
		pGlobal->lGASeconds = pGlobal->lSeconds;
	}

	return;
}
//
// get the TOD out of the RT Clock
//
VOID fj_print_head_GetRTClockTMRegisters( struct tm *pTM )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	volatile LPGA pga     = fj_CVT.pGA;	// get base address

	if ( TRUE == pGlobal->bGAAvail )
	{
		pga->rw.write.RTclockAdd  = FJ_RT_YEAR;
										// TM year is 1900 origin
		pTM->tm_year = pga->rw.read.RTclockData + 100;
		pga->rw.write.RTclockAdd  = FJ_RT_MONTH;
										// TM month is 0 origin
		pTM->tm_mon  = pga->rw.read.RTclockData - 1;
		pga->rw.write.RTclockAdd  = FJ_RT_DAY_OF_MONTH;
		pTM->tm_mday = pga->rw.read.RTclockData;
		pga->rw.write.RTclockAdd  = FJ_RT_DAY_OF_WEEK;
										// TM wday is 0 origin
		pTM->tm_wday = pga->rw.read.RTclockData - 1;
		pga->rw.write.RTclockAdd  = FJ_RT_HOURS;
		pTM->tm_hour = pga->rw.read.RTclockData;
		pga->rw.write.RTclockAdd  = FJ_RT_MINUTES;
		pTM->tm_min  = pga->rw.read.RTclockData;
		pga->rw.write.RTclockAdd  = FJ_RT_SECONDS;
		pTM->tm_sec  = pga->rw.read.RTclockData;
		//save
		fj_CVT.pFJglobal->gaRead.rw.write.RTclockAdd = FJ_RT_SECONDS;
		//save
		fj_CVT.pFJglobal->gaRead.rw.read.RTclockData = pTM->tm_min;
	}
	else
	{
		memcpy( pTM, &(pGlobal->tmGA), sizeof(struct tm) );
		pTM->tm_sec +=  pGlobal->lSeconds - pGlobal->lGASeconds;
		fj_mktime( pTM );				// normalize everyting - DST etc.
	}

	return;
}
//
// get the TOD out of the RT Clock
//
struct tm * fj_print_head_GetRTClockTM( struct tm *pTM )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
	volatile LPGA pga     = fj_CVT.pGA;	// get base address
	WORD8  wRegD;
	WORD8  wCentury;
	struct tm *pTMRet;
	BOOL   bClockOKOld;

	pTMRet = pTM;
	bClockOKOld = pGlobal->bClockOK;

	// Oscillator on | no dividers | Bank 1
	//	pga->rw.read.RTclockAdd  = FJ_RT_REGISTER_A;
	//	pga->rw.read.RTclockData = FJ_RT_REGISTER_A_DV1 | FJ_RT_REGISTER_A_DV0;
	// SET | DataBinary | 24-hour | no daylight savings | no interrupts
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
	pga->rw.write.RTclockData = FJ_RT_REGISTER_B_SET | FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;

	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_D;
	wRegD = pga->rw.read.RTclockData;
	pga->rw.write.RTclockAdd  = FJ_RT_CENTURY;
	wCentury = pga->rw.read.RTclockData;
	fj_print_head_GetRTClockTMRegisters( pTM );
	if ( FALSE == pGlobal->bGAAvail )
	{
		wRegD = FJ_RT_REGISTER_D_VRT;
		wCentury = 20;
	}

	// the following tests were based on testing uninitialized clock chips
										// is century equal to 20
	if ( ( 20 == wCentury                      ) &&
										// and year greater than 100 ( 2000 )
		(100 <  pTM->tm_year                  ) &&
										// and year less than 200 ( 2100 )
		(200 >  pTM->tm_year                  ) &&
										// and seconds less than 60
		( 60 >  pTM->tm_sec                   ) )
	{
		pTM->tm_yday = 0;
		pTM->tm_isdst = 0;
		fj_mktime( pTM );
		pGlobal->bClockOK = TRUE;
	}
	else
	{
		fj_print_head_GetRTClockTMRegisters( pTM );
										// is century equal to 20
		if ( ( 20 == wCentury                      ) &&
										// and year greater than 100 ( 2000 )
			(100 <  pTM->tm_year                  ) &&
										// and year less than 200 ( 2100 )
			(200 >  pTM->tm_year                  ) &&
										// and seconds less than 60
			( 60 >  pTM->tm_sec                   ) )
		{
			pTM->tm_yday = 0;
			pTM->tm_isdst = 0;
			fj_mktime( pTM );
			pGlobal->bClockOK = TRUE;
		}
		else
		{
			pTMRet = NULL;
			pGlobal->bClockOK = FALSE;
		}
	}
	// re-start the register updates by turning off the SET bit
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
	pga->rw.write.RTclockData = FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;
	// save for completeness, but does not mean much
	pGlobal->gaWrite.rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
	// save for completeness, but does not mean much
	pGlobal->gaWrite.rw.write.RTclockData = FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;

	if ( pGlobal->bClockOK != bClockOKOld )
	{
		fj_print_head_SendClockInfo();
		if ( TRUE == pGlobal->bClockOK )
		{
			if ( 0 == pBRAM->tmLastPrint.tm_year     ) memcpy( &(pBRAM->tmLastPrint),     pTM, sizeof(struct tm) );
			if ( 0 == pBRAM->tmCustomerReset.tm_year ) memcpy( &(pBRAM->tmCustomerReset), pTM, sizeof(struct tm) );
			if ( 0 == pBRAM->tmInkFull.tm_year       ) memcpy( &(pBRAM->tmInkFull),       pTM, sizeof(struct tm) );
			if ( 0 == pBRAM->tmInkLow.tm_year        ) memcpy( &(pBRAM->tmInkLow),        pTM, sizeof(struct tm) );
		}
	}

	return( pTMRet );
}
//
// get the Battery indicator out of the RT Clock
//
// Battery flag is set in the Global structure
//
VOID fj_print_head_GetRTClockBattery( VOID )
{
	volatile LPGA pga = fj_CVT.pGA;		// get base address
	WORD8  wRegD;

	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_D;
	wRegD = pga->rw.read.RTclockData;
	if ( FALSE == fj_CVT.pFJglobal->bGAAvail )
	{
		wRegD = FJ_RT_REGISTER_D_VRT;
	}

										// is RT clock valid?
	if ( 0 == (wRegD & FJ_RT_REGISTER_D_VRT) )
	{
										// no
		fj_CVT.pFJglobal->bBatteryOK = FALSE;
	}
	else
	{
										// yes
		fj_CVT.pFJglobal->bBatteryOK = TRUE;
	}
	// save for completeness, but does not mean much
	fj_CVT.pFJglobal->gaRead.rw.read.RTclockAdd  = FJ_RT_REGISTER_D;
										// save for completeness, but does not mean much
	fj_CVT.pFJglobal->gaRead.rw.read.RTclockData = wRegD;

	return;
}
//
// set the encoder value into the Gate Array
//
VOID fj_print_head_SetGAWriteSwitches( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	GA_READ_STATUS wStatus;
	GA_WRITE_SWITCHES sw;
	LONG lAutoPrint;

	wStatus = pGlobal->gaRead.rw.read.status;
	lAutoPrint = fj_CVT.pfph->pfop->lAutoPrint;

	sw.all = 0;
	if ( TRUE == fj_CVT.pBRAM->bInkLow            ) sw.bits.ErrorLED     = 1;
	if ( TRUE == fj_CVT.pFJglobal->bPhotocellInt  ) sw.bits.PhotoCellInt = 1;
	if ( TRUE == fj_CVT.pFJglobal->bHighVoltageOn ) sw.bits.HVswitch     = 1;
	if ( TRUE == fj_CVT.pFJglobal->bStandByOn     ) sw.bits.Vibrate      = 1;
	if ( TRUE == fj_CVT.pfsys->bShaftEncoder      ) sw.bits.ShaftEncoder = 1;
	if ( TRUE == fj_CVT.pfph->bPhotocellBack      ) sw.bits.MasterSlave  = 1;
	if ( TRUE == fj_CVT.pfph->bPhotocellInternal  ) sw.bits.InternalPC   = 1; // Burl v 1.1015

	// if system is not ready flash Error LED
	if ( FALSE == fj_CVT.pFJglobal->bReady )
	{
		sw.bits.ErrorLED     = 1;
		sw.bits.FlashErrorLED= 1;
	}

	// Burl v 1.1015
	fj_CVT.pGA->rw.write.switches.bits.InternalPC           	= 0;
	fj_CVT.pFJglobal->gaWrite.rw.write.switches.bits.InternalPC     = 0;
	fj_CVT.pGA->rw.write.AutoPrint                  		= lAutoPrint;
	fj_CVT.pFJglobal->gaWrite.rw.write.AutoPrint            	= lAutoPrint;
	// Burl v 1.1015

	// save
	fj_CVT.pGA->rw.write.switches = sw;
	fj_CVT.pFJglobal->gaWrite.rw.write.switches = sw;

	return;
}
//
// set the number of nozzles/channels (in bytes) into the Gate Array
//
//
VOID fj_print_head_SetGAChannels( VOID )
{
										// save number of nozzles
	fj_lFIFONozzles = fj_CVT.pfph->pfphy->lChannels;
	if ( 0 == fj_lFIFONozzles ) fj_lFIFONozzles = 32;
										// save number of bytes
	fj_lFIFONozzleBytes = (fj_lFIFONozzles+7)/8;
										// write number of bytes in interface
	fj_CVT.pGA->rw.write.channels = fj_lFIFONozzleBytes;
	// save
	fj_CVT.pFJglobal->gaWrite.rw.write.channels = fj_lFIFONozzleBytes;
	return;
}
//
// set the PWM high voltage value into the Gate Array
//
VOID fj_print_head_SetGAHV( VOID )
{
	fj_CVT.pGA->rw.write.pwmHV = fj_CVT.pFJglobal->lPWMHV;
	// save
	fj_CVT.pFJglobal->gaWrite.rw.write.pwmHV = fj_CVT.pFJglobal->lPWMHV;
	return;
}
//
// set the encoder value into the Gate Array
//
VOID fj_print_head_SetGAEncoder( VOID )
{
	volatile LPGA pga  = fj_CVT.pGA;
	CLPCFJSYSTEM pfsys = fj_CVT.pfsys;
	ULONG      lTestFrequency;

	lTestFrequency = (LONG)(pfsys->fPrintResolution * pfsys->fSpeed);

	if ( TRUE == pfsys->bShaftEncoder )
	{
		lTestFrequency = 0;
	}
	else
	{
		lTestFrequency = fj_CVT.pFJglobal->lGAClockFreq1 / lTestFrequency;
		// the gate array does a flip-flop at this frequency. we want a full cycle.
		lTestFrequency = ( lTestFrequency / 2 );
	}

	pga->rw.write.encoder = lTestFrequency;
	// save
	fj_CVT.pFJglobal->gaWrite.rw.write.encoder = lTestFrequency;

	pga->rw.write.enc_divisor = pfsys->lEncoderDivisor;
	// save
	fj_CVT.pFJglobal->gaWrite.rw.write.enc_divisor = pfsys->lEncoderDivisor;
	return;
}
//
// set the three curve values into the Gate Array
//
VOID fj_print_head_SetGACurve( VOID )
{
	volatile LPGA pga = fj_CVT.pGA;
	LPFJPRINTHEAD_OP pfop;
	FLOAT   fUnits;
	FLOAT   fValue;
	ULONG   lValue;

	pfop = fj_CVT.pfph->pfop;

	if ( TRUE == fj_CVT.pfph->bHeadSelected )
	{
		// newer, higher res
		fUnits =  500000.0 / fj_CVT.pFJglobal->lGAClockFreq1;

		fValue = pfop->fCurve1 / fUnits;
		lValue = fValue + .49*fUnits;
		pga->rw.write.curve1 = lValue;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.curve1 = lValue;

		fValue = pfop->fCurve2 / fUnits;
		lValue = fValue + .49*fUnits;

		if ( lValue < 4) lValue = 4;
		pga->rw.write.curve2 = lValue;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.curve2 = lValue;

		fValue = pfop->fCurve3 / fUnits;
		lValue = fValue + .49*fUnits;
		pga->rw.write.curve3 = lValue;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.curve3 = lValue;
	}
	return;
}
//
// set the AMS values into the Gate Array
//
VOID fj_print_head_SetGAAMS( VOID )
{
	volatile LPGA pga = fj_CVT.pGA;
	LPFJPRINTHEAD_OP pfop;
	LONG    lA1, lA2, lA3, lA4, lA5, lA6, lA7;

	lA1 = lA2 = lA3 = lA4 = lA5 = lA6 = lA7 = 0;

	pfop = fj_CVT.pfph->pfop;

	if ( TRUE == fj_CVT.pfph->bHeadSelected )
	{
		if ( TRUE == pfop->bAMS )
		{
			lA1 = pfop->lAMSA1;
			lA2 = pfop->lAMSA2;
			lA3 = pfop->lAMSA3;
			lA4 = pfop->lAMSA4;
			lA5 = pfop->lAMSA5;
			lA6 = pfop->lAMSA6;
			lA7 = pfop->lAMSA7;
		}
		pga->rw.write.AMSA1 = lA1;
		pga->rw.write.AMSA2 = lA2;
		pga->rw.write.AMSA3 = lA3;
		pga->rw.write.AMSA4 = lA4;
		pga->rw.write.AMSA5 = lA5;
		pga->rw.write.AMSA6 = lA6;
		pga->rw.write.AMSA7 = lA7;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA1 = lA1;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA2 = lA2;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA3 = lA3;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA4 = lA4;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA5 = lA5;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA6 = lA6;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AMSA7 = lA7;
	}
	return;
}
//
// set all the values into the Gate Array.
//
VOID fj_print_head_SetGAValues( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	fj_print_head_SetGAAMS();
	fj_print_head_SetGAChannels();
	fj_print_head_SetGAEncoder();
	fj_print_head_SetGACurve();
	fj_print_head_SetGAHV();
	fj_print_head_SetGAWriteSwitches();
	return;
}
static ULONG  lRasterCounterPrev  = 0;
static ULONG  lRasterCounterCheck = 0;
static ULONG  lRasterCounterCheckTime  = 0;

//
// update the global raster counter from the GA
//
VOID fj_print_head_UpdateRasterCounter( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	ULONG lRasterCounterNow;
	ULONG lDelta;
	int   iOld;

										// disable to insure no interrupts
	iOld = tx_interrupt_control( TX_INT_DISABLE );

	lRasterCounterNow = fj_CVT.pGA->rw.read.rastercounter;
	// save
	pGlobal->gaRead.rw.read.rastercounter = lRasterCounterNow;

	if ( lRasterCounterNow >= lRasterCounterPrev ) lDelta =           lRasterCounterNow - lRasterCounterPrev;
	else                                           lDelta = 0x10000 + lRasterCounterNow - lRasterCounterPrev;
	lRasterCounterPrev = lRasterCounterNow;

	pGlobal->lRasterCount += lDelta;
	lRasterCounterCheck   += lDelta;

	// the photocell counter is used to "disable" the photocell for the length of a box.
	// multiple photocell incidents within the length of a box is illogical.

	if ( 0 != pGlobal->lPhotocellCount )
	{
										// drop photocell counter
		pGlobal->lPhotocellCount -= lDelta;

		if ( pGlobal->lPhotocellCount < 0 ) {
		    pGlobal->lPhotocellCount = 0;
		}

		if (pGlobal->lPhotocellCount == 0) { // burl
		    if (pGlobal->bPhotocellInt == FALSE) { // burl
			pGlobal->bPhotocellInt = TRUE;
			fj_print_head_SetGAWriteSwitches ();
		    }
		}
	}
	
	tx_interrupt_control( iOld );		// return to previous enabled state
	return;
}
//
// check the shaft encoder for movement by looking at the rastercounter
//
VOID fj_print_head_ShaftEncoderCheck( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPCFJSYSTEM  pfsys   = fj_CVT.pfsys;
	BOOL bSERunning;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;
										// save
	fj_CVT.pFJglobal->gaRead.rw.read.status = wStatus;
	if (1 == wStatus.bits.SEstatus)
		bSERunning = TRUE;
	else
		bSERunning = FALSE;

	if (bSERunning != pGlobal->bShaftEncoderRunning)
	{
		pGlobal->bShaftEncoderRunning = bSERunning;
		fj_print_head_SendShaftEncoderInfo();
	}

	return;
}
LONG lRCPC0;
LONG lRCPC1;
LONG lRCPC2;
LONG lRCPC3;

//
// process a PhotoCell detect
//
VOID fj_print_head_PhotoCell( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	if ((TRUE == pGlobal->bReady))		//&& (FALSE == pCVT->pBRAM->bPrintIdle))
	{
		// set photocell raster counter to take into account the time/rasters spent generating the message
		lRCPC0 = pGlobal->lRasterCount;
		fj_print_head_UpdateRasterCounter();
		lRCPC1 = pGlobal->lRasterCount;
		lRCPC2 = 0;
		lRCPC3 = 0;
		pGlobal->lRasterCountPC = pGlobal->lRasterCount = 0;
		// set photocell time into system
		if ( FJ_PC_MODE_ENABLED == pGlobal->lPhotocellMode ) fj_SystemGetTime( &(fj_CVT.pfsys->tmPhotoCell) );
		// start message generation and printing on high-priority thread
		pGlobal->bPrint = TRUE;
		fj_print_head_UpdateRasterCounter();
		lRCPC2 = pGlobal->lRasterCount;
		tx_thread_resume( &(pGlobal->thrPrint) );
		fj_print_head_UpdateRasterCounter();
		lRCPC3 = pGlobal->lRasterCount;

		// test for first use and set inservice date
		if ( 0 == fj_CVT.pfph->tmDateInservice.tm_year )
		{
			fj_SystemGetTime( &(pGlobal->tmNewInservice) );
		}
	}
	return;
}
static int  iPCPrev = 0;

VOID fj_print_head_InitPhotoCell( VOID )
{
	// save
	fj_CVT.pFJglobal->gaRead.rw.read.status.all = fj_CVT.pGA->rw.read.status.all;
	iPCPrev = fj_CVT.pFJglobal->gaRead.rw.read.status.bits.PhotoCell;
	if ( FALSE == fj_CVT.pFJglobal->bGAAvail ) iPCPrev = 0;
	return;
}
//
// check for a photocell transition
//
// send a status message to reflect the state of the photocell
//
// the normal/unblocked state of the photocell is high/1
// the FPGA generates an interrupt when the photcell goes from high/1 to low/0
// this routine will also be called shortly thereafter
//
// NOTE: this routine was originally written to be called from any thread.
//       it may now be called only from the print thread, making the tx_thread_resume calls redundant.
//       they are left in in case this is ever called from outside the print thread.
//
static BOOL bPhotocellOff;
VOID fj_print_head_PhotoCellCheck( VOID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPCFJSYSTEM  pfsys   = fj_CVT.pfsys;
	int   iPCNow;

	// save
	pGlobal->gaRead.rw.read.status.all = fj_CVT.pGA->rw.read.status.all;
	iPCNow = pGlobal->gaRead.rw.read.status.bits.PhotoCell;
										// set to unblocked if no real GA
	if ( FALSE == fj_CVT.pFJglobal->bGAAvail ) iPCNow = 1;
	// just in case there are very fast photocell transitions,
	// we ALWAYS want to send a message that the photocell came on - even if briefly.
	// the ENI interrupt routine sets pGlobal->bSendPhotocell just to let this routine know this.
	// force the photocell on if this is on.
	if ( TRUE == pGlobal->bSendPhotocell )
	{
		iPCNow = 0;
		bPhotocellOff = TRUE;
	}
	if ( (TRUE == pGlobal->bSendPhotocell) || (TRUE == bPhotocellOff) )
	{
		if ( (FALSE == pGlobal->bSendPhotocell) && (1 == iPCNow) ) bPhotocellOff = FALSE;

		pGlobal->bSendPhotocell = FALSE;
		if ( iPCNow != iPCPrev )
		{
			// photocell has changed
			if ( 0 == iPCNow )
			{
				// the photocell is now low/blocked/0
				pGlobal->bPhotocell = TRUE;
			}
			else
			{
				pGlobal->bPhotocell = FALSE;
			}
			iPCPrev = iPCNow;
			fj_print_head_SendPhotocellInfo();
		}
	}

	return;
}
static LONG  lMessageCounter = 0;
static LONG  lMessageCounterPrinting = 0;
static LONG  lMessageCounterCompleted = 0;

//
// check the image for completion
// remove from chain if all rasters printed
//
VOID fj_print_head_RasterCheck(VOID)
{
	LPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD  pfph = fj_CVT.pfph;
	LPFJIMAGE      pfi;
	LPFJPRINTCHAIN pfc;
	LPFJPRINTCHAIN pfcNext;
	LONG lBytes;
	BOOL bFreePfc;

	if ( NULL != pfph )
	{
		tx_semaphore_get( &(pGlobal->semaPrintChainLock), TX_WAIT_FOREVER );
		pfc = pfph->pfcFirst;
		if ( NULL != pfc )
		{
			bFreePfc = TRUE;
			while ( (NULL != pfc) && (TRUE == bFreePfc) )
			{
				pfcNext = pfc->pNext;
				pfi = pfc->pfi;
				if ( NULL != pfi )
				{
					lBytes  = (pfi->lHeight + 7) / 8;
					//pfi->lLength )
					if ( (pfc->lRasterCount*lBytes) < (pfi->lLength +(fj_lFIFORasterOffset*lBytes)))
					{
						bFreePfc = FALSE;
					}
				}
				if ( TRUE == bFreePfc )
				{
					if ( NULL == pfcNext ) pfph->pfcLast = NULL;
					pfph->pfcFirst = pfcNext;
					if ( (NULL == pfcNext) || (pfc->lMessageCounter != pfcNext->lMessageCounter) )
					{
						lMessageCounterCompleted = pfc->lMessageCounter;
					}
					if ( NULL != pfi ) fj_ImageRelease( pfi );
					fj_free( pfc );
				}
				pfc = pfcNext;
			}
		}
		tx_semaphore_put( &(pGlobal->semaPrintChainLock) );
	}
	return;
}
//
//	add to the active print chain
//
VOID fj_print_head_PrintChainAdd( LPFJPRINTCHAIN pfcFirst, LPFJPRINTCHAIN pfcLast )
{
	LPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	CLPFJPRINTHEAD pfph  = fj_CVT.pfph;

	tx_semaphore_get( &(pGlobal->semaPrintChainLock), TX_WAIT_FOREVER );
	if ( NULL == pfph->pfcLast )
	{
		pfph->pfcLast  = pfcLast;
		pfph->pfcFirst = pfcFirst;
	}
	else
	{
		pfph->pfcLast->pNext = pfcFirst;
		pfph->pfcLast = pfcLast;
	}
	tx_semaphore_put( &(pGlobal->semaPrintChainLock) );

	return;
}
//
//   print a message
//   add the message's FJIMAGE structures to the print chain
//
//   the FJIMAGE structures are in no particular order.
//   the first FJIMAGE may be far down the box.
//   if they are queued one at a time,
//   the ENI routine might build a buffer with a lot of blanks before this one - where other FJIMAGEs should go.
//   so the PRINTCHAIN structures are built here and added to the printhead together.
//
VOID fj_print_head_PrintMessage( LPFJMESSAGE pfm )
{
	LPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	CLPCFJSYSTEM   pfsys = fj_CVT.pfsys;
	CLPFJPRINTHEAD pfph  = fj_CVT.pfph;
	LPFJELEMENT   pfe;
	LPFJIMAGE      pfi;
	LPFJIMAGE      pfiPrev;
	LPFJPRINTCHAIN pfc;
	LPFJPRINTCHAIN pfcFirst;
	LPFJPRINTCHAIN pfcLast;
	FLOAT  fRes;
	LONG   lDistPC;
	LONG   lRowWait;
	LONG   lSlantWait;
	LONG   lBoxWidth;
	LONG   lRasterWidth;				// width of image without slant
	LONG   lRasterLeft;					// where left side of printing should go on box
	LONG   lRasterRight;				// where right side of printing should go on box
	LONG   lRasterLeftPrev;
	LONG   lRasterRightPrev;
	LONG   lRasterWait;
	LONG   lRasterMin;
	LONG   lWaitAdjusted;
	LONG   lRC0;
	LONG   lRC1;
	LONG   lRC2;
	LONG   lRC3;
	LONG   lRC4;
	LONG   lRC5;
	int    iSeg;

	pfc = pfcFirst = pfcLast = NULL;
	fj_print_head_UpdateRasterCounter();
	lRC0 = pGlobal->lRasterCount;
	lRC1 = lRC2 = lRC3 = lRC4 = lRC5 = 0;
	if ( FJSYS_MAX_MESSAGES > (lMessageCounter - lMessageCounterCompleted) )
	{
		lMessageCounter++;
		if ( TRUE == pfsys->bShaftEncoder ) fRes = pfsys->fEncoder;
		else                                fRes = pfsys->fPrintResolution;
		lBoxWidth = 0;
		if ( NULL != pfsys->pfl ) lBoxWidth = pfsys->pfl->fBoxWidth * fRes;

										// distance of nozzle 0 downstream from photocell
		lDistPC = pfph->fDistancePC * fRes;

		pfiPrev = NULL;
										// check all segments
		for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
		{
			pfe = pfm->papfe[iSeg];
			pfi = pfe->pfi;
			if ( NULL != pfi )
			{
			    // burl 1.2015
			    LONG lAdjustHeight = pfi->lHeight - pfi->lShifted;
			    ldiv_t d = ldiv (lAdjustHeight, 8);
			    LONG lAdjustBits = (d.quot * 8) + ((d.rem > 0) ? 8 : 0) - lAdjustHeight;
			    LONG lAdjustFactor = 16;
			    LONG lAdjust = pfph->lSlant ? ((lAdjustBits * lAdjustFactor) + 32) : 0;
			    // burl 1.2015

			    lRowWait   = ((pfe->lRow/8)*8)  * pfph->lSlant;
			    lSlantWait = (pfi->lHeight * pfph->lSlant) + lAdjust;
			    lRasterWidth = pfi->lLength/((pfi->lHeight + 7) / 8) - lSlantWait;
			    // slant wait is subtracted above because pfi->lLength includes the slant rasters

/*
{
    char sz [512];
    LONG lTmp = lAdjust * lAdjustFactor;
    
    sprintf (sz, "TODO: rem // lSlant: %-3d, lHeight: %-3d [%-3d], lAdjustBits: %-3d, lAdjustFactor: %-3d [%-4d]",
	    pfph->lSlant,
	    pfi->lHeight, lAdjustHeight,
	    lAdjustBits,
	    lAdjustFactor,
	    lAdjust);    
    fj_socketSendAll(IPC_STATUS, (LPBYTE)sz, strlen (sz));
}
*/
				
				if ( FJPH_DIRECTION_RIGHT_TO_LEFT == pfph->lDirection )
				{
					// forward printing with the left raster first
					if ( (FJ_POSITION_PARALLEL == pfe->lPosition) && (NULL != pfiPrev) )
					{
						lRasterLeft = lRasterLeftPrev;
					}
					else if ( (FJ_POSITION_CONCATENATE == pfe->lPosition) && (NULL != pfiPrev) )
					{
						lRasterLeft = lRasterRightPrev;
					}
					else
					{
						// get unslanted start
						lRasterLeft = lDistPC + pfe->fLeft * fRes;
					}
					// get unslanted end
					lRasterRight = lRasterLeft + lRasterWidth;
					lRasterWait = lRasterLeft - lSlantWait - lRowWait;
				}
				else
				{
					// reverse printing with the right raster first
					if ( (FJ_POSITION_PARALLEL == pfe->lPosition) && (NULL != pfiPrev) )
					{
						lRasterLeft = lRasterLeftPrev;
					}
					else if ( (FJ_POSITION_CONCATENATE == pfe->lPosition) && (NULL != pfiPrev) )
					{
						lRasterLeft = lRasterRightPrev;
					}
					else
					{
						lRasterLeft = lDistPC + lBoxWidth - pfe->fLeft * fRes;
					}
					// get unslanted end
					lRasterRight = lRasterLeft - lRasterWidth;
					lRasterWait = lRasterRight + lRowWait;
				}
				if ( (NULL == pfiPrev) || (lRasterWait < lRasterMin) ) lRasterMin = lRasterWait;

/*
  {
    char sz [512];
    sprintf (sz, "TODO: rem // lRasterWidth: %d [lRasterLeft: %d, lRasterRight: %d, lRasterWait: %d, pfe->fLeft: %f]",
	    lRasterWidth, lRasterLeft, lRasterRight, lRasterWait, pfe->fLeft);
    fj_socketSendAll(IPC_STATUS, (LPBYTE)sz, strlen (sz));
}
*/
				
				pfiPrev = pfi;
				// save wait info if next is concatenate or parallel
				lRasterLeftPrev  = lRasterLeft;
				lRasterRightPrev = lRasterRight;

				// build a PRINTCHAIN structure
				pfi->lUseCounter++;
				pfc = (LPFJPRINTCHAIN)fj_calloc( 1, sizeof(FJPRINTCHAIN) );
				if (NULL == pfc)
				{
					int abc = 0;
					break;
				}
				pfc->pfi = pfi;
				pfc->lMessageCounter = lMessageCounter;
										//pfe->lRow;
				pfc->lRasterRow = (pfe->lRow/8)*8;
				
				pfc->lRasterWait = lRasterWait;
				pfc->lRasterCount = 0;
				pfc->pNext = NULL;
				// add it to our local pfc chain
				if ( 0 == iSeg )
				{
					pfcLast  = pfc;
					pfcFirst = pfc;
				}
				else
				{
					pfcLast->pNext = pfc;
					pfcLast = pfc;
				}
			}
			fj_print_head_UpdateRasterCounter();
			if ( 0 == iSeg ) lRC1 = pGlobal->lRasterCount;
			if ( 1 == iSeg ) lRC2 = pGlobal->lRasterCount;
			if ( 2 == iSeg ) lRC3 = pGlobal->lRasterCount;
		}

		// adjustment for how long it took us since the photocell
		fj_print_head_UpdateRasterCounter();
		lRC4 = pGlobal->lRasterCount;
		lRC5 = lRC0+lRC1+lRC2+lRC3+lRC4;
		lWaitAdjusted = pGlobal->lRasterCount;
		// lWaitAdjusted contains the number of rasters that were printed while the message was built
		// lWaitAdjusted will be subtracted from the calculated wait time/count of each image.

		// the ENI interrupt routine will use this to calculate the number of rasters to wait before using the image.

		// but the ENI interrupt routine will not be executed until the current DMA buffer runs out.
		// so we must also account for the number of rasters in the current DMA buffer.
		{
			dma_sr_t dmasr;
			WORD32 wblenrem;
			LONG   lDMAAdjust;
			UINT   iOld;

			wblenrem = 0;
										// disable to insure no DMA interrupt
			iOld = tx_interrupt_control( TX_INT_DISABLE );
			if ( 1 < (*(NARM_DMA)).dmacr4.bits.state )
			{
										// get transmit DMA remaining length
				wblenrem = (*(NARM_DMA)).dmasr4.bits.blen;
			}
			lDMAAdjust = wblenrem;		// number of bytes left in DMA buffer
			//lDMAAdjust += fj_lENIbytesrem;						 // plus number of bytes sent to ENI driver
										// return to previous enabled state
			tx_interrupt_control( iOld );
			lDMAAdjust += 32;			// plus number of bytes in FIFO
			// gives number of bytes until a raster will go to the print head
										// divided by the number of bytes per raster
			lDMAAdjust /= fj_lFIFONozzleBytes;
			lWaitAdjusted += lDMAAdjust;// gives the additional time to subtract from the wait counts
		}

		// if the amount of rasters to adjust the wait is greater than the rasters to the first image,
		// we have missed the target and the message will not print in the correct position.
		if ( lWaitAdjusted > lRasterMin )
		{
			lWaitAdjusted = lRasterMin;
			pGlobal->bBuildFailure = TRUE;
		}

		if ( NULL != pfcFirst)
		{
			// adjust each PRINTCHAIN
			pfc = pfcFirst;
										// adjust wait for all segments
			for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
			{
				pfc->lRasterWait -= lWaitAdjusted;
				pfc = pfc->pNext;
			}

			// add print chain to printhead queue
			fj_print_head_PrintChainAdd( pfcFirst, pfcLast );
			pBRAM->lSelectedCount++;
		}
	}

	return;
}
//
//  FUNCTION: fj_print_head_PrintStepPattern()
//
//  PURPOSE:  Build and Print a test pattern
//
VOID fj_print_head_PrintStepPattern( VOID )
{
	LPFJSYSTEM     pfsys = fj_CVT.pfsys;
	LPFJPRINTHEAD  pfph  = fj_CVT.pfph;
	LPFJIMAGE      pfi;
	LPFJPRINTCHAIN pfc;
	BYTE   *pBuff;
	LONG    lWidth;
	int     i,j,k;
	BYTE    bit;
	LONG    lBitIndex;
	BYTE    bitLast;
	LONG    lBitIndexLast;
	LONG    lBytes;
	LONG    lHeight;
	LONG    lLength;
	LONG    lSlant;

	if ( FJSYS_MAX_MESSAGES > (lMessageCounter - lMessageCounterCompleted) )
	{
		lMessageCounter++;

		lWidth = 40;
		lHeight = pfph->pfphy->lChannels;
		lBytes = (lHeight+7) / 8;
										// for each of 'height' nozzles, print a 'width'
		lLength = lBytes * lHeight * lWidth;

		pfi = fj_ImageNew( lLength );
		if ( NULL != pfi )
		{
			pfi->lHeight = lHeight;
			pBuff = (LPBYTE)pfi + sizeof(FJIMAGE);

			bit = 0x80;
			lBitIndex = 0;
			bitLast = 0x80;
			lBitIndexLast = (pfi->lHeight-1) / 8;
			for ( i = lBitIndexLast*8; i < pfi->lHeight-1; i++ ) bitLast = bitLast >> 1;

			for ( i = 0; i < pfi->lHeight; i++ )
			{
				for ( j = 0; j < lWidth; j++ )
				{
					for ( k = 0; k < lBytes; k++ )
					{
						*pBuff |= 0x80;
						if ( k == lBitIndex     ) *pBuff |= bit;
						if ( k == lBitIndexLast ) *pBuff |= bitLast;
						pBuff++;
					}
				}
				bit = bit >> 1;
				if ( 0 == bit )
				{
					bit = 0x80;
					lBitIndex++;
				}
			}

			pfi->lTransforms   =  0;	// transforms used to change image
			pfi = fj_ImageRasterTransform( pfi, pfph->lTransformEnv );
			lSlant = pfph->lSlant;
			if ( 0 < lSlant  )
			{
				if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) lSlant = -lSlant;
				pfi = fj_ImageSlant( pfi, lSlant );
			}

			pfc = (LPFJPRINTCHAIN)fj_calloc( 1, sizeof(FJPRINTCHAIN) );
			pfc->pfi = pfi;
			pfc->lMessageCounter = lMessageCounter;
			pfc->lRasterRow = 0;
			pfc->lRasterWait = 0;
			pfc->lRasterCount = 0;
			pfc->pNext = NULL;

			fj_print_head_PrintChainAdd( pfc, pfc );
		}
	}

	return;
}
//
//  FUNCTION: fj_print_head_PrintTestPattern()
//
//  PURPOSE:  Build and Print a RasterImage
//
VOID fj_print_head_PrintTestPattern( LPSTR pPattern, LONG lCount )
{
	LPFJPRINTHEAD  pfph  = fj_CVT.pfph;
	LPFJIMAGE      pfi;
	LPFJPRINTCHAIN pfc;

	BYTE   *pBuff;
	BYTE   *pBuff1;
	LONG    lWidth;
	int     i;
	BYTE    bit;
	BYTE    bit2;
	CHAR    pat;
	LONG    lBytes;
	LONG    lHeight;
	LONG    lLength;
	LONG    lSlant;

	if ( FJSYS_MAX_MESSAGES > (lMessageCounter - lMessageCounterCompleted) )
	{
		lMessageCounter++;

		lWidth  = lCount;
		lHeight = pfph->pfphy->lChannels;
		lBytes  = (lHeight+7) / 8;
		lLength = lBytes * lWidth;

		pfi = fj_ImageNew( lLength );
		if ( NULL != pfi )
		{
			pfi->lHeight = lHeight;
			pBuff = (LPBYTE)pfi + sizeof(FJIMAGE);
			pBuff1 = pBuff;

			for ( i = 0; i < lBytes; i++ )
			{
				bit  = 0;
				bit2 = 0;
				pat = *pPattern;
				if      ( (pat >= '0') && (pat <= '9') ) bit = pat - '0';
				else if ( (pat >= 'a') && (pat <= 'f') ) bit = pat - 'a' + 10;
				else if ( (pat >= 'A') && (pat <= 'F') ) bit = pat - 'A' + 10;
				if ( 0 != pat ) pPattern++;
				pat = *pPattern;
				if      ( (pat >= '0') && (pat <= '9') ) bit2 = pat - '0';
				else if ( (pat >= 'a') && (pat <= 'f') ) bit2 = pat - 'a' + 10;
				else if ( (pat >= 'A') && (pat <= 'F') ) bit2 = pat - 'A' + 10;
				if ( 0 != pat ) pPattern++;
				bit = (bit<<4) | bit2;
				*pBuff1 = bit;
				pBuff1++;
			}
			for ( i = 1; i < lCount; i++ )
			{
				memcpy( pBuff1, pBuff, lBytes );
				pBuff1 += lBytes;
			}

			// use the low digit of the count for the width - temp testing
			i = lCount % 10;
			if ( i > 0 )
			{
										// width is 1 origin
				pfi = fj_ImageWidth( pfi, i+1 );
			}
			pfi = fj_ImageRasterTransform( pfi, pfph->lTransformEnv );
			lSlant = pfph->lSlant;
			if ( 0 < lSlant  )
			{
				if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) lSlant = -lSlant;
				pfi = fj_ImageSlant( pfi, lSlant );
			}

			pfc = (LPFJPRINTCHAIN)fj_calloc( 1, sizeof(FJPRINTCHAIN) );
			pfc->pfi = pfi;
			pfc->lMessageCounter = lMessageCounter;
			pfc->lRasterRow = 0;
			pfc->lRasterWait = 0;
			pfc->lRasterCount = 0;
			pfc->pNext = NULL;

			fj_print_head_PrintChainAdd( pfc, pfc );
		}
	}

	return;
}
VOID fj_print_head_ENItransmit( LONG lChannelID, eniMessageType *pENI )
{
	volatile LPGA  pga  = fj_CVT.pGA;
	LPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD  pfph = fj_CVT.pfph;
	LPFJSYSTEM    pfsys = fj_CVT.pfsys;
	LPFJENI pfjeni;
	LPFJENI pfjeni2;
	register LPBYTE pBuffRaster;
	LPBYTE pBuffRasterOffset;
	register LPBYTE pBuff;
	register LPBYTE pBuffEnd;
	register LPBYTE pBuffRasterEnd;
	LPBYTE pBuffRasterStart;
	register int    i;
	register LONG   lBytes;
	register LONG   lShifted;
	register LONG   lRowBytes;
	register LONG   lRowBits;
	int    iStart;
	int    iStartR;
	LONG   lTopBytes;
	LONG   lRasterBytes;
	LONG   lRasterMaxAll;
	LONG   lRasterMaxUse;
	LONG   lRasterUse;
	LONG   lRasterMaxOne;
	LONG   lRasterZeros;
	LONG   lRasterLimit;
	LONG   lImgByte;
	register LPFJIMAGE       pfi;
	register LPFJPRINTCHAIN  pfc;
	int  iRet;
	UINT iOld;

	BOOL  bFirst;
	LONG  lMessageCounterFound;
	ULONG lClock1;
	ULONG lClock2;
	ULONG lDelta;
	ULONG lWrap;
	
	lClock1 = pga->rw.read.HStimer;
	lMessageCounterFound = 0;

	//	if ( lChannelID == lENIchannel )
	{
		bFirst = TRUE;
		// set data light by looking at the other structure - the one being used by the ENI FIFO now
		if ( pENI == fjeni1.peni ) {pfjeni2 = &fjeni2; pfjeni = &fjeni1;}
		else                       {pfjeni2 = &fjeni1; pfjeni = &fjeni2;}
		if ( TRUE == pfjeni2->bZero )	// any data in active ENI buffer?
		{
										// no. if light on,
			if ( TRUE == pGlobal->bDataReady )
			{
										//     turn it off
				pGlobal->bDataReady = FALSE;
				if ( FALSE == fj_rasterLoop )
				{
										// do not call TCP routines from this interrupt routine
					pGlobal->bSendDataReady = TRUE;
					// set flags to get the high priority thread to send the messages
					tx_thread_resume( &(fj_CVT.pFJglobal->thrPrint) );
				}
			}
		}
		else
		{
										// yes. if light off,
			if ( FALSE == pGlobal->bDataReady )
			{
										//     turn it on
				pGlobal->bDataReady = TRUE;
				if ( FALSE == fj_rasterLoop )
				{
										// do not call TCP routines from this interrupt routine
					pGlobal->bSendDataReady = TRUE;
					// set flags to get the high priority thread to send the messages
					tx_thread_resume( &(fj_CVT.pFJglobal->thrPrint) );
				}
			}
		}

		if ( ENI_SUCCESS == pENI->status )
		{
			if ( FALSE == pfjeni->bZero )
			{
				pfjeni->bZero = TRUE;
				memset( pENI->src_addr, 0 , pENI->length );
			}

			pfc = NULL;
			// this test is here in case the ENI interrupt goes off before the pfph is set up.
			// did this ever happen? maybe once after a reset of some sort.
			if ( NULL != pfph ) pfc = pfph->pfcFirst;

			lRasterMaxAll = 0;
			lRasterZeros = lENIzerorasters;
			lRasterLimit = lENIrasters;
			if ( NULL != pfc )
			{
				// all pfc structures for one message are added to the chain at one time.
				// the pfc structres are not necessarily in order of their delay/rasterwait.
				// if there are no pfc structures, start a small buffer of zeros.
				// if there are pfc structures, and all have a rasterwait value,
				//      start a buffer of zeros up to the shortest wait.
				//      the next ENI buffer will start with their data.
				//
				// an ENI buffer will contain only data for one message
				// this will give more time for dynamic data to be received before going into the ENI buffers
				//
				// more about "dynamic data" later.
				// Dynamic data will lower the lRasterLimit value
				lRasterMaxUse = 0;
				lRasterUse = 0;
				while( NULL != pfc )	// check all chained images
				{
					if ( pfc->lRasterWait < lRasterZeros )
					{
						lRasterZeros = pfc->lRasterWait;
					}
					if ( pfc->lRasterWait < lRasterLimit && lRasterMaxUse != lRasterLimit )
					{
						pfi = pfc->pfi;
						lBytes  = (pfi->lHeight + 7) / 8;
						lRasterUse = (pfi->lLength + (fj_lFIFORasterOffset*lBytes))
							+ pfc->lRasterWait*lBytes - pfc->lRasterCount*lBytes;
						lRasterUse /= lBytes;
						if ( lRasterUse >= lRasterLimit ) lRasterMaxUse = lRasterLimit;
						if ( lRasterUse < lRasterLimit && lRasterUse > lRasterMaxUse ) lRasterMaxUse = lRasterUse;
					}
					pfc = pfc->pNext;
				}
				if ( NULL != pfph ) pfc = pfph->pfcFirst;
				if ( 0 != lRasterZeros )
				{
					while( NULL != pfc )// check all chained images
					{
										// wait for time/distance to elapse
						pfc->lRasterWait -= lRasterZeros;
						pfc = pfc->pNext;
					}
					// when we finish this loop,
					// pfc will be NULL and the next loop to process rasters will be bypassed.
				}
				else lRasterZeros = lENIzerorasters;

				if ( 0 == lRasterMaxUse ) lRasterMaxUse = lRasterZeros;
				while( NULL != pfc )	// check all chained images
				{
					pfi = pfc->pfi;
										// | 0x08000000;
					pBuff = (LONG)pENI->src_addr;
					pBuffEnd = pBuff + lRasterLimit * fj_lFIFONozzleBytes;
					lRasterMaxOne = 0;

					if ( 0 != pfc->lRasterWait )
					{
						if ( pfc->lRasterWait < lRasterLimit )
						{
							pBuff += pfc->lRasterWait * fj_lFIFONozzleBytes;
							lRasterMaxOne = pfc->lRasterWait;
							pfc->lRasterWait = 0;
						}
					}

					// process these rasters
					lBytes  = (pfi->lHeight + 7) / 8;
										// if no longer waiting,
					if ( (0 == pfc->lRasterWait) &&
						// and if not completely processed,
						((pfc->lRasterCount*lBytes) < (pfi->lLength + (fj_lFIFORasterOffset*lBytes))) &&
						// and if first or same message as first pfc,
						((0==lMessageCounterFound )||(lMessageCounterFound == pfc->lMessageCounter)) )
					{					// then process these rasters
										// if first image
						if ( 0 == lMessageCounterFound )
						{
							// save first message counter
							lMessageCounterFound = pfc->lMessageCounter;
							// and if higher than previous
							if ( lMessageCounterFound > lMessageCounterPrinting )
							{
								// save new message number
								lMessageCounterPrinting = lMessageCounterFound;
							}
						}
						pfjeni->bZero = FALSE;
						lRowBytes = pfc->lRasterRow/8;
						lRowBits  = pfc->lRasterRow - lRowBytes*8;
						pBuffRasterStart = (LPBYTE)pfi + sizeof(FJIMAGE);
						pBuffRaster      = pBuffRasterStart + pfc->lRasterCount*lBytes + lBytes - 1;
						pBuffRasterEnd   = pBuffRasterStart + pfi->lLength;
						lTopBytes  = (pfi->lHeight + pfc->lRasterRow + 7) / 8;
						pBuff += fj_lFIFONozzleBytes - lTopBytes;
						iStart = iStartR = 0;
						if ( lTopBytes > fj_lFIFONozzleBytes )
						{
							iStart = iStartR = lTopBytes - fj_lFIFONozzleBytes;
						}

										// offset print head
						if ( 0 != fj_lFIFORasterOffset )
						{
							LONG    lOffsetBytes;
							lOffsetBytes = fj_lFIFORasterOffset * lBytes;
							if ( FJPH_DIRECTION_RIGHT_TO_LEFT == pfph->lDirection)
							{
								pBuffRasterOffset = pBuffRaster - lOffsetBytes;
								if ( 0 == lRowBits )
								{
									while ( (pBuff < pBuffEnd) && (pBuffRasterOffset < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											if ( pBuffRaster < pBuffRasterEnd )
											{
												// OR bits
												*(pBuff+i) |= *(pBuffRaster-i) & 0xAA;
											}
											if ( pBuffRasterOffset >= pBuffRasterStart )
											{
												// OR bits
												*(pBuff+i) |= *(pBuffRasterOffset-i) & 0x55;
											}
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pBuffRasterOffset += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
								else
								{
									while ( (pBuff < pBuffEnd) && (pBuffRasterOffset < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											if ( pBuffRaster < pBuffRasterEnd )
											{
												lShifted = *(pBuffRaster-i)<<(8-lRowBits);
												// OR bits
												if( i >= iStartR ) *(pBuff+i  ) |= (lShifted   ) & 0xAA;
												*(pBuff+i+1) |= (lShifted>>8) & 0xAA;
											}
											if ( pBuffRasterOffset >= pBuffRasterStart )
											{
												lShifted = *(pBuffRasterOffset-i)<<(8-lRowBits);
												// OR bits
												if( i >= iStartR ) *(pBuff+i  ) |= (lShifted   ) & 0x55;
												*(pBuff+i+1) |= (lShifted>>8) & 0x55;
											}
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pBuffRasterOffset += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
							}

							else		// left to right
							{
								pBuffRasterOffset  = pBuffRaster;
								pBuffRaster       -= lOffsetBytes;
								if ( 0 == lRowBits )
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											if ( pBuffRaster >= pBuffRasterStart )
											{
												// OR bits
												*(pBuff+i) |= *(pBuffRaster-i) & 0xAA;
											}
											if ( pBuffRasterOffset < pBuffRasterEnd )
											{
												// OR bits
												*(pBuff+i) |= *(pBuffRasterOffset-i) & 0x55;
											}
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pBuffRasterOffset += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
								else
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											if ( pBuffRaster >= pBuffRasterStart )
											{
												lShifted = *(pBuffRaster-i)<<(8-lRowBits);
												// OR bits
												if( i >= iStartR ) *(pBuff+i  ) |= (lShifted   ) & 0xAA;
												*(pBuff+i+1) |= (lShifted>>8) & 0xAA;
											}
											if ( pBuffRasterOffset < pBuffRasterEnd )
											{
												lShifted = *(pBuffRasterOffset-i)<<(8-lRowBits);
												// OR bits
												if( i >= iStartR ) *(pBuff+i  ) |= (lShifted   ) & 0x55;
												*(pBuff+i+1) |= (lShifted>>8) & 0x55;
											}
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pBuffRasterOffset += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
							}

						}
						else			// no offset
						{
							// this routine is called continuously while printing.
							// it should be fast.
							// to make it faster, there are 4 versions.
							// if it is the first pfc, the bits can be stored instead of being OR'ed.
							// if the image starts on a nozzle that is a multiple of 8, the bits do not have to be shifted.
							if ( TRUE == bFirst )
							{
								bFirst = FALSE;
								if ( 0 == lRowBits )
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											// store bits
											*(pBuff+i) = *(pBuffRaster-i);
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
								else
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											lShifted = *(pBuffRaster-i)<<(8-lRowBits);
											// store bits
											if( i >= iStartR ) *(pBuff+i  ) |= lShifted;
											*(pBuff+i+1) |= lShifted>>8;
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
							}
							else
							{
								if ( 0 == lRowBits )
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											// OR bits
											*(pBuff+i) |= *(pBuffRaster-i);
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
								else
								{
									while ( (pBuff < pBuffEnd) && (pBuffRaster < pBuffRasterEnd) )
									{
										for ( i = iStart; i < lBytes; i++ )
										{
											// the Trident printheads have a serial shift register.
											// the GateArray feeds the register one bit at a time.
											// the first bit becomes the highest channel number.
											// we must feed the highest byte first.
											// luckily, we have the highest nozzle at bit0 in the byte,
											//     this will be the first bit into the shift register.
											//
											lShifted = *(pBuffRaster-i)<<(8-lRowBits);
											// OR bits
											if( i >= iStartR ) *(pBuff+i  ) |= lShifted;
											*(pBuff+i+1) |= lShifted>>8;
										}
										pBuff             += fj_lFIFONozzleBytes;
										pBuffRaster       += lBytes;
										pfc->lRasterCount++;
										lRasterMaxOne++;
									}
								}
							}
						}				// no offset
					}
					// check to see if we should still wait for these rasters
					if ( 0 != pfc->lRasterWait )
					{
						if ( pfc->lRasterWait >= lRasterLimit ) pfc->lRasterWait -= lRasterMaxUse;
					}
					if ( lRasterMaxOne > lRasterMaxAll ) lRasterMaxAll = lRasterMaxOne;
					pfc = pfc->pNext;
				}
			}

			// if nothing to do, send a few zeros
			if ( 0 == lRasterMaxAll )
			{
				lRasterMaxAll = lRasterZeros;
			}
			pfjeni->lRasters = lRasterMaxAll;
			pENI->length = lRasterMaxAll * fj_lFIFONozzleBytes;
			pBuff = (LONG)pENI->src_addr;
			iRet = eniWriteFIFO( lENIchannel, pENI );
		}
	}
	if ( NULL != pfph->pfcFirst )
	{
		lClock2 = pga->rw.read.HStimer;
		// check to see if the 2 byte clock has wrapped
		lWrap = 0;
		if ( lClock2 < lClock1 ) lWrap = 0x10000;
		// get elapsed time
		lDelta = lWrap + lClock2 - lClock1;
		lDelta /= fj_lHSClocksPerMillisecond;
		if ( lDelta > 99 ) lDelta = 99;
		if ( lDelta > 20 )
		{
			lWrap = 0;
			if ( lClock2 < lClock1 ) lWrap = 0x10000;
			// get elapsed time
			lDelta = lWrap + lClock2 - lClock1;
			lDelta /= fj_lHSClocksPerMillisecond;
			if ( lDelta > 99 ) lDelta = 99;
		}
		alENICounters[lDelta]++;
	}

	return;
}
//
// this should never occur
// no data is sent from GA to us
//
VOID fj_print_head_ENIreceive( LONG lChannelID, eniMessageType *pENI )
{
	return;
}
//
// this interrupt is generated when the GA detects a photocell
// it is more responsive than if we poll for the photocell transition
//
static BOOL TestENI_Enter = FALSE;
VOID fj_print_head_ENIinterrupt( LONG lChannelID )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPCFJSYSTEM  pfsys   = fj_CVT.pfsys;
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
	FLOAT fRes;
	GA_READ_STATUS wStatus;
	int   i;


	if (TRUE == TestENI_Enter)
	{
		i = 0;
		return;
	}

	TestENI_Enter = TRUE;

	wStatus = pGlobal->gaRead.rw.read.status;
	// ignore until ENI fully set up or printhead not atTemp
										//&& (TRUE == pGlobal->bReady) && (FALSE == pCVT->pBRAM->bPrintIdle)) //( 0 != wStatus.bits.AtTemp ) && (fj_PrintHeadGetInkLowVolume (pfph) > pBRAM->dInkLowML) )
	if ( (TRUE == pGlobal->bPhotocellInt ) )
	{
										// will also update lPhotocellCount
		fj_print_head_UpdateRasterCounter();
		pGlobal->bPhotocell = TRUE;		// set flag to reflect photocell state
		//pGlobal->bSendPhotocell = TRUE;							 // set flag for photocell status message
		if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
		{
										// has the previous box passed?
			if ( 0 == pGlobal->lPhotocellCount )
			{							// YES!!
				// set the count to ignore new photocell detects until the box passes
				if ( TRUE == pfsys->bShaftEncoder ) fRes = pfsys->fEncoder;
				else                                fRes = pfsys->fPrintResolution;
				if ( NULL != pfsys->pfl ) pGlobal->lPhotocellCount = (fRes * pfsys->pfl->fBoxWidth);
										// set flag for photocell status message

				pGlobal->bPhotocellInt = FALSE; // burl
				fj_print_head_SetGAWriteSwitches (); // burl
				
				pGlobal->bSendPhotocell = TRUE;
				// start the message generation and printing
				fj_print_head_PhotoCell();
				// the above routine sets the Photocell Time
										// set flag for thread to send photocell detect message with Photocell TIme
				pGlobal->bSendPhotocellDetect = TRUE;
				tx_thread_resume( &(pGlobal->thrPrint) );
			}
		}
	}
	TestENI_Enter = FALSE;
	return;
}
//
// initialize the ENI FIFO interface
//
VOID fj_print_head_InitENI(VOID)
{
	LPFJSYSTEM  pfsys = fj_CVT.pfsys;
	volatile    WORD32 gcr1;
	volatile    WORD32 gcr2;
	volatile    WORD32 ecr1;
	volatile    WORD32 ecr2;
	volatile    WORD32 esr1;
	volatile    WORD32 esr2;
	volatile    WORD32 epir;
	int   iRet;
	FLOAT fRes;

	// set the number of printer channels.
	fj_print_head_SetGAChannels();

	fj_CVT.pFJglobal->bHighVoltageOn = TRUE;
	fj_print_head_SetGAWriteSwitches();	// turn on high voltage

	// calculate nozzle offset
	if ( TRUE == pfsys->bShaftEncoder ) fRes = pfsys->fEncoder;
	else                                fRes = pfsys->fPrintResolution;

	fj_lFIFORasterOffset = (LONG)(fj_CVT.pfph->pfphy->fOffsetEvenOdd * fRes);

	if ( NULL == pENIbuff1 ) pENIbuff1 = fj_memory_getOnce( ENI_BUFFER_SIZE );
	if ( NULL == pENIbuff2 ) pENIbuff2 = fj_memory_getOnce( ENI_BUFFER_SIZE );
	memset( pENIbuff1,  0, ENI_BUFFER_SIZE );
	memset( pENIbuff2,  0, ENI_BUFFER_SIZE );
	eniM1.next = eniM2.next = NULL;
	eniM1.dst_addr = eniM2.dst_addr = NULL;
	eniM1.src_addr = (LONG)pENIbuff1;	//& 0x00FFFFFF;
	eniM2.src_addr = (LONG)pENIbuff2;	//& 0x00FFFFFF;
	fjeni1.peni   = &eniM1;
	fjeni1.bZero  = TRUE;
	fjeni1.bInUse = FALSE;
	fjeni2.peni   = &eniM2;
	fjeni2.bZero  = TRUE;
	fjeni2.bInUse = FALSE;

	// the minus 1 allows an extra raster in the buffer to take care of elements shifted past the last nozzle
	lENIrasters =  ENI_BUFFER_SIZE / fj_lFIFONozzleBytes;
	lENIbytes   =  lENIrasters     * fj_lFIFONozzleBytes;
	// this is used for the number of blank rasters when idle
	lENIzerorasters = ENI_BLANK_RASTERS;
	lENIzerobytes   = lENIzerorasters  * fj_lFIFONozzleBytes;

	iRet = eniLoadDriver();
	if ( ENI_SUCCESS == iRet )
	{
		lENIchannel = 43;
										// returned channel ID
		iRet = eniOpenChannel( &lENIchannel,
			ENI_FIFO_MODE_8_BIT,		// ENI mode
			NULL,						// not using shared RAM
			0,							// not using shared RAM
			1,							// receive queue size
			10,							// transmit queue size
			ENI_32_BIT,					// data size
			ENI_NO_BURST,				// transfer size
			fj_print_head_ENIinterrupt,	// interrupt call back
			fj_print_head_ENItransmit,	// transmit call back
			fj_print_head_ENIreceive	// receive call back
			);
	}
	epir = (*(NARM_PC)).DPO_pir;		// read ENI Pulsed Interrupt Register - contains ENI FIFO Mode/Mask Register
	if ( ENI_SUCCESS == iRet )
	{
		gcr1 = (*(NARM_PC)).gcr.reg;	// read general control register - before changing
		ecr1 = (*(NARM_PC)).DPO_cr.reg;	// read ENI control register - before changing
										// allow ENIMODE changes
		(*(NARM_PC)).gcr.bits.pcdiag  = 1;
										// disable pulsed interrupt mode
		(*(NARM_PC)).DPO_cr.bits.pulint_n = 1;
										// disable FIFO DMA on external side
		(*(NARM_PC)).DPO_cr.bits.dmae_n   = 1;
										// set intp* high - low gives an interrupt from pint2*
		(*(NARM_PC)).DPO_cr.bits.intp_n   = 1;
										// enable pint2* as an interrupt
		(*(NARM_PC)).DPO_cr.bits.dint2_n  = 0;
										// disallow ENIMODE changes
		(*(NARM_PC)).gcr.bits.pcdiag  = 0;
		ecr2 = (*(NARM_PC)).DPO_cr.reg;	// read ENI control register - after changing
		gcr2 = (*(NARM_PC)).gcr.reg;	// read general control register - after changing
	}
	epir = (*(NARM_PC)).DPO_pir;		// read ENI Pulsed Interrupt Register - contains ENI FIFO Mode/Mask Register
	if ( ENI_SUCCESS == iRet )
	{
		// the FIFO should be empty.
		// initialize the Gate Array.
										// write anything to initialize the ENI in the GA
		fj_CVT.pGA->rw.write.initializeENI = 1;
		// save
		fj_CVT.pFJglobal->gaWrite.rw.write.initializeENI = 1;
		tx_thread_sleep(2);				// short delay
		// just in case the FIFO was not empty after a soft/hard reset,
		// initialize the Gate Array again.
										// write anything to initialize the ENI in the GA
		fj_CVT.pGA->rw.write.initializeENI = 1;
		// save
		fj_CVT.pFJglobal->gaWrite.rw.write.initializeENI = 1;

		// let's do a little write of zeros to get things started.
		// the interrupt routines will keep things going after this is done.
		eniM1.length = eniM2.length = lENIzerobytes;
		fjeni1.lRasters = fjeni2.lRasters = lENIzerorasters;
		iRet = eniWriteFIFO( lENIchannel, &eniM1 );
		iRet = eniWriteFIFO( lENIchannel, &eniM2 );
		epir = (*(NARM_PC)).DPO_pir;	// read ENI Pulsed Interrupt Register - contains ENI FIFO Mode/Mask Register
		esr1 = (*(NARM_PC)).DPO_sr.reg;	// read ENI shared register - before changing
		esr2 = (*(NARM_PC)).DPO_sr.reg;	// read ENI shared register - after changing

		fj_CVT.pFJglobal->bPhotocellInt = TRUE;
		fj_print_head_SetGAWriteSwitches();
	}
	return;
}
//
// stop the ENI FIFO interface
//
VOID fj_print_head_KillENI(VOID)
{
	int iRet;

	if ( 43 != lENIchannel )
	{
		fj_CVT.pFJglobal->bPhotocellInt = FALSE;
		fj_print_head_SetGAWriteSwitches();

		iRet = eniDisableFIFO( lENIchannel, ENI_TRANSMIT_CHANNEL );
		iRet = eniDisableFIFO( lENIchannel, ENI_RECEIVE_CHANNEL );
		iRet = eniFlushFIFO( lENIchannel, ENI_TRANSMIT_CHANNEL );
		iRet = eniFlushFIFO( lENIchannel, ENI_RECEIVE_CHANNEL );
		iRet = eniCloseChannel( lENIchannel );

		lENIchannel = 43;

		// initialize the Gate Array again.
		// this will synch the GA and it will be waiting for the first byte of a raster again.
										// write anything to initialize the ENI in the GA
		fj_CVT.pGA->rw.write.initializeENI = 1;
		// save
		fj_CVT.pFJglobal->gaWrite.rw.write.initializeENI = 1;
	}

	return;
}
