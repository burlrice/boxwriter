/* C file: c:\MyPrograms\FoxJet1\_message_edit.c created
 by NET+Works HTML to C Utility on Thursday, February 15, 2001 10:25:12 */
#include "fj.h"
#include "barcode.h"
#include "fj_misc.h"
#include "fj_defines.h"
#include "fj_font.h"
#include "fj_barcode.h"
#include "fj_bitmap.h"
#include "fj_counter.h"
#include "fj_datetime.h"
#include "fj_dynimage.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_datamatrix.h"
#include "fj_text.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_memstorage.h"

#define FJ_COLUMNS_FRONT      3
#define FJ_COLUMNS_BACK       3
#define FJ_COLUMNS_BARCODE   (FJ_COLUMNS_FRONT + 13 + FJ_COLUMNS_BACK)
#define FJ_COLUMNS_BITMAP    (FJ_COLUMNS_FRONT +  4 + FJ_COLUMNS_BACK)
#define FJ_COLUMNS_COUNTER   (FJ_COLUMNS_FRONT + 12 + FJ_COLUMNS_BACK)
#define FJ_COLUMNS_DATETIME  (FJ_COLUMNS_FRONT + 12 + FJ_COLUMNS_BACK)
#define FJ_COLUMNS_DYNIMAGE  (FJ_COLUMNS_FRONT +  4 + FJ_COLUMNS_BACK)
#define FJ_COLUMNS_TEXT      (FJ_COLUMNS_FRONT +  5 + FJ_COLUMNS_BACK)

static CHAR sMsgEditName[FJMESSAGE_NAME_SIZE+1];
static CHAR sMsgEditNameQuote[FJMESSAGE_NAME_SIZE*2];
static LONG lMsgEditSegs;

struct fj_dttable
{
	LONG lID;
	CHAR sName[24];
};
static  struct fj_dttable fj_dt_edit_table[] =
{
	DT_DATE_YYYYMMDD  ,"YYYYMMDD",
	DT_DATE_DDMMYYYY  ,"DDMMYYYY",
	DT_DATE_MMDDYYYY  ,"MMDDYYYY",
	DT_DATE_YYYY      ,"YYYY",
	DT_DATE_MM        ,"MM",
	DT_DATE_DD        ,"DD",
	DT_DATE_YYMMDD    ,"YYMMDD",
	DT_DATE_DDMMYY    ,"DDMMYY",
	DT_DATE_MMDDYY    ,"MMDDYY",
	DT_DATE_YY        ,"YY",
	DT_DATE_DAYWK     ,"Day of week",
	DT_DATE_WK        ,"Week number",
	DT_DATE_JULIAN    ,"Julian day",
	DT_DATE_MONTHA    ,"Alphabetic month",
	DT_DATE_DAYWKA    ,"Alphabetic day of week",
	DT_TIME_HHMMSS24  ,"HHMMSS24",
	DT_TIME_HHMMSS12  ,"HHMMSS12",
	DT_TIME_HHMMSS12M ,"HHMMSS12 with am/pm",
	DT_TIME_HHMM24    ,"HHMM24",
	DT_TIME_HHMM12    ,"HHMM12",
	DT_TIME_HHMM12M   ,"HHMM12 with am/pm",
	DT_TIME_HH        ,"HH",
	DT_TIME_MM        ,"MM",
	DT_TIME_SS        ,"SS",
	DT_TIME_AMPM      ,"am/pm",
	DT_TIME_HOURA     ,"Alphabetic Hour",
	DT_TIME_SHIFT     ,"Shift Code"
};
static  struct fj_dttable fj_dt_time_table[] =
{

	DT_TIME_NORMAL      ,"Normal date time",
	DT_TIME_EXPIRATION  ,"Expiration date",
	DT_TIME_ROLLOVER    ,"Rollover time",
	DT_TIME_ROLL_EXP    ,"Expiration + Rollover"
};

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
void na_Double(unsigned long handle, double dInput, int iPlaces);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_MSG_EDIT_PARAM */
static void na_MSG_EDIT_PARAM (unsigned long handle);
/*  @! File: na_MSG_EDIT_TABLE */
static void na_MSG_EDIT_TABLE (unsigned long handle);
/*  @! File: na_MSG_IDS */
static void na_MSG_IDS (unsigned long handle);
/*  @! File: na_ME_title */
static void na_ME_title (unsigned long handle);
/*  @! File: na_AddLoc */
static void na_AddLoc (unsigned long handle);
/*  @! File: na_CopyFromLoc */
static void na_CopyFromLoc (unsigned long handle);
/*  @! File: na_CopyToLoc */
static void na_CopyToLoc (unsigned long handle);
/*  @! File: na_button_submit */
static void na_button_submit (unsigned long handle);
/*  @! File: na_button_addcopy */
static void na_button_addcopy (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\message_edit.htm {{ */
void Send_function_18(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<link rel=stylesheet href=fjipstyle1.css>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>Message Edit</title>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "");
	na_MSG_EDIT_PARAM (handle);
	HSSend (handle, "\n");
	HSSend (handle, "");
	na_MSG_IDS (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "  var of=document.form_me;\n");
	HSSend (handle, "  var ofid = of.msg_id;\n");
	HSSend (handle, "  var idok = IDCheck(ofid);\n");
	HSSend (handle, "  if ( true == idok ){of.action.value=sa; of.item.value=si;of.submit();}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function myreset(formObj) {\n");
	HSSend (handle, "  formObj.reset();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<p align=left class=fjh2>");
	na_ID (handle);
	HSSend (handle, " : ");
	na_ME_title (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, " <form name=form_me action=msgedit_1 method=POST>\n");
	HSSend (handle, "  <input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, ">\n");
	HSSend (handle, "  <input type=hidden value=none name=action>\n");
	HSSend (handle, "  <input type=hidden value=none name=item >\n");
	HSSend (handle, "  <table border=0 cellpadding=1>");
	na_MSG_EDIT_TABLE (handle);
	HSSend (handle, "</table>\n");
	HSSend (handle, "  <p align=center>");
	na_button_submit (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "  <p align=left>");
	na_button_addcopy (handle);
	HSSend (handle, "</p>    \n");
	HSSend (handle, " </form>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\message_edit.htm }} */
/* @! End HTML page   */

void reload_message_edit(unsigned long handle, LPSTR pID)
{
	char str[99];
	sprintf( str, "message_edit.htm?msg_edit=%s", pID );
	fj_reload_page(handle, str);
}
void na_processBarcodeElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJBARCODE  pfb;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    i;
	int    ret;

	pfb = (LPFJBARCODE)pfe->pDesc;
	if ( NULL != pfb )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "text" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBARCODE_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfb->strText, formField );
		}

		strcpy( FieldName, "style" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lSysBCIndex = lValue;
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lWidthValue = lValue;
		}

		strcpy( FieldName, "textalign" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( '0' == formField[0] )
			{
				pfb->bHROutput = FALSE;
			}
			else
			{
				pfb->bHROutput = TRUE;
				pfb->cHRAlign = formField[0];
			}
		}

		strcpy( FieldName, "textbold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lHRBold = lValue;
		}
	}
}
void na_processBitmapElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJBITMAP pfbm;
	char   formField[FJBITMAP_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfbm = (LPFJBITMAP)pfe->pDesc;
	if ( NULL != pfbm )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "image" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBITMAP_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfbm->strName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfbm->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfbm->lWidthValue = lValue;
		}
	}
}
void na_processCounterElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJCOUNTER pfc;
	char   formField[FJFONT_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfc = (LPFJCOUNTER)pfe->pDesc;
	if ( NULL != pfc )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJFONT_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfc->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lGapValue = lValue;
		}

		strcpy( FieldName, "change" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lChange = lValue;
		}

		strcpy( FieldName, "repeat" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lRepeat = lValue;
		}

		strcpy( FieldName, "start" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lStart = lValue;
		}

		strcpy( FieldName, "limit" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lLimit = lValue;
		}

		strcpy( FieldName, "length" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lLength = lValue;
		}

		strcpy( FieldName, "align" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 'L' == formField[0] ) pfc->cAlign = 'L';
			if ( 'R' == formField[0] ) pfc->cAlign = 'R';
		}

		strcpy( FieldName, "fill" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfc->cFill = formField[0];
		}
	}
}
void na_processDatetimeElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDATETIME pfdt;
	char   formField[FJFONT_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfdt = (LPFJDATETIME)pfe->pDesc;
	if ( NULL != pfdt )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJFONT_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfdt->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lGapValue = lValue;
		}

		strcpy( FieldName, "format" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lFormat = lValue;
		}

		strcpy( FieldName, "time" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lTimeType = lValue;
		}

		strcpy( FieldName, "delim" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfdt->cDelim = formField[0];
		}

		strcpy( FieldName, "strings" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( '1' == formField[0] ) pfdt->bStringNormal = TRUE;
			if ( '0' == formField[0] ) pfdt->bStringNormal = FALSE;
		}

		strcpy( FieldName, "shifts" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			if ( (lValue > 0) && (lValue <= 24) )pfdt->lHourACount = lValue;
		}

		strcpy( FieldName, "length" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lLength = lValue;
		}

		strcpy( FieldName, "align" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 'L' == formField[0] ) pfdt->cAlign = 'L';
			if ( 'R' == formField[0] ) pfdt->cAlign = 'R';
		}

		strcpy( FieldName, "fill" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfdt->cFill = formField[0];
		}
	}
}
void na_processDynimageElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDYNIMAGE pfd;
	char   formField[8];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfd = (LPFJDYNIMAGE)pfe->pDesc;
	if ( NULL != pfd )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfd->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfd->lWidthValue = lValue;
		}
	}
}
void na_processTextElement( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJTEXT      pft;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pft = (LPFJTEXT)pfe->pDesc;
	if ( NULL != pft )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "text" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJTEXT_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pft->strText, formField );
		}

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, min(sizeof(formField),FJFONT_NAME_SIZE+1) );
		if ( 0 < ret )
		{
			strcpy( pft->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lGapValue = lValue;
		}
	}
}
/* @! Begin of post function */
/* @! Function: Post_msgedit_1  {{ */
void Post_msgedit_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMESSAGE   pfmROM;
	LPFJMESSAGE   pfm;
	LPFJELEMENT  *papfe;
	LPFJELEMENT   pfeNew;
	LPFJELEMENT   pfeAdd;
	LPFJELEMENT   pfe;
	LPFJBARCODE   pfb;
	LPFJBITMAP    pfbm;
	LPFJCOUNTER   pfc;
	LPFJDATETIME  pfdt;
	LPFJTEXT      pft;
	LONG   lValue;
	LONG   lCountOld;
	FLOAT  fValue;
	char   formField[256];
	char   FieldName[20];
	char   FieldNumber[4];
	char   action[16];
	char   MsgName[FJMESSAGE_NAME_SIZE+1];
	char   MsgNameQ[FJMESSAGE_NAME_SIZE*2];
	int    i;
	int    iSeg;
	int    iSegOld;
	int    ret;
	BOOL   bRet;
	BOOL   bClose;
	BOOL   bNewMsg;

	bNewMsg = FALSE;
	ret = fj_HSGetValue( handle, "msg_id", MsgName, FJMESSAGE_NAME_SIZE+1 );
	if ( 0 < ret )
	{
		ret = fj_HSGetValue( handle, "msgname", formField, sizeof(formField) );
		// check to see if the message name changed - this is a new message
		if ( 0 != strcmp(MsgName, formField) ) bNewMsg = TRUE;

		ret = fj_HSGetValue( handle, "action", action, sizeof(action) );
		if ( 0 < ret )
		{
			if ( (0 == strcmp(action,"delete")) || (0 == strcmp(action,"update")) || (0 == strcmp(action,"elemadd")) ||(0 == strcmp(action,"elemcopy")) )
			{
				pfm = pCVT->fj_MessageNew();
				pfm->pfph = pfph;
				strcpy( pfm->strName, MsgName );
				iSeg = 0;
				ret = fj_HSGetValue( handle, "segments", formField, sizeof(formField) );
				if ( 0 < ret ) iSeg = atol(formField);
				for ( i = 0; i < iSeg; i++ )
				{
					sprintf( FieldNumber, "%i", (int)i );
					strcpy( FieldName, "segtype" );
					strcat( FieldName, FieldNumber );
					ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
					if ( 0 < ret )		// there has to be a type for the segment to be processed
					{
						pfe = pCVT->fj_ElementNewFromTypeString( formField);
						if ( NULL != pfe )
						{
							pfe->pfm = pfm;
							// add the new one to the array
							pfm->lCount = pCVT->fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );

							strcpy( FieldName, "segname" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, min(sizeof(formField)-1,FJELEMENT_NAME_SIZE+1) );
							if ( (0 < ret) && (0 !=  formField[0]) && (FALSE == bNewMsg) )
							{
								strcpy( pfe->strName, formField );
							}
							else
							{
								pCVT->fj_ElementMakeMemstorName( pfe, pfe->strName );
								// add segment & element to ROM/RAM buffer
								pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );
							}

							pfe->lPosition = FJ_POSITION_DISTANCE;
							strcpy( FieldName, "concat" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lPosition = FJ_POSITION_CONCATENATE;
							strcpy( FieldName, "parallel" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lPosition = FJ_POSITION_PARALLEL;

							strcpy( FieldName, "left" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret )
							{
								fValue = atof( formField );
								pfe->fLeft = pCVT->fj_FloatUnits( fValue, - pfsys->lUnits );
							}

							strcpy( FieldName, "nozzle" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret )
							{
								lValue = atol( formField );
								pfe->lRow = lValue;
							}

							lValue = pfe->pActions->fj_DescGetType();
							if      ( FJ_TYPE_BARCODE  == lValue ) na_processBarcodeElement ( handle, pfe, i );
							else if ( FJ_TYPE_BITMAP   == lValue ) na_processBitmapElement  ( handle, pfe, i );
							else if ( FJ_TYPE_COUNTER  == lValue ) na_processCounterElement ( handle, pfe, i );
							else if ( FJ_TYPE_DATETIME == lValue ) na_processDatetimeElement( handle, pfe, i );
							else if ( FJ_TYPE_DYNIMAGE == lValue ) na_processDynimageElement( handle, pfe, i );
							else if ( FJ_TYPE_TEXT     == lValue ) na_processTextElement    ( handle, pfe, i );

							pfe->lTransform = 0;
							strcpy( FieldName, "reverse" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_REVERSE;
							strcpy( FieldName, "inverse" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_INVERSE;
							strcpy( FieldName, "negative" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_NEGATIVE;
						}
					}
				}						// end of for loop thru elements

				bClose = TRUE;
				pfeNew = NULL;
				if ( 0 == strcmp(action,"elemadd") )
				{
					ret = fj_HSGetValue( handle, "addtype", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						if ( 0 == strcmp(formField, "Barcode") )
						{
							pfeNew = pCVT->fj_ElementBarCodeNew();
							pfb = (LPFJBARCODE)pfeNew->pDesc;
							strcpy( pfb->strText, "new" );
						}
						else if ( 0 == strcmp(formField, "Bitmap") )
						{
							pfeNew = pCVT->fj_ElementBitmapNew();
						}
						else if ( 0 == strcmp(formField, "Counter") )
						{
							pfeNew = pCVT->fj_ElementCounterNew();
						}
						else if ( 0 == strcmp(formField, "Datetime") )
						{
							pfeNew = pCVT->fj_ElementDatetimeNew();
						}
						else if ( 0 == strcmp(formField, "Dynimage") )
						{
							pfeNew = pCVT->fj_ElementDynimageNew();
						}
						else if ( 0 == strcmp(formField, "Text") )
						{
							pfeNew = pCVT->fj_ElementTextNew();
							pft = (LPFJTEXT)pfeNew->pDesc;
							strcpy( pft->strText, "new" );
						}
					}
					ret = fj_HSGetValue( handle, "addloc", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
					}
					bClose = FALSE;
				}
				else if ( 0 == strcmp(action,"elemcopy") )
				{
					ret = fj_HSGetValue( handle, "copyfrom", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
						if ( iSeg < pfm->lCount )
						{
							pfe = pfm->papfe[iSeg];
							pfeNew = pCVT->fj_ElementDup( pfe );
						}
					}
					ret = fj_HSGetValue( handle, "copyto", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
					}
					bClose = FALSE;
				}
				else if ( 0 == strcmp(action,"delete") )
				{
					ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						i = atoi(formField);
						pfe = pfm->papfe[i];
						if ( NULL != pfe )
						{
							pfm->lCount = pCVT->fj_ElementListRemove( &(pfm->papfe), pfm->lCount, pfe );
							pfe->pActions->fj_DescDeleteFromMemstor( pfph->pMemStorEdit, pfe );
							pfe->pActions->fj_DescDestroy( pfe );
						}
					}
					bClose = FALSE;
				}

				if ( NULL != pfeNew )
				{
					pfeNew->pfm = pfm;
					papfe = pfm->papfe;
					lCountOld = pfm->lCount;
					pfm->papfe = NULL;
					pfm->lCount = 0;
					iSegOld = 0;
					for ( i = 0; i <= lCountOld; i++ )
					{
						if ( i == iSeg )
						{
							pfeAdd = pfeNew;
						}
						else
						{
							pfeAdd = papfe[iSegOld];
							papfe[iSegOld] = NULL;
							iSegOld++;
						}
						pCVT->fj_ElementMakeMemstorName( pfeAdd, pfeAdd->strName );
						// add segment & element to ROM/RAM buffer
						pfeAdd->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfeAdd );
						pfm->lCount = pCVT->fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfeAdd );
					}
					pCVT->fj_ElementListDestroyAll( &papfe, 0 );
				}

				if ( TRUE == bClose )
				{
					HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">opener.location.replace(\"message.htm?pwid=");
					na_PWID(handle);
					HSSend(handle, "\");</SCRIPT>");
					HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">window.close();</SCRIPT>");
				}
				else
				{
					reload_message_edit(handle, MsgName);
				}
				if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle)) )
				{
					// add to MemStor
					bRet = pCVT->fj_MessageAddToMemstor( pfph->pMemStorEdit, pfm );
				}
				pCVT->fj_MessageDestroy( pfm );
			}
		}
	}
}										/* @! Function: Post_msgedit_1  }} */
/* @! End of post function   */

void na_ElementFont(unsigned long handle, LPFJMEMSTOR pfms, int j, BOOL bEdit, LPSTR pFontName )
{
	LPSTR *papFontStr;
	LPSTR  pParams[3];
	CHAR   str[64];
	int k;

	HSSend (handle, "<td><b>Font</b></td><td><select size=1 name=font");
	na_Long(handle, j);
	HSSend(handle, ">");
	papFontStr = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_FontID );
	while ( NULL != *papFontStr )
	{
		strncpy( str, *papFontStr, sizeof(str) );
		str[sizeof(str)-2] = '}';		// guarantee end for partial scan
		str[sizeof(str)-1] = 0;
		k = pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 2 );
		if ( k >= 2 )
		{
			HSSend(handle, "<option ");
			pCVT->fj_processParameterStringInput( pParams[1] );
			if ( NULL != pFontName ) if ( 0 == strcmp(pParams[1], pFontName) ) HSSend(handle, "SELECTED");
			HSSend(handle, ">");
			na_String(handle, pParams[1]);
			HSSend(handle, "</option>\n");
		}
		papFontStr++;
	}
	HSSend(handle, "</select></td>");
}
void na_ElementGap(unsigned long handle, int j, BOOL bEdit, LONG lGapValue )
{
	HSSend (handle, "<td><b>Gap</b></td><td><input type=text onchange=\"return gapCheck(this)\" name=gap");
	na_Long(handle, j);
	HSSend(handle, " size=2 maxlength=2 value=");
	na_Long(handle, lGapValue);
	HSSend(handle, "></td>");
}
void na_ElementLength(unsigned long handle, int j, BOOL bEdit, LONG lLengthValue, LONG lLengthMaxValue )
{
	HSSend (handle, "<td><b>Length</b></td><td><input type=text name=length");
	na_Long(handle, j);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle, lLengthMaxValue);
	HSSend(handle, ",'Length')\"");
	HSSend(handle, " size=2 maxlength=2 value=");
	na_Long(handle, lLengthValue);
	HSSend(handle, "></td>");
}
void na_ElementAlign(unsigned long handle, int j, BOOL bEdit, CHAR cAlign )
{
	HSSend (handle, "<td><b>Align</b></td><td><select type=text name=align");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	HSSend(handle, "<option value=\"L\"");
	if ( 'L' == cAlign ) HSSend(handle, " SELECTED");
	HSSend(handle, ">Left</option>");
	HSSend(handle, "<option value=\"R\"");
	if ( 'R' == cAlign ) HSSend(handle, " SELECTED");
	HSSend(handle, ">Right</option>");
	HSSend(handle, "</select></td>");
}
void na_ElementFill(unsigned long handle, int j, BOOL bEdit, CHAR cFill )
{
	HSSend (handle, "<td><b>Fill</b></td><td><input type=text name=fill");
	na_Long(handle, j);
	HSSend(handle, " size=1 maxlength=1 value=\"");
	na_Char(handle, cFill);
	HSSend(handle, "\"></td>");
}
void na_ElementSpace(unsigned long handle, LONG lCount)
{
	int i;
	for ( i = 0; i < lCount; i++ )
	{
		HSSend (handle, "<td>&nbsp;</td>");
	}
}
void na_ElementRowSpan(unsigned long handle)
{
	HSSend (handle, "<td rowspan=4>&nbsp;</td>");
}
void na_ElementFront1(unsigned long handle, int j, LPFJELEMENT pfe, BOOL bEdit, LPSTR pTitle )
{
	HSSend (handle, "<tr><input type=hidden name=segtype");
	na_Long(handle, j);
	HSSend(handle, " value=\"");
	if ( NULL != pfe ) HSSend(handle, pfe->pActions->fj_DescGetTypeString());
	HSSend (handle, "\"><input type=hidden name=segname");
	na_Long(handle, j);
	HSSend (handle, " value=\"");
	if ( NULL != pfe ) na_String(handle, pfe->strName);
	HSSend (handle, "\"><td class=fjh2>");
	HSSend (handle, pTitle);
	HSSend (handle, "</td>\n");
	// concatenate
	if ( 0 == j )
	{
		na_ElementSpace(handle, 2);
	}
	else
	{
		HSSend (handle, "<td><b>Concatenate</b></td><td><input type=checkbox value=ON onClick=concatCheck(this,document.form_le.elements[\"parallel");
		na_Long(handle, j);
		HSSend (handle, "\"],document.form_le.elements[\"Left");
		na_Long(handle, j);
		HSSend(handle, "\"]) name=concat");
		na_Long(handle, j);
		if ( NULL != pfe ) if ( FJ_POSITION_CONCATENATE == pfe->lPosition ) HSSend(handle, " CHECKED");
		HSSend(handle, "></td>\n");
	}
	na_ElementRowSpan(handle);
}
void na_ElementFront2(unsigned long handle, int j, LPFJELEMENT pfe, LONG lBoldValue )
{
	HSSend (handle, "<tr>");
	// parallel
	na_ElementSpace(handle, 1);
	if ( 0 == j )
	{
		na_ElementSpace(handle, 2);
	}
	else
	{
		HSSend (handle, "<td><b>Vertical Align</b></td><td><input type=checkbox value=ON onClick=concatCheck(this,document.form_le.elements[\"concat");
		na_Long(handle, j);
		HSSend (handle, "\"],document.form_le.elements[\"Left");
		na_Long(handle, j);

		HSSend(handle, "\"]) name=parallel");
		na_Long(handle, j);
		if ( NULL != pfe ) if ( FJ_POSITION_PARALLEL == pfe->lPosition ) HSSend(handle, " CHECKED");
		HSSend(handle, "></td>\n");
	}

	// bold
	if (FJ_TYPE_BARCODE != pfe->pActions->fj_DescGetType())
	{
		HSSend (handle, "<td><b>Bold</b></td><td><input type=text onchange=\"return boldCheck(this)\" name=bold");
		na_Long(handle, j);
		HSSend(handle, " size=2 maxlength=2 value=");
		na_Long(handle, lBoldValue);
		HSSend(handle, "></td>\n");
	}
	else na_ElementSpace(handle, 2);
}
void na_ElementFront3(unsigned long handle, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lWidthValue )
{
	LPFJSYSTEM  pfsys = pCVT->pfsys;

	HSSend (handle, "<tr>");
	// delete button
	if ( (TRUE == bEdit) && (NULL != pfe) )
	{
		//"<td rowspan=2><button onClick='submit1(\"delete\",\"");
		HSSend(handle, "<td><button class=fjbtn1 onClick='submit1(\"delete\",\"");
		na_Long(handle, j);
		HSSend(handle, "\")'>Delete</button></td>\n");
	}
	else
	{
		HSSend (handle, "<td rowspan=2>&nbsp;</td>\n");
	}
	// horizontal distance
	HSSend (handle, "<td><b>Horizontal Distance</b></td><td><input type=text onchange=\"return distCheck(this)\" name=Left");
	na_Long(handle, j);
	if ( FJ_POSITION_DISTANCE != pfe->lPosition ) HSSend(handle, " DISABLED ");
	HSSend(handle, " size=6 value=");
	na_Double(handle, (double)pCVT->fj_FloatUnits( pfe->fLeft, pfsys->lUnits), 2 );
	HSSend(handle, ">");
	na_String(handle, (*(pCVT->pfj_aUnits))[pfsys->lUnits].pShort);
	HSSend(handle, "</td>\n");
	// width
	if (FJ_TYPE_BARCODE != pfe->pActions->fj_DescGetType())
	{
		HSSend (handle, "<td><b>Width</b></td><td><input type=text onchange=\"return widthCheck(this)\" name=width");
		na_Long(handle, j);
		HSSend(handle, " size=2 maxlength=2 value=");
		na_Long(handle, lWidthValue);
		HSSend(handle, "></td>\n");
	}
	else na_ElementSpace(handle, 2);
}
void na_ElementFront4(unsigned long handle, int j, LPFJELEMENT pfe )
{
	HSSend (handle, "<tr>");
	HSSend (handle, "<td>#");
	na_Long(handle, (j+1));
	HSSend (handle, "</td>");
	// nozzle number
	HSSend (handle, "<td><b>Vertical Nozzle</b></td><td><input type=text onchange=\"return nozzleCheck(this)\" name=nozzle");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfe->lRow);
	HSSend(handle, "></td>\n");
}
void na_ElementBack2(unsigned long handle, int j, LPFJELEMENT pfe )
{
	// reverse
	HSSend (handle, "<td><b>Reverse</b></td><td><input type=checkbox value=ON name=reverse");
	na_Long(handle, j);
	if ( 0 != (pfe->lTransform & FJ_TRANS_REVERSE) ) HSSend(handle, " CHECKED");
	HSSend(handle, "></td></tr>\n");
}
void na_ElementBack3(unsigned long handle, int j, LPFJELEMENT pfe )
{
	// inverse
	HSSend (handle, "<td><b>Inverse</b></td><td><input type=checkbox value=ON name=inverse");
	na_Long(handle, j);
	if ( 0 != (pfe->lTransform & FJ_TRANS_INVERSE) ) HSSend(handle, " CHECKED");
	HSSend(handle, "></td></tr>\n");
}
void na_ElementBack4(unsigned long handle, int j, LPFJELEMENT pfe )
{
	// negative
	HSSend (handle, "<td><b>Negative</b></td><td><input type=checkbox value=ON name=negative");
	na_Long(handle, j);
	if ( 0 != (pfe->lTransform & FJ_TRANS_NEGATIVE) ) HSSend(handle, " CHECKED");
	HSSend(handle, "></td></tr>\n");
}
void na_ElementBarcode(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJSYSTEM  pfsys = pCVT->pfsys;
	LPFJBARCODE pfb;
	//	LONG lCode;
	int i;

	pfb = NULL;
	if( NULL != pfe ) pfb = (LPFJBARCODE)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Barcode" );
	// global barcode style
	HSSend (handle, "<td><b>Style</b></td><td><select size=1 name=style");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, i);
		if ( NULL != pfb ) if ( i == pfb->lSysBCIndex ) HSSend(handle, " SELECTED");
		HSSend(handle, ">");
		na_String(handle, pfsys->bcArray[i].strName);
		HSSend(handle, "</option>\n");
	}
	HSSend(handle, "</select></td>");
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Barcode</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfb->lBoldValue );
	// text
	HSSend(handle, "<td><input type=text name=text");
	na_Long(handle, j);
	HSSend(handle, " size=32 maxlength=");
	na_Long(handle, FJBARCODE_SIZE);
	HSSend(handle, " value=\"");
	if ( NULL != pfb ) na_String(handle, pfb->strText);
	HSSend(handle, "\"</td>\n");
	na_ElementSpace(handle, 4);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfb->lWidthValue );
	na_ElementSpace(handle, 1);
	// text alignment
	HSSend (handle, "<td><b>Text Align</b></td><td><select size=1 name=textalign");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	HSSend(handle, "<option value=0 ");
	if ( NULL != pfb ) if ( FALSE == pfb->bHROutput ) HSSend(handle, "SELECTED");
	HSSend(handle, ">(none)</option>");
	HSSend(handle, "<option value=C ");
	if ( NULL != pfb ) if ( (TRUE == pfb->bHROutput) && ('C' == pfb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Center</option>");
	HSSend(handle, "<option value=L ");
	if ( NULL != pfb ) if ( (TRUE == pfb->bHROutput) && ('L' == pfb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Left</option>");
	HSSend(handle, "<option value=R ");
	if ( NULL != pfb ) if ( (TRUE == pfb->bHROutput) && ('R' == pfb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Right</option>");
	HSSend(handle, "</select></td>");
	na_ElementSpace(handle, 2);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	na_ElementSpace(handle, 3);
	// text bold
	HSSend (handle, "<td><b>Text Bold</b></td><td><input type=text onchange=\"return boldCheck(this)\" name=textbold");
	na_Long(handle, j);
	HSSend(handle, " size=2 maxlength=2 value=");
	if ( NULL != pfb ) na_Long(handle, pfb->lHRBold);
	else               HSSend(handle, "0");
	HSSend(handle, "></td>\n");
	na_ElementSpace(handle, 2);
	na_ElementBack4(handle, j, pfe );
}
void na_ElementBitmap(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJBITMAP  pfbm;
	LPSTR *papBitmapStr;
	LPSTR  pParams[2];
	CHAR   str[64];
	int k;

	pfbm = NULL;
	if( NULL != pfe ) pfbm = (LPFJBITMAP)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Image" );
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Image</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfbm->lBoldValue );
	// image
	HSSend(handle, "<td><select size=1 name=image");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	papBitmapStr = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_bmpID );
	while ( NULL != *papBitmapStr )
	{
		strncpy( str, *papBitmapStr, sizeof(str) );
		str[sizeof(str)-2] = '}';		// guarantee end for partial scan
		str[sizeof(str)-1] = 0;
		k = pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 2 );
		if ( k >= 2 )
		{
			pCVT->fj_processParameterStringInput( pParams[1] );
			HSSend(handle, "<option ");
			if ( 0 == strcmp(pParams[1], pfbm->strName) ) HSSend(handle, "SELECTED");
			HSSend(handle, ">");
			na_String(handle, pParams[1]);
			HSSend(handle, "</option>\n");
		}
		papBitmapStr++;
	}
	HSSend(handle, "</select></td>");
	na_ElementSpace(handle, 4);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfbm->lWidthValue );
	na_ElementSpace(handle, 5);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	na_ElementSpace(handle, 7);
	na_ElementBack4(handle, j, pfe );
}
void na_ElementCounter(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJCOUNTER  pfc;
	LPSTR *papFontStr;
	LPSTR  pParams[3];
	CHAR   str[64];
	int k;

	pfc = NULL;
	if( NULL != pfe ) pfc = (LPFJCOUNTER)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Counter" );
	// font
	na_ElementFont(handle, pfms, j, bEdit, pfc->strFontName );
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Counter</b></td>");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfc->lBoldValue );
	// change
	HSSend(handle, "<td><b>Change</b><input type=text name=change");
	na_Long(handle, j);
	HSSend(handle, " size=9 maxlength=9 value=");
	na_Long(handle, pfc->lChange);
	HSSend(handle, " onChange=\"return parmICheck(this,-");
	na_Long(handle,FJCOUNTER_MAX_VALUE);
	HSSend(handle, ",");
	na_Long(handle,FJCOUNTER_MAX_VALUE);
	HSSend(handle, ",'Change')\"></td>\n");
	// start
	HSSend (handle, "<td><b>Start</b></td><td><input type=text name=start");
	na_Long(handle, j);
	HSSend(handle, " size=9 maxlength=9 value=");
	na_Long(handle, pfc->lStart);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJCOUNTER_MAX_VALUE);
	HSSend(handle, ",'Start')\"></td>\n");
	// length
										// 9 is JavaScript maxvalue parameter
	na_ElementLength(handle, j, bEdit, pfc->lLength , 9);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfc->lWidthValue );
	// repeat
	HSSend(handle, "<td><b>Repeat</b><input type=text name=repeat");
	na_Long(handle, j);
	HSSend(handle, " size=9 maxlength=9 value=");
	na_Long(handle, pfc->lRepeat);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJCOUNTER_MAX_VALUE);
	HSSend(handle, ",'Repeat')\"></td>\n");
	// limit
	HSSend (handle, "<td><b>Limit</b></td><td><input type=text name=limit");
	na_Long(handle, j);
	HSSend(handle, " size=9 maxlength=9 value=");
	na_Long(handle, pfc->lLimit);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJCOUNTER_MAX_VALUE);
	HSSend(handle, ",'Limit')\"></td>\n");
	// align
	na_ElementAlign(handle, j, bEdit, pfc->cAlign );
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	// gap
	na_ElementGap(handle, j, bEdit, pfc->lGapValue );
	na_ElementSpace(handle, 3);
	// fill
	na_ElementFill(handle, j, bEdit, pfc->cFill );
	na_ElementBack4(handle, j, pfe );
}

void na_ElementDataMatrix(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJSYSTEM  pfsys = pCVT->pfsys;
	LPFJDATAMATRIX pfdb;
	//	LONG lCode;
	int i,k;

	pfdb = NULL;
	if( NULL != pfe ) pfdb = (LPFJDATAMATRIX)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "DataMatrix" );
	// global barcode style
	HSSend (handle, "<td><b>Style</b></td><td><select size=1 name=style");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, i);
		if ( NULL != pfdb ) if ( i == pfdb->lSysBCIndex ) HSSend(handle, " SELECTED");
		HSSend(handle, ">");
		na_String(handle, pfsys->bcArray[i].strName);
		HSSend(handle, "</option>\n");
	}
	HSSend(handle, "</select></td>");
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>DataMatrix</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, 0);
	// text
	HSSend(handle, "<td><b>Data source </b><select type=text name=datasrc");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	for ( k = 0; k <= FJMESSAGE_DYNAMIC_SEGMENTS ; k++ )
	{
		HSSend(handle, "<option value=");

		if (k > 0)  na_Long(handle, k);
		else	    HSSend (handle, "Not used");
		
		HSSend(handle, " ");
		if ( k == pfdb->lDynIndex) HSSend(handle, "SELECTED");
		HSSend(handle, ">");

		if (k > 0)  na_Long(handle, k);
		else	    HSSend (handle, "Not used");

		HSSend(handle, "</option>");
	}
	HSSend(handle, "</select></td>\n");
	// start
	HSSend (handle, "<td><b>Start character</b></td><td><input type=text name=start");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->lDynFirstChar+1);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Start character')\"></td>\n");
	// limit
	HSSend (handle, "<td><b>Limit</b></td><td><input type=text name=limit");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->lDynNumChar);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Limit')\"></td>\n");
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, 0);
	HSSend (handle, "<td><b>Text</b></td>\n");
	na_ElementSpace(handle, 2);
	na_ElementSpace(handle, 2);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	na_ElementSpace(handle, 2);
	// text
	HSSend(handle, "<td><input type=text name=text");
	na_Long(handle, j);
	HSSend(handle, " size=32 maxlength=");
	na_Long(handle, FJDATAMATRIX_SIZE);
	HSSend(handle, " value=");
	na_StringWithDoubleQuotes(handle, pfdb->strText);
	HSSend(handle, "</td>\n");
	// width
	HSSend (handle, "<td><b>Symbol width</b></td><td><input type=text name=cx");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->cx);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle,FJDATAMATRIX_WITDH);
	HSSend(handle, ",'cx')\"></td>\n");
	// height
	HSSend (handle, "<td><b>Symbol height</b></td><td><input type=text name=cy");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->cy);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle,FJDATAMATRIX_HEIGHT);
	HSSend(handle, ",'cy')\"></td>\n");
	na_ElementBack4(handle, j, pfe );
}

void na_ElementDynBarcode(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJSYSTEM  pfsys = pCVT->pfsys;
	LPFJDYNBARCODE pfdb;
	//	LONG lCode;
	int i,k;

	pfdb = NULL;
	if( NULL != pfe ) pfdb = (LPFJDYNBARCODE)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Dynamic Barcode" );
	// global barcode style
	HSSend (handle, "<td><b>Style</b></td><td><select size=1 name=style");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, i);
		if ( NULL != pfdb ) if ( i == pfdb->lSysBCIndex ) HSSend(handle, " SELECTED");
		HSSend(handle, ">");
		na_String(handle, pfsys->bcArray[i].strName);
		HSSend(handle, "</option>\n");
	}
	HSSend(handle, "</select></td>");
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Dynamic Barcode</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfdb->lBoldValue );
	// text
	HSSend(handle, "<td><b>Data source </b><select type=text name=datasrc");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	for ( k = 0; k < FJMESSAGE_DYNAMIC_SEGMENTS ; k++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, (k+1) );
		HSSend(handle, " ");
		if ( k == pfdb->lDynamicID) HSSend(handle, "SELECTED");
		HSSend(handle, ">");
		na_Long(handle, (k+1) );
		HSSend(handle, "</option>");
	}
	HSSend(handle, "</select></td>\n");
	// start
	HSSend (handle, "<td><b>Start character</b></td><td><input type=text name=start");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->lFirstChar+1);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Start character')\"></td>\n");
	// limit
	HSSend (handle, "<td><b>Limit</b></td><td><input type=text name=limit");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdb->lNumChar);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Limit')\"></td>\n");
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfdb->lWidthValue );
	na_ElementSpace(handle, 1);
	// text alignment
	HSSend (handle, "<td><b>Text Align</b></td><td><select size=1 name=textalign");
	na_Long(handle, j);
	HSSend(handle, ">\n");
	HSSend(handle, "<option value=0 ");
	if ( NULL != pfdb ) if ( FALSE == pfdb->bHROutput ) HSSend(handle, "SELECTED");
	HSSend(handle, ">(none)</option>");
	HSSend(handle, "<option value=C ");
	if ( NULL != pfdb ) if ( (TRUE == pfdb->bHROutput) && ('C' == pfdb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Center</option>");
	HSSend(handle, "<option value=L ");
	if ( NULL != pfdb ) if ( (TRUE == pfdb->bHROutput) && ('L' == pfdb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Left</option>");
	HSSend(handle, "<option value=R ");
	if ( NULL != pfdb ) if ( (TRUE == pfdb->bHROutput) && ('R' == pfdb->cHRAlign) ) HSSend(handle, "SELECTED");
	HSSend(handle, ">Right</option>");
	HSSend(handle, "</select></td>");
	na_ElementSpace(handle, 2);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	na_ElementSpace(handle, 3);
	// text bold
	HSSend (handle, "<td><b>Text Bold</b></td><td><input type=text onchange=\"return boldCheck(this)\" name=textbold");
	na_Long(handle, j);
	HSSend(handle, " size=2 maxlength=2 value=");
	if ( NULL != pfdb ) na_Long(handle, pfdb->lHRBold);
	else               HSSend(handle, "0");
	HSSend(handle, "></td>\n");
	na_ElementSpace(handle, 2);
	na_ElementBack4(handle, j, pfe );
}
void na_ElementDynText(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJDYNTEXT  pfdynt;
	LPSTR *papFontStr;
	LPSTR  pParams[3];
	CHAR   str[64];
	int k;

	pfdynt = NULL;
	if( NULL != pfe ) pfdynt = (LPFJDYNTEXT)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Dynamic Text" );
	// font
	na_ElementFont(handle, pfms, j, bEdit, pfdynt->strFontName );
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Dynamic Text</b></td>");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfdynt->lBoldValue );
	// data source
	HSSend(handle, "<td><b>Data source </b><select type=text name=datasrc");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	for ( k = 0; k < FJMESSAGE_DYNAMIC_SEGMENTS ; k++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, (k+1) );
		HSSend(handle, " ");
		if ( k == pfdynt->lDynamicID) HSSend(handle, "SELECTED");
		HSSend(handle, ">");
		na_Long(handle, (k+1) );
		HSSend(handle, "</option>");
	}
	HSSend(handle, "</select></td>\n");
	// start
	HSSend (handle, "<td><b>Start character</b></td><td><input type=text name=start");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdynt->lFirstChar+1);
	HSSend(handle, " onChange=\"return parmCheck(this,1,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Start character')\"></td>\n");
	// length
	// 256 is JavaScript maxvalue parameter
	na_ElementLength(handle, j, bEdit, pfdynt->lLength, 256);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfdynt->lWidthValue );
	na_ElementSpace(handle, 1);
	// limit
	HSSend (handle, "<td><b>Limit</b></td><td><input type=text name=limit");
	na_Long(handle, j);
	HSSend(handle, " size=3 maxlength=3 value=");
	na_Long(handle, pfdynt->lNumChar);
	HSSend(handle, " onChange=\"return parmCheck(this,0,");
	na_Long(handle,FJTEXT_SIZE);
	HSSend(handle, ",'Limit')\"></td>\n");
	// align
	na_ElementAlign(handle, j, bEdit, pfdynt->cAlign );
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	// gap
	na_ElementGap(handle, j, bEdit, pfdynt->lGapValue );
	na_ElementSpace(handle, 3);
	// fill
	na_ElementFill(handle, j, bEdit, pfdynt->cFill );
	na_ElementBack4(handle, j, pfe );
}
void na_ElementDatetime(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJDATETIME  pfdt;
	LPSTR *papFontStr;
	LPSTR  pParams[3];
	CHAR   str[64];
	int k;

	pfdt = NULL;
	if( NULL != pfe ) pfdt = (LPFJDATETIME)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Date&nbsp;Time" );
	// font
	na_ElementFont(handle, pfms, j, bEdit, pfdt->strFontName );
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Date Time</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfdt->lBoldValue );
	// format
	HSSend(handle, "<td><select type=text name=format");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	for ( k = 0; k < sizeof(fj_dt_edit_table)/sizeof(struct fj_dttable) ; k++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, fj_dt_edit_table[k].lID );
		HSSend(handle, " ");
		if ( fj_dt_edit_table[k].lID == pfdt->lFormat) HSSend(handle, "SELECTED");
		HSSend(handle, ">");
		na_String(handle, fj_dt_edit_table[k].sName);
		HSSend(handle, "</option>");
	}
	HSSend(handle, "</select></td>\n");
	// delimiter
	HSSend (handle, "<td><b>Delimiter</b></td><td><input type=text name=delim");
	na_Long(handle, j);
	HSSend(handle, " size=1 maxlength=1 value=\"");
	if ( NULL != pfdt ) na_Char(handle, pfdt->cDelim);
	HSSend(handle, "\"></td>");
	// length
										// 99 is JavaScript maxvalue parameter
	na_ElementLength(handle, j, bEdit, pfdt->lLength ,99);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfdt->lWidthValue );
	// time
	HSSend(handle, "<td><select type=text name=time");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	for ( k = 0; k < sizeof(fj_dt_time_table)/sizeof(struct fj_dttable) ; k++ )
	{
		HSSend(handle, "<option value=");
		na_Long(handle, fj_dt_time_table[k].lID );
		if ( fj_dt_time_table[k].lID == pfdt->lTimeType) HSSend(handle, " SELECTED");
		HSSend(handle, ">");
		na_String(handle, fj_dt_time_table[k].sName);
		HSSend(handle, "</option>");
	}
	HSSend(handle, "</select></td>\n");
	// string
	HSSend (handle, "<td><b>String</b></td><td><select type=text name=strings");
	na_Long(handle, j);
	HSSend(handle, " size=1>");
	HSSend(handle, "<option value=1");
	if ( TRUE == pfdt->bStringNormal ) HSSend(handle, " SELECTED");
	HSSend(handle, ">Normal</option>");
	HSSend(handle, "<option value=0");
	if ( FALSE == pfdt->bStringNormal ) HSSend(handle, " SELECTED");
	HSSend(handle, ">Special</option>");
	HSSend(handle, "</select></td>");
	// align
	na_ElementAlign(handle, j, bEdit, pfdt->cAlign );
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	// gap
	na_ElementGap(handle, j, bEdit, pfdt->lGapValue );
	na_ElementSpace(handle, 1);
	// shifts
	HSSend (handle, "<td><b>Hour Groups</b></td><td><input type=text onchange=\"return parmCheck(this,1,24,'Hour Groups');\" name=shifts");
	na_Long(handle, j);
	HSSend(handle, " size=2 maxlength=2 value=");
	if ( NULL != pfdt ) na_Long(handle, pfdt->lHourACount);
	else                HSSend(handle, "1");
	HSSend(handle, "></td>\n");
	// fill
	na_ElementFill(handle, j, bEdit, pfdt->cFill );
	na_ElementBack4(handle, j, pfe );
}
void na_ElementDynimage(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJDYNIMAGE pfd;
	LPSTR *papBitmapStr;
	LPSTR  pParams[2];
	CHAR   str[64];
	int k;

	pfd = NULL;
	if( NULL != pfe ) pfd = (LPFJDYNIMAGE)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Dynamic Image" );
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Dynamic Image</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pfd->lBoldValue );
	na_ElementSpace(handle, 5);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pfd->lWidthValue );
	na_ElementSpace(handle, 5);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	na_ElementSpace(handle, 7);
	na_ElementBack4(handle, j, pfe );
}
void na_ElementText(unsigned long handle, LPFJMEMSTOR pfms, int j, LPFJELEMENT pfe, BOOL bEdit, LONG lCols )
{
	LPFJTEXT  pft;
	LPSTR *papFontStr;
	LPSTR  pParams[3];
	CHAR   str[64];
	int k;

	pft = NULL;
	if( NULL != pfe ) pft = (LPFJTEXT)pfe->pDesc;

	//
	// row 1
	//
	na_ElementFront1(handle, j, pfe, bEdit, "Text" );
	// font
	na_ElementFont(handle, pfms, j, bEdit, pft->strFontName );
	na_ElementRowSpan(handle);
	HSSend (handle, "<td><b>Text</b></td>\n");
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	na_ElementRowSpan(handle);
	na_ElementSpace(handle, 2);
	HSSend (handle, "</tr>\n");

	//
	// row 2
	//
	na_ElementFront2(handle, j, pfe, pft->lBoldValue );
	// text
	HSSend(handle, "<td><input type=text name=text");
	na_Long(handle, j);
	HSSend(handle, " size=32 maxlength=");
	na_Long(handle, FJTEXT_SIZE);
	HSSend(handle, " value=");
	na_StringWithDoubleQuotes(handle, pft->strText);
	HSSend(handle, "</td>\n");

	na_ElementSpace(handle, 4);
	na_ElementBack2(handle, j, pfe );

	//
	// row 3
	//
	na_ElementFront3(handle, j, pfe, bEdit, pft->lWidthValue );
	na_ElementSpace(handle, 5);
	na_ElementBack3(handle, j, pfe );

	//
	// row 4
	//
	na_ElementFront4(handle, j, pfe );
	// gap
	na_ElementGap(handle, j, bEdit, pft->lGapValue );
	na_ElementSpace(handle, 5);
	na_ElementBack4(handle, j, pfe );
}
/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_ID  {{ */
/* @! Function: na_MSG_EDIT_PARAM  {{ */
void na_MSG_EDIT_PARAM(unsigned long handle)
{
	/* add your implementation here: */
	char   formField[50];
	int    ret;

	sMsgEditName[0] = 0;
	ret = fj_HSGetValue( handle, "msg_edit", formField, 50 );
	if ( 0 < ret )
	{
		if ( 0 != strcmp( formField, "-1") )
		{
			strcpy( sMsgEditName, formField );
			// message Name
			pCVT->fj_processParameterStringOutput( sMsgEditNameQuote, sMsgEditName );
		}
		else
		{
			// sMsgEditName is already a 0
			strcpy( sMsgEditNameQuote, "\"\"" );
		}
	}
}										/* @! Function: na_MSG_EDIT_PARAM  }} */
/* @! Function: na_MSG_EDIT_TABLE  {{ */
void na_MSG_EDIT_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL  pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph   = pCVT->pfph;
	LPFJMEMSTOR   pfms;
	LPFJMESSAGE   pfm;
	LPFJELEMENT   pfe;

	LONG lType;
	LONG lCols;
	BOOL bEdit;
	int  i, j;
	int  jSeg;

	bEdit = pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle));
	if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
	else                 pfms = pfph->pMemStor;

	lMsgEditSegs = 0;
	if ( NULL != pfms )
	{
		lMsgEditSegs = 0;
		jSeg = 0;
		lCols = 17;
		pfm = pCVT->fj_MessageBuildFromName( pfms, sMsgEditNameQuote );
		if ( NULL != pfm )
		{
			lMsgEditSegs = pfm->lCount;
			jSeg = pfm->lCount;
		}

		HSSend (handle, "<col align=center style=\"width:130px\">");
		HSSend (handle, "<col align=right  style=\"width: 80px\">");
		HSSend (handle, "<col align=left   style=\"width: 60px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 50px\">");
		HSSend (handle, "<col align=left   style=\"width: 80px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=center style=\"width:200px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 50px\">");
		HSSend (handle, "<col align=left   style=\"width: 80px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 60px\">");
		HSSend (handle, "<col align=left   style=\"width: 60px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 60px\">");
		HSSend (handle, "<col align=left   style=\"width: 20px\">\n");
		HSSend (handle, "<tr><input type=hidden name=segments value=");
		na_Long(handle, jSeg);
		HSSend (handle, "><input type=hidden name=msgname value=");
		na_String(handle, sMsgEditName);
		HSSend (handle, "><td align=left colspan=");
		na_Long(handle, lCols);
		HSSend (handle, "><b>Message Name = </b><input type=text size=10 name=msg_id onchange=IDCheck(this) value=\"");
		na_String (handle, sMsgEditName);
		HSSend (handle, "\"></td>\</tr>\n");

		for ( j = 0; j < jSeg; j++ )
		{
			pfe = NULL;
			lType = FJ_TYPE_TEXT;
			pfe = pfm->papfe[j];
			if ( NULL != pfe )
			{
				lType = pfe->pActions->fj_DescGetType();
			}

			HSSend (handle, "<tr><td colspan=");
			na_Long(handle, lCols);
			HSSend (handle, "><hr></td></tr>\n");
			if      ( FJ_TYPE_BARCODE  == lType ) na_ElementBarcode ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_BITMAP   == lType ) na_ElementBitmap  ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_COUNTER  == lType ) na_ElementCounter ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DATETIME == lType ) na_ElementDatetime( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DYNIMAGE == lType ) na_ElementDynimage( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_TEXT     == lType ) na_ElementText    ( handle, pfms, j, pfe, bEdit, lCols );
		}
		HSSend (handle, "<tr><td colspan=");
		na_Long(handle, lCols);
		HSSend (handle, "><hr></td></tr>\n");
		pCVT->fj_MessageDestroy( pfm );
	}
}										/* @! Function: na_MSG_EDIT_TABLE  }} */
/* @! Function: na_MSG_IDS  {{ */
void na_MSG_IDS(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph   = pCVT->pfph;
	LPSTR *papMsg;
	LPSTR  pParams[2];
	STR    str[FJMESSAGE_MEMSTOR_SIZE];
	int i;

	HSSend(handle, "fjID = \"");
	HSSend(handle, sMsgEditName);
	HSSend(handle, "\"\n");
	HSSend(handle, "fjIDs = new Array(");
	if ( NULL != pfph->pMemStorEdit )
	{
		papMsg = pCVT->fj_MemStorFindAllElements( pfph->pMemStorEdit, pCVT->pfj_MessageID );
		for ( i = 0; NULL != *(papMsg+i); i++ )
		{
			strcpy( str, *(papMsg+i) );
			pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 2 );
			pCVT->fj_processParameterStringInput( pParams[1] );
			if ( 0 != i ) HSSend(handle, ",");
			HSSend(handle, "\"");
			na_String(handle, pParams[1]);
			HSSend(handle, "\"");
			if ( 0 == ((i+1)%10) ) HSSend(handle, "\n");
		}
	}
	HSSend(handle, ")\n");
}										/* @! Function: na_MSG_IDS  }} */
/* @! Function: na_ME_title  {{ */
void na_ME_title(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pCVT->pfph, HSRemoteAddress(handle)) )
		HSSend(handle, "Message Edit");
	else
		HSSend(handle, "Message Details");
}										/* @! Function: na_ME_title  }} */
/* @! Function: na_AddLoc  {{ */
void na_AddLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lMsgEditSegs+1); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_AddLoc  }} */
/* @! Function: na_CopyFromLoc  {{ */
void na_CopyFromLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lMsgEditSegs); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_CopyFromLoc  }} */
/* @! Function: na_CopyToLoc  {{ */
void na_CopyToLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lMsgEditSegs+1); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_CopyToLoc  }} */
/* @! Function: na_button_submit  {{ */
void na_button_submit(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pCVT->pfph, HSRemoteAddress(handle)) )
		HSSend (handle, "<p align=center><button class=fjbtn1 onClick='submit1(\"update\",0)'>Submit</button><button class=fjbtn1 onClick=\"myreset(this.form)\">Reset</button></p>\n");
}										/* @! Function: na_button_submit  }} */
/* @! Function: na_button_addcopy  {{ */
void na_button_addcopy(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pCVT->pfph, HSRemoteAddress(handle)) )
	{
		HSSend (handle, "<p align=left>\n");
		HSSend (handle, "    <table border=0 cellpadding=3>\n");
		HSSend (handle, "      <tr align=center>\n");
		HSSend (handle, "        <td><button onClick='submit1(\"elemadd\",0)'>&nbsp;Add Element&nbsp;</button></td>\n");
		HSSend (handle, "        <td>Type\n");
		HSSend (handle, "          <select size=1 name=addtype>\n");
		HSSend (handle, "            <option value=Text SELECTED>Text</option>\n");
		HSSend (handle, "            <option value=Barcode>Barcode</option>\n");
		HSSend (handle, "            <option value=Counter>Counter</option>\n");
		HSSend (handle, "            <option value=Datetime>Date&nbsp;Time</option>\n");
		HSSend (handle, "            <option value=Bitmap>Image</option>\n");
		HSSend (handle, "            <option value=Dynimage>Dynamic Image</option>\n");
		HSSend (handle, "          </select>\n");
		HSSend (handle, "        </td>\n");
		HSSend (handle, "        <td>At Location\n");
		HSSend (handle, "          <select size=1 name=addloc>");
		na_AddLoc (handle);
		HSSend (handle, "\n");
		HSSend (handle, "          </select>\n");
		HSSend (handle, "        </td>\n");
		HSSend (handle, "      </tr>\n");

		if ( lMsgEditSegs > 0 )
		{
			HSSend (handle, "      <tr align=center>\n");
			HSSend (handle, "        <td><button onClick='submit1(\"elemcopy\",0)'>Copy Element</button></td>\n");
			HSSend (handle, "        <td>From Location\n");
			HSSend (handle, "          <select size=1 name=copyfrom>\n");
			HSSend (handle, "            ");
			na_CopyFromLoc (handle);
			HSSend (handle, "\n");
			HSSend (handle, "          </select>\n");
			HSSend (handle, "        </td>\n");
			HSSend (handle, "        <td>To Location\n");
			HSSend (handle, "          <select size=1 name=copyto>\n");
			HSSend (handle, "            ");
			na_CopyToLoc (handle);
			HSSend (handle, "\n");
			HSSend (handle, "          </select>\n");
			HSSend (handle, "        </td>\n");
			HSSend (handle, "      </tr>\n");
		}
		HSSend (handle, "    </table>\n");
		HSSend (handle, "    </p>\n");
	}
}										/* @! Function: na_button_addcopy  }} */
/* @! Function: na_PWID  {{ */
/* @! End of dynamic functions   */
