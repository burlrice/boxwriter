#include <mailcapi.h>
#include "fj.h"
#include "fj_system.h"

//
// initialize email
//
//
VOID fj_print_head_InitEmail( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPFJSYSTEM  pfsys   = fj_CVT.pfsys;
	devBoardParamsType nvParams;
	STR   strIPEmail[32];
	ULONG lIPEmail;
	int i;
	int iRet;
	int iDNSServers;

	pGlobal->lEmailHandle = 0;

	// read NETsilicon parameters
	// set lock to avoid error message
	tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	iRet = NAReadDevBoardParams( &nvParams );
	tx_semaphore_put( &(pGlobal->semaROMLock) );
	if ( 0 == iRet )
	{
		if ( 0 == nvParams.useDhcp )
		{
			for ( i = 0; i < FJSYS_NUMBER_DNS; i++ )
			{
				if ( 0 != pfsys->alAddrDNS[i])
				{
					iRet = DNSAddServer( pfsys->alAddrDNS[i] );
				}
			}
		}
	}
	// just for checking
	iDNSServers = DNSGetServers( pGlobal->aipAddrDNS,FJSYS_NUMBER_DNS);

	pGlobal->ipAddrEmail = pfsys->lAddrEmail;
	if ( 0 != pfsys->strEmailServer[0] )
	{
		lIPEmail = DNSgethostbyname( pfsys->strEmailServer );
		if ( 0 != lIPEmail ) pGlobal->ipAddrEmail = lIPEmail;
	}

	if ( 0 != pGlobal->ipAddrEmail )
	{
		fj_IPToStr( pGlobal->ipAddrEmail, strIPEmail );
		pGlobal->lEmailHandle = MCCreate( POP3, 110, NULL, 25, strIPEmail );
	}

	return;
}
VOID fj_print_head_SendEmail( LPSTR pStrText, LPSTR pStrTitle, LPSTR pStrTo )
{
	STR strFrom[FJSYS_NAME_SIZE+FJPH_ID_SIZE+FJSYS_EMAIL_ID_SIZE+4];
	int iMailStatus;

	strcpy( strFrom, fj_CVT.pfsys->strID );
	strcat( strFrom, "_" );
	strcat( strFrom, fj_CVT.pfph->strID );
	if ( 0 != fj_CVT.pfsys->strEmailName[0] )
	{
		strcat( strFrom, "@" );
		strcat( strFrom, fj_CVT.pfsys->strEmailName );
	}

	if ( 0 != fj_CVT.pFJglobal->lEmailHandle )
	{
		iMailStatus = MCSendSimpleMail( fj_CVT.pFJglobal->lEmailHandle,
			strFrom,					// from
			pStrTo,						// to
			pStrTitle,					// subject
			pStrText,
			strlen(pStrText) );
	}
}
VOID fj_print_head_SendEmailInk( LPSTR pStrText, LPSTR pStrTitle )
{
	int i;

	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		if ( 0 != fj_CVT.pfsys->astrEmailInk[i][0] )
		{
			fj_print_head_SendEmail( pStrText, pStrTitle, fj_CVT.pfsys->astrEmailInk[i] );
		}
	}
}
VOID fj_print_head_SendEmailService( LPSTR pStrText, LPSTR pStrTitle )
{
	int i;

	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		if ( 0 != fj_CVT.pfsys->astrEmailService[i][0] )
		{
			fj_print_head_SendEmail( pStrText, pStrTitle, fj_CVT.pfsys->astrEmailService[i] );
		}
	}
}
