/* C file: c:\MyPrograms\FoxJet1\_fjscript_edit.c created
 by NET+Works HTML to C Utility on Tuesday, July 03, 2001 11:04:07 */
#include "fj.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_EDIT_NOZZLES */
static void na_EDIT_NOZZLES (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\fjscript_edit.htm {{ */
void Send_function_8(unsigned long handle)
{
	HSSend (handle, "\n");
	HSSend (handle, "var decimalPointDelimiter = \".\"\n");
	HSSend (handle, "function isEmpty(s){return ((s == null) || (s.length == 0))}\n");
	HSSend (handle, "function isDigit(c){return ((c >= \"0\") && (c <= \"9\"))}\n");
	HSSend (handle, "function isInteger(s,emptyOK)\n");
	HSSend (handle, "{   var i\n");
	HSSend (handle, "    if (isEmpty(s)) return emptyOK\n");
	HSSend (handle, "    i = 0;\n");
	HSSend (handle, "    if ('-' == s.charAt(i)) i = 1\n");
	HSSend (handle, "    for (; i < s.length; i++){ if (!isDigit(s.charAt(i))) return false }\n");
	HSSend (handle, "    return true\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function isFloat(s,emptyOK)\n");
	HSSend (handle, "{   var i\n");
	HSSend (handle, "    var seenDecimalPoint = false\n");
	HSSend (handle, "    if (isEmpty(s)) return emptyOK\n");
	HSSend (handle, "    if (s == decimalPointDelimiter) return false\n");
	HSSend (handle, "    i = 0\n");
	HSSend (handle, "    if ('-' == s.charAt(i)) i = 1\n");
	HSSend (handle, "    for ( ; i < s.length; i++)\n");
	HSSend (handle, "    {   \n");
	HSSend (handle, "        var c = s.charAt(i)\n");
	HSSend (handle, "        if ((c == decimalPointDelimiter) && !seenDecimalPoint) seenDecimalPoint = true\n");
	HSSend (handle, "        else if (!isDigit(c)) return false\n");
	HSSend (handle, "    }\n");
	HSSend (handle, "    return true\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function isParmGood(value,low,high) {\n");
	HSSend (handle, "  var isok = isInteger(value,false);\n");
	HSSend (handle, "  if ( true == isok ) {if ( value < low  ) isok = false}\n");
	HSSend (handle, "  if ( true == isok ) {if ( value > high ) isok = false}\n");
	HSSend (handle, "  return isok\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function isParmFDGood(value,low,high) {\n");
	HSSend (handle, "  var isok = isFloat(value,false);\n");
	HSSend (handle, "  if ( true == isok ) {if ( value < low  ) isok = false}\n");
	HSSend (handle, "  if ( true == isok ) {if ( value > high ) isok = false}\n");
	HSSend (handle, "  return isok\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function isParmFGood(value,low) {\n");
	HSSend (handle, "  var isok = isFloat(value,false);\n");
	HSSend (handle, "  if ( true == isok ) {if ( value < low  ) isok = false}\n");
	HSSend (handle, "  return isok\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function parmCheck(Ot,low,high,name) {\n");
	HSSend (handle, "  var isok = isParmGood(Ot.value,low,high)\n");
	HSSend (handle, "  if ( false == isok ){alert(name+\" value must be a positive integer from \"+low+\" to \"+high+\".\")}\n");
	HSSend (handle, "  if ( false == isok ) Ot.value = low\n");
	HSSend (handle, "  if ( false == isok ) Ot.focus()\n");
	HSSend (handle, "  return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function parmICheck(Ot,low,high,name) {\n");
	HSSend (handle, "  var isok = isParmGood(Ot.value,low,high)\n");
	HSSend (handle, "  if ( false == isok ){alert(name+\" value must be a integer from \"+low+\" to \"+high+\".\")}\n");
	HSSend (handle, "  if ( false == isok ) Ot.value = low\n");
	HSSend (handle, "  if ( false == isok ) Ot.focus()\n");
	HSSend (handle, "  return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function parmFDCheck(Ot,low,high,name) {\n");
	HSSend (handle, "  var isok = isParmFDGood(Ot.value,low,high)\n");
	HSSend (handle, "  if ( false == isok ){alert(name+\" value must be a floating point number from \"+low+\" to \"+high+\".\")}\n");
	HSSend (handle, "  if ( false == isok ) Ot.value = low\n");
	HSSend (handle, "  if ( false == isok ) Ot.focus()\n");
	HSSend (handle, "  return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function parmFCheck(Ot,low,name) {\n");
	HSSend (handle, "  var isok = isParmFGood(Ot.value,low)\n");
	HSSend (handle, "  if ( false == isok ){alert(name+\" value must be a positive floating point number.\")}\n");
	HSSend (handle, "  if ( false == isok ) Ot.value = low\n");
	HSSend (handle, "  if ( false == isok ) Ot.focus();\n");
	HSSend (handle, "  return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function distCheck(Ot) {\n");
	HSSend (handle, "  var name = Ot.name;\n");
	HSSend (handle, "  if (true == isDigit(name.charAt( name.length - 1 ))) name = \"Distance\"\n");
	HSSend (handle, "  var isok = parmFCheck(Ot,0,name);\n");
	HSSend (handle, "  if ( Ot.value > MaxDist )\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "  	alert(name+\" value must be a positive floating point number from 0 to \"+MaxDist+\".\");\n");
	HSSend (handle, "  	Ot.value = MaxDist;\n");
	HSSend (handle, "  	Ot.focus();\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "\n");
	HSSend (handle, "function nozzleCheck(Ot){ return parmCheck(Ot,0,");
	na_EDIT_NOZZLES (handle);
	HSSend (handle, " -1,\"Nozzle\")}\n");
	HSSend (handle, "function boldCheck(Ot)  { return parmCheck(Ot,0,31,\"Bold\")  }\n");
	HSSend (handle, "function widthCheck(Ot) { return parmCheck(Ot,1,32,\"Width\") }\n");
	HSSend (handle, "function gapCheck(Ot)   { return parmCheck(Ot,0,31,\"Gap\")   }\n");
	HSSend (handle, "function concatCheck(Othis, Oother, Ol) {\n");
	HSSend (handle, "  var any = false;\n");
	HSSend (handle, "  if (true == Othis.checked) {any = true; Oother.checked = false;}\n");
	HSSend (handle, "  if (true == Oother.checked){any = true;}\n");
	HSSend (handle, "  if (true == any ) Ol.disabled = true\n");
	HSSend (handle, "  else              Ol.disabled = false\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function ipCheck(Ob, name) {\n");
	HSSend (handle, "  var s = Ob.value\n");
	HSSend (handle, "  var parts = new Array();\n");
	HSSend (handle, "  n = 0;\n");
	HSSend (handle, "  parts[0] = \"\";\n");
	HSSend (handle, "  for ( i = 0; i < s.length; i ++ )\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "    var c = s.charAt(i)\n");
	HSSend (handle, "    if ( '.' == c ){ n++;parts[n]=\"\"}\n");
	HSSend (handle, "    else parts[n] = parts[n] + c;\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  if ( 3 != n ){alert(name+\" must have 4 parts.\");Ot.focus(); return false;}\n");
	HSSend (handle, "  for ( n = 0; n < 4; n ++ )\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "    s = \" \" + parts[n];\n");
	HSSend (handle, "    if ( 1 == s.length ){n++;alert(name+\" part \"+n+\" can not be blank.\");Ot.focus(); return false;}\n");
	HSSend (handle, "    if ( 255 < parts[n] ){n++;alert(name+\" part \"+n+\" must be between 0 and 255.\");Ot.focus(); return false;}\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  return true;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "// fjIDs must be declared outside this script\n");
	HSSend (handle, "function isIDNew(n){\n");
	HSSend (handle, "  for (var i = 0; i < fjIDs.length; i++){ if (fjIDs[i] == n) return false }\n");
	HSSend (handle, "  return true\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function IDCheck(Ot) {\n");
	HSSend (handle, "  var check1 = 0\n");
	HSSend (handle, "  var n = Ot.value\n");
	HSSend (handle, "  var nn = 0\n");
	HSSend (handle, "  var newID = fjID\n");
	HSSend (handle, "  if (\"\" == n)\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "        alert(\"A blank name is not allowed. Enter a valid name.\")\n");
	HSSend (handle, "        return false\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  if (fjID == n) return true\n");
	HSSend (handle, "\n");
	HSSend (handle, "  if (false == isIDNew(n))\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "    while( true )\n");
	HSSend (handle, "    {\n");
	HSSend (handle, "      nn++\n");
	HSSend (handle, "      newID = fjID + nn\n");
	HSSend (handle, "      if(true == isIDNew(newID))\n");
	HSSend (handle, "      {\n");
	HSSend (handle, "        alert(\"ID \"+n+\" already exists. Please use another one.\")\n");
	HSSend (handle, "        Ot.select();Ot.focus(); return false\n");
	HSSend (handle, "      }\n");
	HSSend (handle, "    }\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  else\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "    for (var i = 0; i < n.length; i++)\n");
	HSSend (handle, "	{\n");
	HSSend (handle, "		var c = n.charAt(i)\n");
	HSSend (handle, "		if (false == ((c >= \"0\") && (c <= \"9\")))\n");
	HSSend (handle, "		{\n");
	HSSend (handle, "			if (false == ((c >= \"A\") && (c <= \"Z\")))\n");
	HSSend (handle, "			{\n");
	HSSend (handle, "				if (false == ((c >= \"a\") && (c <= \"z\")))\n");
	HSSend (handle, "				{\n");
	HSSend (handle, "					check1 = 1;\n");
	HSSend (handle, "					break;\n");
	HSSend (handle, "				}\n");
	HSSend (handle, "			}\n");
	HSSend (handle, "		}\n");
	HSSend (handle, "	}\n");
	HSSend (handle, "	if (check1 == 1)\n");
	HSSend (handle, "	{\n");
	HSSend (handle, "		alert(\"ID \"+n+\" not valid. Please use alphabet characters and numbers only.\")\n");
	HSSend (handle, "		Ot.focus(); return false;\n");
	HSSend (handle, "	}\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  return true\n");
	HSSend (handle, "}");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\fjscript_edit.htm }} */
/* @! End HTML page   */

/* @! Begin of dynamic functions */
/* @! Function: na_EDIT_NOZZLES  {{ */
void na_EDIT_NOZZLES(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhPhy.lChannels));
}										/* @! Function: na_EDIT_NOZZLES  }} */
/* @! End of dynamic functions   */
