/*
 *            Copyright (c) 1998-2000 by NETsilicon Inc.
 *
 *  This software is copyrighted by and is the sole property of
 *  NETsilicon.  All rights, title, ownership, or other interests
 *  in the software remain the property of NETsilicon.  This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of NETsilicon.
 *
 *  NETsilicon, reserves the right to modify this software
 *  without notice.
 *
 *  NETsilicon
 *  411 Waverley Oaks Road                  USA 781.647.1234
 *  Suite 227                               http://www.netsilicon.com
 *  Waltham, MA 02452                       AmericaSales@netsilicon.com
 *
 *************************************************************************
 *
 *     Module Name: bsproot.c
 *         Version: 1.00
 *   Original Date: 08/23/99
 *          Author: Charles Gordon
 *        Language: Ansi C
 * Compile Options:
 * Compile defines:
 *       Libraries:
 *    Link Options:
 *
 *    Entry Points: rootStart()
 *
 * Description.
 * =======================================================================
 * This file contains the rootStart() function.  It contains the startup
 * routines for NET+OS that are called immediately after the ThreadX
 * kernel has loaded.  These routines complete the NET+OS startup by
 * loading the device drivers, the IP stack, and startup the user's
 * application.
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 * LZ   02/16/00    Added "ioctl(fd, SIO_TERM_SEND_CR, TERM_SEND_CR_ON)"
 *                  to attach "CR" at the end of a line.
 * CHG  05/05/00    Added descriptive comment to setupVectorTable().
 *                  Added some comments here and there.
 *                  Renamed restoreVector() to setupVectorTable()
 * LZ   06/06/00    Took out IpStringToLong(), used NAInet_addr() to
 *                  Convert ip string xxx.xxx.xxx.xxx to unsigned long value.
 * jzw  07/27/00    In function netosStartup we currently only call UseDefaultParams
 *                  (passing ot a pointer to defaultParams) if the appconf.h parameter
 * 	                APP_USR_NVRAM is set to 0. This would leave defaultParams set to the
 *                  hardcoded values. Changed this to always read from appconf.h
 *                  (reference SCR 1006)
 * JLR  08/01/00    Function applicationTcpDown now is called on a timer until TCP/IP stack
 *                  is configured.
 * PMJ  09/01/00    Clean up of QA comments (ER 1381) and unused temp storage.
 * PMJ  10/16/00    Included PPP NVRAM support.
 * JLR  11/21/00	Added the call to naHdlcLoad()
 * JFU  11/30/00    Added changes to set up baudrate for stdio port if used.
 * MLC  03/29/01    Added DPO_SOPS_SOFTWARE to skip ip configuration
 * PMJ  10/05/01    Added root password to System Access Database.
 * PMJ  10/10/01    Used the APP_ROOT_PASSWORD definiton to enable the NAsetSysAccess()
 *                  call, which uses APP_ROOT_PASSWORD as a password to the root account.
 * PMJ  03/20/02    Moved the call to NAsetSysAccess() prior to dialog.  Required to
 *                  password protect the system dialog.
 * PMJ  03/22/02    Included APP_ADMINISTRATORS_ACCOUNT functionality.
 * PMJ  03/28/02    Converted NASecGetSystemFixedKey() return value to a static memory
 *                  location.
 * PMJ  04/09/02    Included Kerberos parameters and SNTP Servers into UseDefaultParams().
 * ESUN 04/23/01    Added getAppFileSystemSize() to support user configure upload file
 *                  number through appconfig.h file.
 * SW   04/30/02    Added GNU support for redirecting std I/O
 * SW   05/22/02    Added GNU support for C++
 * PMJ  07/15/02    Added initialization of params structure in UseDefaultParams().  Removed
 *                  unnecessary UseDefaultParams() prototype.
 *
 */

#include <stdio.h>
#include <string.h>
#include <tx_api.h>
#include <starttcp.h>
#include <appconf.h>
#include <bspconf.h>
#include <netoserr.h>
#include <bsptimer.h>
#include <ddi.h>
#include <narmsrln.h>
#include <netosIo.h>
#include <narmbrd.h>
#include <hdlcapi.h>
#include <sysAccess.h>
#include <narmapi.h>
#include "dialog.h"
#include <NASecurity.h>
#ifdef NETOS_GNU_TOOLS
#include <unistd.h>
extern void   _EXFUN(__sinit,(struct _reent *));
#define CHECK_INIT(fp) \
	do \
	{ \
		if ((fp)->_data == 0) \
			(fp)->_data = _REENT; \
			if (!(fp)->_data->__sdidinit) \
				__sinit ((fp)->_data); \
	} \
	while (0)
#endif

typedef void (*userErrorHandlerType)(char *, int, int);
extern devBoardParamsType defaultParams;
extern void applicationStart (void);	/* declaration for user's entry point*/
extern void applicationTcpDown (void);	/* declaration for user's TCP wait loop function*/

TX_THREAD rootThread;					/* thread control block for root thread*/
TX_TIMER  tcp_down_timer;				/* timer to call TCP down function at startup */

UINT    bsp_default_api_priority = APP_DEFAULT_API_PRIORITY;
int     bsp_kernel_started = 0;			/* set after ThreadX has started*/

int ip_configured = IP_STATE_NONE;

static void setupVectorTable(void);
										/* called on 1 tick timer until TCP/IP stack inits */
static void tcp_down_function(ULONG arg);

/*
 *
 *  Function: void setupVectorTable(void)
 *
 *  Description:
 *
 *      This routine sets up the standard vector table.  When an exception occurs
 *      the ARM processor switches modes and changes the PC so that the processor
 *      executes the instruction at the exception vector.  For example, a prefetch
 *      abort will cause the processor to switch to abort mode and change the PC
 *      to 0xc.  Note that this is different from most other processors that load
 *      an address from the vector table.  The ARM *EXECUTES* the word in the
 *      exception table.  Normally the instruction is a jump to the exception
 *      handler.
 *
 *      In NET+OS, we implement the vector table by placing an instruction at each
 *      table entry that causes the processor to load the PC with the contents of the
 *      word at PC+0x20.  This implements a 32-bit jump.  So, the NET+OS vector
 *      table looks like this.
 *
 *      Note that if you wanted to be really tricky, you could move the second half
 *      of the vector table to create some space after address 0x1c.  This would
 *      allow you to implement a very fast FIQ handler that did not have to start
 *      with a jump.  We don't use FIQ in NET+OS, so we didn't bother with this.
 *
 *
 *           Address     Exception                   Contents
 *           ========    ========================    ===================================
 *           0x00        Reset                       LD PC,PC+0x20
 *           0x04        Undefined Instruction       LD PC,PC+0x20
 *           0x08        Software Interrupt          LD PC,PC+0x20
 *           0x0C        Prefetch Abort              LD PC,PC+0x20
 *           0x10        Data Abort                  LD PC,PC+0x20
 *           0x14        not used                    LD PC,PC+0x20
 *           0x18        IRQ                         LD PC,PC+0x20
 *           0x1c        FIQ                         LD PC,PC+0x20
 *
 *           0x20        Reset                       Address of Reset handler
 *           0x24        Undefined Instruction       Address of UNDEF handler
 *           0x28        Software Interrupt          Address of SWI handler
 *           0x2C        Prefetch Abort              Address of prefetch abort handler
 *           0x30        Data Abort                  Address of data abort handler
 *           0x34        not used
 *           0x38        IRQ                         Address of IRQ handler
 *           0x3c        FIQ                         Address of FIQ handler
 *
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */
static void setupVectorTable(void)
{
	/*
	!! Don't change any thing in reset.s, tx_ill.s, and setupVectorTable() !!
	*/
	extern unsigned long  NA_MAIN,UNDEFINED,SWI,PREFETCH,ABORT,RESERVED,NAIRQ,FIQ;

	*(unsigned long*)(0x0)= 0xe59ff018;	/* 0xe59ff018 is ld pc,pc+0x20*/
	*(unsigned long*)(0x4)= 0xe59ff018;
	*(unsigned long*)(0x8)= 0xe59ff018;
	*(unsigned long*)(0xc)= 0xe59ff018;
	*(unsigned long*)(0x10)= 0xe59ff018;
	*(unsigned long*)(0x14)= 0xe59ff018;
	*(unsigned long*)(0x18)= 0xe59ff018;
	*(unsigned long*)(0x1c)= 0xe59ff018;

	*(unsigned long*)(0x20)= NA_MAIN;
	*(unsigned long*)(0x24)= UNDEFINED;
	*(unsigned long*)(0x28)= SWI;
	*(unsigned long*)(0x2c)= PREFETCH;
	*(unsigned long*)(0x30)= ABORT;
	*(unsigned long*)(0x34)= RESERVED;
	*(unsigned long*)(0x3c)= FIQ;
}
/*
 *
 *  Function: int checkAppUseNvram(void)
 *
 *  Description:
 *
 *      This routine return the setting of the APP_USE_NVRAM value defined in APPCONF.h
 *
 *  Parameters:
 *
 *      None
 *
 *  Return Values:
 *
 *	     Value of APP_USE_NVRAM
 *
 */
int checkAppUseNvram(void)
{
	if (APP_USE_NVRAM)
		return 1;
	else
		return 0;
}
/*
 *
 *  Function: size_t getAppNetHeapSize(void)
 *
 *  Description:
 *
 *      This routine return the setting of the APP_NET_HEAP_SIZE value defined in APPCONF.h
 *
 *  Parameters:
 *
 *      None
 *
 *  Return Values:
 *
 *	     Value of APP_NET_HEAP_SIZE
 *
 */
int getAppNetHeapSize(void)
{
	return APP_NET_HEAP_SIZE;
}
char *NASecGetSystemFixedKey (void)
{
#ifdef APP_FIXED_GATEWAY_KEY
	static char Key[NASYSACC_STRLEN_PASSWORD];
	devBoardParamsType params;

	NAReadDevBoardParams (&params);

	strcpy (Key, params.FixedKey);

	if (Key[0] == 0)
	{
		return APP_FIXED_GATEWAY_KEY;
	}
	else
	{
		return Key;
	}
#else
	return NULL;
#endif
}
char *NAGetSntpPrimaryServer (void)
{
#ifdef APP_USE_NETWORK_TIME_PROTOCOL
	static char ipAddr[16];
	devBoardParamsType params;

	NAReadDevBoardParams (&params);

	if (params.ipAddrSntp1 != 0)
	{
		NAInet_toa (params.ipAddrSntp1, ipAddr);
		return ipAddr;
	}
#endif
	return "0.0.0.0";
}
char *NAGetSntpSecondaryServer (void)
{
#ifdef APP_USE_NETWORK_TIME_PROTOCOL
	static char ipAddr[16];
	devBoardParamsType params;

	NAReadDevBoardParams (&params);

	if (params.ipAddrSntp2 != 0)
	{
		NAInet_toa (params.ipAddrSntp2, ipAddr);
		return ipAddr;
	}
#endif
	return "0.0.0.0";
}
#ifdef APP_USE_KERBEROS

int NAKrb5GetMaxPrincipals(void)
{
	return NAKRB5_MAX_PRINCIPALS;
}
int NAKrb5GetMaxApRequests(void)
{
	return NAKRB5_MAX_AP_REQUESTS;
}
int NAKrb5GetConfigParameters (struct NAKrb5Params *pAppStruct)
{
	devBoardParamsType params;
	int result;

	result = NAReadDevBoardParams (&params);
	if (result != 0)
	{
		return NASEC_SYSTEM_FAILURE;
	}

	if (pAppStruct != NULL)
	{
		memset (pAppStruct, 0, sizeof(struct NAKrb5Params));
		if ((strlen(params.krbUsername) < NASEC_MAX_KRB_USERNAME_LENGTH) &&
			(strlen(params.krbPassword) < NASEC_MAX_KRB_PASSWORD_LENGTH) &&
			(strlen(params.krbRealmname) < NASEC_MAX_KRB_REALM_NAME_LENGTH) &&
			(params.krbIpAddr != 0))
		{
			strcpy (pAppStruct->username, params.krbUsername);
			strcpy (pAppStruct->password, params.krbPassword);
			strcpy (pAppStruct->realm, params.krbRealmname);
			NAInet_toa (params.krbIpAddr, &pAppStruct->KDCIpAddr[0]);
			return 0;
		}
	}

	return NASEC_INVALID_ARGUMENT;
}
#endif

/*
 *
 *  Function: int getAppFileSystemSize(void)
 *
 *  Description:
 *
 *      This routine return the setting of the APP_FILE_SYSTEM_SIZE value defined in APPCONF.h
 *
 *  Parameters:
 *
 *      None
 *
 *  Return Values:
 *
 *	     Value of APP_FILE_SYSTEM_SIZE
 *
 */

int getAppFileSystemSize(void)
{
	return APP_FILE_SYSTEM_SIZE;
}
/*
 *
 *  Function: int UseDefaultParams(devBoardParamsType *params)
 *
 *  Description:
 *
 *      This routine changes the params with the setting in the appconf.h
 *
 *  Parameters:
 *
 *      params      pointer to structure with development board parameters
 *
 *  Return Values:
 *
 *		 0       success
 *      -1      unable to read parameters
 */

int UseDefaultParams(devBoardParamsType *params)
{
	unsigned long app_ip_address_val, app_ip_subnet_mask_val, app_ip_gateway_val;

	if (params == NULL)
	{
		return -1;
	}

	/*  default the structure to zeros  */
	memset (params, 0, sizeof(devBoardParamsType));

	/* calculate app_ip_address_val value */
	app_ip_address_val = NAInet_addr(APP_IP_ADDRESS);

	/* calculate app_ip_subnet_mask_val value */
	app_ip_subnet_mask_val = NAInet_addr(APP_IP_SUBNET_MASK);

	/* calculate app_ip_subnet_mask_val value */
	app_ip_gateway_val = NAInet_addr(APP_IP_GATEWAY);

	params->ipAddress = app_ip_address_val;
	params->subnetMask = app_ip_subnet_mask_val;
	params->gateway = app_ip_gateway_val;
	params->useDhcp = APP_IP_USE_DHCP;
	params->waitTime = APP_BSP_DELAY;
	params->baudrate = APP_BSP_BAUD_RATE;
	memcpy(params->serialNumber,APP_BSP_SERIAL_NUM,sizeof(params->serialNumber));

#ifndef APP_ENABLE_PPP1
#define APP_ENABLE_PPP1         0
#ifndef APP_PPP1_ADDRESS
#define APP_PPP1_ADDRESS        0
#endif
#ifndef APP_PPP1_PEER_ADDRESS
#define APP_PPP1_PEER_ADDRESS   0
#endif
#endif
	if (APP_ENABLE_PPP1)
	{
		/*  set the default ppp ip address  */
		app_ip_address_val      = NAInet_addr(APP_PPP1_ADDRESS);
		params->ipAddrPpp1      = app_ip_address_val;

		/*  set the default ppp peer ip address  */
		app_ip_address_val      = NAInet_addr(APP_PPP1_PEER_ADDRESS);
		params->ipAddrPpp1peer  = app_ip_address_val;
	}
	else
	{
		params->ipAddrPpp1      = app_ip_address_val;
		params->ipAddrPpp1peer  = 0;
	}

#ifndef APP_ENABLE_PPP2
#define APP_ENABLE_PPP2         0
#ifndef APP_PPP2_ADDRESS
#define APP_PPP2_ADDRESS        0
#endif
#ifndef APP_PPP2_PEER_ADDRESS
#define APP_PPP2_PEER_ADDRESS   0
#endif
#endif
	if (APP_ENABLE_PPP2)
	{
		/*  set the default ppp ip address  */
		app_ip_address_val      = NAInet_addr(APP_PPP2_ADDRESS);
		params->ipAddrPpp2      = app_ip_address_val;

		/*  set the default ppp peer ip address  */
		app_ip_address_val      = NAInet_addr(APP_PPP2_PEER_ADDRESS);
		params->ipAddrPpp2peer  = app_ip_address_val;
	}
	else
	{
		params->ipAddrPpp2      = 0;
		params->ipAddrPpp2peer  = 0;
	}

#ifdef APP_USE_NETWORK_TIME_PROTOCOL
#ifdef APP_SNTP_PRIMARY
	params->ipAddrSntp1 = (unsigned long)NAInet_addr(APP_SNTP_PRIMARY);
#endif
#ifdef APP_SNTP_SECONDARY
	params->ipAddrSntp2 = (unsigned long)NAInet_addr(APP_SNTP_SECONDARY);
#endif
#endif

#ifdef APP_ROOT_PASSWORD
	memcpy(params->rootPassword, APP_ROOT_PASSWORD, NASYSACC_STRLEN_PASSWORD);
	params->rootPassword[NASYSACC_STRLEN_PASSWORD - 1] = 0;
#endif

#ifdef APP_ADMINISTRATORS_ACCOUNT
	memcpy(params->adminPassword, APP_ADMINISTRATORS_ACCOUNT, NASYSACC_STRLEN_PASSWORD);
	params->adminPassword[NASYSACC_STRLEN_PASSWORD - 1] = 0;
#endif

#ifdef APP_USE_KERBEROS
#ifdef APP_KERBEROS_KDC
	params->krbIpAddr   = (unsigned long)NAInet_addr(APP_KERBEROS_KDC);
#else
	params->krbIpAddr   = 0;
#endif
#ifdef APP_KERBEROS_USERNAME
	memcpy(params->krbUsername, APP_KERBEROS_USERNAME, NASEC_MAX_KRB_USERNAME_LENGTH);
	params->krbUsername[NASEC_MAX_KRB_USERNAME_LENGTH - 1] = 0;
#endif
#ifdef APP_KERBEROS_PASSWORD
	memcpy(params->krbPassword, APP_KERBEROS_PASSWORD, NASEC_MAX_KRB_PASSWORD_LENGTH);
	params->krbPassword[NASEC_MAX_KRB_PASSWORD_LENGTH - 1] = 0;
#endif
#ifdef APP_KERBEROS_REALMNAME
	memcpy(params->krbRealmname, APP_KERBEROS_REALMNAME, NASEC_MAX_KRB_REALM_NAME_LENGTH);
	params->krbRealmname[NASEC_MAX_KRB_REALM_NAME_LENGTH - 1] = 0;
#endif
#endif
	return 0;
}
/*
 *
 *  Function: void netosBreakpoint (char *reason)
 *
 *  Description:
 *
 *      This routine is called by netosFatalError().  It is used to make it
 *      easy to set breakpoints.
 *
 *  Parameters:
 *
 *      reason      a string that describes the problem
 *
 *  Return Values:
 *
 *      none
 *
 */

void netosBreakpoint (char *reason)

{
	if (reason)
	{
		printf ("%s\n", reason);
	}
}
/*
 *
 *  Function: void netosFatalError (char *description, int redBlinks, int greenBlinks)
 *
 *  Description:
 *
 *      This routine is called when a nonrecoverable error occurs.  If a user defined
 *      error handler has been defined, then it is called.  Otherwise, it blinks the
 *      LEDs in a pattern that indicates the error type.
 *
 *      A user error handler is defined by setting APP_ERROR_HANDLER in app_conf.h to
 *      the address of an error handling routine.  If no error handler is present, this
 *      APP_ERROR_HANDLER should be set to 0.
 *
 *      This error handler blinks the LEDs.  An alternative for a production device
 *      might be to call NAReset() to restart the system.
 *
 *  Parameters:
 *
 *      description     pointer to a string that contains a description of the problem
 *      redBlinks       # of times to blink the red/yellow LED
 *      greeBlinks      # of times to blink the gree LED
 *
 *  Return Values:
 *
 *      none
 *
 */

void netosFatalError (char *description, int redBlinks, int greenBlinks)

{
	userErrorHandlerType userErrorHandler = (userErrorHandlerType) APP_ERROR_HANDLER;

	if (userErrorHandler)				/* did user define their own error handler*/
	{									/* if so, call it*/
		userErrorHandler (description, redBlinks, greenBlinks);
	}
	else								/* if no user error handler*/
	{
		netosBreakpoint (description);	/* try to hit breakpoint in debugger*/

		while (1)						/* if all else fails, blink the LED*/
		{
			NALedBlinkRed (redBlinks, 1);
			NALedBlinkGreen (greenBlinks, 1);
			tx_thread_sleep (BSP_TICKS_PER_SECOND);
		}
	}
}
/*
 *
 *  Function: netosConfigStdio(none)
 *
 *  Description:
 *
 *      This routine sets up stdin, stdout, and stderr to be redirected
 *      out the APP_STDIO_PORT if APP_STDIO_PORT is defined.  Typically,
 *      APP_STDIO_PORT is set to either "/com/0" or "/com/1".
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */
#define SIO_TERM_SEND_CR  0x100B
#define TERM_SEND_CR_ON   1
void netosConfigStdio (void)
{
#if defined(APP_STDIO_PORT)
	int fd, rc;
	devBoardParamsType nvParams;
	int baudrate = 9600;
	int SIO_BAUD_SET   =  0x1003;
	/*
	 *  NAReadDevBoardParams() reads the contents of the section of NVRAM that contains
	 *  the development board's startup parameters if APP_USE_NVRAM flag is set to a nonzero value.
	 *  If APP_USE_NVRAM is 0, NAReadDevBoardParams() reads the default parameters as defined
	 *  in appconfig.h. NAReadDevBoardParams() and default data struct are in src\narmcore\narmsrln.c.
	 */
	NAReadDevBoardParams (&nvParams);

	/* if we have bad baudrate by any chance, then set it to default 9600
	   to get things going */
	baudrate = nvParams.baudrate;
	if ((baudrate != 1200) && (baudrate != 2400) && (baudrate != 4800) && (baudrate != 9600)
		&& (baudrate != 19200) && (baudrate != 38400) && (baudrate != 57600) && (baudrate != 115200))
	{
		nvParams.baudrate = defaultParams.baudrate;
		baudrate = defaultParams.baudrate ;

	}
	/* open device */
	fd = open (APP_STDIO_PORT, O_RDWR);
	if (fd <0)
	{
		netosFatalError("APP_STDIO_PORT improperly defined", 1, 1);
		return;
	}
	/*
	 * This ioctl tells the serial driver to set the baud rate
	 * from the setting in defaultParams.baudrate
	 */

	rc=ioctl(fd, SIO_BAUD_SET , &baudrate);
	if (rc <0)
	{
		netosFatalError("APP_STDIO_PORT can't set option", 1, 1);
		return;
	}

	/*
	 * This ioctl tells the serial driver to add a carriage return
	 * whenever it transmits a line feed character.
	 */
	rc=ioctl(fd, SIO_TERM_SEND_CR, TERM_SEND_CR_ON);
	if (rc <0)
	{
		netosFatalError("APP_STDIO_PORT can't set option", 1, 1);
		return;
	}

	/*
	 * Now redirect I/O out the selected port.
	 */
#ifndef NETOS_GNU_TOOLS
	stdin->io_channel = fd;
	stdout->io_channel = fd;
	stderr->io_channel = fd;
#else
	CHECK_INIT(stdin);
	CHECK_INIT(stdout);
	CHECK_INIT(stderr);

	/* need to over the following so that
	   std I/O will call into DDI */
	stdout->_cookie = (_PTR)fd;
	stdin->_cookie = (_PTR)fd;
	stderr->_cookie = (_PTR)fd;
	stdin->_read = (void*)read;
	stdout->_write = (void*)write;
	stderr->_write = (void*)write;
#endif
#endif
}
/*
 *
 *  Function: void DDIFirstLevelInitialize(void)
 *
 *  Description:
 *
 *      This routine is called just after a reset has been completed
 *      and the ASIC hardware is initialized to call the device
 *      driver initialization routines.  No kernel services will be
 *      available at this point.  The only thing the device drivers
 *      normally do is to initialize some hardware (if necessary).
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */

void DDIFirstLevelInitialize(void)
{
	int i;

	NAT2Init();							/* setup AUX clock used by some drivers*/

	for (i=0; i < MAX_DEV; i++)			/* walk through the device table*/
	{
		if (deviceTable[i].enter)		/* if an enter routine is defined*/
		{								/* call it*/
			(*deviceTable[i].enter)(deviceTable[i].channel);
		}
	}
}
/*
 *
 *  Function: void DDISecondLevelInitialize(void)
 *
 *  Description:
 *
 *      This routine is called after the kernel scheduler has loaded
 *      to call each device driver's initialization routines.  At this
 *      point all kernel services (except other device drivers) are
 *      available.
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */

void DDISecondLevelInitialize(void)
{
	int i;
	for (i=0; i < MAX_DEV; i++)			/* walk through the device table*/
	{
		if (deviceTable[i].init)		/* if device has init routine*/
		{								/* call it*/
			(*deviceTable[i].init)(deviceTable[i].channel);
		}
	}
}
/*
 *
 *  Function: void netosStartup (ULONG not_used)
 *
 *  Description:
 *
 *      This is the entry point for the root thread.  This thread loads the device
 *      drivers and starts the IP stack.  Then it starts the user's application.
 *
 *  Parameters:
 *
 *      not_used        an unused parameter
 *
 *  Return Values:
 *
 *      none
 *
 */

void netosStartup (ULONG not_used)

{
	devBoardParamsType nvParams;

	/* if the APP_USE_NVRAM flag is set to 0, default from the file is used. */

	/*if (!APP_USE_NVRAM)*/
	/* changed 07/27/00. defaultParams will always be loaded with the contents of
	 * appconf.h
	 */
	UseDefaultParams(&defaultParams);
	/*
	 * Load device drivers.
	 */
#ifndef APP_ENABLE_HDLC
#define APP_ENABLE_HDLC         0
#endif

#if APP_ENABLE_HDLC

	naHdlcLoad(NAHDLC_LARGE_FRAME_SIZE,NAHDLC_SMALL_FRAME_SIZE,NAHDLC_NUM_LARGE_FRAMES,
		NAHDLC_NUM_SMALL_FRAMES,NAHDLC_NUM_EMPTY_FRAMES);
#endif

	DDISecondLevelInitialize();			/* load each device driver*/
	bsp_kernel_started = 1;				/* basic kernel is up now*/

#ifndef FOXJET
	/*
	 * Create TCP down timer.
	 */
	tx_timer_create(&tcp_down_timer,"tcp down timer",tcp_down_function,0,1,1,TX_AUTO_ACTIVATE);
#endif

#ifdef APP_ROOT_PASSWORD
	NAReadDevBoardParams (&nvParams);
	if (nvParams.rootPassword[0] == 0)
	{
		/*  Use the default root password defined in APP_ROOT_PASSWORD  */
		NAsetSysAccess (NASYSACC_ADD, "root", APP_ROOT_PASSWORD, NASYSACC_LEVEL_ROOT, 0);
	}
	else
	{
		/*  Use the root password from NVRAM    */
		NAsetSysAccess (NASYSACC_ADD, "root", nvParams.rootPassword, NASYSACC_LEVEL_ROOT, 0);
	}
#endif

	/*
	 * Prompt user with dialog to set configuration parameters.
	 */
	NADialog();
	NAReadDevBoardParams (&nvParams);	/* read new settings from NVRAM*/

	netosConfigStdio();					/* redirect stdio if so configured*/
#ifdef FOXJET
	/*
	 * Open serial port now.
	 */
	fj_openSerialPort( NULL );
	fj_writeInfo("Serial port open\n");	// FOXJET
#endif

	/*
	 * Load the TCP/IP stack.
	 */
	netosStartTCP();					/* start TCP/IP stack */

#ifdef FOXJET
	/*
	 * Create TCP down timer.
	 */
	tx_timer_create(&tcp_down_timer,"tcp down timer",tcp_down_function,0,1,1,TX_AUTO_ACTIVATE);
#endif

#ifndef DPO_SOPS_SOFTWARE
										/* wait until stack has IP parameters*/
	while (ip_configured != IP_STATE_CONFIGURED)
	{
		tx_thread_sleep (1);			/* let other tasks run*/
	}
	/*
	 * Delete TCP down timer.
	 */
	tx_timer_delete(&tcp_down_timer);
#endif

	/*
	 * Create the PPP related semaphores
	 */
	netosCreatePPPSemaphores();

#ifdef APP_ADMINISTRATORS_ACCOUNT
	if (nvParams.adminPassword[0] != 0)
	{
		/*  Use the administrators password from NVRAM    */
		NAsetSysAccess (NASYSACC_ADD, APP_ADMINISTRATORS_ACCOUNT, nvParams.adminPassword, NASYSACC_LEVEL_GATEWAY, 0);
	}
#endif

	/*
	 * Start the user's application.
	 */
	applicationStart();
}
/*
 *
 *  Function: void tcp_down_function(ULONG arg)
 *
 *  Description:
 *
 *		This function is called on a 1 tick timer.
 *		It calls user function applicationTcpDown() while TCP /IP stack
 *		is not fully configured.
 *
 *  Parameters:
 *
 *      not used
 *
 *  Return Values:
 *
 *      none
 *
 */
static void tcp_down_function(ULONG arg)
{
	if (ip_configured != IP_STATE_CONFIGURED)
		applicationTcpDown ();
}
/*
 *
 *  Function: void tx_application_define (void *first_unused_memory)
 *
 *  Description:
 *
 *      ThreadX is hard coded to call this function after it has initialized.
 *      This function is called just before the ThreadX scheduler starts.  It
 *      creates the root thread that is responsible for starting NET+OS and
 *      the IP stack.
 *
 *  Parameters:
 *
 *      first_unused_memory     pointer to first piece of unused memory
 *
 *  Return Values:
 *
 *      none
 *
 */

void tx_application_define (void *first_unused_memory)

{
	int ccode;

	/*
	 * Start the system and aux clocks.
	 */
	netosStartClocks();

	/*
	 * Now create the root thread.  This thread starts NET+OS and the TCP/IP stack.
	 */
										/* control block for root thread*/
	ccode = tx_thread_create (&rootThread,
		"Root Thread",					/* thread name*/
		netosStartup,					/* entry function*/
		0,								/* parameter*/
		first_unused_memory,			/* start of stack*/
		APP_ROOT_STACK_SIZE,			/* size of stack*/
		APP_ROOT_PRIORITY,				/* priority*/
		APP_ROOT_PRIORITY,				/* preemption threshold */
		1,								/* time slice threshold*/
		TX_AUTO_START);					/* start immediately*/

	if (ccode != TX_SUCCESS)			/* if couldn't create thread*/
	{
		netosFatalError ("Unable to create root thread", 1, 1);
	}
}
/*
 *
 *  Function: int main (void)
 *
 *  Description:
 *
 *      This routine is called by the C Library after the basic initialization of
 *      the hardware and the C Library has been done.  The kernel still has not
 *      been loaded yet.
 *
 *      We call NABoardInit() to finish initializing the ASIC and development board.
 *      DDIFirstLevelInitialize() is called to initialize any special hardware
 *      associated with a device driver.
 *
 *      tx_kernel_enter() starts the ThreadX kernel.
 *
 *  Parameters:
 *
 *      none
 *
 *  Return Values:
 *
 *      none
 *
 */
int main()

{
	setupVectorTable();					/* setup standard vector table*/

	NABoardInit ();						/* initialize the hardware */
	DDIFirstLevelInitialize();			/* let device drivers init their hardware*/

#ifndef APP_CPP
#define APP_CPP     0
#endif

#if APP_CPP
#ifndef NETOS_GNU_TOOLS
	_main();							/* Call C++ static constructors */
#endif
#endif

	tx_kernel_enter();					/* Enter the ThreadX kernel.  */
}
#ifndef ENABLE_FLASH_COMPRESSION
void decompress(void)
{
}
#endif
#ifdef ENABLE_FLASH_COMPRESSION
const char ram_buffer_0[]= {0};
#endif
