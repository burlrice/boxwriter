//#include <WinDef.h>
#include "fj_base.h"
#include <string.h>
#include "DllVer.h"

using namespace FoxjetCommon;

extern "C" const char * fj_getVersionNumber(void);

void CALLBACK fj_GetDllVersion (LPVERSIONSTRUCT lpVer)
{
//	TCHAR sz [] = _T ("0.0000");
	CHAR sz [] = "0.0000";
	
//	_tcsncpy (sz, fj_getVersionNumber (), sizeof (sz));
	strncpy (sz, fj_getVersionNumber (), sizeof (sz));

	if (lpVer) {
		lpVer->m_nMajor			= sz [0] - '0';
		lpVer->m_nMinor			= (sz [2] - '0') * 10 + sz [3] - '0';
		lpVer->m_nCustomMajor	= 0;
		lpVer->m_nCustomMinor	= 0;
		lpVer->m_nInternal		= (sz [4] - '0') * 10 + sz [5] - '0'; 
	}
}
