#include "fj_netsilicon.h"
#include <narmled.h>
#include <na_isr.h>
#include <narmsrln.h>					// for NETOS ROM parameters
#include "fj.h"
#include "fj_misc.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_text.h"
#include "fj_bitmap.h"
#include "barcode.h"
#include "fj_barcode.h"
#include "fj_counter.h"
#include "fj_datetime.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_label.h"
#include "fj_printhead.h"
#include "fj_system.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include <stddef.h>

extern const CHAR fj_LabelID[];
extern       BOOL fj_writeROMMsg;		// if true, always reset ROM messages on boot up

//
// initialize the A/D values and tables
//
//
VOID fj_print_head_InitAD( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	LPHEATER pHeater;
	int  i, j;

	pHeater = &(pGlobal->heater1);
	pHeater->pNameJava = "Heater1";		// name
										// name
	pHeater->pNameText = "Temperature Sensor 1";
	pHeater->bHeaterPresent = FALSE;	// FALSE until heater sensed
	pHeater->lTempSent = 0;				// last temperature reported

	pHeater->lTempOnTime = 0;			// time of heater on
	pHeater->bHeaterOn = FALSE;			// not on
	pHeater->bReady = FALSE;			// not ready
	pHeater->bColdStart = FALSE;		// not ready
	pHeater->bColdStartDelay = FALSE;	// not ready
	pHeater->lHeaterTime = 0;
	for ( i = 0; i < HEATER_CYCLES; i++ )
	{
		pHeater->lHeaterOn[i] = 0;
		pHeater->lHeaterOff[i] = 0;
	}

	pGlobal->lChannel = 0;				// first A/D channel
	// clear A/D values
	for ( i = 0; i < 8; i++ )
	{
		for ( j = 0; j < FJ_AD_AVERAGE; j++ )
		{
			pGlobal->wADRaw [i][j] = 0;
			pGlobal->wADRaw2[i][j] = 0;
		}
		pGlobal->lADCount [i] = 0;
		pGlobal->lADCount2[i] = 0;
	}

	return;
}
byte bmp0[] =							// windows bitmap - FoxJet logo
{
	0x42, 0x4D, 0xBE, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00,
	0x00, 0x00, 0x5C, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x1F, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x07, 0xFF, 0xFF, 0xF8, 0x3F, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0x0F, 0xFF, 0x80, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0x87, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x02,
	0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0x87, 0xFF, 0xFF, 0xC0, 0x00, 0x00, 0x00,
	0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0x07, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
	0x00, 0x0F, 0xFF, 0xFF, 0xFC, 0x00, 0x3F, 0xFF, 0xFF, 0x00, 0x00, 0x01,
	0x00, 0x00, 0x7F, 0xFF, 0x80, 0x00, 0x01, 0xFF, 0xFF, 0xC0, 0x00, 0x03,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xF0, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x0F, 0xFF, 0xFC, 0x00, 0x01,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x07, 0xFF, 0xFE, 0x00, 0x01,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x03, 0xFF, 0xFF, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x80, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x00, 0x09,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x03, 0xFF, 0xFF, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x07, 0xFF, 0xFE, 0x00, 0x0F,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x0F, 0xFF, 0xFC, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0x7F, 0xFF, 0xF0, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x00, 0x0F, 0xFF, 0xFF, 0xC0, 0x00, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0x80, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x01,
	0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x03,
	0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00,
	0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x01,
	0x00, 0x01, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0xFF, 0xFF, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x09,
	0x00, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x01,
	0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x01
};

//
//  FUNCTION: fj_print_head_LoadMsgs()
//
//  PURPOSE:  Initialize messages in MemStor
//
VOID fj_print_head_LoadMsgs( LPFJPRINTHEAD pfph )
{
	LPFJBARCODE  pfb;
	LPFJTEXT     pft;
	LPFJBITMAP   pfbm;
	LPFJCOUNTER  pfc;
	LPFJDATETIME pfdt;
	LPFJELEMENT  pfe;
	LPFJMESSAGE  pfm;
	LPFJLABEL    pfl;
	LPSTR       *papmsg;

	fj_bmpAddToMemstor( pfph->pMemStorEdit, bmp0, "FJlogo" );

	pfl = fj_LabelNew();
	pfl->fBoxHeight = 11.0;
	pfl->fBoxWidth  = 16.0;
	strcpy( pfl->strName, "Ketchup" );
	pfm = fj_MessageNew();
	pfm->pfph = pfph;
	pfm->pfl  = pfl;
	strcpy( pfm->strName, "Ketchup" );
	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText,     "FoxJet IP Printer" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = 1;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "fx32x32" );
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "MSG11" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = 1;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 3;
	strcpy( pft->strFontName, "fx16x12" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 1;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 1.0;			// distance from left edge of box
										// type of positioning
	pfe->lPosition    = FJ_POSITION_DISTANCE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 10.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "MSG22" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = 1;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 3;
	strcpy( pft->strFontName, "fx16x12" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 2;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 2.0;			// distance from left edge of box
										// type of positioning
	pfe->lPosition    = FJ_POSITION_DISTANCE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 13.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

										// this message name into label
	strcpy( pfl->fmMessageIDs[0], pfm->strName );
										// add to MemStor
	fj_LabelAddToMemstor( pfph->pMemStorEdit, pfl );
										// add to MemStor
	fj_MessageAddToMemstor( pfph->pMemStorEdit, pfm );
	fj_MessageDestroy( pfm );
	fj_LabelDestroy( pfl );
	pfl = NULL;

	return;

}
//
//  FUNCTION: fj_print_head_LoadTestMsgs()
//
//  PURPOSE:  Initialize messages in MemStor
//
VOID fj_print_head_LoadTestMsgs( LPFJPRINTHEAD pfph )
{
	LPFJBARCODE  pfb;
	LPFJTEXT     pft;
	LPFJBITMAP   pfbm;
	LPFJCOUNTER  pfc;
	LPFJDATETIME pfdt;
	LPFJELEMENT  pfe;
	LPFJMESSAGE  pfm;
	LPFJLABEL    pfl;
	LPSTR       *papmsg;
	LONG         lBold = 1;

	pfl = fj_LabelNew();
	strcpy( pfl->strName, "73047" );
	pfl->fBoxHeight = 14.0;
	pfl->fBoxWidth  = 24.0;
	pfl->lExpValue  = 7;				// Datetime - expiration value
	pfl->lExpUnits  = FJ_LABEL_EXP_DAYS;// Datetime - expiration units
	pfl->bExpRoundUp  = TRUE;				// Datetime - expiration should be round up to top of unit
	pfl->bExpRoundUp  = FALSE;
	pfm = fj_MessageNew();
	pfm->pfph = pfph;
	pfm->pfl  = pfl;
	strcpy( pfm->strName, "73047" );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText,     "73047 " );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "fx32x32" );
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.3;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "MINI CHEDDARS" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "fx16x12" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "McVITTIES CHEESE " );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "fx16x12" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_PARALLEL;
	pfe->lRow          = 16;			// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "12 X 14 PACKS" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "barcode" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 24;			// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "5.9 KG GROSS" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "barcode" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_PARALLEL;
	pfe->lRow          = 16;			// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "STORE COOL & DRY " );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "barcode" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_PARALLEL;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementBarCodeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfb = (LPFJBARCODE)pfe->pDesc;
										// Set up barcode text
	strcpy (pfb->strText, "0500016810107 ");
	pfb->lHRBold     = 3;
	pfb->lSysBCIndex = 0;				// Set up barcode type
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow        = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft       = 0.0;				// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementDatetimeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfdt = (LPFJDATETIME)pfe->pDesc;
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pfdt->lBoldValue    = lBold;
	pfdt->lWidthValue   = 1;
	pfdt->lGapValue     = 0;
										// format of the date/time
	pfdt->lFormat       = DT_TIME_HHMM24;
										// which date/time
	pfdt->lTimeType     = DT_TIME_NORMAL;
	pfdt->lLength       = 7;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign        = 'L';			// alignment of value in larger length - L/R
	pfdt->cFill         = ' ';			// fill character. 0 is no fill.
	pfdt->cDelim        = ':';			// delimiter to use between date/time parts
	pfdt->bStringNormal = TRUE;			// which set of strings to use for alphabetic formats
	pfdt->lHourACount   = 24;			// number of time periods in 24 hours
	strcpy( pfdt->strFontName, "fx16x12" );
										// type of positioning
	pfe->lPosition     = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementDatetimeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfdt = (LPFJDATETIME)pfe->pDesc;
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pfdt->lBoldValue    = lBold;
	pfdt->lWidthValue   = 1;
	pfdt->lGapValue     = 0;
	pfdt->lFormat       = DT_DATE_YY;	// format of the date/time
										// which date/time
	pfdt->lTimeType     = DT_TIME_EXPIRATION;
	pfdt->lLength       = 2;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign        = 'R';			// alignment of value in larger length - L/R
	pfdt->cFill         = '0';			// fill character. 0 is no fill.
	pfdt->cDelim        = ':';			// delimiter to use between date/time parts
	pfdt->bStringNormal = TRUE;			// which set of strings to use for alphabetic formats
	pfdt->lHourACount   = 24;			// number of time periods in 24 hours
	strcpy( pfdt->strFontName, "fx32x32" );
										// type of positioning
	pfe->lPosition     = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementDatetimeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfdt = (LPFJDATETIME)pfe->pDesc;
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pfdt->lBoldValue    = lBold;
	pfdt->lWidthValue   = 1;
	pfdt->lGapValue     = 0;
										// format of the date/time
	pfdt->lFormat       = DT_DATE_MONTHA;
										// which date/time
	pfdt->lTimeType     = DT_TIME_EXPIRATION;
	pfdt->lLength       = 3;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign        = 'R';			// alignment of value in larger length - L/R
	pfdt->cFill         = ' ';			// fill character. 0 is no fill.
	pfdt->cDelim        = ':';			// delimiter to use between date/time parts
	pfdt->bStringNormal = TRUE;			// which set of strings to use for alphabetic formats
	pfdt->lHourACount   = 24;			// number of time periods in 24 hours
	strcpy( pfdt->strFontName, "fx32x32" );
										// type of positioning
	pfe->lPosition     = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementDatetimeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfdt = (LPFJDATETIME)pfe->pDesc;
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pfdt->lBoldValue    = lBold;
	pfdt->lWidthValue   = 1;
	pfdt->lGapValue     = 0;
	pfdt->lFormat       = DT_DATE_DD;	// format of the date/time
										// which date/time
	pfdt->lTimeType     = DT_TIME_EXPIRATION;
	pfdt->lLength       = 2;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign        = 'R';			// alignment of value in larger length - L/R
	pfdt->cFill         = '0';			// fill character. 0 is no fill.
	pfdt->cDelim        = ':';			// delimiter to use between date/time parts
	pfdt->bStringNormal = TRUE;			// which set of strings to use for alphabetic formats
	pfdt->lHourACount   = 24;			// number of time periods in 24 hours
	strcpy( pfdt->strFontName, "fx32x32" );
										// type of positioning
	pfe->lPosition     = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementTextNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pft = (LPFJTEXT)pfe->pDesc;
	strcpy( pft->strText, "A" );
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pft->lBoldValue    = lBold;
	pft->lWidthValue   = 1;
	pft->lGapValue     = 0;
	strcpy( pft->strFontName, "fx32x32" );
										// type of positioning
	pfe->lPosition    = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

	pfe = fj_ElementDatetimeNew();
	pfe->pfm = pfm;
	fj_ElementMakeMemstorName( pfe, pfe->strName );
	// add the new one to the array
	pfm->lCount = fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );
	pfdt = (LPFJDATETIME)pfe->pDesc;
	pfe->lTransform    = FJ_TRANS_NORMAL;
	pfdt->lBoldValue    = lBold;
	pfdt->lWidthValue   = 1;
	pfdt->lGapValue     = 0;
										// format of the date/time
	pfdt->lFormat       = DT_DATE_DAYWKA;
										// which date/time
	pfdt->lTimeType     = DT_TIME_NORMAL;
	pfdt->lLength       = 1;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign        = 'L';			// alignment of value in larger length - L/R
	pfdt->cFill         = ' ';			// fill character. 0 is no fill.
	pfdt->cDelim        = ':';			// delimiter to use between date/time parts
	pfdt->bStringNormal = FALSE;		// which set of strings to use for alphabetic formats
	pfdt->lHourACount   = 24;			// number of time periods in 24 hours
	strcpy( pfdt->strFontName, "fx32x32" );
										// type of positioning
	pfe->lPosition     = FJ_POSITION_CONCATENATE;
	pfe->lRow          = 0;				// row number to start pixels - 0 = bottom of print head
	pfe->fLeft         = 0.0;			// distance from left edge of box
	pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );

										// this message name into label
	strcpy( pfl->fmMessageIDs[0], pfm->strName );
										// add to MemStor
	fj_LabelAddToMemstor( pfph->pMemStorEdit, pfl );
										// add to MemStor
	fj_MessageAddToMemstor( pfph->pMemStorEdit, pfm );
	fj_MessageDestroy( pfm );
	fj_LabelDestroy( pfl );
	pfl = NULL;

	return;
}
//
//  FUNCTION: fj_print_head_InitMsgsAndFonts()
//
//  PURPOSE:  Initialize messages and fonts
//
VOID fj_print_head_InitMsgsAndFonts( LPFJPRINTHEAD pfph )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	LPFJLABEL    pfl;
	LPCROMENTRY  pRomEntry;
	LPBYTE       pMemStorROM;
	LONG         lMemStorSize;
	WORD32       wCheckSum;
	LPSTR       *papmsg;
	LPSTR       *paplabel;

										// get ROM table entry for msgs
	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_FONTMSG;

	tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
										// get ROM address of message store
	pMemStorROM = pGlobal->pROMLow + pRomEntry->lOffset;
										// allow space for checksum
	lMemStorSize  = pRomEntry->lMaxLength - 4;
	wCheckSum = fj_checksumRomEntry( pRomEntry, pMemStorROM );
	if ( wCheckSum != *((WORD32 *)(pMemStorROM+lMemStorSize)) )
	{
		pfph->pMemStor = NULL;			// check sum is not correct. do not use.
	}
	else
	{
		// initialize MemStor in RAM from ROM buffer
		pfph->pMemStor = fj_MemStorNewFromBuffer( pGlobal->pMemStorBuffer, lMemStorSize, pMemStorROM );
	}
	tx_semaphore_put( &(pGlobal->semaROMLock) );

	if ( (NULL == pfph->pMemStor) ||	// if ROM is bad,
		('{' != *(pfph->pMemStor->pBuffer+sizeof(LONG)))) // or if it does not start with a '{',
	{
		// TEMP: build a couple of messages if there are none.
		if ( NULL != pfph->pMemStor )
		{
			fj_MemStorClear( pfph->pMemStor );
		}
		else
		{
			// initialize ROM/RAM buffer
			pfph->pMemStor = fj_MemStorNew( pGlobal->pMemStorBuffer, lMemStorSize );
		}

		if ( TRUE == fj_print_head_EditStart( 321 ) )
		{
			fj_PrintHeadLoadTestFonts( pfph );
			fj_print_head_LoadMsgs( pfph );
			fj_print_head_LoadTestMsgs( pfph );

			fj_print_head_EditSave( 321 );
			strcpy( fj_CVT.pBRAM->strLabelName, "Ketchup" );
		}
	}

	if (fj_writeROMMsg == TRUE) { // burl
	     if (fj_print_head_EditStart (321) == TRUE) {
	          if (pfph->pMemStorEdit) {
		       fj_MemStorClear (pfph->pMemStorEdit);
		  }

		 fj_print_head_EditSave (321);
	     }
	}

	pfph->pMemStor->bDoNotFreeBuffer = TRUE;

	// build the selected label whose name is saved in battery RAM
	pfl = fj_SystemBuildSelectedLabel( pfph->pfsys, fj_CVT.pBRAM->strLabelName );
	// build the scanner default label whose name is saved in battery RAM
	pfl = fj_SystemBuildScannerDefaultLabel( pfph->pfsys, fj_CVT.pBRAM->strScannerLabelName );
	if ( NULL == pfl )
	{
		fj_CVT.pBRAM->strScannerLabelName[0] = 0;
	}

	if ( NULL != pfph->pfmSelected )
	{
		fj_CVT.fj_RestoreBRAMCounters( pfph->pfmSelected );
	}

	return;
}
// global variable in case we ever want to stop this loop and thread
BOOL   bPrintThreadLoop = TRUE;

LONG lRCPT0;
LONG lRCPT1;
LONG lRCPT2;

// thread routine to check photocell
// thread routine to make FJIMAGEs from the selected message
VOID fj_threadPrintStart(ULONG param)
{
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;
	LPCFJELEMENT  pfe;
	LPFJIMAGE     pfi;
	LPFJMESSAGE   pfm;
	LPFJLABEL     pfl;
	LPBYTE        pImage;
	BOOL   bPrint;
	int    iSeg;

	fj_print_head_InitPhotoCell();

	while ( TRUE == bPrintThreadLoop )
	{
		fj_print_head_UpdateRasterCounter();
		lRCPT0 = fj_CVT.pFJglobal->lRasterCount;
		fj_print_head_UpdateRasterCounter();
		lRCPT1 = fj_CVT.pFJglobal->lRasterCount;
		while ( (TRUE == pGlobal->bPrint) || (TRUE == pGlobal->bPrintTestLoop) )
		{
			bPrint = pGlobal->bPrint;
			if ( TRUE == pGlobal->bPrintTestLoop )
			{
				// build the selected label whose name is saved in battery RAM
				fj_SystemBuildSelectedLabel( pfph->pfsys, fj_CVT.pBRAM->strLabelName );
				pGlobal->bPrintTestLoop = FALSE;
			}
			pGlobal->bPrint  = FALSE;
			pfm = pfph->pfmSelected;
			if ( NULL != pfm )
			{
				if ( FJ_PC_MODE_ENABLED == pGlobal->lPhotocellMode ) fj_MessagePhotocellBuild( pfm );

				fj_print_head_UpdateRasterCounter();
				lRCPT2 = fj_CVT.pFJglobal->lRasterCount;
				if ( TRUE == bPrint )	// this is a real request
				{
					fj_print_head_PrintMessage( pfm );
					fj_print_head_Counters( 1, pfm->lBits );
					fj_MessageGetText( pfm, fj_CVT.pBRAM->sLastMessage, sizeof(fj_CVT.pBRAM->sLastMessage)-1 );

					// if we want to use the scanner label only once
					if (pfph->bSerialScannerLabel && pfph->bSerialScannerLabelUseOnce)
					{
						// and if the scanner default is not the selected label
						if ( 0 != strcmp(fj_CVT.pBRAM->strLabelName,fj_CVT.pBRAM->strScannerLabelName) )
						{
							// make it so
							pfl = NULL;
							if( NULL != fj_CVT.pfsys->pflScanner )
							{
								pfl = fj_SystemCopyScannerDefaultLabelToSelectedLabel( fj_CVT.pfsys );
							}
							if ( NULL != pfl )
							{
								// save selected label name
								strcpy( fj_CVT.pBRAM->strLabelName, pfl->strName );
							}
						}

						fj_SysSetPrintMode (pfph->pfsys, "STOP");
					}
				}
				else					// this is a print loop request
				{
										// if nothing queued, excercise the ENI
					if ( NULL == pfph->pfcFirst )
					{
						// clear rasters in all segments
						for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
						{
							pfe = pfm->papfe[iSeg];
							pfi = pfe->pfi;
							pImage = (LPBYTE)pfi + sizeof(FJIMAGE);
							//			            	memset( pImage, 0, pfi->lLength );			// clear the image
						}
						fj_print_head_PrintMessage( pfm );
						fj_print_head_Counters( 1, pfm->lBits );
						fj_MessageGetText( pfm, fj_CVT.pBRAM->sLastMessage, sizeof(fj_CVT.pBRAM->sLastMessage)-1 );
					}
				}
			}
		}
		if ( TRUE == pGlobal->bSendPhotocellDetect )
		{
			pGlobal->bSendPhotocellDetect = FALSE;
			fj_print_head_SendPhotocellDetect();
		}
		if ( TRUE == pGlobal->bSendDataReady )
		{
			pGlobal->bSendDataReady = FALSE;
			fj_print_head_SendDataReadyInfo();
		}
		tx_thread_suspend( &(pGlobal->thrPrint) );
	}
}
static TX_TIMER fj_timerPrintThread;	// timer for starting high priority photocell/print thread

//
// small timer function to count seconds
//
static void fj_timerPrintThreadFunc(ULONG in)
{
	tx_thread_resume( &(fj_CVT.pFJglobal->thrPrint) );
	return;
}
//
// initialize the print head code
//
// write all values to the Gate Array
// install the FIFO Transmit interrupt routine
// disable the ISR until there is something to print
//
VOID fj_print_head_Init( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int    ret;
	LPBYTE pStack;

	fj_PrintHeadReset( fj_CVT.pfph );

	// initialize the SPI hardware for the A/D converter
	fj_print_head_InitSPI();
	fj_print_head_InitAD();
	// set the working values into the Gate Array before ENI
	fj_print_head_SetGAValues();
	// initialize the ENI hardware for the rasters
	fj_print_head_InitENI();

	pStack = fj_memory_getStack(FJ_THREAD_PRINT_STACK_SIZE);
	// create the high priority thread check photcell and to make FJIMAGE structures and queue them for printing
										// control block for thread
	ret = tx_thread_create (&(pGlobal->thrPrint),
		"Printer Thread",				// thread name
		fj_threadPrintStart,			// entry function
		0,								// parameter
		pStack,							// start of stack
		FJ_THREAD_PRINT_STACK_SIZE,		// size of stack
		FJ_THREAD_PRINT_PRIORITY,		// priority
		FJ_THREAD_PRINT_PRIORITY,		// preemption threshold
		1,								// time slice threshold
		TX_AUTO_START);					// start immediately

	if (ret != TX_SUCCESS)				// if couldn't create thread
	{
		netosFatalError ("Unable to create print thread", 1, 1);
	}

	// create the photocell/print thread timer to check photocell in a tight loop
	ret = tx_timer_create( &fj_timerPrintThread,
		"photocell/print",
		fj_timerPrintThreadFunc,
		0x345,							//id
		1,								// as fast as it can run
		1,
		TX_AUTO_ACTIVATE );

	fj_print_head_InitMsgsAndFonts( fj_CVT.pfph );
	return;
}
