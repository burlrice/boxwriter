#include "fj.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_bitmap.h"
#include "fj_element.h"
#include "fj_label.h"
#include "fj_socket.h"


extern ROMENTRY        fj_RomTable[];

//
// this was first written only to serve the browser's java applet. hence, some of the comments and labels.
//
//
// dynamic info, such as heater switch and heater temperature, is written to a port
// as it changes.
// a java applet, downloaded as part of the web pages, listens on this port and
// updates the proper browser pages.
//
// currently, we send a print head status message every 30 seconds to keep the connection alive.
// this also serves as a connection check. if there is send error, the connection is terminated.
//

static    int    iSocketListen;			// socket for listening for an accept
static    int    iSocketListenB;		// socket for listening for a UDP broadcast
static    LPBYTE pBuffer;
static    LONG   lBufferSize = sizeof(_FJ_SOCKET_MESSAGE);
static    LPBYTE pBufferBig = NULL;
static    LONG   lBufferBigSize;

enum IOBOXSTATE
{
	IOBOX_ALL_OFF = 0x00,
	IOBOX_GREEN_ON,
	IOBOX_GREEN_OFF,
	IOBOX_GREEN_FLASH,
	IOBOX_RED_ON,
	IOBOX_RED_OFF,
	IOBOX_RED_FLASH,
};

static enum IOBOXSTATE lIOBox_State = IOBOX_ALL_OFF;

static BYTE IOBox_Buffer[SOCKET_BUFFER_SIZE];

// forward declarations
VOID fj_socketSendBuffer( LPVOID pVoid, LPBYTE pByte, LONG lLength );

VOID fj_socketZapTabEntry( LPFJSOCKET pfjSocket )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;

	pfjSocket->bInUse         = FALSE;	// true if this structure is in use
	pfjSocket->bBrowserApplet = FALSE;	// true if this socket is for a browser applet
	pfjSocket->bDebugger      = FALSE;	// true if this socket is for the debugger application
	pfjSocket->bFTP           = FALSE;	// true if this for a FTP connection
	pfjSocket->bIOBox         = FALSE;	// true if this socket is to the Net I/O box

	pfjSocket->lIPaddr      = 0;
	pfjSocket->iSocketRW    = -1;
	pfjSocket->lSecLevel    = 0;		// security level for this socket
	pfjSocket->lSeconds     = 0;		// time when entry as last active. 0 = never. zeroed after some interval.
	pfjSocket->lAckTime     = 0;		//pGlobal->lSeconds;	// reset time out
	pfjSocket->lBatchIDSend = 0;		// this stays the same for all packets that are sent as part of one command.
	pfjSocket->lBatchIDRecv = 0;		// this stays the same for all packets that are read as part of one command.
	pfjSocket->lEchoBatchID = 0;		// saved to avoid endless re-echos.

	return;
}
VOID fj_socket_IOBoxAllOff( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iStrobeA, iStrobeB;

	if ( pGlobal->lIOBoxSocketIndex >= 0 )
	{
		if ( 1 == fj_CVT.pfsys->iIOBoxStrobe )
		{
			iStrobeB = SET_STROBE_1B;
			iStrobeA = SET_STROBE_1A;
		}
		else
		{
			iStrobeB = SET_STROBE_2B;
			iStrobeA = SET_STROBE_2A;
		}
		memset(IOBox_Buffer,0,SOCKET_BUFFER_SIZE);
		IOBox_Buffer[0] = STROBE_OFF;
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeB, IOBox_Buffer, SOCKET_BUFFER_SIZE+1 );
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeA, IOBox_Buffer, SOCKET_BUFFER_SIZE+1 );
		lIOBox_State = IOBOX_ALL_OFF;
	}
	return;
}
VOID fj_socket_IOBoxGreenOn( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iStrobeB;

	if ( pGlobal->lIOBoxSocketIndex >= 0 )
	{
		if (IOBOX_GREEN_ON != lIOBox_State) fj_socket_IOBoxAllOff();
		if ( 1 == fj_CVT.pfsys->iIOBoxStrobe ) iStrobeB = SET_STROBE_1B;
		else iStrobeB = SET_STROBE_2B;
		IOBox_Buffer[0] = STROBE_ON;
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeB, IOBox_Buffer, SOCKET_BUFFER_SIZE+1);
		lIOBox_State = IOBOX_GREEN_ON;
	}
	return;
}
VOID fj_socket_IOBoxRedOn( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iStrobeA;

	if ( pGlobal->lIOBoxSocketIndex >= 0 )
	{
		if (IOBOX_RED_ON != lIOBox_State) fj_socket_IOBoxAllOff();
		if ( 1 == fj_CVT.pfsys->iIOBoxStrobe ) iStrobeA = SET_STROBE_1A;
		else iStrobeA = SET_STROBE_2A;
		IOBox_Buffer[0] = STROBE_ON;
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeA, IOBox_Buffer, SOCKET_BUFFER_SIZE+1);
		lIOBox_State = IOBOX_RED_ON;
	}
	return;
}
VOID fj_socket_IOBoxRedFlash( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iStrobeA;

	if ( pGlobal->lIOBoxSocketIndex >= 0 )
	{
		if (IOBOX_RED_FLASH != lIOBox_State) fj_socket_IOBoxAllOff();
		if ( 1 == fj_CVT.pfsys->iIOBoxStrobe ) iStrobeA = SET_STROBE_1A;
		else iStrobeA = SET_STROBE_2A;
		IOBox_Buffer[0] = STROBE_FLASH;
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeA, IOBox_Buffer, SOCKET_BUFFER_SIZE+1 );
		lIOBox_State = IOBOX_RED_FLASH;
	}
	return;
}
VOID fj_socket_IOBoxGreenFlash( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iStrobeB;

	if ( pGlobal->lIOBoxSocketIndex >= 0 )
	{
		if (IOBOX_GREEN_FLASH != lIOBox_State) fj_socket_IOBoxAllOff();
		if ( 1 == fj_CVT.pfsys->iIOBoxStrobe ) iStrobeB = SET_STROBE_1B;
		else iStrobeB = SET_STROBE_2B;
		IOBox_Buffer[0] = STROBE_FLASH;
		fj_socketSendOne( pGlobal->lIOBoxSocketIndex, iStrobeB, IOBox_Buffer, SOCKET_BUFFER_SIZE+1 );
		lIOBox_State = IOBOX_GREEN_FLASH;
	}
	return;
}
//
// process IPC_CLOSE
//
VOID fj_socket_IPC_CLOSE( LONG lIndex )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int iSocketClose;

	if  ( FALSE == fj_asockets[lIndex].bFTP )
	{
		iSocketClose  = fj_asockets[lIndex].iSocketRW;
		if ( TRUE == fj_asockets[lIndex].bIOBox )
			fj_CVT.pFJglobal->lIOBoxSocketIndex = -1;

		fj_asockets[lIndex].bBrowserApplet = FALSE;
		fj_asockets[lIndex].iSocketRW = -1;
		fj_asockets[lIndex].lSeconds = fj_CVT.pFJglobal->lSeconds;
		fj_asockets[lIndex].bInUse = FALSE;

		socketclose( iSocketClose );
	}
	return;
}
//
// process a received command
//
VOID fj_socketRecv( LONG lIndex, LPBYTE pBuf )
{
	LPFJPRINTHEAD pfph = fj_CVT.pfph;
	_FJ_SOCKET_MESSAGE *psmsg;
	_FJ_SOCKET_MESSAGE smsgOut;
	LPSTR  *papList;
	LONG   lCmd;
	LONG   lBatchID;
	LONG   lDataLength;
	CHAR   str[88];
	int    i;

	psmsg = (_FJ_SOCKET_MESSAGE *)pBuf;
	lCmd        = psmsg->Header.Cmd;
	lBatchID    = psmsg->Header.BatchID;
	lDataLength = psmsg->Header.Length;

	// first: process the common software commands
	fj_socketCmd( pfph, (LPVOID)lIndex, fj_asockets[lIndex].lIPaddr, psmsg, &smsgOut,fj_socketSendBuffer );
	// second: process what needs to be done for our socket
	switch ( lCmd )
	{
		case IPC_CONNECTION_CLOSE:
			fj_socket_IPC_CLOSE( lIndex );
			break;

		case IPC_ECHO:
			if ( lBatchID != fj_asockets[lIndex].lEchoBatchID )
			{
				fj_asockets[lIndex].lEchoBatchID = lBatchID;
				fj_socketSendBuffer( (LPVOID)lIndex, pBuf, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
			}
			if ( (9 == lDataLength) && (0 == strncmp((LPSTR)psmsg->Data, "FJ APPLET", 9)) )
			{
				fj_asockets[lIndex].bBrowserApplet = TRUE;
			}
			if ( (5 == lDataLength) && (0 == strncmp((LPSTR)psmsg->Data, "DEBUG", 5)) )
			{
				fj_asockets[lIndex].bDebugger = TRUE;
			}
			break;

		case IPC_GET_STATUS:
			fj_print_head_SendInfoAll();// send print head info
			break;

			// the edit commands do nothing in the common fj_socketCmd routine.
			// the edit routines that are invoked here will send a IPC_EDIT_STATUS to every connection - but with our ID.
			// change the current command to a IPC_GET_EDIT_STATUS and send a IPC_EDIT_STATUS with the caller's ID.
		case IPC_EDIT_START:
			fj_print_head_EditStart( fj_asockets[lIndex].lIPaddr );
			psmsg->Header.Cmd = IPC_GET_EDIT_STATUS;
			fj_socketCmd( pfph, (LPVOID)lIndex, fj_asockets[lIndex].lIPaddr, psmsg, &smsgOut,fj_socketSendBuffer );
			break;

		case IPC_EDIT_SAVE:
			fj_print_head_EditSave( fj_asockets[lIndex].lIPaddr );
			psmsg->Header.Cmd = IPC_GET_EDIT_STATUS;
			fj_socketCmd( pfph, (LPVOID)lIndex, fj_asockets[lIndex].lIPaddr, psmsg, &smsgOut,fj_socketSendBuffer );
			break;

		case IPC_EDIT_CANCEL:
			fj_print_head_EditCancel( fj_asockets[lIndex].lIPaddr );
			psmsg->Header.Cmd = IPC_GET_EDIT_STATUS;
			fj_socketCmd( pfph, (LPVOID)lIndex, fj_asockets[lIndex].lIPaddr, psmsg, &smsgOut,fj_socketSendBuffer );
			break;

		case IPC_DELETE_LABEL:
			if ( 0 == strncmp( (LPSTR)psmsg->Data, "LABELID=", 8 ) )
			{
				strncpy( str, (LPSTR)(psmsg->Data+8), 87 );
				i = strlen( str );
				if ( ';' == str[i-1] ) str[i-1] = 0;
				if ( 0 == strcmp(str, fj_CVT.pBRAM->strLabelName) )
				{
					pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
					fj_SystemBuildSelectedLabel( fj_CVT.pfsys, NULL );
										// clear selected label name
					fj_CVT.pBRAM->strLabelName[0] = 0;
				}
			}
			break;

		case IPC_SELECT_LABEL:
		case IPC_SET_LABEL_ID_SELECTED:
			if ( NULL != fj_CVT.pfsys->pfl )
			{
				// save selected label name
				strcpy( fj_CVT.pBRAM->strLabelName, fj_CVT.pfsys->pfl->strName );
			}
			break;

		case IPC_SET_SYSTEM_INFO:
			fj_print_head_KillENI();
										// sent encoder value
			fj_print_head_SetGAEncoder();
										// set encoder switch
			fj_print_head_SetGAWriteSwitches();
			fj_print_head_InitENI();
			break;

		default:
			break;
	}

	return;
}
VOID fj_socket_IOBoxDisconnect( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	STRING str[64];

	if ( pGlobal->lIOBoxSocketIndex != -1)
	{
		fj_socket_IOBoxAllOff();
		sprintf( str, "IOBOX DISCONNECTED (%d) FROM %d.%d.%d.%d\n",(int)pGlobal->lIOBoxSocketIndex,PUSH_IPADDR(fj_asockets[pGlobal->lIOBoxSocketIndex].lIPaddr));
		fj_writeInfo( str );
		fj_socket_IPC_CLOSE( pGlobal->lIOBoxSocketIndex );
		pGlobal->lIOBoxSocketIndex = -1;
	}
}
VOID fj_socket_IOBoxConnect( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJSYSTEM pfsys   = fj_CVT.pfsys;
	LPFJSOCKET   pfjSocket;
	struct sockaddr_in addrNamesIO = { AF_INET, 0, INADDR_ANY, 0 };
	STRING str[64];
	LONG   lRet;
	LONG   rete;
	int    error;
	int    errorSize;
	int iOption;
	int j;

	if (pGlobal->lIOBoxSocketIndex != -1) fj_socket_IOBoxDisconnect();

	for ( j = 0; j < pGlobal->lSocketCount; j++ )
	{
		pfjSocket = &(fj_asockets[j]);
		if ( FALSE == pfjSocket->bInUse )
		{
										// index into socket table for Net I/O Box socket
			pGlobal->lIOBoxSocketIndex = j;
			pfjSocket->bInUse       = TRUE;
										// true if this socket is to the Net I/O box
			pfjSocket->bIOBox       = TRUE;
										//MAKE_IPADDR(192,168,254,140);
			pfjSocket->lIPaddr      = pfsys->lIOBoxIP;
			pfjSocket->iSocketRW    = socket( AF_INET, SOCK_STREAM, 0 );
			if ( pfjSocket->iSocketRW > 0 )
			{
				iOption = 1;			// set value to turn options on
				lRet = setsockopt( pfjSocket->iSocketRW, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption) );
				lRet = setsockopt( pfjSocket->iSocketRW, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption) );
				lRet = setsockopt( pfjSocket->iSocketRW, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );
				// bind this socket to the port
				addrNamesIO.sin_port = _IO_SOCKET_NUMBER;
				lRet = bind( pfjSocket->iSocketRW, &addrNamesIO, sizeof(addrNamesIO));
				addrNamesIO.sin_addr.s_addr = pfjSocket->lIPaddr;
				lRet = connect( pfjSocket->iSocketRW, &addrNamesIO, sizeof(addrNamesIO));
				if ( -1 == lRet )
				{
					errorSize = sizeof(error);
					rete = getsockopt(pfjSocket->iSocketRW, SOL_SOCKET, SO_ERROR, (char *)&error, &errorSize);
					if ( 0 == rete )
					{
						if ( 0 == errorSize ) lRet = 0;
						else if ( EINPROGRESS == error ) lRet = 0;
					}
				}
				fj_socket_IOBoxAllOff();

				if (pGlobal->lIOBoxSocketIndex != -1)
				{
					sprintf( str, "Connecting to IOBOX (%d) FROM %d.%d.%d.%d\n",(int)pGlobal->lIOBoxSocketIndex,PUSH_IPADDR(fj_asockets[pGlobal->lIOBoxSocketIndex].lIPaddr));
					fj_writeInfo( str );
				}
				break;
			}
		}
	}
}
//
// initialize the socket listen / accept code
//
VOID fj_socketInit(VOID)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJSYSTEM pfsys   = fj_CVT.pfsys;
	LPFJSOCKET   pfjSocket;
	struct sockaddr_in addrNames   = { AF_INET, 0, INADDR_ANY, 0 };
	struct sockaddr_in addrNamesB  = { AF_INET, 0, INADDR_ANY, 0 };
	struct sockaddr_in addrNamesIO = { AF_INET, 0, INADDR_ANY, 0 };
	int    iOption;
	int    j;
	LONG   lRet;
	LONG   rete;
	int    error;
	int    errorSize;

	pGlobal->lIOBoxSocketIndex = -1;
	addrNames.sin_port = _FJ_SOCKET_NUMBER;
										// allocate buffer
	pBuffer = (LPBYTE)fj_memory_getOnce( lBufferSize );

	pGlobal->lSocketCount = FJSYS_NUMBER_SOCKETS;
	if ( FJSYS_INTERNET_SLAVE == pfsys->cInternetControl ) pGlobal->lSocketCount = 1;

	for ( j = 0; j < pGlobal->lSocketCount; j++ )
	{
		pfjSocket = &(fj_asockets[j]);
		fj_socketZapTabEntry( pfjSocket );
	}

	if ( TRUE == pfsys->bIOBoxUse )
	{
		fj_socket_IOBoxConnect();
	}

	iSocketListen = socket( AF_INET, SOCK_STREAM, 0 );
	if ( iSocketListen > 0 )
	{
		iOption = 1;					// set value to turn options on
		lRet = setsockopt( iSocketListen, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption) );
		lRet = setsockopt( iSocketListen, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption) );
		lRet = setsockopt( iSocketListen, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );
		//		iOption = 0;					// set value to turn options off
		//		lRet = setsockopt( iSocketListen, SOL_SOCKET, SO_DONTROUTE, (char*)&iOption, sizeof(iOption) );
		// bind this socket to the port
		addrNamesB.sin_port = _FJ_SOCKET_NUMBER;
		lRet = bind( iSocketListen, &addrNamesB, sizeof(addrNamesB));
		if ( 0 == lRet )
		{
			lRet = listen( iSocketListen, 5 );
		}
	}
	else
	{
	}

	iSocketListenB = socket( AF_INET, SOCK_DGRAM, 0 );
	if ( iSocketListenB > 0 )
	{
		iOption = 1;					// set value to turn options on
		lRet = setsockopt( iSocketListenB, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption) );
		lRet = setsockopt( iSocketListenB, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption) );
		lRet = setsockopt( iSocketListenB, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );
		//		iOption = 0;					// set value to turn options off
		//		lRet = setsockopt( iSocketListenB, SOL_SOCKET, SO_DONTROUTE, (char*)&iOption, sizeof(iOption) );
		if ( 0 == lRet )
		{
			lRet = 43;
		}
		// bind this socket to the port
		addrNamesB.sin_port = _FJ_SOCKET_NUMBER;
		lRet = bind( iSocketListenB, &addrNamesB, sizeof(addrNamesB));
		if ( 0 == lRet )
		{
			lRet = 43;
		}
	}
	else
	{
	}

	return;
}
//
// return string for socket command
//
LPSTR fj_socketCommandString( LONG lCmd )
{
	LPSTR pstr;

	switch ( lCmd )
	{
		case IPC_CONNECTION_CLOSE:      pstr = "IPC_CONNECTION_CLOSE";      break;
		case IPC_INVALID:           pstr = "IPC_INVALID";               break;
		case IPC_ECHO:              pstr = "IPC_ECHO";                  break;
		case IPC_STATUS:            pstr = "IPC_STATUS";                break;
		case IPC_GET_STATUS:            pstr = "IPC_GET_STATUS";            break;
		case IPC_PHOTO_TRIGGER:         pstr = "IPC_PHOTO_TRIGGER";         break;
		case IPC_EDIT_STATUS:           pstr = "IPC_EDIT_STATUS";           break;
		case IPC_GET_EDIT_STATUS:       pstr = "IPC_GET_EDIT_STATUS";       break;
		case IPC_EDIT_START:            pstr = "IPC_EDIT_START";            break;
		case IPC_EDIT_SAVE:         pstr = "IPC_EDIT_SAVE";             break;
		case IPC_EDIT_CANCEL:           pstr = "IPC_EDIT_CANCEL";           break;
		case IPC_GET_LABEL_IDS:         pstr = "IPC_GET_LABEL_IDS";         break;
		case IPC_SET_LABEL_IDS:         pstr = "IPC_SET_LABEL_IDS";         break;
		case IPC_GET_LABEL_ID_SELECTED:     pstr = "IPC_GET_LABEL_ID_SELECTED"; break;
		case IPC_SET_LABEL_ID_SELECTED:     pstr = "IPC_SET_LABEL_ID_SELECTED"; break;
		case IPC_GET_LABELS:            pstr = "IPC_GET_LABELS";            break;
		case IPC_GET_LABEL:         pstr = "IPC_GET_LABEL";             break;
		case IPC_PUT_LABEL:         pstr = "IPC_PUT_LABEL";             break;
		case IPC_DELETE_LABEL:          pstr = "IPC_DELETE_LABEL";          break;
		case IPC_DELETE_ALL_LABELS:     pstr = "IPC_DELETE_ALL_LABELS";     break;
		case IPC_SELECT_LABEL:          pstr = "IPC_SELECT_LABEL";          break;
		case IPC_GET_MSG_IDS:           pstr = "IPC_GET_MSG_IDS";           break;
		case IPC_SET_MSG_IDS:           pstr = "IPC_SET_MSG_IDS";           break;
		case IPC_GET_MSGS:          pstr = "IPC_GET_MSGS";              break;
		case IPC_GET_MSG:           pstr = "IPC_GET_MSG";               break;
		case IPC_PUT_MSG:           pstr = "IPC_PUT_MSG";               break;
		case IPC_DELETE_MSG:            pstr = "IPC_DELETE_MSG";            break;
		case IPC_GET_TEXT_IDS:          pstr = "IPC_GET_TEXT_IDS";          break;
		case IPC_SET_TEXT_IDS:          pstr = "IPC_SET_TEXT_IDS";          break;
		case IPC_GET_TEXT_ELEMENTS:     pstr = "IPC_GET_TEXT_ELEMENTS";     break;
		case IPC_GET_TEXT:          pstr = "IPC_GET_TEXT";              break;
		case IPC_PUT_TEXT:          pstr = "IPC_PUT_TEXT";              break;
		case IPC_DELETE_TEXT:           pstr = "IPC_DELETE_TEXT";           break;
		case IPC_GET_BITMAP_IDS:        pstr = "IPC_GET_BITMAP_IDS";        break;
		case IPC_SET_BITMAP_IDS:        pstr = "IPC_SET_BITMAP_IDS";        break;
		case IPC_GET_BITMAP_ELEMENTS:       pstr = "IPC_GET_BITMAP_ELEMENTS";   break;
		case IPC_GET_BITMAP:            pstr = "IPC_GET_BITMAP";            break;
		case IPC_PUT_BITMAP:            pstr = "IPC_PUT_BITMAP";            break;
		case IPC_DELETE_BITMAP:         pstr = "IPC_DELETE_BITMAP";         break;
		case IPC_GET_DYNIMAGE_IDS:      pstr = "IPC_GET_DYNIMAGE_IDS";      break;
		case IPC_SET_DYNIMAGE_IDS:      pstr = "IPC_SET_DYNIMAGE_IDS";      break;
		case IPC_GET_DYNIMAGE_ELEMENTS:     pstr = "IPC_GET_DYNIMAGE_ELEMENTS"; break;
		case IPC_GET_DYNIMAGE:          pstr = "IPC_GET_DYNIMAGE";          break;
		case IPC_PUT_DYNIMAGE:          pstr = "IPC_PUT_DYNIMAGE";          break;
		case IPC_DELETE_DYNIMAGE:       pstr = "IPC_DELETE_DYNIMAGE";       break;
		case IPC_PUT_DYNIMAGE_DATA:     pstr = "IPC_PUT_DYNIMAGE_DATA";     break;
		case IPC_GET_DATETIME_IDS:      pstr = "IPC_GET_DATETIME_IDS";      break;
		case IPC_SET_DATETIME_IDS:      pstr = "IPC_SET_DATETIME_IDS";      break;
		case IPC_GET_DATETIME_ELEMENTS:     pstr = "IPC_GET_DATETIME_ELEMENTS"; break;
		case IPC_GET_DATETIME:          pstr = "IPC_GET_DATETIME";          break;
		case IPC_PUT_DATETIME:          pstr = "IPC_PUT_DATETIME";          break;
		case IPC_DELETE_DATETIME:       pstr = "IPC_DELETE_DATETIME";       break;
		case IPC_GET_COUNTER_IDS:       pstr = "IPC_GET_COUNTER_IDS";       break;
		case IPC_SET_COUNTER_IDS:       pstr = "IPC_SET_COUNTER_IDS";       break;
		case IPC_GET_COUNTER_ELEMENTS:      pstr = "IPC_GET_COUNTER_ELEMENTS";  break;
		case IPC_GET_COUNTER:           pstr = "IPC_GET_COUNTER";           break;
		case IPC_PUT_COUNTER:           pstr = "IPC_PUT_COUNTER";           break;
		case IPC_DELETE_COUNTER:        pstr = "IPC_DELETE_COUNTER";        break;
		case IPC_GET_BARCODE_IDS:       pstr = "IPC_GET_BARCODE_IDS";       break;
		case IPC_SET_BARCODE_IDS:       pstr = "IPC_SET_BARCODE_IDS";       break;
		case IPC_GET_BARCODE_ELEMENTS:      pstr = "IPC_GET_BARCODE_ELEMENTS";  break;
		case IPC_GET_BARCODE:           pstr = "IPC_GET_BARCODE";           break;
		case IPC_PUT_BARCODE:           pstr = "IPC_PUT_BARCODE";           break;
		case IPC_DELETE_BARCODE:        pstr = "IPC_DELETE_BARCODE";        break;
		case IPC_GET_FONT_IDS:          pstr = "IPC_GET_FONT_IDS";          break;
		case IPC_SET_FONT_IDS:          pstr = "IPC_SET_FONT_IDS";          break;
		case IPC_GET_FONTS:         pstr = "IPC_GET_FONTS";             break;
		case IPC_GET_FONT:          pstr = "IPC_GET_FONT";              break;
		case IPC_PUT_FONT:          pstr = "IPC_PUT_FONT";              break;
		case IPC_DELETE_FONT:           pstr = "IPC_DELETE_FONT";           break;
		case IPC_GET_BMP_IDS:           pstr = "IPC_GET_BMP_IDS";           break;
		case IPC_SET_BMP_IDS:           pstr = "IPC_SET_BMP_IDS";           break;
		case IPC_GET_BMPS:          pstr = "IPC_GET_BMPS";              break;
		case IPC_GET_BMP:           pstr = "IPC_GET_BMP";               break;
		case IPC_PUT_BMP:           pstr = "IPC_PUT_BMP";               break;
		case IPC_DELETE_BMP:            pstr = "IPC_DELETE_BMP";            break;
		case IPC_DELETE_ALL_BMPS:       pstr = "IPC_DELETE_ALL_BMPS";       break;
		case IPC_GET_DYNTEXT_IDS:       pstr = "IPC_GET_DYNTEXT_IDS";       break;
		case IPC_SET_DYNTEXT_IDS:       pstr = "IPC_SET_DYNTEXT_IDS";       break;
		case IPC_GET_DYNTEXT_ELEMENTS:  pstr = "IPC_GET_DYNTEXT_ELEMENTS";       break;
		case IPC_GET_DYNTEXT:           pstr = "IPC_GET_DYNTEXT";       break;
		case IPC_PUT_DYNTEXT:           pstr = "IPC_PUT_DYNTEXT";       break;
		case IPC_DELETE_DYNTEXT:        pstr = "IPC_DELETE_DYNTEXT";       break;
		case IPC_GET_DYNBARCODE_IDS:    pstr = "IPC_GET_DYNBARCODE_IDS";       break;
		case IPC_SET_DYNBARCODE_IDS:    pstr = "IPC_SET_DYNBARCODE_IDS";       break;
		case IPC_GET_DYNBARCODE_ELEMENTS: pstr = "IPC_GET_DYNBARCODE_ELEMENTS";       break;
		case IPC_GET_DYNBARCODE:        pstr = "IPC_GET_DYNBARCODE";       break;
		case IPC_PUT_DYNBARCODE:        pstr = "IPC_PUT_DYNBARCODE";       break;
		case IPC_DELETE_DYNBARCODE:     pstr = "IPC_DELETE_DYNBARCODE";       break;
		case IPC_GET_GROUP_INFO:        pstr = "IPC_GET_GROUP_INFO";        break;
		case IPC_SET_GROUP_INFO:        pstr = "IPC_SET_GROUP_INFO";        break;
		case IPC_GET_SYSTEM_INFO:       pstr = "IPC_GET_SYSTEM_INFO";       break;
		case IPC_GET_SYSTEM_INTERNET_INFO:  pstr = "IPC_GET_SYSTEM_INTERNET_INFO"; break;
		case IPC_GET_SYSTEM_TIME_INFO:      pstr = "IPC_GET_SYSTEM_TIME_INFO";  break;
		case IPC_GET_SYSTEM_DATE_STRINGS:   pstr = "IPC_GET_SYSTEM_DATE_STRINGS";   break;
		case IPC_GET_SYSTEM_PASSWORDS:      pstr = "IPC_GET_SYSTEM_PASSWORDS";  break;
		case IPC_GET_SYSTEM_BARCODES:       pstr = "IPC_GET_SYSTEM_BARCODES";   break;
		case IPC_SET_SYSTEM_INFO:       pstr = "IPC_SET_SYSTEM_INFO";       break;
		case IPC_GET_PRINTER_ID:        pstr = "IPC_GET_PRINTER_ID";        break;
		case IPC_GET_PRINTER_INFO:      pstr = "IPC_GET_PRINTER_INFO";      break;
		case IPC_GET_PRINTER_PKG_INFO:  pstr = "IPC_GET_PRINTER_PKG_INFO";  break;
		case IPC_GET_PRINTER_PHY_INFO:  pstr = "IPC_GET_PRINTER_PHY_INFO";  break;
		case IPC_GET_PRINTER_OP_INFO:   pstr = "IPC_GET_PRINTER_OP_INFO";   break;
		case IPC_SET_PRINTER_INFO:      pstr = "IPC_SET_PRINTER_INFO";      break;
		case IPC_GET_PRINTER_ELEMENTS:  pstr = "IPC_GET_PRINTER_ELEMENTS";  break;
		case IPC_PUT_PRINTER_ELEMENT:   pstr = "IPC_PUT_PRINTER_ELEMENT";   break;
		case IPC_DELETE_ALL_PRINTER_ELEMENTS:   pstr = "IPC_DELETE_ALL_PRINTER_ELEMENTS";   break;
		case IPC_FIRMWARE_UPGRADE:      pstr = "IPC_FIRMWARE_UPGRADE";      break;
		case IPC_SET_PH_TYPE:           pstr = "IPC_SET_PH_TYPE";           break;
		case IPC_SET_PRINTER_MODE:      pstr = "IPC_SET_PRINTER_MODE";      break;
		case IPC_GET_PRINTER_MODE:      pstr = "IPC_GET_PRINTER_MODE";      break;
		case IPC_SET_SELECTED_COUNT:    pstr = "IPC_SET_SELECTED_COUNT";    break;
		case IPC_GET_SELECTED_COUNT:    pstr = "IPC_GET_SELECTED_COUNT";    break;
		case IPC_SELECT_VARDATA_ID:     pstr = "IPC_SELECT_VARDATA_ID";     break;
		case IPC_GET_VARDATA_ID:        pstr = "IPC_GET_VARDATA_ID";        break;
		case IPC_SET_VARDATA_ID:        pstr = "IPC_SET_VARDATA_ID";        break;
		case IPC_SAVE_ALL_PARAMS:       pstr = "IPC_SAVE_ALL_PARAMS";       break;
		case IPC_GET_SW_VER_INFO:       pstr = "IPC_GET_SW_VER_INFO";       break;
		case IPC_GET_GA_VER_INFO:       pstr = "IPC_GET_GA_VER_INFO";       break;
		case IPC_GA_UPGRADE:        pstr = "IPC_GA_UPGRADE";      break;
		case IPC_SET_STATUS_MODE:      pstr = "IPC_SET_STATUS_MODE";      break;
		case IPC_GET_STATUS_MODE:      pstr = "IPC_GET_STATUS_MODE";      break;
		default:                        pstr = NULL ;                       break;
	}

	return( pstr );
}
//
// return string for I/O Box command
//
LPSTR fj_socketIOBoxCommandString( LONG lCmd )
{
	LPSTR pstr;

	switch ( lCmd )
	{
		case DATA_FROM_PORT1:       pstr = "IOBOX_DATA_FROM_PORT1";     break;
		case DATA_FROM_PORT2:       pstr = "IOBOX_DATA_FROM_PORT2";     break;
		case DATA_FROM_PORT3:       pstr = "IOBOX_DATA_FROM_PORT3";     break;
		case DATA_FROM_PORT4:       pstr = "IOBOX_DATA_FROM_PORT4";     break;
		case DATA_FROM_PS2_PORT:    pstr = "IOBOX_DATA_FROM_PS2_PORT";  break;
		case READ_INPUT_1:          pstr = "IOBOX_READ_INPUT_1";        break;
		case COMMAND_ACK:           pstr = "IOBOX_COMMAND_ACK";        break;
		case STROBE_OFF:            pstr = "IOBOX_STROBE_OFF";        break;
		default:                    pstr = NULL;                        break;
	}

	return( pstr );
}
//
// check the socket service
//
VOID fj_socketCheck(VOID)
{
	LPFJ_GLOBAL        pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph   = fj_CVT.pfph;
	LPFJSOCKET          pfjSocket;
	_FJ_SOCKET_MESSAGE *psmsg;
	struct sockaddr_in addrLB;
	struct sockaddr_in addrAccept;
	int    sizesockaddr_in = sizeof( struct sockaddr_in );
	int    iOption;
	int    iOptionSize;
	int    i, j;
	LONG   lBytesRead;
	LONG   lBytesRead1;
	LONG   lBytesRead2;
	LONG   lCmd;
	LONG   lBufferSize;
	LONG   lBufferAllocate;
	LONG   lDataLength;
	ULONG  lCheckTime;
	BOOL   bTryAccept;
	BOOL   bAllowed;
	int    iError;
	int    iErrorSize;
	int    iErrorRet;
										// left over from some old test and recovery
	static     BOOL   bForceCloseJava = FALSE;
										// left over from some old test and recovery
	static     BOOL   bForceCloseHere = FALSE;
	LONG   lRet;
	LPSTR  pCommandString;
	STRING str[99];

	if ( iSocketListen > 0 )
	{
		bTryAccept = TRUE;
		for ( j = 0; (j < pGlobal->lSocketCount) && (TRUE == bTryAccept); j++ )
		{
			pfjSocket = &(fj_asockets[j]);
			if ( (FALSE == pfjSocket->bInUse) && (0 == pfjSocket->lIPaddr) )
			{
				bTryAccept = FALSE;		// 1 accept per pass is enough
				pfjSocket->iSocketRW = accept( iSocketListen, &addrAccept, &sizesockaddr_in );
				if ( -1 < pfjSocket->iSocketRW )
				{
					if ( TRUE == pfph->bInfoSocket )
					{
						sprintf( str, "ACCEPT (%d) FROM %d.%d.%d.%d\n",(int)j,PUSH_IPADDR(addrAccept.sin_addr.s_addr));
						fj_writeInfo( str );
					}
					pfjSocket->lIPaddr = addrAccept.sin_addr.s_addr;
					pfjSocket->bInUse = TRUE;
					pfjSocket->bIOBox = FALSE;
					pfjSocket->bBrowserApplet = FALSE;
					pfjSocket->lSecLevel = 0;
										// reset time out
					pfjSocket->lAckTime = pGlobal->lSeconds;
										// this stays the same for all packets that are sent as part of one command.
					pfjSocket->lBatchIDSend = 65536;
										// this stays the same for all packets that are read as part of one command.
					pfjSocket->lBatchIDRecv = 0;
										// saved to avoid endless re-echos.
					pfjSocket->lEchoBatchID = 0;

					for ( i = 0; i < FJSYS_NUMBER_SOCKETS; i++ )
					{
						if ( i != j )
						{
							if ( ((0 < fj_asockets[i].lSeconds)) && (pfjSocket->lIPaddr == fj_asockets[i].lIPaddr) )
							{
								if ( fj_asockets[i].lSecLevel > pfjSocket->lSecLevel )
								{
									pfjSocket->lSecLevel = fj_asockets[i].lSecLevel;
								}
								if ( FALSE == fj_asockets[i].bFTP )
									fj_socketZapTabEntry ( &(fj_asockets[i]) );
							}
						}
					}

					iOptionSize = sizeof( iOption );
					iOption = 255;
					//		lRet = getsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_NONBLOCK,      (char*)&iOption, &iOptionSize );
					iOption = 1;		// set value to turn options on
					//		lRet = setsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption) );
					//		lRet = setsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption) );
					//		lRet = setsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );

					iOptionSize = sizeof( iOption );
					iOption = 255;
					//		lRet = getsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_NONBLOCK,      (char*)&iOption, &iOptionSize );

										// send print head info
					fj_print_head_SendInfoAll();
				}
			}
		}
		for ( j = 0; j < pGlobal->lSocketCount; j++ )
		{
			pfjSocket = &(fj_asockets[j]);
			// try a read, if there is a connection
			if ( (TRUE == pfjSocket->bInUse) && (FALSE == pfjSocket->bFTP) && (-1 < pfjSocket->iSocketRW) )
			{
				iOptionSize = sizeof( iOption );
				iOption = 255;
				//		lRet = getsockopt( pfjSocket->iSocketRW, SOL_SOCKET, SO_NONBLOCK,      (char*)&iOption, &iOptionSize );

				psmsg = (_FJ_SOCKET_MESSAGE *)pBuffer;
				lBytesRead = recv( pfjSocket->iSocketRW, (char*)pBuffer, sizeof(_FJ_SOCKET_MESSAGE_HEADER), 0 );
				if ( sizeof(_FJ_SOCKET_MESSAGE_HEADER) == lBytesRead  )
				{
					lBytesRead1 = lBytesRead;
					lDataLength = psmsg->Header.Length;
					lCmd        = psmsg->Header.Cmd;
					if ( TRUE == pfjSocket->bIOBox )
					{
						//pCommandString = fj_socketIOBoxCommandString( lCmd );
						pCommandString = NULL;
						//lBufferSize = SOCKET_BUFFER_SIZE+1;
						lDataLength = SOCKET_BUFFER_SIZE;
					}
					else
					{
						pCommandString = fj_socketCommandString( lCmd );
						lBufferSize = _FJ_SOCKET_BUFFER_SIZE;
					}
					if ( NULL != pCommandString )
					{
						// check for size of incoming command data
						// if oversize, allocate a big temporary buffer
						lBufferAllocate = lDataLength + sizeof(_FJ_SOCKET_MESSAGE_HEADER) + 1;
						if ( lBufferAllocate > lBufferSize )
						{
							if ( lCmd == IPC_FIRMWARE_UPGRADE || lCmd == IPC_GA_UPGRADE)
							{
								if ((lBufferAllocate < pGlobal->fj_ftp_lRamBufferSize) && (FALSE == pGlobal->fj_ftp_bBufferInUse))
								{
									pBufferBig = pGlobal->fj_ftp_lRamBuffer;
									pGlobal->fj_ftp_bBufferInUse = TRUE;
									//&(fj_RomTable[3]);	// GA ???
									if (lCmd == IPC_GA_UPGRADE)
										memset( pBufferBig, 0, (&(fj_RomTable[3]))->lMaxLength);
								}
								else
									pBufferBig = NULL;
							}
							else pBufferBig = fj_malloc( lBufferAllocate );

							if ( NULL != pBufferBig )
							{
								lBufferSize = lBufferAllocate;
								memcpy( pBufferBig, pBuffer, sizeof(_FJ_SOCKET_MESSAGE_HEADER) );
								psmsg = (_FJ_SOCKET_MESSAGE *)pBufferBig;
							}
						}

						// reset time out
						pfjSocket->lAckTime = pGlobal->lSeconds;
						lBytesRead2 = 0;

						if ( (0 < lDataLength) && (lBufferSize >= lDataLength) )
						{
							LPSTR pReadLoc;
							LONG  lReadLen;
							LONG  lReadTries;
							pReadLoc = (LPSTR)(&(psmsg->Data));
							lReadLen = lDataLength;
							lReadTries = 10;
							lBytesRead2 = 0;
							while ( (lReadLen>0) && (lReadTries>0) )
							{
								lBytesRead = recv( pfjSocket->iSocketRW, pReadLoc, lReadLen, 0 );
								lReadTries--;
								if ( lBytesRead >= 0 )
								{
									lBytesRead2 += lBytesRead;
									pReadLoc    += lBytesRead;
									lReadLen    -= lBytesRead;

									// test for partial read
									if ( lReadLen > 0 )
									{
										sprintf( str, "RECV (%d)%d.%d.%d.%d (%d)(%d/%d)%s partial (%d/%d)\n",
											(int)j,
											PUSH_IPADDR( fj_asockets[j].lIPaddr),
											(int)psmsg->Header.BatchID,
											lBytesRead1 + lBytesRead2,
											lBytesRead2,
											pCommandString,
											lDataLength,
											psmsg->Header.Length);
										fj_writeInfo(str);
										fj_writeInfo(psmsg->Header.Cmd);
										lReadTries++;

										/*
										{
											ULONG l = 0;
											int n = ioctlsocket (pfjSocket->iSocketRW, FIONREAD, &l);
											sprintf (str, "ioctlsocket: %d (len: %d)\n", n, l);
											fj_writeInfo(str);
										}
										*/
									}

									if ( lReadTries > 0 )
									{
										tx_thread_sleep( 10 );
									}
									else
									{
										// recv error. force our data error.
										lBytesRead2 = -1;
									}
								}
								else if ( -1 == lBytesRead )
								{
									STR sError[32];
									LPSTR pErrorStr = NULL;
									iErrorSize = sizeof( int );
									iErrorRet = getsockopt( pfjSocket->iSocketRW, SOL_SOCKET, SO_ERROR, (char *)&iError, &iErrorSize );
									if ( 0 != iErrorRet ) iError = -1;
									else if ( 0 == iErrorSize ) iError = -2;
									if ( EWOULDBLOCK == iError )
									{
										pErrorStr = "EWOULDBLOCK";

										if (lReadTries == 0)
										    fj_reboot ();
									}
									else
									{
										sprintf( sError, "UNKNOWN ERROR %d", iError );
										pErrorStr = sError;
									}
									sprintf( str, "RECV (%d)%d.%d.%d.%d (%d)(%d/%d)%s %s\n",
										(int)j,
										PUSH_IPADDR( fj_asockets[j].lIPaddr),
										(int)psmsg->Header.BatchID,
										lBytesRead1 + lBytesRead2,
										lBytesRead2,
										pCommandString,
										pErrorStr );
									fj_writeInfo(str);
									if ( EWOULDBLOCK == iError )
									{
										tx_thread_sleep( 10 );
									}
									else
									{
										lReadTries = 0;
										// recv error. force our data error.
										lBytesRead2 = -1;
									}
								}
								else
								{
									lBytesRead2 = lBytesRead;
								}
							}
						}

						if ( TRUE == pfph->bInfoSocket )
						{
							LPSTR pStr = (LPSTR)(&(psmsg->Data));
							sprintf( str, "RECV (%d)%d.%d.%d.%d (%d)(%d/%d)%s",
								(int)j,
								PUSH_IPADDR( fj_asockets[j].lIPaddr),
								(int)psmsg->Header.BatchID,
								lBytesRead1 + lBytesRead2,
								lBytesRead2,
								pCommandString );
							fj_writeInfo(str);
										// add a zero to make into a string
							*(pStr + lBytesRead2) = 0;
							fj_writeInfo( pStr );
							fj_writeInfo("\n");
						}

						// we have a command to process
						// let's see if it is from an "allowed" connection
						bAllowed = FALSE;
						// let IPC_ECHO go through to set the "browser" flag, if from our Java Applet
						if ( IPC_ECHO == lCmd ) bAllowed = TRUE;
						// let Net I/O Box commands go through
						if ( FJSYS_INTERNET_HTML_FTP == fj_CVT.pfsys->cInternetControl )
						{
							// if "web page only mode, allow if from Java Applet
							if ( TRUE == pfjSocket->bBrowserApplet ) bAllowed = TRUE;
						}
						else
						{
										// if any other mode, allow TCPIP command
							bAllowed = TRUE;
						}
						if ( TRUE == pfjSocket->bIOBox ) bAllowed = TRUE;
						if ( TRUE == bAllowed )
						{
							if ( lBytesRead2 == lDataLength )
							{
								if ( FALSE == pfjSocket->bIOBox )
									fj_socketRecv( j, (LPBYTE)psmsg );
							}
							else
							{
								// return IPC_INVALID
								psmsg->Header.Cmd = IPC_INVALID;
								// keep the BatchID
										// zero length
								psmsg->Header.Length = 0;
								// let the sender know
								fj_socketSendBuffer( (LPVOID)j, (LPBYTE)psmsg, sizeof(_FJ_SOCKET_MESSAGE_HEADER) );
							}
						}
						else
						{
							fj_socket_IPC_CLOSE( j );
							if ( TRUE == pfph->bInfoSocket )
							{
								sprintf( str, "CLOSED(CONNECTION NOT ALLOWED) (%d) FROM %d.%d.%d.%d\n",(int)j,PUSH_IPADDR(fj_asockets[j].lIPaddr));
								fj_writeInfo( str );
							}
						}
						if ( NULL != pBufferBig )
						{
							if ( lCmd != IPC_FIRMWARE_UPGRADE && lCmd != IPC_GA_UPGRADE )
								fj_free( pBufferBig );
							pBufferBig = NULL;
							psmsg = NULL;
						}
					}
					else
					{
						// clear out the socket buffer
						lBytesRead = recv( pfjSocket->iSocketRW, (char*)(&(psmsg->Data)), _FJ_SOCKET_BUFFER_SIZE, 0 );
						lBytesRead2 = lBytesRead;
						if ( 0 > lBytesRead2 ) lBytesRead2 = 0;
						if ( TRUE == pfph->bInfoSocket )
						{
							sprintf( str, "RECV (%d)%d.%d.%d.%d %s %X %X %X Cleared buffer length = %d\n",              (int)j,
								PUSH_IPADDR( fj_asockets[j].lIPaddr ),
								"Unknown command",
								(int)psmsg->Header.Cmd,
								(int)psmsg->Header.BatchID,
								(int)psmsg->Header.Length,
								(int)lBytesRead2);
							fj_writeInfo(str);
						}
										// return IPC_INVALID
						psmsg->Header.Cmd = IPC_INVALID;
						// keep the BatchID
										// zero length
						psmsg->Header.Length = 0;
						// let the sender know
						if ( FALSE == pfjSocket->bIOBox )
							fj_socketSendBuffer( (LPVOID)j, pBuffer, sizeof(_FJ_SOCKET_MESSAGE_HEADER) );
					}
				}
				// if termination, close our end and wait for another connection
				if ( 0 == lBytesRead )
				{
					                   
					if ( TRUE == pfph->bInfoSocket )
					{
						sprintf( str, "CLOSED(READ TERMINATION) (%d) FROM %d.%d.%d.%d\n",(int)j,PUSH_IPADDR(pfjSocket->lIPaddr));
						fj_writeInfo( str );
					}
				}
				// if error, close our end and wait for another connection
				if ( -1 == lBytesRead )
				{
					iOptionSize = sizeof(iOption);
					lRet = getsockopt( fj_asockets[j].iSocketRW, SOL_SOCKET, SO_ERROR, (char *)&iOption, &iOptionSize );
					if ( 0 != lRet ) iOption = -1;
					else if ( 0 == iOptionSize ) iOption = 0;
					if ( ECONNRESET == iOption )
					{
						fj_socket_IPC_CLOSE( j );
						if ( TRUE == pfph->bInfoSocket )
						{
							sprintf( str, "CLOSED BY PEER (%d) FROM %d.%d.%d.%d\n",(int)j,PUSH_IPADDR(pfjSocket->lIPaddr));
							fj_writeInfo( str );
						}
					}
					else if ( ENOTCONN == iOption )
					{
						fj_socket_IPC_CLOSE( j );
						if ( TRUE == pfph->bInfoSocket )
						{
							sprintf( str, "NOT CONNECTED (%d) FROM %d.%d.%d.%d\n",(int)j,PUSH_IPADDR(pfjSocket->lIPaddr));
							fj_writeInfo( str );
						}
					}
					else if ( !((EWOULDBLOCK == iOption) || (0 == iOption)) )
					{
						fj_socket_IPC_CLOSE( j );
						if ( TRUE == pfph->bInfoSocket )
						{
							sprintf( str, "CLOSED(READ ERROR=%d) (%d) FROM %d.%d.%d.%d\n",(int)iOption,(int)j,PUSH_IPADDR(pfjSocket->lIPaddr));
							fj_writeInfo( str );
						}
					}
				}
			}
			// check if time for "keep alive", if there is a connection
			if ( (TRUE == pfjSocket->bInUse) && (-1 < pfjSocket->iSocketRW) )
			{
				lCheckTime = pGlobal->lSeconds;
				if ( (0 != pfjSocket->lAckTime) &&
					(lCheckTime > (pfjSocket->lAckTime + 30)) )
				{
					if ( FALSE == bForceCloseHere )
					{
						// don't close it.
						// send a keep alive
						fj_print_head_SendStatus();
						fj_asockets[j].lAckTime = pGlobal->lSeconds;
					}
					else
					{
						fj_socket_IPC_CLOSE( j );
						if ( TRUE == pfph->bInfoSocket )
						{
							fj_writeInfo("socket check forced a close socket\n");
						}
					}
					if ( TRUE == bForceCloseJava )
					{
						fj_socketSendOne( j, IPC_CONNECTION_CLOSE, NULL, 0 );
						if ( TRUE == pfph->bInfoSocket )
						{
							fj_writeInfo("socket check forced a close socket at other end\n");
						}
					}
				}
			}
			// check if time to zap socket level for a closed socket
			if ( (-1 == pfjSocket->iSocketRW) && (0 != pfjSocket->lSeconds) )
			{
				if ( (pfjSocket->lSeconds + 5*60) < pGlobal->lSeconds )
				{
					fj_socketZapTabEntry( pfjSocket );
				}
			}
		}
	}
	if ( iSocketListenB > 0 )
	{
		lBytesRead = recvfrom( iSocketListenB, (char*)pBuffer, sizeof(_FJ_SOCKET_MESSAGE_HEADER), 0, &addrLB, &sizesockaddr_in );
		if ( lBytesRead == sizeof(_FJ_SOCKET_MESSAGE_HEADER) )
		{
			psmsg = (_FJ_SOCKET_MESSAGE *)pBuffer;
			if ( IPC_GET_GROUP_INFO == psmsg->Header.Cmd )
			{
				if ( TRUE == pfph->bInfoSocket )
				{
					pCommandString = fj_socketCommandString( psmsg->Header.Cmd );
					sprintf( str, "RECV UDP %d.%d.%d.%d (%d)%s\n",
						PUSH_IPADDR( addrLB.sin_addr.s_addr),
						lBytesRead,
						pCommandString );
					fj_writeInfo(str);
				}
				if ( 0 == fj_CVT.pfph->lGroupNumber )
				{
					fj_SystemGroupToString( fj_CVT.pfph->pfsys, (CHAR *)(&(psmsg->Data)) );
					psmsg->Header.Length = strlen( (CHAR *)(&(psmsg->Data)) );
					psmsg->Header.Cmd = IPC_SET_GROUP_INFO;
					fj_socketSendBufferUDP( addrLB.sin_addr.s_addr, (LPBYTE)psmsg, psmsg->Header.Length + sizeof(_FJ_SOCKET_MESSAGE_HEADER) );
				}
			}
		}
	}
	return;
}
//
// send a buffer out the UDP socket
//
VOID fj_socketSendBufferUDP( LONG ipAddr, LPBYTE pByte, LONG lLength )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	_FJ_SOCKET_MESSAGE *psmsg;
	struct sockaddr_in addr = { AF_INET, _FJ_SOCKET_NUMBER, 0, 0 };
	int    sizesockaddr_in = sizeof( struct sockaddr_in );
	LONG   lRet;
	STRING str[64];

	addr.sin_addr.s_addr = ipAddr;
	lRet = sendto( iSocketListenB,
		(CHAR *)pByte,
		lLength,
		0,
		&addr,
		sizesockaddr_in);
	if ( TRUE == pfph->bInfoSocket )
	{
		psmsg = (_FJ_SOCKET_MESSAGE *)pByte;
		sprintf( str, "SEND UDP %d.%d.%d.%d (%d)%s",
			PUSH_IPADDR( addr.sin_addr.s_addr),
			psmsg->Header.Length + sizeof(_FJ_SOCKET_MESSAGE_HEADER),
			fj_socketCommandString( psmsg->Header.Cmd ) );
		fj_writeInfo(str);
		fj_writeInfo((CHAR *)(&(psmsg->Data)));
		fj_writeInfo("\n");
	}
	return;
}
//
// send a buffer out one socket
//
VOID fj_socketSendBuffer( LPVOID pVoid, LPBYTE pByte, LONG lLength )
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	LONG   lIndex;
	int    iSocket;
	LONG   lSent;
	LONG   lNeed;
	LONG   lRet;
	int    iError;
	int    iErrorSize;
	int    iErrorRet;
	int    iID = 0;
	int    iLengthNet = lLength;
	LPCHAR pData = (LPCHAR)pByte;
	LPCHAR pCommand = "FTP";
	STRING str[64];

	lIndex = (LONG)pVoid;
	lRet = 0;

	if ( lIndex < pGlobal->lSocketCount )
	{
		if ( FALSE == fj_asockets[lIndex].bFTP )
		{
			iID = ((_FJ_SOCKET_MESSAGE_HEADER *)pByte)->BatchID;
			iLengthNet = lLength-sizeof(_FJ_SOCKET_MESSAGE_HEADER);
			pData += sizeof(_FJ_SOCKET_MESSAGE_HEADER);
			pCommand = fj_socketCommandString( ((_FJ_SOCKET_MESSAGE_HEADER *)pByte)->Cmd );
		}

		iSocket = fj_asockets[lIndex].iSocketRW;
		if ( -1 < iSocket )
		{
			lRet  = 0;
			lSent = 0;
			lNeed = lLength;
			while ( (lSent < lNeed) && (lRet >= 0) )
			{
				iErrorSize = sizeof( int );
				iErrorRet = getsockopt( iSocket, SOL_SOCKET, SO_ERROR, (char *)&iError, &iErrorSize );
				lRet = send( iSocket, (char *)(pByte+lSent), (int)(lNeed-lSent), 0);

				if ( lSent != 0 )
				{
					str[0] = 0;
				}
				if ( -1 == lRet )
				{
					iErrorSize = sizeof( int );
					iErrorRet = getsockopt( iSocket, SOL_SOCKET, SO_ERROR, (char *)&iError, &iErrorSize );
					if ( 0 != iErrorRet ) iError = -1;
					else if ( 0 == iErrorSize ) iError = -2;
										// use the next test to sleep for a brief pause
					if ( EWOULDBLOCK == iError ) lRet = 0;
				}
				if ( 0 == lRet )
				{
					tx_thread_sleep( 10 );
				}
				else if ( 0 < lRet )
				{
					lSent += lRet;
					if ( lSent < lNeed )
					{
						if ( TRUE == pfph->bInfoSocket )
						{
							sprintf( str, "SENT (%d)%d.%d.%d.%d (%d)(%d) partial\n",
								(int)lIndex,
								PUSH_IPADDR(fj_asockets[lIndex].lIPaddr),
								iID,
								(int)lRet );
							fj_writeInfo(str);
						}
					}
				}
			}

			if ( TRUE == pfph->bInfoSocket )
			{
				sprintf( str, "SENT (%d)%d.%d.%d.%d (%d)(%d/%d)%s",
					(int)lIndex,
					PUSH_IPADDR(fj_asockets[lIndex].lIPaddr),
					iID,
					(int)lLength,
					iLengthNet,
					pCommand );
				fj_writeInfo(str);
				*(pByte+lLength) = 0;
				fj_writeInfo(pData);
				fj_writeInfo("\n");
			}

			// reset time out
			fj_asockets[lIndex].lAckTime = fj_CVT.pFJglobal->lSeconds;
			// if error, close our end and wait for another connection
			if ( -1 == lRet )
			{
				if ( TRUE == pfph->bInfoSocket )
				{
					if ( ECONNRESET == iError )
					{
						fj_socket_IPC_CLOSE( lIndex );
						sprintf( str, "CLOSED BY PEER (%d) FROM %d.%d.%d.%d\n",(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
						fj_writeInfo( str );
					}
					else if ( ENOTCONN == iError )
					{
						fj_socket_IPC_CLOSE( lIndex );
						sprintf( str, "CLOSED(NOT CONNECTED) (%d) FROM %d.%d.%d.%d\n",(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
						fj_writeInfo( str );
					}
					else if ( !((EWOULDBLOCK == iError) || (0 == iError)) )
					{
						fj_socket_IPC_CLOSE( lIndex );
						sprintf( str, "CLOSED(SEND ERROR=%d) (%d) FROM %d.%d.%d.%d\n",(int)iError,(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
						fj_writeInfo( str );
					}
				}
			}
		}
	}

	return;
}
//
// send a message out one socket
//
VOID fj_socketSendOne( LONG lIndex, IPSockCmds cmd, LPBYTE pByte, LONG lLength )
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	_FJ_SOCKET_MESSAGE *psmsg;

	if ( TRUE == pGlobal->bInitComplete )
	{
		if ( lIndex < pGlobal->lSocketCount )
		{
			if ( -1 < fj_asockets[lIndex].iSocketRW )
			{
				fj_asockets[lIndex].lBatchIDSend++;

				psmsg = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lLength + 1);
				psmsg->Header.Cmd     = cmd;
				psmsg->Header.BatchID = fj_asockets[lIndex].lBatchIDSend;

				if ( TRUE == fj_asockets[lIndex].bIOBox )
					psmsg->Header.Length  = 1;
				else
					psmsg->Header.Length  = lLength;

				memcpy( psmsg->Data, pByte, lLength);

				fj_socketSendBuffer( (LPVOID)lIndex, (LPBYTE)psmsg, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lLength );
				fj_free( psmsg );
			}
		}
	}

	return;
}
//
// send a message out all sockets
//
VOID fj_socketSendAll( IPSockCmds cmd, LPBYTE pbyte, LONG lLength )
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	BOOL   bSent;
	int    j;
	STRING str[64];

	bSent = FALSE;
	if ( TRUE == pGlobal->bInitComplete && TRUE == pGlobal->bStatusReport )
	{
		for ( j = 0; j < pGlobal->lSocketCount; j++ )
		{
			if ( (FALSE == fj_asockets[j].bFTP) &&
				(FALSE == fj_asockets[j].bDebugger) &&
				(FALSE == fj_asockets[j].bIOBox) &&
				(-1 < fj_asockets[j].iSocketRW) )
			{
				fj_socketSendOne( j, cmd, pbyte, lLength );
				bSent = TRUE;
			}
		}
	}

	if ( FALSE == bSent )
	{
		if ( TRUE == pfph->bInfoSocket )
		{
			sprintf( str, "SendAll-No Socket:%s ", fj_socketCommandString(cmd) );
			fj_writeInfo(str);
			fj_writeInfo((LPSTR)pbyte);
			fj_writeInfo("\n");
		}
	}

	return;
}
//
// send a string out all debug sockets
//
VOID fj_socketSendAllDebug( LPSTR pStr )
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	LPBYTE pByte;
	LONG   lIndex;
	int    iSocket;
	LONG   lLength;
	LONG   lSent;
	LONG   lNeed;
	LONG   lRet;
	int    iError;
	int    iErrorSize;
	int    iErrorRet;
	STRING str[64];

	lLength = strlen( pStr );
	pByte = (LPBYTE)pStr;
	if ( TRUE == pGlobal->bInitComplete )
	{
		for ( lIndex = 0; lIndex < pGlobal->lSocketCount; lIndex++ )
		{
			if ( (TRUE == fj_asockets[lIndex].bDebugger) &&
				(-1 < fj_asockets[lIndex].iSocketRW) )
			{
				iSocket = fj_asockets[lIndex].iSocketRW;
				if ( -1 < iSocket )
				{
					lRet  = 0;
					lSent = 0;
					lNeed = lLength;
					while ( (lSent < lNeed) && (lRet >= 0) )
					{
						iErrorSize = sizeof( int );
						iErrorRet = getsockopt( iSocket, SOL_SOCKET, SO_ERROR, (char *)&iError, &iErrorSize );
						lRet = send( iSocket, (char *)(pByte+lSent), (int)(lNeed-lSent), 0);

						if ( -1 == lRet )
						{
							iErrorSize = sizeof( int );
							iErrorRet = getsockopt( iSocket, SOL_SOCKET, SO_ERROR, (char *)&iError, &iErrorSize );
							if ( 0 != iErrorRet ) iError = -1;
							else if ( 0 == iErrorSize ) iError = -2;
							// use the next test to sleep for a brief pause
							if ( EWOULDBLOCK == iError ) lRet = 0;
						}
						if ( 0 == lRet )
						{
							tx_thread_sleep( 10 );
						}
						else if ( 0 < lRet )
						{
							lSent += lRet;
						}
					}

					// reset time out
					fj_asockets[lIndex].lAckTime = fj_CVT.pFJglobal->lSeconds;
					// if error, close our end and wait for another connection
					if ( -1 == lRet )
					{
						if ( TRUE == pfph->bInfoSocket )
						{
							if ( ECONNRESET == iError )
							{
								fj_socket_IPC_CLOSE( lIndex );
								sprintf( str, "CLOSED BY PEER (%d) FROM %d.%d.%d.%d\n",(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
								fj_writeInfo( str );
							}
							else if ( ENOTCONN == iError )
							{
								fj_socket_IPC_CLOSE( lIndex );
								sprintf( str, "CLOSED(NOT CONNECTED) (%d) FROM %d.%d.%d.%d\n",(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
								fj_writeInfo( str );
							}
							else if ( !((EWOULDBLOCK == iError) || (0 == iError)) )
							{
								fj_socket_IPC_CLOSE( lIndex );
								sprintf( str, "CLOSED(SEND ERROR=%d) (%d) FROM %d.%d.%d.%d\n",(int)iError,(int)lIndex,PUSH_IPADDR(fj_asockets[lIndex].lIPaddr));
								fj_writeInfo( str );
							}
						}
					}
				}

			}
		}
	}

	return;
}
