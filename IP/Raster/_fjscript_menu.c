/* C file: c:\MyPrograms\FoxJet1\_fjscript_menu.c created
 by NET+Works HTML to C Utility on Thursday, July 26, 2001 11:09:06 */
#include "fj.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_MENU_PRINTHEAD */
static void na_MENU_PRINTHEAD (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\fjscript_menu.htm {{ */
void Send_function_10(unsigned long handle)
{
	HSSend (handle, "var aTag = \"onMouseOver=menuOver(this) onMouseOut=menuOut(this)\";\n");
	HSSend (handle, "");
	na_MENU_PRINTHEAD (handle);
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\fjscript_menu.htm }} */
/* @! End HTML page   */
char * strMenuTable[] =
{
	"ph",      "Printer",              "printhead.htm",     "null",   "null", 
	"st",      "Statistics",           "statistics.htm",    "null",   "null",
	//"cf",      "Configuration",        "null",              "null",   "\"cfptr,cfsysin,cfsystm,cfeth,cfpw,cfdis,cfmaint,cfman,\"",
	"cf",      "Configuration",        "null",              "null",   "\"cfptr,cfsysin,cfsystm,cfeth,cfpw,cfman,cfmaint,\"",
	"cfptr",   "Printer",              "printerconfig.htm", "\"cf\"", "null",
	//	"cfsys",   "System&nbsp;Layout",   "systemconfig.htm",  "\"cf\"", "null",
	"cfsysin", "System&nbsp;Info",     "systeminfo.htm",    "\"cf\"", "null",
	"cfsystm", "System&nbsp;Codes",    "time.htm",          "\"cf\"", "null",
	"cfeth",   "System&nbsp;Internet", "ethernet.htm",      "\"cf\"", "null",
	"cfpw",    "System&nbsp;Security", "password.htm",      "\"cf\"", "null",

	"cfman",   "Manufacturing",        "manufacturing.htm", "\"cf\"", "null",
	//"cfdis",   "Distributor",          "distributor.htm", 	"\"cf\"", "null",
	"cfmaint",   "Maintenance",        "maintenance.htm", 	"\"cf\"", "null",
	//"cftest",  "Test",                 "test.htm",          "\"cf\"", "null",

	"mf",      "User Data",       "null",              "null",   "\"mflbl,mffont,mfbmp,mfvard,\"",
	//        "mf",      "Labels & Fonts",       "null",              "null",   "\"mflbl,mfmsg,mffont,mfbmp,\"",
	"mflbl",   "Labels",               "label.htm",         "\"mf\"", "null",
	//	"mfmsg",   "Messages",             "message.htm",       "\"mf\"", "null",
	"mffont",  "Fonts",                "font.htm",          "\"mf\"", "null",
	"mfbmp",   "Bitmaps",              "bitmap.htm",        "\"mf\"", "null",
	"mfvard",   "Variable Data",       "vardata.htm",       "\"mf\"", "null"
};
void menu_write_one( unsigned long handle, int i )
{
	i *= 5;
	HSSend (handle, "writeElt( \"");
	HSSend (handle, strMenuTable[i+0] );
	HSSend (handle, "\", \"");
	HSSend (handle, strMenuTable[i+0] );
	HSSend (handle, "\",false,false,false,false,false,\"hidden\",false,false,false,false,false,false,\"fjmenu\",aTag );\n");

	HSSend (handle, "objElt = getElt( \"");
	HSSend (handle, strMenuTable[i+0] );
	HSSend (handle, "\" );\n");

	HSSend (handle, "objElt.fjText = \"");
	HSSend (handle, strMenuTable[i+1] );
	HSSend (handle, "\";\n");

	HSSend (handle, "objElt.fjLinkHref = ");
	if ( 0 == strcmp("null",strMenuTable[i+2]) )
	{
		HSSend (handle, strMenuTable[i+2] );
	}
	else
	{
		HSSend (handle, "\"");
		HSSend (handle, strMenuTable[i+2] );
		HSSend (handle, "?pwid=\"+pwid_js");
	}
	HSSend (handle, ";\n");

	HSSend (handle, "objElt.fjMainmenu = ");
	HSSend (handle, strMenuTable[i+3] );
	HSSend (handle, ";\n");

	HSSend (handle, "objElt.fjSubmenus = ");
	HSSend (handle, strMenuTable[i+4] );
	HSSend (handle, ";\n");
}
/* @! Begin of dynamic functions */
/* @! Function: na_MENU_PRINTHEAD  {{ */
void na_MENU_PRINTHEAD(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < sizeof(strMenuTable)/(5*sizeof(char *)); i++ )
	{
		menu_write_one( handle,  i );
	}
}										/* @! Function: na_MENU_PRINTHEAD  }} */
/* @! End of dynamic functions   */
