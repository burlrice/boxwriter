#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <time.h>
#endif
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_datetime.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"

// used for MemStor identification
const  CHAR fj_DatetimeID[]   = "Datetime";

//
//  FUNCTION: fj_DatetimeNew()
//
//  PURPOSE:  Create a new FJDATETIME structure
//
FJDLLExport LPFJDATETIME fj_DatetimeNew( VOID )
{
	LPFJDATETIME pfdt;

	pfdt = (LPFJDATETIME)fj_calloc( 1, sizeof (FJDATETIME) );

	pfdt->lBoldValue     =  0;			// amount of boldness. 0 origin.
	pfdt->lWidthValue    =  1;			// amount of width stretch. 1 origin.
	pfdt->lGapValue      =  0;			// amount of gap. 0 = vertical

										// format of the date/time
	pfdt->lFormat        = DT_TIME_HHMMSS24;
										// which date/time
	pfdt->lTimeType      = DT_TIME_NORMAL;
	pfdt->lLength        =  0;			// length of string output. 0 = 'natural' ( no alteration )
	pfdt->cAlign         = 'R';			// alignment of value in larger length - L/R
	pfdt->cFill          =  0;			// fill character. 0 is no fill.
	pfdt->cDelim         =  ':';		// delimiter to use between date parts
	pfdt->bStringNormal  =  TRUE;		// which set of strings to use for alphabetic formats
	pfdt->lHourACount    = 24;			// number of time periods in 24 hours

	pfdt->strFontName[0] = 0;			// font name
	pfdt->pfeText        = NULL;		// element for text generation
	return( pfdt );
}
//
//  FUNCTION: fj_DatetimeDestroy()
//
//  PURPOSE:  Destroy a FJDATETIME structure
//
FJDLLExport VOID fj_DatetimeDestroy( LPFJELEMENT pfe )
{
	LPFJDATETIME pfdt;
	LPFJELEMENT  pfeText;

	pfdt = (LPFJDATETIME)pfe->pDesc;
	if (NULL != pfdt)
	{
		pfeText = pfdt->pfeText;
		if ( NULL != pfeText )
		{
			fj_ElementDestroy( pfeText );
		}
		fj_free( pfdt );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_DatetimeDup()
//
//  PURPOSE:  Duplicate a FJDATETIME structure
//
FJDLLExport LPFJDATETIME fj_DatetimeDup( CLPCFJDATETIME pfdtIn )
{
	LPFJDATETIME pfdtDup;
	LPFJELEMENT  pfeText;

	pfdtDup = (LPFJDATETIME)fj_malloc( sizeof(FJDATETIME) );
	memcpy( pfdtDup, pfdtIn, sizeof(FJDATETIME) );

	pfeText = pfdtIn->pfeText;
	if ( NULL != pfeText )
	{
		pfdtDup->pfeText = fj_ElementDup( pfeText );
		pfdtDup->pfeText->pfm = pfdtIn->pfeText->pfm;
	}

	return( pfdtDup );
}

/*
static long fj_DateDiff (const struct tm * ptm1, const struct tm * ptm1)
{
	long n1 = 0;
	long n2 = 0;

	if (ptm1 && ptm2) {
		n1 = ((ptm1->tm_year + 1900) * 12) + ptm1->tm_mon;
		n2 = ((ptm2->tm_year + 1900) * 12) + ptm2->tm_mon;
	}

	return n1 - n2;
}
*/

VOID fj_DatetimeCreateImage1( CLPFJELEMENT pfe )
{
	LPFJDATETIME pfdt;
	LPFJELEMENT  pfeText;
	LPFJTEXT     pft;

	pfdt = (LPFJDATETIME)pfe->pDesc;

	// pre-process
	// get what we need to build FJIMAGE

	{

		LPFJSYSTEM  pfsys;
		struct tm  mtm;
		LONG  lTimeType;
		LONG  lFormat;

		STR   strYear[5];
		STR   strMonth[3];
		STR   strDay[3];
		STR   strHour[3];
		STR   strMinute[3];
		STR   strSecond[3];
		STR   strDelim[2];
		STR   strTemp[1+FJTEXT_SIZE];
		LPSTR pTemp;
		LPSTR pOutput;
		LPSTR pShiftCode;
		LPSTR pListEntry;

		LONG  lYear;
		LONG  lHour;
		LONG  lDayWk;
		LONG  lHourCount;
		int i;

		pfeText = pfdt->pfeText;
		pfsys = pfe->pfm->pfph->pfsys;

		if ( NULL == pfeText )
		{
			pfeText = fj_ElementTextNew();
			pfeText->pfm = pfe->pfm;
			pfdt->pfeText = pfeText;
		}
		pft = (LPFJTEXT)pfeText->pDesc;
		pTemp   = strTemp;
		pOutput = pft->strText;

		strcpy( pTemp, "DateTime" );	// default print if code is not found

		lTimeType   = pfdt->lTimeType;
		lFormat     = pfdt->lFormat;
		strDelim[0] = pfdt->cDelim;
		strDelim[1] = 0;
		// early test to run through all the formats
		//pfdt->lFormat++;
		//if( pfdt->lFormat > (DT_TIME_LAST+1)) pfdt->lFormat=0;
		//if( pfdt->lFormat == (DT_DATE_LAST+1)) pfdt->lFormat=DT_TIME_FIRST;

		memcpy( &mtm, &(pfsys->tmPhotoCell), sizeof(struct tm) );

		if ( (DT_TIME_ROLLOVER   == lTimeType) || (DT_TIME_ROLL_EXP == lTimeType) )
		{
										// if a date
			if ( lFormat <= DT_DATE_LAST )
			{
				// adjust for rollover time
				mtm.tm_min += (LONG)(-pfsys->fRolloverHours * 60.);
				fj_mktime( &mtm );		// normalize the incremented value from rollover
			}
		}

		if (lTimeType == DT_TIME_ROLLOVER) {
			LPFJLABEL pfl = pfe->pfm->pfl;

			if (pfl && pfl->bExpRoundDown) {
				switch (pfl->lExpUnits) {
				case FJ_LABEL_EXP_DAYS:
					{
						mtm.tm_hour = 0;
						mtm.tm_min  = 0;
						mtm.tm_sec  = 0;
					}
					break;
				case FJ_LABEL_EXP_WEEKS:
					{
						int nFDOW = pfsys->lWeekDay - 1;
						int nCurrentDOW = 0;
						int nCount = 0;

						while ((nCurrentDOW = mtm.tm_wday) != (nFDOW % 7)) {
							mtm.tm_mday -= 1;
							fj_mktime (&mtm);
						
							if (nCount++ > 10)
								break; // just to make sure we never bomb
						}

						{ char sz [128]; strftime (sz, 128, "%c [%A]\n", &mtm); OutputDebugString (sz); }
					}
					break;
				case FJ_LABEL_EXP_MONTHS:
					{
						mtm.tm_mday = 1;
						fj_mktime (&mtm);
					}
					break;
				case FJ_LABEL_EXP_YEARS:
					{
						mtm.tm_mon = 0;
						mtm.tm_mday = 1;
						fj_mktime (&mtm);
					}
					break;
				}
			}
		}

		if ( (DT_TIME_EXPIRATION == lTimeType) || (DT_TIME_ROLL_EXP == lTimeType) )
		{
			LPFJLABEL pfl = pfe->pfm->pfl;
			// skip this if there is no Label
			if ( NULL != pfl )
			{
				if      ( FJ_LABEL_EXP_DAYS   == pfl->lExpUnits )
				{
					mtm.tm_mday += pfl->lExpValue;
					if ( TRUE == pfl->bExpRoundUp )
					{
						mtm.tm_hour = 23;
						mtm.tm_min  = 59;
						mtm.tm_sec  = 59;
					}
					else if (pfl->bExpRoundDown == TRUE) {
						mtm.tm_hour = 0;
						mtm.tm_min  = 0;
						mtm.tm_sec  = 0;
					}
				}
				else if ( FJ_LABEL_EXP_WEEKS  == pfl->lExpUnits )
				{
					mtm.tm_mday += 7 * pfl->lExpValue;
					if ( TRUE == pfl->bExpRoundUp )
					{
										// (last day of week = first day of week - 1 ) - 1 for origin difference between two fields
						i = pfsys->lWeekDay - 2;
										// minus current day of week = days to end of week
						i -= mtm.tm_wday;
										// if negative, add a week
						if ( i < 0 ) i += 7;
						mtm.tm_mday += i;
						mtm.tm_hour = 23;
						mtm.tm_min  = 59;
						mtm.tm_sec  = 59;
					}
					else if (pfl->bExpRoundDown == TRUE) {
						int nFDOW = pfsys->lWeekDay - 1;
						int nCurrentDOW = 0;
						int nCount = 0;

						if (nFDOW == 0) {
							mtm.tm_mday -= 1;
							fj_mktime (&mtm);
						}

						nCurrentDOW = mtm.tm_wday;

						while ((nCurrentDOW = mtm.tm_wday) != (nFDOW % 7)) {
							mtm.tm_mday -= 1;
							fj_mktime (&mtm);
						
							if (nCount++ > 10)
								break; // just to make sure we never bomb
						}
					}
				}
				else if ( FJ_LABEL_EXP_MONTHS == pfl->lExpUnits )
				{
					mtm.tm_mon  += pfl->lExpValue;
					if ( TRUE == pfl->bExpRoundUp )
					{
										// set time to midddle of day so that any DST transition will not change the date
						mtm.tm_hour = 12;
						mtm.tm_mon++;	// to round up a month, add a month,
										// normalize,
						fj_mktime( &mtm );
						mtm.tm_mday = 0;// set day to 0th of the month,
										// normalize again
						fj_mktime( &mtm );
						mtm.tm_hour = 23;
						mtm.tm_min  = 59;
						mtm.tm_sec  = 59;
					}
					else if (pfl->bExpRoundDown == TRUE) {
						mtm.tm_mday = 1;
						fj_mktime (&mtm);
					}
				}
				else if ( FJ_LABEL_EXP_YEARS  == pfl->lExpUnits )
				{
					mtm.tm_year += pfl->lExpValue;
					if ( TRUE == pfl->bExpRoundUp )
					{
										// set time to midddle of day so that any DST transition will not change the date
						mtm.tm_hour = 12;
						mtm.tm_year++;	// to round up a year, add a year,
						mtm.tm_mday = 0;// set day to 0th,
						mtm.tm_mon = 0;	// set month to January,
										// normalize again
						fj_mktime( &mtm );
						mtm.tm_hour = 23;
						mtm.tm_min  = 59;
						mtm.tm_sec  = 59;
					}
					else if (pfl->bExpRoundDown == TRUE) {
						mtm.tm_mon = 0;
						mtm.tm_mday = 1;
						fj_mktime (&mtm);
					}
				}
				// normalize the incremented values from expiration.
				// but if the new-to-old time crosses a DST boundary, we need to preserve the hour.
				// this will also prevent a rollup time of 23:59:59 from going into the next day.
				lHour = mtm.tm_hour;
				mtm.tm_hour = 20;		// an hour not in any DST transition logic
				fj_mktime( &mtm );
				mtm.tm_hour = lHour;
			}
		}

		// date format ???
		if ( (DT_DATE_FIRST <= lFormat) && (lFormat <= DT_DATE_LAST) )
		{
			lYear = mtm.tm_year + 1900;
			if ( DT_DATE_YEAR2 <= lFormat )
			{
				lYear = lYear%100;
			}
			sprintf( strYear,  "%.2d", lYear );
			sprintf( strMonth, "%.2d", mtm.tm_mon+1 );
			sprintf( strDay,   "%.2d", mtm.tm_mday );

			if      ( (DT_DATE_YYYYMMDD == lFormat) || (DT_DATE_YYMMDD == lFormat) )
			{
				strcpy( pTemp, strYear  );
				strcat( pTemp, strDelim );
				strcat( pTemp, strMonth );
				strcat( pTemp, strDelim );
				strcat( pTemp, strDay   );
			}
			else if ( (DT_DATE_MMDDYYYY == lFormat) || (DT_DATE_MMDDYY == lFormat) )
			{
				strcpy( pTemp, strMonth );
				strcat( pTemp, strDelim );
				strcat( pTemp, strDay   );
				strcat( pTemp, strDelim );
				strcat( pTemp, strYear  );
			}
			else if ( (DT_DATE_DDMMYYYY == lFormat) || (DT_DATE_DDMMYY == lFormat) )
			{
				strcpy( pTemp, strDay   );
				strcat( pTemp, strDelim );
				strcat( pTemp, strMonth );
				strcat( pTemp, strDelim );
				strcat( pTemp, strYear  );
			}
			else if ( (DT_DATE_YYYY == lFormat) || (DT_DATE_YY == lFormat) )
			{
				strcpy( pTemp, strYear );
			}
			else if ( DT_DATE_MM == lFormat )
			{
				strcpy( pTemp, strMonth );
			}
			else if ( DT_DATE_DD == lFormat )
			{
				strcpy( pTemp, strDay );
			}
			else if ( DT_DATE_DAYWK == lFormat )
			{
				// tm_wday is 0 origin. lWeekDay is 1 origin. result is 1 origin.
				lDayWk = mtm.tm_wday - pfsys->lWeekDay + 2;
				if ( lDayWk < 1 ) lDayWk += 7;
				sprintf( pTemp, "%d", lDayWk);
			}
			else if ( DT_DATE_JULIAN == lFormat )
			{
				mtm.tm_yday++;			// adjust for 0 origin
				sprintf( pTemp, "%d", mtm.tm_yday );
			}
			else if ( DT_DATE_MONTHA == lFormat )
			{
				if ( TRUE == pfdt->bStringNormal ) pListEntry = pfsys->aMonthN[mtm.tm_mon];
				else                               pListEntry = pfsys->aMonthS[mtm.tm_mon];
				strcpy( pTemp, pListEntry );
			}
			else if ( DT_DATE_DAYWKA == lFormat )
			{
				if ( TRUE == pfdt->bStringNormal ) pListEntry = pfsys->aDayN[mtm.tm_wday];
				else                               pListEntry = pfsys->aDayS[mtm.tm_wday];
				strcpy( pTemp, pListEntry );
			}
			else if ( DT_DATE_WK == lFormat )
			{
				int wDay1;				// wDay for Jan 1
				int yDay;				// adjusted yDay
				int iWeek1;				// week of year

				wDay1 = mtm.tm_wday - (mtm.tm_yday-(mtm.tm_yday/7)*7);
				if ( wDay1 < 0 ) wDay1 += 7;
										// yDay adjusted for Jan 1 wDay
				yDay = mtm.tm_yday + wDay1;
										// yDay adjusted for first day of week
				yDay += 8 - pfsys->lWeekDay;

										// comparator for days in first week test
				wDay1 += pfsys->lWeek1 -1;

				// yDay adjusted for days in first week
				if      ( wDay1 <  (pfsys->lWeekDay-1  ) ) yDay += 7;
				// yDay adjusted for days in first week
				else if ( wDay1 >= (pfsys->lWeekDay-1+7) ) yDay -= 7;

				iWeek1 = yDay / 7;
				if ( 0 == iWeek1 )
				{
					// do this again, but first change time to last day of previous year.
					mtm.tm_sec  = -1;
					mtm.tm_min  = -1;
					mtm.tm_hour = -1;
					mtm.tm_mday = 1;
					mtm.tm_mon  = 0;
					fj_mktime( &mtm );

					wDay1 = mtm.tm_wday - (mtm.tm_yday-(mtm.tm_yday/7)*7);
					if ( wDay1 < 0 ) wDay1 += 7;
										// yDay adjusted for Jan 1 wDay
					yDay = mtm.tm_yday + wDay1;
										// yDay adjusted for first day of week
					yDay += 8 - pfsys->lWeekDay;

										// comparator for days in first week test
					wDay1 += pfsys->lWeek1 -1;

					// yDay adjusted for days in first week
					if      ( wDay1 <  (pfsys->lWeekDay-1  ) ) yDay += 7;
					// yDay adjusted for days in first week
					else if ( wDay1 >= (pfsys->lWeekDay-1+7) ) yDay -= 7;

					iWeek1 = yDay / 7;

				}
				if ( 53 == iWeek1 )
				{
										// value for 53rd week
					iWeek1 = pfsys->lWeek53;
				}
				sprintf (pTemp, "%d", iWeek1 );
			}
		}
		// time format ???
		else if ( (DT_TIME_FIRST <= lFormat) && (lFormat <= DT_TIME_LAST) )
		{
			lHour = mtm.tm_hour;
			if ( (DT_TIME_HHMMSS12 == lFormat) || (DT_TIME_HHMMSS12M == lFormat) || (DT_TIME_HHMM12 == lFormat) || (DT_TIME_HHMM12M == lFormat) )
			{
				if ( lHour >  12 ) lHour -= 12;
				if ( lHour ==  0 ) lHour  = 12;
			}
			sprintf( strHour,   "%.2d", lHour );
			sprintf( strMinute, "%.2d", mtm.tm_min );
			sprintf( strSecond, "%.2d", mtm.tm_sec );

			if ( lFormat <= DT_TIME_HHMMLAST )
			{
				strcpy( pTemp, strHour   );
				strcat( pTemp, strDelim  );
				strcat( pTemp, strMinute );
				if ( (DT_TIME_HHMMSS24 == lFormat) || (DT_TIME_HHMMSS12 == lFormat) || (DT_TIME_HHMMSS12M == lFormat) )
				{
					strcat( pTemp, strDelim  );
					strcat( pTemp, strSecond );
				}
				if ( (DT_TIME_HHMMSS12M == lFormat) || (DT_TIME_HHMM12M == lFormat) )
				{
					if ( mtm.tm_hour < 12 )
					{
						strcat( pTemp, pfsys->aAMPM[0] );
					}
					else
					{
						strcat( pTemp, pfsys->aAMPM[1] );
					}
				}
			}
			else if ( DT_TIME_HH == lFormat ) strcpy( pTemp, strHour   );
			else if ( DT_TIME_MM == lFormat ) strcpy( pTemp, strMinute );
			else if ( DT_TIME_SS == lFormat ) strcpy( pTemp, strSecond );
			else if ( DT_TIME_AMPM == lFormat )
			{
				if ( mtm.tm_hour < 12 )
				{
					strcpy( pTemp, pfsys->aAMPM[0] );
				}
				else
				{
					strcpy( pTemp, pfsys->aAMPM[1] );
				}
			}
			else if ( DT_TIME_HOURA == lFormat )
			{
				lHourCount = (mtm.tm_hour * 60 + mtm.tm_min) / (24 * 60 / pfdt->lHourACount);
				pListEntry = pfsys->aHour[lHourCount];
				strcpy( pTemp, pListEntry );
			}
			else if ( DT_TIME_SHIFT == lFormat )
			{
										// default shift code
				pShiftCode = pfsys->sShiftCode1;
				i = mtm.tm_min + mtm.tm_hour * 60;
										// 3 shifts
				if ( 0 != pfsys->lShift3 )
				{
					if      ( (i >= pfsys->lShift3) || (i < pfsys->lShift1) ) pShiftCode = pfsys->sShiftCode3;
					else if ( (i >= pfsys->lShift2) && (i < pfsys->lShift3) ) pShiftCode = pfsys->sShiftCode2;
				}
				else
				{
					if      ( (i >= pfsys->lShift2) || (i < pfsys->lShift1) ) pShiftCode = pfsys->sShiftCode2;
				}
				strcpy( pTemp, pShiftCode );
			}
		}

		strcpy( pOutput, pTemp );

		if ( 0 != pfdt->lLength )
		{
			int strLen;

			strLen = strlen( pTemp );
			if ( strLen > pfdt->lLength )
			{
				switch( lFormat )
				{
					// numeric values keep the low/right characters
					case DT_DATE_YYYY:
					case DT_DATE_MM:
					case DT_DATE_DD:
					case DT_DATE_YY:
					case DT_DATE_DAYWK:
					case DT_DATE_WK:
					case DT_DATE_JULIAN:
					case DT_TIME_HH:
					case DT_TIME_MM:
					case DT_TIME_SS:
						pTemp += ( strLen - pfdt->lLength );
						break;

						// alphabetic values keep the high/left characters
					case DT_TIME_AMPM:
					case DT_DATE_MONTHA:
					case DT_DATE_DAYWKA:
					case DT_TIME_HOURA:
					case DT_TIME_SHIFT:
						*(pTemp+pfdt->lLength) = 0;
						break;

						// no length adjust is made to these formats. they are left 'natural'.
					case DT_DATE_YYYYMMDD:
					case DT_DATE_DDMMYYYY:
					case DT_DATE_MMDDYYYY:
					case DT_DATE_YYMMDD:
					case DT_DATE_DDMMYY:
					case DT_DATE_MMDDYY:
					case DT_TIME_HHMMSS24:
					case DT_TIME_HHMMSS12:
					case DT_TIME_HHMMSS12M:
					case DT_TIME_HHMM24:
					case DT_TIME_HHMM12:
					case DT_TIME_HHMM12M:
					default:
						break;
				}
			}
			strLen = strlen( pTemp );
			if ( strLen < pfdt->lLength )
			{
				if ( 0 != pfdt->cFill )
				{
					for ( i = 0; i < pfdt->lLength; i++ )
					{
						pOutput[i] = pfdt->cFill;
					}
					pOutput[i] = 0;
					if ( 'R' == pfdt->cAlign ) pOutput += (pfdt->lLength - strLen);
					for ( i = 0; i < strLen; i++ )
					{
						pOutput[i] = pTemp[i];
					}
				}
			}
			else
			{
				strcpy( pOutput, pTemp );
			}
		}
	}

	return;
}
VOID fj_DatetimeCreateImage2( CLPFJELEMENT pfe )
{
	LPFJDATETIME pfdt;
	LPFJELEMENT  pfeText;
	LPFJTEXT     pft;

	pfdt = (LPFJDATETIME)pfe->pDesc;

	// build FJIMAGE

	pfeText = pfdt->pfeText;
	pft = (LPFJTEXT)pfeText->pDesc;

	pft->lBoldValue  = pfdt->lBoldValue;
	pft->lWidthValue = pfdt->lWidthValue;
	pft->lGapValue   = pfdt->lGapValue;
	strcpy( pft->strFontName, pfdt->strFontName );

	// let text routines create rasters, do bold and width, etc.
	pfeText->pfi = pfe->pfi;
	pfe->pfi = NULL;
	pfeText->pActions->fj_DescCreateImage( pfeText );
	pfe->pfi = pfeText->pfi;
	pfeText->pfi = NULL;

	return;
}
FJDLLExport VOID fj_DatetimeCreateImage( CLPFJELEMENT pfe )
{
	fj_DatetimeCreateImage1( pfe );
	fj_DatetimeCreateImage2( pfe );
	return;
}
//
//   make new Datetime string
//   make new rasters if changed
//
FJDLLExport VOID fj_DatetimePhotocellBuild( CLPFJELEMENT pfe )
{
	CLPCFJDATETIME pfdt    = (CLPCFJDATETIME)pfe->pDesc;
	LPFJELEMENT    pfeText = pfdt->pfeText;
	LPFJTEXT       pft     = (LPFJTEXT)pfeText->pDesc;
	STR  str[100];

	if ( 100 > strlen(pft->strText))
	{
		strcpy( str, pft->strText );
		fj_DatetimeCreateImage1( pfe );
		if ( 0 != strcmp(str, pft->strText) ) fj_DatetimeCreateImage2( pfe );
	}
	return;
}
//
//  FUNCTION: fj_DatetimeToStr()
//
//  PURPOSE:  Convert a FJDATETIME structure into a descriptive string
//
FJDLLExport VOID fj_DatetimeToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJDATETIME pfdt = (CLPCFJDATETIME)pfe->pDesc;
	STR sName[FJDATETIME_NAME_SIZE*2];
	STR sFill[3];
	STR sFillQuote[5];
	STR sDelim[3];
	STR sDelimQuote[5];

										// text element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	sFill[0]  = pfdt->cFill;
	sFill[1]  = 0;
	sDelim[0] = pfdt->cDelim;
	sDelim[1] = 0;
										// fill character
	fj_processParameterStringOutput( sFillQuote,  sFill  );
										// delimiter character
	fj_processParameterStringOutput( sDelimQuote, sDelim );
	if (NULL != pStr) sprintf (pStr,"{%s,%s,%f,%d,%c,%d,%d,%d,%c,%c,%c,%d,%d,%d,%c,%s,%s,%c,%d,",
			fj_DatetimeID,
			sName,						// text element Name
			pfe->fLeft,					// distance from left edge of box
			pfe->lRow,					// row number to start pixels - 0 = bottom of print head
			(CHAR)pfe->lPosition,		// type of positioning
			pfdt->lBoldValue,			// amount of boldness. 0 origin.
			pfdt->lWidthValue,			// amount of width stretch. 1 origin.
			pfdt->lGapValue,			// amount of gap. 0 = vertical
										// true for invert
			(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F'),
		pfdt->lFormat,					// format of the date/time
		pfdt->lTimeType,				// which date/time
		pfdt->lLength,					// length of string output. 0 = 'natural' ( no alteration )
		pfdt->cAlign,					// alignment of value in larger length - L/R
		sFillQuote,						// fill character. 0 is no fill.
		sDelimQuote,					// delimiter to use between date/time parts
										// which set of strings to use for alphabetic formats
		(pfdt->bStringNormal == TRUE ? 'T' : 'F'),
		pfdt->lHourACount				// number of time periods in 24 hours
		);
	// font name
	fj_processParameterStringOutput( pStr+strlen(pStr), pfdt->strFontName );
	strcat( pStr, "}" );
	return;
}
//
//  FUNCTION: fj_DatetimeFromStr()
//
//  PURPOSE:  Convert a descriptive string into a FJTEXT structure
//
FJDLLExport VOID fj_DatetimeFromStr( LPFJELEMENT pfe, LPCSTR pStr )
{
#define DATETIME_PARAMETERS 20
	LPFJDATETIME pfdt;
	LPSTR  pParams[DATETIME_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfdt = (LPFJDATETIME)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJDATETIME_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJDATETIME_MEMSTOR_SIZE );
	*(pStrM+FJDATETIME_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, DATETIME_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_DatetimeID ) ) res = 0;

	if ( res == DATETIME_PARAMETERS )	// FJ_DATETIME string must have 20 fields!!!!!
	{
		pfe->lTransform   =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft        = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow         = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition    =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfdt->lBoldValue  = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfdt->lWidthValue = atol(pParams[ 6]);
										// amount of gap. 0 = vertical
		pfdt->lGapValue   = atol(pParams[ 7]);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[10]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
										// format of the date/time
		pfdt->lFormat     = atol(pParams[11]);
										// which date/time
		pfdt->lTimeType   = atol(pParams[12]);
										// length of string output. 0 = 'natural' ( no alteration )
		pfdt->lLength     = atol(pParams[13]);
										// alignment of value in larger length - L/R
		pfdt->cAlign      =    *(pParams[14]);
		fj_processParameterStringInput( pParams[15] );
										// fill character. 0 is no fill.
		pfdt->cFill       =    *(pParams[15]);
		fj_processParameterStringInput( pParams[16] );
										// delimiter to use between date/time parts
		pfdt->cDelim      =    *(pParams[16]);
		// which set of strings to use for alphabetic formats
		pfdt->bStringNormal = (*(pParams[17]) == 'T' ? TRUE : FALSE);
										// number of time periods in 24 hours
		pfdt->lHourACount = atol(pParams[18]);
		fj_processParameterStringInput( pParams[19] );
		// font name
		strncpy( pfdt->strFontName, pParams[19], FJFONT_NAME_SIZE ); pfdt->strFontName[FJFONT_NAME_SIZE] = 0;
	}

	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_DatetimeBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJDATETIME from FJMEMSTOR
//
FJDLLExport VOID fj_DatetimeBuildFromName( CLPCFJMEMSTOR pfms, LPFJELEMENT pfe, LPCSTR pName )
{
	LPSTR    pTextStr;

	pTextStr = (LPSTR)fj_MemStorFindElement( pfms, fj_DatetimeID, pName );
	if ( NULL != pTextStr )
	{
		fj_DatetimeFromStr( pfe, pTextStr );
	}

	return;
}
//
//	FUNCTION: fj_DatetimeAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJDATETIME element to FJMEMSTOR
//
FJDLLExport BOOL fj_DatetimeAddToMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	CHAR buff[FJDATETIME_MEMSTOR_SIZE];

	fj_DatetimeToStr( pfe, buff );

	return( fj_MemStorAddElementString( pfms, buff ) );
}
//
//	FUNCTION: fj_DatetimeDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Delete FJDATETIME element from FJMEMSTOR
//
FJDLLExport BOOL fj_DatetimeDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	STR sName[FJDATETIME_NAME_SIZE*2];

										// Datetime element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	return( fj_MemStorDeleteElement( pfms, fj_DatetimeID, sName ) );
}
//
//	FUNCTION: fj_DatetimeGetType
//
//	Return: Datetime type
//
//
FJDLLExport enum FJELETYPE fj_DatetimeGetType( VOID )
{

	return( FJ_TYPE_DATETIME );
}
//
//	FUNCTION: fj_DatetimeGetTypeString
//
//	Return: Pointer to string for Text type
//
//
FJDLLExport LPCSTR fj_DatetimeGetTypeString( VOID )
{

	return( fj_DatetimeID );
}
//
//	FUNCTION: fj_DatetimetGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_DatetimeGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJELEMENT  pfeText;
	LPFJDATETIME pfd;

	pfd = (LPFJDATETIME)pfe->pDesc;

	pfeText = pfd->pfeText;
	// the following call could be dangerous.
	// the text string should not be built without making the call to build the image.
	// the normal PC build makes the string, compares it to the previous string,
	//     and builds an image only if the strings are different.
	// if the string and image do not agree, the wrong image could be printed.
	// the only reason there would be no pfeText and this routine is called,
	//     is when a list is made for display and the text is wanted.
	//     the label/message are then destroyed and not used to make images.
	if ( NULL == pfeText )
	{
		fj_DatetimeCreateImage1( (CLPFJELEMENT)pfe );
		pfeText = pfd->pfeText;
	}
	if ( NULL != pfeText )
	{
		strncpy( pStr, ((LPFJTEXT)pfeText->pDesc)->strText, lMaxCount );
		*(pStr+lMaxCount-1) = 0;
	}
	else *pStr = 0;
	return;
}
FJCELEMACTION fj_DatetimeActions =
{
	(LPVOID  (*)( VOID ))fj_DatetimeNew,
	(LPVOID  (*)( LPVOID ))fj_DatetimeDup,
	fj_DatetimeDestroy,
	fj_DatetimeCreateImage,
	fj_DatetimePhotocellBuild,
	fj_DatetimeBuildFromName,
	fj_DatetimeAddToMemstor,
	fj_DatetimeDeleteFromMemstor,
	fj_DatetimeGetTextString,
	fj_DatetimeGetType,
	fj_DatetimeGetTypeString
};
