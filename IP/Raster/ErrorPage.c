/*
 *            Copyright (c) 1998-2000 by NETsilicon Inc.
 *
 *  This software is copyrighted by and is the sole property of
 *  NETsilicon.  All rights, title, ownership, or other interests
 *  in the software remain the property of NETsilicon.  This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of NETsilicon.
 *
 *  NETsilicon, reserves the right to modify this software
 *  without notice.
 *
 *  NETsilicon
 *  411 Waverley Oaks Road                  USA 781.647.1234
 *  Suite 227                               http://www.netsilicon.com
 *  Waltham, MA 02452                       AmericaSales@netsilicon.com
 *
 *************************************************************************
 *
 *     Module Name: errorPage.c
 *         Version: 1.00
 *   Original Date: 08/23/99
 *          Author: Charles Gordon
 *        Language: Ansi C
 * Compile Options:
 * Compile defines:
 *       Libraries:
 *    Link Options:
 *
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 *
 *
 */

/*This file defines many character strings used to display error page when the
  required pages not found, failure to login the protected page,server busy,
  File System Error, input Too large, ..... The following 1s a example of "server
  busy" error page.

Server Busy
The object you requested on the RomPager Server is being used by someone else. Please
try again later.

Return to Last page    (<http://7.52.193.163/>)
*/

/*deviceName is the product device name just like "RomPager Server" in the above Server
  Busy example. You can change it by changing the deviceName.
*/
char deviceName[64]="Sentry Power Tower";

/* errorPageLinkStart is the begining part of the link string <A HREF=" (default). You can
   put many atrributes in this link, such as <A TYPE="video/mpeg" HREF=\"
*/
char errorPageLinkStart[64]="<A TARGET=\"_top\" HREF=\"";

/* theUrlReturnTo is the page url from the error page return to. The default is the last
   page to navigate to the request page. If theUrlReturnTo is initialized to "", the default
   page is returned to. theUrlReturnTo length is limited to 256 bytes, if larger than that,
   the server may be crashed.
*/
char theUrlReturnTo[256]="index.html";

/*thePagenameReturnTo is the string displayed in the link. You can change it as "Home Page".
 */
char thePagenameReturnTo[64]="Home Page";
