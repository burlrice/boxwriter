/* C file: c:\MyPrograms\FoxJet1\_romwait.c created
 by NET+Works HTML to C Utility on Thursday, November 08, 2001 10:34:03 */
#include "fj.h"

void fj_send_pwid_parameter(unsigned long handle, char *pPrevious);

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_romwait */
static void na_romwait (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\romwait.htm {{ */
void Send_function_21(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<title>Wait for ROM Write</title>\n");
	HSSend (handle, "<script type=text/javascript>\n");
	HSSend (handle, "");
	na_romwait (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p><center>Waiting for data to be written into permanent memory.</center></p>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\romwait.htm }} */
/* @! End HTML page   */

/* @! Begin of dynamic functions */
/* @! Function: na_romwait  {{ */
void na_romwait(unsigned long handle)
{
	/* add your implementation here: */
	char   formField[50];
	BOOL   bRet;
	int    ret;

	ret = fj_HSGetValue( handle, "page", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		bRet = TRUE;
	}
	if ( 0 == pCVT->pFJglobal->semaROMLock.tx_semaphore_count )
	{
		HSSend (handle, "setTimeout(\"document.location.reload()\",4000); // 4 seconds\n");
	}
	else
	{
		HSSend (handle, "document.location.replace('");
		HSSend (handle, formField);
		// append pwid if not already present
		fj_send_pwid_parameter( handle, formField );
		HSSend (handle, "');");
	}
}										/* @! Function: na_romwait  }} */
/* @! End of dynamic functions   */
