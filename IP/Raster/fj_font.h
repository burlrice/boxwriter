#ifndef FJ_FONT
#define FJ_FONT

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjmemstor;

typedef struct fjfont
{
	LONG     lType;						// font type
	LONG     lHeight;					// height of font charascter in pixels  - depends on font
	LONG     lWidth;					// width of font character in pixels - depends on font
	LONG     lFirstChar;				// first defined character in font
	LONG     lLastChar;					// last defined character in font
	LPBYTE   pBuffer;					// pointer to font rasters
	ULONG    lBufLen;					// Length of font rasters buffer
	LPULONG  pWBuffer;					// pointer to font width array
										// font name
	STR      strName[1+FJFONT_NAME_SIZE];
} FJFONT, *PFJFONT, FAR *LPFJFONT, FAR * const CLPFJFONT;
typedef const struct fjfont FAR *LPCFJFONT, FAR * const CLPCFJFONT;
#define FJFONT_BITMAP   1
#define FJFONT_TRUETYPE 2

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	//
	// declarations for routines in font DLL
	//
	FJDLLExport LPFJFONT  fj_FontNew( VOID );
	FJDLLExport VOID      fj_FontDestroy( LPFJFONT pff );
	FJDLLExport LONG      fj_FontToBuffer( CLPCFJFONT pff, LPBYTE pBuff );
	FJDLLExport LPFJFONT  fj_FontFromBuffer( LPBYTE pBuff );
	FJDLLExport LPFJFONT  fj_FontBuildFromName( struct fjmemstor FAR * pMemStor, LPCSTR pName );
	FJDLLExport BOOL      fj_FontAddToMemstor( struct fjmemstor FAR *, LPCFJFONT pff );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_FONT
