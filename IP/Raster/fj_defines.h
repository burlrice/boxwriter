#ifndef FJ_DEFINES
#define FJ_DEFINES

#include "iec16022-0.2.1/iec16022ecc200.h"

// the sizes for strings are for the number of allowable characters.
// the string fields are always made one larger to account for the 0 string terminator.

#define FJ_DEFAULT_NAME_SIZE       32	// default name size for the following items

// segment and element names are dependnt on message name
// gesment and element names are internal only
										// maximum size of name for a message element
#define FJMESSAGE_NAME_SIZE        FJ_DEFAULT_NAME_SIZE

#define FJBMP_NAME_SIZE            128	// maximum size of name for a bmp file. large to accomodate Windows file names for the editor.
										// maximum number of characters to build a string for a bmp in MemStor
#define FJBMP_MEMSTOR_SIZE         (32+FJBMP_NAME_SIZE)

										// maximum size of name for a font
#define FJFONT_NAME_SIZE           (FJ_DEFAULT_NAME_SIZE)
										// maximum number of characters to build a string for a font element in MemStor
#define FJFONT_MEMSTOR_SIZE        (128+FJFONT_NAME_SIZE)

										// maximum size of name for a text element
#define FJTEXT_NAME_SIZE           (FJMESSAGE_NAME_SIZE+16)
#define FJTEXT_SIZE                256	// maximum number of characters in a text element
// maximum number of characters to build a string for a text element in MemStor
#define FJTEXT_MEMSTOR_SIZE        (256+FJTEXT_NAME_SIZE+FJTEXT_SIZE)

										// maximum size of name for a barcode element
#define FJBARCODE_NAME_SIZE        (FJMESSAGE_NAME_SIZE+16)
#define FJBARCODE_SIZE             64	// maximum number of characters in a barcode element
// maximum number of characters to build a string for a barcode element in MemStor
#define FJBARCODE_MEMSTOR_SIZE     (256+FJBARCODE_NAME_SIZE+FJTEXT_SIZE)

#define FJDATAMATRIX_NAME_SIZE        (FJMESSAGE_NAME_SIZE+16)
#define FJDATAMATRIX_SIZE             MAXBARCODE
#define FJDATAMATRIX_MEMSTOR_SIZE     (256+FJDATAMATRIX_NAME_SIZE+FJTEXT_SIZE)

										// maximum size of name for a bitmap element
#define FJBITMAP_NAME_SIZE         (FJMESSAGE_NAME_SIZE+16)
										// maximum number of characters to build a string for a bitmap element in MemStor
#define FJBITMAP_MEMSTOR_SIZE      (64+FJBITMAP_NAME_SIZE)

										// maximum size of name for a dynamic image element
#define FJDYNIMAGE_NAME_SIZE       (FJMESSAGE_NAME_SIZE+16)
										// maximum number of characters to build a string for a dynamic image element in MemStor
#define FJDYNIMAGE_MEMSTOR_SIZE    (64+FJDYNIMAGE_NAME_SIZE)

										// maximum size of name for a datetime element
#define FJDATETIME_NAME_SIZE       (FJMESSAGE_NAME_SIZE+16)
// maximum number of characters to build a string for a datetime element in MemStor
#define FJDATETIME_MEMSTOR_SIZE    (256+FJDATETIME_NAME_SIZE)
#define FJDATETIME_STRING_SIZE     15	// max size of strings for datetime values - day names, month names, etc.

										// maximum size of name for a counter element
#define FJCOUNTER_NAME_SIZE        (FJMESSAGE_NAME_SIZE+16)
										// maximum number of characters to build a string for a counter element in MemStor
#define FJCOUNTER_MEMSTOR_SIZE     (256+FJCOUNTER_NAME_SIZE)
										// maximum counter value
#define FJCOUNTER_MAX_VALUE        100000000

										// maximum size of name for an element
#define FJELEMENT_NAME_SIZE        (FJMESSAGE_NAME_SIZE+16)

#define FJMESSAGE_NUMBER_SEGMENTS  50	// maximum number of segments/elements in one message
// maximum number of segments/elements in one message
#define FJMESSAGE_DYNAMIC_SEGMENTS  FJMESSAGE_NUMBER_SEGMENTS
// maximum number of characters to build a string for a message element in MemStor
#define FJMESSAGE_MEMSTOR_SIZE     (64+FJMESSAGE_NAME_SIZE+(FJMESSAGE_NUMBER_SEGMENTS*FJELEMENT_NAME_SIZE))

#define FJPH_TYPE_NAME_SIZE        47	// maximum size for a descriptve name of a print head type
										// maximum size of name for a print head
#define FJPH_NAME_SIZE_LONG        FJ_DEFAULT_NAME_SIZE
#define FJPH_NAME_SIZE_SHORT       7
#define FJPH_SERIAL_SIZE           8
#define FJPH_ID_SIZE               (FJPH_NAME_SIZE_SHORT+FJPH_SERIAL_SIZE)
#define FJPH_MEMSTOR_SIZE          (64+FJPH_NAME_SIZE_LONG)
// NETBIOS names are limited to 15 characters.
// We limit the ID to 15 characters - 7 for Short Name and 8 for Serial Number

#define FJSYS_PASSWORD_SIZE        15

#define FJSYS_SHIFT_CODE_SIZE      15

#define FJSYS_NUMBER_PRINTERS      16	// maximum number of printers in one group
#define FJSYS_NAME_SIZE            15	// maximum size of name for a print head group
										// maximum number of characters to build a string for a printhead group element in MemStor
#define FJSYS_MEMSTOR_SIZE         (1400+FJSYS_NAME_SIZE)

										// maximum size of name for a label
#define FJLABEL_NAME_SIZE          FJ_DEFAULT_NAME_SIZE
// maximum number of characters to build a string for a label element in MemStor
#define FJLABEL_MEMSTOR_SIZE       (64+FJLABEL_NAME_SIZE+(FJSYS_NUMBER_PRINTERS*(1+FJPH_NAME_SIZE_LONG)))

										// largest buffer needed for user names
#define FJSYS_LARGEST_USER_NAME    FJBMP_NAME_SIZE

#define FJSYS_NOZZLES              256	// maximum number of nozzles in a printhead

#define FJSYS_MAX_MESSAGES         4	// maximum number of messages that can be queued between photocell and printhead

#define FJSYS_BARCODES             8	// number of global barcode entries
										// maximum size of name for a system barcode
#define FJSYS_BARCODE_NAME_SIZE    FJ_DEFAULT_NAME_SIZE

#define FJSYS_NUMBER_SOCKETS        24	// maximum number of open socket and browser connections

#define FJSYS_NUMBER_LABEL_COUNTERS 10	// number of label counters for statistics

#define FJSYS_NUMBER_DNS            3	// maximum number of DNS internet address
#define FJSYS_EMAIL_SERVER_SIZE     32	// maximum size of name of Email server name
#define FJSYS_EMAIL_NUMBER          3	// maximum number of Email address for each Email service
#define FJSYS_EMAIL_ID_SIZE         64	// maximum size of an Email address

#define FJ_SYS_SERIAL_MIN        20000	// lowest allowable customer serial number
#define FJ_SYS_SERIAL_MAX     16000000	// highest allowable customer serial number
#define FJ_SYS_SERIAL_TEST        2000	// highest allowable test serial number
// default serial number is less than 2000.

#define FJSYS_MAX_INKLOW_ML 5.0
#define FJSYS_MAX_INKLOW_UJ2_192_32NP_ML 9.8
// if less than FJ_SYS_SERIAL_TEST, another serial number less than FJ_SYS_SERIAL_TEST can be set.

#define FJSYS_TEST_PATTERN	"TESTPATTERN" // burl

#endif									// ifndef FJ_DEFINES
