#include "fj.h"
#include "fj_system.h"

// NOTE: time_t typedef is 32-bit value of seconds since Jan 1, 1970
// NOTE: tm structure uses a year value where 0 = 1900
// NOTE: tm structure uses a 0 origin month

#define TIMER_MAX_BUFFER           256
#define TIMER_PORT_ASCII            13
#define TIMER_PORT_BINARY           37

#define TIMER_SYNCH_INTERVAL        10	// minutes between reads of external TOD clock
#define LOCAL_UDP_RETRY_COUNT        3	// number of read attempts
// delay between reads - 1/5 second
#define LOCAL_UDP_RETRY_DELAY        (BSP_TICKS_PER_SECOND/5)
#define TCP_RETRY_COUNT              3	// number of read attempts
// delay between reads - 1/5 second
#define TCP_RETRY_DELAY              (BSP_TICKS_PER_SECOND/5)

static TX_TIMER fj_TimeRead;

static    BOOL    bServer = FALSE;		// not yet implemented. be a time server?

typedef struct fjtztable
{
	CHAR  stName[8];
	FLOAT fDelta;
} FJTZTABLE, FAR *LPTZTABLE;

FJTZTABLE FJ_tzTable[] =
{
	"AST"   ,-4,						// usa/canada
	"ADT"   ,-3,
	"EST"   ,-5,
	"EDT"   ,-4,
	"CST"   ,-6,
	"CDT"   ,-5,
	"MST"   ,-7,
	"MDT"   ,-6,
	"PST"   ,-8,
	"PDT"   ,-7,
	"AKST"  ,-9,
	"AKDT"  ,-8,
	"HST"   ,-10,
	"HDT"   ,-9,
	"YST"   ,-9,						// canada
	"YDT"   ,-8,
	"UTC"   ,0,
	"GMT"   ,0,							// europe
	"BST"   ,+1,
	"IST"   ,+1,
	"WET"   ,0,
	"WEST"  ,+1,
	"CET"   ,+1,
	"CEST"  ,+2,
	"EET"   ,+2,
	"EEST"  ,+3,
	"MSK"   ,+3,
	"MSD"   ,+4,
	"BT"    ,+3,
	"AEST"  ,+10,						// australia
	"AEDT"  ,+11,
	"ACST"  ,+9.5,
	"ACDT"  ,+10.5,
	"AWST"  ,+8,
	"CCT"   ,+8,						// china
	"JST"   ,+9,						// japan
	"GST"   ,+10,						// guam
	"A"     ,+1,
	"B"     ,+2,
	"C"     ,+3,
	"D"     ,+4,
	"E"     ,+5,
	"F"     ,+6,
	"G"     ,+7,
	"H"     ,+8,
	"I"     ,+9,
	"K"     ,+10,
	"L"     ,+11,
	"M"     ,+12,
	"N"     ,-1,
	"O"     ,-2,
	"P"     ,-3,
	"Q"     ,-4,
	"R"     ,-5,
	"S"     ,-6,
	"T"     ,-7,
	"U"     ,-8,
	"V"     ,-9,
	"W"     ,-10,
	"X"     ,-11,
	"Y"     ,-12,
	"Z"     ,0
};

//
// find next field in message read from port 13
//
CHAR * fj_TimeParse13NextField(CHAR * field)
{
	CHAR *ret;
	ret = field;						// start of next field
										// skip field
	while ( (' ' != *ret) && (0 != *ret) ) ret++;
	*ret = 0;							// end it with a 0
	ret++;
										// skip blanks
	while ( (' ' == *ret) && (0 != *ret) ) ret++;
	return ret;
}
//
// parse a buffer read from port 13
//
BOOL fj_TimeParse13( LPCHAR buffer, struct tm *pTM, BOOL *pTimeUTC )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	// pointers to parts of the input buffer
	//			NIST
	CHAR *b1;							//	Julian
	CHAR *b2;							//	yy-mm-dd
	CHAR *b3;							//	hh:mm:ss
	CHAR *b4;							//	DST
	CHAR *b5;							//	Leap Second
	CHAR *b6;							//	Health
	CHAR *b7;							//	ms Advance
	CHAR *b8;							//	label
	CHAR *b9;							//	on-time '*'
	struct tm time;
	BOOL   bTimeUTC;
	BOOL   bRet;

	bRet = FALSE;

	b1 = buffer;
	// skip starting blanks and/or '\n'
	while ( ((' ' == *b1) || ('\n' == *b1)) && (0 != *b1) ) b1++;
	b2 = fj_TimeParse13NextField( b1 );
	b3 = fj_TimeParse13NextField( b2 );
	b4 = fj_TimeParse13NextField( b3 );
	b5 = fj_TimeParse13NextField( b4 );
	b6 = fj_TimeParse13NextField( b5 );
	b7 = fj_TimeParse13NextField( b6 );
	b8 = fj_TimeParse13NextField( b7 );
	b9 = fj_TimeParse13NextField( b8 );
	// is it NIST time format?
	if ( ( 0 != *b8 ) &&				// at least 8 fields ?
		( b2[2] == '-' ) &&				// 2nd field has a '-' ?
		( b3[2] == ':' ) )				// 3rd field has a ':' ?
	{
		if ( '0' == *b6 )				// if health is good
		{
			b2[2] = 0;
			b2[5] = 0;
			b3[2] = 0;
			b3[5] = 0;
			time.tm_hour  = atoi(&b3[0]);
			time.tm_min   = atoi(&b3[3]);
			time.tm_sec   = atoi(&b3[6]);
			time.tm_year  = atoi(&b2[0]);
			time.tm_mon   = atoi(&b2[3]);
			time.tm_mday  = atoi(&b2[6]);
			time.tm_wday  = 0;
			time.tm_yday  = 0;
			time.tm_isdst = 0;			// no dst
			time.tm_mon--;				// tm_mon is 0 origin
			time.tm_year += 2000 - 1900;// year is 1900 origin

			bTimeUTC = FALSE;
			if ( 0 == strcmp(b8, "UTC(NIST)") )
			{
				bTimeUTC = TRUE;
			}
			*pTimeUTC = bTimeUTC;
			memcpy( pTM, &time, sizeof(struct tm) );
			bRet = TRUE;

			if ( TRUE == pfph->bInfoTime )
			{
				CHAR  str[80];
				fj_writeInfo( "Parse 13:   " );
				fj_SystemGetDateTimeString( str, &time );
				fj_writeInfo(str);
				fj_writeInfo( "\n" );
			}
		}
	}

	return(bRet);
}
//
// read a local UDP port13 tod clock
//
BOOL fj_timeReadLocalUDP( ip_addr *pipRead, BOOL bBroadcast, struct tm *pTM, BOOL *pTimeUTC )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	struct sockaddr_in   addrTimerA = { AF_INET, TIMER_PORT_ASCII, INADDR_ANY, 0 };
	struct sockaddr_in   addrTo     = { AF_INET, TIMER_PORT_ASCII, 0, 0 };
	struct sockaddr_in   addrFrom;
	int    sizesockaddr_in = sizeof( struct sockaddr_in );
	int    socketl;
	int    optval;
	int    optsize = sizeof(int);
										// buffer for Ethernet reads
	CHAR   strTimeBuffer[TIMER_MAX_BUFFER];
	CHAR   str[180];
	LONG   i;
	LONG   retry;
	LONG   bytesread;
	LONG   ret;
	int    error;
	int    errorSize = sizeof(error);
	BOOL   bRet;

	bRet = FALSE;

	addrTo.sin_addr.s_addr = *pipRead;

	socketl = socket( AF_INET, SOCK_DGRAM, 0 );
	if ( socketl > 0 )
	{
		optval = 1;						// set value to turn options on
		if ( TRUE == bBroadcast )
		{
			ret = setsockopt( socketl, SOL_SOCKET, SO_BROADCAST, (char*)&optval, 4 );
		}
		ret = setsockopt( socketl, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, 4 );
		ret = setsockopt( socketl, SOL_SOCKET, SO_NBIO, (char*)&optval, 4 );
		optval = 0;						// set value to turn options off
		if ( TRUE == bBroadcast )
		{
			ret = setsockopt( socketl, SOL_SOCKET, SO_DONTROUTE, (char*)&optval, 4 );
		}
		// bind this socket to the port
		ret = bind( socketl, &addrTimerA, sizeof(addrTimerA));
		if ( 0 == ret )
		{
			bytesread = 0;
			for ( retry = 0; (retry < LOCAL_UDP_RETRY_COUNT) && (FALSE == bRet) ; retry++ )
			{
				// send a null packet
				ret = sendto( socketl, strTimeBuffer, 0, 0, &addrTo, sizesockaddr_in );
				for ( i = 0; (i < (3*BSP_TICKS_PER_SECOND)) && (bytesread <= 0) ; i += LOCAL_UDP_RETRY_DELAY )
				{
					bytesread = recvfrom( socketl, strTimeBuffer, TIMER_MAX_BUFFER-1, 0, &addrFrom, &sizesockaddr_in );
					if ( bytesread > 0 )
					{
						strTimeBuffer[bytesread] = 0;
						if ( TRUE == pfph->bInfoTime )
						{
							sprintf( str, "Local Time find from (%d) %d.%d.%d.%d %d bytes %s\n", (int)TIMER_PORT_ASCII, PUSH_IPADDR(addrFrom.sin_addr.s_addr),bytesread,strTimeBuffer);
							fj_writeInfo(str);
						}
						bRet = fj_TimeParse13(strTimeBuffer, pTM, pTimeUTC);
						if ( TRUE == bRet )
						{
							*pipRead = addrFrom.sin_addr.s_addr;
						}
						else
						{
							bytesread = 0;
						}
					}
					if (  bytesread <= 0)
					{
						tx_thread_sleep( LOCAL_UDP_RETRY_DELAY );
					}
				}
			}
		}
		socketclose( socketl );
	}

	return( bRet );
}
//
// read a TCP port13 tod clock
//
BOOL fj_timeReadTCP( ip_addr ipRead, struct tm *pTM, BOOL *pTimeUTC )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	struct sockaddr_in   addrTo = { AF_INET, TIMER_PORT_ASCII, 0, 0 };
	int    sizesockaddr_in = sizeof( struct sockaddr_in );
	int    socketl;
	int    optval;
	int    optsize = sizeof(int);
										// buffer for Ethernet reads
	CHAR   strTimeBuffer[TIMER_MAX_BUFFER];
	CHAR   str[180];
	LONG   i;
	LONG   retry;
	LONG   bytesread;
	LONG   ret;
	LONG   rete;
	int    error;
	int    errorSize = sizeof(error);
	BOOL   bRet;

	bRet = FALSE;

	addrTo.sin_addr.s_addr = ipRead;

	socketl = socket( AF_INET, SOCK_STREAM, 0 );
	if ( socketl > 0 )
	{
		optval = 1;						// set value to turn options on
		ret = setsockopt( socketl, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, 4 );
		ret = setsockopt( socketl, SOL_SOCKET, SO_NBIO, (char*)&optval, 4 );
		// connect this socket
		ret = connect( socketl, &addrTo, sizeof(addrTo));
		if ( -1 == ret )
		{
			errorSize = sizeof(error);
			rete = getsockopt(socketl, SOL_SOCKET, SO_ERROR, (char *)&error, &errorSize);
			if ( 0 == rete )
			{
				if ( 0 == errorSize ) ret = 0;
				else if ( EINPROGRESS == error ) ret = 0;
			}
		}
		if ( 0 == ret )
		{
			bytesread = 0;
			for ( retry = 0; (retry < TCP_RETRY_COUNT) && (FALSE == bRet) ; retry++ )
			{
				for ( i = 0; (i < (3*BSP_TICKS_PER_SECOND)) && (bytesread <= 0) ; i += TCP_RETRY_DELAY )
				{
					sizesockaddr_in = sizeof( struct sockaddr_in );
					bytesread = recv( socketl, strTimeBuffer, TIMER_MAX_BUFFER-1, 0 );
					if ( -1 == bytesread )
					{
						errorSize = sizeof(error);
						rete = getsockopt(socketl, SOL_SOCKET, SO_ERROR, (char *)&error, &errorSize);
						if ( 0 == rete )
						{
							if ( 0 == errorSize ) bytesread = 0;
							else if ( EINPROGRESS == error ) bytesread = 0;
						}
					}
					if ( bytesread > 0 )
					{
						strTimeBuffer[bytesread] = 0;
						if ( TRUE == pfph->bInfoTime )
						{
							sprintf( str, "Time read from (%d) %d.%d.%d.%d %d bytes %s\n", (int)TIMER_PORT_ASCII, PUSH_IPADDR(addrTo.sin_addr.s_addr),bytesread,strTimeBuffer);
							fj_writeInfo(str);
						}
						bRet = fj_TimeParse13(strTimeBuffer, pTM, pTimeUTC);
						if ( FALSE == bRet )
						{
							bytesread = 0;
						}
					}
					if (  bytesread <= 0)
					{
						tx_thread_sleep( TCP_RETRY_DELAY );
					}
				}
			}
		}
		socketclose( socketl );
	}

	return( bRet );
}
#define  MAKE_IPADDR(ip1,ip2,ip3,ip4)  ((ip1<<24)|(ip2<<16)|(ip3<<8)|(ip4))

static ULONG fj_timeServers[] =
{
	MAKE_IPADDR(192, 43,244, 18),		// NCAR Boulder
	MAKE_IPADDR(132,163,  4,101),		// NIST Boulder
	MAKE_IPADDR(129,  6, 15, 28),		// NIST Gaithersburg
	MAKE_IPADDR(132,163,  4,102),		// NIST Boulder
	MAKE_IPADDR(128,138,140, 44),		// U of Colorado
	MAKE_IPADDR(131,107,  1, 10),		// NIST at Microsoft
	MAKE_IPADDR(129,  6, 15, 29),		// NIST Gaithersburg
	MAKE_IPADDR(132,163,  4,103),		// NIST Boulder
	MAKE_IPADDR(216,200, 93,  8),		// NIST DC
	MAKE_IPADDR(208,184, 49,129)		// NIST NYC

};

//
// find an external tod clock
//
BOOL fj_timeFind( struct tm *pTM, BOOL *pTimeUTC )
{
	CLPFJ_GLOBAL  pGlobal  = fj_CVT.pFJglobal;
	CLPCFJSYSTEM  pfsys    = fj_CVT.pfsys;
	ip_addr ipTry;
	LONG    lTimer;
	int     i, j, n;
	BOOL    bRet;

	bRet = FALSE;

	// try suggested local UDP13 device
	if ( 0 != pfsys->lAddrUDP13 )
	{
		ipTry = pfsys->lAddrUDP13;
		bRet = fj_timeReadLocalUDP( &ipTry, FALSE, pTM, pTimeUTC );
	}
	// try a local broadcast to UDP13 devices
	if ( FALSE == bRet )
	{
		ipTry = pGlobal->ipbroadcastnet;
		bRet = fj_timeReadLocalUDP( &ipTry, TRUE, pTM, pTimeUTC );
	}
	if( FALSE == bRet )
	{
		ipTry = 0;
	}
	pGlobal->ipTimeUDP = ipTry;

	if ( FALSE == bRet )
	{
		// try suggested TCP13 device
		if ( 0 != pfsys->lAddrTCP13 )
		{
			ipTry = pfsys->lAddrTCP13;
			bRet = fj_timeReadTCP( ipTry, pTM, pTimeUTC );
		}
		if ( FALSE == bRet )
		{
			lTimer = fj_CVT.pGA->rw.read.HStimer;
			n = sizeof(fj_timeServers) / sizeof(ULONG);
			j = lTimer - ((lTimer / n) * n);
			for ( i = 0; i < n; i++ )
			{
				ipTry = fj_timeServers[j];
				bRet = fj_timeReadTCP( ipTry, pTM, pTimeUTC );
				if ( TRUE == bRet )
				{
					break;
				}
				j++;
				if ( j >= n ) j = 0;
			}
		}
		if( FALSE == bRet )
		{
			ipTry = 0;
		}
		pGlobal->ipTimeTCP = ipTry;
	}

	return( bRet );
}
VOID fj_TimeTimer(ULONG unused)
{
	tx_thread_resume( &(fj_CVT.pFJglobal->thrTime) );
	return;
}
VOID fj_threadTimeStart(ULONG in)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	CLPFJSYSTEM  pfsys   = fj_CVT.pfsys;
	BOOL   bLoop = TRUE;
	BOOL   bFound = FALSE;
	BOOL   bTimeUTC;
	time_t tRead;
	time_t tWait;
	time_t tNew;
	time_t tOld;
	struct tm tm;
	struct tm tmOld;
	CHAR   str[80];
	int    retrom;
	int    iRet;

	iRet = tx_timer_create( &fj_TimeRead, "TimeRead", fj_TimeTimer, 0, 1, 0, TX_NO_ACTIVATE );
	pGlobal->ipTimeUDP = 0;
	pGlobal->ipTimeTCP = 0;
	pGlobal->ipTimeIP  = 0;

	while ( TRUE == bLoop )
	{
		if ( FALSE == pfsys->bInternalClock )
		{
			if ( TRUE == bFound )
			{
				bFound = FALSE;
				pGlobal->ipTimeUDP = 0;
				pGlobal->ipTimeTCP = 0;
				pGlobal->ipTimeIP  = 0;
				if ( 0 != pGlobal->ipTimeUDP )
				{
					bFound = fj_timeReadLocalUDP( &(pGlobal->ipTimeUDP), FALSE, &tm, &bTimeUTC );
				}
				if ( 0 != pGlobal->ipTimeTCP )
				{
					bFound = fj_timeReadTCP( pGlobal->ipTimeTCP, &tm, &bTimeUTC );
				}
			}
			if ( FALSE == bFound )
			{
				bFound = fj_timeFind( &tm, &bTimeUTC );
			}
			if ( FALSE == bFound )
			{
				if ( TRUE == pfph->bInfoTime ) fj_writeInfo("sleep for 1 minute\n");
				tWait = 61;				// if no external timer found, try every minute
			}
			else
			{
				BOOL bDST = pfsys->bDSTUSA;
				tm.tm_isdst = 0;
				if ( TRUE == bTimeUTC )	// if UTC is read
				{
					pfsys->bDSTUSA = FALSE;
										// change to local STANDARD time
					tm.tm_sec += pfsys->lTimeZoneSeconds;
					fj_mktime( &tm );
					pfsys->bDSTUSA = bDST;
				}
				else					// if local time is read
				{
					if ( TRUE == bDST )
					{
						pfsys->bDSTUSA = FALSE;
						tm.tm_hour--;	// change to local STANDARD time
						fj_mktime( &tm );
						pfsys->bDSTUSA = bDST;
					}
				}
				// read the current RT clock
				fj_print_head_GetRTClockTM( &tmOld );
				// set the RT clock
										// set the RTC to local time
				fj_print_head_SetRTClockTM( &tm );

				if ( TRUE == pfph->bInfoTime )
				{
					fj_writeInfo( "Clock set:  " );
					fj_SystemGetDateTimeString( str, &tm );
					fj_writeInfo(str);
					fj_writeInfo( "\n" );

					tNew = fj_mktime( &tm );
					fj_writeInfo( "Local time: " );
					fj_SystemGetDateTimeString( str, &tm );
					fj_writeInfo(str);
					fj_writeInfo( "\n" );

					tOld = fj_mktime( &tmOld );
					fj_writeInfo( "  Old time: " );
					fj_SystemGetDateTimeString( str, &tmOld );
					fj_writeInfo(str);
					fj_writeInfo( "\n" );

					sprintf( str, "RTClock changed by %i seconds\n", (int)(tNew-tOld) );
					fj_writeInfo( str );
				}

				// plan the time of the next read/synch
				fj_SystemGetTime( &tm );// get local time
				tWait = tm.tm_min + TIMER_SYNCH_INTERVAL;
				if ( TIMER_SYNCH_INTERVAL < 59 )
				{
					// round down to interval
					tWait = ((tWait)/TIMER_SYNCH_INTERVAL)*TIMER_SYNCH_INTERVAL;
				}
				tWait *= 60;
				tWait -= tm.tm_sec;		// on the minute
			}
		}
		else
		{
			tWait = 61;					// using internal timer. wake up every minute and check if setting changed.
		}

		if ( tWait > 0 )
		{
			tWait *= BSP_TICKS_PER_SECOND;
			iRet = tx_timer_deactivate( &fj_TimeRead );
			iRet = tx_timer_change( &fj_TimeRead, tWait, tWait );
			iRet = tx_timer_activate( &fj_TimeRead );
			tx_thread_suspend( &(fj_CVT.pFJglobal->thrTime) );
		}
	}
	return;
}
//
// initialize the time find / read code
//
VOID fj_timeInit(VOID)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int    ret;
	LPBYTE pStack;

	pStack = fj_memory_getStack(FJ_THREAD_TIME_STACK_SIZE);
	// create the thread to get/send external time.
										// control block for time thread
	ret = tx_thread_create (&(pGlobal->thrTime),
		"Time Thread",					// thread name
		fj_threadTimeStart,				// entry function
		0,								// parameter
		pStack,							// start of stack
		FJ_THREAD_TIME_STACK_SIZE,		// size of stack
		FJ_THREAD_TIME_PRIORITY,		// priority
		FJ_THREAD_TIME_PRIORITY,		// preemption threshold
		1,								// time slice threshold
		TX_AUTO_START);					// start immediately

	if (ret != TX_SUCCESS)				// if couldn't create thread
	{
		netosFatalError ("Unable to create Time thread", 1, 1);
	}
	return;
}
//
// check time
//
VOID fj_timeCheck(VOID)
{
	CLPFJ_GLOBAL pGlobal  = fj_CVT.pFJglobal;
	CLPFJ_PARAM  pParamFJ = fj_CVT.pFJparam;
	struct tm tm;
	BOOL   bManu      = FALSE;
	BOOL   bInservice = FALSE;
	BOOL   bUpdate    = FALSE;
	BOOL   bFreeLock  = FALSE;
	int    retrom;

										// set the manufacture date
	if ( 0 == fj_CVT.pfph->tmDateManu.tm_year )
	{
		fj_SystemGetTime( &(pGlobal->tmNewManu) );
	}
										// set a new manufacture date?  year has origin of 1900
	if ( pGlobal->tmNewManu.tm_year > 100 )
	{
		bManu = TRUE;
		bUpdate = TRUE;
	}
	// set a new inservice date?  year has origin of 1900
	if ( (pGlobal->tmNewInservice.tm_year > 100) || (pGlobal->tmNewInservice.tm_year == 1) )
	{
		bInservice = TRUE;
		bUpdate = TRUE;
	}

	if ( TRUE == bUpdate )				// update the ROM
	{
		tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
		bFreeLock = TRUE;
		retrom = fj_ReadFJRomParams( pParamFJ );
		if ( 0 == retrom  )
		{
			if ( TRUE == bManu )
			{
										// sets date in working copy and param copy
				fj_setDateManu( &(pGlobal->tmNewManu) );
				pGlobal->tmNewManu.tm_year = 0;
			}
			if ( TRUE == bInservice )
			{
				if ( pGlobal->tmNewInservice.tm_year == 1 )
				{
					memset( &(pGlobal->tmNewInservice), 0, sizeof(struct tm) );
				}
				// sets date in working copy and param copy
				fj_setDateInservice( &(pGlobal->tmNewInservice) );
				pGlobal->tmNewInservice.tm_year = 0;
			}
										// write and free ROM lock
			fj_WriteFJRomParams( pParamFJ, TRUE, NULL, 0 );
			bFreeLock = FALSE;
		}
	}

	if ( TRUE == bFreeLock )
	{
		tx_semaphore_put( &(pGlobal->semaROMLock) );
	}
	return;
}
