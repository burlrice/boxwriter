/* C file: c:\MyPrograms\FoxJet1\_printerconfig.c created
 by NET+Works HTML to C Utility on Wednesday, November 07, 2001 09:54:56 */
#include "fj.h"
#include "fj_misc.h"
#include "fj_message.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
void na_Double(unsigned long handle, double dInput, int iPlaces);
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_SERIAL_BAUD */
static void na_SERIAL_BAUD (unsigned long handle);
/*  @! File: na_SERnone */
static void na_SERnone (unsigned long handle);
/*  @! File: na_SERlabel */
static void na_SERlabel (unsigned long handle);
/*  @! File: na_SERinfo */
static void na_SERinfo (unsigned long handle);
/*  @! File: na_DIRrtol */
static void na_DIRrtol (unsigned long handle);
/*  @! File: na_DIRltor */
static void na_DIRltor (unsigned long handle);
/*  @! File: na_PRDIST */
static void na_PRDIST (unsigned long handle);
/*  @! File: na_PRDIST2 */
static void na_PRDIST2 (unsigned long handle);
/*  @! File: na_PRHGT */
static void na_PRHGT (unsigned long handle);
/*  @! File: na_PRANG */
static void na_PRANG (unsigned long handle);
/*  @! File: na_PRSLT */
static void na_PRSLT (unsigned long handle);
/*  @! File: na_TITLE_CF_PRINTER_LAYOUT */
static void na_TITLE_CF_PRINTER_LAYOUT (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_PRAMSINTER */
static void na_PRAMSINTER (unsigned long handle);
/*  @! File: na_PRAMSINACT */
static void na_PRAMSINACT (unsigned long handle);
/*  @! File: na_PRUNIT */
static void na_PRUNIT (unsigned long handle);
/*  @! File: na_INFOSTAT */
static void na_INFOSTAT (unsigned long handle);
/*  @! File: na_INFOSOCK */
static void na_INFOSOCK (unsigned long handle);
/*  @! File: na_INFORAM */
static void na_INFORAM (unsigned long handle);
/*  @! File: na_INFOROM */
static void na_INFOROM (unsigned long handle);
/*  @! File: na_INFONAME */
static void na_INFONAME (unsigned long handle);
/*  @! File: na_INFOTIME */
static void na_INFOTIME (unsigned long handle);
/*  @! File: na_INFOAD */
static void na_INFOAD (unsigned long handle);
/*  @! File: na_PCHead */
static void na_PCHead (unsigned long handle);
/*  @! File: na_PCback */
static void na_PCback (unsigned long handle);
/*  @! File: na_SCANONCE */
static void na_SCANONCE (unsigned long handle);
/*  @! File: na_SOP */
static void na_SOP (unsigned long handle);
/*  @! File: na_SEYes */
static void na_SEYes (unsigned long handle);
/*  @! File: na_SEYres */
static void na_SEYres (unsigned long handle);
/*  @! File: na_DIV1 */
static void na_DIV1 (unsigned long handle);
/*  @! File: na_DIV2 */
static void na_DIV2 (unsigned long handle);
/*  @! File: na_DIV3 */
static void na_DIV3 (unsigned long handle);
/*  @! File: na_DIV4 */
static void na_DIV4 (unsigned long handle);
/*  @! File: na_SEYspeed */
static void na_SEYspeed (unsigned long handle);
/*  @! File: na_SEYfreq */
static void na_SEYfreq (unsigned long handle);
/*  @! File: na_SENo */
static void na_SENo (unsigned long handle);
/*  @! File: na_SEres */
static void na_SEres (unsigned long handle);
/*  @! File: na_SEspeed */
static void na_SEspeed (unsigned long handle);
/*  @! File: na_SEfreq */
static void na_SEfreq (unsigned long handle);
/*  @! File: na_phlist */
static void na_phlist (unsigned long handle);
/*  @! File: na_APSen */
static void na_APSen (unsigned long handle);
/*  @! File: na_APSdis */
static void na_APSdis (unsigned long handle);
/*  @! File: na_APSrun */
static void na_APSrun (unsigned long handle);
/*  @! File: na_SERvardata */
static void na_SERvardata (unsigned long handle);
/*  @! File: na_SERdynmem */
static void na_SERdynmem (unsigned long handle);
/*  @! File: na_PHTFEATURE */
static void na_PHTFEATURE (unsigned long handle);
/*  @! File: na_GA_FACTOR */
static void na_GA_FACTOR (unsigned long handle);
/*  @! File: na_PCInternal */
static void na_PCInternal (unsigned long handle);
/*  @! File: na_AUTOPRINT */
static void na_AUTOPRINT (unsigned long handle);
/*  @! File: na_BiDirectional */
static void na_BiDirectional (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\printerconfig.htm {{ */
void Send_function_19(unsigned long handle)
{
	HSSend (handle, "<HTML><HEAD><TITLE>");
	na_TITLE_CF_PRINTER_LAYOUT (handle);
	HSSend (handle, "</TITLE>\n");
	HSSend (handle, "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "gafactor = ");
	na_GA_FACTOR (handle);
	HSSend (handle, " ;\n");
	HSSend (handle, "baud = \"");
	na_SERIAL_BAUD (handle);
	HSSend (handle, "\" ;\n");
	HSSend (handle, "function setbaud() {\n");
	HSSend (handle, "var i,Ou;\n");
	HSSend (handle, "Ou = document.formse.baudrate;\n");
	HSSend (handle, "for ( i = 0; i < Ou.options.length; i++ )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "if ( baud == Ou.options[i].text )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "Ou.selectedIndex = i;break;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function showslant() {\n");
	HSSend (handle, "var str = \"printer_slant.htm\";\n");
	HSSend (handle, "window.open(str,\"Slant\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=700\");\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function sefreq(Ovspeed,Ovdiv,Ovres,Ovfreq) {\n");
	HSSend (handle, "parmFDCheck(Ovspeed,1,450,'Conveyor Speed');\n");
	HSSend (handle, "parmFDCheck(Ovres,1,600,'Resolution');\n");
	HSSend (handle, "var speed = Ovspeed.value;\n");
	HSSend (handle, "var res = Ovres.value;\n");
	HSSend (handle, "if ( 1 == speed )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "speed = 100;\n");
	HSSend (handle, "Ovspeed.value = speed;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "if ( 1 == res )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "res = 100;\n");
	HSSend (handle, "Ovres.value = res;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "Ovfreq.value = res * speed *12/60 / Ovdiv ;\n");
	HSSend (handle, "seydiv();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function senfreq() {\n");
	HSSend (handle, "var Of = document.formse;\n");
	HSSend (handle, "sefreq(Of.sen_speed,1,Of.sen_res,Of.sen_freq);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function seydiv() {\n");
	HSSend (handle, "var text;\n");
	HSSend (handle, "var Of = document.formse;\n");
	HSSend (handle, "var decimal;\n");
	HSSend (handle, "var efres = gafactor * Of.sey_res.value;\n");
	HSSend (handle, "text = 'a' + efres;\n");
	HSSend (handle, "decimal = 0;\n");
	HSSend (handle, "j = text.indexOf(\".\");\n");
	HSSend (handle, "if ( j > 0 ) decimal = text.length-j-1;\n");
	HSSend (handle, "for ( i = 0; i < 4; i++ )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "text = 'a' + efres/(i+1);\n");
	HSSend (handle, "j = text.indexOf(\".\");\n");
	HSSend (handle, "if ( j > 0 ){ if ( decimal > 0 ) j += decimal;else j--;}\n");
	HSSend (handle, "else j = text.length;\n");
	HSSend (handle, "text = text.substr( 1, j );\n");
	HSSend (handle, "if ( false == isFloat(text,false)) text = \"\";\n");
	HSSend (handle, "Of.sey_freqdiv.options[i].text=text;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function seyfreq() {\n");
	HSSend (handle, "var Of = document.formse;\n");
	HSSend (handle, "j = Of.sey_freqdiv.selectedIndex;\n");
	HSSend (handle, "sefreq(Of.sey_speed,j+1,Of.sey_res,Of.sey_freq);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function myreset(formObj) {\n");
	HSSend (handle, "formObj.reset();\n");
	HSSend (handle, "senfreq();seyfreq();setbaud();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function mysubmit(formObj) {\n");
	HSSend (handle, "formObj.submit();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function submitPC(formObj,ac) {\n");
	HSSend (handle, "formObj.action.value=ac;\n");
	HSSend (handle, "formObj.submit();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Configuration: Printer\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "</HEAD>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<form name=formse action=printerlayout_1 method=post>\n");
	HSSend (handle, "<table width=\"95%\" border=\"0\" rules=\"none\" cellPadding=\"6\">\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=\"1\" cellpadding=\"2\" rules=\"groups\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=\"3\"><b>Printhead Type</b></td></tr>\n");
	HSSend (handle, "</thead><tbody><tr><td width=\"20%\">Choose Printhead Type</td>\n");
	HSSend (handle, "<td width=\"20%\"><select name=ph_type size=1>");
	na_phlist (handle);
	HSSend (handle, "</select></td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "</tbody></table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=3><b>Printer Installation</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right rowspan=2>Direction</td><td><input type=radio name=dir ");
	na_DIRrtol (handle);
	HSSend (handle, " value=RtoL>Right to Left</td><td rowspan=2>Direction of conveyor movement - looking from behind printer towards the conveyor</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=dir ");
	na_DIRltor (handle);
	HSSend (handle, " value=LtoR>Left to Right</td></tr>\n");

	HSSend (handle, "<td><input type=checkbox name=bidirectional ");
	na_BiDirectional (handle);
	HSSend (handle, ">Bi-directional print</td>\n");

	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right>Distance</td><td><input type=text onchange=\"return parmFCheck(this,0,'Distance');\" name=prdist size=6 value=\"");
	na_PRDIST (handle);
	HSSend (handle, "\">");
	na_PRUNIT (handle);
	HSSend (handle, "</td><td>Distance of Printer Nozzle 0 from Photo Cell</td></tr>\n");
	HSSend (handle, "</tbody>\n");

	HSSend (handle, "<tr><td align=right>Distance</td><td><input type=text onchange=\"return parmFCheck(this,0,'Distance');\" name=prdist2 size=6 value=\"");
	na_PRDIST2 (handle);
	HSSend (handle, "\">");
	na_PRUNIT (handle);
	HSSend (handle, "</td><td>(Bi-directional) Distance of Printer Nozzle 0 from Photo Cell</td></tr>\n");
	HSSend (handle, "</tbody>\n");

	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right>Height</td><td><input type=text onchange=\"return parmFCheck(this,0,'Height');\" name=prhgt size=6 value=\"");
	na_PRHGT (handle);
	HSSend (handle, "\">");
	na_PRUNIT (handle);
	HSSend (handle, "</td><td>Height of Printer Nozzle 0 above Conveyor</td></tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right>Slant Value</td><td><input type=text onchange=\"return parmCheck(this,0,31,'Slant Value');\" name=prslt size=6 value=\"");
	na_PRSLT (handle);
	HSSend (handle, "\"></td><td>Slant Value to get vertical print from Printer. 0 for vertical printer.<button onClick=\"showslant()\" class=fjbtn1 type=button>Show Slant/Angle table</button></td></tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right rowspan=2>Photo&nbsp;Cell&nbsp;Connection</td><td><input type=radio name=pc ");
	na_PCHead (handle);
	HSSend (handle, " value=head>Photo Cell at Print Head</td><td rowspan=2>Location of Photo Cell connector</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=pc ");
	na_PCback (handle);
	HSSend (handle, " value=back>Cable at rear</td></tr>\n");
	HSSend (handle, "\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td></td>\n");
	HSSend (handle, "<td><input type=checkbox name=pcint ");
	na_PCInternal (handle);
	HSSend (handle, ">Internal photocell</td>\n");
	HSSend (handle, "\n");
	HSSend (handle, "<td rowspan=2> \n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "	<td align=right>Auto print</td>\n");
	HSSend (handle, "	<td><input type=text onchange=\"return parmFDCheck(this,6,144,'AutoPrint');\" name=autoprint size=6 value=\"");
	na_AUTOPRINT (handle);
	HSSend (handle, "\">in</td>\n");
	HSSend (handle, "	<td></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=9><b>Serial Port</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td>Data Rate</td><td><select size=1 name=baudrate><option selected>1200</option><option>2400</option><option>4800</option><option>9600</option><option>19200</option><option>38400</option><option>57600</option><option>115200</option></select>&nbsp;&nbsp;bits per second</td><td colspan=7>&nbsp;</td></tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td rowspan=5>Use</td><td colspan=8><input type=radio name=ser ");
	na_SERnone (handle);
	HSSend (handle, " value=none>None</td></tr>\n");
	HSSend (handle, "<tr><td colspan=8><input type=radio name=ser ");
	na_SERvardata (handle);
	HSSend (handle, " value=vardata>Variable Data</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=ser ");
	na_SERdynmem (handle);
	HSSend (handle, " value=dynmem>Dynamic memory</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=ser ");
	na_SERlabel (handle);
	HSSend (handle, " value=label>Scanner to select Label</td><td colspan=7><input type=checkbox name=scanonce ");
	na_SCANONCE (handle);
	HSSend (handle, ">Use Scanner input once</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=ser ");
	na_SERinfo (handle);
	HSSend (handle, " value=info>System Information</td>\n");
	HSSend (handle, "<td><input type=checkbox name=infoStat ");
	na_INFOSTAT (handle);
	HSSend (handle, ">Status</td><td><input type=checkbox name=infoSock ");
	na_INFOSOCK (handle);
	HSSend (handle, ">Sockets</td><td><input type=checkbox name=infoRAM ");
	na_INFORAM (handle);
	HSSend (handle, ">RAM</td><td><input type=checkbox name=infoROM ");
	na_INFOROM (handle);
	HSSend (handle, ">ROM</td><td><input type=checkbox name=infoName ");
	na_INFONAME (handle);
	HSSend (handle, ">Name registration</td><td><input type=checkbox name=infoTime ");
	na_INFOTIME (handle);
	HSSend (handle, ">Time</td> <td><input type=checkbox name=infoAD ");
	na_INFOAD (handle);
	HSSend (handle, ">A/D info</td>\n");
	
	HSSend (handle, "<td><input type=checkbox name=sop ");
	na_SOP (handle);
	HSSend (handle, ">Start-of-print</td>\n");
	
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "");
	na_PHTFEATURE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">setbaud();</SCRIPT>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=4><b>Shaft Encoder</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td rowspan=2 align=right>Yes<input type=radio name=se ");
	na_SEYes (handle);
	HSSend (handle, " value=yes></td><td>Required</td> <td><input type=text name=sey_res maxlength=6 size=6 value=\"");
	na_SEYres (handle);
	HSSend (handle, "\" onChange=\"seyfreq();seydiv();\"></td><td>Shaft encoder resolution - dots per inch</td></tr>\n");
	HSSend (handle, "<tr><td>Required</td><td><select name=sey_freqdiv size=1 onChange=\"return seyfreq();\"><option value=1 ");
	na_DIV1 (handle);
	HSSend (handle, " selected>1</option><option value=2 ");
	na_DIV2 (handle);
	HSSend (handle, " >2</option><option value=3 ");
	na_DIV3 (handle);
	HSSend (handle, " >3</option><option value=4 ");
	na_DIV4 (handle);
	HSSend (handle, " >4</option></select></td><td>Printer resolution - dots per inch <input type=hidden name=sey_speed maxlength=6 size=6 value=\"");
	na_SEYspeed (handle);
	HSSend (handle, "\" onChange=\"return seyfreq()\"><input type=hidden name=sey_freq maxlength=6 size=6 value=\"");
	na_SEYfreq (handle);
	HSSend (handle, "\" DISABLED></td></tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td rowspan=3 align=right>No<input type=radio name=se ");
	na_SENo (handle);
	HSSend (handle, " value=no></td><td>Required</td><td><input type=text name=sen_res maxlength=6 size=6 value=\"");
	na_SEres (handle);
	HSSend (handle, "\" onChange=\"return senfreq()\"></td><td>Printer resolution - dots per inch</td></tr>\n");
	HSSend (handle, "<tr><td>Required</td><td><input type=text name=sen_speed maxlength=6 size=6 value=\"");
	na_SEspeed (handle);
	HSSend (handle, "\" onChange=\"return senfreq()\"></td><td>Conveyor speed - feet per minute</td></tr>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td><input type=text name=sen_freq maxlength=6 size=6 value=\"");
	na_SEfreq (handle);
	HSSend (handle, "\" DISABLED ><font color=#808080></font></td><td>Printer frequency - dots per\n");
	HSSend (handle, "second</FONT></td></tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<tr><td><input type=\"hidden\" value=\"none\" name=\"action\">\n");
	HSSend (handle, "<center><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, "><button class=fjbtn1 onClick=\"mysubmit(this.form)\" type=button>Submit</button><button class=fjbtn1 onClick=\"myreset(this.form)\" type=button>Reset</button></center>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">senfreq();seydiv();seyfreq();</SCRIPT>\n");
	HSSend (handle, "</HTML>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\printerconfig.htm }} */
/* @! End HTML page   */

void reload_printerconfig(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=printerconfig.htm");
	}
	else
	{
		fj_reload_page(handle, "printerconfig.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_printerlayout_1  {{ */
void Post_printerlayout_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM       pBRAM       = pCVT->pBRAM;
	LPFJ_GLOBAL     pGlobal     = pCVT->pFJglobal;
	LPFJ_NETOSPARAM pParamNETOS = pCVT->pNETOSparam;
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfphP       = &(pParam->FJPh);
	LPFJPRINTHEAD   pfph        = pCVT->pfph;
	LPFJSYSTEM      pfsys       = pCVT->pfsys;
	LPFJSYSTEM      pfsysP      = &(pParam->FJSys);
	int    retrom;
	char   formField[20];
	BOOL   bROMWait;
	BOOL   bChangedFJ;
	BOOL   bChangedNETOS;
	BOOL   bChangedSerial;
	LONG   lBaud;
	BOOL   bSerialScannerLabelUseOnce;
	LONG   bSOP;
	BOOL   bVarData;
	BOOL   bSerialDynMem;
	BOOL   bSerialLabel;
	BOOL   bSerialInfo;
	BOOL   bInfoStatus;
	BOOL   bInfoSocket;
	BOOL   bInfoRAM;
	BOOL   bInfoROM;
	BOOL   bInfoName;
	BOOL   bInfoTime;
	BOOL   bInfoAD;
	BOOL   bPhotocellBack;
	LONG   lDirection;
	FLOAT  fDistancePC;
	FLOAT  fDistancePC2;
	FLOAT  fHeight0;
	FLOAT  fSlantAngle;
	LONG   lSlant;
	FLOAT  fAMSInterval;
	LONG   lAMSInactive;
	BOOL   bAPSenable;
	FLOAT  fStandByInactive;
	BOOL   bStandByEnable;
	STRING sName[1+FJLABEL_NAME_SIZE];
	char   grname[20];
	int    ret;
	int    index;
	BOOL   bValue;
	FLOAT  fValue;
	LONG   lValue;
	LPSTR  pKey;
	LONG GAver;
	BYTE GA_Mver;
	BYTE GA_Lver;
	BOOL bPhotocellInternal;			// Burl v 1.1015
	LONG lAutoPrint;					// Burl v 1.1015
	FLOAT fAutoPrint;					// Burl v 1.1015
	BOOL bBiDirectional;

	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if (0 == strcmp(formField,"runams"))
		{
										// set AMS start time to now
			pCVT->pBRAM->lAMS_SST = pGlobal->lSeconds;
			reload_printerconfig(handle,FALSE);
			return;
		}
		else if (0 == strcmp(formField,"standby"))
		{
			if ( FALSE == pGlobal->bStandByOn )
			{
				pGlobal->bStandByOn = TRUE;
				fj_print_head_ScheduleStandBy();
				if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
				{
					pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
					pCVT->fj_print_head_SendPhotocellMode();
				}
			}
			else
			{
				pGlobal->bStandByOn = FALSE;
				fj_print_head_ScheduleStandBy();
				if ( FALSE == pBRAM->bPrintIdle &&
					0 != pBRAM->strLabelName[0] &&
					FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
				{
					pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
					pCVT->fj_print_head_SendPhotocellMode();
				}
			}
			reload_printerconfig(handle,FALSE);
			return;
		}
	}

	// get ROM structure
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	bChangedNETOS = FALSE;
	bROMWait = FALSE;
	retrom = pCVT->fj_ReadNETOSRomParams( pParamNETOS );
	if ( 0 == retrom  )
	{
		lBaud    = pParamNETOS->baudrate;

		ret = fj_HSGetValue( handle, "baudrate", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lBaud = atoi( formField );
		}

		if ( lBaud != pParamNETOS->baudrate )
		{
			pParamNETOS->baudrate = lBaud;
			bChangedNETOS = TRUE;
			bChangedSerial = TRUE;
		}
	}

	// get ROM structure
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	bChangedFJ = FALSE;
	if ( 0 == retrom  )
	{
		// Start - PH config code
		ret = fj_HSGetValue( handle, "ph_type", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			index = atoi(formField);
			if ( index != pfph->lFJPhOpIndex )
			{
				pfph->lFJPhOpIndex = index;
				pParam->FJPh.lFJPhOpIndex = index;
				if ( index < pCVT->lFJPhOpCount )
				{
					pfph->bHeadSelected = TRUE;
					pParam->FJPh.bHeadSelected = TRUE;
					memcpy( &(pParam->FJPhOp ), &(*(pCVT->paFJPhOp ))[index], sizeof(FJPRINTHEAD_OP ) );
					index = pParam->FJPhOp.lPhyTable;
					memcpy( &(pParam->FJPhPhy), &(*(pCVT->paFJPhPhy))[index], sizeof(FJPRINTHEAD_PHY) );
					index = pParam->FJPhOp.lPkgTable;
					memcpy( &(pParam->FJPhPkg), &(*(pCVT->paFJPhPkg))[index], sizeof(FJPRINTHEAD_PKG) );
				}
				else if ( 999 == index)
				{
					memset( &(pParam->FJPhOp ), 0, sizeof(FJPRINTHEAD_OP ) );
					memset( &(pParam->FJPhPhy), 0, sizeof(FJPRINTHEAD_PHY) );
					memset( &(pParam->FJPhPkg), 0, sizeof(FJPRINTHEAD_PKG) );
					pfph->bHeadSelected = FALSE;
					pParam->FJPh.bHeadSelected = FALSE;
				}

				bChangedFJ = TRUE;
			}
		}
		// End - PH config code

		// Start - SE code
		ret = fj_HSGetValue( handle, "sey_res", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fValue = atof( formField );
			if ( fValue != pfsysP->fEncoderWheel )
			{
				pfsysP->fEncoderWheel = fValue;
				bChangedFJ = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "sey_freqdiv", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atoi( formField );
			if ( lValue != pfsysP->lEncoderDivisor )
			{
				pfsysP->lEncoderDivisor = lValue;
				bChangedFJ = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "sen_res", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fValue = atof( formField );
			if ( fValue != pfsysP->fPrintResolution )
			{
				pfsysP->fPrintResolution = fValue;
				bChangedFJ = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "se", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if( 0 == strcmp("yes",formField) )
			{
				bValue = TRUE;
				pKey = "sen_speed";
			}
			if( 0 == strcmp("no",formField) )
			{
				bValue = FALSE;
				pKey = "sen_speed";
			}
			if ( bValue != pfsysP->bShaftEncoder )
			{
				pfsysP->bShaftEncoder = bValue;
				bChangedFJ = TRUE;
			}
			ret = fj_HSGetValue( handle, pKey, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				fValue = atof( formField ) * 12. / 60.;
				if ( fValue != pfsysP->fSpeed )
				{
					pfsysP->fSpeed = fValue;
					bChangedFJ = TRUE;
				}
			}

			if ( TRUE == bChangedFJ)
			{
				GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
				GA_Mver = ((GAver>>4)& 0x0F);
				GA_Lver = (GAver & 0x0F);
				if ( 1 <= GA_Mver )
				{
					if ( 1 == GA_Mver && 3 > GA_Lver)
						pfsysP->fEncoder = pfsysP->fEncoderWheel / pfsysP->lEncoderDivisor;
					else
						pfsysP->fEncoder = pfsysP->fEncoderWheel * 2 / pfsysP->lEncoderDivisor;
				}
				else
					pfsysP->fEncoder = pfsysP->fEncoderWheel / pfsysP->lEncoderDivisor;
			}
		}
		// End - SE code

		bBiDirectional = pfph->bBiDirectional;
		lDirection   = pfph->lDirection;
		fDistancePC  = pfph->fDistancePC;
		fDistancePC2 = pfph->fDistancePC2;
		fHeight0     = pfph->fHeight0;
		fSlantAngle  = pfph->fSlantAngle;
		lSlant       = pfph->lSlant;
		bPhotocellBack = pfph->bPhotocellBack;
										// Burl v 1.1015
		bPhotocellInternal = pfph->bPhotocellInternal;
										// Burl v 1.1015
		lAutoPrint   = pfph->pfop->lAutoPrint;
		bSerialScannerLabelUseOnce    = pfph->bSerialScannerLabelUseOnce;
		bSOP         = pfph->bSOP;
		bSerialLabel = pfph->bSerialScannerLabel;
		bVarData     = pfph->bSerialVarData;
		bSerialDynMem= pfph->bSerialDynMem;
		bSerialInfo  = pfph->bSerialInfo;
		bInfoStatus  = pfph->bInfoStatus;
		bInfoSocket  = pfph->bInfoSocket;
		bInfoRAM     = pfph->bInfoRAM;
		bInfoROM     = pfph->bInfoROM;
		bInfoName    = pfph->bInfoName;
		bInfoTime    = pfph->bInfoTime;
		bInfoAD      = pfph->bInfoAD;
		fAMSInterval = pfph->fAMSInterval;
		lAMSInactive = pfph->lAMSInactive;
		bAPSenable   = pfph->bAPSenable;
		fStandByInactive = pfph->fStandByInactive;
		bStandByEnable   = pfph->bStandByEnable;

		ret = fj_HSGetValue( handle, "dir", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "RtoL" ) )
			{
				lDirection = FJPH_DIRECTION_RIGHT_TO_LEFT;
			}
			if ( 0 == strcmp( formField, "LtoR" ) )
			{
				lDirection = FJPH_DIRECTION_LEFT_TO_RIGHT;
			}
		}

		ret = fj_HSGetValue( handle, "bidirectional", formField, sizeof(formField) );
		if ( 0 < ret ) bBiDirectional = TRUE;
		else           bBiDirectional = FALSE;

		ret = fj_HSGetValue( handle, "prdist", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fDistancePC = atof( formField );
			fDistancePC = pCVT->fj_FloatUnits( fDistancePC, - pCVT->pfsys->lUnits );
		}
		ret = fj_HSGetValue( handle, "prdist2", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fDistancePC2 = atof( formField );
			fDistancePC2 = pCVT->fj_FloatUnits( fDistancePC2, - pCVT->pfsys->lUnits );
		}
		ret = fj_HSGetValue( handle, "prhgt", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fHeight0 = atof( formField );
			fHeight0 = pCVT->fj_FloatUnits( fHeight0, - pCVT->pfsys->lUnits );
		}
		ret = fj_HSGetValue( handle, "prang", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fSlantAngle = atof( formField );
		}
		ret = fj_HSGetValue( handle, "prslt", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lSlant = atoi( formField );
		}
		ret = fj_HSGetValue( handle, "pc", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "head" ) )
			{
				bPhotocellBack = FALSE;
			}
			if ( 0 == strcmp( formField, "back" ) )
			{
				bPhotocellBack = TRUE;
			}
		}

		ret = fj_HSGetValue( handle, "ser", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			bSerialInfo   = FALSE;
			bSerialLabel  = FALSE;
			bVarData      = FALSE;
			bSerialDynMem = FALSE;
			
			if ( 0 == strcmp( formField, "label" ) )
			{
				bSerialLabel = TRUE;
			}
			else if ( 0 == strcmp( formField, "info" ) )
			{
				bSerialInfo = TRUE;
			}
			else if ( 0 == strcmp( formField, "vardata" ) )
			{
				bVarData = TRUE;
			}
			else if ( 0 == strcmp( formField, "dynmem" ) )
			{
				bSerialDynMem = TRUE;
			}
		}

		ret = fj_HSGetValue( handle, "scanonce", formField, sizeof(formField) );
		if ( 0 < ret ) bSerialScannerLabelUseOnce = TRUE;
		else           bSerialScannerLabelUseOnce = FALSE;

		ret = fj_HSGetValue( handle, "infoStat", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoStatus = TRUE;
		else           bInfoStatus = FALSE;

		ret = fj_HSGetValue( handle, "infoSock", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoSocket = TRUE;
		else           bInfoSocket = FALSE;

		ret = fj_HSGetValue( handle, "infoRAM", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoRAM = TRUE;
		else           bInfoRAM = FALSE;

		ret = fj_HSGetValue( handle, "infoROM", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoROM = TRUE;
		else           bInfoROM = FALSE;

		ret = fj_HSGetValue( handle, "infoName", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoName = TRUE;
		else           bInfoName = FALSE;

		ret = fj_HSGetValue( handle, "infoTime", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoTime = TRUE;
		else           bInfoTime = FALSE;

		ret = fj_HSGetValue( handle, "infoAD", formField, sizeof(formField) );
		if ( 0 < ret ) bInfoAD = TRUE;
		else           bInfoAD = FALSE;

		ret = fj_HSGetValue( handle, "sop", formField, sizeof(formField) );
		if ( 0 < ret ) bSOP = TRUE;
		else           bSOP = FALSE;

		ret = fj_HSGetValue( handle, "ams_inter", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fAMSInterval = atof( formField );
		}

		ret = fj_HSGetValue( handle, "ams_inact", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lAMSInactive = atoi( formField );
		}
		ret = fj_HSGetValue( handle, "ams_mode", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "yes" ) ) bAPSenable = TRUE;
			else                                   bAPSenable = FALSE;
		}
		ret = fj_HSGetValue( handle, "stdb_in", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fStandByInactive = atof( formField );
		}
		ret = fj_HSGetValue( handle, "stdb_mode", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "yes" ) ) bStandByEnable = TRUE;
			else                                   bStandByEnable = FALSE;
		}

										// Burl v 1.1015
		ret = fj_HSGetValue( handle, "pcint", formField, sizeof(formField) );
										// Burl v 1.1015
		if ( 0 < ret ) bPhotocellInternal = TRUE;
										// Burl v 1.1015
		else           bPhotocellInternal = FALSE;

										// Burl v 1.1015
		ret = fj_HSGetValue( handle, "autoprint", formField, sizeof(formField) );
		if ( 0 < ret )					// Burl v 1.1015
		{								// Burl v 1.1015
										// Burl v 1.1015
			fAutoPrint = atof( formField );
										// Burl v 1.1015
			fAutoPrint = pCVT->fj_FloatUnits( fAutoPrint, - pCVT->pfsys->lUnits );
										// Burl v 1.1015
			lAutoPrint = (LONG)(fAutoPrint * pfsysP->fPrintResolution);
		}								// Burl v 1.1015

		if ( bAPSenable != pfphP->bAPSenable )
		{
			pfphP->bAPSenable = bAPSenable;
			bChangedFJ = TRUE;
		}

		if ( lDirection != pfphP->lDirection )
		{
			pfphP->lDirection = lDirection;
			bChangedFJ = TRUE;
		}
		if (bBiDirectional != pfphP->bBiDirectional) {
			pfphP->bBiDirectional = bBiDirectional;
			bChangedFJ = TRUE;
		}
		if ( fDistancePC != pfphP->fDistancePC )
		{
			pfphP->fDistancePC1 = pfphP->fDistancePC = fDistancePC;
			bChangedFJ = TRUE;
		}
		if ( fDistancePC2 != pfphP->fDistancePC2 )
		{
			pfphP->fDistancePC2 = fDistancePC2;
			bChangedFJ = TRUE;
		}
		if ( fHeight0 != pfphP->fHeight0 )
		{
			pfphP->fHeight0 = fHeight0;
			bChangedFJ = TRUE;
		}
		if ( fSlantAngle != pfphP->fSlantAngle )
		{
			pfphP->fSlantAngle = fSlantAngle;
			bChangedFJ = TRUE;
		}
		if ( lSlant != pfphP->lSlant )
		{
			pfphP->lSlant = lSlant;
			bChangedFJ = TRUE;
		}
		if ( bPhotocellBack != pfphP->bPhotocellBack )
		{
			pfphP->bPhotocellBack = bPhotocellBack;
			bChangedFJ = TRUE;
		}
										// Burl v 1.1015
		if ( bPhotocellInternal != pfphP->bPhotocellInternal )
		{								// Burl v 1.1015
										// Burl v 1.1015
			pfphP->bPhotocellInternal = bPhotocellInternal;
			bChangedFJ = TRUE;			// Burl v 1.1015
		}								// Burl v 1.1015
										// Burl v 1.1015
		if ( lAutoPrint != pfphP->pfop->lAutoPrint )
		{								// Burl v 1.1015
										// Burl v 1.1015
			pfphP->pfop->lAutoPrint = lAutoPrint;
			bChangedFJ = TRUE;			// Burl v 1.1015
		}								// Burl v 1.1015
		if ( bSerialLabel != pfphP->bSerialScannerLabel )
		{
			pfphP->bSerialScannerLabel = bSerialLabel;
			bChangedFJ = TRUE;
			bChangedSerial = TRUE;
		}
		if ( bSerialInfo != pfphP->bSerialInfo )
		{
			pfphP->bSerialInfo = bSerialInfo;
			bChangedFJ = TRUE;
			bChangedSerial = TRUE;
		}
		if ( bSerialScannerLabelUseOnce != pfphP->bSerialScannerLabelUseOnce )
		{
			pfphP->bSerialScannerLabelUseOnce = bSerialScannerLabelUseOnce;
			bChangedFJ = TRUE;
		}
		if ( bSOP != pfphP->bSOP )
		{
			pfphP->bSOP = bSOP;
			bChangedFJ = TRUE;
		}
		if ( bVarData != pfphP->bSerialVarData )
		{
			pfphP->bSerialVarData = bVarData;
			bChangedFJ = TRUE;
		}
		if ( bSerialDynMem != pfphP->bSerialDynMem )
		{
			pfphP->bSerialDynMem = bSerialDynMem;
			bChangedFJ = TRUE;
		}
		if ( bInfoStatus != pfphP->bInfoStatus )
		{
			pfphP->bInfoStatus = bInfoStatus;
			bChangedFJ = TRUE;
		}
		if ( bInfoSocket != pfphP->bInfoSocket )
		{
			pfphP->bInfoSocket = bInfoSocket;
			bChangedFJ = TRUE;
		}
		if ( bInfoRAM != pfphP->bInfoRAM )
		{
			pfphP->bInfoRAM = bInfoRAM;
			bChangedFJ = TRUE;
		}
		if ( bInfoROM != pfphP->bInfoROM )
		{
			pfphP->bInfoROM = bInfoROM;
			bChangedFJ = TRUE;
		}
		if ( bInfoName != pfphP->bInfoName )
		{
			pfphP->bInfoName = bInfoName;
			bChangedFJ = TRUE;
		}
		if ( bInfoTime != pfphP->bInfoTime )
		{
			pfphP->bInfoTime = bInfoTime;
			bChangedFJ = TRUE;
		}
		if ( bInfoAD != pfphP->bInfoAD )
		{
			pfphP->bInfoAD = bInfoAD;
			bChangedFJ = TRUE;
		}
		if ( fAMSInterval != pfphP->fAMSInterval )
		{
			pfphP->fAMSInterval = fAMSInterval;
			bChangedFJ = TRUE;
		}
		if ( lAMSInactive != pfphP->lAMSInactive )
		{
			pfphP->lAMSInactive = lAMSInactive;
			bChangedFJ = TRUE;
		}
		if ( fStandByInactive != pfphP->fStandByInactive )
		{
			pfphP->fStandByInactive = fStandByInactive;
			bChangedFJ = TRUE;
			fj_print_head_ScheduleStandBy();
		}
		if ( bStandByEnable != pfphP->bStandByEnable )
		{
			pfphP->bStandByEnable = bStandByEnable;
			bChangedFJ = TRUE;
		}

	}

	// write ROM structure
	if ( TRUE == bChangedNETOS )
	{
		// write and keep ROM lock
		pCVT->fj_WriteNETOSRomParams( pParamNETOS, !bChangedFJ, NULL, 0 );
		bROMWait = TRUE;
	}

	// write ROM structure
	if ( TRUE == bChangedFJ )
	{
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		retrom = 0;
		if ( 0 == retrom )
		{
			memcpy( pfph->pfop , &(pParam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
			memcpy( pfph->pfphy, &(pParam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
			memcpy( pfph->pfpkg, &(pParam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );

			pfph->bSerialScannerLabel 	 = FALSE;
			pfph->bSerialInfo   	  	 = FALSE;
			pfph->bSerialVarData 		 = FALSE;
			pfph->bSerialDynMem       	 = FALSE;
			pfph->bSerialScannerLabelUseOnce = FALSE;
			
			pfph->bSerialScannerLabel 	 = pfphP->bSerialScannerLabel;
			pfph->bSerialInfo   	  	 = pfphP->bSerialInfo;
			pfph->bSerialVarData 		 = pfphP->bSerialVarData;
			pfph->bSerialDynMem       	 = pfphP->bSerialDynMem;
			pfph->bSerialScannerLabelUseOnce = pfphP->bSerialScannerLabelUseOnce;
			
			pfph->lDirection    = pfphP->lDirection;
			pfph->bBiDirectional = pfphP->bBiDirectional;
			pfph->fDistancePC1 = pfph->fDistancePC   = pfphP->fDistancePC;
			pfph->fDistancePC2 = pfphP->fDistancePC2;
			pfph->fHeight0      = pfphP->fHeight0;
			pfph->fSlantAngle   = pfphP->fSlantAngle;
			pfph->lSlant        = pfphP->lSlant;
			pfph->bPhotocellBack        = pfphP->bPhotocellBack;
										// Burl v 1.1015
			pfph->bPhotocellInternal    = pfphP->bPhotocellInternal;
										// Burl v 1.1015
			pfph->pfop->lAutoPrint      = pfphP->pfop->lAutoPrint;
			pfph->bInfoStatus   = pfphP->bInfoStatus;
			pfph->bInfoSocket   = pfphP->bInfoSocket;
			pfph->bInfoRAM      = pfphP->bInfoRAM;
			pfph->bInfoROM      = pfphP->bInfoROM;
			pfph->bInfoName     = pfphP->bInfoName;
			pfph->bInfoTime     = pfphP->bInfoTime;
			pfph->bInfoAD       = pfphP->bInfoAD;
			pfph->fAMSInterval  = pfphP->fAMSInterval;
			pfph->lAMSInactive  = pfphP->lAMSInactive;
			pfph->bAPSenable    = pfphP->bAPSenable;
			pfph->fStandByInactive  = pfphP->fStandByInactive;
			pfph->bStandByEnable    = pfphP->bStandByEnable;
			bROMWait = TRUE;

			pfph->bSOP 	    = pfphP->bSOP;

			if (!pfph->bSOP)
			    pfph->lSOP = 0;
			
			pfsys->bShaftEncoder    = pfsysP->bShaftEncoder;
			pfsys->fSpeed           = pfsysP->fSpeed;
			pfsys->fEncoder         = pfsysP->fEncoder;
			pfsys->fEncoderWheel    = pfsysP->fEncoderWheel;
			pfsys->lEncoderDivisor  = pfsysP->lEncoderDivisor;
			pfsys->fPrintResolution = pfsysP->fPrintResolution;

			pCVT->fj_PrintHeadReset( pfph );
			pCVT->fj_print_head_KillENI();
										// sent encoder value
			pCVT->fj_print_head_SetGAEncoder();
										// set encoder switch
			pCVT->fj_print_head_SetGAWriteSwitches();
			pCVT->fj_print_head_InitENI();

			pCVT->fj_print_head_SetGAValues();

			pCVT->fj_print_head_setPWMVoltageValue();
			pCVT->fj_print_head_SetStatus();
			pCVT->fj_print_head_SendInfoAll();

			strcpy( sName, pCVT->pBRAM->strLabelName );
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			pCVT->fj_SystemBuildSelectedLabel( pCVT->pfsys, sName );
		}
	}

	if ( (FALSE == bChangedNETOS) && (FALSE == bChangedFJ) )
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	if ( TRUE == bChangedSerial )
	{
		// the new baudrate was written into ROM and completed
		// the serial port routines read parameters fom ROM
		// close and open the serial port to set the new baud rate
		pCVT->fj_closeSerialPort();
		pCVT->fj_openSerialPort( pfph );
	}

	reload_printerconfig(handle,bROMWait);
}										/* @! Function: Post_printerlayout_1  }} */
/* @! Function: Post_systemlayout_1  {{ */
void Post_systemlayout_1(unsigned long handle)
{
	return;
}										/* @! Function: Post_systemlayout_1  }} */
/* @! Function: Post_manu_2  {{ */
void Post_manu_2(unsigned long handle)
{
	return;
}										/* @! Function: Post_manu_2  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_SERIAL_BAUD  {{ */
void na_SERIAL_BAUD(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pNETOSparam->baudrate);
}										/* @! Function: na_SERIAL_BAUD  }} */
/* @! Function: na_SERnone  {{ */
void na_SERnone(unsigned long handle)
{
    if (!pCVT->pfph->bSerialScannerLabel &&
	!pCVT->pfph->bSerialInfo &&
	!pCVT->pfph->bSerialVarData &&
	!pCVT->pfph->bSerialDynMem)
    {
	HSSend (handle, "CHECKED");
    }
}										/* @! Function: na_SERnone  }} */
/* @! Function: na_SERlabel  {{ */
void na_SERlabel(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bSerialScannerLabel ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SERlabel  }} */
/* @! Function: na_SERinfo  {{ */
void na_SERinfo(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bSerialInfo ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SERinfo  }} */
/* @! Function: na_DIRrtol  {{ */
void na_DIRrtol(unsigned long handle)
{
	/* add your implementation here: */
	if ( FJPH_DIRECTION_RIGHT_TO_LEFT == pCVT->pfph->lDirection ) HSSend (handle, "CHECKED");
}										/* @! Function: na_DIRrtol  }} */
/* @! Function: na_DIRltor  {{ */
void na_DIRltor(unsigned long handle)
{
	/* add your implementation here: */
	if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pCVT->pfph->lDirection ) HSSend (handle, "CHECKED");
}										/* @! Function: na_DIRltor  }} */
/* @! Function: na_PRDIST  {{ */
void na_PRDIST(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (double)pCVT->fj_FloatUnits( pCVT->pfph->fDistancePC, pCVT->pfsys->lUnits), 2 );
}										/* @! Function: na_PRDIST  }} */
/* @! Function: na_PRDIST2  {{ */
void na_PRDIST2(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (double)pCVT->fj_FloatUnits( pCVT->pfph->fDistancePC2, pCVT->pfsys->lUnits), 2 );
}										/* @! Function: na_PRDIST2  }} */
/* @! Function: na_PRHGT  {{ */
void na_PRHGT(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (double)pCVT->fj_FloatUnits( pCVT->pfph->fHeight0, pCVT->pfsys->lUnits), 2 );
}										/* @! Function: na_PRHGT  }} */
/* @! Function: na_PRANG  {{ */
void na_PRANG(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfph->fSlantAngle, 2 );
}										/* @! Function: na_PRANG  }} */
/* @! Function: na_PRSLT  {{ */
void na_PRSLT(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pfph->lSlant );
}										/* @! Function: na_PRSLT  }} */
/* @! Function: na_TITLE_CF_PRINTER_LAYOUT  {{ */
void na_TITLE_CF_PRINTER_LAYOUT(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Printer" );
}										/* @! Function: na_TITLE_CF_PRINTER_LAYOUT  }} */
/* @! Function: na_DHCPyes  {{ */

void na_PRAMSINTER(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (DOUBLE)pCVT->pfph->fAMSInterval, 2);
}										/* @! Function: na_PRAMSINTER  }} */
/* @! Function: na_PRAMSINACT  {{ */
void na_PRAMSINACT(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pfph->lAMSInactive);
}										/* @! Function: na_PRAMSINACT  }} */
/* @! Function: na_PRUNIT  {{ */
void na_PRUNIT(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, (*(pCVT->pfj_aUnits))[pCVT->pfsys->lUnits].pShort);
}										/* @! Function: na_PRUNIT  }} */
/* @! Function: na_INFOSTAT  {{ */
void na_INFOSTAT(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoStatus ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFOSTAT  }} */
/* @! Function: na_INFOSOCK  {{ */
void na_INFOSOCK(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoSocket ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFOSOCK  }} */
/* @! Function: na_INFORAM  {{ */
void na_INFORAM(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoRAM ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFORAM  }} */
/* @! Function: na_INFOROM  {{ */
void na_INFOROM(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoROM ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFOROM  }} */
/* @! Function: na_INFONAME  {{ */
void na_INFONAME(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoName ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFONAME  }} */
/* @! Function: na_INFOTIME  {{ */
void na_INFOTIME(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoTime ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFOTIME  }} */
/* @! Function: na_INFOAD  {{ */
void na_INFOAD(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bInfoAD ) HSSend (handle, "CHECKED");
}										/* @! Function: na_INFOAD  }} */
/* @! Function: na_PCHead  {{ */
void na_PCHead(unsigned long handle)
{
	/* add your implementation here: */
	if ( FALSE == pCVT->pfph->bPhotocellBack ) HSSend (handle, "CHECKED");
}										/* @! Function: na_PCHead  }} */
/* @! Function: na_PCback  {{ */
void na_PCback(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bPhotocellBack ) HSSend (handle, "CHECKED");
}										/* @! Function: na_PCback  }} */
/* @! Function: na_SCANONCE  {{ */
void na_SCANONCE(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bSerialScannerLabelUseOnce ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SCANONCE  }} */
/* @! Function: na_SOP  {{ */
void na_SOP(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfph->bSOP ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SCANONCE  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_SEYes  {{ */
void na_SEYes(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfsys->bShaftEncoder ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SEYes  }} */
/* @! Function: na_DIV1  {{ */
void na_DIV1(unsigned long handle)
{
	/* add your implementation here: */
	if ( 1 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV1  }} */
/* @! Function: na_DIV2  {{ */
void na_DIV2(unsigned long handle)
{
	/* add your implementation here: */
	if ( 2 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV2  }} */
/* @! Function: na_DIV3  {{ */
void na_DIV3(unsigned long handle)
{
	/* add your implementation here: */
	if ( 3 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV3  }} */
/* @! Function: na_DIV4  {{ */
void na_DIV4(unsigned long handle)
{
	/* add your implementation here: */
	if ( 4 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV4  }} */
/* @! Function: na_SEYspeed  {{ */
void na_SEYres(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fEncoderWheel, 2 );
}										/* @! Function: na_SEYres  }} */
/* @! Function: na_SEYspeed  {{ */
void na_SEYspeed(unsigned long handle)
{
	/* add your implementation here: */
	na_SEspeed(handle);
}										/* @! Function: na_SEYspeed  }} */
/* @! Function: na_SEYfreq  {{ */
void na_SEYfreq(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * pCVT->pfsys->fEncoder / (FLOAT)pCVT->pfsys->lEncoderDivisor * 12. /60.), 2 );
}										/* @! Function: na_SEYfreq  }} */
/* @! Function: na_SENo  {{ */
void na_SENo(unsigned long handle)
{
	/* add your implementation here: */
	if ( FALSE == pCVT->pfsys->bShaftEncoder ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SENo  }} */
/* @! Function: na_SEres  {{ */
void na_SEres(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fPrintResolution, 2 );
}										/* @! Function: na_SEres  }} */
/* @! Function: na_SEspeed  {{ */
void na_SEspeed(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * 60. / 12.), 2 );
}										/* @! Function: na_SEspeed  }} */
/* @! Function: na_SEfreq  {{ */
void na_SEfreq(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * pCVT->pfsys->fPrintResolution * 12. / 60.), 2 );
}										/* @! Function: na_SEfreq  }} */
/* @! Function: na_phlist  {{ */
void na_phlist(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	int i;
	CHAR str[120];

	HSSend (handle, "<option value=\"999\">Disable</option>\n");
	for ( i=0; i<pCVT->lFJPhOpCount; i++)
	{
		HSSend (handle, "<option ");
		if ( i == pfph->lFJPhOpIndex )HSSend (handle, "SELECTED");
		sprintf(str," value=\"%i\">%s</option>\n",i,(*(pCVT->paFJPhOp))[i].strName);
		HSSend (handle, str);
	}
}										/* @! Function: na_phlist  }} */
/* @! Function: na_APSen  {{ */
void na_APSen(unsigned long handle)
{
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfph       = &(pParam->FJPh);

	if (TRUE == pfph->bAPSenable) HSSend (handle, "CHECKED");
}										/* @! Function: na_APSen  }} */
/* @! Function: na_APSdis  {{ */
void na_APSdis(unsigned long handle)
{
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfph       = &(pParam->FJPh);

	if (FALSE == pfph->bAPSenable) HSSend (handle, "CHECKED");
}										/* @! Function: na_APSdis  }} */
/* @! Function: na_APSrun  {{ */
void na_APSrun(unsigned long handle)
{
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfph       = &(pParam->FJPh);

	if (TRUE == pfph->bAPSenable) HSSend (handle, "<input type=button class=fjbtn1 name=runams value='Run APS' onClick='submitPC(this.form,\"runams\");'>");
	else HSSend (handle, "&nbsp;");
}										/* @! Function: na_APSrun  }} */
/* @! Function: na_SERvardata  {{ */
void na_SERvardata(unsigned long handle)
{
	if ( TRUE == pCVT->pfph->bSerialVarData ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SERvardata  }} */
/* @! Function: na_SERdynmem  {{ */
void na_SERdynmem(unsigned long handle)
{
	if ( TRUE == pCVT->pfph->bSerialDynMem ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SERdynmem  }} */
/* @! Function: na_PHTFEATURE  {{ */
void na_PHTFEATURE(unsigned long handle)
{
	LPFJPRINTHEAD   pfph  = pCVT->pfph;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	HSSend (handle, "<table width='100%' border=1 cellpadding='2' rules=groups bgcolor='#f5f5f5'>\n");
	if (0 != strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
	{
		HSSend (handle, "<thead><tr><td colspan=4><b>APS</b></td></tr></thead><tbody>\n");
		HSSend (handle, "<tr><td align=right>Interval</td><td colspan=3><input type=text name=ams_inter size=8 onChange=\"return parmFDCheck(this,0,99,'Hours');\" value='");
		na_PRAMSINTER(handle);
		HSSend (handle, "'>&nbsp;Hours between APS cycles</td></tr>\n");
		HSSend (handle, "<tr><td align=right>Inactive</td><td colspan=3><input type=text name=ams_inact size=8 onChange=\"return parmCheck(this,0,9999,'Seconds');\" value='");
		na_PRAMSINACT(handle);
		HSSend (handle, "'>&nbsp;Seconds of inactivity to wait before APS cycle</td></tr>\n");
		HSSend (handle, "<tr><td align=right>Status</td><td align=middle><input type=radio name=ams_mode ");
		na_APSen(handle);
		HSSend (handle, " value=yes>Enable</td><td align=middle><input type=radio name=ams_mode ");
		na_APSdis(handle);
		HSSend (handle, " value=no>Disable</td><td align=middle>");
		na_APSrun(handle);
		HSSend (handle, "</td></tr>\n");
	}
	else
	{
		HSSend (handle, "<thead><tr><td colspan=4><b>Automatic StandBy mode</b></td></tr></thead><tbody>\n");
		HSSend (handle, "<tr><td align=right>Inactive</td><td colspan=3><input type=text name=stdb_in size=8 onchange=\"return parmFDCheck(this,0,99,'Hours');\" value='");
		na_Double(handle, (DOUBLE)pfph->fStandByInactive, 2);
		HSSend (handle, "'>&nbsp;Hours of inactivity to wait before activating StandBy mode.</td></tr>\n");
		HSSend (handle, "<tr><td align=right>Status</td><td align=center><input type=radio name=stdb_mode ");
		if (TRUE == pCVT->pfph->bStandByEnable) HSSend (handle, "CHECKED");
		HSSend (handle, " value=yes>Enable</td><td align=center><input type=radio name=stdb_mode ");
		if (FALSE == pCVT->pfph->bStandByEnable) HSSend (handle, "CHECKED");
		HSSend (handle, " value=no>Disable</td><td align=center><input type=button class=fjbtn1 name=standby value='StandBy ");
		if (TRUE == pGlobal->bStandByOn) HSSend (handle, "Off");
		else HSSend (handle, "On");
		HSSend (handle, "'	onclick='submitPC(this.form,\"standby\");'></td></tr>\n");
	}
	HSSend (handle, "</tbody></table>\n");
}										/* @! Function: na_PHTFEATURE  }} */
/* @! Function: na_GA_FACTOR  {{ */
void na_GA_FACTOR(unsigned long handle)
{
	LONG GA_factor = 1;
	// if GA version > 1.2, encoder pulse will be double by GA
	LONG GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
	BYTE GA_Mver = ((GAver>>4)& 0x0F);
	BYTE GA_Lver = (GAver & 0x0F);
	if ( 1 <= GA_Mver )
	{
		if ( 1 == GA_Mver && 3 > GA_Lver)
			GA_factor = 1;
		else
			GA_factor = 2;
	}
	na_Long(handle, GA_factor);
}										/* @! Function: na_GA_FACTOR  }} */

/* @! Function: na_BiDirectional  {{ */
void na_BiDirectional(unsigned long handle)
{
	if (pCVT->pfph->bBiDirectional == TRUE) HSSend (handle, "CHECKED");
}										/* @! Function: na_BiDirectional  }} */

/* @! Function: na_PCInternal  {{ */
void na_PCInternal(unsigned long handle)// Burl v 1.1015
{
	if ( TRUE == pCVT->pfph->bPhotocellInternal ) HSSend (handle, "CHECKED");
}										/* @! Function: na_PCInternal  }} */
/* @! Function: na_AUTOPRINT  {{ */

void na_AUTOPRINT(unsigned long handle)	// Burl v 1.1015
{
	LPFJSYSTEM pfsys = pCVT->pfsys;
	FLOAT fAutoPrint = (FLOAT)pCVT->pfph->pfop->lAutoPrint / pfsys->fPrintResolution;

	na_Double(handle, (double)pCVT->fj_FloatUnits( fAutoPrint, pCVT->pfsys->lUnits), 2 );

}										/* @! Function: na_AUTOPRINT  }} */
/* @! End of dynamic functions   */
