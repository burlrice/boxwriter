/* C file: c:\MyPrograms\FoxJet1\_header.c created
 by NET+Works HTML to C Utility on Monday, May 08, 2000 13:47:06 */
#include "fj.h"
#include "fj_printhead.h"
#include "fj_system.h"
#include "fj_memstorage.h"

struct tm tmDTCur;

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_get_long_name */
void na_LNam (unsigned long handle);
/*  @! File: na_get_IPA */
void na_IPA (unsigned long handle);
/*  @! File: na_get_ID */
void na_ID (unsigned long handle);
/*  @! File: na_GRNAME */
void na_GRNAME (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_LOGO_WIDTH */
static void na_LOGO_WIDTH (unsigned long handle);
/*  @! File: na_LOGO_HEIGHT */
static void na_LOGO_HEIGHT (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\header.htm {{ */
void Send_function_0(unsigned long handle)
{
	HSSend (handle, "<link rel=\"stylesheet\" href=\"fjipstyle1.css\">\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_layers.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_menu_func.htm\"></SCRIPT>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body onload=\"fjInit()\" onresize=\"fjResize()\">\n");
	HSSend (handle, "<table border=\"0\" width=\"100%\">\n");
	HSSend (handle, "<tr valign=\"top\">\n");
	HSSend (handle, "<td rowspan=\"2\" width=\"");
	na_LOGO_WIDTH (handle);
	HSSend (handle, "\"><img border=\"0\" src=\"fjlogo.jpg\" width=\"");
	na_LOGO_WIDTH (handle);
	HSSend (handle, "\" height=\"");
	na_LOGO_HEIGHT (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><div class=\"fjh1\">");
	na_LNam (handle);
	HSSend (handle, " Printer Group</div>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td>&nbsp;</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr valign=\"bottom\">\n");
	HSSend (handle, "<td><div class=\"fjh2\">");
	na_GRNAME (handle);
	HSSend (handle, "</div>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td>&nbsp;</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<hr>\n");
	HSSend (handle, "<left>\n");
	HSSend (handle, "<span class=\"fjh2\">");
	na_ID (handle);
	HSSend (handle, " : <SCRIPT type=\"text/javascript\">document.writeln(h2text);</SCRIPT></span>\n");
	HSSend (handle, "</left>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var pwid_js=\"");
	na_PWID (handle);
	HSSend (handle, "\";\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_menu.htm\"></SCRIPT>\n");
	HSSend (handle, "<p>&nbsp;</p>");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\header.htm }} */
/* @! End HTML page   */

// this is here to fix several bugs in the 4.0 version of HSGetValue.
// if the input field is empty, the returned length is 0, but there is not zero in the buffer.
// if the input field equals the requested len, there is no zero to end the string.
// if the input field i slarger than len, more than len is returned.
int fj_HSGetValue(unsigned long handle, char *pField, char *pValue, int len )
{
	char  str[300];
	int   ret;

	ret = HSGetValue( handle, pField, str, len );
	if ( ret >= 0 )
	{
		if      (   0 == ret )
		{
			str[0] = 0;
		}
		else if ( len <= ret )
		{
			str[len-1] = 0;
		}
		strcpy( pValue, str );
		ret = strlen( pValue );
	}
	return( ret );
}
int fj_get_pwid(unsigned long handle, char *ppwid)
{
	int   iLevel;
	int   ret;

	*ppwid = 0;
	ret = fj_HSGetValue( handle, "pwid", ppwid, 10 );
	return( ret );
}
void fj_send_pwid_parameter(unsigned long handle, char *pPrevious)
{
	char   pwid[10];
	int    ret;

	ret = fj_get_pwid( handle, pwid );
	if ( (0 != pwid[0]) && (NULL == strstr(pPrevious,"pwid")) )
	{
		if ( NULL == strchr(pPrevious,'?') )
		{
			HSSend (handle, "?");
		}
		else
		{
			HSSend (handle, "&");
		}
		HSSend (handle, "pwid=");
		HSSend (handle, pwid);
	}
	return;
}
int fj_get_level(unsigned long handle)
{
	ULONG ipAdd;
	int   iLevel;
	char  pwid[10];
	int   ret;

	ipAdd = HSRemoteAddress(handle);
	ret = fj_get_pwid( handle, pwid );
	iLevel = pCVT->fj_SystemGetLevelID( pCVT->pfph, ipAdd, pwid );
	return( iLevel );
}
void fj_reload_page(unsigned long handle, char *pName)
{
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">location.replace(\"");
	HSSend (handle, pName);
	fj_send_pwid_parameter( handle, pName );
	HSSend (handle, "\");</SCRIPT>");
	return;
}
void na_Hex(unsigned long handle, long l)
{
	char str[20];
	sprintf(str, "0x%X", (int)l );
	HSSend (handle, str);
}
void na_Long(unsigned long handle, long l)
{
	char str[20];
	sprintf(str,"%li",l);
	HSSend (handle, str);
}
void na_Char(unsigned long handle, char c)
{
	static char str[2] = {0,0};
	str[0] = c;
	HSSend (handle, str);
}
void na_String(unsigned long handle, char *pStr)
{
	char c;

	while ( 0 != (c=*pStr++) )
	{
		if      ( '"' == c ) HSSend(handle, "&quot;");
		else if ( '<' == c ) HSSend(handle, "&lt;");
		else if ( '>' == c ) HSSend(handle, "&gt;");
		else if ( '&' == c ) HSSend(handle, "&amp;");
		else na_Char(handle, c);
	}
}
void na_StringWithDoubleQuotes(unsigned long handle, char *pStr)
{
	char c;

	na_Char(handle, '"');
	na_String(handle, pStr);
	na_Char(handle, '"');
}
// strip trailing zeros
void na_Double(unsigned long handle, double dInput, int iPlaces)
{
	int  len;
	int  i;
	char str[32];

	pCVT->fj_DoubleToStr( str, dInput );
	len = strlen(str)-1;
	i = len;
	while( i >= 0 )
	{
		if ( '.' == str[i] )
		{
			if ( (i+iPlaces+1) < len ) str[i+iPlaces+1] = 0;
			break;
		}
		i--;
	}
	HSSend (handle, str);
}
void na_IPaddr(unsigned long handle, unsigned long ip )
{
	/* add your implementation here: */
	char ipa[20];
	pCVT->fj_IPToStr( ip, ipa );
	HSSend (handle, ipa);
}
void na_TITLE_ALL(unsigned long handle, char *pPage )
{
	na_LNam( handle );
	HSSend ( handle, " Printer " );
	na_GRNAME( handle );
	HSSend ( handle, " " );
	na_ID( handle );
	HSSend ( handle, " : " );
	HSSend ( handle, pPage );
}
/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
void na_HEADER(unsigned long handle)
{
	/* add your implementation here: */
										// find and send the core of the header page
	pCVT->AppSearchURL (handle, "/Header.htm");
}										/* @! Function: na_HEADER  }} */
/* @! Function: na_PWID  {{ */
void na_PWID(unsigned long handle)
{
	/* add your implementation here: */
	char   pwid[10];
	int    ret;

	ret = fj_get_pwid( handle, pwid );
	HSSend (handle, pwid);
}										/* @! Function: na_PWID  }} */
/* @! Function: na_ID  {{ */
void na_ID(unsigned long handle)
{
	/* add your implementation here: */
										// print head id = short name + serial number
	na_String(handle, pCVT->pfph->strID);
}										/* @! Function: na_ID  }} */
/* @! Function: na_PHtype  {{ */
void na_PHtype(unsigned long handle)
{
	/* add your implementation here: */
										// print head type
	na_String(handle, pCVT->pfph->pfop->strName);
}										/* @! Function: na_PHtype  }} */
void na_IPA(unsigned long handle)
{
	/* add your implementation here: */
	na_IPaddr(handle, pCVT->pfph->lIPaddr );
}										/* @! Function: na_IPA  }} */
/* @! Function: na_getDTCur  {{ */
void na_getDTCur(unsigned long handle)
{
	/* add your implementation here: */
	pCVT->fj_SystemGetTime( &tmDTCur );
}										/* @! Function: na_getDTCur  }} */
/* @! Function: na_DTCur  {{ */
void na_DTCur(unsigned long handle)
{
	/* add your implementation here: */
	char  daydate[64];
	// day, date, time string
	pCVT->fj_SystemGetDateTimeFullString(pCVT->pfsys,daydate, &tmDTCur );
	na_String(handle, daydate);
}										/* @! Function: na_DTCur  }} */
/* @! Function: na_DTDD  {{ */
void na_DTDD(unsigned long handle)
{
	/* add your implementation here: */
	char   daydate[20];
	// full name of day plus date
	pCVT->fj_SystemGetDayDateString(pCVT->pfsys, daydate, &tmDTCur );
	na_String(handle, daydate);
}										/* @! Function: na_DTDD  }} */
/* @! Function: na_DTMon  {{ */
void na_DTMon(unsigned long handle)
{
	/* add your implementation here: */
	char  month[16];
	// full name of month
	pCVT->fj_SystemGetMonthString(pCVT->pfsys, month, &tmDTCur );
	na_String(handle, month);
}										/* @! Function: na_DTMon  }} */
/* @! Function: na_LNam  {{ */
void na_LNam(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfph->strNameLong);
}										/* @! Function: na_LNam  }} */
/* @! Function: na_SN  {{ */
void na_SN(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfph->strSerial);
}										/* @! Function: na_SN  }} */
/* @! Function: na_SNam  {{ */
void na_SNam(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfph->strNameShort);
}										/* @! Function: na_SNam  }} */
/* @! Function: na_VDat  {{ */
void na_VDat(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, pCVT->fj_getVersionDate());
}										/* @! Function: na_VDat  }} */
/* @! Function: na_VNum  {{ */
void na_VNum(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, pCVT->fj_getVersionNumber());
}										/* @! Function: na_VNum  }} */
/* @! Function: na_DGA  {{ */
void na_DGA(unsigned long handle)
{
	/* add your implementation here: */
	CHAR str[16];
	if ( 100 < pCVT->pFJglobal->tmFPGAProgram.tm_year )
	{
		pCVT->fj_SystemGetDateString( str, &(pCVT->pFJglobal->tmFPGAProgram) );
		HSSend (handle, str );
	}
	else
	{
		HSSend (handle, "&nbsp;");
	}
}										/* @! Function: na_DGA  }} */
/* @! Function: na_GAVNum  {{ */
void na_GAVNum(unsigned long handle)
{
	CHAR str[16];
	LONG GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
	BYTE GA_Mver = ((GAver>>4)& 0x0F);
	BYTE GA_Lver = (GAver & 0x0F);
	sprintf(str,"%d.%d",GA_Mver,GA_Lver);
	HSSend (handle, str );
}										/* @! Function: na_GAVNum  }} */
/* @! Function: na_GRNAME  {{ */
void na_GRNAME(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->strID);
}										/* @! Function: na_GRNAME  }} */
/* @! Function: na_STAvail  {{ */
void na_STAvail(unsigned long handle)
{
	/* add your implementation here: */
	LPCFJMEMSTOR pfms;

	pfms = pCVT->pfph->pMemStor;
	na_Long( handle, pfms->lSize - pfms->lNext );
}										/* @! Function: na_STAvail  }} */
/* @! Function: na_STAvailEdit  {{ */
void na_STAvailEdit(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph = pCVT->pfph;
	LPCFJMEMSTOR  pfms;
	ULONG  ipAdd;

	ipAdd = HSRemoteAddress(handle);
	pfms = pfph->pMemStor;
	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, ipAdd) ) pfms = pfph->pMemStorEdit;
	na_Long( handle, pfms->lSize - pfms->lNext );
}										/* @! Function: na_STAvailEdit  }} */
/* @! Function: na_LOGO_WIDTH  {{ */
void na_LOGO_WIDTH(unsigned long handle)
{
	/* add your implementation here: */
	na_Long( handle, pCVT->pFJglobal->lLogoWidth );
}										/* @! Function: na_LOGO_WIDTH  }} */
/* @! Function: na_LOGO_HEIGHT  {{ */
void na_LOGO_HEIGHT(unsigned long handle)
{
	/* add your implementation here: */
	na_Long( handle, pCVT->pFJglobal->lLogoHeight );
}										/* @! Function: na_LOGO_HEIGHT  }} */
/* @! End of dynamic functions   */
