/*
 *            Copyright (c) 1998-2000 by NETsilicon Inc.
 *
 *  This software is copyrighted by and is the sole property of
 *  NETsilicon.  All rights, title, ownership, or other interests
 *  in the software remain the property of NETsilicon.  This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of NETsilicon.
 *
 *  NETsilicon, reserves the right to modify this software
 *  without notice.
 *
 *  NETsilicon
 *  411 Waverley Oaks Road                  USA 781.647.1234
 *  Suite 227                               http://www.netsilicon.com
 *  Waltham, MA 02452                       AmericaSales@netsilicon.com
 *
 *************************************************************************
 *
 *     Module Name: naflash.c
 *	       Version: 1.00
 *   Original Date: 06/23/99
 *	        Author: Ross Dreyer and Peter McA'Nulty
 *	      Language: Ansi C
 * Compile Options:
 * Compile defines:
 *	     Libraries:
 *    Link Options:
 *
 *    Entry Points: NAprogram_flash(), NAflash_write(), NAFlashEnable (),
 *
 *
 *  Copyrighted (c) by NETsilicon, Inc.  All Rights Reserved.
 *
 * Description.
 * =======================================================================
 * This file contains flash driver routines including flash device table.
 *
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  =======================================================
 * LZ   11/05/99   Ported to NETOS from VX_WORKS.
 * LZ   12/21/99   Rewrite some portion of the code to support 16-bit and 32-bit mode flash.
 *                 while we still support the same old version of APIs,
 *                 the major change is that erasing, verifying  and programming are based on
 *                 logical SECTOR concept rather than 32k data SEGMENT base according to the
 *                 flash parts data book, this would allow better understanding of the code
 *                 and easier expending code to support 4M or 8M flashes in the future.
 * LZ   12/21/99   Added "total_sector_number" field to struct "flash_id_table_t"
 * LZ   03/10/00   Fixed a bug in NAflash_write(), when verify the first
 *                 32k chuck of data, we should verify 16k+8k+8k.
 * LZ   03/14/00   If identify_flash() returns 0xff, we default flash_id
 *                 to be 6, we assume the flash is AMD compatible.
 * LZ   04/18/00   Changed NAflash_write() to be SLOW_MODE to be successful
 *                 on EVERY 32k segment programming.
 * LZ   05/01/00   Added support to FUJITSU flash.
 * JF    6/30/00   Added support for ATMEL AT49BV8011 & (T). Should be able
 * JF	 7/03/00   Added support for SST39VF800 flash.
 * JF    9/11/00   Added support for STM_M29W800AB
 * JF    1/22/01   Added support for AMD29DL323D
 * DXL	05/04/01   Corrected AMD_29DL323D_flash_top_sector_erase_cmd for flash_id_table[]
 * SW   08/17/01   Added support for any ROM size
 * PMJ  10/23/01   Added NAFlashEnable() to NAFlash_write().
 * LZ   02/05/02   Added NACacheDisable() & NACacheRestore() in the driver.
 * DXL	03/08/02   Added support for ATMEL AT49BV1614A
 * JW   03/28/02   Added NAFlashSectors(), NAFlashSectorSizes(), NAFlashErase(), NAFlashEraseStatus(),
 *                 NAFlashRead(), NAFlashWrite()
 * JW   03/29/02   Fixed bug in NAFlashWrite(), where buffer indexes and bytes to write are not
 *                 tracked correctly if sector is not programmed because buffer data is the same
 *                 as sector data. Added more comments for NAFlashWrite().
 * JW   04/05/02   1) Added support for multiple flash memory banks.  The following internal
 *                 functions were modified: exec_flash_cmd(), LEraseSector(), LSetSectorAddress(),
 *                 LGetSectorSize().  Some of the new API functions were also modified.
 *                 2) Changed return values for new API functions to use the NAFLASH_ prefix.
 *                 3) Added NAFlashSectorOffsets()
 * JW   04/08/02   Changed the implementation of LSetSectorAddress() to use tables to store sector
 *                 base addresses.  Previous implementation used a loop to compute sector base
 *                 addresses.  As sector numbers increase, the longer it takes to compute the
 *                 sector base address.
 * CHG  04/09/02   Added support for S29W320DB.
 * JW   04/11/02   1) Bug fix in NAFlashRead(). where one of the arguments for memcpy() was typecasted
 *                 incorrectly
 *                 2) Modifified NAFlashWrite() to use a table to store sector offsets.  Previous
 *                 implementation uses a loop to compute sector offsets.  As sector numbers increase,
 *                 the longer it takes to compute the sector offsets.
 *                 3) Modified NAFlashWrite() and NAFlashRead() to compute the total flash memory only
 *                 if needed.
 * JW   04/16/02   Use new define (MAX_SECTOR_SIZE) to specify size of first_sector_data[] buffer.
 * DXL	04/16/02   Cleaned up variable declared but never reference; warnings generated by new compiler
 * JW   04/24/02   Cleaned up comments preceeding API functions.
 * JW   04/26/02   Modified sections of LProgramSector(), LProgramPartialSector(), LVerifySector() and
 *                 LVerifyPartialSector() to read data from a buffer one byte at a time.  These changes
 *                 allow the driver to read data from a buffer regardless of the buffer's memory
 *                 alignment.  Previous code had problems reading data from a buffer in 2 or 4 byte
 *                 sizes if the buffer was allocated on a odd memory address.
 * JW   05/01/02   Added NAFlashInit() and removed argument from NAFlashSectors().
 * JW   05/04/02   1) Added a semaphore to synchronize access to NAFlashErase(), NAFlashRead(),
 *                 NAFlashWrite() and NAFlashEraseStatus().  Modified these 4 functions to be
 *                 synchronized by the semaphore.
 *                 2) Added NAFlashCreateSemaphores()
 *                 3) Modified NAFlashInit() to detect flash device using identify_flash()
 *                 4) Removed calls to identify_flash() from the rest of the API functions.
 * JW   05/06/02   Changed the implementation of NAFlashInit().
 * JW   05/08/02   1) Removed calls to NAFlashRead() in NAFlashWrite() and replaced them with memcpy()
 *                 to fix a semaphore deadlock condition.
 *                 2) Added tx_semaphore_get() to NAFlashErase()
 * JW   05/09/02   Added LSetSectorAddress() calls in NAFlashWrite()
 * JW   05/17/02   Removed identify_flash() from NAFlashInit() and added the identify_flash()
 *                 call to all other API functions except NAFlashEnable(), NAFlashBase() and
 *                 NAFlashCreateSemaphores()
 */

#include "ncc_io.h"
#include <string.h >
#ifdef NETOS_GNU_TOOLS
extern char free_mem_begin[];
#else
extern char __ghsend_free_mem[];
#endif

#include <flash.h>
#include "naflash.h"
#include <tx_api.h>

#define SUCCESS     0

static void LFlashWriteOn(void);
static void TurnFlashWriteOff(void);
static void LResetWord(register WORD32 whichWord);
static void LPollWord(register WORD32 whichWord, WORD32 pollWait);
static WORD32 LReadWord(register WORD32 whichWord);
static void LWriteWord (WORD32 whichWord, WORD32 data);
static void LWait(WORD32 useconds);
#ifdef FOXJET
WORD16 LGetSectorNumber(WORD16 id, WORD16 segment_number, WORD32 segment_offset);
WORD32 LGetSectorOffset(WORD16 id, WORD16 segment_number, WORD32 segment_offset);
WORD32 LGetSectorSize(WORD32 sectorNumber);
#else if
static WORD16 LGetSectorNumber(WORD16 id, WORD16 segment_number, WORD32
segment_offset);
static WORD32 LGetSectorSize(WORD32 sectorNumber);
#endif
static void LSetSectorAddress(WORD32 sectorNumber);
static int LVerifySector(WORD32 sectorNumber, WORD16 *sectorData);
static BOOLEAN LVerifyAllFs(WORD32 sectorNumber);
static int LProgramSector(WORD32 sectorNumber, register WORD16 *sectorData);
static int LEraseSector(WORD32 sectorNumber);
#ifdef FOXJET
WORD8 identify_flash ( void );
#else if
static WORD8 identify_flash ( void );
#endif
static WORD8 exec_flash_cmd ( flash_cmd_t *command, WORD16 cmd_len );
static WORD16 LIsSegmentMisAligned(WORD32 SegmentNumber);
static int LProgramPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToWrite, register char *sectorData);
static int LVerifyPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToRead, register char *sectorData);

#define  TOGGLE_BIT_MASK    0x40404040
#define  DATA_BIT_MASK      0x80808080
#define  AMD_FLASH_PART ((flash_id == AMD_B_5V_FLASH_ID) || (flash_id == AMD_B_3V_FLASH_ID) || (flash_id == AMD_T_5V_FLASH_ID) || (flash_id == AMD_T_3V_FLASH_ID))
#define  SEGMENT_SIZE    0x8000			/* 32k */

#ifdef FOXJET
WORD16 fj_ROMType = 0xffff;
int    fj_ROMId   = -1;
WORD8  flash_id = 0xFF;
#else if
static WORD8  flash_id = 0xFF;
#endif

WORD16  *mSectorBase;
WORD32  *mSectorBase32;					/* needed for 32-bit flash */

#ifdef FOXJET
#else if
/* We need this buffer for AMD flash which has 64K(16-bit) or 128k(32-bit) sectors */

BYTE first_sector_data[MAX_SECTOR_SIZE];/* 128k */
#endif

/* store the number of flash banks on the board */
WORD32 flash_banks = 0;

/* store the total amount of flash memory in bytes */
WORD32 total_flash_memory = 0;

/* semaphore to synchronize access to API functions */
TX_SEMAPHORE flash_semaphore1;

/* semaphore flag to indicate whether semaphores were created */
int semaphore_created = 0;

#ifdef FOXJET
#else if
/* program flash in fast or slow mode */
#define    FAST_MODE         1
#define    SLOW_MODE         0
int NAflash_write_local (WORD32 sectorNumber, WORD16 *sectorData, int mode);
#endif

#ifdef FOXJET
/*
 * Command sequences for the AMD Am29F800B
 */
static const flash_cmd_t AMD_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

static const flash_cmd_t AMD_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

static const flash_cmd_t AMD_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

static const flash_cmd_t AMD_flash_erase_cmd[] =
{
	0xAA, 0x5555,
	0x55, 0x2AAA,
	0x80, 0x5555,
	0xAA, 0x5555,
	0x55, 0x2AAA
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
static const flash_cmd_t AMD_flash_bottom_sector_erase_cmd[20] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x02000},
	{ 0x30, 0x03000},
	{ 0x30, 0x04000},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000}
};
/*
static const flash_cmd_t AMD160_flash_bottom_sector_erase_cmd[36] =
{
   { 0x30, 0x00000}, // Command to erase the first sector //
   { 0x30, 0x02000},
   { 0x30, 0x03000},
   { 0x30, 0x04000},
{ 0x30, 0x08000},
{ 0x30, 0x10000},
{ 0x30, 0x18000},
{ 0x30, 0x20000},
{ 0x30, 0x28000},
{ 0x30, 0x30000},
{ 0x30, 0x38000},
{ 0x30, 0x40000},
{ 0x30, 0x48000},
{ 0x30, 0x50000},
{ 0x30, 0x58000},
{ 0x30, 0x60000},
{ 0x30, 0x68000},
{ 0x30, 0x70000},
{ 0x30, 0x78000},
{ 0x30, 0x80000},
{ 0x30, 0x88000},
{ 0x30, 0x90000},
{ 0x30, 0x98000},
{ 0x30, 0xa0000},
{ 0x30, 0xa8000},
{ 0x30, 0xb0000},
{ 0x30, 0xb8000},
{ 0x30, 0xc0000},
{ 0x30, 0xc8000},
{ 0x30, 0xd0000},
{ 0x30, 0xd8000},
{ 0x30, 0xe0000},
{ 0x30, 0xe8000},
{ 0x30, 0xf0000},
{ 0x30, 0xf8000}
};
*/
static const flash_cmd_t AMD160_flash_bottom_sector_erase_cmd[68] =
{
	{									/* Command to erase the first sector */
		0x30, 0x000000
	},
	{ 0x30, 0x002000},
	{ 0x30, 0x003000},
	{ 0x30, 0x004000},
	{ 0x30, 0x008000},
	{ 0x30, 0x010000},
	{ 0x30, 0x018000},
	{ 0x30, 0x020000},
	{ 0x30, 0x028000},
	{ 0x30, 0x030000},
	{ 0x30, 0x038000},
	{ 0x30, 0x040000},
	{ 0x30, 0x048000},
	{ 0x30, 0x050000},
	{ 0x30, 0x058000},
	{ 0x30, 0x060000},
	{ 0x30, 0x068000},
	{ 0x30, 0x070000},
	{ 0x30, 0x078000},
	{ 0x30, 0x080000},
	{ 0x30, 0x088000},
	{ 0x30, 0x090000},
	{ 0x30, 0x098000},
	{ 0x30, 0x0a0000},
	{ 0x30, 0x0a8000},
	{ 0x30, 0x0b0000},
	{ 0x30, 0x0b8000},
	{ 0x30, 0x0c0000},
	{ 0x30, 0x0c8000},
	{ 0x30, 0x0d0000},
	{ 0x30, 0x0d8000},
	{ 0x30, 0x0e0000},
	{ 0x30, 0x0e8000},
	{ 0x30, 0x0f0000},
	{ 0x30, 0x0f8000},
	{ 0x30, 0x100000},
	{ 0x30, 0x108000},
	{ 0x30, 0x110000},
	{ 0x30, 0x118000},
	{ 0x30, 0x120000},
	{ 0x30, 0x128000},
	{ 0x30, 0x130000},
	{ 0x30, 0x138000},
	{ 0x30, 0x140000},
	{ 0x30, 0x148000},
	{ 0x30, 0x150000},
	{ 0x30, 0x158000},
	{ 0x30, 0x160000},
	{ 0x30, 0x168000},
	{ 0x30, 0x170000},
	{ 0x30, 0x178000},
	{ 0x30, 0x180000},
	{ 0x30, 0x188000},
	{ 0x30, 0x190000},
	{ 0x30, 0x198000},
	{ 0x30, 0x1a0000},
	{ 0x30, 0x1a8000},
	{ 0x30, 0x1b0000},
	{ 0x30, 0x1b8000},
	{ 0x30, 0x1c0000},
	{ 0x30, 0x1c8000},
	{ 0x30, 0x1d0000},
	{ 0x30, 0x1d8000},
	{ 0x30, 0x1e0000},
	{ 0x30, 0x1e8000},
	{ 0x30, 0x1f0000},
	{ 0x30, 0x1f8000}
};

/* Array of all the sector sizes */
static const WORD32 amd_bottom_sector_size_array[] =
{
	0x4000,  0x2000,  0x2000,  0x8000,  0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000
};
static const WORD32 amd160_bottom_sector_size_array[] =
{
	0x4000,  0x2000,  0x2000,  0x8000,  0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000
};

#else if
/*
 *
 *  Commands for Macronix Flash
 *
 */

static const flash_cmd_t MX_flash_id_cmd[] =
{
	0x90, 0x0000
};

static const flash_cmd_t MX_flash_erase_cmd[] =
{
	0x20, 0x0000, 0xD0, 0x0000
};

static const flash_cmd_t MX_flash_write_cmd[] =
{
	0x40, 0x0000
};

/*
 *
 *  Commands for Atmel Flash
 *
 */

static const flash_cmd_t AT_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

static const flash_cmd_t AT_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

static const flash_cmd_t AT_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

const flash_cmd_t AT_flash_erase_cmd[] =
{
	0xAA, 0x5555,
	0x55, 0x2AAA,
	0x80, 0x5555,
	0xAA, 0x5555,
	0x55, 0x2AAA
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AT_flash_AT49BV8011_sector_erase_cmd[] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x02000},
	{ 0x30, 0x06000},
	{ 0x30, 0x07000},
	{ 0x30, 0x08000},
	{ 0x30, 0x09000},
	{ 0x30, 0x0a000},
	{ 0x30, 0x0e000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000}
};

/* Array of all the sector sizes */
const WORD32 AT_AT49BV8011_sector_size_array[] =
{
	0x4000, 0x8000,
	0x2000, 0x2000, 0x2000, 0x2000, 0x8000, 0x4000,
	0x10000,  0x10000,  0x10000,  0x10000,  0x10000, 0x10000,
	0x10000,  0x10000,  0x10000,  0x10000,  0x10000, 0x10000,
	0x10000,  0x10000
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AT_flash_AT49BV8011T_sector_erase_cmd[] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x72000},
	{ 0x30, 0x76000},
	{ 0x30, 0x77000},
	{ 0x30, 0x78000},
	{ 0x30, 0x79000},
	{ 0x30, 0x7a000},
	{ 0x30, 0x7e000}
};
/* Array of all the sector sizes */

const WORD32 AT_AT49BV8011T_sector_size_array[] =
{
	0x10000,  0x10000,  0x10000,  0x10000,  0x10000, 0x10000,
	0x10000,  0x10000,  0x10000,  0x10000,  0x10000, 0x10000,
	0x10000,  0x10000,  0x4000, 0x8000,
	0x2000, 0x2000, 0x2000, 0x2000, 0x8000, 0x4000
};

/* 
 *	AT49BV1614A 2 Meg Bytes Flash Memory
 *  Sectors Size: 	8 Sectors 8K Bytes
 *					31 Sectors 64K Bytes
 */

/*
 * Flash Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AT_flash_AT49BV1614A_sector_erase_cmd[] =
{
	{ 0x30, 0x00000},
	{
		0x30, 0x01000
	}
	,
	{
		0x30, 0x02000
	}
	,
	{
		0x30, 0x03000
	}
	,
	{
		0x30, 0x04000
	}
	,
	{
		0x30, 0x05000
	},
	{ 0x30, 0x06000},
	{
		0x30, 0x07000
	}
	,
	{
		0x30, 0x08000
	}
	,
	{
		0x30, 0x10000
	}
	,
	{
		0x30, 0x18000
	}
	,
	{
		0x30, 0x20000
	},
	{ 0x30, 0x28000},
	{
		0x30, 0x30000
	}
	,
	{
		0x30, 0x38000
	}
	,
	{
		0x30, 0x40000
	}
	,
	{
		0x30, 0x48000
	}
	,
	{
		0x30, 0x50000
	},
	{ 0x30, 0x58000},
	{
		0x30, 0x60000
	}
	,
	{
		0x30, 0x68000
	}
	,
	{
		0x30, 0x70000
	}
	,
	{
		0x30, 0x78000
	}
	,
	{
		0x30, 0x80000
	},
	{ 0x30, 0x88000},
	{
		0x30, 0x90000
	}
	,
	{
		0x30, 0x98000
	}
	,
	{
		0x30, 0xA0000
	}
	,
	{
		0x30, 0xA8000
	}
	,
	{
		0x30, 0xB0000
	},
	{ 0x30, 0xB8000},
	{
		0x30, 0xC0000
	}
	,
	{
		0x30, 0xC8000
	}
	,
	{
		0x30, 0xD0000
	}
	,
	{
		0x30, 0xD8000
	}
	,
	{
		0x30, 0xE0000
	},
	{ 0x30, 0xE8000},
	{
		0x30, 0xF0000
	}
	,
	{
		0x30, 0xF8000
	},
};

/* AT49BV1614A  Array of all the sector sizes */
const WORD32 AT_AT49BV1614A_sector_size_array[] =
{
	0x2000,     0x2000,     0x2000,     0x2000,     0x2000,     0x2000,
	0x2000,     0x2000,     0x10000,    0x10000,    0x10000,    0x10000,
	0x10000,    0x10000,    0x10000,    0x10000,    0x10000,    0x10000,
	0x10000,    0x10000,    0x10000,    0x10000,    0x10000,    0x10000,
	0x10000,    0x10000,    0x10000,    0x10000,    0x10000,    0x10000,
	0x10000,    0x10000,    0x10000,    0x10000,    0x10000,    0x10000,
	0x10000,    0x10000,    0x10000,
};

/*
 *
 *  Commands for SST Flash
 *
 */

static const flash_cmd_t SST_flash_id_enter_cmd[] =
{
	0x90, 0x0000
};

static const flash_cmd_t SST_flash_id_exit_cmd[] =
{
	0xFF, 0x0000
};

static const flash_cmd_t SST_flash_erase_cmd[] =
{
	0x20, 0x0000, 0xD0, 0x0000
};

static const flash_cmd_t SST_flash_write_cmd[] =
{
	0x10, 0x0000
};

/*
 *
 *  Commands for SST39VF800
 *
 *
 */

static const flash_cmd_t SST_39VF800_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

static const flash_cmd_t SST_39VF800_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

static const flash_cmd_t SST_39VF800_flash_erase_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x80, 0x5555, 0xAA, 0x5555, 0x55, 0x2AAA
};

static const flash_cmd_t SST_39VF800_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t SST_39VF800_flash_block_erase_cmd[16] =
{
	{									/* Command to erase the first sector */
		0x50, 0x00000
	},
	{ 0x50, 0x08000},
	{ 0x50, 0x10000},
	{ 0x50, 0x18000},
	{ 0x50, 0x20000},
	{ 0x50, 0x28000},
	{ 0x50, 0x30000},
	{ 0x50, 0x38000},
	{ 0x50, 0x40000},
	{ 0x50, 0x48000},
	{ 0x50, 0x50000},
	{ 0x50, 0x58000},
	{ 0x50, 0x60000},
	{ 0x50, 0x68000},
	{ 0x50, 0x70000},
	{ 0x50, 0x78000}
};

/* Array of all the sector sizes */
/* Although the SST39VF800 has uniform block size, it has different erase command for
 * each block.  So we are treating it as flash with variable sector size.  In addition
 * we also use 64k byte block instead of the 2k sector size when working with this flash.
 */

const WORD32 SST_39VF800_flash_block_size_array[] =
{
	0x10000,  0x10000,  0x10000,  0x10000,  0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000
};

/*
 *
 *  Commands for SHARP Flash
 *
 */

static const flash_cmd_t SHARP_flash_id_enter_cmd[] =
{
	0x90, 0x0000
};

static const flash_cmd_t SHARP_flash_id_exit_cmd[] =
{
	0xFF, 0x0000
};

static const flash_cmd_t SHARP_flash_erase_cmd[] =
{
	0x20, 0x0000, 0xD0, 0x0000
};

static const flash_cmd_t SHARP_flash_write_cmd[] =
{
	0x10, 0x0000
};

/*
 * Command sequences for the AMD Am29F800B
 */
const flash_cmd_t AMD_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

const flash_cmd_t AMD_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

const flash_cmd_t AMD_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

const flash_cmd_t AMD_flash_erase_cmd[] =
{
	0xAA, 0x5555,
	0x55, 0x2AAA,
	0x80, 0x5555,
	0xAA, 0x5555,
	0x55, 0x2AAA
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AMD_flash_top_sector_erase_cmd[20] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000},
	{ 0x30, 0x7c000},
	{ 0x30, 0x7d000},
	{ 0x30, 0x7e000}
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AMD_flash_bottom_sector_erase_cmd[20] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x02000},
	{ 0x30, 0x03000},
	{ 0x30, 0x04000},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000}
};

/* Array of all the sector sizes */
const WORD32 amd_bottom_sector_size_array[] =
{
	0x4000,  0x2000,  0x2000,  0x8000,  0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000
};
const WORD32 amd_top_sector_size_array[] =
{
	0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x8000,  0x2000,  0x2000,  0x4000
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AMD_29DL323D_flash_top_sector_erase_cmd[71] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000},
	{ 0x30, 0x80000},
	{ 0x30, 0x88000},
	{ 0x30, 0x90000},
	{ 0x30, 0x98000},
	{ 0x30, 0xA0000},
	{ 0x30, 0xA8000},
	{ 0x30, 0xB0000},
	{ 0x30, 0xB8000},
	{ 0x30, 0xC0000},
	{ 0x30, 0xC8000},
	{ 0x30, 0xD0000},
	{ 0x30, 0xD8000},
	{ 0x30, 0xE0000},
	{ 0x30, 0xE8000},
	{ 0x30, 0xF0000},
	{ 0x30, 0xF8000},
	{ 0x30, 0x100000},
	{ 0x30, 0x108000},
	{ 0x30, 0x110000},
	{ 0x30, 0x118000},
	{ 0x30, 0x120000},
	{ 0x30, 0x128000},
	{ 0x30, 0x130000},
	{ 0x30, 0x138000},
	{ 0x30, 0x140000},
	{ 0x30, 0x148000},
	{ 0x30, 0x150000},
	{ 0x30, 0x158000},
	{ 0x30, 0x160000},
	{ 0x30, 0x168000},
	{ 0x30, 0x170000},
	{ 0x30, 0x178000},
	{ 0x30, 0x180000},
	{ 0x30, 0x188000},
	{ 0x30, 0x190000},
	{ 0x30, 0x198000},
	{ 0x30, 0x1A0000},
	{ 0x30, 0x1A8000},
	{ 0x30, 0x1B0000},
	{ 0x30, 0x1B8000},
	{ 0x30, 0x1C0000},
	{ 0x30, 0x1C8000},
	{ 0x30, 0x1D0000},
	{ 0x30, 0x1D8000},
	{ 0x30, 0x1E0000},
	{ 0x30, 0x1E8000},
	{ 0x30, 0x1F0000},
	{ 0x30, 0x1F8000},
	{ 0x30, 0x1F9000},
	{ 0x30, 0x1FA000},
	{ 0x30, 0x1FB000},
	{ 0x30, 0x1FC000},
	{ 0x30, 0x1FD000},
	{ 0x30, 0x1FE000},
	{ 0x30, 0x1FF000}
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t AMD_29DL323D_flash_bottom_sector_erase_cmd[71] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x01000},
	{ 0x30, 0x02000},
	{ 0x30, 0x03000},
	{ 0x30, 0x04000},
	{ 0x30, 0x05000},
	{ 0x30, 0x06000},
	{ 0x30, 0x07000},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000},
	{ 0x30, 0x80000},
	{ 0x30, 0x88000},
	{ 0x30, 0x90000},
	{ 0x30, 0x98000},
	{ 0x30, 0xA0000},
	{ 0x30, 0xA8000},
	{ 0x30, 0xB0000},
	{ 0x30, 0xB8000},
	{ 0x30, 0xC0000},
	{ 0x30, 0xC8000},
	{ 0x30, 0xD0000},
	{ 0x30, 0xD8000},
	{ 0x30, 0xE0000},
	{ 0x30, 0xE8000},
	{ 0x30, 0xF0000},
	{ 0x30, 0xF8000},
	{ 0x30, 0x100000},
	{ 0x30, 0x108000},
	{ 0x30, 0x110000},
	{ 0x30, 0x118000},
	{ 0x30, 0x120000},
	{ 0x30, 0x128000},
	{ 0x30, 0x130000},
	{ 0x30, 0x138000},
	{ 0x30, 0x140000},
	{ 0x30, 0x148000},
	{ 0x30, 0x150000},
	{ 0x30, 0x158000},
	{ 0x30, 0x160000},
	{ 0x30, 0x168000},
	{ 0x30, 0x170000},
	{ 0x30, 0x178000},
	{ 0x30, 0x180000},
	{ 0x30, 0x188000},
	{ 0x30, 0x190000},
	{ 0x30, 0x198000},
	{ 0x30, 0x1A0000},
	{ 0x30, 0x1A8000},
	{ 0x30, 0x1B0000},
	{ 0x30, 0x1B8000},
	{ 0x30, 0x1C0000},
	{ 0x30, 0x1C8000},
	{ 0x30, 0x1D0000},
	{ 0x30, 0x1D8000},
	{ 0x30, 0x1E0000},
	{ 0x30, 0x1E8000},
	{ 0x30, 0x1F0000},
	{ 0x30, 0x1F8000},

};

/* Array of all the sector sizes */
const WORD32 amd_29DL323D_bottom_sector_size_array[] =
{
	0x2000, 0x2000,  0x2000,  0x2000,
	0x2000, 0x2000,  0x2000,  0x2000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000
};

const WORD32 amd_29DL323D_top_sector_size_array[] =
{
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000,
	0x2000, 0x2000,  0x2000,  0x2000,
	0x2000, 0x2000,  0x2000,  0x2000
};

/*
 *
 *  Commands for STM29W800AB
 *
 *
 */

static const flash_cmd_t STM_M29W800AB_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

static const flash_cmd_t STM_M29W800AB_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

static const flash_cmd_t STM_M29W800AB_flash_erase_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x80, 0x5555, 0xAA, 0x5555, 0x55, 0x2AAA
};

static const flash_cmd_t STM_M29W800AB_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t STM_M29W800AB_flash_block_erase_cmd[19] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x02000},
	{ 0x30, 0x03000},
	{ 0x30, 0x04000},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000}
};

/* Array of all the sector sizes */

const WORD32 STM_M29W800AB_flash_block_size_array[] =
{
	0x4000,  0x2000,  0x2000,  0x8000,  0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000
};

/*
 *
 *  Commands for STM29W320DB
 *
 *
 */

static const flash_cmd_t STM_M29W320DB_flash_id_enter_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x90, 0x5555
};

static const flash_cmd_t STM_M29W320DB_flash_id_exit_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xF0, 0x5555
};

static const flash_cmd_t STM_M29W320DB_flash_erase_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0x80, 0x5555, 0xAA, 0x5555, 0x55, 0x2AAA
};

static const flash_cmd_t STM_M29W320DB_flash_write_cmd[] =
{
	0xAA, 0x5555, 0x55, 0x2AAA, 0xA0, 0x5555
};

/*
 * Array of commands user to erase sectors, starting with the first.
 */
const flash_cmd_t STM_M29W320DB_flash_block_erase_cmd[] =
{
	{									/* Command to erase the first sector */
		0x30, 0x00000
	},
	{ 0x30, 0x02000},
	{ 0x30, 0x03000},
	{ 0x30, 0x04000},
	{ 0x30, 0x08000},
	{ 0x30, 0x10000},
	{ 0x30, 0x18000},
	{ 0x30, 0x20000},
	{ 0x30, 0x28000},
	{ 0x30, 0x30000},
	{ 0x30, 0x38000},
	{ 0x30, 0x40000},
	{ 0x30, 0x48000},
	{ 0x30, 0x50000},
	{ 0x30, 0x58000},
	{ 0x30, 0x60000},
	{ 0x30, 0x68000},
	{ 0x30, 0x70000},
	{ 0x30, 0x78000},
	{ 0x30, 0x80000},
	{ 0x30, 0x88000},
	{ 0x30, 0x90000},
	{ 0x30, 0x98000},
	{ 0x30, 0xa0000},
	{ 0x30, 0xa8000},
	{ 0x30, 0xb0000},
	{ 0x30, 0xb8000},
	{ 0x30, 0xc0000},
	{ 0x30, 0xc8000},
	{ 0x30, 0xd0000},
	{ 0x30, 0xd8000},
	{ 0x30, 0xe0000},
	{ 0x30, 0xe8000},
	{ 0x30, 0xf0000},
	{ 0x30, 0xf8000},
	{ 0x30, 0x100000},
	{ 0x30, 0x108000},
	{ 0x30, 0x110000},
	{ 0x30, 0x118000},
	{ 0x30, 0x120000},
	{ 0x30, 0x128000},
	{ 0x30, 0x130000},
	{ 0x30, 0x138000},
	{ 0x30, 0x140000},
	{ 0x30, 0x148000},
	{ 0x30, 0x150000},
	{ 0x30, 0x158000},
	{ 0x30, 0x160000},
	{ 0x30, 0x168000},
	{ 0x30, 0x170000},
	{ 0x30, 0x178000},
	{ 0x30, 0x180000},
	{ 0x30, 0x188000},
	{ 0x30, 0x190000},
	{ 0x30, 0x198000},
	{ 0x30, 0x1a0000},
	{ 0x30, 0x1a8000},
	{ 0x30, 0x1b0000},
	{ 0x30, 0x1b8000},
	{ 0x30, 0x1c0000},
	{ 0x30, 0x1c8000},
	{ 0x30, 0x1d0000},
	{ 0x30, 0x1d8000},
	{ 0x30, 0x1e0000},
	{ 0x30, 0x1e8000},
	{ 0x30, 0x1f0000},
	{ 0x30, 0x1f8000}
};

/* Array of all the sector sizes */

const WORD32 STM_M29W320DB_flash_block_size_array[] =
{
	0x4000,  0x2000,  0x2000,  0x8000,  0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000, 0x10000,
	0x10000, 0x10000, 0x10000, 0x10000
};
#endif

static const flash_id_table_t flash_id_table[] =
{
#ifdef FOXJET
	{									/* FoxJet board #1 */
		//	  0x01, 0x00, 0x2249, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		0x01, 0x00, 0x2249, 0x01, 0x0023U, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD160_flash_bottom_sector_erase_cmd,
		(WORD32 *) amd160_bottom_sector_size_array
	},

	{									/* NetSilicon development board */
		0x01, 0x00, 0x225b, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_bottom_sector_erase_cmd,
		(WORD32 *) amd_bottom_sector_size_array
	}
#else if
	{
		0xB0, 0x00, 0x50, 0x01, SHARP_FLASH_SECTORS, SHARP_SECTOR_SIZE, SHARP_PROG_SECTOR_SIZE, 70,
		(flash_cmd_t *)SHARP_flash_id_enter_cmd,sizeof( SHARP_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SHARP_flash_id_exit_cmd, sizeof( SHARP_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SHARP_flash_erase_cmd, sizeof( SHARP_flash_erase_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SHARP_flash_write_cmd, sizeof( SHARP_flash_write_cmd ) / sizeof( flash_cmd_t ),
		NULL,
		(WORD32 *)0
	},

	{
		0x1F, 0x00, 0x5B, 0x01, AT_FLASH_SECTORS, AT_SECTOR_SIZE, AT_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		0, 0,
		0, 0,
		(flash_cmd_t *)AT_flash_write_cmd, sizeof( AT_flash_write_cmd ) / sizeof( flash_cmd_t ),
		NULL,
		(WORD32 *)0
	},

	{
		0x1F, 0x00, 0xA4, 0x01, AT_A_FLASH_SECTORS, AT_A_SECTOR_SIZE, AT_A_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AT_flash_id_exit_cmd, sizeof( AT_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		0, 0,
		(flash_cmd_t *)AT_flash_write_cmd, sizeof( AT_flash_write_cmd ) / sizeof( flash_cmd_t ),
		NULL,
		(WORD32 *)0
	},

	{
		0xC2, 0x00, 0x41, 0x01, MX_FLASH_SECTORS, MX_SECTOR_SIZE, MX_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)MX_flash_id_cmd, sizeof( MX_flash_id_cmd ) / sizeof( flash_cmd_t ),
		0, 0,
		(flash_cmd_t *)MX_flash_erase_cmd, sizeof( MX_flash_erase_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)MX_flash_write_cmd, sizeof( MX_flash_write_cmd ) / sizeof( flash_cmd_t ),
		NULL,
		(WORD32 *)0
	},

	{
		0xBF, 0x00, 0xA4, 0x01, SST_FLASH_SECTORS, SST_SECTOR_SIZE, SST_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)SST_flash_id_enter_cmd, sizeof( SST_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SST_flash_id_exit_cmd, sizeof( SST_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SST_flash_erase_cmd, sizeof( SST_flash_erase_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)SST_flash_write_cmd, sizeof( SST_flash_write_cmd ) / sizeof( flash_cmd_t ),
		NULL,
		(WORD32 *)0
	},

	{
		0x01, 0x00, 0x2258, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_bottom_sector_erase_cmd,
		(WORD32 *) amd_bottom_sector_size_array
	},

	{
		0x01, 0x00, 0x225b, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)  AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)  AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)  AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)  AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_bottom_sector_erase_cmd,
		(WORD32 *) amd_bottom_sector_size_array
	},

	{
		0x01, 0x00, 0x22d6, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_top_sector_erase_cmd,
		(WORD32 *) amd_top_sector_size_array
	},

	{
		0x01, 0x00, 0x22da, 0x01, AMD_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_flash_top_sector_erase_cmd,
		(WORD32 *)amd_top_sector_size_array
	},

	{
		0x1F, 0x00, 0x00CB, 0x01, AT_49BV8011_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AT_49BV8011_PROG_SECTOR_SIZE, 160,
		(flash_cmd_t *) AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_id_exit_cmd, sizeof( AT_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_erase_cmd, sizeof(AT_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AT_flash_write_cmd, sizeof(AT_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AT_flash_AT49BV8011_sector_erase_cmd,
		(WORD32 *)AT_AT49BV8011_sector_size_array
	},

	{
		0x04, 0x00, 0x225b, 0x01, FUJITSU_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, FUJITSU_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *)  AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)  AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *)  AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)  AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_bottom_sector_erase_cmd,
		(WORD32 *) amd_bottom_sector_size_array
	},

	{
		0x1F, 0x00, 0x00CB, 0x01, AT_49BV8011_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AT_49BV8011_PROG_SECTOR_SIZE, 160,
		(flash_cmd_t *) AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_id_exit_cmd, sizeof( AT_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_erase_cmd, sizeof(AT_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AT_flash_write_cmd, sizeof(AT_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AT_flash_AT49BV8011_sector_erase_cmd,
		(WORD32 *)AT_AT49BV8011_sector_size_array
	},

	{
		0x1F, 0x00, 0x004A, 0x01, AT_49BV8011_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AT_49BV8011_PROG_SECTOR_SIZE, 160,
		(flash_cmd_t *) AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_id_exit_cmd, sizeof( AT_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_erase_cmd, sizeof(AT_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AT_flash_write_cmd, sizeof(AT_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AT_flash_AT49BV8011T_sector_erase_cmd,
		(WORD32 *)AT_AT49BV8011T_sector_size_array
	},

	{
		0xBF, 0x00, 0x2781, 0x01, SST_39VF800_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, SST_39VF800_PROG_SECTOR_SIZE, 160,
		(flash_cmd_t *) SST_39VF800_flash_id_enter_cmd, sizeof( SST_39VF800_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) SST_39VF800_flash_id_exit_cmd, sizeof( SST_39VF800_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) SST_39VF800_flash_erase_cmd, sizeof(SST_39VF800_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) SST_39VF800_flash_write_cmd, sizeof(SST_39VF800_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)SST_39VF800_flash_block_erase_cmd,
		(WORD32 *)SST_39VF800_flash_block_size_array
	},

	{
		0x20, 0x00, 0x005B, 0x01, STM_M29W800AB_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, STM_M29W800AB_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) STM_M29W800AB_flash_id_enter_cmd, sizeof( STM_M29W800AB_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) STM_M29W800AB_flash_id_exit_cmd, sizeof( STM_M29W800AB_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) STM_M29W800AB_flash_erase_cmd, sizeof(STM_M29W800AB_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) STM_M29W800AB_flash_write_cmd, sizeof(STM_M29W800AB_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)STM_M29W800AB_flash_block_erase_cmd,
		(WORD32 *)STM_M29W800AB_flash_block_size_array
	},
	/* Since the driver only supports up to 2 Meg of flash, only the first 32 sectors of the 29DL323D are used */
	/* To increase the size of usage, please modify the flash memory mask and change the maximum number of  */
	/* sectors to what the chip can support.  */
	{
		0x01, 0x00, 0x2250, 0x01, AMD_29DL323D_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_29DL323D_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_29DL323D_flash_top_sector_erase_cmd,
		(WORD32 *) amd_29DL323D_top_sector_size_array
	},

	{									/* Since the driver only supports up to 2 Meg of flash, only the first 32 sectors are used */
		0x01, 0x00, 0x2253, 0x01, AMD_29DL323D_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AMD_29DL323D_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) AMD_flash_id_enter_cmd, sizeof( AMD_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_id_exit_cmd, sizeof( AMD_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AMD_flash_erase_cmd, sizeof(AMD_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AMD_flash_write_cmd, sizeof(AMD_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AMD_29DL323D_flash_bottom_sector_erase_cmd,
		(WORD32 *)amd_29DL323D_bottom_sector_size_array
	},

	{
		0x1F, 0x00, 0x00C0, 0x01, AT_49BV1614A_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, AT_49BV1614A_PROG_SECTOR_SIZE, 160,
		(flash_cmd_t *) AT_flash_id_enter_cmd, sizeof( AT_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_id_exit_cmd, sizeof( AT_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) AT_flash_erase_cmd, sizeof(AT_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) AT_flash_write_cmd, sizeof(AT_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)AT_flash_AT49BV1614A_sector_erase_cmd,
		(WORD32 *)AT_AT49BV1614A_sector_size_array
	},
	{
		0x20, 0x00, 0x22cb, 0x01, STM_M29W320DB_FLASH_SECTORS, VARIABLE_SECTOR_SIZE, STM_M29W320DB_PROG_SECTOR_SIZE, 120,
		(flash_cmd_t *) STM_M29W320DB_flash_id_enter_cmd, sizeof(STM_M29W320DB_flash_id_enter_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) STM_M29W320DB_flash_id_exit_cmd, sizeof(STM_M29W320DB_flash_id_exit_cmd ) / sizeof( flash_cmd_t ),
		(flash_cmd_t *) STM_M29W320DB_flash_erase_cmd, sizeof(STM_M29W320DB_flash_erase_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *) STM_M29W320DB_flash_write_cmd, sizeof(STM_M29W320DB_flash_write_cmd) / sizeof(flash_cmd_t),
		(flash_cmd_t *)STM_M29W320DB_flash_block_erase_cmd,
		(WORD32 *)STM_M29W320DB_flash_block_size_array
	},
#endif
};

static const WORD8 flash_num_id = sizeof( flash_id_table ) / sizeof( flash_id_table_t );

#ifdef FOXJET
/*
 * NAflash_write_fj
 *
 * modified at FoxJet to always assume the input is equal to one physical ROM sector.
 * physical ROM sectors are not always 32k.
 * the AMD ROM on the prototype board has variable sector sizes, most are 64k.
 * buffering is removed.
 *
 * This routine is used to program 1 flash rom sector.
 *
 * This is an external API routine.
 *
 * INPUTS
 *
 *    sectorNumber       Specifies hardware to be programmed.
 *    sectorData         A pointer specifying the flash data.
 *
 * RETURNS
 *
 *    0                  The sector was programmed successfully.
 *   <0                  An error occurred programming the flash.
 *
 */
int NAflash_write_fj (WORD32 sectorNumber, WORD16 *sectorData)
{
	int     status;

	/* verify flash, if it is the same, then don't program it, save time */
	status = LVerifySector(sectorNumber, sectorData);
	if (status == SUCCESS)
		return (status);

	/* erase flash */
	status = LEraseSector(sectorNumber);
	if (status != SUCCESS)
		return (3);

	/* program flash */
	status = LProgramSector(sectorNumber, sectorData);
	if (status != SUCCESS)
		return (4);

	/* verify flash */
	status = LVerifySector(sectorNumber, sectorData);
	if (status != SUCCESS)
		return (5);

	return (status);
}
#else if
/*
 * NAprogram_flash
 *
 * This routine is used to program an entire flash rom. Currently flash rom is
 * 1 megabyte in size (1024 ^2) for 16-bit mode or 2 megabyte in size for 32-bit mode
 * flash.
 *
 *      Flash cannot be accessed while it is being written.  Therefore, the application
 *      must be running from RAM when this function is called.
 *
 *      The function only programs the part attached to CS0, and only works on
 *      development boards configured for 16-bit 1Meg flash or 32-bit 2Meg flash
 *
 *      To configure the development board to 32-bit mode, move jumpers P27, P28, P29
 *      from pin 1-2 position to pin 2-3 position, and make sure CS00 of SW3 is OFF
 *      and CS01 of SW4 is ON.
 *
 *
 *
 * This is an external API routine.
 *
 * INPUTS
 *
 *    flashdata        Points to a 1 megabyte(16-bit mode) buffer or a 2 megabyte
 *                     buffer(32-bit mode) containing flash data.
 * RETURNS
 *
 *    0                  The whole flash was programmed successfully.
 *   <0                  An error occurred programming the flash.
 *    -1                  UNKNOWN_FLASH
 *    -2                  ERASE_FAILED
 *    -3                  WRITE_FAILED
 *    -4                  VERIFY_FAILED
 *    -5                  SECTOR_NUMBER_INVALID
 *    -6                  MEMORY_OUTOFBOUND
 *
 */

int NAprogram_flash (char *flashdata)
{

	WORD32 segment,total_segment;
	char *flashdatap;
	int retval;
	void NAFlashEnable ();

	total_segment = (physical_size ((*(unsigned long *)0xffc00014 & 0xfffff000)))>> 15;

	/* to check if we're out of data memory boundary to avoid possible Data Abort */

#ifdef NETOS_GNU_TOOLS
	if ((flashdata + SEGMENT_SIZE * total_segment) >= (char *)free_mem_begin)
#else
		if ((flashdata + SEGMENT_SIZE * total_segment) >= (char *)__ghsend_free_mem)
#endif
	{
		return (MEMORY_OUTOFBOUND);
	}

	NAFlashEnable ();
	for (flashdatap = flashdata, segment = 0; segment < total_segment; flashdatap += 0x8000, segment++)
	{

		retval = NAflash_write_local (segment, (unsigned short *)flashdatap, FAST_MODE);

		if (retval != SUCCESS)
			break;
	}

	return retval;
}
/*
 * NAflash_write
 *
 * This routine is used to program 1 flash rom segment. A flash rom segment is
 * 32K (32 * 1024) bytes of data. Currently there are a total of 32 segments for
 * 1M flash or 64 segments for 2M flash.
 *
 *      Flash cannot be accessed while it is being written.  Therefore, the application
 *      must be running from RAM when this function is called.
 *
 *      The function only programs the part attached to CS0, and only works on
 *      development boards configured for 16-bit 1Meg flash or 32-bit 2Meg flash
 *
 *      To configure the development board to 32-bit mode, move jumpers P27, P28, P29
 *      from pin 1-2 position to pin 2-3 position, and make sure CS00 of SW3 is OFF
 *      and CS01 of SW4 is ON.
 *
 *
 *
 * This is an external API routine.
 *
 * INPUTS
 *
 *    sectorNumber       Specifies segment to be programmed.
 *                       The range is 0 to 31 or 0 t0 63.
 *    sectorData         A pointer specifying the flash data.
 *
 * RETURNS
 *
 *    0                  The segment was programmed successfully.
 *   <0                  An error occurred programming the flash.
 *    -1                  UNKNOWN_FLASH
 *    -2                  ERASE_FAILED
 *    -3                  WRITE_FAILED
 *    -4                  VERIFY_FAILED
 *    -5                  SECTOR_NUMBER_INVALID
 *    -6                  MEMORY_OUTOFBOUND
 *
 */
int NAflash_write (WORD32 sectorNumber, WORD16 *sectorData)
{
	int rc;
	void NAFlashEnable ();

	NAFlashEnable ();

	rc = NAflash_write_local (sectorNumber, sectorData, SLOW_MODE);

	return (rc);
}
/*
 * NAflash_write_local
 *
 * Description:
 *
 * This routine is used to program 1 flash rom segment. A flash rom segment is
 * 32K (32 * 1024) bytes of data. Currently there are a total of 32 segments for
 * 1M flash or 64 segments for 2M flash. In the FAST_MODE, we don't program every
 * 32k data segment, instead, we buffer the data until we have enough segments
 * for one logical sector before we erase a sector. FAST_MODE always assume the
 * the segments arrive in order, in the SLOW_MODE, we do program every 32k data
 * segment,and we make sure other segments in a sector are still be safe.
 *
 * This is NOT an external API routine.
 *
 * Parameters:
 *
 *    sectorNumber       Specifies segment to be programmed.
 *                       The range is 0 to 31 or 0 t0 63 for
 *                       1M and 2M flash respectively.
 *    sectorData         A pointer specifying the flash data.
 *    mode               FAST_MODE or SLOW_MODE
 *
 *
 * RETURNS
 *
 *    0                  The segment was programmed successfully.
 *   <0                  An error occurred programming the flash.
 *    -1                  UNKNOWN_FLASH
 *    -2                  ERASE_FAILED
 *    -3                  WRITE_FAILED
 *    -4                  VERIFY_FAILED
 *    -5                  SECTOR_NUMBER_INVALID
 *    -6                  MEMORY_OUTOFBOUND
 *
 */
int NAflash_write_local (WORD32 sectorNumber, WORD16 *sectorData, int mode)
{
	int     status,i;
	WORD32  segmentNumber, currentSectorSize, currentSectorNumber, offset, *base;
	static  BYTE first_sector = 0;
	BYTE    *sData;
	static  WORD32 checksum = 0;
	BOOLEAN SectorMisAligned = FALSE;
	BOOLEAN TopSectorMisAligned = FALSE;
	BOOLEAN BottomSectorMisAligned = FALSE;
	WORD32  lastSegment;

	lastSegment = (physical_size ((*(unsigned long *)0xffc00014 & 0xfffff000)))>> 15 -1;
	if ((int)sectorNumber < 0 || sectorNumber > lastSegment)
	{
		return (SECTORNUMBER_INVALID);
	}

	/* to check if we're out of data memory boundary to avoid possible Data Abort */

#ifdef NETOS_GNU_TOOLS
	if (((char *)sectorData + SEGMENT_SIZE) >= (char *)free_mem_begin)
#else
		if (((char *)sectorData + SEGMENT_SIZE) >= (char *)__ghsend_free_mem)
#endif
	{
		return (MEMORY_OUTOFBOUND);
	}

	/*
	 *
	 */
	segmentNumber = sectorNumber;
	sData = (BYTE *)sectorData;

	/*
	 *calculate checksum
	 */
	base = (WORD32 *)sectorData;
	for(i=0; i < SEGMENT_SIZE/4; i++)
		checksum += base[i];

	if (segmentNumber == lastSegment)
	{
		checksum -= base[SEGMENT_SIZE/4 - 1];
		base[SEGMENT_SIZE/4 - 1] = ~checksum;
		checksum =0;
	}

	/*******************************************************************************************
	 *
	 *
	 * Usually, when identify_flash() returns 0xFF, it means either flash is not turned on or
	 * your flash is not in the flash table. If your flash is 3V AMD compatible flash, you can
	 * default flash_id to be 0x06, and do NOT return (UNKNOWN_FLASH).
	 *
	 *
	 *******************************************************************************************/
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();

		if (flash_id == 0xFF)
		{
			flash_id = 0x6;
			/* return (UNKNOWN_FLASH);*/
		}
	}

	/* this segment's starting sector number and size */
	currentSectorNumber = LGetSectorNumber(flash_id, segmentNumber, 0);
	currentSectorSize = LGetSectorSize(currentSectorNumber);

	if (mode == FAST_MODE)
	{

		/*
		 * FAST_MODE
		 * in the FAST_MODE, we don't program every 32k data segment, instead, we buffer
		 * the data until we have enough segments for one logical sector before we erase
		 * a sector. FAST_MODE always assume the the segments arrive in order.
		 *
		 * Check to see if we're in a 64K(16-bit) or 128k(32-bit) sector, if so we need to
		 * save the data since our segment size is only 32k.
		 * we will need to erase and write the entire sector.
		 */
		/* 64k case */
		if ((currentSectorSize == 0x10000) && (first_sector < 2))
		{

			memcpy(&first_sector_data[first_sector * SEGMENT_SIZE], sData, SEGMENT_SIZE);

			if (first_sector == 0)
			{
				first_sector++;
				return (SUCCESS);
			}

			first_sector = 0;
		}
		/* 128k case */
		if ((currentSectorSize == 0x20000) && (first_sector < 4))
		{

			memcpy(&first_sector_data[first_sector * SEGMENT_SIZE], sData, SEGMENT_SIZE);

			if (first_sector < 3)
			{
				first_sector++;
				return (SUCCESS);
			}

			first_sector = 0;

		}
	}

	else								/* SLOW_MODE */
	{

		/*
		 * SLOW_MODE
		 * in the SLOW_MODE, we do program every 32k data segment,and we make sure other
		 * segments in a sector are still be safe.

		 * Check to see if we're in a 64K(16-bit) or 128k(32-bit) sector, if so we need to
		 * save the other segment data since our segment size is only 32k.
		 * we will need to erase and rewrite the entire sector.
		 */
		/* 64k case */
		if (currentSectorSize == 0x10000)
		{

			/* Setup addressing */
			LSetSectorAddress(currentSectorNumber);

			/* read out the whole sector data */
			if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
				memcpy(&first_sector_data, (char *) mSectorBase, currentSectorSize);
			else
				memcpy(&first_sector_data, (char *) mSectorBase32, currentSectorSize);

			/* then replace a segment with new data */
			memcpy(&first_sector_data[(segmentNumber %2) * SEGMENT_SIZE], sData, SEGMENT_SIZE);

		}
		/* 128k case */
		if (currentSectorSize == 0x20000)
		{

			/* Setup addressing */
			LSetSectorAddress(currentSectorNumber);

			/* read out the whole sector data */
			if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
				memcpy(&first_sector_data, (char *) mSectorBase, currentSectorSize);
			else
				memcpy(&first_sector_data, (char *) mSectorBase32, currentSectorSize);

			/* then replace a segment with new data */
			memcpy(&first_sector_data[(segmentNumber %4) * SEGMENT_SIZE], sData, SEGMENT_SIZE);

		}

	}									/* end FAST_MODE or SLOW_MODE */

	/* we have gathered 64k or 128k data, let's program it */
	if ((currentSectorSize == 0x20000) || (currentSectorSize == 0x10000))
	{
		/* verify flash, if it is the same as the coming data, then don't program it, save time */
		status = LVerifySector(currentSectorNumber, (WORD16 *)first_sector_data);
		if (status != SUCCESS)
		{

			/* erase flash */
			status = LEraseSector(currentSectorNumber);
			if (status != SUCCESS)
				return (ERASE_FAILED);

			/* program flash */
			status = LProgramSector(currentSectorNumber, (WORD16 *)first_sector_data);
			if (status != SUCCESS)
				return (WRITE_FAILED);

			/* verify flash */
			status = LVerifySector(currentSectorNumber, (WORD16 *)first_sector_data);
			if (status != SUCCESS)
				return (VERIFY_FAILED);

		}

		return (status);
	}

	/* let's handle the smaller sector size here */
	/* The following section calculate and handle misalignment from the top of the sector */

										/* offset is number of bytes */
	offset = LIsSegmentMisAligned(currentSectorNumber);
	if (offset)							/* that can be written for this sector */
	{
		LSetSectorAddress(currentSectorNumber);

		/* read out the whole sector data */
		if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
			memcpy(&first_sector_data, (char *) mSectorBase, currentSectorSize);
		else
			memcpy(&first_sector_data, (char *) mSectorBase32, currentSectorSize);

		/* then replace a segment with new data */
		memcpy(&first_sector_data[currentSectorSize-offset], sData, offset);
		SectorMisAligned = TRUE;
		TopSectorMisAligned = TRUE;
	}
	/* done with the top section misalignment test */
	do
	{

		/* verify flash, if it is the same as the coming data, then don't program it, save time */
		if (!((SectorMisAligned && TopSectorMisAligned)||(SectorMisAligned && BottomSectorMisAligned)))
		{
			status = LVerifySector(currentSectorNumber, (WORD16 *)&sData[offset]);

		}
		else
		{
			status = LVerifySector(currentSectorNumber, (WORD16 *)first_sector_data);

		}
		if (status != SUCCESS)
		{

			/* erase flash */
			status = LEraseSector(currentSectorNumber);
			if (status != SUCCESS)
			{
				printf("LEraseSector return: %d sector \n",status);
				return (ERASE_FAILED);
			}

			/* program flash */
			if (!((SectorMisAligned && TopSectorMisAligned)||(SectorMisAligned && BottomSectorMisAligned)))
				status = LProgramSector(currentSectorNumber, (WORD16 *)&sData[offset]);
			else
				status = LProgramSector(currentSectorNumber, (WORD16 *)first_sector_data);
			if (status != SUCCESS)
			{

				return (WRITE_FAILED);
			}

			/* verify flash */
			if (!((SectorMisAligned && TopSectorMisAligned)||(SectorMisAligned && BottomSectorMisAligned)))
				status = LVerifySector(currentSectorNumber, (WORD16 *)&sData[offset]);
			else
				status = LVerifySector(currentSectorNumber, (WORD16 *)first_sector_data);

			if (status != SUCCESS)
				return (VERIFY_FAILED);

		}

		/* increase the offset */
										/* otherwise offset already has the current value */
		if (TopSectorMisAligned && SectorMisAligned)
		{

			SectorMisAligned = FALSE;
			TopSectorMisAligned = FALSE;
		}
		else
		{
			offset+= currentSectorSize;
			BottomSectorMisAligned = FALSE;
			SectorMisAligned = FALSE;
		}
		currentSectorNumber = LGetSectorNumber(flash_id, segmentNumber, offset);
		currentSectorSize = LGetSectorSize(currentSectorNumber);
		/* bottom section of the sector misalignment */
		if (((offset+currentSectorSize) > SEGMENT_SIZE) && (offset < SEGMENT_SIZE))
		{
			/* Setup addressing */
			LSetSectorAddress(currentSectorNumber);

			/* read out the whole sector data */
			if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
				memcpy(&first_sector_data, (char *) mSectorBase, currentSectorSize);
			else
				memcpy(&first_sector_data, (char *) mSectorBase32, currentSectorSize);

			/* then replace a segment with new data */
			memcpy(&first_sector_data, sData+offset, SEGMENT_SIZE-offset);
			SectorMisAligned = TRUE;
			BottomSectorMisAligned = TRUE;
		}
	} while (offset < SEGMENT_SIZE);
	return (status);
}
#endif

/*
 *  WORD8 exec_flash_cmd ( flash_cmd_t * , WORD16 )
 *
 *  Description:
 *
 *  This function executes the sequence of flash commands
 *
 * Arguments:
 *    command:     pointer to the flash command table
 *    cmd_len:     command length
 *
 * Return:
 *    0            no command needed
 *    cmd          command executed
 *
 */

static WORD8 exec_flash_cmd ( flash_cmd_t *command, WORD16 cmd_len )
{
	WORD8   cmd;
	WORD16  ccode_word, *sectorBase= 0;
	WORD32  ccode_word32, cmd_address, *sectorBase32=0, offsetFromBase, offsetFromBase32, i;
	static WORD32 flashsize = 0;

	if ( !command )
		return 0;

	/*
	 *  If the address of the first command is non-zero,
	 *  we need to reset the mSectorBase to the first sector because
	 *  the address of the command is the absolute address of the
	 *  flash address space, not the offset from the current sector
	 */

	if (command[0].ccode_addr)
	{
		sectorBase = mSectorBase;
		sectorBase32 = mSectorBase32;

		mSectorBase = (WORD16 *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
		mSectorBase32 = (WORD32 *)mSectorBase;

		offsetFromBase = (WORD32)sectorBase - (WORD32)mSectorBase;
		offsetFromBase32 = (WORD32)sectorBase32 - (WORD32)mSectorBase32;

		if (flash_id != 0xFF)
		{
			/* flashsize is allocated as a static variable to avoid recomputing
			   flash size each time exec_flash_cmd() is executed */
			if (!flashsize)
			{
				for (i = 0; i < flash_id_table[flash_id].total_sector_number; i++)
					flashsize += LGetSectorSize(i);
			}

			/* 16 bit flash */
			if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
			{
				if (offsetFromBase >= flashsize)
				{
					mSectorBase += (offsetFromBase / flashsize) * flashsize / 2;
				}
			}
			else						/* 32 bit flash */
			{
				if (offsetFromBase32 >= flashsize)
				{
					mSectorBase32 += (offsetFromBase32 / flashsize) * flashsize / 4;
				}
			}
		}
	}

	for (cmd = 0; cmd_len > 0; cmd++, cmd_len-- )
	{
										/* 16 bit flash */
		if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
		{
			ccode_word = command[cmd].ccode;
			ccode_word <<= 8;
			ccode_word |= command[cmd].ccode;
			LWriteWord( command[cmd].ccode_addr, (WORD32)ccode_word );
		}

		else							/* 32 bit flash */
		{
			ccode_word = command[cmd].ccode;
			ccode_word <<= 8;
			ccode_word |= command[cmd].ccode;
			ccode_word32 = ccode_word;
			ccode_word32 <<= 16;
			ccode_word32 |= ccode_word;	/* command is 32 bit */
			cmd_address = command[cmd].ccode_addr;
			LWriteWord( cmd_address, ccode_word32 );
		}

	}

	if (command[0].ccode_addr)
	{
		mSectorBase = sectorBase;
		mSectorBase32 = sectorBase32;
	}

	return cmd;
}
/*
 *  WORD8 identify_flash (  )
 *
 * Description:
 *
 *  This function identifies the flash chip by checking
 *  the manufacture code and device code
 *
 * Arguments:
 *    none
 *
 * Return:
 *    flash_id:    index in the flash table
 *
 */

#ifdef FOXJET
WORD8 identify_flash ( void )
#else if
static WORD8 identify_flash ( void )
#endif
{
	WORD8   id;
	WORD32  id_addr, id_mcode, id_dcode, temp_mcode, temp_dcode;

#ifdef FOXJET
	if ( -1 != fj_ROMId )
		return(fj_ROMId);
#endif

	mSectorBase = (WORD16 *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
	mSectorBase32 = (WORD32 *)mSectorBase;

	LFlashWriteOn();

	for (id = 0; id < flash_num_id; id++)
	{
		/* Enter Product Identification Mode */
		if (exec_flash_cmd( flash_id_table[id].id_enter_cmd,
			flash_id_table[id].id_enter_len ))
			LWait(500UL);

		/* Read the manufacturer code */

		id_addr  = (WORD32)flash_id_table[id].mcode_addr;
		id_mcode = LReadWord( id_addr );

		/* If there is no Exit Product Identificate Mode,       *
		 * we most likely need to execute the enter mode again. */

		if (!flash_id_table[id].id_exit_cmd &&
			exec_flash_cmd( flash_id_table[id].id_enter_cmd,
			flash_id_table[id].id_enter_len ))
			LWait(500UL);

		/* Read the device code */
		id_addr  = (WORD32)flash_id_table[id].dcode_addr;
		id_dcode = LReadWord( id_addr );

		/* Exit Product Identification Mode */
		if (exec_flash_cmd( flash_id_table[id].id_exit_cmd,
			flash_id_table[id].id_exit_len ))
			LWait(500UL);

		/* temp = (*NCC_MEM).cs0.csor.reg; */

										/* 16 bit flash */
		if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
		{

			if ((WORD16)id_mcode == flash_id_table[id].mcode &&
				(WORD16)id_dcode == flash_id_table[id].dcode)
				break;
		}
		else							/* 32 bit flash */
		{
			temp_mcode = flash_id_table[id].mcode;
			temp_mcode <<= 16;
			temp_mcode |= flash_id_table[id].mcode;

			temp_dcode = flash_id_table[id].dcode;
			temp_dcode <<= 16;
			temp_dcode |= flash_id_table[id].dcode;

			if (id_mcode == temp_mcode && id_dcode == temp_dcode)
				break;
		}

	}

	TurnFlashWriteOff();

	if ( id == flash_num_id )
		return 0xFF;
#ifdef FOXJET
	else
	{
		fj_ROMId = id;
		fj_ROMType = id_dcode & 0x0000ffff;
	}
#endif

	return id;
}
/*
 * static int LEraseSector(WORD32 sectorNumber)
 *
 * Description:
 *    erease a sector before write
 *
 * Arguments:
 *    sector_number:   Which sector are we in.
 *
 * Return:
 *    0:  sector has been successfully erased
 *   >0:  sector erase failure
 *
 */
#define MAX_ERASE_RETRIES      20

static int LEraseSector(WORD32 sectorNumber)
{
	register WORD32  data, sanityCounter;
	WORD16   retryCounter=0;
	int done = 0;

	/* Check if there is an erase command for this flash */

	if (!flash_id_table[flash_id].erase_cmd )
		return (SUCCESS);

	/* This will only slow things down if the non-Fs are at the
	 * end of a sector, as they are in the last production sector
	 */

	if (LVerifyAllFs(sectorNumber))
	{

		return (SUCCESS);

	}

	/* Setup addressing */
	LSetSectorAddress(sectorNumber);

	/* erase begins here */
	LFlashWriteOn();
	LResetWord(0UL);

	/* Loop once for flash sector to erase. */
	do
	{
		/* Write begin erase block commands */
		if (exec_flash_cmd( flash_id_table[flash_id].erase_cmd,
			flash_id_table[flash_id].erase_len ))
		{
			/*
			 * If were are AMD type flash we need to give a specific
			 * command to erase the each sector.
			 */
			if (flash_id_table [flash_id].sector_erase_cmd)
			{
				sectorNumber %= flash_id_table[flash_id].total_sector_number;
				exec_flash_cmd(&flash_id_table [flash_id].sector_erase_cmd[sectorNumber], 1);
			}

			LWait(500UL);				/* wait for erase take place and finish */

			/* Loop until the erase is signalled as done, or we've gotten
			 * 1024 successful polls, but no good data
			 * The "done" signal is when bit 7 is set in each byte of the word
			 */
			sanityCounter = 0;
			do
			{
				sanityCounter++;
				LResetWord(0UL);
				data = LReadWord(0UL);

				/* 16 bit flash */
				if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
					done = ((WORD16)data == 0xFFFF);
				else					/* 32 bit flash */
					done = (data == 0xFFFFFFFF);

			} while ((sanityCounter < 1024) && (!done));

		}

		retryCounter++;

	} while ((retryCounter < MAX_ERASE_RETRIES) && (!done));

	if (retryCounter == MAX_ERASE_RETRIES)
	{
		TurnFlashWriteOff();
		return (ERASE_FAILED);
	}

	TurnFlashWriteOff();
	return (SUCCESS);
}
/* Actually write the ROM
 *
 * static int LProgramSector(WORD32 sectorNumber, register WORD16 *sectorData)
 *
 * Description:
 *
 *    Actually write the ROM
 *
 * Arguments:
 *    sector_number:   Which sector are we in.
 *    sectorData:      pointer to a 16-bit array
 *
 * Return:
 *    0:  sector has been successfully writen
 *   >0:  sector write failure
 *
 */
#define MAX_WRITE_RETRIES    20

static int LProgramSector(WORD32 sectorNumber, register WORD16 *sectorData)
{
	register WORD32 i, j, startData, data_index = 0, writeData ;
	WORD16          sanityCounter, prog_size;
	WORD32   size;						/*, *sectorData32 */
	register char *bufferData;

	/* sectorData32 = (WORD32 *)sectorData; */
	bufferData = (char *)sectorData;

	/* Set up Addressing and size */
	LSetSectorAddress(sectorNumber);
	prog_size = flash_id_table[flash_id].prog_size;
	size = LGetSectorSize(sectorNumber);

	/* writing begins */
	LFlashWriteOn();
	LResetWord(0UL);

	/* we check if we have 16-bit flash or 32-bit flash before we really write
	   so the write wouldn't be slowed down by checking this in many other places */

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		for (i = 0; i < size / prog_size; i++)
		{
			sanityCounter = 0;
			writeData = (WORD32)(*bufferData) << 8;
			bufferData++;
			writeData |= (WORD32)(*bufferData);
			bufferData++;
			do
			{
				if ( flash_id_table[flash_id].erase_cmd )
				{
					/* Check that we can do a write. If writing this flashWordT
					 * to this address will change a 0 into a 1, then we can't
					 * write (which is why Erase changes everything into FF)
					 */

					startData = LReadWord(i);
					if ((startData | writeData) != startData)
					{
						TurnFlashWriteOff();
						return (1);
					}
					//if ((startData | (WORD32)sectorData[i]) != startData)
					//{
					//    TurnFlashWriteOff();
					//    return (1);
					//}
				}

				/* Write the write command */
				exec_flash_cmd( flash_id_table[flash_id].write_cmd,
					flash_id_table[flash_id].write_len );

				/* Load the data into flash memory */
				for (j = 0; j < prog_size / 2; j++)
				{
					data_index = i * prog_size / 2 + j;
					LWriteWord(data_index, writeData);
					//LWriteWord(data_index, (WORD32)sectorData[data_index]);
					/* printf("l= 0x%x, d= 0x%x\n",data_index,(WORD32)sectorData[data_index]);*/
				}

				/* Wait until poll complete, and reset ourselves */

				LPollWord(data_index, 1UL);
				sanityCounter++;
			}
			while ( (LReadWord(data_index) != writeData) &&
				(sanityCounter < MAX_WRITE_RETRIES) );

			//while ( (LReadWord(data_index) != sectorData[data_index]) &&
			//        (sanityCounter < MAX_WRITE_RETRIES) );

			if (sanityCounter == MAX_WRITE_RETRIES)
			{
				TurnFlashWriteOff();
				return (2);
			}
		}
	}

	else								/* 32 bit flash */
	{
		for (i = 0; i < (size / prog_size)/2; i++)
		{
			sanityCounter = 0;
			writeData = (WORD32)(*bufferData) << 24;
			bufferData++;
			writeData |= (WORD32)(*bufferData) << 16;
			bufferData++;
			writeData |= (WORD32)(*bufferData) << 8;
			bufferData++;
			writeData |= (WORD32)(*bufferData);
			bufferData++;
			do
			{
				if ( flash_id_table[flash_id].erase_cmd )
				{
					/* Check that we can do a write. If writing this flashWordT
					 * to this address will change a 0 into a 1, then we can't
					 * write (which is why Erase changes everything into FF)
					 */

					startData = LReadWord(i);
					if ((startData | writeData) != startData)
					{
						TurnFlashWriteOff();
						return (1);
					}
					//if ((startData | sectorData32[i]) != startData)
					//{
					//    TurnFlashWriteOff();
					//    return (1);
					//}
				}

				/* Write the write command */
				exec_flash_cmd( flash_id_table[flash_id].write_cmd,
					flash_id_table[flash_id].write_len );

				/* Load the data into flash memory */
				for (j = 0; j < prog_size / 2; j++)
				{
					data_index = i * prog_size / 2 + j;
					LWriteWord(data_index, writeData);
					//LWriteWord(data_index, sectorData32[data_index]);

				}

				/* Wait until poll complete, and reset ourselves */

				LPollWord(data_index, 1UL);
				sanityCounter++;

			}
			while ( (LReadWord(data_index) != writeData) &&
				(sanityCounter < MAX_WRITE_RETRIES) );

			//while ( (LReadWord(data_index) != sectorData32[data_index]) &&
			//        (sanityCounter < MAX_WRITE_RETRIES) );

			if (sanityCounter == MAX_WRITE_RETRIES)
			{
				TurnFlashWriteOff();
				return (2);
			}
		}

	}

	TurnFlashWriteOff();
	return (SUCCESS);
}
/*
 * LVerifyAllFs
 *
 * Description:
 *
 * verify if a logical sector filled with all "0xFF"
 *
 * Arguments:
 *    sector_number:   Which sector are we in.
 *
 * RETURNS
 *
 *    FALSE            the data is not 0Xff
 *    TRUE             all data are 0xff
 *
 */
static
BOOLEAN LVerifyAllFs(WORD32 sectorNumber)
{
	WORD16  i;
	WORD32  data, size;

	/* setup starting address and size */
	LSetSectorAddress(sectorNumber);
	size = LGetSectorSize(sectorNumber);

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{

		for (i = 0; i < size / 2; i++)
		{
			data = LReadWord(i);
			if (data != 0xFFFF)
			{
				return (FALSE);
			}
		}
	}

	else								/* 32 bit flash */
	{
		for (i = 0; i < size / 4; i++)
		{
			data = LReadWord(i);
			if (data != 0xFFFFFFFF)
			{
				return (FALSE);
			}
		}
	}

	return (TRUE);
}
/*
 * static int LVerifySector(WORD32 sectorNumber, WORD16 *sectorData)
 *
 * Description:
 *
 * A pretty simple block compare, using the low level command
 * to get data from the ROM segment
 * Just get data from ROM, compare to buffer, blow out if bad,  or kick
 * buffer along until the end.
 *
 * Arguments:
 *    sector_number:   Which sector are we in.
 *    sectorData:      pointer to 16-bit array
 *
 * RETURNS
 *
 *    VERIFY_FAILED:   the data is not match
 *    SUCCESS          all data match
 *
 */

static
int LVerifySector(WORD32 sectorNumber, WORD16 *sectorData)
{
	register WORD16  i;
	register WORD32  data, buf_data;
	WORD32  size;						/*, *sectorData32 */
	register char *bufferData;

	/* sectorData32 = (WORD32 *)sectorData; */
	bufferData = (char *)sectorData;

	LSetSectorAddress(sectorNumber);
	size = LGetSectorSize(sectorNumber);

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		for (i = 0; i < size / 2; i++)
		{
			data = LReadWord(i);
			buf_data = (WORD32)(*bufferData) << 8;
			bufferData++;
			buf_data |= (WORD32)(*bufferData);
			bufferData++;
			if (data != buf_data)
			{
				return (VERIFY_FAILED);
			}
			//if (data != (WORD32)(*sectorData))
			//{
			//    return (VERIFY_FAILED);
			//}
			//sectorData++;
		}
	}

	else								/* 32 bit flash */
	{
		for (i = 0; i < size / 4; i++)
		{
			data = LReadWord(i);
			buf_data = (WORD32)(*bufferData) << 24;
			bufferData++;
			buf_data |= (WORD32)(*bufferData) << 16;
			bufferData++;
			buf_data |= (WORD32)(*bufferData) << 8;
			bufferData++;
			buf_data |= (WORD32)(*bufferData);
			bufferData++;
			if (data != buf_data)
			{
				return (VERIFY_FAILED);
			}
			//if (data != *sectorData32)
			//{
			//    return (VERIFY_FAILED);
			//}
			//sectorData32++;
		}
	}

	return (SUCCESS);
}
/*
 * static void LSetSectorAddress(WORD32 sectorNumber)
 *
 * Description:
 *
 * this function set an starting address of a given sector.
 * according to the data book, sector is logically seperated and
 * each sector may have different size and different access command.
 *
 * Arguments:
 *    sector_number:   Which sector are we in.
 *
 * RETURNS
 *
 *      none
 *
 */
static void LSetSectorAddress(WORD32 sectorNumber)
{
	/* The previous implementation of this function uses a loop to
	   compute the sector base address.  As the sector numbers increase,
	   the longer it takes to compute the sector base address */

#if 0
	WORD16 i, k;

	mSectorBase = (WORD16 *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
	mSectorBase32 =  (WORD32 *)mSectorBase;

	if ( flash_id_table[flash_id].sector_size == VARIABLE_SECTOR_SIZE )
	{
		for (i=0; i < sectorNumber; i++)
		{
			k = i % flash_id_table[flash_id].total_sector_number;
			mSectorBase += flash_id_table[flash_id].sector_size_array[k]/2;
			mSectorBase32 += flash_id_table[flash_id].sector_size_array[k]/2;
		}
	}
	else
	{
		for (i=0; i < sectorNumber; i++)
		{
			mSectorBase += flash_id_table[flash_id].sector_size/2;
			mSectorBase32 += flash_id_table[flash_id].sector_size/2;
		}
	}
#endif

	/* The new implementation of this function uses static tables to
	   store the sector base addresses.  The sector base addresses are
	   computed only once and stored in the tables. Subsequent calls
	   to this function retrieve sector base addresses from the tables. */

	WORD16 i, k;
	WORD32 totalsectors;
	/* static variables used to build a table of sector
	   addresses for faster sector address retrieval */
	static int sectorBaseTableValid = 0;
	static WORD32 *mSectorBase32Table[MAX_SECTORS];
	static WORD16 *mSectorBaseTable[MAX_SECTORS];

	mSectorBase = (WORD16 *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
	mSectorBase32 =  (WORD32 *)mSectorBase;

	/* if sector base address table not valid, fill in the table and set flag to 1 */
	if (!sectorBaseTableValid)
	{
		totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

		if ( flash_id_table[flash_id].sector_size == VARIABLE_SECTOR_SIZE )
		{
			for (i = 0; i < totalsectors; i++)
			{
				k = i % flash_id_table[flash_id].total_sector_number;
				mSectorBase += flash_id_table[flash_id].sector_size_array[k]/2;
				mSectorBase32 += flash_id_table[flash_id].sector_size_array[k]/2;
				mSectorBaseTable[i] = mSectorBase;
				mSectorBase32Table[i] = mSectorBase32;
			}
		}
		else
		{
			for (i = 0; i < totalsectors; i++)
			{
				mSectorBase += flash_id_table[flash_id].sector_size/2;
				mSectorBase32 += flash_id_table[flash_id].sector_size/2;
				mSectorBaseTable[i] = mSectorBase;
				mSectorBase32Table[i] = mSectorBase32;
			}
		}
		sectorBaseTableValid = 1;
	}
	/* set the sector base address */
	if (sectorNumber)
	{
		mSectorBase = mSectorBaseTable[sectorNumber - 1];
		mSectorBase32 = mSectorBase32Table[sectorNumber - 1];
	}
	else
	{
		mSectorBase = (WORD16 *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
		mSectorBase32 =  (WORD32 *)mSectorBase;
	}
}
/*
 * static WORD32 LGetSectorSize(WORD32 sectorNumber)
 *
 * Description:
 *
 * Return the number of bytes in the given sector.
 *
 * Arguments:
 *    sectorNumber:   Which sector are we in.
 *
 * RETURNS
 *
 *      the number of bytes in the given sector.
 *
 */
#ifndef FOXJET
static
#endif
WORD32 LGetSectorSize(WORD32 sectorNumber)
{
	sectorNumber %= flash_id_table[flash_id].total_sector_number;

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		if ( flash_id_table[flash_id].sector_size == VARIABLE_SECTOR_SIZE )
			return (flash_id_table[flash_id].sector_size_array[sectorNumber]);
		else
			return (flash_id_table[flash_id].sector_size);
	}

	else								/* 32 bit flash */
	{
		if ( flash_id_table[flash_id].sector_size == VARIABLE_SECTOR_SIZE )
			return (flash_id_table[flash_id].sector_size_array[sectorNumber] * 2);
		else
			return (flash_id_table[flash_id].sector_size* 2);
	}

}
/*
 * static WORD16 LGetSectorNumber(WORD16 id, WORD16 segment_number, WORD32
 *                             segment_offset)
 * Description:
 *
 * Return the number of the current sector.
 *
 * Arguments:
 *
 *    flash_id
 *    segment_number:   Which 32K segment are we in.
 *    segment_offseet:  How many bytes into the 32K segment we are.
 *
 * RETURNS
 *
 *      the number of the current sector.
 */
#ifndef FOXJET
static
#endif
WORD16 LGetSectorNumber(WORD16 id, WORD16 segment_number, WORD32
segment_offset)
{
	WORD16 i, num_sectors = 0;
	WORD32 offset, start_addr = 0, end_addr = 0;

	start_addr = 0;

	/* Calculate how far into the flash we currently are */
	offset = segment_offset + (segment_number * SEGMENT_SIZE);

	num_sectors = flash_id_table[id].total_sector_number;

	for (i = 0; i < num_sectors; i++)
	{

		/* Point to the end of the sector where checking */
		end_addr += LGetSectorSize(i);

		if ((start_addr <= offset) && (offset < end_addr))
		{
			break;
		}

		start_addr += LGetSectorSize(i);
	}

	return(i);
}
#ifdef FOXJET
/*
 * added by FoxJet.
 * Return the starting offset of the current sector.
 *
 * Arguments:
 *    flash_id
 *    segment_number:   Which 32K segment are we in.
 *    segment_offseet:  How many bytes into the 32K segment we are.
 */
WORD32 LGetSectorOffset(WORD16 id, WORD16 segment_number, WORD32 segment_offset)
{
	WORD16 i, num_sectors = 0;
	WORD32 size, offset, start_addr = 0, end_addr = 0;

	start_addr = 0;

	/* Calculate how far into the flash we currently are */
	offset = segment_offset + (segment_number * SEGMENT_SIZE);

	num_sectors = flash_id_table[id].total_sector_number;

	for (i = 0; i < num_sectors; i++)
	{

		/* Point to the end of the sector where checking */
		end_addr += LGetSectorSize(i);

		if ((start_addr <= offset) && (offset < end_addr))
		{
			break;
		}

		start_addr += LGetSectorSize(i);
	}

	return(start_addr);
}
#endif

/*
 *  These are the low level routines used to access the flash rom
 */

/* ---------------------------------------------------- */
/*
 * LWait
 *
 * Description:
 *
 * simple delay
 *
 * Parameters:
 *      useconds     time to wait
 *
 * RETURNS
 *
 *      none
 *
 */
static
void LWait(WORD32 useconds)
{
	register WORD32 total_passes;
	register WORD32 loop_counter;
	/* From erase4m.asm, 2000 clicks in a loop == c. 100 us
	 * so, 1us = 20 loops...
	 *                   ... but try to speed it up some more
	 */
	total_passes = useconds * 2;
	loop_counter = 0;
	while (loop_counter < total_passes)
	{
		loop_counter++;
	}
}
/*
 * LWriteWord
 *
 * Description:
 *
 * Put the word into the segment.
 *
 * Parameters:
 *      whichWord    data index
 *      data         data to be written
 *
 * RETURNS
 *
 *      none
 *
 */

void
LWriteWord (WORD32 whichWord, WORD32 data)
{
										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
		ASMLWriteWord16 (whichWord, data, (WORD32) mSectorBase);

	else								/* 32 bit flash */
		ASMLWriteWord32 (whichWord, data, (WORD32) mSectorBase32);
}
/*
 * LReadWord
 *
 * Description:
 *
 * Read a word from the segment
 *
 * Parameters:
 *      whichWord    data index
 *
 * RETURNS
 *
 *      word from reading
 *
 */
static
WORD32 LReadWord(register WORD32 whichWord)
{

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
		return ( (WORD32) (mSectorBase[whichWord]) );

	else								/* 32 bit flash */
		return ( (mSectorBase32[whichWord]) );

}
/*
 * LPollWord
 *
 * Description:
 *
 * This code was pretty much lifted from the MXFLASH examples
 * The basic algorithm is: wait 100 useconds, then read data twice,
 * if the two reads are different, then things are still running, so
 * continue the loop.  I've added a sanityCounter to prevent an infinte
 * loop.
 *
 * Parameters:
 *      whichWord    data index
 *      pollWait     wait time in useconds
 *
 * RETURNS
 *
 *      none
 *
 */
static
void LPollWord(register WORD32 whichWord, WORD32 pollWait)
{
	register WORD32  data1;
	register WORD32  data2;
	WORD16  sanityCounter;

	sanityCounter = 0;
	do
	{
		LWait(pollWait);
		sanityCounter++;
		data1 = LReadWord(whichWord) & TOGGLE_BIT_MASK;
		data2 = LReadWord(whichWord) & TOGGLE_BIT_MASK;
	}
	while ((data1 != data2) && (sanityCounter < 10000));
}
/*
 * LResetWord
 *
 * Description:
 *
 * This resets the state-machine in the chip
 * -- write the reset commands and poll
 *
 * Parameters:
 *      whichWord
 *
 * RETURNS
 *
 *      none
 *
 */

static
void LResetWord(register WORD32 whichWord)
{
	LWriteWord(whichWord, 0xFFFFFFFF);
	LWriteWord(whichWord, 0xFFFFFFFF);
	LPollWord(whichWord, 4L);
}
/*
 * static void LFlashWriteOn(void)
 *
 * Description:
 *
 * This routine is used to enable writing to flash
 *
 * Parameters:
 *      none
 *
 * RETURNS
 *
 *      none
 *
 */
static void LFlashWriteOn(void)
{

	WORD32 temp, *p;

	/* turn cache off before writing to flash   */
	NACacheDisable();

	p = (WORD32 *) 0xffc00010;
	temp = *p;
	*p = temp & 0xfffffffd;

}
/*
 * TurnFlashWriteOff
 *
 * Description:
 *
 * This routine is used to disable writing to flash
 *
 * Parameters:
 *      none
 *
 * RETURNS
 *
 *      none
 *
 */

static void TurnFlashWriteOff(void)
{

	WORD32 temp, *p;

	p = (WORD32 *) 0xffc00010;
	temp = *p;
	*p = temp | 2;

	/*
	 *      This routine restores cache to it's previous settings saved the last time
	 *      NACacheEnable() or NACacheSetDefaults() was called.
	 */
	NACacheRestore();

}
/*
 * LIsSegmentMisAligned
 *
 * Description:
 *
 * This routine is used to calculate how much if any the segment is misaligned.
 *
 * Parameters:
 *      SegmentNumber: current segment number
 *
 * RETURNS
 *
 *       > 0:   number of bytes offseted.
 *	 = 0: no misalignment
 */

static WORD16 LIsSegmentMisAligned(WORD32 SegmentNumber)
{
	WORD32 size = 0;
	WORD32 temp = (WORD32) LGetSectorSize(SegmentNumber);
	while(SegmentNumber != ((WORD32)-1))
	{
		size += LGetSectorSize(SegmentNumber--);
	}
	size = (WORD16)(size- (size/SEGMENT_SIZE)*SEGMENT_SIZE);
	if (size != temp)
		return size;
	else
		return 0;
}
/*
 * static int LProgramPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToWrite, register char *sectorData)
 *
 * Description:
 *
 *    Writes a partial Flash memory sector as specified by the arguments
 *
 * Arguments:
 *    sectorNumber  - The sector to write
 *    sectorOffset  - The sector offset to start writing to
 *    bytesToWrite  - The number of bytes to write
 *    sectorData    - The pointer to a char array
 *
 * Return:
 *    0:  sector has been successfully writen
 *   >0:  sector write failure
 *
 */

static int LProgramPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToWrite, register char *sectorData)
{
	register WORD32 i, j, startData, data_index = 0, writeData;
	WORD16          sanityCounter, prog_size;
	WORD32   firstWord, lastWord, lastWriteOffset, partialWriteStart, partialWriteEnd;

	/* Set up Addressing and size */
	LSetSectorAddress(sectorNumber);
	prog_size = flash_id_table[flash_id].prog_size;
	/* size = LGetSectorSize(sectorNumber); */

	/* writing begins */
	LFlashWriteOn();
	LResetWord(0UL);

	/* we check if we have 16-bit flash or 32-bit flash before we really write
	   so the write wouldn't be slowed down by checking this in many other places */

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		/* find out the first word and last word to write */
		/* in 16 bit mode, a word is 2 bytes */
		firstWord = sectorOffset / prog_size;
		lastWord = (sectorOffset + bytesToWrite - 1) / prog_size;
		lastWriteOffset = sectorOffset + bytesToWrite - 1;

		for (i = firstWord; i <= lastWord; i++)
		{
			sanityCounter = 0;
			do
			{
				if ( flash_id_table[flash_id].erase_cmd )
				{
					/* Determine if the first and last words are partial word writes */
					if (i == firstWord)
					{
						partialWriteStart = sectorOffset % prog_size;

						if (partialWriteStart == 0)
						{
							if (bytesToWrite == 1)
							{
								/* the first byte of a 16 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0x000000FF;
								writeData |= ((WORD32)(*sectorData) << 8);
								sectorData++;
							}
							else if (bytesToWrite >= 2)
							{
								/* both bytes of the 16 bit word are written */
								writeData = (WORD32)(*sectorData) << 8;
								sectorData++;
								writeData |= (WORD32)(*sectorData);
								sectorData++;
								//writeData = (WORD32)(*(WORD16*)sectorData);
								//sectorData += 2;
							}
						}

						if (partialWriteStart == 1)
						{
							/* second byte of a 16 bit word is written */
							writeData = LReadWord(i);
							writeData &= 0x0000FF00;
							writeData |= (WORD32)(*sectorData);
							sectorData++;
						}

					}
					else if (i == lastWord)
					{
						partialWriteEnd = lastWriteOffset % prog_size;

						if (partialWriteEnd == 0)
						{
							/* the first byte of a 16 bit word is written */
							writeData = LReadWord(i);
							writeData &= 0x000000FF;
							writeData |= ((WORD32)(*sectorData) << 8);
						}
						else
						{
							/* both bytes of the 16 bit word are written */
							writeData = (WORD32)(*sectorData) << 8;
							sectorData++;
							writeData |= (WORD32)(*sectorData);
							//writeData = (WORD32)(*(WORD16*)sectorData);
						}
					}
					else
					{
						/* both bytes of the 16 bit word are written */
						writeData = (WORD32)(*sectorData) << 8;
						sectorData++;
						writeData |= (WORD32)(*sectorData);
						sectorData++;
						//writeData = (WORD32)(*(WORD16*)sectorData);
						//sectorData += 2;
					}

					/* Check that we can do a write. If writing this flashWordT
					 * to this address will change a 0 into a 1, then we can't
					 * write (which is why Erase changes everything into FF)
					 */

					startData = LReadWord(i);
					if ((startData | writeData) != startData)
					{
						TurnFlashWriteOff();
						return (1);
					}
				}
				/* Write the write command */
				exec_flash_cmd( flash_id_table[flash_id].write_cmd,
					flash_id_table[flash_id].write_len );

				/* Load the data into flash memory */
				for (j = 0; j < prog_size / 2; j++)
				{
					data_index = i * prog_size / 2 + j;
					LWriteWord(data_index, (WORD32)writeData);
					/* printf("l= 0x%x, d= 0x%x\n",data_index,(WORD32)sectorData[data_index]);*/
				}

				/* Wait until poll complete, and reset ourselves */

				LPollWord(data_index, 1UL);
				sanityCounter++;
			}
			while ( (LReadWord(data_index) != writeData) &&
				(sanityCounter < MAX_WRITE_RETRIES) );

			if (sanityCounter == MAX_WRITE_RETRIES)
			{
				TurnFlashWriteOff();
				return (2);
			}
		}
	}

	else								/* 32 bit flash */
	{
		/* find out the first word and last word to write */
		/* in 32 bit mode, a word is 4 bytes */
		firstWord = (sectorOffset / prog_size)/2;
		lastWord = ((sectorOffset + bytesToWrite - 1) / prog_size)/2;
		lastWriteOffset = sectorOffset + bytesToWrite - 1;

		for (i = firstWord; i <= lastWord; i++)
		{
			sanityCounter = 0;
			do
			{
				if ( flash_id_table[flash_id].erase_cmd )
				{
					/* Determine if the first and last words are partial word writes */
					if (i == firstWord)
					{
						partialWriteStart = sectorOffset % (prog_size * 2);

						if (partialWriteStart == 0)
						{
							if (bytesToWrite == 1)
							{
								/* the first byte of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0x00FFFFFF;
								writeData |= (WORD32)(*sectorData) << 24;
								sectorData++;
							}
							else if (bytesToWrite == 2)
							{
								/* the first two bytes of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0x0000FFFF;
								writeData |= (WORD32)(*sectorData) << 24;
								sectorData++;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;
								//writeData |= (WORD32)(*(WORD16*)sectorData) << 16;
								//sectorData += 2;
							}
							else if (bytesToWrite == 3)
							{
								/* the first three bytes of a 32 bit word is written */

								/* process first 2 bytes of word */
								writeData = LReadWord(i);
								writeData &= 0x0000FFFF;
								writeData |= (WORD32)(*sectorData) << 24;
								sectorData++;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;

								//writeData |= (WORD32)(*(WORD16*)sectorData) << 16;
								//sectorData += 2;

								/* process third byte of word */
								writeData &= 0xFFFF00FF;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
							}
							else if (bytesToWrite >= 4)
							{
								/* the four bytes of the 32 bit word are written */
								writeData = (WORD32)(*sectorData) << 24;
								sectorData++;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
								writeData |= (WORD32)(*sectorData);
								sectorData++;
								//writeData = *(WORD32*)sectorData;
								//sectorData += 4;
							}
						}

						else if (partialWriteStart == 1)
						{
							if (bytesToWrite == 1)
							{
								/* the second byte of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0xFF00FFFF;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;
							}
							else if (bytesToWrite == 2)
							{
								/* the second and third bytes of a 32 bit word is written */

								/* process second byte */
								writeData = LReadWord(i);
								writeData &= 0xFF00FFFF;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;

								/* process third byte */
								writeData &= 0xFFFF00FF;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
							}
							else if (bytesToWrite >= 3)
							{
								/* the second, third and fourth bytes of a 32 bit word is written */
								/* process second byte */
								writeData = LReadWord(i);
								writeData &= 0xFF00FFFF;
								writeData |= (WORD32)(*sectorData) << 16;
								sectorData++;

								/* process third and fourth byte */
								writeData &= 0xFFFF0000;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
								writeData |= (WORD32)(*sectorData);
								sectorData++;
								//writeData |= (WORD32)(*(WORD16*)sectorData);
								//sectorData += 2;
							}
						}

						else if (partialWriteStart == 2)
						{
							if (bytesToWrite == 1)
							{
								/* the third byte of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0xFFFF00FF;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
							}
							else if (bytesToWrite >= 2)
							{
								/* the last two bytes of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0xFFFF0000;
								writeData |= (WORD32)(*sectorData) << 8;
								sectorData++;
								writeData |= (WORD32)(*sectorData);
								sectorData++;
								//writeData |= (WORD32)(*(WORD16*)sectorData);
								//sectorData += 2;
							}
						}

						else if (partialWriteStart == 3)
						{
							if (bytesToWrite >= 1)
							{
								/* the fourth byte of a 32 bit word is written */
								writeData = LReadWord(i);
								writeData &= 0xFFFFFF00;
								writeData |= (WORD32)(*sectorData);
								sectorData++;
							}
						}
					}

					else if (i == lastWord)
					{
						partialWriteEnd = lastWriteOffset % (prog_size * 2);

						if(partialWriteEnd == 0)
						{
							/* the first byte of the 32 bit word are written */
							/* process first byte of word */
							writeData = LReadWord(i);
							writeData &= 0x00FFFFFF;
							writeData |= (WORD32)(*sectorData) << 24;
						}

						else if (partialWriteEnd == 1)
						{
							/* the first two bytes of a 32 bit word is written */
							writeData = LReadWord(i);
							writeData &= 0x0000FFFF;
							writeData |= (WORD32)(*sectorData) << 24;
							sectorData++;
							writeData |= (WORD32)(*sectorData) << 16;
							sectorData++;
							//writeData |= (WORD32)(*(WORD16*)sectorData) << 16;
						}

						else if (partialWriteEnd == 2)
						{
							/* the first three bytes of a 32 bit word is written */
							/* process first 2 bytes of word */
							writeData = LReadWord(i);
							writeData &= 0x0000FFFF;
							writeData |= (WORD32)(*sectorData) << 24;
							sectorData++;
							writeData |= (WORD32)(*sectorData) << 16;
							sectorData++;
							//writeData |= (WORD32)(*(WORD16*)sectorData) << 16;
							//sectorData += 2;

							/* process third byte of word */
							writeData &= 0xFFFF00FF;
							writeData |= (WORD32)(*sectorData) << 8;
						}

						else if (partialWriteEnd == 3)
						{
							/* the four bytes of a 32 bit word are written */
							writeData = (WORD32)(*sectorData) << 24;
							sectorData++;
							writeData |= (WORD32)(*sectorData) << 16;
							sectorData++;
							writeData |= (WORD32)(*sectorData) << 8;
							sectorData++;
							writeData |= (WORD32)(*sectorData);
							sectorData++;
							//writeData = *(WORD32*)sectorData;
						}
					}
					else
					{
						/* four bytes of the 32 bit word are written */
						writeData = (WORD32)(*sectorData) << 24;
						sectorData++;
						writeData |= (WORD32)(*sectorData) << 16;
						sectorData++;
						writeData |= (WORD32)(*sectorData) << 8;
						sectorData++;
						writeData |= (WORD32)(*sectorData);
						sectorData++;
						//writeData = *(WORD32*)sectorData;
						//sectorData += 4;
					}

					/* Check that we can do a write. If writing this flashWordT
					 * to this address will change a 0 into a 1, then we can't
					 * write (which is why Erase changes everything into FF)
					 */

					startData = LReadWord(i);
					if ((startData | writeData) != startData)
					{
						TurnFlashWriteOff();
						return (1);
					}
				}

				/* Write the write command */
				exec_flash_cmd( flash_id_table[flash_id].write_cmd,
					flash_id_table[flash_id].write_len );

				/* Load the data into flash memory */
				for (j = 0; j < prog_size / 2; j++)
				{
					data_index = i * prog_size / 2 + j;
					LWriteWord(data_index, writeData);

				}

				/* Wait until poll complete, and reset ourselves */

				LPollWord(data_index, 1UL);
				sanityCounter++;
			}
			while ( (LReadWord(data_index) != writeData) &&
				(sanityCounter < MAX_WRITE_RETRIES) );

			if (sanityCounter == MAX_WRITE_RETRIES)
			{
				TurnFlashWriteOff();
				return (2);
			}
		}

	}

	TurnFlashWriteOff();
	return (SUCCESS);
}
/*
 * static int LVerifyPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToRead, char *sectorData)
 *
 * Description:
 *
 * A pretty simple block compare, using the low level command
 * to get data from the ROM segment
 * Just get data from ROM, compare to buffer, blow out if bad,  or kick
 * buffer along until the end.
 *
 * Arguments:
 *    sectorNumber  - The sector to write
 *    sectorOffset  - The sector offset to start writing to
 *    bytesToREad   - The number of bytes to read
 *    sectorData    - The pointer to a char array
 *
 *
 * RETURNS
 *
 *    VERIFY_FAILED:   the data is not match
 *    SUCCESS          all data match
 *
 */

static
int LVerifyPartialSector(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToRead, register char *sectorData)
{
	register WORD16  i;
	register WORD32  data, buf_data;
	WORD16  prog_size;
	WORD32  firstWord, lastWord, lastReadOffset, partialReadStart, partialReadEnd;

	LSetSectorAddress(sectorNumber);
	prog_size = flash_id_table[flash_id].prog_size;

										/* 16 bit flash */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		/* find out the first word and last word to read */
		/* in 16 bit mode, a word is 2 bytes */
		firstWord = sectorOffset / prog_size;
		lastWord = (sectorOffset + bytesToRead - 1) / prog_size;
		lastReadOffset = sectorOffset + bytesToRead - 1;

		for (i = firstWord; i <= lastWord; i++)
		{
			/* Determine if the first and last words are partial word reads */
			if (i == firstWord)
			{
				partialReadStart = sectorOffset % prog_size;

				if (partialReadStart == 0)
				{
					if (bytesToRead == 1)
					{
						/* the first byte of a 16 bit word is read */
						data = LReadWord(i);
						data &= 0x0000FF00;
						data >>= 8;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead >= 2)
					{
						/* both bytes of the 16 bit word are read */
						data = LReadWord(i);
						buf_data = (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if ((WORD16)data != (WORD16)buf_data)
							return (NAFLASH_VERIFY_FAILED);
						//if ((WORD16)data != (*(WORD16*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 2;
					}
				}

				if (partialReadStart == 1)
				{
					/* second byte of a 16 bit word is read */
					data = LReadWord(i);
					data &= 0x000000FF;
					if ((char)data != (*sectorData))
						return (NAFLASH_VERIFY_FAILED);

					sectorData++;
				}
			}
			else if (i == lastWord)
			{
				partialReadEnd = lastReadOffset % prog_size;

				if (partialReadEnd == 0)
				{
					/* the first byte of a 16 bit word is read */
					data = LReadWord(i);
					data &= 0x0000FF00;
					data >>= 8;

					if ((char)data != (*sectorData))
						return (NAFLASH_VERIFY_FAILED);
				}
				else
				{
					/* both bytes of the 16 bit word are read */
					data = LReadWord(i);
					buf_data = (WORD32)(*sectorData) << 8;
					sectorData++;
					buf_data |= (WORD32)(*sectorData);
					sectorData++;
					if ((WORD16)data != (WORD16)buf_data)
						return (NAFLASH_VERIFY_FAILED);
					//if ((WORD16)data != (*(WORD16*)sectorData))
					//    return (NAFLASH_VERIFY_FAILED);
				}
			}
			else
			{
				/* both bytes of the 16 bit word are read */
				data = LReadWord(i);
				buf_data = (WORD32)(*sectorData) << 8;
				sectorData++;
				buf_data |= (WORD32)(*sectorData);
				sectorData++;
				if ((WORD16)data != (WORD16)buf_data)
					return (NAFLASH_VERIFY_FAILED);
				//if ((WORD16)data != (*(WORD16*)sectorData))
				//    return (NAFLASH_VERIFY_FAILED);

				//sectorData += 2;
			}
		}
	}

	else								/* 32 bit flash */
	{
		/* in 32 bit mode, a word is 4 bytes */
		firstWord = (sectorOffset / prog_size)/2;
		lastWord = ((sectorOffset + bytesToRead - 1) / prog_size)/2;
		lastReadOffset = sectorOffset + bytesToRead - 1;

		for (i = firstWord; i <= lastWord; i++)
		{
			/* Determine if the first and last words are partial word reads */
			if (i == firstWord)
			{
				partialReadStart = sectorOffset % (prog_size * 2);

				if (partialReadStart == 0)
				{
					if (bytesToRead == 1)
					{
						/* the first byte of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0xFF000000;
						data >>= 24;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead == 2)
					{
						/* the first two bytes of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0xFFFF0000;
						data >>= 16;
						buf_data = (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if ((WORD16)data != (WORD16)buf_data)
							return (NAFLASH_VERIFY_FAILED);
						//if ((WORD16)data != (*(WORD16*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 2;
					}
					else if (bytesToRead == 3)
					{
						/* the first three bytes of a 32 bit word is read */

						/* process first 2 bytes of word */
						data = LReadWord(i);
						data &= 0xFFFF0000;
						data >>= 16;
						buf_data = (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if ((WORD16)data != (WORD16)buf_data)
							return (NAFLASH_VERIFY_FAILED);

						//if ((WORD16)data != (*(WORD16*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 2;

						/* process third byte of word */
						data = LReadWord(i);
						data &= 0x0000FF00;
						data >>= 8;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead >= 4)
					{
						/* the four bytes of the 32 bit word are read */
						data = LReadWord(i);
						buf_data = (WORD32)(*sectorData) << 24;
						sectorData++;
						buf_data |= (WORD32)(*sectorData) << 16;
						sectorData++;
						buf_data |= (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if (data != buf_data)
							return (NAFLASH_VERIFY_FAILED);
						//if (data != (*(WORD32*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 4;
					}
				}

				else if (partialReadStart == 1)
				{
					if (bytesToRead == 1)
					{
						/* the second byte of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0x00FF0000;
						data >>= 16;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead == 2)
					{
						/* the second and third bytes of a 32 bit word is read */
						/* process second byte */
						data = LReadWord(i);
						data &= 0x00FF0000;
						data >>= 16;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;

						/* process third byte */
						data = LReadWord(i);
						data &= 0x0000FF00;
						data >>= 8;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead >= 3)
					{
						/* the second, third and fourth bytes of a 32 bit word is read */
						/* process second byte */
						data = LReadWord(i);
						data &= 0x00FF0000;
						data >>= 16;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;

						/* process third and fourth byte */
						data = LReadWord(i);
						data &= 0x0000FFFF;
						buf_data = (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if ((WORD16)data != (WORD16)buf_data)
							return (NAFLASH_VERIFY_FAILED);
						//if ((WORD16)data != (*(WORD16*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 2;
					}
				}

				else if (partialReadStart == 2)
				{
					if (bytesToRead == 1)
					{
						/* the third byte of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0x0000FF00;
						data >>= 8;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
					else if (bytesToRead >= 2)
					{
						/* the last two bytes of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0x0000FFFF;
						buf_data = (WORD32)(*sectorData) << 8;
						sectorData++;
						buf_data |= (WORD32)(*sectorData);
						sectorData++;
						if ((WORD16)data != (WORD16)buf_data)
							return (NAFLASH_VERIFY_FAILED);

						//if ((WORD16)data != (*(WORD16*)sectorData))
						//    return (NAFLASH_VERIFY_FAILED);

						//sectorData += 2;
					}
				}

				else if (partialReadStart == 3)
				{
					if (bytesToRead >= 1)
					{
						/* the fourth byte of a 32 bit word is read */
						data = LReadWord(i);
						data &= 0x000000FF;
						if ((char)data != (*sectorData))
							return (NAFLASH_VERIFY_FAILED);

						sectorData++;
					}
				}
			}

			else if (i == lastWord)
			{
				partialReadEnd = lastReadOffset % (prog_size * 2);

				if(partialReadEnd == 0)
				{
					/* the first byte of the 32 bit word are read */
					/* process first byte of word */
					data = LReadWord(i);
					data &= 0xFF000000;
					data >>= 24;
					if ((char)data != (*sectorData))
						return (NAFLASH_VERIFY_FAILED);
				}

				else if (partialReadEnd == 1)
				{
					/* the first two bytes of a 32 bit word is read */
					data = LReadWord(i);
					data &= 0xFFFF0000;
					data >>= 16;
					buf_data = (WORD32)(*sectorData) << 8;
					sectorData++;
					buf_data |= (WORD32)(*sectorData);
					sectorData++;
					if ((WORD16)data != (WORD16)buf_data)
						return (NAFLASH_VERIFY_FAILED);

					//if ((WORD16)data != (*(WORD16*)sectorData))
					//    return (NAFLASH_VERIFY_FAILED);
				}

				else if (partialReadEnd == 2)
				{
					/* the first three bytes of a 32 bit word is read */
					/* process first 2 bytes of word */
					data = LReadWord(i);
					data &= 0xFFFF0000;
					data >>= 16;
					buf_data = (WORD32)(*sectorData) << 8;
					sectorData++;
					buf_data |= (WORD32)(*sectorData);
					sectorData++;
					if ((WORD16)data != (WORD16)buf_data)
						return (NAFLASH_VERIFY_FAILED);
					//if ((WORD16)data != (*(WORD16*)sectorData))
					//    return (NAFLASH_VERIFY_FAILED);

					//sectorData += 2;

					/* process third byte of word */
					data = LReadWord(i);
					data &= 0x0000FF00;
					data >>= 8;
					if ((char)data != (*sectorData))
						return (NAFLASH_VERIFY_FAILED);
				}

				else if (partialReadEnd == 3)
				{
					/* the four bytes of a 32 bit word are written */
					data = LReadWord(i);
					buf_data = (WORD32)(*sectorData) << 24;
					sectorData++;
					buf_data |= (WORD32)(*sectorData) << 16;
					sectorData++;
					buf_data |= (WORD32)(*sectorData) << 8;
					sectorData++;
					buf_data |= (WORD32)(*sectorData);
					sectorData++;
					if (data != buf_data)
						return (NAFLASH_VERIFY_FAILED);
					//if (data != (*(WORD32*)sectorData))
					//    return (NAFLASH_VERIFY_FAILED);
				}
			}
			else
			{
				/* four bytes of the 32 bit word are written */
				data = LReadWord(i);
				buf_data = (WORD32)(*sectorData) << 24;
				sectorData++;
				buf_data |= (WORD32)(*sectorData) << 16;
				sectorData++;
				buf_data |= (WORD32)(*sectorData) << 8;
				sectorData++;
				buf_data |= (WORD32)(*sectorData);
				sectorData++;
				if (data != buf_data)
					return (NAFLASH_VERIFY_FAILED);

				//if (data != (*(WORD32*)sectorData))
				//    return (NAFLASH_VERIFY_FAILED);

				//sectorData += 4;
			}
		}
	}

	return (SUCCESS);
}
/*
 * NAFlashBase
 *
 * Description:
 *
 * This routine is used to retrieve memory address of the start of
 * flash ROM. The address is returned as a pointer to an unsigned short
 * int.
 *
 * Parameters:
 *      none
 *
 * RETURNS
 *
 *    Memory address of the start of flash rom.
 *
 */
unsigned short *
NAFlashBase ()
{
	return (unsigned short *)((*NCC_MEM).cs0.csar.reg & 0xfffff000);
}
/*
 * NAFlashEnable
 *
 * Description:
 *
 *      This routine is used to enable flash ROM so that it can be
 *      read.
 *
 * Parameters:
 *      none
 *
 *  Return Values:
 *      none
 */

void
NAFlashEnable ()
{

	WORD32 temp, *p;

	p = (WORD32 *) 0xffc00010;
	temp = *p;
	*p = temp | 1;
}
/*
 * NAFlashInit
 *
 * Description:
 *
 * This routine initializes the flash driver and sets the number of flash memory banks.
 * This routine is called by the startup code once after power up.
 *
 * Parameters:
 *    flashBanks - The number of flash memory banks.
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                  - The flash driver was initialized successfully.
 *    NAFLASH_UNKNOWN_FLASH            - The flash part was not recognized.
 *    NAFLASH_FLASHBANKS_INVALID       - The specified number of flash memory banks is invalid.
 *    NAFLASH_DUPLICATE_CALL           - The flash driver was initialized previously.
 *
 */
int
NAFlashInit(WORD32 flashBanks )
{
	/* check for duplicate initialization call */
	if (flash_banks > 0)
		return NAFLASH_DUPLICATE_CALL;
	else
	{
		/* check for valid flash banks */
		if ((flashBanks < 1) || (flashBanks > MAX_FLASH_BANKS))
			return NAFLASH_FLASHBANKS_INVALID;

		NAFlashEnable();				/* enable flash memory for reading */
		flash_banks = flashBanks;		/* set number of flash banks */
		return NAFLASH_SUCCESS;
	}
}
/*
 * NAFlashCreateSemaphores()
 *
 * Description:
 *
 * This routine creates the semaphores used to synchronize access to the flash driver API.
 * This routine is called by the application root thread once after power up.
 *
 * Parameters:
 *    none
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                  - The flash driver semaphores were created.
 *    NAFLASH_DUPLICATE_CALL           - The flash driver semaphores were previously created.
 *    NAFLASH_SEMAPHORE_CREATE_FAILED  - The flash driver semaphores could not be created.
 *
 */
int
NAFlashCreateSemaphores()
{
	unsigned int status;

	/* create semaphores */
	if (!semaphore_created)
	{
		status = tx_semaphore_create(&flash_semaphore1, "flash_sem1", 1);
		if (status != TX_SUCCESS)
			return NAFLASH_SEMAPHORE_CREATE_FAILED;
		semaphore_created = 1;
		return NAFLASH_SUCCESS;
	}
	else
		return NAFLASH_DUPLICATE_CALL;
}
/*
 * NAFlashSectors
 *
 * Description:
 *
 * This routine returns the number of physical sectors for the flash memory part.
 * For both 16 and 32 bit configurations, the routine returns the number of
 * physical sectors for the flash memory part.
 *
 * Parameters:
 *    none
 *
 * RETURNS
 *
 *    0 < (greater than 0)       - The number of sectors in the flash part.
 *    0 > (less than 0)
 *    NAFLASH_UNKNOWN_FLASH      - The flash part was not recognized.
 *
 */
int
NAFlashSectors()
{
	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}
	return flash_id_table[flash_id].total_sector_number * ((int)flash_banks);
}
/*
 * NAFlashSectorSizes
 *
 * Description:
 *
 * This routine retrieves the physical sector sizes for the flash memory configuration.
 * This routine should be called after NAFlashSectors(), so a sufficiently large array
 * is passed to the function to store the sector size data.
 *
 * Parameters:
 *    sectorSizeArray - pointer to an unsigned int array to store the sector sizes for
 *                      the flash configuration
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS            - The flash sector sizes were retrieved successfully.
 *    NAFLASH_UNKNOWN_FLASH      - The flash part was not recognized.
 *
 */
int
NAFlashSectorSizes (WORD32 * sectorSizeArray)
{
	int j, k;
	WORD32 totalsectors;

	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	for (j = 0; j < totalsectors; j++)
	{
		k = j % flash_id_table[flash_id].total_sector_number;
		sectorSizeArray[j] = LGetSectorSize(k);
	}

	return NAFLASH_SUCCESS;
}
/*
 * NAFlashSectorOffsets
 *
 * Description:
 *
 * This routine retrieves the flash "sector offsets" from the flash "base address".
 * This routine should be called after NAFlashSectors(), so a sufficiently large array
 * is passed to the function to store the sector offset data.
 *
 * Parameters:
 *    sectorOffsetArray - pointer to an unsigned int array to store the sector offsets for
 *                      the flash configuration
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS            - The flash sector offsets were retrieved successfully.
 *    NAFLASH_UNKNOWN_FLASH      - The flash part was not recognized.
 *
 */
int
NAFlashSectorOffsets (WORD32 * sectorOffsetArray)
{
	int j;
	WORD32 totalsectors, sectoroffset;

	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	for (j = 0; j < totalsectors; j++)
	{
		if (!j)
		{
			sectoroffset = 0;
		}
		else
		{
			sectoroffset += LGetSectorSize(j-1);
		}
		sectorOffsetArray[j] = sectoroffset;
	}

	return NAFLASH_SUCCESS;
}
/*
 * NAFlashErase
 *
 * Description:
 *
 * This routine erases a specified range of flash sectors.
 *
 * Parameters:
 *    firstSectorNumber - the first sector number to erase
 *    lastSectorNumber  - the last sector number to erase
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                - The specified sectors were erased.
 *    NAFLASH_ERASE_FAILED           - The sector erase operation failed.
 *    NAFLASH_SECTORNUMBER_INVALID   - One or more sector numbers are invalid.
 */
int
NAFlashErase(WORD32 firstSectorNumber, WORD32 lastSectorNumber)
{
	WORD32 totalsectors;
	int j, rc;

	tx_semaphore_get(&flash_semaphore1, TX_WAIT_FOREVER);
	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			tx_semaphore_put(&flash_semaphore1);
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	if ( (firstSectorNumber > (totalsectors - 1)) ||
		(lastSectorNumber > (totalsectors - 1)) ||
		(firstSectorNumber > lastSectorNumber) )
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTORNUMBER_INVALID;
	}

	for (j = firstSectorNumber; j <= lastSectorNumber; j++)
	{
		rc = LEraseSector(j);
		if (rc)
		{
			tx_semaphore_put(&flash_semaphore1);
			return rc;
		}
	}
	tx_semaphore_put(&flash_semaphore1);
	return NAFLASH_SUCCESS;
}
/*
 * NAFlashEraseStatus
 *
 * Description:
 *
 * This routine checks the erase status of the specified flash sectors.  A flash
 * sector is either erased (all 0xFFs) or not erased (not all 0xFFs).  The status is
 * retrieved through the "status" argument.
 *
 * Parameters:
 *    firstSectorNumber - the first sector number to check erase status
 *    lastSectorNumber  - the last sector number to check erase status
 *    status            - pointer to an array to store the status of the sectors
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                - The sector erase status check was completed.
 *    NAFLASH_UNKNOWN_FLASH          - The flash part was not recognized.
 *    NAFLASH_SECTORNUMBER_INVALID   - sector number is invalid
 */
int
NAFlashEraseStatus (WORD32 firstSectorNumber, WORD32 lastSectorNumber, char *status)
{
	WORD32 totalsectors, j;

	tx_semaphore_get(&flash_semaphore1, TX_WAIT_FOREVER);
	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			tx_semaphore_put(&flash_semaphore1);
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	if ( (firstSectorNumber > (totalsectors - 1)) ||
		(lastSectorNumber > (totalsectors - 1)) ||
		(firstSectorNumber > lastSectorNumber) )
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTORNUMBER_INVALID;
	}

	for (j = firstSectorNumber; j <= lastSectorNumber; j++)
	{
		status[j - firstSectorNumber] = (char)LVerifyAllFs(j);
	}
	tx_semaphore_put(&flash_semaphore1);
	return NAFLASH_SUCCESS;
}
/*
 * NAFlashRead
 *
 * Description:
 *
 * This routine reads data from a range specified sectors and stores it in a buffer..
 *
 * Parameters:
 *    sectorNumber - the flash sector to read from
 *    sectorOffset - the starting offset of the flash sector to read from
 *    bytesToRead  - the number of bytes to read
 *    buffer       - buffer to store the flash memory data
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                - The flash read operation was completed.
 *    NAFLASH_UNKNOWN_FLASH          - The flash part was not recognized.
 *    NAFLASH_SECTORNUMBER_INVALID   - The sector number is invalid.
 *    NAFLASH_SECTOROFFSET_INVALID   - The sector offset is invalid.
 *    NAFLASH_MEMORY_OUTOFBOUND      - The read operation exceeds flash memory boundaries.
 *
 */
int
NAFlashRead(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToRead, char * buffer)
{
	WORD32 totalsectors, sectorsize;
	int j;

	tx_semaphore_get(&flash_semaphore1, TX_WAIT_FOREVER);
	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			tx_semaphore_put(&flash_semaphore1);
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	if (sectorNumber > (totalsectors - 1))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTORNUMBER_INVALID;
	}

	sectorsize = LGetSectorSize(sectorNumber);

	if (sectorOffset > (sectorsize - 1))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTOROFFSET_INVALID;
	}

	if (!bytesToRead)
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SUCCESS;
	}

	/* Find out flash size */
	if (!total_flash_memory)
	{
		for (j = 0; j < totalsectors; j++)
			total_flash_memory += LGetSectorSize(j);
	}

	/* Setup addressing */
	LSetSectorAddress(sectorNumber);

	/* read out the data into the buffer*/
										/* 16 bit mode */
	if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
	{
		/* check if read goes beyond flash memory boundary */
		if ((NAFlashBase() + total_flash_memory) < (mSectorBase + sectorOffset + bytesToRead - 1))
		{
			tx_semaphore_put(&flash_semaphore1);
			return NAFLASH_MEMORY_OUTOFBOUND;
		}

		memcpy(buffer, ((char *)mSectorBase) + sectorOffset, bytesToRead);
	}
	else								/* 32 bit mode */
	{
		/* check if read goes beyond flash memory boundary */
		if (((WORD32*)NAFlashBase() + total_flash_memory) < (mSectorBase32 + sectorOffset + bytesToRead - 1))
		{
			tx_semaphore_put(&flash_semaphore1);
			return NAFLASH_MEMORY_OUTOFBOUND;
		}

		memcpy(buffer, ((char *)mSectorBase32) + sectorOffset, bytesToRead);
	}
	tx_semaphore_put(&flash_semaphore1);
	return NAFLASH_SUCCESS;
}
/*
 * NAFlashWriteSector
 *
 * Description:
 *
 * This routine programs a specified sector with data from a buffer.
 *
 * Parameters:
 *    sectorNumber - the flash sector to write
 *    buffer       - buffer with the new flash sector data
 *
 * RETURNS
 *
 *    SUCCESS
 *    SECTORNUMBER_INVALID   sector number is invalid
 *    WRITE_FAILED           sector write failed
 *    VERIFY_FAILED          sector verify failed
 *    ERASE_FAILED           sector erase failed
 */
int
NAFlashWriteSector (WORD32 sectorNumber, char * buffer)
{
	WORD32 totalsectors;
	int status;

	if (flash_id == 0xFF)
	{
		return (NAFLASH_UNKNOWN_FLASH);
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	if (sectorNumber > (totalsectors - 1))
		return NAFLASH_SECTORNUMBER_INVALID;

	/* verify flash, if sector data is the same as the incoming data, then don't program it, save time */
	status = LVerifySector(sectorNumber, (WORD16 *)buffer);
	if (status != SUCCESS)
	{
		/* erase flash sector */
		status = LEraseSector(sectorNumber);
		if (status != SUCCESS)
			return (NAFLASH_ERASE_FAILED);

		/* program flash sector */
		status = LProgramSector(sectorNumber, (WORD16 *)buffer);
		if (status != SUCCESS)
			return (NAFLASH_WRITE_FAILED);

		/* verify flash sector */
		status = LVerifySector(sectorNumber, (WORD16 *)buffer);
		if (status != SUCCESS)
			return (NAFLASH_VERIFY_FAILED);
	}

	return status;
}
/*
 * NAFlashWrite
 *
 * Description:
 *
 * This routine writes data starting from a specified sector number and offset location.
 *
 * Parameters:
 *
 * sectorNumber		The "0 index based" flash sector to start writing to
 * sectorOffset		The "0 index based" starting offset of the flash sector to write to.
 * bytesToWrite		The number of bytes to write.
 * buffer			The buffer with the new data for the flash.
 * options			One of the following advanced write options:
 *                  ERASE_AS_NEEDED (or NULL)
 *                  ALWAYS_ERASE
 *                  DO_NOT_ERASE
 *
 * RETURNS
 *
 *    NAFLASH_SUCCESS                - The flash memory write operation was completed.
 *    NAFLASH_UNKNOWN_FLASH          - The flash part was not recognized.
 *    NAFLASH_SECTORNUMBER_INVALID   - The sector number is invalid.
 *    NAFLASH_WRITE_FAILED           - The write sector operation failed.
 *    NAFLASH_VERIFY_FAILED          - The verify sector operation failed.
 *    NAFLASH_ERASE_FAILED           - The erase sector operation failed.
 *    NAFLASH_SECTOROFFSET_INVALID	 - The sector offset is invalid.
 *    NAFLASH_MEMORY_OUTOFBOUND	     - The write operation exceeds flash memory boundaries.
 *    NAFLASH_WRITEOPTION_INVALID    - The write option specified is not valid.
 *
 */

#ifdef FOXJET
#else if
int
NAFlashWrite(WORD32 sectorNumber, WORD32 sectorOffset, WORD32 bytesToWrite, char * buffer, char options)
{
	WORD32 totalsectors, current_sector_base, current_sector_wbytes;
	WORD32 lastsector, first_write_offset, last_write_offset, current_sector_size, current_sector_woffset;
	static WORD32 sectorOffsetTable[MAX_SECTORS];
	static int sectorOffsetTableValid = 0;
	int status, j;

	tx_semaphore_get(&flash_semaphore1, TX_WAIT_FOREVER);
	/*
	 * Identify the flash part.  When identify_flash() returns 0xFF, it means
	 * that the flash is not turned on or your flash is not in the flash table.
	 * If your flash is a 3V AMD compatible flash, you can default flash_id to
	 * be 0x06, and do NOT return (NAFLASH_UNKNOWN_FLASH).
	 */
	if (flash_id == 0xFF)
	{
		flash_id = identify_flash();
		if (flash_id == 0xFF)
		{
			/* flash_id = 0x6; */
			tx_semaphore_put(&flash_semaphore1);
			return (NAFLASH_UNKNOWN_FLASH);
		}
	}

	if (((int)options < ERASE_AS_NEEDED) || (options > DO_NOT_ERASE))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_WRITEOPTION_INVALID;
	}

	totalsectors = flash_id_table[flash_id].total_sector_number * flash_banks;

	if (sectorNumber > (totalsectors - 1))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTORNUMBER_INVALID;
	}

	if (sectorOffset > (LGetSectorSize(sectorNumber) - 1))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SECTOROFFSET_INVALID;
	}

	if (!bytesToWrite)
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_SUCCESS;
	}

	/* Find out Flash size */
	if (!total_flash_memory)
	{
		for (j = 0; j < totalsectors; j++)
			total_flash_memory += LGetSectorSize(j);
	}

	/* Compute the first write offset from the base address of Flash */
	if (!sectorNumber)
	{
		first_write_offset = sectorOffset;
		current_sector_base = 0;
	}
	else
	{
		first_write_offset = 0;
		current_sector_base = 0;
		/* if sector table offset not valid, fill in table and set flag to 1 */
		if (!sectorOffsetTableValid)
		{
			NAFlashSectorOffsets(sectorOffsetTable);
			sectorOffsetTableValid = 1;
		}
		current_sector_base = sectorOffsetTable[sectorNumber];
		first_write_offset = current_sector_base + sectorOffset;
	}

	/* Compute the last write offset from the base address of Flash */
	last_write_offset = first_write_offset + bytesToWrite - 1;

	if (last_write_offset > (total_flash_memory - 1))
	{
		tx_semaphore_put(&flash_semaphore1);
		return NAFLASH_MEMORY_OUTOFBOUND;
	}

	/* Compute the last sector that will be programmed */
	for (j = sectorNumber; j < totalsectors; j++)
	{
		current_sector_size = LGetSectorSize(j);
		if ((current_sector_base + current_sector_size) <= last_write_offset)
		{
			current_sector_base += current_sector_size;
		}
		else
		{
			lastsector = j;
			break;
		}
	}

	current_sector_woffset = sectorOffset;

	/* program the range of sectors needed */
	for (j = sectorNumber; j <= lastsector; j++)
	{
		current_sector_size = LGetSectorSize(j);
		if (!current_sector_woffset)
		{
			if (bytesToWrite >= current_sector_size)
			{
				/* verify flash, if sector data is the same as the incoming data, then don't program it, save time */
				status = LVerifySector(j, (WORD16 *)buffer);
				if (status != SUCCESS)
				{
					if ((options == ERASE_AS_NEEDED) || (options == ALWAYS_ERASE))
					{
						/* erase flash sector */
						status = LEraseSector(j);
						if (status != SUCCESS)
						{
							tx_semaphore_put(&flash_semaphore1);
							return (NAFLASH_ERASE_FAILED);
						}
					}

					/* program flash sector */
					status = LProgramSector(j, (WORD16 *)buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify flash sector */
					status = LVerifySector(j, (WORD16 *)buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}
				bytesToWrite -= current_sector_size;
				buffer += current_sector_size;
			}
			else
			{
				if (options == ERASE_AS_NEEDED)
				{
					/* save the current sector data */
										/* Setup addressing */
					LSetSectorAddress(j);
					/* 16 bit mode */
					if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase, LGetSectorSize(j));
					}
					else				/* 32 bit */
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase32, LGetSectorSize(j));
					}

					/* erase flash sector */
					status = LEraseSector(j);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_ERASE_FAILED);
					}

					/* copy new data into sector, but keep data that was not overwritten */
					memcpy((void*)first_sector_data, (const void*) buffer, bytesToWrite);

					/* program sector */
					status = LProgramSector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify sector */
					status = LVerifySector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}
				else
				{
					if (options == ALWAYS_ERASE)
					{
						/* erase flash sector */
						status = LEraseSector(j);
						if (status != SUCCESS)
						{
							tx_semaphore_put(&flash_semaphore1);
							return (NAFLASH_ERASE_FAILED);
						}
					}

					/* program partial sector */
					status = LProgramPartialSector(j, 0, bytesToWrite, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify partial sector */
					status = LVerifyPartialSector(j, 0, bytesToWrite, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}
			}
		}
		else							/* current_sector_woffset is NOT 0 */
		{
			if ((current_sector_woffset + bytesToWrite - 1) >= (current_sector_size - 1))
			{
				/* compute bytes to write for current sector */
				current_sector_wbytes = current_sector_size - current_sector_woffset;

				if (options == ERASE_AS_NEEDED)
				{
					/* save the current sector data */
										/* Setup addressing */
					LSetSectorAddress(j);
					/* 16 bit mode */
					if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase, LGetSectorSize(j));
					}
					else				/* 32 bit */
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase32, LGetSectorSize(j));
					}

					/* erase flash sector */
					status = LEraseSector(j);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_ERASE_FAILED);
					}

					/* copy new data into sector, but keep data that was not overwritten */
					memcpy((void*)(first_sector_data + current_sector_woffset), (const void*) buffer, current_sector_wbytes);

					/* program sector */
					status = LProgramSector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify sector */
					status = LVerifySector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}
				else
				{
					if (options == ALWAYS_ERASE)
					{
						/* erase flash sector */
						status = LEraseSector(j);
						if (status != SUCCESS)
						{
							tx_semaphore_put(&flash_semaphore1);
							return (NAFLASH_ERASE_FAILED);
						}
					}

					/* program partial sector */
					status = LProgramPartialSector(j, current_sector_woffset, current_sector_wbytes, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify partial sector */
					status = LVerifyPartialSector(j, current_sector_woffset, current_sector_wbytes, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}

				bytesToWrite -= current_sector_wbytes;
				buffer += current_sector_wbytes;
				current_sector_woffset = 0;
			}
			else
			{
				if (options == ERASE_AS_NEEDED)
				{
					/* save the current sector data */
										/* Setup addressing */
					LSetSectorAddress(j);
					/* 16 bit mode */
					if (((*NCC_MEM).cs0.csor.reg & 0x0000000C) == 0x4)
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase, LGetSectorSize(j));
					}
					else				/* 32 bit */
					{
						memcpy((void*)first_sector_data, (const void*)mSectorBase32, LGetSectorSize(j));
					}

					/* erase flash sector */
					status = LEraseSector(j);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_ERASE_FAILED);
					}

					/* copy new data into sector, but keep data that was not overwritten */
					memcpy((void*)(first_sector_data + current_sector_woffset), (const void*) buffer, bytesToWrite);

					/* program sector */
					status = LProgramSector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify sector */
					status = LVerifySector(j, (WORD16*)first_sector_data);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
					tx_semaphore_put(&flash_semaphore1);
					return NAFLASH_SUCCESS;
				}
				else
				{
					if (options == ALWAYS_ERASE)
					{
						/* erase flash sector */
						status = LEraseSector(j);
						if (status != SUCCESS)
						{
							tx_semaphore_put(&flash_semaphore1);
							return (NAFLASH_ERASE_FAILED);
						}
					}

					/* program partial sector */
					status = LProgramPartialSector(j, current_sector_woffset, bytesToWrite, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_WRITE_FAILED);
					}

					/* verify partial sector */
					status = LVerifyPartialSector(j, current_sector_woffset, bytesToWrite, buffer);
					if (status != SUCCESS)
					{
						tx_semaphore_put(&flash_semaphore1);
						return (NAFLASH_VERIFY_FAILED);
					}
				}
			}
		}
	}
	tx_semaphore_put(&flash_semaphore1);
	return (NAFLASH_SUCCESS);
}
#endif
