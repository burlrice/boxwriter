#include "fj.h"

#define MAX_BUFFER                1024

#ifdef FJ_LISTEN
void fj_threadListenStart(ULONG port_number)
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	struct sockaddr_in addrNames = { AF_INET, 0, INADDR_ANY, 0 };
	struct sockaddr_in addrTo    = { AF_INET, 0, 0, 0 };
	struct sockaddr_in addrFrom;
	int    sizesockaddr_in = sizeof( struct sockaddr_in );
	int    socketudpl;
	int    sockettcpl;
	int   optval;
	int   optsize = sizeof(int);
	CHAR *buffer;
	CHAR  str[180];
	LONG  i;
	LONG  retry;
	LONG  bytesread;
	LONG  ret;

	addrTo.sin_port = port_number;
	addrNames.sin_port = port_number;
										// allocate buffer
	buffer = (CHAR*)fj_memory_getOnce(MAX_BUFFER);

	addrTo.sin_addr.s_addr = pGlobal->ipbroadcastnet;

	sockettcpl = socket( AF_INET, SOCK_STREAM, 0 );
	socketudpl = socket( AF_INET, SOCK_DGRAM, 0 );
	while ( (socketudpl > 0) || (sockettcpl > 0) )
	{
		if ( socketudpl > 0 )
		{
			optval = 1;					// set value to turn options on
			ret = setsockopt( socketudpl, SOL_SOCKET, SO_BROADCAST, (char*)&optval, 4 );
			ret = setsockopt( socketudpl, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, 4 );
			ret = setsockopt( socketudpl, SOL_SOCKET, SO_NBIO, (char*)&optval, 4 );
			optval = 0;					// set value to turn options off
			ret = setsockopt( socketudpl, SOL_SOCKET, SO_DONTROUTE, (char*)&optval, 4 );
			// bind this socket to the port
			ret = bind( socketudpl, &addrNames, sizeof(addrNames));
			if ( 0 == ret )
			{
				bytesread = 0;
				for ( retry = 0; (retry < 4) && (0 == bytesread) ; retry++ )
				{
					strcpy( buffer, "Broadcast" );
					// send a short packet
					ret = sendto( socketudpl, buffer, strlen(buffer)+1, 0, &addrTo, sizesockaddr_in );
					tx_thread_sleep( 2*BSP_TICKS_PER_SECOND );
					bytesread = recvfrom( socketudpl, buffer, MAX_BUFFER, 0, &addrFrom, &sizesockaddr_in );

					if ( bytesread > 0 )
					{
						sprintf( str, "LISTEN UDP(%d) %d.%d.%d.%d %d bytes %s", (int)port_number, PUSH_IPADDR(addrFrom.sin_addr.s_addr),bytesread,buffer);
						fj_writeInfo(str);
						for ( i = 0; i < bytesread; i++ )
						{
							if( (i%16) == 0 )
							{
								sprintf( str, "\n%d:", i );
								fj_writeInfo(str);
							}
							sprintf( str, " %x", buffer[i] );
							fj_writeInfo(str);
						}
						fj_writeInfo("\n\n");
					}
				}
			}
		}
		if ( sockettcpl > 0 )
		{
			optval = 1;					// set value to turn options on
			ret = setsockopt( sockettcpl, SOL_SOCKET, SO_BROADCAST, (char*)&optval, 4 );
			ret = setsockopt( sockettcpl, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, 4 );
			ret = setsockopt( sockettcpl, SOL_SOCKET, SO_NBIO, (char*)&optval, 4 );
			optval = 0;					// set value to turn options off
			ret = setsockopt( sockettcpl, SOL_SOCKET, SO_DONTROUTE, (char*)&optval, 4 );
			// bind this socket to the port
			ret = bind( sockettcpl, &addrNames, sizeof(addrNames));
			if ( 0 == ret )
			{
				bytesread = 0;
				for ( retry = 0; (retry < 4) && (0 == bytesread) ; retry++ )
				{
					//	strcpy( buffer, "Broadcast" );
					//	ret = sendto( socketudpl, buffer, strlen(buffer)+1, 0, &addrTo, sizesockaddr_in ); // send a short packet
					tx_thread_sleep( 2*BSP_TICKS_PER_SECOND );
					bytesread = recvfrom( sockettcpl, buffer, MAX_BUFFER, 0, &addrFrom, &sizesockaddr_in );

					if ( bytesread > 0 )
					{
						sprintf( str, "LISTEN TCP(%d) %d.%d.%d.%d %d bytes %s", (int)port_number, PUSH_IPADDR(addrFrom.sin_addr.s_addr),bytesread,buffer);
						fj_writeInfo(str);
						for ( i = 0; i < bytesread; i++ )
						{
							if( (i%16) == 0 )
							{
								sprintf( str, "\n%d:", i );
								fj_writeInfo(str);
							}
							sprintf( str, " %x", buffer[i] );
							fj_writeInfo(str);
						}
						fj_writeInfo("\n\n");
					}
				}
			}
		}
		tx_thread_sleep( 60*BSP_TICKS_PER_SECOND );
	}
	socketclose( socketudpl );
	socketclose( sockettcpl );

	return;
}
#endif
