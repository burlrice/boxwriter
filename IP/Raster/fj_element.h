#ifndef FJ_ELEMENT
#define FJ_ELEMENT

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjmemstor;
struct fjimage;
struct fjmessage;

enum FJELETYPE
{
	FJ_TYPE_TEXT = 0x01,
	FJ_TYPE_BARCODE,
	FJ_TYPE_BITMAP,
	FJ_TYPE_DYNIMAGE,
	FJ_TYPE_DATETIME,
	FJ_TYPE_COUNTER,
	FJ_TYPE_DYNTEXT,
	FJ_TYPE_DYNBARCODE,
	FJ_TYPE_DATAMATRIX
};

typedef struct fjelemaction
{
	LPVOID  (*fj_DescNew)( VOID );
	LPVOID  (*fj_DescDup)( LPVOID pfd );
	VOID    (*fj_DescDestroy)( struct fjelement FAR * pfe );
	VOID    (*fj_DescCreateImage)( struct fjelement FAR * const pfe );
	VOID    (*fj_DescPhotocellBuild)( struct fjelement FAR * const pfe );
	VOID    (*fj_DescBuildFromName)( const struct fjmemstor FAR * const pMemStor, struct fjelement FAR * pfe, LPCSTR pName );
	BOOL    (*fj_DescAddToMemstor)( struct fjmemstor FAR * pMemStor, const struct fjelement FAR * const pfe );
	BOOL    (*fj_DescDeleteFromMemstor)( struct fjmemstor FAR * pMemStor, const struct fjelement FAR * const pfe );
	VOID    (*fj_DescGetTextString)( const struct fjelement FAR * const pfe, LPSTR pStr, LONG lMaxCount );
	enum FJELETYPE (*fj_DescGetType)( VOID );
	LPCSTR  (*fj_DescGetTypeString)( VOID );
} FJELEMACTION, FAR *LPFJELEMACTION, FAR * const CLPFJELEMACTION;
typedef const struct fjelemaction FJCELEMACTION, FAR *LPCFJELEMACTION, FAR * const CLPCFJELEMACTION;

typedef struct fjelement
{
	LONG        lRow;					// row number to start pixels - 0 = bottom of print head
	FLOAT       fLeft;					// distance from left edge of box to segment anchor - lower right
	LONG        lPosition;				// type of positioning
	BOOL        bEdit;					// element is being edited. used for different display.
	LONG        lTransform;				// 'user' transforms to use
										// element name
	STR         strName[1+FJELEMENT_NAME_SIZE];
	LPCFJELEMACTION pActions;			// pointer to actions structure
	LPVOID      pDesc;					// pointer to individual element descriptor
	struct fjimage FAR * pfi;			// pointer to FJIMAGE structure
	struct fjmessage FAR * pfm;			// pointer to owning FJMESSAGE structure
} FJELEMENT, FAR *LPFJELEMENT, FAR * const CLPFJELEMENT;
typedef const struct fjelement FAR *LPCFJELEMENT, FAR * const CLPCFJELEMENT;

// position value
#define FJ_POSITION_DISTANCE     'D'
#define FJ_POSITION_CONCATENATE  'C'
#define FJ_POSITION_PARALLEL     'P'

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	//
	// declarations for routines
	//
	//FJDLLExport LPFJELEMENT fj_ElementNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementTextNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementBarCodeNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementDynTextNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementDynBarCodeNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementBitmapNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementCounterNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementDatetimeNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementDynimageNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementDataMatrixNew( VOID );
	FJDLLExport LPFJELEMENT fj_ElementNewFromType( LONG lType );
	FJDLLExport LPFJELEMENT fj_ElementNewFromTypeString( LPCSTR pType );
	FJDLLExport LPFJELEMENT fj_ElementDup( CLPCFJELEMENT pfe );
	FJDLLExport VOID        fj_ElementDestroy( LPFJELEMENT pfe );
	FJDLLExport LONG        fj_ElementListAdd( LPFJELEMENT **ppapfeIn, LONG lCount, LPCFJELEMENT pfe );
	FJDLLExport LONG        fj_ElementListRemove( LPFJELEMENT **ppapfeIn, LONG lCount, LPCFJELEMENT pfe );
	FJDLLExport VOID        fj_ElementListDestroyAll( LPFJELEMENT **ppapfeIn, LONG lCount );
	FJDLLExport VOID        fj_ElementReset( LPFJELEMENT pfe );
	FJDLLExport VOID        fj_ElementPhotocellBuild( LPFJELEMENT pfe );

	FJDLLExport LPCFJELEMACTION fj_ElementGetActionsFromTypeString( LPCSTR pType );
	FJDLLExport LPCFJELEMACTION fj_ElementGetActionsFromType( LONG lType );

	FJDLLExport VOID            fj_ElementMakeMemstorName( LPFJELEMENT pfe, LPSTR pName );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_ELEMENT
