#include "fj.h"
#include "fj_system.h"

#define NAME_SERVICE_UDP_PORT     137
#define INFINITE_TTL                0
#define NAME_SUFFIX_WORKSTATION  0x00

// 250 milliseconds.
#define BCAST_REQ_RETRY_TIMEOUT  (250*BSP_TICKS_PER_SECOND)/1000
#define BCAST_REQ_RETRY_COUNT       3
										// 5 seconds
#define UCAST_REQ_RETRY_TIMEOUT     5*BSP_TICKS_PER_SECOND
#define UCAST_REQ_RETRY_COUNT       3

#define NAME_SERVICE_MAX_BUFFER   576

#pragma pack(1)
// note: the instructions don't work correctly with data types that are not aligned.
//       especially 4-byte long variables that are not on a 4-byte boundary.
//       the definitions here are correct. memset() must be used for certain fields.
typedef struct nameserviceheader_struct
{
	WORD16 sTranID;
	WORD16 sFlags;
	WORD16 sQuestionCount;
	WORD16 sAnswerCount;
	WORD16 sResourceRecordCount;
	WORD16 sAddResourceRecordCount;
} NameServiceHeader, *pNameServiceHeader;
#define NSH_RESPONSE         0x8000
#define NSH_REQUEST          0x0000		// not a NSH_RESPONSE
#define NSH_OPCODE           0x7800
#define NSH_OP_QUERY        (0x0<<11)
#define NSH_OP_REGISTRATION (0x5<<11)
#define NSH_OP_RELEASE      (0x6<<11)
#define NSH_OP_WAITACK      (0x7<<11)
#define NSH_OP_REFRESH      (0x8<<11)
#define NSH_AUTHORITATIVE    0x0400
#define NSH_TRUNCATION       0x0200
#define NSH_RECURDESIRED     0x0100
#define NSH_RECURAVAIL       0x0080
#define NSH_BROADCAST        0x0010
#define NSH_RETURNCODE       0x000F
#define NSH_RETURN_SUCCESS   0x0000

typedef struct namequestionrecord_struct
{
	WORD8  length;
	WORD8  name[32];
	WORD8  null;
	WORD16 type;
	WORD16 class;
} NameQuestionRecord, *pNameQuestionRecord;

typedef struct nameanswerrecord_struct
{
	WORD8  length;
	WORD8  name[32];
	WORD8  null;
	WORD16 type;
	WORD16 class;
	WORD32 ttl;
	WORD16 rdlength;
	WORD16 rdflags;
	WORD32 address;
} NameAnswerRecord, *pNameAnswerRecord;

typedef struct nameresourcerecord_struct
{
	WORD8  length;
	WORD8  name[32];
	WORD8  null;
	WORD16 type;
	WORD16 class;
	WORD32 ttl;
	WORD16 rdlength;
	WORD16 rdflags;
	WORD32 address;
} NameResourceRecord, *pNameResourceRecord;
typedef struct nameresourcerecordp_struct
{
	WORD16 namePointer;
	WORD16 type;
	WORD16 class;
	WORD32 ttl;
	WORD16 rdlength;
	WORD16 rdflags;
	WORD32 address;
} NameResourceRecordP, *pNameResourceRecordP;
#define NRR_CLASS_INTERNET      0x0001
#define NRR_TYPE_IP         0x0000
#define NRR_TYPE_NAMESERVER     0x0002
#define NRR_TYPE_NULL           0x000A
#define NRR_TYPE_NETBIOSNAMESERVICE 0x0020
#define NRR_TYPE_NETBIOSNODESTATUS  0x0021
#define NRR_FLAG_NODETYPE_B     (0x0<<13)
#define NRR_FLAG_NODETYPE_P     (0x1<<13)
#define NRR_FLAG_NODETYPE_M     (0x2<<13)
#define NRR_FLAG_NODETYPE_H     (0x3<<13)
#define NRR_NAME_POINTER        0xC00C

typedef struct nameregistration_struct
{
	NameServiceHeader   nshead;
	NameQuestionRecord  nqr;
	NameResourceRecordP nrrp;
} NameRegistration, *pNameRegistration;

typedef struct nameregistrationresponse_struct
{
	NameServiceHeader   nshead;
	NameAnswerRecord    nar;
} NameRegistrationResponse, *pNameRegistrationResponse;

typedef struct namequeryrequest_struct
{
	NameServiceHeader  nshead;
	NameQuestionRecord nqr;
} NameQueryRequest, *pNameQueryRequest;

typedef struct namequeryresponse_struct
{
	NameServiceHeader  nshead;
	NameResourceRecord nrr;
} NameQueryResponse, *pNameQueryResponse;

typedef struct nodestatusresponse_struct
{
	NameServiceHeader  nshead;
	NameQuestionRecord nqr;
	WORD32 ttl;
	WORD16 rdlength;
	WORD8  numNames;
	WORD8  name[16];
	WORD16 nameFlags;
	WORD8  macAddress[6];
	WORD16 stats[20];
} NodeStatusResponse, *pNodeStatusResponse;
#define NSR_FLAG_NODETYPE_B     (0x0<<13)
#define NSR_FLAG_NODETYPE_P     (0x1<<13)
#define NSR_FLAG_NODETYPE_M     (0x2<<13)
#define NSR_FLAG_NODETYPE_H     (0x3<<13)
#define NSR_FLAG_DEREGISTER     0x1000
#define NSR_FLAG_CONFLICT       0x0800
#define NSR_FLAG_ACTIVE         0x0400
#define NSR_FLAG_PERMANENT      0x0200

#pragma pack()

										// constant needed for several API calls
static    int  sizeint         = sizeof( int );
// constant needed for several API calls
static    int  sizesockaddr_in = sizeof( struct sockaddr_in );
static WORD16  transaction_id = 0;		// unique ID needed for our requests
static WORD16  transaction_reg;			// unique ID for last registration request

static    ULONG ttlRegistration;		// ttl response from name registration in milliseconds
static    ULONG ttlBSP;					// ttl in tx_timer_ticks
static    ULONG ttlTimer;				// value used for timer to issue registration renewal
static     BOOL bRegistered;			// switch to indicate registration response received
static     BOOL bRegisterSend;			// switch to indicate registration required
static     BOOL bRegisterWait;			// switch to indicate waiting for registration response
static    ULONG lRegisterCount;			// registration retry count
static    ULONG lRegisterWaitTime;		// registration retry delay
static TX_TIMER tx_name_renew;			// timer for registration renewal

static int    fj_socketNETBIOS;
static WORD8 *netbiosBuffer;
static CHAR   fj_serialNetBios[17];
static CHAR   fj_groupNetBios[17];

static struct sockaddr_in addrNames   = { AF_INET, NAME_SERVICE_UDP_PORT, INADDR_ANY, 0 };
static struct sockaddr_in addrBroad   = { AF_INET, NAME_SERVICE_UDP_PORT,          0, 0 };
static struct sockaddr_in addrNETBIOS = { AF_INET, NAME_SERVICE_UDP_PORT,          0, 0 };
static struct sockaddr_in addrReg     = { AF_INET, NAME_SERVICE_UDP_PORT,          0, 0 };

// convert DNS encoded names to ASCII string.
// length is assumed to be a multiple of 2.
// a terminating 0x00 is added to the ASCII name.
// NOTE: an encoded character might decode to a 0x00.
//       thus the returned 'string' might appear shorter than expected from input length.
static VOID fj_dnsToAscii( WORD8* in, LPCHAR out, int length )
{
	while ( (*in >= 'A') && length > 0 )
	{
		*out = (((*(in+0))-'A')<<4) + (((*(in+1))-'A'));
		in += 2;
		out++;
		length -= 2;
	}
	*out = '\0';
	return;
}
// encode ASCII characters to DNS name
// 16 characters will be encoded into 32 bytes
static VOID fj_asciiToDns( LPCHAR in, WORD8 *out )
{
	int i;

	for ( i = 0; i < 16; i++ )
	{
		*out = (((*(in)>>4)&0x0F)+'A');
		out++;
		*out = (((*(in))&0x0F)+'A');
		out++;
		in++;
	}
	return;
}
// create the netbios-format name from a string
VOID fj_nameNETBIOSsub( CHAR *pStrIn, CHAR* pNetOut)
{
	int    i, j;

	j = strlen( pStrIn );
	for ( i = 0; i < 16; i++ )
	{
		if ( i < j ) *(pNetOut+i) = toupper(*(pStrIn+i));
		else         *(pNetOut+i) = ' ';
	}
	*(pNetOut+15) = NAME_SUFFIX_WORKSTATION;
	*(pNetOut+16) = 0;
	return;
}
// create the netbios-format name from the normal ID
VOID fj_nameNETBIOS()
{
	fj_nameNETBIOSsub( fj_CVT.pfph->strID, fj_serialNetBios );
	return;
}
// create the netbios-format name from the group ID
VOID fj_groupNETBIOS()
{
	fj_nameNETBIOSsub( fj_CVT.pfsys->strID, fj_groupNetBios );
	return;
}
// send the NETBIOS Name Registration Message
// broadcast if B mode
// send to NETBIOS server if P mode
// returns a count of bytes transmitted. 0 is failure.
// sets a timer to renew the registration
static LONG fj_nameNetbiosRegistration(VOID)
{
	CLPCFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph    = fj_CVT.pfph;
	NameRegistration nr;
	WORD32 ttl;
	struct tm tm;
	LONG   lRet;
	STRING str[100];

	// build a Name Registration Request
	transaction_reg = transaction_id;	// get unique transaction id
	nr.nshead.sTranID = transaction_reg;
	nr.nshead.sFlags  = NSH_REQUEST|NSH_OP_REGISTRATION|NSH_RECURDESIRED;
	nr.nshead.sQuestionCount          = 1;
	nr.nshead.sAnswerCount            = 0;
	nr.nshead.sResourceRecordCount    = 0;
	nr.nshead.sAddResourceRecordCount = 1;
	nr.nqr.length = 32;
	fj_asciiToDns( fj_serialNetBios, nr.nqr.name);
	nr.nqr.null   = 0;
	nr.nqr.type   = NRR_TYPE_NETBIOSNAMESERVICE;
	nr.nqr.class  = NRR_CLASS_INTERNET;
	nr.nrrp.namePointer = NRR_NAME_POINTER;
	nr.nrrp.type   = NRR_TYPE_NETBIOSNAMESERVICE;
	nr.nrrp.class  = NRR_CLASS_INTERNET;
	ttl = ttlRegistration;
	memcpy( &(nr.nrrp.ttl), &ttl, sizeof(ttl));
	nr.nrrp.rdlength = 6;
	nr.nrrp.rdflags  = fj_CVT.pfsys->sNETBIOSService;
	memcpy( &(nr.nrrp.address), &(pfph->lIPaddr), sizeof(pfph->lIPaddr));

	// if B node, broadcast
	if ( FJSYS_NETBIOS_B_NODE == fj_CVT.pfsys->sNETBIOSService )
	{
		nr.nshead.sFlags |= NSH_BROADCAST;
	}
	lRet = sendto( fj_socketNETBIOS,
		(CHAR*)&nr,
		sizeof(NameRegistration),
		0,
		&addrReg,
		sizesockaddr_in );

	if ( 0 == pfph->lGroupNumber )		// if first in group,
	{
		fj_asciiToDns( fj_groupNetBios, nr.nqr.name);
		lRet = sendto( fj_socketNETBIOS,
			(CHAR*)&nr,
			sizeof(NameRegistration),
			0,
			&addrReg,
			sizesockaddr_in );
	}

	if ( TRUE == pfph->bInfoName )
	{
		fj_SystemGetTime( &tm );
		fj_SystemGetTime24String( str, &tm );
		fj_writeInfo(str);
		sprintf( str, " Sent name registration to %d.%d.%d.%d  %d bytes\n", PUSH_IPADDR(addrReg.sin_addr.s_addr), (int)lRet);
		fj_writeInfo(str);
		if ( 0 == pfph->lGroupNumber )	// if first in group,
		{
			sprintf( str, " Sent group registration to %d.%d.%d.%d  %d bytes\n", PUSH_IPADDR(addrReg.sin_addr.s_addr), (int)lRet);
			fj_writeInfo(str);
		}
	}

	return( lRet );
}
//
// start registration process
//
static VOID fj_nameNetbiosRegistrationInit(VOID)
{
	transaction_id++;					// get unique transaction id
	bRegisterSend  = FALSE;
	if ( FJSYS_NETBIOS_B_NODE == fj_CVT.pfsys->sNETBIOSService )
	{
		lRegisterCount    = BCAST_REQ_RETRY_COUNT;
		lRegisterWaitTime = BCAST_REQ_RETRY_TIMEOUT;
		addrReg.sin_addr.s_addr = addrBroad.sin_addr.s_addr;
		bRegisterSend  = TRUE;
	}
	else if ( FJSYS_NETBIOS_P_NODE == fj_CVT.pfsys->sNETBIOSService )
	{
		lRegisterCount    = UCAST_REQ_RETRY_COUNT;
		lRegisterWaitTime = UCAST_REQ_RETRY_TIMEOUT;
		addrReg.sin_addr.s_addr = addrNETBIOS.sin_addr.s_addr;
		bRegisterSend  = TRUE;
	}
	bRegisterWait  = FALSE;
	bRegistered    = FALSE;
	return;
}
//
// small timer function for registration
//
static void fj_nameTimerFunc(ULONG in)
{
	if ( TRUE == bRegisterSend )		// if registration process is active
	{
		bRegisterWait = FALSE;			// wait time is expired. send again.
	}
	else
	{
										// else, start registration process
		fj_nameNetbiosRegistrationInit();
	}
	return;
}
static LONG fj_namesNetbiosQueryResponse( struct sockaddr_in *pTo, pNameQueryRequest pnqr)
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph  = fj_CVT.pfph;
	NameQueryResponse nqa;
	WORD32 ttl;
	LONG   lRet;

	memcpy( &nqa, pnqr, sizeof(NameQueryRequest));
	nqa.nshead.sFlags = NSH_RESPONSE|NSH_OP_QUERY|NSH_AUTHORITATIVE|NSH_RECURDESIRED|NSH_RECURAVAIL|NSH_RETURN_SUCCESS;
	nqa.nshead.sQuestionCount          = 0;
	nqa.nshead.sAnswerCount            = 1;
	nqa.nshead.sResourceRecordCount    = 0;
	nqa.nshead.sAddResourceRecordCount = 0;
	ttl = ttlRegistration;
	memcpy( &(nqa.nrr.ttl), &ttl, sizeof(ttl));
	nqa.nrr.rdlength = 6;
	nqa.nrr.rdflags  = fj_CVT.pfsys->sNETBIOSService;
	memcpy( &(nqa.nrr.address), &(pfph->lIPaddr), sizeof(pfph->lIPaddr));

	lRet = sendto( fj_socketNETBIOS, (CHAR*)&nqa, sizeof(NameQueryResponse), 0, pTo, sizesockaddr_in );

	return( lRet );
}
static LONG fj_namesNetbiosNodeStatusResponse( struct sockaddr_in *pTo, pNameQueryRequest pnqr)
{
	NodeStatusResponse nsr;
	WORD32 ttl;
	USHORT ush;
	int    i;
	LONG   lRet;

	memcpy( &nsr, pnqr, sizeof(NameQueryRequest));
	nsr.nshead.sFlags = NSH_RESPONSE|NSH_OP_QUERY|NSH_AUTHORITATIVE|NSH_RETURN_SUCCESS;
	nsr.nshead.sQuestionCount          = 0;
	nsr.nshead.sAnswerCount            = 1;
	nsr.nshead.sResourceRecordCount    = 0;
	nsr.nshead.sAddResourceRecordCount = 0;
	ttl = ttlRegistration;
	memcpy( &(nsr.ttl), &ttl, sizeof(ttl));
	// figure out how to use offset later
	nsr.rdlength = 0x41;
	nsr.numNames = 1;
	memcpy( &(nsr.name), &fj_serialNetBios, 16 );
	ush = fj_CVT.pfsys->sNETBIOSService|NSR_FLAG_ACTIVE;
	memcpy( &(nsr.nameFlags), &ush, sizeof(USHORT) );
	memcpy( nsr.macAddress, fj_CVT.pFJglobal->macaddr, 6 );

	ush = 0;
	for ( i = 0; i < 20; i++ )			// send 20 0 counters becasue this is what I saw in a document
	{
		memcpy( &(nsr.stats[i]), &ush, sizeof(USHORT) );
	}

	lRet = sendto( fj_socketNETBIOS, (CHAR*)&nsr, sizeof(NodeStatusResponse), 0, pTo, sizesockaddr_in );

	memcpy( &(nsr.name), &fj_serialNetBios, 16 );
	lRet = sendto( fj_socketNETBIOS, (CHAR*)&nsr, sizeof(NodeStatusResponse), 0, pTo, sizesockaddr_in );

	return( lRet );
}
void fj_threadNamesStart(ULONG param)
{
	CLPCFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph    = fj_CVT.pfph;
	struct sockaddr_in addrFrom;
	struct sockaddr_in addrPrevious;
	USHORT sTranIDPrevious;
	WORD8 *buffer;
	WORD8 *q1;
	WORD8 *q2;
	WORD8 *q3;
	WORD16 squestiontype;
	WORD16 squestionclass;
	NameServiceHeader  *pnsh;
	NameQueryRequest   *pnqr;
	NameRegistrationResponse  *pnrr;
	CHAR  str[180];
	CHAR  asc[32];
	CHAR  suffix;
	CHAR  *strop;
	WORD16 opcode;
	WORD16 returncode;
	WORD32 optval;
	WORD32 ttl;
	WORD16 rdlen;
	WORD16 rdflags;
	WORD32 ipaddr;
	BOOL   bNameQuery, bNameRespond, bStatusRespond, bShow;
	struct tm tm;
	LONG   i;
	LONG   seglen;
	LONG   seg1;
	LONG   lBytesRead;
	LONG   lRet;

	// initialize socket addresses
	addrBroad.sin_addr.s_addr   = pGlobal->ipbroadcastnet;
	addrNETBIOS.sin_addr.s_addr = fj_CVT.pfsys->lAddrWINS;

	// set default ttl
	ttlRegistration = 400000;			// 400 seconds
	// convert ttl milliseconds to BSP timer ticks
	ttlBSP = ( ttlRegistration / 1000 ) * BSP_TICKS_PER_SECOND;
	ttlTimer = ttlBSP / 2;				// sleep for half the ttl

	// create NETBIOS names
	fj_nameNETBIOS();
	fj_groupNETBIOS();

	// initialize previous request IDs
	addrPrevious.sin_port = 0;
	sTranIDPrevious = 0;

	// create the renewal timer
	lRet = tx_timer_create( &tx_name_renew,
		"name registration timer",
		fj_nameTimerFunc,
		0x234,							//id
		1*BSP_TICKS_PER_SECOND,
		1*BSP_TICKS_PER_SECOND,
		TX_NO_ACTIVATE );

	// open the socket
	fj_socketNETBIOS = socket( AF_INET, SOCK_DGRAM, 0 );

	if ( fj_socketNETBIOS > -1 )
	{
		// allocate buffer
		netbiosBuffer = fj_memory_getOnce(NAME_SERVICE_MAX_BUFFER);
										// set up request pointer
		pnqr = (NameQueryRequest *)netbiosBuffer;
		pnsh = &(pnqr->nshead);			// set up header pointer
										// set up registration response pointer
		pnrr = (NameRegistrationResponse *)netbiosBuffer;

		optval = 1;						// set value to turn options on
		lRet = setsockopt( fj_socketNETBIOS, SOL_SOCKET, SO_BROADCAST, (char*)&optval, 4 );
		lRet = setsockopt( fj_socketNETBIOS, SOL_SOCKET, SO_DONTROUTE, (char*)&optval, 4 );
		lRet = setsockopt( fj_socketNETBIOS, SOL_SOCKET, SO_KEEPALIVE, (char*)&optval, 4 );
		lRet = setsockopt( fj_socketNETBIOS, SOL_SOCKET, SO_BIO      , (char*)&optval, 4 );
		// bind this socket to the NETBIOS port
		lRet = bind( fj_socketNETBIOS, &addrNames, sizeof(addrNames));
	}

	// start registration
	fj_nameNetbiosRegistrationInit();

	// the blocked I/O option was set into the socket.
	// no sleep is necessary since the recvfrom will wait.
	while( fj_socketNETBIOS > 0 )
	{
		bNameQuery     = FALSE;			// don't do nuttin' til it's set to TRUE
		bNameRespond   = FALSE;			//
		bStatusRespond = FALSE;			//
		bShow          = FALSE;			//
		lBytesRead = recvfrom( fj_socketNETBIOS,
			(CHAR*)netbiosBuffer,
			NAME_SERVICE_MAX_BUFFER,
			0,
			&addrFrom,
			&sizesockaddr_in );
		if( lBytesRead > 0 )
		{
			opcode = (int)(pnqr->nshead.sFlags & NSH_OPCODE);

			if      ( NSH_OP_QUERY        == opcode ) bShow = FALSE;
			else if ( NSH_OP_REGISTRATION == opcode ) bShow = FALSE;
			else if ( NSH_OP_RELEASE      == opcode ) bShow = FALSE;
			else bShow = TRUE;

			// is it a name registration response?
			if ( ( pnrr->nshead.sTranID == transaction_reg ) &&
				( 1 == pnrr->nshead.sAnswerCount ) &&
				( 0 != (pnrr->nshead.sFlags&NSH_RESPONSE)) )
			{
				memcpy( &ipaddr, &(pnrr->nar.address), sizeof(ip_addr));
				returncode = pnrr->nshead.sFlags & NSH_RETURNCODE;
				if ( (NSH_RETURN_SUCCESS == returncode) &&
					(pfph->lIPaddr    == (LONG)ipaddr    ) )
				{
					// good return code
					// we are registered
					// save ttl in milliseconds
					memcpy( &ttl, &(pnrr->nar.ttl), sizeof(ttl) );
					ttlRegistration = ttl;
				}
				bRegistered = TRUE;		// end registration retry loop
				if ( TRUE == pfph->bInfoName )
				{
					fj_SystemGetTime( &tm );
					fj_SystemGetTime24String( str, &tm );
					fj_writeInfo(str);
					sprintf( str, " Registration response = %d. ttl = %d seconds.\n", (int)returncode, (int)(ttlRegistration/1000) );
					fj_writeInfo(str);
				}
			}

			// make sure that it is the simple name query that we understand
			// is it request
			if ( (  0 == (pnqr->nshead.sFlags&NSH_RESPONSE) ) &&
										// is it Name Query Request opcode
				(  NSH_OP_QUERY == opcode ) &&
				// can only respond to 1 question
				(  1 == pnqr->nshead.sQuestionCount          ) &&
				// required to be 0
				(  0 == pnqr->nshead.sAnswerCount            ) &&
				// required to be 0
				(  0 == pnqr->nshead.sResourceRecordCount    ) &&
				// required to be 0
				(  0 == pnqr->nshead.sAddResourceRecordCount ) )
				bNameQuery = TRUE;

			// NETBIOS name querys are broadcast in groups of 3.
			// ignore this if the same as the last one.
			// it is important not to reply more than once to our WinProxy server.
			if ( TRUE == bNameQuery )	// if it's worthy of a query,
			{
				// but it's the same as the
				if ( (sTranIDPrevious == pnqr->nshead.sTranID ) &&
					//  the last response,
					(addrPrevious.sin_addr.s_addr == addrFrom.sin_addr.s_addr) )
				{
					bNameQuery = FALSE;	//   skip it
					bShow  = FALSE;
				}
			}

			// save the critical info to be compared to the next query
			// and compare to see if this printer is being asked for
			if ( TRUE == bNameQuery )
			{
										// save previous id
				sTranIDPrevious = pnqr->nshead.sTranID;
				// save previous sender
				memcpy ( &addrPrevious, &addrFrom, sizeof(addrFrom) );

				// translate the name
				fj_dnsToAscii( pnqr->nqr.name, asc , pnqr->nqr.length );

				q2 = &(pnqr->nqr.length) + pnqr->nqr.length + 1;
				squestiontype  = ((*(q2+1))<<8) + (*(q2+2));
				squestionclass = ((*(q2+3))<<8) + (*(q2+4));

				// should we reply ????
				if ( (  NRR_TYPE_NETBIOSNAMESERVICE == squestiontype ) &&
										// NETBIOS name must be 32 bytes long
					( 32 == pnqr->nqr.length ) &&
					(  0 == *q2 ) &&	// can only have 1 segment - no NETBIOS scope
					(  (0 == strcmp(fj_serialNetBios, asc)) || ((0 == strcmp(fj_groupNetBios, asc))&&(0 == pfph->lGroupNumber)))
					) bNameRespond = TRUE;
				else if ( (  NRR_TYPE_NETBIOSNODESTATUS == squestiontype ) &&
					(  0 == *q2 ) &&	// can only have 1 segment - no NETBIOS scope
					(  0 == strcmp("*", asc) ) )
					bStatusRespond = TRUE;
				//		else bShow = TRUE;
			}

			if ( TRUE == pfph->bInfoName )
			{
				sprintf( str, "bytes read = %ld    from = %d.%d.%d.%d\n", lBytesRead,PUSH_IPADDR(addrFrom.sin_addr.s_addr));
				fj_writeInfo(str);
				sprintf( str, "NS header = ID(%d) QCount(%d) ACount(%d) RRCount(%d) ARRCount(%d) \n",
					(int)pnqr->nshead.sTranID,
					(int)pnqr->nshead.sQuestionCount,
					(int)pnqr->nshead.sAnswerCount,
					(int)pnqr->nshead.sResourceRecordCount,
					(int)pnqr->nshead.sAddResourceRecordCount);
				fj_writeInfo(str);
				if      ( NSH_OP_QUERY        == opcode ) strop = "NameQueryRequest";
				else if ( NSH_OP_REGISTRATION == opcode ) strop = "NameRegisRequest";
				else if ( NSH_OP_RELEASE      == opcode ) strop = "NameRelease     ";
				else if ( NSH_OP_WAITACK      == opcode ) strop = "WaitAcknowledgme";
				else if ( NSH_OP_REFRESH      == opcode ) strop = "NameRefresh     ";
				sprintf( str, "NS header = %s %s %s %s %s %s %s %x\n",
					((pnqr->nshead.sFlags&NSH_RESPONSE)==0)?"Request":"Response",
					strop,
					((pnqr->nshead.sFlags&NSH_AUTHORITATIVE)!=0)?"AuthAns":"!AuthAns",
					((pnqr->nshead.sFlags&NSH_TRUNCATION)   !=0)?"Trunc":"!Trunc",
					((pnqr->nshead.sFlags&NSH_RECURDESIRED) !=0)?"RecDes":"!RecDes",
					((pnqr->nshead.sFlags&NSH_RECURAVAIL)   !=0)?"RecAvail":"!RecAvail",
					((pnqr->nshead.sFlags&NSH_BROADCAST)    !=0)?"Broad":"!Broad",
					(int)((pnqr->nshead.sFlags&NSH_RETURNCODE)));
				fj_writeInfo(str);

				q1 = netbiosBuffer + sizeof(NameServiceHeader);
				for ( i = 0; i < pnqr->nshead.sQuestionCount; i++ )
				{
					q2 = q1;
					seglen = *q2;
					seg1 = 0;
					while ( seglen > 0 )
					{
						if ( 0 == seg1 )
						{
							// translate the name
							fj_dnsToAscii( q2+1, asc, seglen );
							suffix = asc[15];
							asc[15] = 0;
							seg1 = seglen;
						}
						q2 += seglen + 1;
						seglen = *q2;
					}
					squestiontype  = ((*(q2+1))<<8) + (*(q2+2));
					squestionclass = ((*(q2+3))<<8) + (*(q2+4));
					sprintf( str, "Question type = %x class = %x (%d)""%s[%x]""\n",
						(int)squestiontype,
						(int)squestionclass,
						seg1,
						asc,
						(int)suffix );
					fj_writeInfo(str);
					q1 = q2 + 5;
				}
				for ( i = 0; i < pnqr->nshead.sAnswerCount; i++ )
				{
					q2 = q1;
					seglen = *q2;
					seg1 = 0;
					while ( seglen > 0 )
					{
						if ( 0 == seg1 )
						{
							// translate the name
							fj_dnsToAscii( q2+1, asc, seglen );
							suffix = asc[15];
							asc[15] = 0;
							seg1 = seglen;
						}
						q2 += seglen + 1;
						seglen = *q2;
					}
					squestiontype  = ((*(q2+1))<<8) + (*(q2+2));
					squestionclass = ((*(q2+3))<<8) + (*(q2+4));
					q2 += 5;
					memcpy( &ttl, q2, sizeof(ttl) );
					q2 += 4;
					rdlen   = ((*(q2+0))<<8) + (*(q2+1));
					rdflags = ((*(q2+2))<<8) + (*(q2+3));
					q2 += 4;
					memcpy( &ipaddr, q2, sizeof(ipaddr) );
					sprintf( str, "Answer type = %x class = %x (%d)""%s[%x]"" ttl = %d rdlen = %x rdflags = %x ip = %d.%d.%d.%d\n",
						(int)squestiontype,
						(int)squestionclass,
						seg1,
						asc,
						(int)suffix,
						(int)(ttl/1000),
						(int)rdlen,
						(int)rdflags,
						PUSH_IPADDR(ipaddr) );
					fj_writeInfo(str);
					q1 = q2 + 4;
				}

				if ( 0 == pnqr->nshead.sResourceRecordCount )
				{
					for ( i = 0; i < pnqr->nshead.sAddResourceRecordCount; i++ )
					{
						sprintf(str, "ARR = %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x\n",
							*(q1+0),  *(q1+1),  *(q1+2),  *(q1+3),  *(q1+4),  *(q1+5),  *(q1+6),  *(q1+7),
							*(q1+8),  *(q1+9),  *(q1+10), *(q1+11), *(q1+12), *(q1+13), *(q1+14), *(q1+15),
							*(q1+16), *(q1+17), *(q1+18), *(q1+19), *(q1+20), *(q1+21), *(q1+22), *(q1+23) );
						fj_writeInfo(str);
						q1 += 24;
					}
				}
			}

			// OK. we have gotten this far. they want our name. reply to the sender.
			if ( TRUE == bNameRespond )
			{
				lRet = fj_namesNetbiosQueryResponse( &addrFrom, pnqr);
				if ( TRUE == pfph->bInfoName )
				{
					fj_SystemGetTime( &tm );
					fj_SystemGetTime24String( str, &tm );
					fj_writeInfo(str);
					sprintf( str, " Sent name response to %d.%d.%d.%d  %d bytes  -  %s\n", PUSH_IPADDR(addrFrom.sin_addr.s_addr), (int)lRet, asc);
					fj_writeInfo(str);
				}
			}
			// OK. we have gotten this far. they want our node status. reply to the sender.
			if ( TRUE == bStatusRespond )
			{
				lRet = fj_namesNetbiosNodeStatusResponse( &addrFrom, pnqr);
				if ( TRUE == pfph->bInfoName )
				{
					fj_SystemGetTime( &tm );
					fj_SystemGetTime24String( str, &tm );
					fj_writeInfo(str);
					sprintf( str, " Sent node status response to %d.%d.%d.%d  %d bytes\n", PUSH_IPADDR(addrFrom.sin_addr.s_addr), (int)lRet);
					fj_writeInfo(str);
				}
			}
		}
	}
	return;
}
//
// initialize the names code
//
//
VOID fj_nameInit(VOID)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int    ret;
	LPBYTE pStack;

	pStack = fj_memory_getStack(FJ_THREAD_NAMES_STACK_SIZE);
	// create the thread to process NETBIOS names requests.
										// control block for thread
	ret = tx_thread_create (&(pGlobal->thrNames),
		"Names Thread",					// thread name
		fj_threadNamesStart,			// entry function
		0,								// parameter
		pStack,							// start of stack
		FJ_THREAD_NAMES_STACK_SIZE,		// size of stack
		FJ_THREAD_NAMES_PRIORITY,		// priority
		FJ_THREAD_NAMES_PRIORITY,		// preemption threshold
		1,								// time slice threshold
		TX_AUTO_START);					// start immediately

	if (ret != TX_SUCCESS)				// if couldn't create thread
	{
		netosFatalError ("Unable to create Names thread", 1, 1);
	}

	return;
}
//
// check the names service
//
VOID fj_nameCheck(VOID)
{
	BOOL bSetTTL;

	if ( TRUE == bRegisterSend )		// is registration process active?
	{
		bSetTTL = FALSE;
		if ( FALSE == bRegistered )		// did registration response come in yet?
		{
										// waiting for a response?
			if ( FALSE == bRegisterWait )
			{
										// more retries to go?
				if ( lRegisterCount > 0 )
				{
					lRegisterCount--;
					bRegisterWait = TRUE;
										// send registration request
					fj_nameNetbiosRegistration();
					tx_timer_deactivate( &tx_name_renew );
					tx_timer_change( &tx_name_renew, lRegisterWaitTime, 0 );
					tx_timer_activate( &tx_name_renew );
				}
				else					// no more retries
				{
										// stop sending
					bRegisterSend = FALSE;
					// reduce interval if no response came in
					ttlTimer /= 2;
					// if too small, give up and set to large value
					if ( ttlTimer < UCAST_REQ_RETRY_TIMEOUT*UCAST_REQ_RETRY_COUNT ) ttlTimer = ttlBSP;
					bSetTTL = TRUE;
				}
			}
		}
		else							// registration response came in
		{
			bRegisterSend = FALSE;		// stop sending
			// ttlRegistration is either the default, the previous response, or the latest positive response.
			// in any case, use it to set the timer for the next registration retry.
			// convert ttl milliseconds to BSP timer ticks
			ttlBSP = ( ttlRegistration / 1000 ) * BSP_TICKS_PER_SECOND;
			ttlTimer = ttlBSP / 2;		// sleep for half the ttl
			bSetTTL = TRUE;
		}
		if ( TRUE == bSetTTL )
		{
			tx_timer_deactivate( &tx_name_renew );
			tx_timer_change( &tx_name_renew, ttlTimer, 0 );
			tx_timer_activate( &tx_name_renew );
		}
	}
	return;
}
