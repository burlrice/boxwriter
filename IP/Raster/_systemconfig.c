/* C file: c:\MyPrograms\FoxJet1\_systemconfig.c created
 by NET+Works HTML to C Utility on Wednesday, November 07, 2001 09:54:57 */
#include "fj.h"
#include "barcode.h"
#include "fj_label.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
void na_Double(unsigned long handle, double dInput, int iPlaces);
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_SEYes */
static void na_SEYes (unsigned long handle);
/*  @! File: na_SEYres */
static void na_SEYres (unsigned long handle);
/*  @! File: na_SEYspeed */
static void na_SEYspeed (unsigned long handle);
/*  @! File: na_SEYfreq */
static void na_SEYfreq (unsigned long handle);
/*  @! File: na_SENo */
static void na_SENo (unsigned long handle);
/*  @! File: na_SEres */
static void na_SEres (unsigned long handle);
/*  @! File: na_SEspeed */
static void na_SEspeed (unsigned long handle);
/*  @! File: na_SEfreq */
static void na_SEfreq (unsigned long handle);
/*  @! File: na_PCHigh */
static void na_PCHigh (unsigned long handle);
/*  @! File: na_PCLow */
static void na_PCLow (unsigned long handle);
/*  @! File: na_TITLE_CF_SYSTEMLAYOUT */
static void na_TITLE_CF_SYSTEMLAYOUT (unsigned long handle);
/*  @! File: na_DIV1 */
static void na_DIV1 (unsigned long handle);
/*  @! File: na_DIV2 */
static void na_DIV2 (unsigned long handle);
/*  @! File: na_DIV3 */
static void na_DIV3 (unsigned long handle);
/*  @! File: na_DIV4 */
static void na_DIV4 (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_BARCODE_TABLE */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\systemconfig.htm {{ */
void Send_function_23(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<meta name=\"GENERATOR\" content=\"Microsoft FrontPage 4.0\">\n");
	HSSend (handle, "<meta name=\"ProgId\" content=\"FrontPage.Editor.Document\">\n");
	HSSend (handle, "<title>");
	na_TITLE_CF_SYSTEMLAYOUT (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "function sefreq(Ovspeed,Ovdiv,Ovres,Ovfreq) {\n");
	HSSend (handle, " parmFCheck(Ovspeed,1,'Conveyor Speed');\n");
	HSSend (handle, " parmFCheck(Ovres,1,'Resolution');\n");
	HSSend (handle, " var speed = Ovspeed.value;\n");
	HSSend (handle, " var res   = Ovres.value;\n");
	HSSend (handle, " if ( 0 == speed )\n");
	HSSend (handle, " {\n");
	HSSend (handle, "  speed = 100;\n");
	HSSend (handle, "  Ovspeed.value = speed;\n");
	HSSend (handle, " }\n");
	HSSend (handle, " if ( 0 == res ) \n");
	HSSend (handle, " {\n");
	HSSend (handle, "  res = 100;\n");
	HSSend (handle, "  Ovres.value = res;\n");
	HSSend (handle, " }\n");
	HSSend (handle, " Ovfreq.value = res * speed *12/60 / Ovdiv ;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function senfreq() {\n");
	HSSend (handle, " var Of = document.formse;\n");
	HSSend (handle, " sefreq(Of.sen_speed,1,Of.sen_res,Of.sen_freq);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function seydiv() {\n");
	HSSend (handle, " var text;\n");
	HSSend (handle, " var Of = document.formse;\n");
	HSSend (handle, " var decimal;\n");
	HSSend (handle, " text = 'a' + Of.sey_res.value;\n");
	HSSend (handle, " decimal = 0;\n");
	HSSend (handle, " j = text.indexOf(\".\");\n");
	HSSend (handle, " if ( j > 0 ) decimal = text.length-j-1;\n");
	HSSend (handle, " for ( i = 0; i < 4; i++ )\n");
	HSSend (handle, " {\n");
	HSSend (handle, "  text = 'a' + Of.sey_res.value/(i+1);\n");
	HSSend (handle, "  j = text.indexOf(\".\");\n");
	HSSend (handle, "  if ( j > 0 ){ if ( decimal > 0 ) j += decimal;else j--;}\n");
	HSSend (handle, "  else j = text.length;\n");
	HSSend (handle, "  text = text.substr( 1, j );\n");
	HSSend (handle, "  Of.sey_freqdiv.options[i].text=text\n");
	HSSend (handle, " }\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function seyfreq() {\n");
	HSSend (handle, " var Of = document.formse;\n");
	HSSend (handle, " j = Of.sey_freqdiv.selectedIndex;\n");
	HSSend (handle, " sefreq(Of.sey_speed,j+1,Of.sey_res,Of.sey_freq);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function myreset(formObj) {\n");
	HSSend (handle, " formObj.reset();\n");
	HSSend (handle, " senfreq();seyfreq();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function mysubmit(formObj) {\n");
	HSSend (handle, " formObj.submit();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Configuration: System Layout\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<form name=formse action=systemlayout_1 method=POST>\n");
	HSSend (handle, " <left>\n");
	HSSend (handle, "  <table border=1 cellpadding=6 rules=groups>\n");
	HSSend (handle, "   <thead><tr><td colspan=4><b>Shaft Encoder</b></td></tr></thead>\n");
	HSSend (handle, "   <tbody>\n");
	HSSend (handle, "    <tr><td rowspan=4 align=right>Yes<input type=radio name=se ");
	na_SEYes (handle);
	HSSend (handle, " value=yes></td><td>Required</td> <td><input type=text name=sey_res size=6 value=\"");
	na_SEYres (handle);
	HSSend (handle, "\" onChange=\"seydiv();seyfreq();\"></td><td>Shaft Encoder true resolution - dots per inch</td></tr>\n");
	HSSend (handle, "    <tr><td>Required</td><td><select name=sey_freqdiv size=1 onChange=\"seyfreq();\"><option value=1 ");
	na_DIV1 (handle);
	HSSend (handle, " >1</option><option value=2 ");
	na_DIV2 (handle);
	HSSend (handle, " >2</option><option value=3 ");
	na_DIV3 (handle);
	HSSend (handle, " >3</option><option value=4 ");
	na_DIV4 (handle);
	HSSend (handle, " >4</option></select></td><td>Shaft Encoder effective resolution - dots per inch</td></tr>\n");
	HSSend (handle, "    <tr><td>Optional</td><td><input type=text name=sey_speed size=6 value=\"");
	na_SEYspeed (handle);
	HSSend (handle, "\" onChange=\"seyfreq()\"></td><td>Conveyor speed - feet per minute</td></tr>\n");
	HSSend (handle, "    <tr><td>&nbsp;</td><td><input type=text name=sey_freq size=6 value=\"");
	na_SEYfreq (handle);
	HSSend (handle, "\" DISABLED ><font color=#808080></td><td>Printer frequency - dots per second</font></td></tr>\n");
	HSSend (handle, "   </tbody>\n");
	HSSend (handle, "   <tbody>\n");
	HSSend (handle, "    <tr><td rowspan=3 align=right>No<input type=radio name=se ");
	na_SENo (handle);
	HSSend (handle, " value=no></td><td>Required</td><td><input type=text name=sen_res size=6 value=\"");
	na_SEres (handle);
	HSSend (handle, "\" onChange=\"senfreq()\"></td><td>Printer resolution - dots per inch</td></tr>\n");
	HSSend (handle, "    <tr><td>Required</td><td><input type=text name=sen_speed size=6 value=\"");
	na_SEspeed (handle);
	HSSend (handle, "\" onChange=\"senfreq()\"></td><td>Conveyor speed - feet per minute</td></tr>\n");
	HSSend (handle, "    <tr><td>&nbsp;</td><td><input type=text name=sen_freq size=6 value=\"");
	na_SEfreq (handle);
	HSSend (handle, "\" DISABLED ><font color=#808080></td><td>Printer frequency - dots per second</font></td></tr>\n");
	HSSend (handle, "   </tbody>\n");
	HSSend (handle, "  </table>\n");
	HSSend (handle, " </left>\n");
	HSSend (handle, " <p>&nbsp;</p>\n");
	HSSend (handle, " <center><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, "><button class=fjbtn1 onClick=\"mysubmit(this.form)\">Submit</button><button class=fjbtn1 onClick=\"myreset(this.form)\">Reset</button></center>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">senfreq();seydiv();seyfreq();</SCRIPT>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\systemconfig.htm }} */
/* @! End HTML page   */

void reload_systemconfig(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=systemconfig.htm");
	}
	else
	{
		fj_reload_page(handle, "systemconfig.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_systemlayout_1  {{ */
void Post_systemlayout_1(unsigned long handle)
{
	LPFJ_GLOBAL     pGlobal     = pCVT->pFJglobal;
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJSYSTEM      pfsysP      = &(pParam->FJSys);
	LPFJSYSTEM      pfsys       = pCVT->pfsys;
	int    retrom;
	char   formField[FJSYS_NAME_SIZE+1];
	BOOL   bROMWait;
	BOOL   bChanged;
	BOOL   bValue;
	FLOAT  fValue;
	LONG   lValue;
	LPSTR  pKey;
	int    ret;

	bROMWait = FALSE;

	// get ROM structure
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	bChanged = FALSE;
	if ( 0 == retrom  )
	{
		ret = fj_HSGetValue( handle, "sey_res", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fValue = atof( formField );
			if ( fValue != pfsysP->fEncoder )
			{
				pfsysP->fEncoder = fValue;
				bChanged = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "sey_freqdiv", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atoi( formField );
			if ( lValue != pfsysP->lEncoderDivisor )
			{
				pfsysP->lEncoderDivisor = lValue;
				bChanged = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "sen_res", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fValue = atof( formField );
			if ( fValue != pfsysP->fPrintResolution )
			{
				pfsysP->fPrintResolution = fValue;
				bChanged = TRUE;
			}
		}
		ret = fj_HSGetValue( handle, "se", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if( 0 == strcmp("yes",formField) )
			{
				bValue = TRUE;
				pKey = "sey_speed";
			}
			if( 0 == strcmp("no",formField) )
			{
				bValue = FALSE;
				pKey = "sen_speed";
			}
			if ( bValue != pfsysP->bShaftEncoder )
			{
				pfsysP->bShaftEncoder = bValue;
				bChanged = TRUE;
			}
			ret = fj_HSGetValue( handle, pKey, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				fValue = atof( formField ) * 12. / 60.;
				if ( fValue != pfsysP->fSpeed )
				{
					pfsysP->fSpeed = fValue;
					bChanged = TRUE;
				}
			}
		}

	}
	// write ROM structure
	if ( TRUE == bChanged )
	{
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		retrom = 0;
		if ( 0 == retrom )
		{
			pfsys->bShaftEncoder    = pfsysP->bShaftEncoder;
			pfsys->fSpeed           = pfsysP->fSpeed;
			pfsys->fEncoder         = pfsysP->fEncoder;
			pfsys->lEncoderDivisor  = pfsysP->lEncoderDivisor;
			pfsys->fPrintResolution = pfsysP->fPrintResolution;
			pCVT->fj_print_head_KillENI();
										// sent encoder value
			pCVT->fj_print_head_SetGAEncoder();
										// set encoder switch
			pCVT->fj_print_head_SetGAWriteSwitches();
			pCVT->fj_print_head_InitENI();

			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			pCVT->fj_SystemBuildSelectedLabel( pCVT->pfsys, pCVT->pBRAM->strLabelName );
			bROMWait = TRUE;
		}
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	reload_systemconfig(handle,bROMWait);
}										/* @! Function: Post_systemlayout_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_SEYes  {{ */
void na_SEYes(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfsys->bShaftEncoder ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SEYes  }} */
/* @! Function: na_SEYres  {{ */
void na_SEYres(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fEncoder, 2 );
}										/* @! Function: na_SEYres  }} */
/* @! Function: na_SEYspeed  {{ */
void na_SEYspeed(unsigned long handle)
{
	/* add your implementation here: */
	na_SEspeed(handle);
}										/* @! Function: na_SEYspeed  }} */
/* @! Function: na_SEYfreq  {{ */
void na_SEYfreq(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * pCVT->pfsys->fEncoder / (FLOAT)pCVT->pfsys->lEncoderDivisor * 12. /60.), 2 );
}										/* @! Function: na_SEYfreq  }} */
/* @! Function: na_SENo  {{ */
void na_SENo(unsigned long handle)
{
	/* add your implementation here: */
	if ( FALSE == pCVT->pfsys->bShaftEncoder ) HSSend (handle, "CHECKED");
}										/* @! Function: na_SENo  }} */
/* @! Function: na_SEres  {{ */
void na_SEres(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fPrintResolution, 2 );
}										/* @! Function: na_SEres  }} */
/* @! Function: na_SEspeed  {{ */
void na_SEspeed(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * 60. / 12.), 2 );
}										/* @! Function: na_SEspeed  }} */
/* @! Function: na_SEfreq  {{ */
void na_SEfreq(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (pCVT->pfsys->fSpeed * pCVT->pfsys->fPrintResolution * 12. / 60.), 2 );
}										/* @! Function: na_SEfreq  }} */
/* @! Function: na_TITLE_CF_SYSTEMLAYOUT  {{ */
void na_TITLE_CF_SYSTEMLAYOUT(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "System Layout" );
}										/* @! Function: na_TITLE_CF_SYSTEMLAYOUT  }} */
/* @! Function: na_DIV1  {{ */
void na_DIV1(unsigned long handle)
{
	/* add your implementation here: */
	if ( 1 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV1  }} */
/* @! Function: na_DIV2  {{ */
void na_DIV2(unsigned long handle)
{
	/* add your implementation here: */
	if ( 2 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV2  }} */
/* @! Function: na_DIV3  {{ */
void na_DIV3(unsigned long handle)
{
	/* add your implementation here: */
	if ( 3 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV3  }} */
/* @! Function: na_DIV4  {{ */
void na_DIV4(unsigned long handle)
{
	/* add your implementation here: */
	if ( 4 == pCVT->pfsys->lEncoderDivisor ) HSSend (handle, "SELECTED");
}										/* @! Function: na_DIV4  }} */
/* @! Function: na_PWID  {{ */
/* @! End of dynamic functions   */
