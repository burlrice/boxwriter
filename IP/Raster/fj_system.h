#ifndef FJ_SYSTEM
#define FJ_SYSTEM

#include <time.h>

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fj_bram;
struct fjlabel;
struct fjprinthead;
										// number of security levels
#define FJSYS_PRIORITY_LEVELS           10
#define FJSYS_PRIORITY_MANUFACTURING   111
#define FJSYS_PRIORITY_DISTRIBUTOR      98
#define FJSYS_PRIORITY_SECURITY         87
#define FJSYS_PRIORITY_CONFIGURATION    78
										// save and restore info through FTP
#define FJSYS_PRIORITY_RESTORE          65
										// edit, add, delete messages, labels, fonts, etc.
#define FJSYS_PRIORITY_MESSAGE_EDIT     56
										// reset counters
#define FJSYS_PRIORITY_COUNTERS         43
#define FJSYS_PRIORITY_TEST             34
#define FJSYS_PRIORITY_MESSAGE_SELECT   21
										// view pages other than first page
#define FJSYS_PRIORITY_VIEW_ALL         12
										// view only first page
#define FJSYS_PRIORITY_FIRST             0

										// HTML pages + FTP + TCP/IP
#define FJSYS_INTERNET_FULL             'F'
										// HTML pages + FTP
#define FJSYS_INTERNET_HTML_FTP         'W'
										// TCP/IP
#define FJSYS_INTERNET_TCPIP            'T'
										// one TCP/IP connection only
#define FJSYS_INTERNET_SLAVE            'S'

typedef struct fjsysgroupmem
{
	STR     strID[1+FJPH_ID_SIZE];		// print head ID
	LONG    lIPaddr;					// print head IP address
	struct fjprinthead FAR * pfph;		// pointer to FJPRINTHEAD structure
} FJSYSGROUPMEM, FAR *LPFJSYSGROUPMEM, FAR * const CLPFJSYSGROUPMEM;
typedef const struct fjsysgroupmem FAR *LPCFJSYSGROUPMEM, FAR * const CLPCFJSYSGROUPMEM;

typedef struct fjsysbarcode
{
										// name of this entry
	CHAR    strName[1+FJSYS_BARCODE_NAME_SIZE];
	LONG    lBCFlags;					// all necessary flags for BC Lib
	LONG    lBCHeight;					// Height of barcode image
	BOOL    bCheckDigit;				// TRUE to add check digit to the barcode
	LONG    lHor_Bearer;				// Horizontal bearer value
	LONG    lVer_Bearer;				// Vertical bearer value
	LONG    lQuietZone;					// QuietZone value
	LONG    lBarVal[5];					// Values for bar widths - [0]is narrow unit, 1-4 are bleed corrections for various bar widths
} FJSYSBARCODE, FAR *LPFJSYSBARCODE, FAR * const CLPFJSYSBARCODE;
typedef const struct fjsysbarcode FAR *LPCFJSYSBARCODE, FAR * const CLPCFJSYSBARCODE;

typedef struct fjsysdatamatrix
{
	CHAR    strName[1+FJSYS_BARCODE_NAME_SIZE];
	LONG    lHeight;
	LONG    lHeightBold;
	LONG    lWidth;
	LONG    lWidthBold;
} FJSYSDATAMATRIX, FAR *LPFJSYSDATAMATRIX, FAR * const CLPFJSYSDATAMATRIX;
typedef const struct fjsysdatamatrix FAR *LPCFJSYSDATAMATRIX, FAR * const CLPCFJSYSDATAMATRIX;

enum FJSYSTIMEDIS
{
	FJSYS_TIMEDIS_24         = 0x01,
	FJSYS_TIMEDIS_12,
	FJSYS_TIMEDIS_12M					// 12 hour with AM/PM
};
enum FJSYSUNITS
{
	FJSYS_UNITS_ENGLISH_FEET = 0x01,
	FJSYS_UNITS_ENGLISH_INCHES,
	FJSYS_UNITS_METRIC_METERS,
	FJSYS_UNITS_METRIC_CM,
	FJSYS_UNITS_METRIC_MM
};
typedef struct fjsysunits
{
	LPSTR     pLong;					// long word
	LPSTR     pShort;					// short abbreviation
}
#ifdef _WINDOWS
_FJSYSUNITS, FAR *LPFJSYSUNITS, FAR * const CLPFJSYSUNITS;

_FJSYSUNITS fj_aUnits[];
#else
FJSYSUNITS, FAR *LPFJSYSUNITS, FAR * const CLPFJSYSUNITS;
#endif									// _WINDOWS

#define FJSYS_NETBIOS_B_NODE     (0x0<<13)
#define FJSYS_NETBIOS_P_NODE     (0x1<<13)
#define FJSYS_NETBIOS_NONE       99

typedef struct fjsystem
{
	enum FJSYSUNITS   lUnits;			// measurement system for displaying info
	enum FJSYSTIMEDIS lTimeDisplay;		// format for displaying time
	BOOL    bShaftEncoder;				// shaft encoder present
	LONG    lEncoderDivisor;			// encoder divisor to get effective encoder rate
	FLOAT   fEncoder;					// encoder pulse speed - counts per inch
	FLOAT   fEncoderWheel;				// encoder wheel pulse speed - counts per inch
	FLOAT   fSpeed;						// no encoder - conveyor speed - inches per second
	FLOAT   fPrintResolution;			// no encoder - resolution     - dots per inch
	FLOAT   fInkBottleSize;				// bottle volume - milliliters only
	FLOAT   fInkBottleCost;				// bottle cost
	BOOL    bInternalClock;				// TRUE = use internal clock. FALSE = use external TOD clock.
	BOOL    bDSTUSA;					// TRUE = observe USA DST rules. FALSE = ignore DST.
	LONG    lTimeZoneSeconds;			// difference in seconds between UTC and local time
	ULONG   lAddrUDP13;					// suggested address for UDP 13 tod clock
	ULONG   lAddrTCP13;					// suggested address for TCP 13 tod clock
	ULONG   lAddrIP;					// suggested address for IP tod clock
	ULONG   lAddrEmail;					// ip address for Email server
	ULONG   lAddrWINS;					// ip address for WINS NETBIOS server
	ULONG   alAddrDNS[FJSYS_NUMBER_DNS];// DNS internet addresses
										// Email server name
	STR     strEmailServer[FJSYS_EMAIL_SERVER_SIZE+1];
										// Email appendage name
	STR     strEmailName[FJSYS_EMAIL_SERVER_SIZE+1];
	// low ink Email addresses
	STR     astrEmailInk[FJSYS_EMAIL_NUMBER][FJSYS_EMAIL_ID_SIZE+1];
	// service ink Email addresses
	STR     astrEmailService[FJSYS_EMAIL_NUMBER][FJSYS_EMAIL_ID_SIZE+1];
	USHORT  sNETBIOSService;			// type of NETBIOS name service
	LONG    lActiveCount;				// number of active print heads in group
	STR     strID[1+FJSYS_NAME_SIZE];	// group ID
	struct fjlabel FAR * pfl;			// pointer to FJLABEL structure
	struct fjlabel FAR * pflScanner;	// pointer to default scanner FJLABEL structure
										// group member entries
	FJSYSGROUPMEM gmMembers[FJSYS_NUMBER_PRINTERS];
										// system barcode definitions
	FJSYSBARCODE	bcArray[FJSYS_BARCODES];
	FJSYSDATAMATRIX bcDataMatrixArray[FJSYS_BARCODES];
	struct tm tmPhotoCell;				// Datetime - system time to use for DT conversions
	FLOAT   fRolloverHours;				// Datetime - rollover time in hours. value is added to midnight to get rollover time
	LONG    lWeekDay;					// Datetime - first day of week
	LONG    lWeek1;						// Datetime - number of January days in first week
	LONG    lWeek53;					// Datetime - value to use for week 53
	LONG    lShift1;					// Datetime - value for start of shift 1 - in minutes
	LONG    lShift2;					// Datetime - value for start of shift 2 - in minutes
	LONG    lShift3;					// Datetime - value for start of shift 3 - in minutes
										// Datetime - shift 1 string
	CHAR    sShiftCode1[1+FJSYS_SHIFT_CODE_SIZE];
										// Datetime - shift 2 string
	CHAR    sShiftCode2[1+FJSYS_SHIFT_CODE_SIZE];
										// Datetime - shift 3 string
	CHAR    sShiftCode3[1+FJSYS_SHIFT_CODE_SIZE];
										// Datetime - array of strings for days - normal
	CHAR    aDayN[7][1+FJDATETIME_STRING_SIZE];
										// Datetime - array of strings for days - special
	CHAR    aDayS[7][1+FJDATETIME_STRING_SIZE];
										// Datetime - array of strings for months - normal
	CHAR    aMonthN[12][1+FJDATETIME_STRING_SIZE];
										// Datetime - array of strings for months - special
	CHAR    aMonthS[12][1+FJDATETIME_STRING_SIZE];
										// Datetime - array of strings for AM and PM
	CHAR    aAMPM[2][1+FJDATETIME_STRING_SIZE];
										// Datetime - array of strings for alphabetic hours
	CHAR    aHour[24][1+FJDATETIME_STRING_SIZE];
	// passwords
	CHAR    aPassword[FJSYS_PRIORITY_LEVELS][1+FJSYS_PASSWORD_SIZE];
										// password levels
	LONG    alPasswordLevel[FJSYS_PRIORITY_LEVELS];
	CHAR    cInternetControl;			// type of internet control
	ULONG   lIOBoxIP;					// IP address for IOBox
	BOOL    bIOBoxUse;					// IOBox present
	int iIOBoxStrobe;					// IOBox strobe

} FJSYSTEM, FAR *LPFJSYSTEM, FAR * const CLPFJSYSTEM;
typedef const struct fjsystem FAR *LPCFJSYSTEM, FAR * const CLPCFJSYSTEM;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport LPFJSYSTEM fj_SystemNew( VOID );
	FJDLLExport VOID       fj_SystemDestroy( LPFJSYSTEM pfsys );
	FJDLLExport VOID       fj_SystemInit( LPFJSYSTEM pfsys );
	FJDLLExport struct fjlabel FAR * fj_SystemBuildSelectedLabel( LPFJSYSTEM pfsys, LPCSTR pName );
	FJDLLExport struct fjlabel FAR * fj_SystemBuildScannerDefaultLabel( LPFJSYSTEM pfsys, LPCSTR pName );
	FJDLLExport struct fjlabel FAR * fj_SystemCopyScannerDefaultLabelToSelectedLabel( LPFJSYSTEM pfsys );
	FJDLLExport LPCSTR     fj_SystemGetPasswordForLevel( CLPCFJSYSTEM pfsys, LONG lLevel );
	FJDLLExport LONG       fj_SystemGetPasswordLevel( CLPCFJSYSTEM pfsys, LPCSTR pPassword );
	FJDLLExport VOID       fj_SystemMakeLevelID( const struct fjprinthead FAR * const pfph, LONG lLevel, ULONG lIPaddr, LPSTR pAuth );
	FJDLLExport LONG       fj_SystemGetLevelID( struct fjprinthead FAR * pfph, ULONG lIPaddr, LPSTR pAuth );
	FJDLLExport VOID       fj_SystemToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemTimeToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemRomTimeToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemDateStringsToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemInternetToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemPasswordToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemBarCodesToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport BOOL       fj_SystemFromString( LPFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemGroupToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport BOOL       fj_SystemGroupFromString( LPFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_PrintModeToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport BOOL       fj_PrintModeFromString( LPFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_StatusModeToString( CLPCFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport BOOL       fj_StatusModeFromString( LPFJSYSTEM pfsys, LPSTR pString );
	FJDLLExport VOID       fj_SystemDataMatrixToString( CLPCFJSYSTEM pfsys, LPSTR pString );

	FJDLLExport struct tm * fj_SystemGetTime( struct tm *pTM );
	FJDLLExport LPSTR  fj_SystemGetDateTimeFullString( CLPCFJSYSTEM pfsys, LPSTR pString, struct tm *pTM );
	//FJDLLExport LPSTR fj_SystemGetDateTimeFullInetString( LPSTR pString, time_t t );
	//FJDLLExport LPSTR fj_SystemGetDateTimeFullUnixString( LPSTR pString, time_t t );
	FJDLLExport LPSTR  fj_SystemGetDateTimeUnixFileString( CLPCFJSYSTEM pfsys, LPSTR pString, struct tm *pTM );
	// 20 characters - yyyy/mm/dd hh:mm:ss
	FJDLLExport LPSTR  fj_SystemGetDateTimeString( LPSTR pString, const struct tm * const pTM );
	// 11 characters - yyyy/mm/dd
	FJDLLExport LPSTR  fj_SystemGetDateString( LPSTR pString, const struct tm * const pTM );
	//  9 characters - hh:mm:ss
	FJDLLExport LPSTR  fj_SystemGetTime24String( LPSTR pString, const struct tm * const pTM );
	//  9 characters - hh:mm:ss
	FJDLLExport LPSTR  fj_SystemGetTime12String( LPSTR pString, const struct tm * const pTM );
	// 15 characters max - hh:mm:ss A.M.
	FJDLLExport LPSTR  fj_SystemGetTime12AMPMString( LPSTR pString, const struct tm * const pTM );
	//  5 characters max
	FJDLLExport LPSTR  fj_SystemGetAMPMString( LPSTR pString, const struct tm * const pTM );
	// return time according to customer var1ables
	FJDLLExport LPSTR  fj_SystemGetTimeString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM );
	// return time without seconds according to customer var1ables
	FJDLLExport LPSTR  fj_SystemGetTimeNoSecString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM );
	// full name of month
	FJDLLExport LPSTR  fj_SystemGetMonthString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM );
	// full name of day of week
	FJDLLExport LPSTR  fj_SystemGetDayString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM );
	// full name of day of week plus the day of month
	FJDLLExport LPSTR  fj_SystemGetDayDateString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM );
	FJDLLExport VOID fj_SaveBRAMCounter( const struct fjelement * pfe );
	FJDLLExport VOID fj_ResetBRAMCounters( struct fj_bram * pBRAM );
	FJDLLExport VOID fj_RestoreBRAMCounters( struct fjmessage * pfm );
	FJDLLExport struct tm * fj_SystemParseDateTime( LPSTR pStr, struct tm *pTM );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_SYSTEM
