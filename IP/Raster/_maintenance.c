/* C file: C:\MyPrograms\IP_APP_v01\_maintenance.c created
 by NET+Works HTML to C Utility on Tuesday, February 26, 2002 08:25:31 */
#include "fj.h"
#include "fj_printhead.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_AD0 */
static void na_AD0 (unsigned long handle);
/*  @! File: na_AD1 */
static void na_AD1 (unsigned long handle);
/*  @! File: na_AD2 */
static void na_AD2 (unsigned long handle);
/*  @! File: na_AD3 */
static void na_AD3 (unsigned long handle);
/*  @! File: na_AD4 */
static void na_AD4 (unsigned long handle);
/*  @! File: na_AD5 */
static void na_AD5 (unsigned long handle);
/*  @! File: na_AD6 */
static void na_AD6 (unsigned long handle);
/*  @! File: na_AD7 */
static void na_AD7 (unsigned long handle);
/*  @! File: na_TITLE_MAINT */
static void na_TITLE_MAINT (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_GAHSclock */
static void na_GAHSclock (unsigned long handle);
/*  @! File: na_GAW0 */
static void na_GAW0 (unsigned long handle);
/*  @! File: na_GAW1 */
static void na_GAW1 (unsigned long handle);
/*  @! File: na_GAW2 */
static void na_GAW2 (unsigned long handle);
/*  @! File: na_GAW3 */
static void na_GAW3 (unsigned long handle);
/*  @! File: na_GAW45 */
static void na_GAW45 (unsigned long handle);
/*  @! File: na_GAW6 */
static void na_GAW6 (unsigned long handle);
/*  @! File: na_GAW7 */
static void na_GAW7 (unsigned long handle);
/*  @! File: na_GAW89 */
static void na_GAW89 (unsigned long handle);
/*  @! File: na_GAWA */
static void na_GAWA (unsigned long handle);
/*  @! File: na_GAWB */
static void na_GAWB (unsigned long handle);
/*  @! File: na_GAWC */
static void na_GAWC (unsigned long handle);
/*  @! File: na_GAWD */
static void na_GAWD (unsigned long handle);
/*  @! File: na_GAWE */
static void na_GAWE (unsigned long handle);
/*  @! File: na_GAWF */
static void na_GAWF (unsigned long handle);
/*  @! File: na_GAW10 */
static void na_GAW10 (unsigned long handle);
/*  @! File: na_GAW11 */
static void na_GAW11 (unsigned long handle);
/*  @! File: na_GAW1213 */
static void na_GAW1213 (unsigned long handle);
/*  @! File: na_GAW1415 */
static void na_GAW1415 (unsigned long handle);
/*  @! File: na_GAW1617 */
static void na_GAW1617 (unsigned long handle);
/*  @! File: na_GAW1819 */
static void na_GAW1819 (unsigned long handle);
/*  @! File: na_GAW1A1B */
static void na_GAW1A1B (unsigned long handle);
/*  @! File: na_GAR0 */
static void na_GAR0 (unsigned long handle);
/*  @! File: na_GAR45 */
static void na_GAR45 (unsigned long handle);
/*  @! File: na_GAR89 */
static void na_GAR89 (unsigned long handle);
/*  @! File: na_GARC */
static void na_GARC (unsigned long handle);
/*  @! File: na_GARD */
static void na_GARD (unsigned long handle);
/*  @! File: na_GARF */
static void na_GARF (unsigned long handle);
/*  @! File: na_HVTlow */
static void na_HVTlow (unsigned long handle);
/*  @! File: na_HVThigh */
static void na_HVThigh (unsigned long handle);
/*  @! File: na_TempLow */
static void na_TempLow (unsigned long handle);
/*  @! File: na_TempHigh */
static void na_TempHigh (unsigned long handle);
/*  @! File: na_HVAlow */
static void na_HVAlow (unsigned long handle);
/*  @! File: na_HVAhigh */
static void na_HVAhigh (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_H1DC */
static void na_H1DC (unsigned long handle);
/*  @! File: na_BOOTLOG */
static void na_BOOTLOG (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\maintenance.htm {{ */
void Send_function_29(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_MAINT (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Maintenance\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, " <p>\n");
	HSSend (handle, "  <table border=0 cellpadding=6>\n");
	HSSend (handle, "  <tr align=center><td><b>Boot Time</b></td><td><b>Reason</b></td><td><b>Battery</b></td><td><b>Clock</b></td></tr>\n");
	HSSend (handle, "  ");
	na_BOOTLOG (handle);
	HSSend (handle, "\n");
	HSSend (handle, "  </table>\n");
	HSSend (handle, " <p>\n");
	HSSend (handle, "  <table border=1>\n");
	HSSend (handle, "   <tr align=center><td colspan=9><b>GA values</b></td></tr>\n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>0</td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Write</td><td>Init ENI</td><td>Curve 1</td><td>Curve 2</td><td>Curve 3</td><td colspan=2>Encoder Freq</td><td>Vibrate Curve</td><td>Encoder Divisor</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>");
	na_GAW0 (handle);
	HSSend (handle, "</td><td>");
	na_GAW1 (handle);
	HSSend (handle, "</td><td>");
	na_GAW2 (handle);
	HSSend (handle, "</td><td>");
	na_GAW3 (handle);
	HSSend (handle, "</td><td colspan=2>");
	na_GAW45 (handle);
	HSSend (handle, "</td><td>");
	na_GAW6 (handle);
	HSSend (handle, "</td><td>");
	na_GAW7 (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Read</td><td>Version</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td colspan=2>Raster Counter</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>");
	na_GAR0 (handle);
	HSSend (handle, "</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td> <td colspan=2>");
	na_GAR45 (handle);
	HSSend (handle, "</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr><td colspan=9><b>&nbsp;</b></td></tr>\n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>8</td><td>9</td><td>A</td><td>B</td><td>C</td><td>D</td><td>E</td><td>F</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Write</td><td colspan=2>Vibrate Freq</td><td>Voltage PWM</td><td>AMS control</td><td>RT Clock Add</td><td>RT Clock Data</td><td>Channels</td><td>Control Flags</td></tr>\n");
	HSSend (handle, "   <tr align=center><td colspan=2>");
	na_GAW89 (handle);
	HSSend (handle, "</td><td>");
	na_GAWA (handle);
	HSSend (handle, "</td><td>");
	na_GAWB (handle);
	HSSend (handle, "</td><td>");
	na_GAWC (handle);
	HSSend (handle, "</td><td>");
	na_GAWD (handle);
	HSSend (handle, "</td><td>");
	na_GAWE (handle);
	HSSend (handle, "</td><td>");
	na_GAWF (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Read</td><td colspan=2>HS Clock</td><td>&nbsp;</td><td>&nbsp;</td> <td>RT Clock Add</td><td>RT Clock Data</td><td>&nbsp;</td><td>Status Flags</td></tr>\n");
	HSSend (handle, "   <tr align=center> <td colspan=2>");
	na_GAR89 (handle);
	HSSend (handle, "</td><td>&nbsp;</td><td>&nbsp;</td><td>");
	na_GARC (handle);
	HSSend (handle, "</td><td>");
	na_GARD (handle);
	HSSend (handle, "</td><td>&nbsp;</td><td>");
	na_GARF (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "   <tr><td colspan=9><b>&nbsp;</b></td></tr>   \n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Write</td><td>AMS A1<br>sets of pulses</td><td>AMS A6<br>pulses per set</td><td colspan=2>AMS A2<br>time before pulses</td><td colspan=2>AMS A3<br>time of pulse</td><td colspan=2>AMS A4<br>time between sets</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>");
	na_GAW10 (handle);
	HSSend (handle, "</td><td>");
	na_GAW11 (handle);
	HSSend (handle, "</td><td colspan=2>");
	na_GAW1213 (handle);
	HSSend (handle, "</td><td colspan=2>");
	na_GAW1415 (handle);
	HSSend (handle, "</td><td colspan=2>");
	na_GAW1617 (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Read</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr><td colspan=9><b>&nbsp;</b></td></tr>\n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>18</td><td>19</td><td>1A</td><td>1B</td><td>1C</td><td>1D</td><td>1E</td><td>1F</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Write</td><td colspan=2>AMS A5<br>time after pulses</td><td colspan=2>AMS A7<br>delay between pulses</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td colspan=2>");
	na_GAW1819 (handle);
	HSSend (handle, "</td><td colspan=2>");
	na_GAW1A1B (handle);
	HSSend (handle, "</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td rowspan=2>Read</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "  </table>  \n");
	HSSend (handle, " <p>\n");
	HSSend (handle, "  <table border=1>\n");
	HSSend (handle, "   <tr align=center><td colspan=8><b>A/D converter values</b></td></tr>\n");
	HSSend (handle, "   <tr align=center><td>");
	na_AD0 (handle);
	HSSend (handle, "</td><td>");
	na_AD1 (handle);
	HSSend (handle, "</td> <td>");
	na_AD2 (handle);
	HSSend (handle, "</td><td>");
	na_AD3 (handle);
	HSSend (handle, "</td><td>");
	na_AD4 (handle);
	HSSend (handle, "</td><td>");
	na_AD5 (handle);
	HSSend (handle, "</td><td>");
	na_AD6 (handle);
	HSSend (handle, "</td><td>");
	na_AD7 (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "   <tr align=center>\n");
	HSSend (handle, "    <td><applet code=fjinteger.class name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_BoardTemp\" width=26 height=16><param name=font value=\"Dialog\"><param name=style value=\"bold\"><param name=size value=\"14\"><param name=color value=\"0x000000\"></applet></td>\n");
	HSSend (handle, "    <td><applet code=fjinteger.class name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_Voltage\" width=26 height=16><param name=font value=\"Dialog\"><param name=style value=\"bold\"><param name=size value=\"14\"><param name=color value=\"0x000000\"></applet></td>\n");
	HSSend (handle, "    <td><applet code=fjinteger.class name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_HeadVoltage\" width=26 height=16><param name=font value=\"Dialog\"><param name=style value=\"bold\"><param name=size value=\"14\"><param name=color value=\"0x000000\"></applet></td>\n");
	HSSend (handle, "    <td><applet code=fjinteger.class name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_Ht1Temp\" width=20 height=16><param name=font value=\"Dialog\"><param name=style value=\"bold\"><param name=size value=\"14\"><param name=color value=\"0x000000\"></applet></td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "   </tr>\n");
	HSSend (handle, "   <tr align=center>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td><applet code=fjlight.class name=\"");
	na_ID (handle);
	HSSend (handle, " Light_VoltageReady\" width=16 height=16><param name=size value=11><param name=colorOn value=0x00FF00><param name=colorOff value=0xFF0000></applet></td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td><applet code=fjlight.class name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Ht1Ready\" width=16 height=16><param name=size value=11><param name=colorOn value=0x00FF00><param name=colorOff value=0xFF0000></applet></td>\n");
	HSSend (handle, "    <td><applet code=fjlight.class name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Ink\" width=16 height=16><param name=size value=11><param name=colorOn value=0x00FF00><param name=colorOff value=0xFFFF00></applet></td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "    <td>&nbsp;</td>\n");
	HSSend (handle, "   </tr>\n");
	HSSend (handle, "   <tr align=center><td>0 - 70 C</td><td>");
	na_HVAlow (handle);
	HSSend (handle, " - ");
	na_HVAhigh (handle);
	HSSend (handle, " V</td> <td>");
	na_HVTlow (handle);
	HSSend (handle, " - ");
	na_HVThigh (handle);
	HSSend (handle, " V</td><td>");
	na_TempLow (handle);
	HSSend (handle, " - ");
	na_TempHigh (handle);
	HSSend (handle, " C</td><td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td>Board Temperature</td><td>Actual Voltage</td><td>Head Voltage</td><td>Head Temperature</td><td>Ink Level</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "   <tr align=center><td colspan=8><applet code=fjsocket.class name=fjsocket width=500 height=16 ><param name=id  value=");
	na_ID (handle);
	HSSend (handle, "></applet></td></tr>\n");
	HSSend (handle, "  </table>\n");
	HSSend (handle, " <p>\n");
	HSSend (handle, "  <table border=1>\n");
	HSSend (handle, "   <tr align=center><td><b>Heater Duty Cycle</b></td></tr>\n");
	HSSend (handle, "   <tr align=center><td>");
	na_H1DC (handle);
	HSSend (handle, " %</td></tr>\n");
	HSSend (handle, "  </table>  \n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\maintenance.htm }} */
/* @! End HTML page   */

/* @! Begin of post function */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_AD0  {{ */
void na_AD0(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+0);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD0  }} */
/* @! Function: na_AD1  {{ */
void na_AD1(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+1);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD1  }} */
/* @! Function: na_AD2  {{ */
void na_AD2(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+2);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD2  }} */
/* @! Function: na_AD3  {{ */
void na_AD3(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+3);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD3  }} */
/* @! Function: na_AD4  {{ */
void na_AD4(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+4);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD4  }} */
/* @! Function: na_AD5  {{ */
void na_AD5(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+5);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD5  }} */
/* @! Function: na_AD6  {{ */
void na_AD6(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+6);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD6  }} */
/* @! Function: na_AD7  {{ */
void na_AD7(unsigned long handle)
{
	/* add your implementation here: */
	STR  str[16];
	WORD16 ad = *(((WORD16 *)&(pCVT->pFJglobal->adValues))+7);
	sprintf(str, "  0x%X", (int)ad );
	na_Long(handle, ad );
	HSSend (handle, str);
}										/* @! Function: na_AD7  }} */
/* @! Function: na_TITLE_MAINT  {{ */
void na_TITLE_MAINT(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Maintenance" );
}										/* @! Function: na_TITLE_MAINT  }} */
/* @! Function: na_ID  {{ */
/* @! Function: na_GAW0  {{ */
void na_GAW0(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+0));
}										/* @! Function: na_GAW0  }} */
/* @! Function: na_GAW1  {{ */
void na_GAW1(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+1));
}										/* @! Function: na_GAW1  }} */
/* @! Function: na_GAW2  {{ */
void na_GAW2(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+2));
}										/* @! Function: na_GAW2  }} */
/* @! Function: na_GAW3  {{ */
void na_GAW3(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+3));
}										/* @! Function: na_GAW3  }} */
/* @! Function: na_GAW45  {{ */
void na_GAW45(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+4));
}										/* @! Function: na_GAW45  }} */
/* @! Function: na_GAW6  {{ */
void na_GAW6(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+6));
}										/* @! Function: na_GAW6  }} */
/* @! Function: na_GAW7  {{ */
void na_GAW7(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+7));
}										/* @! Function: na_GAW7  }} */
/* @! Function: na_GAW89  {{ */
void na_GAW89(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+8));
}										/* @! Function: na_GAW89  }} */
/* @! Function: na_GAWA  {{ */
void na_GAWA(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+10));
}										/* @! Function: na_GAWA  }} */
/* @! Function: na_GAWB  {{ */
void na_GAWB(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+11));
}										/* @! Function: na_GAWB  }} */
/* @! Function: na_GAWC  {{ */
void na_GAWC(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+12));
}										/* @! Function: na_GAWC  }} */
/* @! Function: na_GAWD  {{ */
void na_GAWD(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+13));
}										/* @! Function: na_GAWD  }} */
/* @! Function: na_GAWE  {{ */
void na_GAWE(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+14));
}										/* @! Function: na_GAWE  }} */
/* @! Function: na_GAWF  {{ */
void na_GAWF(unsigned long handle)
{
	/* add your implementation here: */
	na_Hex(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+15));
}										/* @! Function: na_GAWF  }} */
/* @! Function: na_GAW10  {{ */
void na_GAW10(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+16));
}										/* @! Function: na_GAW10  }} */
/* @! Function: na_GAW11  {{ */
void na_GAW11(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+17));
}										/* @! Function: na_GAW11  }} */
/* @! Function: na_GAW1213  {{ */
void na_GAW1213(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+18));
}										/* @! Function: na_GAW1213  }} */
/* @! Function: na_GAW1415  {{ */
void na_GAW1415(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+20));
}										/* @! Function: na_GAW1415  }} */
/* @! Function: na_GAW1617  {{ */
void na_GAW1617(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+22));
}										/* @! Function: na_GAW1617  }} */
/* @! Function: na_GAW1819  {{ */
void na_GAW1819(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+24));
}										/* @! Function: na_GAW1819  }} */
/* @! Function: na_GAW1A1B  {{ */
void na_GAW1A1B(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaWrite))+26));
}										/* @! Function: na_GAW1A1B  }} */
/* @! Function: na_GAR0  {{ */
void na_GAR0(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaRead))+0));
}										/* @! Function: na_GAR0  }} */
/* @! Function: na_GAR45  {{ */
void na_GAR45(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaRead))+4));
}										/* @! Function: na_GAR45  }} */
/* @! Function: na_GAR89  {{ */
void na_GAR89(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(WORD16 *)(((WORD8 *)&(pCVT->pFJglobal->gaRead))+8));
}										/* @! Function: na_GAR89  }} */
/* @! Function: na_GARC  {{ */
void na_GARC(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaRead))+12));
}										/* @! Function: na_GARC  }} */
/* @! Function: na_GARD  {{ */
void na_GARD(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaRead))+13));
}										/* @! Function: na_GARD  }} */
/* @! Function: na_GARF  {{ */
void na_GARF(unsigned long handle)
{
	/* add your implementation here: */
	na_Hex(handle, *(((WORD8 *)&(pCVT->pFJglobal->gaRead))+15));
}										/* @! Function: na_GARF  }} */
/* @! Function: na_HVTlow  {{ */
void na_HVTlow(unsigned long handle)
{
	/* add your implementation here: */
	LPCVOLTINFO pvi;
	LPCVOLTINFO *pavi;
	LONG lTableNumber;
	LONG lVoltage = 0;

	lTableNumber = pCVT->pfph->pfop->lVoltageTable;
	if ( 0 != lTableNumber )
	{
		pavi = pCVT->paVoltInfo;
		pvi = pavi[lTableNumber];
		if ( NULL != pvi ) lVoltage = pvi->wFirstVolt;
	}
	else
	{
		lVoltage = pCVT->pfph->pfop->lVoltage;
	}
	na_Long(handle, lVoltage);
}										/* @! Function: na_HVTlow  }} */
/* @! Function: na_HVThigh  {{ */
void na_HVThigh(unsigned long handle)
{
	/* add your implementation here: */
	VOLTINFO **pavi;
	const VOLTINFO * pvi;
	LONG lTableNumber;
	LONG lVoltage = 0;

	lTableNumber = pCVT->pfph->pfop->lVoltageTable;
	if ( 0 != lTableNumber )
	{
		pavi = pCVT->paVoltInfo;
		pvi = pavi[lTableNumber];
		if ( NULL != pvi ) lVoltage = pvi->wLastVolt;
	}
	else
	{
		lVoltage = pCVT->pfph->pfop->lVoltage;
	}
	na_Long(handle, lVoltage);
}										/* @! Function: na_HVThigh  }} */
/* @! Function: na_TempLow  {{ */
void na_TempLow(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pfph->pfop->lTemp1 - pCVT->pfph->pfop->lTempRange);
}										/* @! Function: na_TempLow  }} */
/* @! Function: na_TempHigh  {{ */
void na_TempHigh(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pfph->pfop->lTemp1 + pCVT->pfph->pfop->lTempRange);
}										/* @! Function: na_TempHigh  }} */
/* @! Function: na_HVAlow  {{ */
void na_HVAlow(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pFJglobal->lHighVoltageWanted - 5);
}										/* @! Function: na_HVAlow  }} */
/* @! Function: na_HVAhigh  {{ */
void na_HVAhigh(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, pCVT->pFJglobal->lHighVoltageWanted + 5);
}										/* @! Function: na_HVAhigh  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_H1DC  {{ */
void na_H1DC(unsigned long handle)
{
	/* add your implementation here: */
	LPHEATER pHeater;
	LONG lOn, lOff;
	int  i;

	pHeater = &(pCVT->pFJglobal->heater1);

	lOn = lOff = 0;
	for ( i = HEATER_CYCLES-1; i > 0; i-- )
	{
		lOff += pHeater->lHeaterOff[i];
		lOn  += pHeater->lHeaterOn [i];
	}
	i = 0;
	if ( 0 != lOff )
	{
		i = 100 * lOn / ( lOn + lOff );
	}
	na_Long(handle, (LONG)i);
}										/* @! Function: na_H1DC  }} */
/* @! Function: na_BOOTLOG  {{ */
void na_BOOTLOG(unsigned long handle)
{
	LPFJ_BRAM pBRAM = pCVT->pBRAM;
	STR str[32];
	int i;

	for ( i = 0; i < BOOT_NUMBER; i++ )
	{
		if ( 0 == pBRAM->aBoots[i].lReason ) break;

		HSSend (handle, "<tr align=center><td>");
		pCVT->fj_SystemGetDateTimeString( str, &(pBRAM->aBoots[i].tmBoot) );
		HSSend (handle, str);
		HSSend (handle, "</td>");

		HSSend (handle, "<td>");
		if ( BOOT_REASON_EXTERAL  == pBRAM->aBoots[i].lReason ) HSSend (handle, "Power On");
		if ( BOOT_REASON_WATCHDOG == pBRAM->aBoots[i].lReason ) HSSend (handle, "Watch Dog Timer");
		if ( BOOT_REASON_DEBUG    == pBRAM->aBoots[i].lReason ) HSSend (handle, "Debug");
		HSSend (handle, "</td>");

		HSSend (handle, "<td>");
		if ( TRUE == pBRAM->aBoots[i].bBatt ) HSSend (handle, "OK");
		else                                  HSSend (handle, "bad");
		HSSend (handle, "</td>");

		HSSend (handle, "<td>");
		if ( TRUE == pBRAM->aBoots[i].bClock ) HSSend (handle, "OK");
		else                                   HSSend (handle, "bad");
		HSSend (handle, "</td></tr>");

		//		memcpy( &(pBRAM->aBoots[i]), &(pBRAM->aBoots[i-1]), sizeof(BOOTINFO) );
	}
	/* add your implementation here: */
}										/* @! Function: na_BOOTLOG  }} */
/* @! End of dynamic functions   */
