/* C file: c:\MyPrograms\FoxJet1\_ethernet.c created
 by NET+Works HTML to C Utility on Tuesday, June 27, 2000 16:10:04 */
#include "fj.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_TITLE_CF_ETHERNET */
static void na_TITLE_CF_ETHERNET (unsigned long handle);
/*  @! File: na_INK1 */
static void na_INK1 (unsigned long handle);
/*  @! File: na_INK2 */
static void na_INK2 (unsigned long handle);
/*  @! File: na_INK3 */
static void na_INK3 (unsigned long handle);
/*  @! File: na_SERV1 */
static void na_SERV1 (unsigned long handle);
/*  @! File: na_SERV2 */
static void na_SERV2 (unsigned long handle);
/*  @! File: na_SERV3 */
static void na_SERV3 (unsigned long handle);
/*  @! File: na_ESOURCE */
static void na_ESOURCE (unsigned long handle);
/*  @! File: na_ESERVIP */
static void na_ESERVIP (unsigned long handle);
/*  @! File: na_ESERVNAME */
static void na_ESERVNAME (unsigned long handle);
/*  @! File: na_DNS1 */
static void na_DNS1 (unsigned long handle);
/*  @! File: na_DNS2 */
static void na_DNS2 (unsigned long handle);
/*  @! File: na_DNS3 */
static void na_DNS3 (unsigned long handle);
/*  @! File: na_NBT_NONE */
static void na_NBT_NONE (unsigned long handle);
/*  @! File: na_NBT_P */
static void na_NBT_P (unsigned long handle);
/*  @! File: na_NBT_B */
static void na_NBT_B (unsigned long handle);
/*  @! File: na_WSERVIP */
static void na_WSERVIP (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_CONT_FULL */
static void na_CONT_FULL (unsigned long handle);
/*  @! File: na_CONT_WEB */
static void na_CONT_WEB (unsigned long handle);
/*  @! File: na_CONT_TCP */
static void na_CONT_TCP (unsigned long handle);
/*  @! File: na_CONT_SLAVE */
static void na_CONT_SLAVE (unsigned long handle);
/*  @! File: na_DHCPyes */
static void na_DHCPyes (unsigned long handle);
/*  @! File: na_DHCPno */
static void na_DHCPno (unsigned long handle);
/*  @! File: na_PRIP */
static void na_PRIP (unsigned long handle);
/*  @! File: na_PRSUB */
static void na_PRSUB (unsigned long handle);
/*  @! File: na_PRGATE */
static void na_PRGATE (unsigned long handle);
/*  @! File: na_IOBOXIP */
static void na_IOBOXIP (unsigned long handle);
/*  @! File: na_IOBOXyes */
static void na_IOBOXyes (unsigned long handle);
/*  @! File: na_IOBOXno */
static void na_IOBOXno (unsigned long handle);
/*  @! File: na_IOBOXstatus */
static void na_IOBOXstatus (unsigned long handle);
/*  @! File: na_IOBOXS1 */
static void na_IOBOXS1 (unsigned long handle);
/*  @! File: na_IOBOXS2 */
static void na_IOBOXS2 (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\ethernet.htm {{ */
void Send_function_13(unsigned long handle)
{
	HSSend (handle, "<HTML><HEAD><TITLE>");
	na_TITLE_CF_ETHERNET (handle);
	HSSend (handle, "</TITLE>\n");
	HSSend (handle, "<META http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Configuration: System Internet\"\n");
	HSSend (handle, "function submiteth(sa,si) {\n");
	HSSend (handle, "var of=document.formeth;\n");
	HSSend (handle, "of.action.value=sa; of.item.value=si;\n");
	HSSend (handle, "of.submit();}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "</HEAD>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<form name=formeth action=eth_1 method=post>\n");
	HSSend (handle, "<input type=hidden value=none name=action>\n");
	HSSend (handle, "<input type=hidden value=none name=item>\n");
	HSSend (handle, "<table width=\"95%\" border=\"0\" rules=\"none\" cellPadding=\"10\">\n");
	HSSend (handle, "<tr><td align=center>\n");
	HSSend (handle, "<table width=\"60%\" border=\"1\" cellpadding=\"2\" rules=\"groups\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=4><b>Ethernet</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=right rowspan=2>DHCP</td><td><input type=radio name=dhcp ");
	na_DHCPyes (handle);
	HSSend (handle, " value=yes>Yes</td><td colspan=2>Use DHCP to obtain a dynamic IP address</td></tr>\n");
	HSSend (handle, "<tr><td><input type=radio name=dhcp ");
	na_DHCPno (handle);
	HSSend (handle, " value=no>No</td><td>Static IP address</td><td><input type=text onchange=\"return ipCheck(this,'IP address');\" name=ip value=\"");
	na_PRIP (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td rowspan=2>&nbsp;</td><td>&nbsp;</td><td>Subnet mask</td><td><input type=text onchange=\"return ipCheck(this,'Subnet mask');\" name=sub value=\"");
	na_PRSUB (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td>Gateway address</td><td><input type=text onchange=\"return ipCheck(this,'Gateway address');\" name=gate value=\"");
	na_PRGATE (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "</tbody><tfoot>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td colspan=3><b>Note:</b> A change to the DHCP option or IP addresses <br>will not have any effect until the next power on.</td></tr>\n");
	HSSend (handle, "</tfoot></table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td align=center>\n");
	HSSend (handle, "<table width=\"60%\" border=\"1\" cellpadding=\"2\" rules=\"groups\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=3><b>Marksman Hub</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td align=left>Hub IP address</td><td colspan=2><input type=text onchange=\"return ipCheck(this,'IP address');\" name=ioboxip value=\"");
	na_IOBOXIP (handle);
	HSSend (handle, "\" ID=\"Text1\"></td></tr>\n");
	HSSend (handle, "<tr><td align=left>Enable communication</td><td><input type=radio name=iobox ");
	na_IOBOXyes (handle);
	HSSend (handle, " value=yes>Yes</td><td><input type=radio name=iobox ");
	na_IOBOXno (handle);
	HSSend (handle, " value=no>No</td></tr>\n");
	HSSend (handle, "<tr><td align=left>Use strobe</td><td><input type=radio name=ioboxst ");
	na_IOBOXS1 (handle);
	HSSend (handle, " value=yes>1</td><td><input type=radio name=ioboxst ");
	na_IOBOXS2 (handle);
	HSSend (handle, " value=no>2</td></tr>\n");
	HSSend (handle, "");
	na_IOBOXstatus (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table border=0 cellpadding=\"2\">\n");
	HSSend (handle, "<tr><td>EMail: Ink</td><td><input type=text size=25 maxlength=25 name=ink1 value=\"");
	na_INK1 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=ink2 value=\"");
	na_INK2 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=ink3 value=\"");
	na_INK3 (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td>EMail: Service</td><td><input type=text size=25 maxlength=25 name=serv1 value=\"");
	na_SERV1 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=serv2 value=\"");
	na_SERV2 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=serv3 value=\"");
	na_SERV3 (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td>EMail Source Name</td><td colspan=3><input type=text size=25 maxlength=25 name=esource value=\"");
	na_ESOURCE (handle);
	HSSend (handle, "\">&nbsp;&nbsp;If Email Server default is not desirable. Example,\"xyz.com\"</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td>EMail Server Name</td><td colspan=3><input type=text size=25 maxlength=25 name=eservname value=\"");
	na_ESERVNAME (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td>EMail Server IP Address</td><td colspan=3><input type=text size=25 maxlength=25 name=eservip value=\"");
	na_ESERVIP (handle);
	HSSend (handle, "\">&nbsp;&nbsp;If Email Server Name can not be found</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td>DNS Server IP Addresses</td><td><input type=text size=25 maxlength=25 name=dns1 value=\"");
	na_DNS1 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=dns2 value=\"");
	na_DNS2 (handle);
	HSSend (handle, "\"></td><td><input type=text size=25 maxlength=25 name=dns3 value=\"");
	na_DNS3 (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td colspan=3>DNS Servers are needed if EMail Server Name is given and DHCP is not used to obtain a dynamic IP address.</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td>WINS/NetBIOS Node Type</td><td colspan=3><select name=netbiostype size=1><option ");
	na_NBT_NONE (handle);
	HSSend (handle, " selected>None</option><option ");
	na_NBT_P (handle);
	HSSend (handle, ">P-Node</option><option ");
	na_NBT_B (handle);
	HSSend (handle, ">B-Node</option></select></td></tr>\n");
	HSSend (handle, "<tr><td>WINS Server IP Address</td><td colspan=3><input type=text size=25 maxlength=25 name=wservip value=\"");
	na_WSERVIP (handle);
	HSSend (handle, "\">&nbsp;&nbsp;If P-Node</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td colspan=4>&nbsp;</td></tr>\n");
	HSSend (handle, "<tr><td>Internet control</td><td colspan=3><select name=control size=1>\n");
	HSSend (handle, "<option ");
	na_CONT_FULL (handle);
	HSSend (handle, " selected>HTML + FTP + TCP/IP Sockets</option>\n");
	HSSend (handle, "<option ");
	na_CONT_WEB (handle);
	HSSend (handle, ">HTML + FTP only</option>\n");
	HSSend (handle, "<option ");
	na_CONT_TCP (handle);
	HSSend (handle, ">TCP/IP Sockets only</option>\n");
	HSSend (handle, "<option ");
	na_CONT_SLAVE (handle);
	HSSend (handle, ">one slave TCP/IP Socket only</option></select></td></tr>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td colspan=3>If HTML Web Pages are disabled, all Internet configuration data must be correctly entered into the external software; otherwise this printer may never be accessed again.</td></tr>\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td colspan=3>If the one Socket option for slave mode is selected, all data must be entered into the external software.</td></tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td align=center><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, " ID=\"Hidden1\"><input class=fjbtn1 type=submit value=\"Submit\"><input class=fjbtn1 type=reset value=\"Reset\"></center></td></tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</form></HTML>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\ethernet.htm }} */
/* @! End HTML page   */

void reload_ethernet(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=ethernet.htm");
	}
	else
	{
		fj_reload_page(handle, "ethernet.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_eth_1  {{ */
void Post_eth_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL     pGlobal     = pCVT->pFJglobal;
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJSYSTEM      pfsysP      = &(pParam->FJSys);
	LPFJSYSTEM      pfsys       = pCVT->pfsys;
	int    retrom;
	char   formField[99];
	BOOL   bROMWait;
	BOOL   bChanged;
	int    ret;
	int    i;
	STR    strEmailInk1[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailInk2[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailInk3[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailService1[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailService2[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailService3[FJSYS_EMAIL_ID_SIZE+1];
	STR    strEmailSource[FJSYS_EMAIL_SERVER_SIZE+1];
	STR    strEmailServer[FJSYS_EMAIL_SERVER_SIZE+1];
	ULONG  lEmail;
	ULONG  lDNS1;
	ULONG  lDNS2;
	ULONG  lDNS3;
	ULONG  lWINS;
	USHORT sWINSType;
	CHAR   cInternetControl;
	BOOL   bDHCP;
	WORD32 wIPaddr;
	WORD32 wSubnet;
	WORD32 wGateway;

	BOOL   bIOBox;
	int    iIOBoxStrobe;
	WORD32 lIPBoxIPaddr;

	LPFJ_NETOSPARAM pParamNETOS = pCVT->pNETOSparam;
	BOOL   bChangedNETOS;
	BOOL   bChangedFJ;

	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"IOBox") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				if ( 0 == strcmp(formField,"Connect") ) {fj_socket_IOBoxConnect(); fj_print_head_IOBoxInfo();}
				else if ( 0 == strcmp(formField,"Disconnect") ) fj_socket_IOBoxDisconnect();
				reload_ethernet(handle, bROMWait);
				return;
			}
		}
	}

	bROMWait = FALSE;

	// get ROM structure
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );

	bChangedNETOS = FALSE;
	retrom = pCVT->fj_ReadNETOSRomParams( pParamNETOS );
	if ( 0 == retrom  )
	{
		bDHCP    = pParamNETOS->useDhcp;
		wIPaddr  = pParamNETOS->ipAddress;
		wSubnet  = pParamNETOS->subnetMask;
		wGateway = pParamNETOS->gateway;

		ret = fj_HSGetValue( handle, "dhcp", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "yes" ) ) bDHCP = TRUE;
			else                                   bDHCP = FALSE;
		}
		ret = fj_HSGetValue( handle, "ip", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			wIPaddr = pCVT->fj_StrToIP( formField );
		}
		ret = fj_HSGetValue( handle, "sub", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			wSubnet = pCVT->fj_StrToIP( formField );
		}
		ret = fj_HSGetValue( handle, "gate", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			wGateway = pCVT->fj_StrToIP( formField );
		}

		if ( bDHCP != pParamNETOS->useDhcp )
		{
			pParamNETOS->useDhcp = bDHCP;
			bChangedNETOS = TRUE;
		}
		if ( wIPaddr != pParamNETOS->ipAddress )
		{
			pParamNETOS->ipAddress = wIPaddr;
			bChangedNETOS = TRUE;
		}
		if ( wSubnet != pParamNETOS->subnetMask )
		{
			pParamNETOS->subnetMask = wSubnet;
			bChangedNETOS = TRUE;
		}
		if ( wGateway != pParamNETOS->gateway )
		{
			pParamNETOS->gateway = wGateway;
			bChangedNETOS = TRUE;
		}
	}

	// get ROM structure
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	bChanged = FALSE;
	if ( 0 == retrom  )
	{
		bIOBox       = pfsys->bIOBoxUse;
		lIPBoxIPaddr = pfsys->lIOBoxIP;
		iIOBoxStrobe = pfsys->iIOBoxStrobe;

		ret = fj_HSGetValue( handle, "iobox", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "yes" ) ) bIOBox = TRUE;
			else                                   bIOBox = FALSE;
		}

		ret = fj_HSGetValue( handle, "ioboxst", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp( formField, "yes" ) ) iIOBoxStrobe = 1;
			else                                   iIOBoxStrobe = 2;
		}

		ret = fj_HSGetValue( handle, "ioboxip", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lIPBoxIPaddr = pCVT->fj_StrToIP( formField );
		}

		if ( lIPBoxIPaddr != pfsys->lIOBoxIP) bChanged = TRUE;
		if ( bIOBox != pfsys->bIOBoxUse)
		{
			pfsys->bIOBoxUse = bIOBox;
			pfsys->lIOBoxIP = lIPBoxIPaddr;
			if ( bIOBox == TRUE)
			{
				fj_socket_IOBoxConnect();
				fj_print_head_IOBoxInfo();
			}
			else fj_socket_IOBoxDisconnect();
			bChanged = TRUE;
		}
		if ( iIOBoxStrobe != pfsys->iIOBoxStrobe)
		{
			pfsys->iIOBoxStrobe = iIOBoxStrobe;
			bChanged = TRUE;
		}

		strcpy( strEmailInk1, pfsysP->astrEmailInk[0] );
		strcpy( strEmailInk2, pfsysP->astrEmailInk[1] );
		strcpy( strEmailInk3, pfsysP->astrEmailInk[2] );
		strcpy( strEmailService1, pfsysP->astrEmailService[0] );
		strcpy( strEmailService2, pfsysP->astrEmailService[1] );
		strcpy( strEmailService3, pfsysP->astrEmailService[2] );
		strcpy( strEmailSource, pfsysP->strEmailName );
		strcpy( strEmailServer, pfsysP->strEmailServer );
		lEmail = pfsysP->lAddrEmail;
		lDNS1 = pfsysP->alAddrDNS[0];
		lDNS2 = pfsysP->alAddrDNS[1];
		lDNS3 = pfsysP->alAddrDNS[2];
		lWINS = pfsysP->lAddrWINS;
		sWINSType = pfsysP->sNETBIOSService;
		cInternetControl = pfsysP->cInternetControl;

		formField[0] = 0;
		ret = fj_HSGetValue( handle, "ink1", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailInk1, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "ink2", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailInk2, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "ink3", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailInk3, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "serv1", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailService1, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "serv2", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailService2, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "serv3", formField, FJSYS_EMAIL_ID_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailService3, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "esource", formField, FJSYS_EMAIL_SERVER_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailSource, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "eservname", formField, FJSYS_EMAIL_SERVER_SIZE+1 );
		if ( -1 < ret )
		{
			strcpy( strEmailServer, formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "eservip", formField, sizeof(formField) );
		if ( -1 < ret )
		{
			lEmail = pCVT->fj_StrToIP( formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "dns1", formField, sizeof(formField) );
		if ( -1 < ret )
		{
			lDNS1 = pCVT->fj_StrToIP( formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "dns2", formField, sizeof(formField) );
		if ( -1 < ret )
		{
			lDNS2 = pCVT->fj_StrToIP( formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "dns3", formField, sizeof(formField) );
		if ( -1 < ret )
		{
			lDNS3 = pCVT->fj_StrToIP( formField );
		}
		formField[0] = 0;
		ret = fj_HSGetValue( handle, "wservip", formField, sizeof(formField) );
		if ( -1 < ret )
		{
			lWINS = pCVT->fj_StrToIP( formField );
		}
		ret = fj_HSGetValue( handle, "netbiostype", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			sWINSType = (USHORT)(atoi( formField ));
		}
		ret = fj_HSGetValue( handle, "control", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			cInternetControl = formField[0];
		}

		if ( 0 != strcmp(strEmailInk1,pfsysP->astrEmailInk[0]) )
		{
			strcpy( pfsysP->astrEmailInk[0], strEmailInk1 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailInk2,pfsysP->astrEmailInk[1]) )
		{
			strcpy( pfsysP->astrEmailInk[1], strEmailInk2 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailInk3,pfsysP->astrEmailInk[2]) )
		{
			strcpy( pfsysP->astrEmailInk[2], strEmailInk3 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailService1,pfsysP->astrEmailService[0]) )
		{
			strcpy( pfsysP->astrEmailService[0], strEmailService1 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailService2,pfsysP->astrEmailService[1]) )
		{
			strcpy( pfsysP->astrEmailService[1], strEmailService2 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailService3,pfsysP->astrEmailService[2]) )
		{
			strcpy( pfsysP->astrEmailService[2], strEmailService3 );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailSource,pfsysP->strEmailName) )
		{
			strcpy( pfsysP->strEmailName, strEmailSource );
			bChanged = TRUE;
		}
		if ( 0 != strcmp(strEmailServer,pfsysP->strEmailServer) )
		{
			strcpy( pfsysP->strEmailServer, strEmailServer );
			bChanged = TRUE;
		}
		if ( lEmail != pfsysP->lAddrEmail )
		{
			pfsysP->lAddrEmail = lEmail;
			bChanged = TRUE;
		}
		if ( lDNS1 != pfsysP->alAddrDNS[0] )
		{
			pfsysP->alAddrDNS[0] = lDNS1;
			bChanged = TRUE;
		}
		if ( lDNS2 != pfsysP->alAddrDNS[1] )
		{
			pfsysP->alAddrDNS[1] = lDNS2;
			bChanged = TRUE;
		}
		if ( lDNS3 != pfsysP->alAddrDNS[2] )
		{
			pfsysP->alAddrDNS[2] = lDNS3;
			bChanged = TRUE;
		}
		if ( lWINS != pfsysP->lAddrWINS )
		{
			pfsysP->lAddrWINS = lWINS;
			bChanged = TRUE;
		}
		if ( sWINSType != pfsysP->sNETBIOSService )
		{
			pfsysP->sNETBIOSService = sWINSType;
			bChanged = TRUE;
		}
		/*
				if ( cInternetControl != pfsysP->cInternetControl )
				{
					pfsysP->cInternetControl = cInternetControl;
					bChanged = TRUE;
				}*/
	}

	// write ROM structure
	if ( TRUE == bChangedNETOS )
	{
		// write and keep ROM lock
		pCVT->fj_WriteNETOSRomParams( pParamNETOS, !bChanged, NULL, 0 );
		bROMWait = TRUE;
	}

	// write ROM structure
	if ( TRUE == bChanged )
	{
		pfsysP->bIOBoxUse = bIOBox;
		pfsysP->lIOBoxIP = lIPBoxIPaddr;
		pfsysP->iIOBoxStrobe = iIOBoxStrobe;
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		retrom = 0;
		if ( 0 == retrom )
		{
			pfsys->bIOBoxUse = bIOBox;
			pfsys->lIOBoxIP = lIPBoxIPaddr;
			strcpy( pfsys->astrEmailInk[0], pfsysP->astrEmailInk[0] );
			strcpy( pfsys->astrEmailInk[1], pfsysP->astrEmailInk[1] );
			strcpy( pfsys->astrEmailInk[2], pfsysP->astrEmailInk[2] );
			strcpy( pfsys->astrEmailService[0], pfsysP->astrEmailService[0] );
			strcpy( pfsys->astrEmailService[1], pfsysP->astrEmailService[1] );
			strcpy( pfsys->astrEmailService[2], pfsysP->astrEmailService[2] );
			strcpy( pfsys->strEmailName, pfsysP->strEmailName );
			strcpy( pfsys->strEmailServer, pfsysP->strEmailServer );
			pfsys->lAddrEmail = pfsysP->lAddrEmail;
			pfsys->alAddrDNS[0] = pfsysP->alAddrDNS[0];
			pfsys->alAddrDNS[1] = pfsysP->alAddrDNS[1];
			pfsys->alAddrDNS[2] = pfsysP->alAddrDNS[2];
			pfsys->lAddrWINS    = pfsysP->lAddrWINS;
			pfsys->sNETBIOSService = pfsysP->sNETBIOSService;
			pfsys->cInternetControl = pfsysP->cInternetControl;
			bROMWait = TRUE;
		}
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	reload_ethernet(handle,bROMWait);
}										/* @! Function: Post_eth_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_ID  {{ */
/* @! Function: na_TITLE_CF_ETHERNET  {{ */
void na_TITLE_CF_ETHERNET(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "System Internet" );
}										/* @! Function: na_TITLE_CF_ETHERNET  }} */
/* @! Function: na_INK1  {{ */
void na_INK1(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailInk[0]);
}										/* @! Function: na_INK1  }} */
/* @! Function: na_INK2  {{ */
void na_INK2(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailInk[1]);
}										/* @! Function: na_INK2  }} */
/* @! Function: na_INK3  {{ */
void na_INK3(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailInk[2]);
}										/* @! Function: na_INK3  }} */
/* @! Function: na_SERV1  {{ */
void na_SERV1(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailService[0]);
}										/* @! Function: na_SERV1  }} */
/* @! Function: na_SERV2  {{ */
void na_SERV2(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailService[1]);
}										/* @! Function: na_SERV2  }} */
/* @! Function: na_SERV3  {{ */
void na_SERV3(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->astrEmailService[2]);
}										/* @! Function: na_SERV3  }} */
/* @! Function: na_ESOURCE  {{ */
void na_ESOURCE(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->strEmailName);
}										/* @! Function: na_ESOURCE  }} */
/* @! Function: na_ESERVIP  {{ */
void na_ESERVIP(unsigned long handle)
{
	/* add your implementation here: */
	char str[20];
	ULONG lip;
	lip = pCVT->pfsys->lAddrEmail;
	if ( 0 != lip )
	{
		pCVT->fj_IPToStr( lip, str );
		HSSend (handle, str);
	}
}										/* @! Function: na_ESERVIP  }} */
/* @! Function: na_ESERVNAME  {{ */
void na_ESERVNAME(unsigned long handle)
{
	/* add your implementation here: */
	na_String(handle, pCVT->pfsys->strEmailServer);
}										/* @! Function: na_ESERVNAME  }} */
/* @! Function: na_DNS1  {{ */
void na_DNS1(unsigned long handle)
{
	/* add your implementation here: */
	char str[20];
	ULONG lip;
	lip = pCVT->pfsys->alAddrDNS[0];
	if ( 0 != lip )
	{
		pCVT->fj_IPToStr( lip, str );
		HSSend (handle, str);
	}
}										/* @! Function: na_DNS1  }} */
/* @! Function: na_DNS2  {{ */
void na_DNS2(unsigned long handle)
{
	/* add your implementation here: */
	char str[20];
	ULONG lip;
	lip = pCVT->pfsys->alAddrDNS[1];
	if ( 0 != lip )
	{
		pCVT->fj_IPToStr( lip, str );
		HSSend (handle, str);
	}
}										/* @! Function: na_DNS2  }} */
/* @! Function: na_DNS3  {{ */
void na_DNS3(unsigned long handle)
{
	/* add your implementation here: */
	char str[20];
	ULONG lip;
	lip = pCVT->pfsys->alAddrDNS[2];
	if ( 0 != lip )
	{
		pCVT->fj_IPToStr( lip, str );
		HSSend (handle, str);
	}
}										/* @! Function: na_DNS3  }} */
/* @! Function: na_NBT_NONE  {{ */
void na_NBT_NONE(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Long(handle, FJSYS_NETBIOS_NONE);
	if ( FJSYS_NETBIOS_NONE == pCVT->pfsys->sNETBIOSService )HSSend (handle, " SELECTED");
}										/* @! Function: na_NBT_NONE  }} */
/* @! Function: na_NBT_P  {{ */
void na_NBT_P(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Long(handle, FJSYS_NETBIOS_P_NODE);
	if ( FJSYS_NETBIOS_P_NODE == pCVT->pfsys->sNETBIOSService )HSSend (handle, " SELECTED");
}										/* @! Function: na_NBT_P  }} */
/* @! Function: na_NBT_B  {{ */
void na_NBT_B(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Long(handle, FJSYS_NETBIOS_B_NODE);
	if ( FJSYS_NETBIOS_B_NODE == pCVT->pfsys->sNETBIOSService )HSSend (handle, " SELECTED");
}										/* @! Function: na_NBT_B  }} */
/* @! Function: na_WSERVIP  {{ */
void na_WSERVIP(unsigned long handle)
{
	/* add your implementation here: */
	char str[20];
	ULONG lip;
	lip = pCVT->pfsys->lAddrWINS;
	if ( 0 != lip )
	{
		pCVT->fj_IPToStr( lip, str );
		HSSend (handle, str);
	}
}										/* @! Function: na_WSERVIP  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_CONT_FULL  {{ */
void na_CONT_FULL(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Char(handle, (char)FJSYS_INTERNET_FULL);
	if ( FJSYS_INTERNET_FULL == pCVT->pfsys->cInternetControl )HSSend (handle, " SELECTED");
}										/* @! Function: na_CONT_FULL  }} */
/* @! Function: na_CONT_WEB  {{ */
void na_CONT_WEB(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Char(handle, (char)FJSYS_INTERNET_HTML_FTP);
	if ( FJSYS_INTERNET_HTML_FTP == pCVT->pfsys->cInternetControl )HSSend (handle, " SELECTED");
}										/* @! Function: na_CONT_WEB  }} */
/* @! Function: na_CONT_TCP  {{ */
void na_CONT_TCP(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Char(handle, (char)FJSYS_INTERNET_TCPIP);
	if ( FJSYS_INTERNET_TCPIP == pCVT->pfsys->cInternetControl )HSSend (handle, " SELECTED");
}										/* @! Function: na_CONT_TCP  }} */
/* @! Function: na_CONT_SLAVE  {{ */
void na_CONT_SLAVE(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "VALUE=");
	na_Char(handle, (char)FJSYS_INTERNET_SLAVE);
	if ( FJSYS_INTERNET_SLAVE == pCVT->pfsys->cInternetControl )HSSend (handle, " SELECTED");
}										/* @! Function: na_CONT_SLAVE  }} */
/* @! Function: na_DHCPyes  {{ */
void na_DHCPyes(unsigned long handle)
{
	/* add your implementation here: */
	if ( 0 != pCVT->pNETOSparam->useDhcp ) HSSend (handle, "CHECKED");
}										/* @! Function: na_DHCPyes  }} */
/* @! Function: na_DHCPno  {{ */
void na_DHCPno(unsigned long handle)
{
	/* add your implementation here: */
	if ( 0 == pCVT->pNETOSparam->useDhcp ) HSSend (handle, "CHECKED");
}										/* @! Function: na_DHCPno  }} */
/* @! Function: na_PRIP  {{ */
void na_PRIP(unsigned long handle)
{
	/* add your implementation here: */

	na_IPaddr(handle, pCVT->pNETOSparam->ipAddress );
}										/* @! Function: na_PRIP  }} */
/* @! Function: na_PRSUB  {{ */
void na_PRSUB(unsigned long handle)
{
	/* add your implementation here: */
	na_IPaddr(handle, pCVT->pNETOSparam->subnetMask );
}										/* @! Function: na_PRSUB  }} */
/* @! Function: na_PRGATE  {{ */
void na_PRGATE(unsigned long handle)
{
	/* add your implementation here: */
	na_IPaddr(handle, pCVT->pNETOSparam->gateway );
}										/* @! Function: na_PRGATE  }} */
/* @! Function: na_IOBOXIP  {{ */
void na_IOBOXIP(unsigned long handle)
{
	na_IPaddr(handle, pCVT->pfsys->lIOBoxIP );
}										/* @! Function: na_IOBOXIP  }} */
/* @! Function: na_IOBOXyes  {{ */
void na_IOBOXyes(unsigned long handle)
{
	if ( TRUE == pCVT->pfsys->bIOBoxUse ) HSSend (handle, "CHECKED");
}										/* @! Function: na_IOBOXyes  }} */
/* @! Function: na_IOBOXno  {{ */
void na_IOBOXno(unsigned long handle)
{
	if ( FALSE == pCVT->pfsys->bIOBoxUse ) HSSend (handle, "CHECKED");
}										/* @! Function: na_IOBOXno  }} */
/* @! Function: na_IOBOXstatus  {{ */
void na_IOBOXstatus(unsigned long handle)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	char pStr[12];

	if ( TRUE == pCVT->pfsys->bIOBoxUse )
	{
		if ( pGlobal->lIOBoxSocketIndex == -1) strcpy ( pStr,"Connect");
		else  strcpy ( pStr,"Disconnect");

		HSSend (handle, "<tfoot><tr><td align=left>Status</td><td>");
		if ( pGlobal->lIOBoxSocketIndex == -1) HSSend (handle, "<i>Disconnected</i>");
		else  HSSend (handle, "<i>Connected</i>");
		HSSend (handle, "</td><td align=center><button class=fjbtn1 onClick='submiteth(\"IOBox\",\"");
		HSSend (handle, pStr);
		HSSend (handle, "\")'>");
		HSSend (handle, pStr);
		HSSend (handle, "</button></td></tr></tfoot>");
	}
}										/* @! Function: na_IOBOXstatus  }} */
/* @! Function: na_IOBOXS1  {{ */
void na_IOBOXS1(unsigned long handle)
{
	if ( 1 == pCVT->pfsys->iIOBoxStrobe ) HSSend (handle, "CHECKED");
}										/* @! Function: na_IOBOXS1  }} */
/* @! Function: na_IOBOXS2  {{ */
void na_IOBOXS2(unsigned long handle)
{
	if ( 2 == pCVT->pfsys->iIOBoxStrobe ) HSSend (handle, "CHECKED");
}										/* @! Function: na_IOBOXS2  }} */
/* @! End of dynamic functions   */
