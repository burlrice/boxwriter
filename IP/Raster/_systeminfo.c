/* C file: c:\MyPrograms\FoxJet1\_systeminfo.c created
 by NET+Works HTML to C Utility on Wednesday, October 03, 2001 12:09:38 */
#include "fj.h"
#include "barcode.h"
#include "fj_label.h"
#include "fj_system.h"

void na_Double(unsigned long handle, double dInput, int iPlaces);

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_UNITS_DEF */
static void na_UNITS_DEF (unsigned long handle);
/*  @! File: na_INK_SIZE */
static void na_INK_SIZE (unsigned long handle);
/*  @! File: na_INK_COST */
static void na_INK_COST (unsigned long handle);
/*  @! File: na_TITLE_CF_SYSTEMINFO */
static void na_TITLE_CF_SYSTEMINFO (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_TIME_DEF */
static void na_TIME_DEF (unsigned long handle);
/*  @! File: na_GRVIS */
static void na_GRVIS (unsigned long handle);
/*  @! File: na_GRNAME */
void na_GRNAME (unsigned long handle);
/*  @! File: na_BARCODE_TABLE */
static void na_BARCODE_TABLE (unsigned long handle);
static void na_DATAMATRIX_TABLE (unsigned long handle);
/*  @! File: na_GRNAME */
void na_GRNAME (unsigned long handle);
/*  @! File: na_TODint */
static void na_TODint (unsigned long handle);
/*  @! File: na_TMYEAR */
static void na_TMYEAR (unsigned long handle);
/*  @! File: na_TMMONTH */
static void na_TMMONTH (unsigned long handle);
/*  @! File: na_TMDAY */
static void na_TMDAY (unsigned long handle);
/*  @! File: na_TMHOUR */
static void na_TMHOUR (unsigned long handle);
/*  @! File: na_TMMINUTE */
static void na_TMMINUTE (unsigned long handle);
/*  @! File: na_TMSECOND */
static void na_TMSECOND (unsigned long handle);
/*  @! File: na_TODext */
static void na_TODext (unsigned long handle);
/*  @! File: na_TMUDP13INUSE */
static void na_TMUDP13INUSE (unsigned long handle);
/*  @! File: na_TMTCP13INUSE */
static void na_TMTCP13INUSE (unsigned long handle);
/*  @! File: na_TMIPINUSE */
static void na_TMIPINUSE (unsigned long handle);
/*  @! File: na_TMTZN */
static void na_TMTZN (unsigned long handle);
/*  @! File: na_TMDSTUSA */
static void na_TMDSTUSA (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\systeminfo.htm {{ */
void Send_function_24(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_CF_SYSTEMINFO (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "function TZCheck(Otz) {\n");
	HSSend (handle, "var value = Otz.value;\n");
	HSSend (handle, "var isok = isFloat(value,false);\n");
	HSSend (handle, "if ( true == isok ){if ( (value < -12.0) || (value > 12.0) ) isok = false;}\n");
	HSSend (handle, "if ( false == isok )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "alert(\"Time Zone value must be between -12.0 and +12.0\");\n");
	HSSend (handle, "Otz.value = 0; Otz.focus();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "return isok;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "units = \"");
	na_UNITS_DEF (handle);
	HSSend (handle, "\" ;\n");
	HSSend (handle, "function sesetunit() {\n");
	HSSend (handle, "var i,Ou;\n");
	HSSend (handle, "Ou = document.formsi.units;\n");
	HSSend (handle, "for ( i = 0; i < Ou.options.length; i++ )\n");
	HSSend (handle, "{if ( units == Ou.options[i].value ){Ou.selectedIndex = i; break;} }\n");
	HSSend (handle, "}\n");
	HSSend (handle, "time = \"");
	na_TIME_DEF (handle);
	HSSend (handle, "\" ;\n");
	HSSend (handle, "function sesettime() {\n");
	HSSend (handle, "var i,Ot;\n");
	HSSend (handle, "Ot = document.formsi.time;\n");
	HSSend (handle, "for ( i = 0; i < Ot.options.length; i++ )\n");
	HSSend (handle, "{if ( time == Ot.options[i].value ){Ot.selectedIndex = i; break;} }\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function myreset(formObj) {\n");
	HSSend (handle, "formObj.reset();sesetunit();sesettime();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function mysubmit(formObj) {\n");
	HSSend (handle, "formObj.submit();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Configuration: System Info\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<form name=formsi action=info_1 method=POST>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table width=\"95%\" border=\"0\" rules=\"none\" cellPadding=\"6\">\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" style=\"");
	na_GRVIS (handle);
	HSSend (handle, "\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<thead><tr><td colspan=2><b>Group Name</b></td></tr></thead>\n");
	HSSend (handle, "<tr><td><input type=TEXT size=20 maxlength=15 name=grname value=\"");
	na_GRNAME (handle);
	HSSend (handle, "\"></td><td>If Group Name contains no blanks, its name may be used to symbolically address the group over the LAN.</td></tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");

	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\"><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center>\n");
	HSSend (handle, "<thead><tr><td align=left colspan=12><b>Bar Codes</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>");
	na_BARCODE_TABLE (handle);
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "<tfoot><tr><td colspan=12><b>NOTE:</b> Narrow bar width is in mils (thousandths of an inch). All other values are in dots/pixels.</td></tr></tfoot>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");

	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\"><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center>\n");
	HSSend (handle, "<thead><tr><td align=left colspan=12><b>DataMatrix parameters</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>");
	na_DATAMATRIX_TABLE (handle);
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");

	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<colgroup span=\"2\"></colgroup><colgroup span=\"2\"></colgroup>\n");
	HSSend (handle, "<thead><tr><td colspan=2><b>System Units</b></td><td colspan=2><b>Ink cost</b></td></tr></thead>\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr><td width=25%>Time Displays</td><td width=25%><select size=1 name=time><option value=24>24 hour</option><option value=12>12 hour</option><option value=12M>12 hour with AM/PM</option></select></td>\n");
	HSSend (handle, "<td width=25%>Ink bottle size in milliliters</td><td width=25%><input type=text name=size size=6 value=\"");
	na_INK_SIZE (handle);
	HSSend (handle, "\"></td></tr>\n");
	HSSend (handle, "<tr><td width=25%>Units</td><td width=25%><select size=1 name=units><option value=i>Inches</option><option value=f>Feet</option><option value=mm>Millimeters</option><option value=cm>Centimeters</option><option value=m>Meters</option></select></td>\n");
	HSSend (handle, "<td width=25%>Ink bottle cost</td><td width=25%><input type=text name=cost size=6 value=\"");
	na_INK_COST (handle);
	HSSend (handle, "\"></td></tr></tbody>\n");
	HSSend (handle, "<tfoot><tr><td colspan=2 align=center whith=50%><b>NOTE:</b> These units are the default units used on other screens.</td><td colspan=2>&nbsp;</td></tr></tfoot>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">sesetunit();sesettime();</SCRIPT>\n");
	HSSend (handle, "<table width=\"100%\" border=1 cellpadding=\"2\" rules=groups bgcolor=\"#f5f5f5\"><colgroup><col align=left></colgroup><colgroup><col align=right></colgroup><colgroup><col align=left></colgroup><thead>\n");
	HSSend (handle, "<tr><td colspan=3><b>Clock Source</b></td></tr></thead>\n");
	HSSend (handle, "<tbody><tr>\n");
	HSSend (handle, "<td><input type=radio name=tod ");
	na_TODint (handle);
	HSSend (handle, " value=int>Use Internal Clock</td>\n");
	HSSend (handle, "<td colspan=\"2\" align=center>\n");
	HSSend (handle, "<table border=\"0\" cellpadding=3><tr>\n");
	HSSend (handle, "<td>Year&nbsp;<input type=\"text\" size=\"4\" onchange=\"return parmCheck(this,2000,2099,'Year');\" name=\"tmyear\" value=\"");
	na_TMYEAR (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td>Month&nbsp;<input type=\"text\" size=\"2\" onchange=\"return parmCheck(this,1,12,'Month');\" name=\"tmmonth\" value=\"");
	na_TMMONTH (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td>Day&nbsp;<input type=\"text\" size=\"2\" onchange=\"return parmCheck(this,1,31,'Day');\" name=\"tmday\" value=\"");
	na_TMDAY (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td>Hour&nbsp;<input type=\"text\" size=\"2\" onchange=\"return parmCheck(this,0,23,'Hour');\" name=\"tmhour\" value=\"");
	na_TMHOUR (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td>Minute&nbsp;<input type=\"text\" size=\"2\" onchange=\"return parmCheck(this,0,59,'Minute');\" name=\"tmmin\" value=\"");
	na_TMMINUTE (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td>Second&nbsp;<input type=\"text\" size=\"2\" onchange=\"return parmCheck(this,0,59,'Second');\" name=\"tmsec\" value=\"");
	na_TMSECOND (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr></table>\n");
	HSSend (handle, "</td></tr></tbody><tbody>\n");
	HSSend (handle, "<tr><td rowspan=3><input type=radio name=tod ");
	na_TODext (handle);
	HSSend (handle, " value=ext>External Time Source</td><td width=5%>");
	na_TMUDP13INUSE (handle);
	HSSend (handle, "</td><td>Local UDP13</td></tr>\n");
	HSSend (handle, "<tr><td width=5%>");
	na_TMTCP13INUSE (handle);
	HSSend (handle, "</td><td>TCP13</td></tr>\n");
	HSSend (handle, "<tr><td width=5%>");
	na_TMIPINUSE (handle);
	HSSend (handle, "</td><td>Other Printer</td></tr></tbody><tbody>\n");
	HSSend (handle, "<tr><td colspan=3 align=center><table border=\"0\" cellpadding=3<tr><td>Local Standard Time difference from UTC</td><td><input type=TEXT size=8 onchange=\"return TZCheck(this);\" name=tmtzn value=\"");
	na_TMTZN (handle);
	HSSend (handle, "\"></td></tr></table></td></tr></tbody><tbody>\n");
	HSSend (handle, "<tr><td colspan=3 align=center>Observe USA DST<input type=checkbox name=tmdstusa ");
	na_TMDSTUSA (handle);
	HSSend (handle, " value=ON></td></tr></tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "<tr><td align=center>\n");
	HSSend (handle, "<input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, "><button class=fjbtn1 onClick=\"mysubmit(this.form)\" ID=\"Button1\">Submit</button><button class=fjbtn1 onClick=\"myreset(this.form)\" ID=\"Button2\">Reset</button>\n");
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</html>");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\systeminfo.htm }} */
/* @! End HTML page   */

void reload_sysinfo(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=systeminfo.htm");
	}
	else
	{
		fj_reload_page(handle, "systeminfo.htm");
	}
}
// first function called in page must be na_TMYEAR so this gets set once and is then reused for other than year
static struct tm fj_time_TM;

/* @! Begin of post function */
/* @! Function: Post_info_1  {{ */
void Post_info_1(unsigned long handle)
{
	LPFJ_GLOBAL pGlobal = pCVT->pFJglobal;
	LPFJ_PARAM pParam = pCVT->pFJparam;
	LPFJSYSTEM pfsysP = &(pParam->FJSys);
	LPFJSYSTEM pfsys  = pCVT->pfsys;
	LPFJSYSBARCODE  pfsysBC;
	LPFJSYSBARCODE  pfsysBCP;
	LPFJSYSDATAMATRIX  pfsysDM;
	LPFJSYSDATAMATRIX  pfsysDMP;
	char   FieldName[20];
	char   FieldNumber[4];
	int    retrom;
	char   formField[20];
	BOOL   bROMWait;
	BOOL   bChanged;
	BOOL   bValue;
	LONG   lValue;
	FLOAT  fSize;
	FLOAT  fCost;
	enum FJSYSUNITS   lUnits;
	enum FJSYSTIMEDIS lTimeDisplay;
	int   ret;
	int   i;
	BOOL   bTOD;
	BOOL   bDSTUSA;
	struct tm tm;
	FLOAT  fTZ;
	LONG   lTZseconds;

	bROMWait = FALSE;
	// get ROM structure
	pCVT->tx_semaphore_get( &(pCVT->pFJglobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	// prime TM structure with current clock
	pCVT->fj_SystemGetTime( &tm );

	bChanged = FALSE;
	if ( 0 == retrom  )
	{
		// Start - clock code
		bDSTUSA    = pfsysP->bDSTUSA;
		bTOD       = pfsysP->bInternalClock;
		lTZseconds = pfsysP->lTimeZoneSeconds;

		ret = fj_HSGetValue( handle, "tod", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0 == strcmp(formField, "int") ) bTOD = TRUE;
			else                                 bTOD = FALSE;
		}
		ret = fj_HSGetValue( handle, "tmyear", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret > 2000) && (ret < 2099) )
			{
				tm.tm_year = ret - 1900;
			}
		}
		ret = fj_HSGetValue( handle, "tmmonth", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret >= 1) && (ret <= 12) )
			{
				tm.tm_mon = ret - 1;
			}
		}
		ret = fj_HSGetValue( handle, "tmday", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret >= 1) && (ret <= 31) )
			{
				tm.tm_mday = ret;
			}
		}
		ret = fj_HSGetValue( handle, "tmhour", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret >= 0) && (ret <= 23) )
			{
				tm.tm_hour = ret;
			}
		}
		ret = fj_HSGetValue( handle, "tmmin", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret >= 0) && (ret <= 59) )
			{
				tm.tm_min = ret;
			}
		}
		ret = fj_HSGetValue( handle, "tmsec", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			ret = atoi(formField);
			if ( (ret >= 0) && (ret <= 59) )
			{
				tm.tm_sec = ret;
			}
		}

		ret = fj_HSGetValue( handle, "tmtzn", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fTZ = atof(formField);
			if ( (fTZ >= -12.0) && (fTZ <= 12.0) )
			{
				lTZseconds = (LONG)(fTZ * 3600.0);
			}
		}

		if ( lTZseconds != pfsys->lTimeZoneSeconds )
		{
			pfsysP->lTimeZoneSeconds = lTZseconds;
			bChanged = TRUE;
		}

		if ( bTOD  != pfsys->bInternalClock )
		{
			pfsysP->bInternalClock = bTOD;
			bChanged = TRUE;
		}

		// checkbox only returns a value if it is CHECKED
		ret = fj_HSGetValue( handle, "tmdstusa", formField, sizeof(formField) );
		if ( 0 < ret ) bDSTUSA = TRUE;
		else           bDSTUSA = FALSE;

		if ( bDSTUSA != pfsys->bDSTUSA )
		{
			pfsysP->bDSTUSA = bDSTUSA;
			bChanged = TRUE;
		}
		// End - clock code
		ret = fj_HSGetValue( handle, "grname", formField, FJSYS_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			if ( 0 != strcmp( pfsysP->strID, formField ) )
			{
				strcpy( pfsysP->strID, formField );
				bChanged = TRUE;
			}
		}

		for ( i = 0; i < FJSYS_BARCODES; i++ )
		{
			pfsysBCP = &(pfsysP->bcArray[i]);
			sprintf( FieldNumber, "%i", (int)i );

			strcpy( FieldName, "type" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				lValue |= pfsysBCP->lBCFlags & ~BARCODE_ENCODING_MASK;
				if ( lValue != pfsysBCP->lBCFlags )
				{
					pfsysBCP->lBCFlags = lValue;
					bChanged = TRUE;
				}
			}

			strcpy( FieldName, "hgt" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBCHeight )
				{
					pfsysBCP->lBCHeight = lValue;
					bChanged = TRUE;
				}
			}

			strcpy( FieldName, "cd" );
			strcat( FieldName, FieldNumber );
			// checkbox only returns a value if it is CHECKED
			bValue = FALSE;
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret ) bValue = TRUE;
			if ( bValue != pfsysBCP->bCheckDigit )
			{
				pfsysBCP->bCheckDigit = bValue;
				bChanged = TRUE;
			}

			strcpy( FieldName, "hbr" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lHor_Bearer )
				{
					pfsysBCP->lHor_Bearer = lValue;
					bChanged = TRUE;
				}
			}

			strcpy( FieldName, "vbr" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lVer_Bearer )
				{
					pfsysBCP->lVer_Bearer = lValue;
					bChanged = TRUE;
				}
			}

			strcpy( FieldName, "qz" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lQuietZone )
				{
					pfsysBCP->lQuietZone = lValue;
					bChanged = TRUE;
				}
			}
			strcpy( FieldName, "base" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBarVal[0] )
				{
					pfsysBCP->lBarVal[0] = lValue;
					bChanged = TRUE;
				}
			}
			strcpy( FieldName, "b1" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBarVal[1] )
				{
					pfsysBCP->lBarVal[1] = lValue;
					bChanged = TRUE;
				}
			}
			strcpy( FieldName, "b2" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBarVal[2] )
				{
					pfsysBCP->lBarVal[2] = lValue;
					bChanged = TRUE;
				}
			}
			strcpy( FieldName, "b3" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBarVal[3] )
				{
					pfsysBCP->lBarVal[3] = lValue;
					bChanged = TRUE;
				}
			}
			strcpy( FieldName, "b4" );
			strcat( FieldName, FieldNumber );
			ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
			if ( 0 < ret )
			{
				lValue = atoi( formField );
				if ( lValue != pfsysBCP->lBarVal[4] )
				{
					pfsysBCP->lBarVal[4] = lValue;
					bChanged = TRUE;
				}
			}

		}

		for ( i = 0; i < FJSYS_BARCODES; i++ )
		{
			pfsysDMP = &(pfsysP->bcDataMatrixArray[i]);
			sprintf( FieldNumber, "%i", (int)i );

			strcpy (FieldName, "dmcy"); strcat(FieldName, FieldNumber);
			if (fj_HSGetValue (handle, FieldName, formField, sizeof (formField)) > 0) {
				lValue = atoi (formField);
				if (lValue != pfsysDM->lHeight) {
				    pfsysDMP->lHeight = lValue;
				    bChanged = TRUE;
				}
			}

			strcpy (FieldName, "dmcyb"); strcat(FieldName, FieldNumber);
			if (fj_HSGetValue (handle, FieldName, formField, sizeof (formField)) > 0) {
				lValue = atoi (formField);
				if (lValue != pfsysDM->lHeightBold) {
				    pfsysDMP->lHeightBold = lValue;
				    bChanged = TRUE;
				}
			}

			strcpy (FieldName, "dmcx"); strcat(FieldName, FieldNumber);
			if (fj_HSGetValue (handle, FieldName, formField, sizeof (formField)) > 0) {
				lValue = atoi (formField);
				if (lValue != pfsysDM->lWidth) {
				    pfsysDMP->lWidth = lValue;
				    bChanged = TRUE;
				}
			}

			strcpy (FieldName, "dmcxb"); strcat(FieldName, FieldNumber);
			if (fj_HSGetValue (handle, FieldName, formField, sizeof (formField)) > 0) {
				lValue = atoi (formField);
				if (lValue != pfsysDM->lWidthBold) {
				    pfsysDMP->lWidthBold = lValue;
				    bChanged = TRUE;
				}
			}
		}
		
		fSize        = pfsysP->fInkBottleSize;
		fCost        = pfsysP->fInkBottleCost;
		lUnits       = pfsysP->lUnits;
		lTimeDisplay = pfsysP->lTimeDisplay;

		ret = fj_HSGetValue( handle, "time", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if( 0 == strcmp("24",  formField) ) lTimeDisplay = FJSYS_TIMEDIS_24;
			if( 0 == strcmp("12",  formField) ) lTimeDisplay = FJSYS_TIMEDIS_12;
			if( 0 == strcmp("12M", formField) ) lTimeDisplay = FJSYS_TIMEDIS_12M;
		}
		ret = fj_HSGetValue( handle, "units", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if( 0 == strcmp("f", formField) ) lUnits = FJSYS_UNITS_ENGLISH_FEET;
			if( 0 == strcmp("i", formField) ) lUnits = FJSYS_UNITS_ENGLISH_INCHES;
			if( 0 == strcmp("m", formField) ) lUnits = FJSYS_UNITS_METRIC_METERS;
			if( 0 == strcmp("cm",formField) ) lUnits = FJSYS_UNITS_METRIC_CM;
			if( 0 == strcmp("mm",formField) ) lUnits = FJSYS_UNITS_METRIC_MM;
		}
		ret = fj_HSGetValue( handle, "cost", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fCost = atof( formField );
		}
		ret = fj_HSGetValue( handle, "size", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			fSize = atof( formField );
		}

		if ( fCost  != pfsysP->fInkBottleCost )
		{
			pfsysP->fInkBottleCost = fCost;
			bChanged = TRUE;
		}
		if ( fSize  != pfsysP->fInkBottleSize )
		{
			pfsysP->fInkBottleSize = fSize;
			bChanged = TRUE;
		}
		if ( lUnits != pfsysP->lUnits )
		{
			pfsysP->lUnits = lUnits;
			bChanged = TRUE;
		}
		if ( lTimeDisplay != pfsysP->lTimeDisplay )
		{
			pfsysP->lTimeDisplay = lTimeDisplay;
			bChanged = TRUE;
		}
	}

	// write ROM structure
	if ( TRUE == bChanged )
	{
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		retrom = 0;
		if ( 0 == retrom )
		{
			pfsys->lUnits           = pfsysP->lUnits;
			pfsys->lTimeDisplay     = pfsysP->lTimeDisplay;
			pfsys->fInkBottleCost   = pfsysP->fInkBottleCost;
			pfsys->fInkBottleSize   = pfsysP->fInkBottleSize;

			pfsys->bInternalClock   = pfsysP->bInternalClock;
			pfsys->lTimeZoneSeconds = pfsysP->lTimeZoneSeconds;
			pfsys->bDSTUSA          = pfsysP->bDSTUSA;

			strcpy( pfsys->strID, pfsysP->strID );
			pCVT->fj_setGroupID();

			for ( i = 0; i < FJSYS_BARCODES; i++ )
			{
				pfsysBCP = &(pfsysP->bcArray[i]);
				pfsysBC  = &(pfsys->bcArray[i]);
				pfsysBC->lBCFlags    = pfsysBCP->lBCFlags;
				pfsysBC->lBCHeight   = pfsysBCP->lBCHeight;
				pfsysBC->bCheckDigit = pfsysBCP->bCheckDigit;
				pfsysBC->lHor_Bearer = pfsysBCP->lHor_Bearer;
				pfsysBC->lVer_Bearer = pfsysBCP->lVer_Bearer;
				pfsysBC->lQuietZone  = pfsysBCP->lQuietZone;
				pfsysBC->lBarVal[0]  = pfsysBCP->lBarVal[0];
				pfsysBC->lBarVal[1]  = pfsysBCP->lBarVal[1];
				pfsysBC->lBarVal[2]  = pfsysBCP->lBarVal[2];
				pfsysBC->lBarVal[3]  = pfsysBCP->lBarVal[3];
				pfsysBC->lBarVal[4]  = pfsysBCP->lBarVal[4];
			}

			for ( i = 0; i < FJSYS_BARCODES; i++ )
			{
				pfsysDMP 		= &(pfsysP->bcDataMatrixArray[i]);
				pfsysDM  		= &(pfsys->bcDataMatrixArray[i]);
				pfsysDM->lHeight   	= pfsysDMP->lHeight;
				pfsysDM->lHeightBold   	= pfsysDMP->lHeightBold;
				pfsysDM->lWidth   	= pfsysDMP->lWidth;
				pfsysDM->lWidthBold   	= pfsysDMP->lWidthBold;
			}
			
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			pCVT->fj_SystemBuildSelectedLabel( pCVT->pfsys, pCVT->pBRAM->strLabelName );
			bROMWait = TRUE;
		}
	}
	else
	{
		pCVT->tx_semaphore_put( &(pCVT->pFJglobal->semaROMLock) );
	}
	if ( TRUE == pfsys->bInternalClock )
	{
		BOOL bDST = pfsys->bDSTUSA;
		tm.tm_isdst = 0;
		if ( TRUE == bDST )
		{
			pfsys->bDSTUSA = FALSE;
			//tm.tm_hour--;										 // change to local STANDARD time
			pCVT->fj_mktime( &tm );
			pfsys->bDSTUSA = bDST;
		}
		pCVT->fj_print_head_SetRTClockTM( &tm );
		memcpy( &(pfsys->tmPhotoCell), &tm, sizeof(tm) );
	}
	else
	{
		pCVT->tx_thread_resume( &(pGlobal->thrTime) );
	}
	reload_sysinfo(handle,bROMWait);
}										/* @! Function: Post_info_1  }} */
/* @! Function: Post_time_1  {{ */
void Post_time_1(unsigned long handle)
{
}										/* @! Function: Post_time_1  }} */
char * fj_post_find_number( char* pstr)
{
	char *pRet = NULL;

	while ( 0 != *pstr )
	{
		if ( 0 != isdigit(*pstr) )
		{
			pRet = pstr;
			break;
		}
		pstr++;
	}
	return pRet;
}
int Post_find_time( char* pstr)
{
	int iMinutes;
	int iHours;

	iMinutes = 0;
	iHours = 0;
	pstr = fj_post_find_number( pstr );
	if ( NULL != pstr )
	{
		while ( 0 != isdigit(*pstr) )
		{
			iMinutes = (10 * iMinutes) + *pstr - '0';
			pstr++;
		}
		pstr = fj_post_find_number( pstr );
		if ( NULL != pstr )
		{
			iHours = iMinutes;
			iMinutes = 0;
			while ( 0 != isdigit(*pstr) )
			{
				iMinutes = (10 * iMinutes) + *pstr - '0';
				pstr++;
			}
		}
	}

	return (iMinutes + iHours*60);
}										/* @! Function: Post_time_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_ID  {{ */
/* @! Function: na_UNITS_DEF  {{ */
void na_UNITS_DEF(unsigned long handle)
{
	/* add your implementation here: */
	if      ( FJSYS_UNITS_ENGLISH_FEET   == pCVT->pfsys->lUnits ) HSSend (handle, "f");
	else if ( FJSYS_UNITS_ENGLISH_INCHES == pCVT->pfsys->lUnits ) HSSend (handle, "i");
	else if ( FJSYS_UNITS_METRIC_METERS  == pCVT->pfsys->lUnits ) HSSend (handle, "m");
	else if ( FJSYS_UNITS_METRIC_CM      == pCVT->pfsys->lUnits ) HSSend (handle, "cm");
	else if ( FJSYS_UNITS_METRIC_MM      == pCVT->pfsys->lUnits ) HSSend (handle, "mm");
}										/* @! Function: na_UNITS_DEF  }} */
/* @! Function: na_INK_SIZE  {{ */
void na_INK_SIZE(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fInkBottleSize, 2 );
}										/* @! Function: na_INK_SIZE  }} */
/* @! Function: na_INK_COST  {{ */
void na_INK_COST(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fInkBottleCost, 2 );
}										/* @! Function: na_INK_COST  }} */
/* @! Function: na_TITLE_CF_SYSTEMINFO  {{ */
void na_TITLE_CF_SYSTEMINFO(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "System Information" );
}										/* @! Function: na_TITLE_CF_SYSTEMINFO  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_TIME_DEF  {{ */
void na_TIME_DEF(unsigned long handle)
{
	/* add your implementation here: */
	if      ( FJSYS_TIMEDIS_24  == pCVT->pfsys->lTimeDisplay ) HSSend (handle, "24");
	else if ( FJSYS_TIMEDIS_12  == pCVT->pfsys->lTimeDisplay ) HSSend (handle, "12");
	else if ( FJSYS_TIMEDIS_12M == pCVT->pfsys->lTimeDisplay ) HSSend (handle, "12M");
}										/* @! Function: na_TIME_DEF  }} */
/* @! Function: na_GRVIS  {{ */
void na_GRVIS(unsigned long handle)
{
	/* add your implementation here: */
	if ( 0 != pCVT->pfph->lGroupNumber ) HSSend (handle, "visibility:hidden");
}										/* @! Function: na_GRVIS  }} */
typedef struct fjsysbct
{
	CHAR   cText[32];
	LONG   lCode;
} FJSYSBCT, FAR *LPFJSYSBCT, FAR * const CLPFJSYSBCT;
typedef const struct fjsysbct FAR *LPCFJSYSBCT, FAR * const CLPCFJSYSBCT;
const FJSYSBCT fj_SysBCTable[] =
{
	"I 2 of 5", BARCODE_I25,			/* interleaved 2 of 5 (only digits) */
	"C128",     BARCODE_128,			/* code 128 (a,b,c: autoselection) */
	"C39",      BARCODE_39,				/* code 39 */
	"UPC",      BARCODE_UPC,			/* upc == 12-digit ean */
	"EAN",      BARCODE_EAN,
	"EAN128",   BARCODE_EAN128,			/* ean 128 // Burl */
};

/* @! Function: na_BARCODE_TABLE  {{ */

void na_BARCODE_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM      pfsys    = pCVT->pfsys;
	LPFJSYSBARCODE  pfsysBC;
	LONG lCode;
	int  i, j;

	HSSend (handle, "<tr><td>Name</td><td>BarCode Type</td><td>Height</td><td>Check Digit</td><td>Horz. Bearer</td><td>Vert. Bearer</td><td>Quiet Zone</td><td>Narrow Bar</td><td>Bleed 1</td><td>Bleed 2</td><td>Bleed 3</td><td>Bleed 4</td></tr>\n");
	for ( i = 0; i < FJSYS_BARCODES; i ++ )
	{
		pfsysBC = &(pfsys->bcArray[i]);
		HSSend (handle, "<tr><td>");
		na_String(handle, pfsysBC->strName );
		HSSend (handle, "</td><td>");

		HSSend (handle, "<select size=1 name=type");
		na_Long(handle, i);
		HSSend(handle, ">");
		lCode = pfsysBC->lBCFlags & BARCODE_ENCODING_MASK;
		for ( j=0; j<(sizeof(fj_SysBCTable)/sizeof(FJSYSBCT)); j++ )
		{
			HSSend(handle, "<option value=");
			na_Long(handle, fj_SysBCTable[j].lCode);
			if ( lCode == fj_SysBCTable[j].lCode ) HSSend(handle, " SELECTED");
			HSSend(handle, ">");
			HSSend(handle, fj_SysBCTable[j].cText);
			HSSend(handle, "</option>");
		}
		HSSend(handle, "</select>");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=hgt");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBCHeight );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=CHECKBOX name=cd");
		na_Long(handle, i);
		if( TRUE == pfsysBC->bCheckDigit ) HSSend (handle, " CHECKED");
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=hbr");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lHor_Bearer );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=vbr");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lVer_Bearer );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=qz");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lQuietZone );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=base");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBarVal[0] );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=b1");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBarVal[1] );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=b2");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBarVal[2] );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=b3");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBarVal[3] );
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=2 maxlength=2 name=b4");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lBarVal[4] );
		HSSend(handle, ">");
		HSSend (handle, "</td></tr>\n");
	}
}										/* @! Function: na_BARCODE_TABLE  }} */

void na_DATAMATRIX_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM      pfsys    = pCVT->pfsys;
	LPFJSYSDATAMATRIX  pfsysBC;
	LONG lCode;
	int  i, j;

	HSSend (handle, "<tr><td>Name</td> <td>Height</td> <td>Height (bold)</td> <td>Width</td> <td>Width (bold)</td> </tr>\n");
	for ( i = 0; i < FJSYS_BARCODES; i ++ )
	{
		pfsysBC = &(pfsys->bcDataMatrixArray[i]);
		HSSend (handle, "<tr><td>");
		na_String(handle, pfsysBC->strName);
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=dmcy");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lHeight);
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=dmcyb");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lHeightBold);
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=dmcx");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lWidth);
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

		HSSend (handle, "<input type=TEXT size=3 maxlength=3 name=dmcxb");
		na_Long(handle, i);
		HSSend (handle, " value=");
		na_Long(handle, pfsysBC->lWidthBold);
		HSSend(handle, ">");
		HSSend (handle, "</td><td>");

	}
}										/* @! Function: na_BARCODE_TABLE  }} */

/* @! Function: na_TODint  {{ */
void na_TODint(unsigned long handle)
{
	/* add your implementation here: */
	if( TRUE == pCVT->pfsys->bInternalClock ) HSSend(handle, "CHECKED" );
}										/* @! Function: na_TODint  }} */
/* @! Function: na_TMYEAR  {{ */
void na_TMYEAR(unsigned long handle)
{
	/* add your implementation here: */
	pCVT->fj_SystemGetTime( &fj_time_TM );
	na_Long(handle, fj_time_TM.tm_year + 1900);
}										/* @! Function: na_TMYEAR  }} */
/* @! Function: na_TMMONTH  {{ */
void na_TMMONTH(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, fj_time_TM.tm_mon + 1);
}										/* @! Function: na_TMMONTH  }} */
/* @! Function: na_TMDAY  {{ */
void na_TMDAY(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, fj_time_TM.tm_mday);
}										/* @! Function: na_TMDAY  }} */
/* @! Function: na_TMHOUR  {{ */
void na_TMHOUR(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, fj_time_TM.tm_hour);
}										/* @! Function: na_TMHOUR  }} */
/* @! Function: na_TMMINUTE  {{ */
void na_TMMINUTE(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, fj_time_TM.tm_min);
}										/* @! Function: na_TMMINUTE  }} */
/* @! Function: na_TMSECOND  {{ */
void na_TMSECOND(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, fj_time_TM.tm_sec);
}										/* @! Function: na_TMSECOND  }} */
/* @! Function: na_TODext  {{ */
void na_TODext(unsigned long handle)
{
	/* add your implementation here: */
	if( FALSE == pCVT->pfsys->bInternalClock ) HSSend(handle, "CHECKED" );
}										/* @! Function: na_TODext  }} */
/* @! Function: na_TMUDP13INUSE  {{ */
void na_TMUDP13INUSE(unsigned long handle)
{
	/* add your implementation here: */
	STRING str[20];
	if( (FALSE == pCVT->pfsys->bInternalClock) && (0 != pCVT->pFJglobal->ipTimeUDP) )
	{
		pCVT->fj_IPToStr( pCVT->pFJglobal->ipTimeUDP, str );
		HSSend(handle, str );
	}
	else
	{
		HSSend(handle, "&nbsp;" );
	}
}										/* @! Function: na_TMUDP13INUSE  }} */
/* @! Function: na_TMTCP13INUSE  {{ */
void na_TMTCP13INUSE(unsigned long handle)
{
	/* add your implementation here: */
	STRING str[20];
	if( (FALSE == pCVT->pfsys->bInternalClock) && (0 != pCVT->pFJglobal->ipTimeTCP) )
	{
		pCVT->fj_IPToStr( pCVT->pFJglobal->ipTimeTCP, str );
		HSSend(handle, str );
	}
	else
	{
		HSSend(handle, "&nbsp;" );
	}
}										/* @! Function: na_TMTCP13INUSE  }} */
/* @! Function: na_TMIPINUSE  {{ */
void na_TMIPINUSE(unsigned long handle)
{
	/* add your implementation here: */
	STRING str[20];
	if( (FALSE == pCVT->pfsys->bInternalClock) && (0 != pCVT->pFJglobal->ipTimeIP) )
	{
		pCVT->fj_IPToStr( pCVT->pFJglobal->ipTimeIP, str );
		HSSend(handle, str );
	}
	else
	{
		HSSend(handle, "&nbsp;" );
	}
}										/* @! Function: na_TMIPINUSE  }} */
/* @! Function: na_TMTZN  {{ */
void na_TMTZN(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, (DOUBLE)pCVT->pfsys->lTimeZoneSeconds/3600.0, 2);
}										/* @! Function: na_TMTZN  }} */
/* @! Function: na_TMDSTUSA  {{ */
void na_TMDSTUSA(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pfsys->bDSTUSA )HSSend(handle, "CHECKED" );
}										/* @! Function: na_TMDSTUSA  }} */
/* @! End of dynamic functions   */
