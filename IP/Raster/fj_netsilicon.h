#ifndef FJ_NETSILICON_H
#define FJ_NETSILICON_H

// we must include stdlib.h before any NetSilicon includes.
// c:\NETOS4\H\tcpip\std.h contains its definition for abs.
#include <stdlib.h>

// we must have LITTLE_ENDIAN and BIG_ENDIAN correct, and
// we must include Ncc_io.h before anything else.
// becasue ipport.h screws up by defining both LITTLE_ENDIAN and BIG_ENDIAN.
// ipport.h is included directly and indirectly in a lot of other headers.
// Ncc_io.h must have LITTLE_ENDIAN undefined to define the hardware registers correctly.
#ifdef LITTLE_ENDIAN
#undef LITTLE_ENDIAN					// make sure LITTLE_ENDIAN is not defined
#endif
#include <Ncc_io.h>
// we used to use narm_io.h and it was removed in NET+OS 2.0.
// used the NARM_??? definitions to modifiy the hardware registers.
// these are modified from the old narm_io.h.
#define NARM_EFE (volatile NCC_IO_EFE *)EFE_BASE
#define NARM_DMA (volatile NCC_IO_DMA *)DMA_BASE
#define NARM_PC  (volatile NCC_IO_PC  *)PC_BASE
#define NARM_GEN (volatile NCC_IO_GEN *)GEN_BASE
#define NARM_MEM (volatile NCC_IO_MEM *)MEM_BASE
#define NARM_SER (volatile NCC_IO_SER *)SER_BASE

#include <bspconf.h>					// includes tx_api.h which includes tx_port.h which has several typedefs that we need
//#include <Nptypes.h>		// needed to define types for socket.h, net.h, ????
#include <Npttypes.h>
#include <socket.h>
#include <sockapi.h>
//#include <nptcp.h>		// needed for sockaddr_in
#include <narmsrln.h>					// for NETOS ROM parameters
typedef  devBoardParamsType        FJ_NETOSPARAM;
typedef  devBoardParamsType     * PFJ_NETOSPARAM;
typedef  devBoardParamsType     *LPFJ_NETOSPARAM;
void AppSearchURL (unsigned long, char*);
void AppPreprocessURL (unsigned long, char*);

#ifdef TRUE
#undef TRUE
#endif
#ifdef FALSE
#undef FALSE
#endif
#define FALSE 0
#define TRUE (!(FALSE))

#ifdef NULL
#undef NULL
#endif
#ifndef NULL							// the real definition of null
#ifdef  __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#ifdef LITTLE_ENDIAN
#undef LITTLE_ENDIAN					// make sure LITTLE_ENDIAN is not defined. bug from ipport.h
#endif

#ifdef putchar
#undef putchar							// bug from ipport.h
#endif
#define putchar(ch) putc((ch),stdout)
#endif FJ_NETSILICON_H
