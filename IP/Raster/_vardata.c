/* C file: C:\MyPrograms\IP_APP_v01\_vardata.c created
 by NET+Works HTML to C Utility on Wednesday, June 16, 2004 10:30:06 */
#include "fj.h"
#include "fj_misc.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_TITLE_VARDATA */
static void na_TITLE_VARDATA (unsigned long handle);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_VARDATA_TABLE */
static void na_VARDATA_TABLE (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\vardata.htm {{ */
void Send_function_35(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_VARDATA (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Variable Data\"\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "<script language=\"JavaScript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "var of=document.vardata1;\n");
	HSSend (handle, "of.action.value=sa; of.item.value=si;\n");
	HSSend (handle, "of.submit(); }\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<form name=\"vardata1\" action=\"vardata_input\" method=\"post\">\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"4\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "");
	na_VARDATA_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" value=\"none\" name=\"action\"><input type=\"hidden\" value=\"none\" name=\"item\">\n");
	HSSend (handle, "<input class=\"fjbtn1\" type=\"submit\" value=\"Update\"><input class=\"fjbtn1\" type=\"reset\" value=\"Reset\"></center>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\vardata.htm }} */
/* @! End HTML page   */

/* @! Begin of post function */
/* @! Function: Post_vardata_input  {{ */
void Post_vardata_input(unsigned long handle)
{
	LPFJ_BRAM   pBRAM   = pCVT->pBRAM;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPDYNSEGMENT    DynSeg;
	char        formField[FJTEXT_SIZE+1];
	char        FieldName[20];
	char        FieldNumber[4];
	LONG        lValue;
	int         ret;
	int         i;

	ret = fj_HSGetValue( handle, "action", formField, FJTEXT_SIZE+1 );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"select"))
		{
			ret = fj_HSGetValue( handle, "item", formField, FJTEXT_SIZE+1 );
			if ( 0 < ret )
			{
				pfph->lSerialVarDataID = atoi(formField);
				pfph->lSerialVarDataID--;
			}
		}
	}

	for ( i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++ )
	{
		sprintf( FieldNumber, "%i", (int)i );

		strcpy( FieldName, "dynseg" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJTEXT_SIZE+1 );
		if ( 0 < ret )
		{
			DynSeg = &(pBRAM->DynamicSegments[i]);
			strcpy(DynSeg->strDynData,formField);
			DynSeg->bChanged = TRUE;
		}
	}

	fj_reload_page(handle, "vardata.htm");
}										/* @! Function: Post_vardata_input  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_PWID  {{ */
/* @! Function: na_TITLE_VARDATA  {{ */
void na_TITLE_VARDATA(unsigned long handle)
{
	na_TITLE_ALL( handle, "Variable Data" );
}										/* @! Function: na_TITLE_VARDATA  }} */
/* @! Function: na_VARDATA_TABLE  {{ */
void na_VARDATA_TABLE(unsigned long handle)
{
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	//DYNSEGMENT DynSeg;
	int i;

	HSSend(handle, "<thead><tr>");
	if ( TRUE == pfph->bSerialVarData ) HSSend(handle, "<td>&nbsp;</td>");
	HSSend(handle, "<td>&nbsp;</td><td align=center><b>Variable Data</b></td>");
	HSSend(handle, "</tr></thead><tbody>\n");
	for ( i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++ )
	{
		//DynSeg = pBRAM->DynamicSegments[i];

		HSSend(handle, "<tr>");
		if ( TRUE == pfph->bSerialVarData )
		{
			HSSend(handle, "<td align=center>");
			if ( i == pfph->lSerialVarDataID )
			{
				HSSend(handle, "Selected</td>");
			}
			else
			{
				HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"select\",\"");
				na_Long(handle, i+1);
				HSSend(handle, "\")'>Select</button></td>");
			}
		}
		HSSend(handle, "<td align=right>");
		na_Long(handle, i+1);
		HSSend(handle, "</td><td align=center><input type=text size=60 maxlength=255 name=dynseg");
		na_Long(handle, i);
		HSSend(handle, " value=\"");
		na_String(handle, pBRAM->DynamicSegments [i].strDynData);//DynSeg.strDynData);
		HSSend(handle, "\"></td></tr>\n");
	}
	HSSend(handle, "</tbody>\n");

}										/* @! Function: na_VARDATA_TABLE  }} */
/* @! End of dynamic functions   */
