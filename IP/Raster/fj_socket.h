#if !defined (_FJ_SOCKET_H_)
#define _FJ_SOCKET_H_

#if defined (_WIN32)
#define WORD16 unsigned __int16
#define WORD32 unsigned __int32
#endif

#define _FJ_SOCKET_NUMBER       1281	// socket/port number for tcp/ip communications
#define _FJ_SOCKET_BUFFER_SIZE  ((13+4) * 200) //1400	// maximum text Data size in _FJ_SOCKET_MESSAGE structure.
// larger messages can be transmitted.
// fonts and bmps are much larger.

//
// Any value that might contain user input, will be sent as a quoted("") string.
// These include all names/ids (system, label, message, etc.), and all user text for elements such as Text and Barcode.
// Additional parameters will be quoted. An attempt has been made to show the quoted values below.
//
// The input processing of a quoted string is as follows -
// If the first character is not a double quote, do nothing.
// else, any /" will be replaced with a "
//       any // will be replaced with a /
//       remove the leading "
//       remove the trailing "
//

//
// Command definitions
// Command is sent in _FJ_SOCKET_MESSAGE structure of each transmission.
//
// Data returned from a 'GET' command is in the corresponding 'SET' command.
//
typedef enum
{
	IPC_COMPLETE        = 0x01,			// Not yet implemented. ?   Command Completed.
	IPC_CONNECTION_CLOSE,				// Close socket connection at receiver's end. no parameters.
	IPC_INVALID,						// Command received was not valid. returned with same data.
	IPC_ECHO,							// Echo command back to transmitter. returned with same data.
	IPC_STATUS,							// Status - asynchronus update. One or more of the following 'PhotoCell=ON/OFF;PrintData=T/F;Heater1Temp=n;Heater1Good=T/F;Heater1Ready=T/F;BoardTemperature=n;VoltReady=T/F;Voltage=n;HeadVoltage=n;InkLevel=FULL/LOW;InkLowTime=yyyy/mm/dd hh:mm:ss;InkFullTime=yyyy/mm/dd hh:mm:ss;PhotoCellMode=E/D/T;BatteryOK=T/F;ClockOK=T/F;SERunning=T/F;AvailableStorage=n;HeadStatus=text;' where PrintData is ON if Message is ready or printing, Heater1Good is status of heater circuit, HeadVoltage is voltage setting read from printhead, PhotoCellMode is Enabled/Disabled/Test, AvailableStorage is the number of bytes available for fonts, messages, etc.
	IPC_GET_STATUS,						// Get all Status - data returned in multiple IPC_STATUS commands
	IPC_PHOTO_TRIGGER,					// Photocell Trigger. 'DATETIME=yyyy/mm/dd hh:mm:ss;'
	IPC_EDIT_STATUS,					// Edit session status. 'EDIT_LOCK=SET;EDIT_IP=n.n.n.n;EDIT_TIME=yyyy/mm/dd hh:mm:ss;'  or  'EDIT_LOCK=FREE;EDIT_IP=0.0.0.0;EDIT_TIME=yyyy/mm/dd hh:mm:ss;'
	IPC_GET_EDIT_STATUS,				// Get edit session status. data returned in IPC_EDIT_STATUS command
	IPC_EDIT_START,						// Request an editing session. IPC_EDIT_STATUS will show which IP address has edit session. If yours, go to it.
	IPC_EDIT_SAVE,						// End an editing session and save changes.
	IPC_EDIT_CANCEL,					// End an editing session and throw away any proposed changes.
										// the following two commands are no longer implemented.
										// they are replaced by IPC_PUT_DYNIMAGE_DATA.
	IPC_IMAGE_HEADER,					// Image header. 'IMAGE_HEIGHT=n;IMAGE_COLUMNS=n;IMAGE_LENGTH=n;'  note: only columns or length must be specified.
	IPC_IMAGE_DATA,						// Binary Image data. must follow IPC_IMAGE_HEADER with same BatchID.

	IPC_GET_LABEL_IDS   = 0x20,			// Get a List of Label names -  all names returned in IPC_SET_LABEL_IDS commands
	IPC_SET_LABEL_IDS,					// Set a List of Label names 'COUNT=n;LABELIDS="aaa","bbb","ccc";
	IPC_GET_LABELS,						// Retrieve all stored labels from IP - data returned in IPC_PUT_LABEL commands
	IPC_GET_LABEL,						// Get a stored label from IP. 'LABELID="aaa";' - data returned in IPC_PUT_LABEL commands, one at a time. a final message with no Label signals completion.
	IPC_PUT_LABEL,						// Put a label on IP. '{Label,"LABELD1",11.0,16.0,0,1,F,"MSGID1"}' where {Label Identifier, Label ID/Name, BoxHeight, BoxWidth, Expiration Value, Expiration Units, Expiration Round Up (T/F), MSGID for each printer in group}
	IPC_DELETE_LABEL,					// Delete stored label from IP. 'LABELID="aaa";'
	IPC_SELECT_LABEL,					// Select label for printing. 'LABELID="aaa";'
	IPC_GET_LABEL_ID_SELECTED,			// Get the selected label id - data returned in IPC_SET_LABEL_ID_SELECTED command
	IPC_SET_LABEL_ID_SELECTED,			// Set the selected label. 'LABELID="aaa";'
	IPC_DELETE_ALL_LABELS,				// Delete all stored labels.

	IPC_GET_MSG_IDS     = 0x30,			// Get a List of Message names -  all names are returned in IPC_SET_MSG_IDS commands
	IPC_SET_MSG_IDS,					// Set a List of Message names 'COUNT=n;MESSAGEIDS="aaa","bbb","ccc";'
	IPC_GET_MSGS,						// Retrieve all stored Messages from IP - data returned in IPC_PUT_MSG commands
	IPC_GET_MSG,						// Get a stored Message from IP. 'MESSAGEID="aaa";' - data returned in IPC_PUT_MSG commands, one at a time. a final message with no Message signals completion.
	IPC_PUT_MSG,						// Put a Message on IP. '{Message,"MSGID1","ELEMENTID1","ELEMENTID2",..}' where {Message Identifier, Message ID/Name, ELEMENTID for each Element in Message}
	IPC_DELETE_MSG,						// Delete stored Message from IP. 'MESSAGEID="aaa";'

	IPC_GET_TEXT_IDS    = 0x40,			// Get a List of TextElement names -  all are returned in IPC_SET_TEXT_IDS commands
	IPC_SET_TEXT_IDS,					// Set a List of TextElement names 'COUNT=n;TEXTIDS="aaa","bbb","ccc";'
	IPC_GET_TEXT_ELEMENTS,				// Retrieve all stored TextElements from IP - data returned in IPC_PUT_TEXT commands
	IPC_GET_TEXT,						// Get a stored TextElement from IP. 'TEXTID="aaa";' - data returned in IPC_PUT_TEXT commands, one at a time. a final message with no TextElement signals completion.
	IPC_PUT_TEXT,						// Put a TextElement on IP. '{Text,"TextID1",Left,Row,Position,Bold,Width,Gap,Inverse,Reverse,Negative,"Font Name","Text"}' where {Text Identifier, Text Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Gap value, Inverse(T/F), Reverse(T/F), Negative(T/F), Font ID/Name, Text to be printed}
	IPC_DELETE_TEXT,					// Delete stored TextElement from IP. 'TEXTID="aaa";'

	IPC_GET_BITMAP_IDS  = 0x50,			// Get a List of BitmapElement names -  all are returned in IPC_SET_BITMAP_IDS commands
	IPC_SET_BITMAP_IDS,					// Set a List of BitmapElement names 'COUNT=n;BITMAPIDS="aaa","bbb","ccc";'
	IPC_GET_BITMAP_ELEMENTS,			// Retrieve all stored BitmapElements from IP - data returned in IPC_PUT_BITMAP commands
	IPC_GET_BITMAP,						// Get a stored BitmapElement from IP. 'BITMAPID="aaa";' - data returned in IPC_PUT_BITMAP commands, one at a time. a final message with no BitmapElement signals completion.
	IPC_PUT_BITMAP,						// Put a BitmapElement on IP. '{Bitmap,"BitmapID1",Left,Row,Position,Bold,Width,Inverse,Reverse,Negative,"BMP"}' where {Bitmap Identifier, Bitmap Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Inverse(T/F), Reverse(T/F), Negative(T/F), BMP file name}
	IPC_DELETE_BITMAP,					// Delete stored BitmapElement from IP. 'BITMAPID="aaa";'

	IPC_GET_DYNIMAGE_IDS = 0x60,		// Get a List of DynimageElement names -  all are returned in IPC_SET_DYNIMAGE_IDS commands
	IPC_SET_DYNIMAGE_IDS,				// Set a List of DynimageElement names 'COUNT=n;DYNIMAGEIDS="aaa","bbb","ccc";'
	IPC_GET_DYNIMAGE_ELEMENTS,			// Retrieve all stored DynimageElements from IP - data returned in IPC_PUT_DYNIMAGE commands
	IPC_GET_DYNIMAGE,					// Get a stored DynimageElement from IP. 'DYNIMAGEID="aaa";' - data returned in IPC_PUT_DYNIMAGE commands, one at a time. a final message with no DynimageElement signals completion.
	IPC_PUT_DYNIMAGE,					// Put a DynimageElement on IP. '{Dynimage,"DYNIMAGEID1",Left,Row,Position,Bold,Width,Inverse,Reverse,Negative}' where {Dynimage Identifier, Dynimage Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Inverse(T/F), Reverse(T/F), Negative(T/F)}
	IPC_DELETE_DYNIMAGE,				// Delete stored DynimageElement from IP. 'DYNIMAGEID="aaa";'
	IPC_PUT_DYNIMAGE_DATA,				// Put the Dynamic Image. 'IMAGE_HEIGHT=n;IMAGE_COLUMNS=n;IMAGE_LENGTH=n;'  note: only columns or length must be specified. This string is followed by the data in row order. Nozzle 0 (bottom) is the first bit of each row. The number of data bytes is indicated by the given or calculated IMAGE_LENGTH.

	IPC_GET_DATETIME_IDS = 0x70,		// Get a List of DatetimeElement names -  all are returned in IPC_SET_DATETIME_IDS commands
	IPC_SET_DATETIME_IDS,				// Set a List of DatetimeElement names 'COUNT=n;DATETIMEIDS="aaa","bbb","ccc";'
	IPC_GET_DATETIME_ELEMENTS,			// Retrieve all stored DatetimeElements from IP - data returned in IPC_PUT_DATETIME commands
	IPC_GET_DATETIME,					// Get a stored DatetimeElement from IP. 'DATETIMEID="aaa";' - data returned in IPC_PUT_DATETIME commands, one at a time. a final message with no DatetimeElement signals completion.
	IPC_PUT_DATETIME,					// Put a DatetimeElement on IP. '{Datetime,"DATETIMEID1",Left,Row,Position,Bold,Width,Gap,Inverse,Reverse,Negative,Format,TimeType,Length,Align,"Fill","Delim",Strings,HourIntervals,"Font Name"}' where {Datetime Identifier, Datetime Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Gap value, Inverse(T/F), Reverse(T/F), Negative(T/F), Format Type(number), Time Type(number), Field Length, Alignment(L/R), Fill Character, Delimiter Character, Normal/Special String Values, Number of Intervals in 24 hours, Font ID/Name}
	IPC_DELETE_DATETIME,				// Delete stored DatetimeElement from IP. 'DATETIMEID="aaa";'

	IPC_GET_COUNTER_IDS = 0x80,			// Get a List of CounterElement names -  all are returned in IPC_SET_COUNTER_IDS commands
	IPC_SET_COUNTER_IDS,				// Set a List of CounterElement names 'COUNT=n;COUNTERIDS="aaa","bbb","ccc";'
	IPC_GET_COUNTER_ELEMENTS,			// Retrieve all stored CounterElements from IP - data returned in IPC_PUT_COUNTER commands
	IPC_GET_COUNTER,					// Get a stored CounterElement from IP. 'COUNTERID="aaa";' - data returned in IPC_PUT_COUNTER commands, one at a time. a final message with no CounterElement signals completion.
	IPC_PUT_COUNTER,					// Put a CounterElement on IP. '{Counter,"COUNTERID1",Left,Row,Position,Bold,Width,Gap,Inverse,Reverse,Negative,Change,Repeat,Start,Limit,Length,Align,"Fill","Font Name"}' where {Counter Identifier, Counter Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Gap value, Inverse(T/F), Reverse(T/F), Negative(T/F), Change Value(+ or -), Repeate Value, Start Value, Limit Value, Field Length, Alignment(L/R), Fill Character, Font ID/Name}
	IPC_DELETE_COUNTER,					// Delete stored CounterElement from IP. 'COUNTERID="aaa";'

	IPC_GET_BARCODE_IDS = 0x90,			// Get a List of BarcodeElement names -  all are returned in IPC_SET_BARCODE_IDS commands
	IPC_SET_BARCODE_IDS,				// Set a List of BarcodeElement names 'COUNT=n;BARCODEIDS="aaa","bbb","ccc";'
	IPC_GET_BARCODE_ELEMENTS,			// Retrieve all stored BarcodeElements from IP - data returned in IPC_PUT_BARCODE commands
	IPC_GET_BARCODE,					// Get a stored BarcodeElement from IP. 'BARCODEID="aaa";' - data returned in IPC_PUT_BARCODE commands, one at a time. a final message with no BarcodeElement signals completion.
	IPC_PUT_BARCODE,					// Put a BarcodeElement on IP. '{Barcode,"BARCODEID1",Left,Row,Position,Bold,Width,Inverse,Reverse,Negative,Style,HRText,TextAlign,TextBold,"Text"}' where {Barcode Identifier, Barcode Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Inverse(T/F), Reverse(T/F), Negative(T/F), Style is index into Global BC table, HumanReadable Text(T/F), HR Text Alignment(L/C/R), HR text Bold value, Text to be printed as BarCode}
	IPC_DELETE_BARCODE,					// Delete stored BarcodeElement from IP. 'BARCODEID="aaa";'

	IPC_GET_FONT_IDS    = 0xA0,			// Get a List of Font names -  all are returned in IPC_SET_FONT_IDS commands
	IPC_SET_FONT_IDS,					// Set a List of Font names 'COUNT=n;FONTIDS="aaa","bbb","ccc";'
	IPC_GET_FONTS,						// Retrieve all stored Fonts from IP - data returned in IPC_PUT_FONT commands
	IPC_GET_FONT,						// Get a stored Font from IP. 'FONTID="aaa";' - data returned in IPC_PUT_FONT commands, one at a time. a final message with no BarcodeElement signals completion.
	IPC_PUT_FONT,						// Put a Font on IP. '{Font,"FONTID1",Type,Height,Width,FirstChar,LastChar,BinaryLength}' where {Font Identifier, Font ID/Name, Height in pixels, Maximum character width, index/id of first character, index/id of last character, length of the binary font that follows this string}
	IPC_DELETE_FONT,					// Delete stored Font from IP. 'FONTID="aaa";'

	IPC_GET_BMP_IDS     = 0xB0,			// Get a List of bmp names -  all are returned in IPC_SET_BMP_IDS commands
	IPC_SET_BMP_IDS,					// Set a List of bmp names 'COUNT=n;BMPIDS="aaa","bbb","ccc";'
	IPC_GET_BMPS,						// Retrieve all stored bmps from IP - data returned in IPC_PUT_BMP commands
	IPC_GET_BMP,						// Get a stored bmp from IP. 'BMPID="aaa";' - data returned in IPC_PUT_BMP commands, one at a time. a final message with no BarcodeElement signals completion.
	IPC_PUT_BMP,						// Put a bmp on IP. '{bmp,"BMPID1",BinaryLength}' where {bmp Identifier, bmp ID/Name, length of the binary bmp that follows this string}
	IPC_DELETE_BMP,						// Delete stored bmp from IP. 'BMPID="aaa";'
	IPC_DELETE_ALL_BMPS,				// Delete all stored bmps from IP.

	IPC_GET_DYNTEXT_IDS = 0xC0,			// Get a List of DyntextElement names -  all are returned in IPC_SET_DYNTEXT_IDS commands
	IPC_SET_DYNTEXT_IDS,				// Set a List of DyntextElement names 'COUNT=n;DYNTEXTIDS="aaa","bbb","ccc";'
	IPC_GET_DYNTEXT_ELEMENTS,			// Retrieve all stored DyntextElements from IP - data returned in IPC_PUT_DYNTEXT commands
	IPC_GET_DYNTEXT,					// Get a stored DyntextElement from IP. 'DYNIMAGEID="aaa";' - data returned in IPC_PUT_DYNTEXT commands, one at a time. a final message with no DyntextElement signals completion.
	IPC_PUT_DYNTEXT,					// Put a DyntextElement on IP. '{Dyntext,"DyntextID1",Left,Row,Position,Bold,Width,Gap,VarDataID,Length,FirstChar,Align,NumChar,"Fill",Inverse,Reverse,Negative,"Font Name"}' where {Dyntext Identifier, Dyntext Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Gap value, Variable Data ID,Field Length,1st char in vardata string, number charaters in vardata string,Alignment(L/R),Inverse(T/F), Reverse(T/F), Negative(T/F), Font ID/Name}
	IPC_DELETE_DYNTEXT,					// Delete stored DyntextElement from IP. 'DYNTEXTID="aaa";'

	IPC_GET_DYNBARCODE_IDS = 0xD0,		// Get a List of DynbarcodeElement names -  all are returned in IPC_SET_DYNBARCODE_IDS commands
	IPC_SET_DYNBARCODE_IDS,				// Set a List of DynbarcodeElement names 'COUNT=n;DYNBARCODEIDS="aaa","bbb","ccc";'
	IPC_GET_DYNBARCODE_ELEMENTS,		// Retrieve all stored DynbarcodeElements from IP - data returned in IPC_PUT_DYNIMAGE commands
	IPC_GET_DYNBARCODE,					// Get a stored DynbarcodeElement from IP. 'DYNBARCODEID="aaa";' - data returned in IPC_PUT_DYNBARCODE commands, one at a time. a final message with no DynbarcodeElement signals completion.
	IPC_PUT_DYNBARCODE,					// Put a DynbarcodeElement on IP. '{'Dynbarcode,"DYNBARCODEID1",Left,Row,Position,Bold,Width,Inverse,Reverse,Negative,Style,HRText,TextAlign,TextBold,VarDataID}' where {Dynbarcode Identifier, Dynbarcode Element ID/Name, Left from box edge value, Row/Nozzle of printer value, Position(D/C/P), Bold value, Width value, Inverse(T/F), Reverse(T/F), Negative(T/F), Style is index into Global BC table, HumanReadable Text(T/F), HR Text Alignment(L/C/R), HR text Bold value, Variable Data ID}
	IPC_DELETE_DYNBARCODE,				// Delete stored DynbarcodeElement from IP. 'DYNBARCODEID="aaa";'

	IPC_GET_GROUP_INFO = 0x100,			// get info about printers in system group - data returned in IPC_SET_GROUP_INFO command
	IPC_SET_GROUP_INFO,					// set info about printers in system group. group leader is first. 'GROUP_NAME="abc";GROUP_COUNT=n;GROUP_IDS="aaa","bbb","ccc","ddd";GROUP_IPADDRS=n.n.n.n,n.n.n.n,n.n.n.n;' 4 element IP addresses.
	IPC_GET_SYSTEM_INFO,				// get info about system. returned in IPC_SET_SYSTEM_INFO command with 'UNITS=ENGLISH_INCHES;TIME=24/12/12M;ENCODER=T;ENCODER_COUNT=n.n;ENCODER_DIVISOR=n;CONVEYOR_SPEED=n.n;PRINT_RESOLUTION=n.n;INK_SIZE=n.n;INK_COST=n.n;'
	IPC_GET_SYSTEM_INTERNET_INFO,		// get info about system. returned in IPC_SET_SYSTEM_INFO command with 'INTERNET_CONTROL=F/W/T/S;DNS_IP=n.n.n.n,n.n.n.n,n.n.n.n;EMAIL_IP=n.n.n.n;EMAIL_SERVER="name";EMAIL_SOURCE="name";EMAIL_INK="uid","uid","uid";EMAIL_SERVICE="uid","uid","uid";WINS_IP=n.n.n.n;NETBIOS_TYPE=NONE/P/B;' where INTERNET_CONTROL is Full/Web/Tcpip/Slave, DNS_IP is the addresses of up to 3 DNS servers, EMAIL_IP is the IP address of the Email server, EMAIL_SERVER is the name of the Email server, EMAIL_SOURCE is the location for the Email source, EMAIL_INK is up to 3 userids to get Email for low ink, EMAIL_SERVICE i sup to 3 userid to get Email for service, WINS_IP is the IP address for a WINS name server, NETBIOS_TYPE is the NETBIOS mode of operation.
	IPC_GET_SYSTEM_TIME_INFO,			// get info about system time. returned in IPC_SET_SYSTEM_INFO command with 'INTERNAL_CLOCK=T;SYSTEM_TIME=yyyy/mm/dd hh:mm:ss;TIME_ZONE=n.n;OBSERVE_USA_DST=T;FIRST_DAY=n;DAYS_WEEK1=n;WEEK53=n;ROLLOVER_HOURS=n.n;SHIFT1=hh:mm;SHIFT2=hh:mm;SHIFT3=hh:mm;SHIFTCODE1="a";SHIFTCODE2="a";SHIFTCODE3="a";' where INTERNAL_CLOCK is T/F, SYSTEM_TIME is local time, TIME_ZONE is the difference between UTC and local time in hours, OBSERVE_USA_DST is T/F to indicate if automatic DST changover should take place, FIRST_DAY is first day of week (1=Sunday). DAYS_WEEK1 is number of days in January for weeek #1, WEEK53 is what number to use when week calculates as 53, ROLLOVER_HOURS is the amount of rollover time added for date calculations, SHIFT1/2/3 is the start time of work shifts, SHIFTCODE1/2/3 is the string to use for the shift codes.
	IPC_GET_SYSTEM_DATE_STRINGS,		// get info about system date/time strings. returned in IPC_SET_SYSTEM_INFO command with 'DAYS="Sunday","Monday","etc";MONTHS="January","February","etc";HOURS="A",B","etc";DAYS_SPECIAL="Sunday","Monday","etc";MONTHS_SPECIAL="January","February","etc"' where DAYS and DAYS_SPECIAL are 7 strings, MONTHS and MONTHS_SPECIAL are 12 strings, and HOURS is 24 strings.
	IPC_GET_SYSTEM_PASSWORDS,			// get info about system passwords. returned in IPC_SET_SYSTEM_INFO command with 'PASSWORDS="DISTRIBUTOR","SECURITY","CONFIGURATION","SAVE/RESTORE","MESSAGE_EDIT","COUNTERS","TEST",MESSAGE_SELECT","VIEW_ALL";'
	IPC_GET_SYSTEM_BARCODES,			// get info about system barcodes. returned in IPC_SET_SYSTEM_INFO command with repeated groups of 'BC_N=n;BC_NAME="name";BC_TY=n;BC_H=n;BC_CD=T;BC_HB=n;BC_VB=n;BC_QZ=n;BC_Base=n;BC_B1=n;BC_B2=n;BC_B3=n;BC_B4=n;' where BC_N is index into BarCode descriptors for the following parameters, BC_NAME is the displayed name, BC_TY is the type, BC_H is the height, BC_CD is T/F for the check digit, BC_HB is Horizontal Bearer size, BC_VB is the Vertical Bearer size, BC_QZ is the size of the Quiet Zone, BC_Base is base/narrow Bar size, BC_B1, etc. are bleed adjustments for widths 1,2,3,4.
	IPC_SET_SYSTEM_INFO,				// set info into system.  any info can be set. see GET commands for keyword details.

	IPC_GET_PRINTER_ID = 0x120,			// get ID of this print head. returned in IPC_SET_PRINTER_INFO command with 'PRINTER_ID="abc";'
    IPC_GET_PRINTER_INFO,				// get info of this print head. returned in IPC_SET_PRINTER_INFO command with 'PRINTER_ID="abc";LONGNAME="abc";SHORTNAME="abc";SERIAL="abc";DIRECTION=RIGHT_TO_LEFT;SLANTANGLE=n.n;SLANTVALUE=n.n;HEIGHT=n.n;DISTANCE=n.n;AMS_INTERVAL=n.n;AMS_INACTIVE=n;PHOTOCELL_BACK=T/F;MSGSTORE_SIZE=n;TABLE_INDEX=n;DATE_MANU=yyyy/mm/dd hh:mm:ss;DATE_SERVICE=yyyy/mm/dd hh:mm:ss;SCANNER_USE_ONCE=T/F;SERIAL_PORT=SCANNER_LABEL/VAR_DATA/INFO/NONE;INFO_ACTIVE=T/F;INFO_STATUS=T/F;INFO_SOCKET=T/F;INFO_RAM=T/F;INFO_ROM=T/F;INFO_NAME=T/F;INFO_TIME=T/F;INFO_AD=T/F;SOP=T/F;'
	IPC_GET_PRINTER_PKG_INFO,			// get packaging info of this print head. returned in IPC_SET_PRINTER_INFO command with 'PKG_LEFT=n.n;PKG_RIGHT=n.n;PKG_TOP=n.n;PKG_BOTTOM=n.n;PKG_UNITS=ENGLISH_INCHES;'
	IPC_GET_PRINTER_PHY_INFO,			// get physical info of this print head. returned in IPC_SET_PRINTER_INFO command with 'PHY_CHANNELS=n;PHY_ORIFICES=n;PHY_INKDROP=n;PHY_DOTHEIGHT=n.n;PHY_DOTWIDTH=n.n;PHY_ANGLE=n.n;PHY_SPAN_CENTER=n.n;PHY_SPAN_OUTER=n.n;PHY_CH_STRAIGHT=n.n;PHY_CH_ANGLE=n.n;PHY_OFF_EVENODD=n.n;PHY_UNITS=ENGLISH_INCHES;'
	IPC_GET_PRINTER_OP_INFO,			// get operating info of this print head. returned in IPC_SET_PRINTER_INFO command with 'OP_NAME="abc";OP_PHY_TABLE=n;OP_PKG_TABLE=n;OP_VOLT_TABLE=n;OP_VOLTAGE=n;OP_FORCEHEADPRESENT=T/F;OP_CURVE1=n.n;OP_CURVE2=n.n;OP_CURVE3=n.n;OP_SUBPULSE=T;OP_SUBPULSE_LENGTH=n.n;OP_SUBPULSE_FREQ=n;OP_SUBPULSE_ON=n;OP_SUBPULSE_OFF=n;OP_SUBPULSE_DUR=n;OP_SUBPULSE_INT=n;OP_HEAD_INVERT=T/F;OP_CHIP_INVERT=T/F;OP_TEMP_TABLE=n;OP_TEMP_RANGE=n;OP_TEMP_1=n;OP_TEMP_2=n;OP_TEMP_3=n;OP_TEMP_CAL_1=n;OP_TEMP_CAL_2=n;OP_TEMP_CAL_3=n;OP_AMS=T/F;OP_AMSA1=n;OP_AMSA2=n;OP_AMSA3=n;OP_AMSA4=n;OP_AMSA5=n;OP_AMSA6=n;OP_AMSA7=n;'
	IPC_SET_PRINTER_INFO,				// set info of this print head. any info can be set. see GET commands for keyword details.
	IPC_GET_PRINTER_ELEMENTS,			// get print head elements out of MemStorage. all are returned in IPC_PUT_PRINTER_ELEMENT commands, one at a time. a final message with no element signals completion.
	IPC_PUT_PRINTER_ELEMENT,			// set print head elements into MemStorage.
	IPC_DELETE_ALL_PRINTER_ELEMENTS,	// delete all print head elements from MemStorage.
	IPC_FIRMWARE_UPGRADE,				// firmware upgrade
	IPC_SET_PH_TYPE,					// set PrintHead type

	IPC_SET_PRINTER_MODE,				// set "PRINT_MODE=RESUME/IDLE/STOP;" Note: Set mode STOP will cancel the label selection
										// set "APS_MODE=ENABLE/DISABLE/RUN;" Note: APS_MODE=RUN will be ignore is the current APS_MODE is DISABLE
	IPC_GET_PRINTER_MODE,				// get current print mode and APS mode, return in IPC_SET_PRINTER_MODE ("PRINT_MODE=RESUME/IDLE/STOP;APS_MODE=ENABLE/DISABLE;")

	IPC_SET_SELECTED_COUNT,				// set selected label counter - ("CountSel=%d;"), return in IPC_STATUS ("LabelSel=%s;CountSel=%d;")
	IPC_GET_SELECTED_COUNT,				// return in IPC_STATUS ("LabelSel=%s;CountSel=%d;")
	IPC_SELECT_VARDATA_ID,				// select variable data field for serial port ("ID=%d;").
	IPC_GET_VARDATA_ID,					// get string of variable data field - ("ID=%d;"). return in IPC_SET_VARDATA_ID ("ID=%d;DATA="%s";")
	IPC_SET_VARDATA_ID,					// set string of variable data field (ID=%d;DATA="%s";)
	IPC_SAVE_ALL_PARAMS,				// write all system parameters into ROM. note - editing session requared.

	IPC_GET_SW_VER_INFO,				// get software version info, return in IPC_STATUS ("SW_VER=%s;")
	IPC_GET_GA_VER_INFO,				// not GA version info, return in IPC_STATUS ("GA_VER=%s;")

	IPC_GA_UPGRADE,						// GA upgrade
	IPC_SET_STATUS_MODE,				// set status reporting mode - "STATUS_MODE=ON/OFF;"
	IPC_GET_STATUS_MODE,					// get current status mode, return in IPC_SET_STATUS_MODE - "STATUS_MODE=ON/OFF;"
	IPC_GET_DISK_USAGE,
	IPC_GET_PH_TYPE,					// get PrintHead type
	IPC_GET_SYSTEM_DATAMATRIX,

	IPC_PUT_BMP_DRIVER = 0x150,

}IPSockCmds;

typedef struct
{
	ULONG  Cmd;							// NetworkCmd value
	ULONG  BatchID;						// this stays the same for all packets that are part of one command. also used for command response.
	ULONG  Length;						// length of Data
}_FJ_SOCKET_MESSAGE_HEADER;

typedef struct
{
	_FJ_SOCKET_MESSAGE_HEADER Header;
	BYTE   Data[_FJ_SOCKET_BUFFER_SIZE];
}_FJ_SOCKET_MESSAGE;

//
//
// the following defines and structures are used to communicate with the Net I/O box
//
//  NOTE: the command structure is the same as the IP structure above.
//        the existing socket code does not differentiate between the two structures
//        when communicating with the two different devices.

// PS2 Data Port Recieve only
#define DATA_FROM_PS2_PORT     0x19

// Serial Ports
// Read
#define DATA_FROM_PORT1        0x14
#define DATA_FROM_PORT2        0x15
#define DATA_FROM_PORT3        0x16
#define DATA_FROM_PORT4        0x17
//    Write
#define WRITE_SERIAL_PORT1     0x0A
#define WRITE_SERIAL_PORT2     0x0B
#define WRITE_SERIAL_PORT3     0x0C
#define WRITE_SERIAL_PORT4     0x0D

// Strobe 1
#define SET_STROBE_1A          0x0E
#define SET_STROBE_1B          0x10
#define SET_STROBE_1C          0x12

// Strobe 2
#define SET_STROBE_2A          0x22
#define SET_STROBE_2B          0x24
#define SET_STROBE_2C          0x26

#define STROBE_OFF             0x30		//0x00
#define STROBE_ON              0x31		//0x01
#define STROBE_FLASH           0x32		//0x02

// Digital I/O
#define SET_OUTPUT_1           0x33
#define READ_INPUT_1           0x3D

// Change Device IP Address - Requires reconnect after COMMAND_ACK
#define SET_DEVICE_ADDRESS     70

#define _IO_SOCKET_NUMBER       1200	// socket/port number for tcp/ip communications to Net I/O Box

#define COMMAND_ACK            0xFF
#define SOCKET_BUFFER_SIZE     0xFF
typedef struct
{
	unsigned long  Cmd;					// NetworkCmd value
	unsigned long  BatchID;				// this stays the same for all packets that are part of one command. also used for command response.
	unsigned long  Length;				// length of Data
}_SOCKET_PACKET_HEADER;

typedef struct
{
	_SOCKET_PACKET_HEADER Header;
	unsigned char Data[SOCKET_BUFFER_SIZE];
}_SOCKET_PACKET;
#endif									// ifndef _FJ_SOCKET_H
