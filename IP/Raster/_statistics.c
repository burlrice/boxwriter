/* C file: c:\MyPrograms\FoxJet1\_statistics.c created
 by NET+Works HTML to C Utility on Tuesday, June 06, 2000 10:50:34 */
#include "fj.h"
#include "fj_system.h"

extern const struct tm tmDTCur;
void na_Double(unsigned long handle, double dInput, int iPlaces);
STRING nbsp[] = "&nbsp;";

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_DIns */
void na_DIns (unsigned long handle);
/*  @! File: na_getDTCur */
void na_getDTCur (unsigned long handle);
/*  @! File: na_DTCur */
void na_DTCur (unsigned long handle);
/*  @! File: na_DTMon */
void na_DTMon (unsigned long handle);
/*  @! File: na_DTDD */
void na_DTDD (unsigned long handle);
/*  @! File: na_StIM */
static void na_StIM (unsigned long handle);
/*  @! File: na_StID */
static void na_StID (unsigned long handle);
/*  @! File: na_DTH_H */
static void na_DTH_H (unsigned long handle);
/*  @! File: na_DTD30 */
static void na_DTD30 (unsigned long handle);
/*  @! File: na_DTH24 */
static void na_DTH24 (unsigned long handle);
/*  @! File: na_DTM60 */
static void na_DTM60 (unsigned long handle);
/*  @! File: na_StM60M */
static void na_StM60M (unsigned long handle);
/*  @! File: na_StM60D */
static void na_StM60D (unsigned long handle);
/*  @! File: na_StH24M */
static void na_StH24M (unsigned long handle);
/*  @! File: na_StH24D */
static void na_StH24D (unsigned long handle);
/*  @! File: na_StD30M */
static void na_StD30M (unsigned long handle);
/*  @! File: na_StD30D */
static void na_StD30D (unsigned long handle);
/*  @! File: na_StCHM */
static void na_StCHM (unsigned long handle);
/*  @! File: na_StCHD */
static void na_StCHD (unsigned long handle);
/*  @! File: na_StCDM */
static void na_StCDM (unsigned long handle);
/*  @! File: na_StCDD */
static void na_StCDD (unsigned long handle);
/*  @! File: na_StCMM */
static void na_StCMM (unsigned long handle);
/*  @! File: na_StCMD */
static void na_StCMD (unsigned long handle);
/*  @! File: na_DTCustR */
static void na_DTCustR (unsigned long handle);
/*  @! File: na_StCustM */
static void na_StCustM (unsigned long handle);
/*  @! File: na_StCustD */
static void na_StCustD (unsigned long handle);
/*  @! File: na_DTInkF */
static void na_DTInkF (unsigned long handle);
/*  @! File: na_StInkFM */
static void na_StInkFM (unsigned long handle);
/*  @! File: na_StInkFD */
static void na_StInkFD (unsigned long handle);
/*  @! File: na_DTInkL */
static void na_DTInkL (unsigned long handle);
/*  @! File: na_StInkLM */
static void na_StInkLM (unsigned long handle);
/*  @! File: na_StInkLD */
static void na_StInkLD (unsigned long handle);
/*  @! File: na_Lab */
static void na_Lab (unsigned long handle);
/*  @! File: na_LabDelta */
static void na_LabDelta (unsigned long handle);
/*  @! File: na_LabTime */
static void na_LabTime (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_StM60ML */
static void na_StM60ML (unsigned long handle);
/*  @! File: na_StM60CO */
static void na_StM60CO (unsigned long handle);
/*  @! File: na_StH24ML */
static void na_StH24ML (unsigned long handle);
/*  @! File: na_StH24CO */
static void na_StH24CO (unsigned long handle);
/*  @! File: na_StD30ML */
static void na_StD30ML (unsigned long handle);
/*  @! File: na_StD30CO */
static void na_StD30CO (unsigned long handle);
/*  @! File: na_StCHML */
static void na_StCHML (unsigned long handle);
/*  @! File: na_StCHCO */
static void na_StCHCO (unsigned long handle);
/*  @! File: na_StCDML */
static void na_StCDML (unsigned long handle);
/*  @! File: na_StCDCO */
static void na_StCDCO (unsigned long handle);
/*  @! File: na_StCMML */
static void na_StCMML (unsigned long handle);
/*  @! File: na_StCMCO */
static void na_StCMCO (unsigned long handle);
/*  @! File: na_StIML */
static void na_StIML (unsigned long handle);
/*  @! File: na_StICO */
static void na_StICO (unsigned long handle);
/*  @! File: na_StCustML */
static void na_StCustML (unsigned long handle);
/*  @! File: na_StCustCO */
static void na_StCustCO (unsigned long handle);
/*  @! File: na_StInkFML */
static void na_StInkFML (unsigned long handle);
/*  @! File: na_StInkFCO */
static void na_StInkFCO (unsigned long handle);
/*  @! File: na_StInkLML */
static void na_StInkLML (unsigned long handle);
/*  @! File: na_StInkLCO */
static void na_StInkLCO (unsigned long handle);
/*  @! File: na_StCO) */
static void na_StCO (unsigned long handle);
/*  @! File: na_StCO */
static void na_StCO (unsigned long handle);
/*  @! File: na_TITLE_STATISTICS */
static void na_TITLE_STATISTICS (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_ST_RESET */
static void na_ST_RESET (unsigned long handle);
/*  @! File: na_ST_LABEL_TABLE */
static void na_ST_LABEL_TABLE (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\statistics.htm {{ */
void Send_function_22(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_STATISTICS (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<script type=text/javascript>\n");
	HSSend (handle, "var h2text=\"Statistics\"\n");
	HSSend (handle, "setTimeout(\"document.location.reload()\",60000); // 60 seconds\n");
	HSSend (handle, "function startcounter() {\n");
	HSSend (handle, "  var str = \"counter_change.htm\";\n");
	HSSend (handle, "  window.open(str,\"Counter\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=400\");\n");
	HSSend (handle, " }\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "");
	na_getDTCur (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<p>Last message \"<q>");
	na_Lab (handle);
	HSSend (handle, "</q>\" was printed ");
	na_LabDelta (handle);
	HSSend (handle, " ago at ");
	na_LabTime (handle);
	HSSend (handle, " .</p>\n");
	HSSend (handle, "<table border=\"0\" cellpadding=\"2\" width=\"99%\">\n");
	HSSend (handle, "<form action=stat_1 method=POST><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, ">\n");
	HSSend (handle, " <table border=1 cellpadding=\"2\" width=\"80%\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, " <thead>\n");
	HSSend (handle, "  <tr><td valign=top>&nbsp;</td><td align=center valign=top><b>Date</b></td><td align=center valign=top><b>Messages</b></td><td align=center valign=top><b>Dots</b>(000,000)</td><td align=center valign=top><b>Milliliters</b></td><td align=center valign=top><b>Cost</b>( ");
	na_StCO (handle);
	HSSend (handle, " )</td></tr>\n");
	HSSend (handle, "  </thead><tbody>\n");
	HSSend (handle, "  <tr><td><b>last 60 minutes</b></td><td>");
	na_DTM60 (handle);
	HSSend (handle, "</td><td>");
	na_StM60M (handle);
	HSSend (handle, "</td><td>");
	na_StM60D (handle);
	HSSend (handle, "</td><td>");
	na_StM60ML (handle);
	HSSend (handle, "</td><td>");
	na_StM60CO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>last 24 hours</b></td><td>");
	na_DTH24 (handle);
	HSSend (handle, "</td><td>");
	na_StH24M (handle);
	HSSend (handle, "</td><td>");
	na_StH24D (handle);
	HSSend (handle, "</td><td>");
	na_StH24ML (handle);
	HSSend (handle, "</td><td>");
	na_StH24CO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>last 30 days</b></td><td>");
	na_DTD30 (handle);
	HSSend (handle, "</td><td>");
	na_StD30M (handle);
	HSSend (handle, "</td><td>");
	na_StD30D (handle);
	HSSend (handle, "</td><td>");
	na_StD30ML (handle);
	HSSend (handle, "</td><td>");
	na_StD30CO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Clock Hour</b></td><td>");
	na_DTH_H (handle);
	HSSend (handle, "</td><td>");
	na_StCHM (handle);
	HSSend (handle, "</td> <td>");
	na_StCHD (handle);
	HSSend (handle, "</td><td>");
	na_StCHML (handle);
	HSSend (handle, "</td><td>");
	na_StCHCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Calendar Day</b></td><td>");
	na_DTDD (handle);
	HSSend (handle, "</td><td>");
	na_StCDM (handle);
	HSSend (handle, "</td><td>");
	na_StCDD (handle);
	HSSend (handle, "</td> <td>");
	na_StCDML (handle);
	HSSend (handle, "</td><td>");
	na_StCDCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Calendar Month</b></td><td>");
	na_DTMon (handle);
	HSSend (handle, "</td><td>");
	na_StCMM (handle);
	HSSend (handle, "</td><td>");
	na_StCMD (handle);
	HSSend (handle, "</td><td>");
	na_StCMML (handle);
	HSSend (handle, "</td><td>");
	na_StCMCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>In Service</b></td><td>");
	na_DIns (handle);
	HSSend (handle, "</td><td>");
	na_StIM (handle);
	HSSend (handle, "</td><td>");
	na_StID (handle);
	HSSend (handle, "</td><td>");
	na_StIML (handle);
	HSSend (handle, "</td><td>");
	na_StICO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Customer&nbsp;<input class=fjbtn1 type=submit ");
	na_ST_RESET (handle);
	HSSend (handle, "></b></td><td>");
	na_DTCustR (handle);
	HSSend (handle, "</td><td>");
	na_StCustM (handle);
	HSSend (handle, "</td> <td>");
	na_StCustD (handle);
	HSSend (handle, "</td><td>");
	na_StCustML (handle);
	HSSend (handle, "</td><td>");
	na_StCustCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Ink Full</b></td><td>");
	na_DTInkF (handle);
	HSSend (handle, "</td><td>");
	na_StInkFM (handle);
	HSSend (handle, "</td><td>");
	na_StInkFD (handle);
	HSSend (handle, "</td><td>");
	na_StInkFML (handle);
	HSSend (handle, "</td><td>");
	na_StInkFCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  <tr><td><b>Ink Low</b></td><td>");
	na_DTInkL (handle);
	HSSend (handle, "</td><td>");
	na_StInkLM (handle);
	HSSend (handle, "</td><td>");
	na_StInkLD (handle);
	HSSend (handle, "</td> <td>");
	na_StInkLML (handle);
	HSSend (handle, "</td><td>");
	na_StInkLCO (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  </tbody>\n");
	HSSend (handle, " </table>\n");
	HSSend (handle, "  <p></p>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "<form action=stat_2 method=POST><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, ">\n");
	HSSend (handle, " <table border=1 cellpadding=\"2\" width=\"80%\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, " <col align=left><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=center>\n");
	HSSend (handle, " ");
	na_ST_LABEL_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, " </table>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<p></p>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\statistics.htm }} */
/* @! End HTML page   */

/* @! Begin of post function */
/* @! Function: Post_stat_1  {{ */
void Post_stat_1(unsigned long handle)
{
	/* add your implementation here: */
	pCVT->fj_print_head_CountsCustomerReset();
	fj_reload_page(handle, "statistics.htm");
}										/* @! Function: Post_stat_1  }} */
/* @! Function: Post_stat_2  {{ */
void Post_stat_2(unsigned long handle)
{
	/* add your implementation here: */
}										/* @! Function: Post_stat_2  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_StIM  {{ */
void na_StIM(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle,pCVT->pBRAM->countInService.lMessages);
}										/* @! Function: na_StIM  }} */
/* @! Function: na_StID  {{ */
void na_StID(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle,pCVT->pBRAM->countInService.lDotsHi);
}										/* @! Function: na_StID  }} */
/* @! Function: na_StIML  {{ */
void na_StIML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countInService.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countInService.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StIML  }} */
/* @! Function: na_StICO  {{ */
void na_StICO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countInService.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countInService.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StICO  }} */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_DTCur  {{ */
/* @! Function: na_DTMon  {{ */
/* @! Function: na_DTDD  {{ */
/* @! Function: na_DTH_H  {{ */
void na_DTH_H(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	CHAR   m1;
	CHAR   m2;
	CHAR   str1[16];
	// get current time
	pCVT->fj_SystemGetTimeNoSecString( pfsys, str1, &tmDTCur );
	m1 = str1[3];
	m2 = str1[4];
	str1[3] = '0';
	str1[4] = '0';
	HSSend(handle, str1);
	HSSend(handle, " - ");
	str1[3] = m1;
	str1[4] = m2;
	HSSend(handle, str1);
}										/* @! Function: na_DTH_H  }} */
/* @! Function: na_DIns  {{ */
void na_DIns(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	char  str[16];

	if ( 0 != pfph->tmDateInservice.tm_year )
	{
		pCVT->fj_SystemGetDateString( str, &(pfph->tmDateInservice) );
		HSSend (handle, str);
	}
	else
	{
		HSSend (handle, nbsp);
	}
}										/* @! Function: na_DIns  }} */
/* @! Function: na_DTD30  {{ */
void na_DTD30(unsigned long handle)
{
	/* add your implementation here: */
	struct tm tm;
	CHAR   str1[16];
	CHAR   str2[16];
										// get current date
	pCVT->fj_SystemGetDateString( str2, &tmDTCur );
	memcpy( &tm, &tmDTCur, sizeof(struct tm) );
	tm.tm_mday -= 29;					// subtract 29 days
	pCVT->fj_mktime( &tm );
										// get previous date
	pCVT->fj_SystemGetDateString( str1, &tm );
	HSSend(handle, str1+5);
	HSSend(handle, " - ");
	HSSend(handle, str2+5);
}										/* @! Function: na_DTD30  }} */
/* @! Function: na_DTH24  {{ */
void na_DTH24(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	struct tm tm;
	CHAR   day1[16];
	CHAR   day2[16];
	CHAR   str1[16];
	CHAR   str2[16];
	pCVT->fj_SystemGetDayString( pfsys, day2, &tmDTCur );
	// get current time
	pCVT->fj_SystemGetTimeNoSecString( pfsys, str2, &tmDTCur );
	memcpy( &tm, &tmDTCur, sizeof(struct tm) );
	tm.tm_min   = 0;					// set minutes to 0
	tm.tm_hour -= 23;					// subtract 23 hours
	pCVT->fj_mktime( &tm );
										// get current time
	pCVT->fj_SystemGetTimeNoSecString( pfsys, str1, &tm );
	pCVT->fj_SystemGetDayString( pfsys, day1, &tm );
	//    day1[3] = ' ';
	//    day1[4] = 0;
	day1[3] = 0;
	strcat( day1, " " );
	HSSend(handle, day1);
	HSSend(handle, str1);
	HSSend(handle, " - ");
	//    day2[3] = ' ';
	//    day2[4] = 0;
	day2[3] = 0;
	strcat( day2, " " );
	HSSend(handle, day2);
	HSSend(handle, str2);
}										/* @! Function: na_DTH24  }} */
/* @! Function: na_DTM60  {{ */
void na_DTM60(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	struct tm tm;
	CHAR   str1[16];
	CHAR   str2[16];
	// get current time
	pCVT->fj_SystemGetTimeNoSecString( pfsys, str2, &tmDTCur );
	memcpy( &tm, &tmDTCur, sizeof(struct tm) );
	tm.tm_min -= 59;
	pCVT->fj_mktime( &tm );
										// get previous date
	pCVT->fj_SystemGetTimeNoSecString( pfsys, str1, &tm );
	HSSend(handle, str1);
	HSSend(handle, " - ");
	HSSend(handle, str2);
}										/* @! Function: na_DTM60  }} */
/* @! Function: na_DTCustR  {{ */
void na_DTCustR(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	CHAR   str[64];
	pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pBRAM->tmCustomerReset) );
	HSSend(handle, str);
}										/* @! Function: na_DTCustR  }} */
/* @! Function: na_StM60M  {{ */
void na_StM60M(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count60Minutes.lMessages);
}										/* @! Function: na_StM60M  }} */
/* @! Function: na_StM60D  {{ */
void na_StM60D(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count60Minutes.lDotsHi);
}										/* @! Function: na_StM60D  }} */
/* @! Function: na_StM60ML  {{ */
void na_StM60ML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->count60Minutes.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count60Minutes.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StM60ML  }} */
/* @! Function: na_StM60CO  {{ */
void na_StM60CO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->count60Minutes.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count60Minutes.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StM60CO  }} */
/* @! Function: na_StH24M  {{ */
void na_StH24M(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count24Hours.lMessages);
}										/* @! Function: na_StH24M  }} */
/* @! Function: na_StH24D  {{ */
void na_StH24D(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count24Hours.lDotsHi);
}										/* @! Function: na_StH24D  }} */
/* @! Function: na_StH24ML  {{ */
void na_StH24ML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->count24Hours.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count24Hours.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StH24ML  }} */
/* @! Function: na_StH24CO  {{ */
void na_StH24CO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->count24Hours.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count24Hours.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StH24CO  }} */
/* @! Function: na_StD30M  {{ */
void na_StD30M(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count30Days.lMessages);
}										/* @! Function: na_StD30M  }} */
/* @! Function: na_StD30D  {{ */
void na_StD30D(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->count30Days.lDotsHi);
}										/* @! Function: na_StD30D  }} */
/* @! Function: na_StD30ML  {{ */
void na_StD30ML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->count30Days.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count30Days.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StD30ML  }} */
/* @! Function: na_StD30CO  {{ */
void na_StD30CO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->count30Days.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->count30Days.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StD30CO  }} */
/* @! Function: na_StCHM  {{ */
void na_StCHM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countClockHour.lMessages);
}										/* @! Function: na_StCHM  }} */
/* @! Function: na_StCHD  {{ */
void na_StCHD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countClockHour.lDotsHi);
}										/* @! Function: na_StCHD  }} */
/* @! Function: na_StCHML  {{ */
void na_StCHML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countClockHour.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countClockHour.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StCHML  }} */
/* @! Function: na_StCHCO  {{ */
void na_StCHCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countClockHour.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countClockHour.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StCHCO  }} */
/* @! Function: na_StCDM  {{ */
void na_StCDM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCalDay.lMessages);
}										/* @! Function: na_StCDM  }} */
/* @! Function: na_StCDD  {{ */
void na_StCDD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCalDay.lDotsHi);
}										/* @! Function: na_StCDD  }} */
/* @! Function: na_StCDML  {{ */
void na_StCDML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countCalDay.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCalDay.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StCDML  }} */
/* @! Function: na_StCDCO  {{ */
void na_StCDCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countCalDay.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCalDay.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StCDCO  }} */
/* @! Function: na_StCMM  {{ */
void na_StCMM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCalMonth.lMessages);
}										/* @! Function: na_StCMM  }} */
/* @! Function: na_StCMD  {{ */
void na_StCMD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCalMonth.lDotsHi);
}										/* @! Function: na_StCMD  }} */
/* @! Function: na_StCMML  {{ */
void na_StCMML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countCalMonth.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCalMonth.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StCMML  }} */
/* @! Function: na_StCMCO  {{ */
void na_StCMCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countCalMonth.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCalMonth.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StCMCO  }} */
/* @! Function: na_StCustM  {{ */
void na_StCustM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCustomer.lMessages);
}										/* @! Function: na_StCustM  }} */
/* @! Function: na_StCustD  {{ */
void na_StCustD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countCustomer.lDotsHi);
}										/* @! Function: na_StCustD  }} */
/* @! Function: na_StCustML  {{ */
void na_StCustML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countCustomer.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCustomer.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 6 );
}										/* @! Function: na_StCustML  }} */
/* @! Function: na_StCustCO  {{ */
void na_StCustCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countCustomer.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countCustomer.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 6 );
}										/* @! Function: na_StCustCO  }} */
/* @! Function: na_DTInkF  {{ */
void na_DTInkF(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	CHAR   str[64];
	pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pBRAM->tmInkFull) );
	HSSend(handle, str);
}										/* @! Function: na_DTInkF  }} */
/* @! Function: na_StInkFM  {{ */
void na_StInkFM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countInkFull.lMessages);
}										/* @! Function: na_StInkFM  }} */
/* @! Function: na_StInkFD  {{ */
void na_StInkFD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_Long(handle,pBRAM->countInkFull.lDotsHi);
}										/* @! Function: na_StInkFD  }} */
/* @! Function: na_StInkFML  {{ */
void na_StInkFML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d;
	d  = (DOUBLE)pBRAM->countInkFull.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countInkFull.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	na_Double(handle, d, 2 );
}										/* @! Function: na_StInkFML  }} */
/* @! Function: na_StInkFCO  {{ */
void na_StInkFCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	DOUBLE d,c;
	d  = (DOUBLE)pBRAM->countInkFull.lDotsHi * (DOUBLE)1000000.0;
	d += (DOUBLE)pBRAM->countInkFull.lDotsLo;
	d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
	c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
	na_Double(handle, c, 2 );
}										/* @! Function: na_StInkFCO  }} */
/* @! Function: na_DTInkL  {{ */
void na_DTInkL(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	CHAR   str[64];

	if ( TRUE == pBRAM->bInkLow )
	{
		pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pBRAM->tmInkLow) );
		HSSend(handle, str);
	}
	else
	{
		HSSend(handle, nbsp);
	}
}										/* @! Function: na_DTInkL  }} */
/* @! Function: na_StInkLM  {{ */
void na_StInkLM(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	if ( TRUE == pBRAM->bInkLow ) na_Long(handle,pBRAM->countInkLow.lMessages);
	else                          HSSend(handle, nbsp);
}										/* @! Function: na_StInkLM  }} */
/* @! Function: na_StInkLD  {{ */
void na_StInkLD(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	if ( TRUE == pBRAM->bInkLow ) na_Long(handle,pBRAM->countInkLow.lDotsHi);
	else                          HSSend(handle, nbsp);
}										/* @! Function: na_StInkLD  }} */
/* @! Function: na_StInkLML  {{ */
void na_StInkLML(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	if ( TRUE == pBRAM->bInkLow )
	{
		DOUBLE d;
		d  = (DOUBLE)pBRAM->countInkLow.lDotsHi * (DOUBLE)1000000.0;
		d += (DOUBLE)pBRAM->countInkLow.lDotsLo;
		d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
		na_Double(handle, d, 2 );
	}
	else
	{
		HSSend(handle, nbsp);
	}
}										/* @! Function: na_StInkLML  }} */
/* @! Function: na_StInkLCO  {{ */
void na_StInkLCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	if ( TRUE == pBRAM->bInkLow )
	{
		DOUBLE d,c;
		d  = (DOUBLE)pBRAM->countInkLow.lDotsHi * (DOUBLE)1000000.0;
		d += (DOUBLE)pBRAM->countInkLow.lDotsLo;
		d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;
		c  = (DOUBLE)pfsys->fInkBottleCost/(DOUBLE)pfsys->fInkBottleSize*d;
		na_Double(handle, c, 2 );
	}
	else
	{
		HSSend(handle, nbsp);
	}
}										/* @! Function: na_StInkLCO  }} */
/* @! Function: na_Lab  {{ */
void na_Lab(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	na_String(handle, pBRAM->sLastMessage);

	// void call to update statistics data
	fj_print_head_Counters (0,0);
}										/* @! Function: na_Lab  }} */
/* @! Function: na_LabDelta  {{ */
void na_LabDelta(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	time_t ttNow;
	time_t ttLast;
	time_t ttDelta;
	struct tm tm;
	CHAR   str[64];
	BOOL   bFirst = FALSE;

	pCVT->fj_SystemGetTime( &tm );
	ttNow  = pCVT->fj_mktime( &tm );
	ttLast = pCVT->fj_mktime( &(pBRAM->tmLastPrint) );
	ttDelta = ttNow - ttLast;
	gmtime_r( &ttDelta, &tm );
	if ( ( tm.tm_year != 0 ) &&
		( tm.tm_mon  != 0 ) )
	{
		HSSend(handle, " more than 30 days");
	}
	else
	{
		tm.tm_mday--;					// time conversion sets Jan 1 as the 0 day. make the 1 into a 0.
		if ( (tm.tm_mday != 0) || (TRUE == bFirst) )
		{
			bFirst = TRUE;
			sprintf( str, " %i day", tm.tm_mday );
			if ( tm.tm_mday != 1 ) strcat( str, "s" );
			HSSend(handle, str);
		}
		if ( (tm.tm_hour != 0) || (TRUE == bFirst) )
		{
			bFirst = TRUE;
			sprintf( str, " %i hour", tm.tm_hour );
			if ( tm.tm_hour != 1 ) strcat( str, "s" );
			HSSend(handle, str);
		}
		if ( (tm.tm_min != 0) || (TRUE == bFirst) )
		{
			bFirst = TRUE;
			sprintf( str, " %i minute", tm.tm_min );
			if ( tm.tm_min != 1 ) strcat( str, "s" );
			HSSend(handle, str);
		}
										// don't print seconds if there is a day or hour
		if ( (tm.tm_mday == 0) && (tm.tm_hour == 0) )
		{
			bFirst = TRUE;
			sprintf( str, " %i second", tm.tm_sec );
			if ( tm.tm_sec != 1 ) strcat( str, "s" );
			HSSend(handle, str);
		}
	}
}										/* @! Function: na_LabDelta  }} */
/* @! Function: na_LabTime  {{ */
void na_LabTime(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	CHAR   str[64];
	pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pBRAM->tmLastPrint) );
	HSSend(handle, str);
}										/* @! Function: na_LabTime.  }} */
/* @! Function: na_ID  {{ */
/* @! Function: na_StCO  {{ */
void na_StCO(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	CHAR str[64];

	sprintf( str, "%0.2f / %d ml", (DOUBLE)pfsys->fInkBottleCost, (int)pfsys->fInkBottleSize );
	HSSend(handle, str);
}										/* @! Function: na_StCO  }} */
/* @! Function: na_TITLE_STATISTICS  {{ */
void na_TITLE_STATISTICS(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Statistics" );
}										/* @! Function: na_TITLE_STATISTICS  }} */
/* @! Function: na_getDTCur  {{ */
/* @! Function: na_PWID  {{ */
/* @! Function: na_ST_RESET  {{ */
void na_ST_RESET(unsigned long handle)
{
	/* add your implementation here: */
	LONG  lLevel;

	lLevel = fj_get_level(handle);
	HSSend(handle, "value=\"Reset\"");
	if ( lLevel < FJSYS_PRIORITY_COUNTERS )
	{
		HSSend(handle, " DISABLED");
	}
}										/* @! Function: na_ST_RESET  }} */
/* @! Function: na_ST_LABEL_TABLE  {{ */
void na_ST_LABEL_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPLABELCOUNTER pLC;
	LONG lLevel;
	int  i;
	CHAR str[64];

	lLevel = fj_get_level(handle);
	HSSend(handle, "<tr><td colspan=4>&nbsp;</td><td colspan=4><button class=fjbtn1 onClick=\"startcounter()\"");
	if ( lLevel < FJSYS_PRIORITY_COUNTERS )
	{
		HSSend(handle, " DISABLED");
	}
	HSSend(handle, ">Change Counters</button></td></tr>\n");
	HSSend(handle, "<tr><td align=center><b>Label</b></td><td><b>Count</b></td><td><b>First Print</b></td><td><b>Last Print</b></td><td colspan=2><b>Counter 1</b></td><td colspan=2><b>Counter 2</b></td></tr>\n");
	for ( i = 0; i < FJSYS_NUMBER_LABEL_COUNTERS; i++ )
	{
		pLC = &(pBRAM->LabelCounters[i]);
		HSSend(handle, "<tr><td>&nbsp;" );
		na_String(handle, pLC->cLabelName );
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->lCount ) na_Long(handle,pLC->lCount );
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmFirst.tm_year )
		{
			pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pLC->tmFirst) );
			HSSend(handle, str);
		}
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmLast.tm_year )
		{
			pCVT->fj_SystemGetDateTimeFullString( pfsys, str, &(pLC->tmLast) );
			HSSend(handle, str);
		}
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmFirst.tm_year ) na_Long(handle,pLC->lCount1First );
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmFirst.tm_year ) na_Long(handle,pLC->lCount1Last );
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmFirst.tm_year ) na_Long(handle,pLC->lCount2First );
		HSSend(handle, "</td><td>&nbsp;" );
		if ( 0 != pLC->tmFirst.tm_year ) na_Long(handle,pLC->lCount2Last );
		HSSend(handle, "</td></tr>\n");
	}
}										/* @! Function: na_ST_LABEL_TABLE  }} */
/* @! End of dynamic functions   */
