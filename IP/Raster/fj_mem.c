#include <windows.h>
#include "fj_base.h"
//#include "fj_win.h"

#ifdef _WINDOWS

FJDLLExport VOID* fj_malloc( LONG lSize )
{
	LPBYTE pBuf = malloc( lSize );
	/*
    if( (LPBYTE)0x007EE0A0 == pBuf )
	{
		pBuf++;
		pBuf -= 1;
	}
*/
	return( pBuf );
}

FJDLLExport VOID* fj_calloc( LONG lCount, LONG lSize )
{
	return( calloc( lCount, lSize ) );
}

FJDLLExport VOID  fj_free( VOID* pBuffer )
{
	free( pBuffer );
	return;
}

#endif //_WINDOWS
