#ifndef FJ_IMAGE
#define FJ_IMAGE

#include "fj_base.h"

//
// the image to be printed follows the FJIMAGE structure in memory. There is no  separate pointer to it.
//
typedef struct fjimage
{
	LONG    lHeight;					// height of image in pixels  - depends on font
	LONG    lLength;					// number of bytes in image buffer
	LONG    lTransforms;				// transforms used to change image
	LONG    lBits;						// number of 'on bits' (dots) in the image buffer
	LONG    lUseCounter;				// number of users
        LONG	lShifted; 				// used by fj_ImageShift // burl 1.2016
} FJIMAGE, FAR *LPFJIMAGE, FAR * const CLPFJIMAGE;
typedef const struct fjimage FAR *LPCFJIMAGE, FAR * const CLPCFJIMAGE;

// transform values
#define FJ_TRANS_NORMAL   0x00000000	// normal painting
#define FJ_TRANS_REVERSE  0x00000010	// for right-to-left painting
#define FJ_TRANS_INVERSE  0x00000020	// upside down
#define FJ_TRANS_NEGATIVE 0x00000040	// 'negative' printing. paint black background.

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	//
	// declarations for routines in image DLL
	//
	FJDLLExport LPFJIMAGE fj_ImageNew( LONG lLength );
	FJDLLExport VOID      fj_ImageDestroy( LPFJIMAGE pfi );
	FJDLLExport VOID      fj_ImageUse( LPFJIMAGE pfi );
	FJDLLExport VOID      fj_ImageRelease( LPFJIMAGE pfi );
	FJDLLExport LPFJIMAGE fj_ImageBold ( LPFJIMAGE pfiIn, LONG lBold );
	FJDLLExport LPFJIMAGE fj_ImageWidth( LPFJIMAGE pfiIn, LONG lWidth );
	FJDLLExport LPFJIMAGE fj_ImageSlant( LPFJIMAGE pfiIn, LONG lSlant );
	FJDLLExport LPFJIMAGE fj_ImageRasterTransform( LPFJIMAGE pfiIn, LONG lTransforms );
	FJDLLExport LPFJIMAGE fj_ImageShift( LPFJIMAGE pfiIn, LONG lBits );
	FJDLLExport VOID      fj_ImageNegative( LPFJIMAGE pfi );
	FJDLLExport VOID      fj_ImageTrace ( LPFJIMAGE pfi );
	FJDLLExport LPFJIMAGE fj_ImageHeightBold ( LPFJIMAGE pfiIn, LONG lBold );
	FJDLLExport LPFJIMAGE fj_ImageHeight ( LPFJIMAGE pfiIn, LONG lHeight );
#ifdef _WINDOWS
	extern
#endif
		BYTE      fj_ImageCountBits[256];//  table to count bits in a byte // Burl // made 'extern'
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_IMAGE
