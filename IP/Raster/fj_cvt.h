#ifndef FJ_CVT_H
#define FJ_CVT_H

#ifndef _WINDOWS
#include "fj_netsilicon.h"
#endif
#include "fj_base.h"

// forward declarations
struct tm;
struct fjfont;
struct fjelement;
struct fjmessage;
struct fjlabel;

#ifdef _WINDOWS
// CVT - Communication Vector Table
// structure for communications between modules
typedef struct fj_cvt
{
	LPFJ_NETOSPARAM    pNETOSparam;		// pointer to "permanent" parameters in ROM
	LPFJ_PARAM         pFJparam;		// pointer to "permanent" parameters in ROM
	struct fjmemstor   *pFJMemStor;		// pointer to memstore for "permanent" parameters
	LPFJ_GLOBAL        pFJglobal;		// pointer to global shared variables in RAM
	LPFJSYSTEM         pfsys;			// pointer to FJSYSTEM structure
	LPFJPRINTHEAD      pfph;			// pointer to FJPRINTHEAD structure
										// pointer to array of print head package descriptors
	const FJPRINTHEAD_PKG  (* const paFJPhPkg)[];
										// pointer to array of print head physical descriptors
	const FJPRINTHEAD_PHY  (* const paFJPhPhy)[];
										// pointer to array of print head operational descriptors
	const FJPRINTHEAD_OP   (* const paFJPhOp )[];
	LONG               lFJPhOpCount;	// number of print head operational descriptors
	ROMENTRY         (*pfj_RomTable)[];	// pointer to fj_RomTable[]
	LPGA               pGA;				// address of Gate Array memory
	LPFJ_BRAM          pBRAM;			// address of battery backed-up RAM
	VOID (*pfj_writeInfo)(LPCHAR);		// pointer to fj_writeinfo()
	VOID*(*pfj_malloc   )(LONG);		// pointer to fj_malloc()
	VOID (*pfj_free     )(VOID*);		// pointer to fj_free()
	LONG (*punused1     )(VOID);		// pointer to unused
	LONG (*punused2     )(VOID);		// pointer to unused
	LONG (*punused3     )(VOID);		// pointer to unused

} FJ_CVT, FAR *LPFJ_CVT, FAR * const CLPFJ_CVT;
typedef const struct fj_cvt FAR *LPCFJ_CVT, FAR * const CLPCFJ_CVT;
#else									//!_WINDOWS

// CVT - Communication Vector Table
// structure for communications between modules
typedef struct fj_cvt
{
										// pointer to "permanent" parameters in ROM
	devBoardParamsType * const pNETOSparam;
	struct fj_param    * const pFJparam;// pointer to "permanent" parameters in ROM
										// pointer to memstore for "permanent" parameters
	struct fjmemstor   * const pFJMemStor;
										// pointer to global shared variables in RAM
	struct fj_global   * const pFJglobal;
	struct fjsystem    * const pfsys;	// pointer to FJSYSTEM structure
	struct fjprinthead * const pfph;	// pointer to FJPRINTHEAD structure
										// pointer to array of print head package descriptors
	const struct fjprinthead_package (* const paFJPhPkg)[];
										// pointer to array of print head physical descriptors
	const struct fjprinthead_phy     (* const paFJPhPhy)[];
										// pointer to array of print head operational descriptors
	const struct fjprinthead_op      (* const paFJPhOp )[];
	const LONG           lFJPhOpCount;	// number of print head operational descriptors
	struct ga         * const pGA;		// address of Gate Array memory
	struct fj_bram    * const pBRAM;	// address of battery backed-up RAM
	const struct voltinfo  (* const paVoltInfo)[];
	const struct tempinfo  (* const paTempInfo)[];

	const struct rom_entry (*pfj_RomTable)[];
	const LONG lFJRomTableCount;

	const STRING * const pfj_FontID;
	const STRING * const pfj_LabelID;
	const STRING * const pfj_MessageID;
	const STRING * const pfj_bmpID;

	void (* const fj_1AppSearchURL)    (unsigned long, char *);
	void (* const fj_1AppPreprocessURL)(unsigned long, char *);
	void (* const fj_2AppSearchURL)    (unsigned long, char *);
	void (* const fj_2AppPreprocessURL)(unsigned long, char *);

	// from fj_info_soft.c
	int            (* const fj_openSerialPort)( struct fjprinthead FAR * );
	int            (* const fj_closeSerialPort)( void );
	const STRING * (* const fj_getVersionNumber)(VOID);
	const STRING * (* const fj_getVersionDate  )(VOID);
	BOOL           (* const fj_setSerial)( LPSTR pInput );
	VOID           (* const fj_setID)( VOID );
	VOID           (* const fj_setGroupID)( VOID );

	// from fj_print_head.c
	VOID   (* const fj_print_head_SetStatus)(VOID);
	BOOL   (* const fj_print_head_EditStart)( ULONG ipAdd );
	VOID   (* const fj_print_head_EditCancel)( ULONG ipAdd );
	VOID   (* const fj_print_head_EditSave)( ULONG ipAdd );
	VOID   (* const fj_print_head_setPWMVoltageValue)( VOID );

	// from fj_print_head_info.c
	VOID   (* const fj_print_head_SendPhotocellMode)( VOID );
	VOID   (* const fj_print_head_SendSelectedCNT)(VOID);
	VOID   (* const fj_print_head_SendInfoAll)(VOID);
	VOID   (* const fj_print_head_CountsResetAll)( VOID );
	VOID   (* const fj_print_head_CountsCustomerReset)(VOID);

	// from fj_print_headGAENI.c
	VOID   (* const fj_print_head_SetRTClockTM)( struct tm *pTM );
	VOID   (* const fj_print_head_SetGAValues)( VOID );
	VOID   (* const fj_print_head_SetGAEncoder)( VOID );
	VOID   (* const fj_print_head_SetGAWriteSwitches)( VOID );
	VOID   (* const fj_print_head_PhotoCell)( VOID );
	VOID   (* const fj_print_head_PrintStepPattern)( VOID );
	VOID   (* const fj_print_head_PrintTestPattern)( LPSTR pPattern, LONG lCount );
	VOID   (* const fj_print_head_InitENI)( VOID );
	VOID   (* const fj_print_head_KillENI)( VOID );

	// from fj_memstorage.c
	BOOL      (* const fj_MemStorDeleteElement)( struct fjmemstor * pfms, LPCSTR pType, LPCSTR pName );
	STRING ** (* const fj_MemStorFindAllElements)( const struct fjmemstor * const pfms, LPCSTR pType );

	// from fj_misc.c
	long  (* const fj_mktime)( struct tm *pTM );
	FLOAT (* const fj_FloatUnits)( FLOAT fInput, LONG lUnits );
	VOID  (* const fj_DoubleToStr)( LPSTR pStr, double dInput );
	VOID  (* const fj_IPToStr)( ULONG lInput, LPSTR pStr );
	ULONG (* const fj_StrToIP)( LPSTR pStr );
	VOID  (* const fj_processParameterStringInput)( LPSTR pInput );
	VOID  (* const fj_processParameterStringOutput)( LPSTR pOutput, LPCSTR pInput );
	LONG  (* const fj_ParseKeywordsFromStr)( LPSTR pStr, CHAR chDelim, LPSTR *pList, LONG lListSize );

	// from fj_font.c
	VOID            (* const fj_FontDestroy)( struct fjfont * pff );
	struct fjfont * (* const fj_FontFromBuffer)( LPBYTE pBuff );

	// from fj_element.c
	struct fjelement * (* const fj_ElementTextNew)( VOID );
	struct fjelement * (* const fj_ElementBarCodeNew)( VOID );
	struct fjelement * (* const fj_ElementDynTextNew)( VOID );
	struct fjelement * (* const fj_ElementDynBarCodeNew)( VOID );
	struct fjelement * (* const fj_ElementDataMatrixNew)( VOID );
	struct fjelement * (* const fj_ElementBitmapNew)( VOID );
	struct fjelement * (* const fj_ElementCounterNew)( VOID );
	struct fjelement * (* const fj_ElementDatetimeNew)( VOID );
	struct fjelement * (* const fj_ElementDynimageNew)( VOID );
	struct fjelement * (* const fj_ElementNewFromTypeString)( LPCSTR pType );
	struct fjelement * (* const fj_ElementDup)( const struct fjelement * const pfe );
	LONG               (* const fj_ElementListAdd)( struct fjelement ***ppapfeIn, LONG lCount, const struct fjelement * pfe );
	LONG               (* const fj_ElementListRemove)( struct fjelement ***ppapfeIn, LONG lCount, const struct fjelement * pfe );
	VOID               (* const fj_ElementListDestroyAll)( struct fjelement ***ppapfeIn, LONG lCount );
	VOID               (* const fj_ElementMakeMemstorName)( struct fjelement * pfe, LPSTR pName );

	// from fj_message.c
	struct fjmessage * (* const fj_MessageNew)( VOID );
	VOID               (* const fj_MessageDestroy)( struct fjmessage * pfm );
	VOID               (* const fj_MessageReset)( struct fjmessage * pfm );
	VOID               (* const fj_MessagePhotocellBuild)( struct fjmessage * pfm );
	VOID               (* const fj_MessageGetText)( const struct fjmessage * const pfm, LPSTR pString, LONG lStringLength );
	struct fjmessage * (* const fj_MessageBuildFromName)( const struct fjmemstor * const pMemStor, LPCSTR pName );
	struct fjmessage * (* const fj_MessageBuildFromString)( const struct fjmemstor * const pMemStor, LPCSTR pMessage );
	BOOL               (* const fj_MessageAddToMemstor)( struct fjmemstor * pMemStor, const struct fjmessage * const pfm );
	BOOL               (* const fj_MessageDeleteFromMemstor)( struct fjmemstor FAR *pMemStor, const struct fjmessage * const pfm );

	// from fj_printhead.c
	BOOL (* const fj_PrintHeadSetSerial)( struct fjprinthead * pfph, LPCSTR pInput );
	BOOL (* const fj_PrintHeadSetNameShort)( struct fjprinthead * pfph, LPCSTR pInput );
	BOOL (* const fj_PrintHeadSetNameLong)( struct fjprinthead * pfph, LPCSTR pInput );
	VOID (* const fj_PrintHeadReset)( struct fjprinthead * pfph );
	BOOL (* const fj_PrintHeadIsEdit)( const struct fjprinthead * const  pfph, LONG lEditID );

	// from fj_label.c
	struct fjlabel * (* const fj_LabelNew)( VOID );
	VOID             (* const fj_LabelDestroy)( struct fjlabel * pfl );
	struct fjlabel * (* const fj_LabelBuildFromString)( const struct fjmemstor FAR * const pMemStor, LPCSTR pLabel );
	struct fjlabel * (* const fj_LabelBuildFromName)( const struct fjmemstor FAR * const pMemStor, LPCSTR pName );
	BOOL             (* const fj_LabelAddToMemstor)( struct fjmemstor FAR * pMemStor, const struct fjlabel * const pfl );

	// from fj_system.c
										// table for unit description strings
	const struct fjsysunits ((* const pfj_aUnits)[]);
	struct fjlabel * (* const fj_SystemBuildSelectedLabel)( struct fjsystem * pfsys, LPCSTR pName );
	struct fjlabel * (* const fj_SystemBuildScannerDefaultLabel)( struct fjsystem * pfsys, LPCSTR pName );
	LPCSTR           (* const fj_SystemGetPasswordForLevel)( const struct fjsystem * const pfsys, LONG lLevel );
	LONG             (* const fj_SystemGetPasswordLevel)( const struct fjsystem * const pfsys, LPCSTR pPassword );
	VOID             (* const fj_SystemMakeLevelID)( const struct fjprinthead FAR * const pfph, LONG lLevel, ULONG lIPaddr, LPSTR pAuth );
	LONG             (* const fj_SystemGetLevelID)( struct fjprinthead * pfph, ULONG lIPaddr, LPSTR pAuth );
	struct tm *      (* const fj_SystemGetTime)( struct tm *pTM );
	LPSTR            (* const fj_SystemGetDateTimeFullString)( const struct fjsystem * const pfsys, LPSTR pString, struct tm *pTM );
	LPSTR            (* const fj_SystemGetDateTimeString)( LPSTR pString, const struct tm * const pTM );
	LPSTR            (* const fj_SystemGetDateString)( LPSTR pString, const struct tm * const pTM );
	LPSTR            (* const fj_SystemGetTimeNoSecString)( const struct fjsystem * const pfsys, LPSTR pString, const struct tm * const pTM );
	LPSTR            (* const fj_SystemGetMonthString)( const struct fjsystem * const pfsys, LPSTR pString, const struct tm * const pTM );
	LPSTR            (* const fj_SystemGetDayString)( const struct fjsystem * const pfsys, LPSTR pString, const struct tm * const pTM );
	LPSTR            (* const fj_SystemGetDayDateString)( const struct fjsystem * const pfsys, LPSTR pString, const struct tm * const pTM );
	struct tm *      (* const fj_SystemParseDateTime)( LPSTR pStr, struct tm *pTM );
	VOID			 (* const fj_SaveBRAMCounter)( const struct fjelement * pfe );
	VOID			 (* const fj_ResetBRAMCounters)( struct fj_bram * pBRAM);
	VOID			 (* const fj_RestoreBRAMCounters)( struct fjmessage * pfm );

	// from fj_rom.c
	// read FoxJet parameters from ROM
	int    (* const fj_ReadFJRomParams) ( struct fj_param * pFJ_PARAM );
	// write FoxJet parameters to ROM
	VOID   (* const fj_WriteFJRomParams)( struct fj_param * pFJ_PARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue );
	int    (* const fj_ReadNETOSRomParams)( devBoardParamsType * pFJ_NETOSPARAM );
	VOID   (* const fj_WriteNETOSRomParams)( VOID *pFJ_NETOSPARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue );

	// from url.c
	void (* const AppSearchURL  )(unsigned long handle, char *URLp);

	// from NetSilicon hservapi.h
	int           (* const HSSend) (unsigned long handle, const char *data);
	int           (* const HSSendBinary) (unsigned long handle, char *datap, int len);
	int           (* const HSGetValue) (unsigned long handle, char *namep, char *valuep, int maxlen);
	unsigned long (* const HSRemoteAddress) (unsigned long handle);
	void          (* const HSTypeHtml) (unsigned long handle);
	void          (* const HSTypeApplet) (unsigned long handle, int length);
	void          (* const HSTypeJpeg) (unsigned long handle, int length);

	// from tx
	UINT        (*tx_semaphore_get)(TX_SEMAPHORE *semaphore_ptr, ULONG wait_option);
	UINT        (*tx_semaphore_put)(TX_SEMAPHORE *semaphore_ptr);
	UINT        (*tx_thread_resume)(TX_THREAD *thread_ptr);

} FJ_CVT, FAR *LPFJ_CVT, FAR * const CLPFJ_CVT;
typedef const struct fj_cvt FAR *LPCFJ_CVT, FAR * const CLPCFJ_CVT;

#define FJ_CVT_ADDRESS  0x4000
#define pCVT            ((LPFJ_CVT)FJ_CVT_ADDRESS)
#endif									// _WINDOWS
#endif FJ_CVT_H
