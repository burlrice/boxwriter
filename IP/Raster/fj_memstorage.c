#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#endif

#include "fj_mem.h"
#include "fj_memstorage.h"

//
//	FUNCTION: fj_MemStorNew
//
//	Return: Pointer to initialized FJMEMSTOR
//
FJDLLExport LPFJMEMSTOR fj_MemStorNew( LPBYTE pMemStorBuffer, LONG lLength )
{
	LPFJMEMSTOR pfms;

	pfms = fj_malloc( sizeof(FJMEMSTOR) );

	pfms->pBuffer = pMemStorBuffer;
	pfms->lSize   = lLength;
	pfms->bDoNotFreeBuffer = FALSE;		// set TRUE in IP. default FALSE for use in an Editor or other program emulating IP on Windows or other systems.
	pfms->lNext   = 0;
	pfms->pList   = NULL;				// pointer to first list of elements

	return( pfms );
}
//
//	FUNCTION: fj_MemStorNewFromBuffer
//
//	Return: Pointer to initialized FJMEMSTOR
//
FJDLLExport LPFJMEMSTOR fj_MemStorNewFromBuffer( LPBYTE pMemStorBuffer, LONG lLength, LPBYTE pInBuffer )
{
	LPFJMEMSTOR pfms;
	LPBYTE  pTemp;
	LONG    lOffset;
	LONG    lLen;

	pfms = fj_malloc( sizeof(FJMEMSTOR) );

	pfms->pBuffer = pMemStorBuffer;
	pfms->lSize   = lLength;
	pfms->bDoNotFreeBuffer = FALSE;		// set TRUE in IP. default FALSE for use in an Editor or other program emulating IP on Windows or other systems.
	pfms->lNext   = 0;
	pfms->pList   = NULL;				// pointer to first list of elements

	memcpy( pfms->pBuffer, pInBuffer, pfms->lSize );

	lLength -= 4;						// leave room for a final NULL entry
	lOffset = 0;
	while ( (lOffset < lLength) && (0 == pfms->lNext) )
	{
		pTemp = pfms->pBuffer + lOffset;
		memcpy( &lLen, pTemp, sizeof(LONG) );
		if ( 0 == lLen )
		{
			pfms->lNext = lOffset;
			break;
		}
		lOffset += lLen;
	}
	return( pfms );
}
//
//	FUNCTION: fj_MemStorDestroyLists
//
//	Return: None
//
//  Destroy all list in FJMEMSTOR
//
static NEAR VOID fj_MemStorDestroyLists( LPFJMEMSTOR pfms )
{
	LPFJMEMSTORLIST pList;
	LPFJMEMSTORLIST pListNext;

	if ( NULL != pfms )
	{
		pList = pfms->pList;
		pfms->pList = NULL;
		while ( NULL != pList )
		{
			if ( NULL != pList->papElements ) fj_free( pList->papElements );
			pListNext = pList->pNext;
			fj_free( pList );
			pList = pListNext;
		}
	}

	return;
}
//
//	FUNCTION: fj_MemStorClear
//
//
FJDLLExport VOID fj_MemStorClear( LPFJMEMSTOR pfms )
{
	fj_MemStorDestroyLists( pfms );
	pfms->lNext = 0;
	memset( pfms->pBuffer, 0, pfms->lSize );

	return;
}
//
//	FUNCTION: fj_MemStorLoadFromBuffer
//
//	Return: Pointer to initialized FJMEMSTOR
//
//  NOTE: most of the code here to test for bad values and clear the MemStor,
//        is only needed when changing ROM mapping or ROM definitions.
//
FJDLLExport LPFJMEMSTOR fj_MemStorLoadFromBuffer( LPFJMEMSTOR pfms, LPBYTE pBuffer )
{
	LPBYTE  pTemp;
	LONG    lLength;
	LONG    lOffset;
	ULONG   lLen;

	fj_MemStorDestroyLists( pfms );
	pfms->lNext = 0;
	lLength = pfms->lSize;
	memcpy( pfms->pBuffer, pBuffer, lLength );

	lLength -= 4;						// leave room for a final NULL entry
	lOffset = 0;
	while ( (lOffset < lLength) && (0 == pfms->lNext) )
	{
		pTemp = pfms->pBuffer + lOffset;
		memcpy( &lLen, pTemp, sizeof(LONG) );
		if ( 0 == lLen )
		{
			pfms->lNext = lOffset;
			break;
		}
		else if ( lLen > 512 * 1024 )	// no known MemStor object larger than this
		{
			lLen = 0;
			memcpy( pTemp, &lLen, sizeof(LONG) );
			break;
		}
		lOffset += lLen;
	}
	if ( 0 == pfms->lNext )				// if no good length/object found,
	{
										// clear it all!
		memset( pfms->pBuffer, 0, pfms->lSize );
	}
	return( pfms );
}
//
//	FUNCTION: fj_MemStorDestroy
//
//	Return: None
//
//  Destroy a FJMEMSTOR
//
FJDLLExport VOID  fj_MemStorDestroy( LPFJMEMSTOR pfms )
{
	if ( NULL != pfms )
	{
		fj_MemStorDestroyLists( pfms );
		if ( FALSE == pfms->bDoNotFreeBuffer ) fj_free( pfms->pBuffer );
		fj_free( pfms );
	}

	return;
}
//
//	FUNCTION: fj_MemStorBuildList
//
//	Return: None
//
//  Build a list of Elements for a given type
//
static NEAR LPFJMEMSTORLIST fj_MemStorBuildList( CLPCFJMEMSTOR pfms, LPCSTR pType )
{
	LPFJMEMSTORLIST pList;
	LPSTR *papElements;
	LPSTR *papElementsNew;
	LPSTR pElement;
	LPSTR pName;
	LPSTR pName2;
	LONG  lTypeLen;
	LONG  lListSize;
	LONG  lOffset;
	LONG  lLen;
	int   i,j,m,n;

	lListSize = 100;					// initial list size
	pList = fj_malloc( sizeof(FJMEMSTORLIST) );
	papElements = fj_calloc( lListSize, sizeof(LPSTR) );

	pList->pNext       = NULL;			// Pointer to next list
	strcpy( pList->sName, pType );		// Name string of element type
	pList->lCount      = 0;				// Number of element pointers in list - the actual non-NULL entries
	pList->lMaxCount   = lListSize;		// Number of element pointers in list - the actual size of the list
	pList->papElements = papElements;	// Pointer to array of pointers to elements in MemStor

	strcat( pList->sName, "," );		// add a comma
	lTypeLen = strlen( pList->sName );	// length includes comma

	lOffset = 0;
	while ( lOffset < pfms->lNext )
	{
		pElement = (LPSTR)pfms->pBuffer + lOffset;

		memcpy( &lLen, pElement, sizeof(LONG) );
		pElement += sizeof(LONG);
		// skip the opening '{'. include the comma at the end.
		if ( 0 == memcmp( pList->sName, (LPSTR)(pElement+1) , lTypeLen ) )
		{
										// if list is full, get a larger one
			if ( (pList->lCount+1) == pList->lMaxCount )
			{
				n = pList->lCount * 2;
				papElementsNew = fj_calloc( n, sizeof(LPSTR) );
				memcpy( papElementsNew, papElements, pList->lCount * sizeof(LPSTR) );
				fj_free( papElements );
				pList->papElements = papElements = papElementsNew;
				pList->lMaxCount = n;
			}
			if ( 0 == pList->lCount )
			{
				*(papElements+pList->lCount) = (LPSTR)pElement;
			}
			else
			{
				pName = pElement + 1 + lTypeLen;
				n = 0;
				while ( ',' != *(pName+n) ) n++;
				i = 0;
				m = -1;
				while ( i < pList->lCount )
				{
					pName2 = (*(papElements+i))+lTypeLen+1;
										// compare names
					m = memcmp( pName, pName2 , n+1 );
					if ( 0 < m ) i++;
					else         break;
				}
				for ( j = pList->lCount; j >= i; j-- ) *(papElements+j+1) = *(papElements+j);
				*(papElements+i) = pElement;
			}
			pList->lCount++;
		}
		lOffset += lLen;
	}

	*(pList->sName + lTypeLen - 1) = 0;	// remove comma

	pList->pNext = pfms->pList;			// chain to current list
	((LPFJMEMSTOR)pfms)->pList  = pList;// make this the first list

	return( pList );
}
//
//	FUNCTION: fj_MemStorFindElement1
//
//	Return: if element found, LPBYTE to element, else NULL
//
//  Find element in FJMEMSTOR
//
static NEAR LPBYTE fj_MemStorFindElement1( CLPCFJMEMSTOR pfms, LPCSTR pTypeName, LONG lTypeNameLen )
{
	LPBYTE  pElement;
	LPBYTE  pTemp;
	LONG    lOffset;
	LONG    lLen;

	pElement = NULL;
	if ( (NULL != pfms) && (NULL != pTypeName) && (0 < lTypeNameLen) )
	{
		lOffset = 0;

		while ( (lOffset < pfms->lNext) && (NULL == pElement) )
		{
			pTemp = pfms->pBuffer + lOffset;
			memcpy( &lLen, pTemp, sizeof(LONG) );
			pTemp += sizeof(LONG);
			// skip the opening '{'
			if ( 0 == memcmp( pTypeName, (LPSTR)(pTemp+1) , lTypeNameLen ) )
			{
				pElement = pTemp;
			}

			lOffset += lLen;
		}
	}
	return( pElement );
}
//
//	FUNCTION: fj_MemStorFindElement
//
//	Return: if element found, LPBYTE to element, else NULL
//
//  Find element in FJMEMSTOR
//
FJDLLExport LPBYTE fj_MemStorFindElement( CLPCFJMEMSTOR pfms, LPCSTR pType, LPCSTR pName )
{
	LPFJMEMSTORLIST pList;
	LPFJMEMSTORLIST pListLast;
	LPBYTE  pElement;
	LONG    lTypeLen;
	LONG    lNameLen;
	CHAR    sName[50];
	int i;

	pElement = NULL;
	if ( (NULL != pfms) && (NULL != pType) && (NULL != pName) )
	{
		lTypeLen = strlen( pType );

		pListLast = NULL;
		pList = pfms->pList;
		while ( NULL != pList )
		{
			if ( 0 == strcmp(pType, pList->sName) ) break;
			pListLast = pList;			// save the last pList in case we are at the end
			pList = pList->pNext;
		}
		if ( NULL == pList )			// if no list,
		{
										// build one
			pList = fj_MemStorBuildList( pfms, pType );
		}

		if ( NULL != pList )			// if there is a list,
		{
			strcpy( sName, pName );
			strcat( sName, "," );
			lNameLen = strlen( sName );
			i = 0;
			while ( i < pList->lCount )	// search it.
			{
				// compare names
				if ( 0 == memcmp(sName, (*(pList->papElements+i))+2+lTypeLen , lNameLen )   )
				{
					// names equal. set return value.
					pElement = (LPBYTE)*(pList->papElements+i);
					break;
				}
				i++;
			}
		}
	}

	return( pElement );
}
//
//	FUNCTION: fj_MemStorFindAllElements1
//
//	Return: if element found, LPBYTE to element, else NULL
//
//  Find all matching elements in FJMEMSTOR
//  List is sorted on name
//
FJDLLExport LONG fj_MemStorFindAllElements1( CLPCFJMEMSTOR pfms, LPCSTR pType, LPSTR *pList, LONG lListSize )
{
	LONG   lCount;
	LONG   lOffset;
	LONG   lLen;
	LPBYTE pElement;
	LPBYTE pName;
	CHAR   str[50];
	LONG   lStrLen;
	int    i;
	int    j;
	int    m;
	int    n;

	lCount = 0;

	if ( (NULL != pfms) && (NULL != pType) && (NULL != pList) && (0 < lListSize) )
	{
		strcpy( str, pType );
		strcat( str, "," );
		lStrLen = strlen( str );
		lOffset = 0;
		while ( (lOffset < pfms->lNext) && (lCount < lListSize) )
		{
			pElement = pfms->pBuffer + lOffset;
			memcpy( &lLen, pElement, sizeof(LONG) );
			pElement += sizeof(LONG);
			// skip the opening '{'. include the comma at the end.
			if ( 0 == memcmp( str, (LPSTR)(pElement+1) , lStrLen ) )
			{
				if ( 0 == lCount )
				{
					*(pList+lCount) = (LPSTR)pElement;
				}
				else
				{
					pName = pElement + 1 + lStrLen;
					n = 0;
					while ( ',' != *(pName+n) ) n++;
					i = 0;
					m = -1;
					while ( i < lCount )
					{
						// compare names
						m = memcmp( pName, (*(pList+i))+lStrLen , n+1 );
						if ( 0 < m ) i++;
						else         break;
					}
					for ( j = lCount; j > i; j-- ) *(pList+j+1) = *(pList+j);
					*(pList+i) = (LPSTR)pElement;
				}
				lCount++;
			}
			lOffset += lLen;
		}
	}

	return ( lCount );
}
//
//	FUNCTION: fj_MemStorFindAllElements
//
//	Return: if element found, LPBYTE to element, else NULL
//
//  Find all matching elements in FJMEMSTOR
//  List is sorted on name
//
FJDLLExport LPSTR * fj_MemStorFindAllElements( CLPCFJMEMSTOR pfms, LPCSTR pType )
{
	LPFJMEMSTORLIST pList;
	LPFJMEMSTORLIST pListLast;
	LPSTR * pListRet;

	pListLast = NULL;
	pList = pfms->pList;
	while ( NULL != pList )
	{
		if ( 0 == strcmp(pType, pList->sName) ) break;
		pListLast = pList;				// save the last pList in case we are at the end
		pList = pList->pNext;
	}
	if ( NULL == pList )				// if no list,
	{
										// build one
		pList = fj_MemStorBuildList( pfms, pType );
	}

	if ( NULL == pList ) pListRet = NULL;
	else                 pListRet = pList->papElements;

	return ( pListRet );
}
//
//	FUNCTION: fj_MemStorDeleteElement
//
//	Return: TRUE if success.
//
//  Delete element out of FJMEMSTOR
//
FJDLLExport BOOL fj_MemStorDeleteElement( LPFJMEMSTOR pfms, LPCSTR pType, LPCSTR pName )
{
	LPBYTE pElement;
	LPBYTE pSize;
	LONG   lSize;
	BOOL   bRet;

	bRet = FALSE;
	pElement = NULL;

	if ( (NULL != pfms) && (NULL != pType) && (NULL != pName) )
	{
		pElement = (LPBYTE)fj_MemStorFindElement( pfms, pType, pName );
	}
	if ( NULL != pElement )
	{
		pSize = pElement - sizeof(LONG);
		memcpy( &lSize, pSize, sizeof(LONG) );
		memcpy( pSize, pSize+lSize, pfms->lNext - ( pSize - pfms->pBuffer ) );
		pfms->lNext -= lSize;			// new offset
		memset( pfms->pBuffer+pfms->lNext, 0, lSize );

		fj_MemStorDestroyLists( pfms );	// destroy all old lists

		bRet = TRUE;
	}
	return( bRet );
}
//
//	FUNCTION: fj_MemStorDeleteElement1
//
//	Return: TRUE if success.
//
//  Delete element out of FJMEMSTOR
//
FJDLLExport BOOL fj_MemStorDeleteElement1( LPFJMEMSTOR pfms, LPCSTR pTypeName, LONG lTypeNameLen )
{
	LPBYTE pElement;
	LPBYTE pSize;
	LONG   lSize;
	BOOL   bRet;

	bRet = FALSE;
	pElement = NULL;

	if ( (NULL != pfms) && (NULL != pTypeName) )
	{
		pElement = (LPBYTE)fj_MemStorFindElement1( pfms, pTypeName, lTypeNameLen );
	}
	if ( NULL != pElement )
	{
		pSize = pElement - sizeof(LONG);
		memcpy( &lSize, pSize, sizeof(LONG) );
		memcpy( pSize, pSize+lSize, pfms->lNext - ( pSize - pfms->pBuffer ) );
		pfms->lNext -= lSize;			// new offset
		memset( pfms->pBuffer+pfms->lNext, 0, lSize );

		fj_MemStorDestroyLists( pfms );	// destroy all old lists

		bRet = TRUE;
	}
	return( bRet );
}
//
//	FUNCTION: fj_MemStorAddElementBinary
//
//	Return: TRUE if success.
//          FALSE if no memory available.
//
//  Add element to FJMEMSTOR
//
//  The Element buffer must start with a string of the form "{Type,Name,....}"
//  The length is the total length, including the leading string.
//
FJDLLExport BOOL fj_MemStorAddElementBinary( LPFJMEMSTOR pfms, LPBYTE pData, LONG lDataLen )
{
	LPSTR  pStr;
	LPSTR  pFind;
	LONG   lStrLen;
	LONG   lLen;
	LONG   lSize;
	int    i;
	LPBYTE pNext;
	BOOL   bRet;

	bRet = FALSE;

	if ( NULL != pfms )
	{
		pStr = (LPSTR)pData;
		lStrLen = strlen( pStr );
		lLen = 0;
		i = 0;
		while ( (i != 2) && (lLen <= lStrLen) )
		{
			if ( ',' == *(pStr+lLen) ) i++;
			lLen++;
		}
		lLen++;
		pFind = (LPSTR)fj_MemStorFindElement1( pfms, pStr+1, lLen-1 );
		if ( NULL != pFind )
		{
			if ( 0 != strcmp(pFind, pStr) )
			{
				fj_MemStorDeleteElement1( pfms, pStr+1, lLen-1 );
				pFind = NULL;
			}
			else bRet  = TRUE;
		}
		if ( NULL == pFind )
		{
			// Calculate size of element in MemStor
			lSize = lDataLen + sizeof (LONG);
			// Check avalable memory space in buf
			if ( pfms->lNext + lSize <= pfms->lSize - 4 )
			{
										// pointer to memory for element
				pNext = pfms->pBuffer + pfms->lNext;
				pfms->lNext += lSize;	// new offset

				memcpy( pNext, &lSize, sizeof(LONG) );
				pNext += sizeof(LONG);
				memcpy( pNext, pData, lDataLen );

										// destroy all old lists
				fj_MemStorDestroyLists( pfms );

				bRet = TRUE;
			}
		}
	}
	return( bRet );
}
//
//	FUNCTION: fj_MemStorAddElementString
//
//	Return: TRUE if success.
//          FALSE if no memory available.
//
//  Add element to FJMEMSTOR
//
//  String must be of form "{Type,Name,....}"
//
FJDLLExport BOOL fj_MemStorAddElementString( LPFJMEMSTOR pfms, LPCSTR pStr )
{
	CHAR   ch;
	LONG   lLen;
	LONG   lSize;
	LPSTR  pFind;
	LPBYTE pNext;
	BOOL   bRet;

	bRet = FALSE;

	lLen = 0;
	lSize = 0;
	ch = 1;

	if ( NULL != pfms )
	{
		while ( (lSize < 2) && ( 0 != ch ) )
		{
			ch = *(pStr+lLen);
			lLen++;
			if ( ',' == ch ) lSize++;
		}

		pFind = (LPSTR)fj_MemStorFindElement1( pfms, pStr+1, lLen-1 );
		if ( NULL != pFind )
		{
			if ( 0 != strcmp(pFind, pStr) )
			{
				fj_MemStorDeleteElement1( pfms, pStr+1, lLen-1 );
				pFind = NULL;
			}
			else bRet  = TRUE;
		}
		if ( NULL == pFind )
		{
			// Calculate size of element in MemStor
			lLen  = 1 + strlen(pStr);
			lSize = lLen + sizeof(LONG);
			// Check avalable memory space in buf
			if ( pfms->lNext + lSize <= pfms->lSize - 4 )
			{
										// pointer to memory for element
				pNext = pfms->pBuffer + pfms->lNext;
				pfms->lNext += lSize;	// new offset

				memcpy( pNext, &lSize, sizeof(LONG) );
				pNext += sizeof(LONG);
				memcpy( pNext, pStr, lLen );

										// destroy all old lists
				fj_MemStorDestroyLists( pfms );

				bRet = TRUE;
			}
		}
	}
	return( bRet );
}
