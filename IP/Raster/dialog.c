/*
*            Copyright (c) 1998-2000 by NETsilicon Inc.
*
*  This software is copyrighted by and is the sole property of
*  NETsilicon.  All rights, title, ownership, or other interests
*  in the software remain the property of NETsilicon.  This
*  software may only be used in accordance with the corresponding
*  license agreement.  Any unauthorized use, duplication, transmission,
*  distribution, or disclosure of this software is expressly forbidden.
*
*  This Copyright notice may not be removed or modified without prior
*  written consent of NETsilicon.
*
*  NETsilicon, reserves the right to modify this software
*  without notice.
*
*  NETsilicon
*  411 Waverley Oaks Road                  USA 781.647.1234
*  Suite 227                               http://www.netsilicon.com
*  Waltham, MA 02452                       AmericaSales@netsilicon.com
*
*************************************************************************
*
*     Module Name: dialog.c
*         Version: 1.00
*   Original Date: 10/06/99
*          Author: Li Zhen
*   Original Date: 10-Feb-2000
*          Author:
*        Language: Ansi C
* Compile Options:
* Compile defines:
*       Libraries:
*    Link Options:
*
*    Entry Points: NADialog()
*
* Description.
* =======================================================================
*    This file contains startup dialog routines.
*
*
* Edit Date/Ver   Edit Description
* ==============  ===================================================
* LZ   02/08/00   added "printf("APPLICATION: %s\r\n", APP_DIALOG_APP_NAME)"
* LZ   02/16/00   changed "Copyright (c) 1999" to "Copyright (c) 2000"
* SW   03/06/00   cast return value of getchar() to char avoid a run time error
* LZ   04/05/00   Added support for little endian.
* LZ   06/06/00   Added more comments for NVM.
*                 Added code to handle no NVM case.
* DXL	07/06/00   netosConfigDialog () return -1 if APP_DIALOG_PORT is not defined
* LZ   07/12/00   if !nvParams.useDhcp, then print IP addresses.
* PMJ  10/18/00   Updated to support dynamic PPP NVRAM parameters.
* PMJ  10/19/00   Corrected problems with mislabeling of COM1 ports to COM2
* PMJ  10/26/00   Removed private function NAResolve_MAC in place of public API.
* NGUR 08/13/01   Add useAppConfh.
* PMJ  03/20/02   Added root password protection to the system dialog on modification.
* PMJ  03/22/02   Included dialog capabilities for root password, administrators
*                 password, Fixed Key setting and Kerberos parameters.
* PMJ  03/29/02   Included dialog capabilities for SNTP Servers.
* PMJ  04/11/02   Included dialog capabilities for the Kerberos parameters: KDC address,
*                 username, password, and realmname.   Cleaned up some application
*                 issues with the dialog.
* PMJ  04/23/02   Corrected compiler Warnings.
* SW   04/30/02   Added GNU support for redirecting std I/O
*/

#include <tx_api.h>
#include <appconf.h>
#include <narmsrln.h>
#include <netosIo.h>
#include <netosErr.h>

#ifdef FOXJET
#include <netos_serl.h>
#include <serParam.h>
#include "fj.h"
extern    BOOL    bDialogParamChange;
extern BOOL fj_writeROMMsg; // burl
#endif

#include <sysAccess.h>
#include <dialog.h>
#include <NASecurity.h>
#ifdef NETOS_GNU_TOOLS
#include <stdio.h>
#include <unistd.h>
extern void   _EXFUN(__sinit,(struct _reent *));
#define CHECK_INIT(fp) \
	do \
{ \
	if ((fp)->_data == 0) \
	(fp)->_data = _REENT; \
	if (!(fp)->_data->__sdidinit) \
	__sinit ((fp)->_data); \
} \
	while (0)
/* need to save cookies, and read/write only */
#define SAVE_IO_FOR_DIALOG          void *readFn, *writeFn; \
	CHECK_INIT(stdin); CHECK_INIT(stdout); CHECK_INIT(stderr);\
	readFn = stdin->_read; writeFn= stdout->_write;\
	inch = (int)stdin->_cookie; outch=(int)stdout->_cookie;errch=(int)stderr->_cookie;
#define RESTORE_IO               stdin->_read = (void*)readFn;stdout->_write= (void*)writeFn;\
	stdin->_cookie=(_PTR)inch; stdout->_cookie=(_PTR)outch;stderr->_cookie=(_PTR)errch;
#endif

#define SERIALNUMBER_LENGTH     8
#define MAX_IPADDRESS_LENGTH    15
#define SIO_BAUD_SET        0x1003

extern devBoardParamsType defaultParams;

#ifdef APP_DIALOG_PORT
static int checkSerialNumber(char *buffer)
{
	int i;

	i= strlen(buffer);
	if (i != SERIALNUMBER_LENGTH)
	{
		return 0;
	}
	else
	{
		for (i=0; i<SERIALNUMBER_LENGTH; i++)
		{
			if (!(isdigit(buffer[i])))
			{
				return 0;
			}
		}
	}

	return 1;
}
#ifndef FOXJET
static
#endif
int  checkIpAddress(char *buffer)
{
	unsigned int length,i;
	unsigned int j =0;
	unsigned int val;

	length = strlen(buffer);

	if ( (length > MAX_IPADDRESS_LENGTH))
		return 0;
	else
	{
		for (i=0; i<length; i++)
		{

			if (buffer[i] =='.')
			{
				j++;
				if (j > 3)				/* only three '.' allowed */
					return 0;
			}
			/* all input char must be numeric */
			else if (!(isdigit(buffer[i])))
				return 0;
		}

		for (i = 0; i < 4; i++)
		{
			if (*buffer != '\0')
			{
				val = 0;
				while (*buffer != '\0' && *buffer != '.')
				{
					val *= 10;
					val += *buffer - '0';
					buffer++;
				}
				if (val > 255)
					return 0;			/* any field should be less than 256 */

				if (*buffer != '\0')
					buffer++;
			}
		}
	}
	return 1;
}
static void printIpAddress(char *buffer, unsigned long ip)
{
#ifndef _LITTLE_ENDIAN
	printf(buffer, (ip >> 24) & 0xFF,
		(ip >> 16) & 0xFF,
		(ip >> 8)  & 0xFF,
		(ip) & 0xFF);
#else
	printf(buffer, (ip) & 0xFF,
		(ip >> 8)  & 0xFF,
		(ip >> 16) & 0xFF,
		(ip >> 24) & 0xFF);
#endif
}
static void printMacAddress(char *buffer, char * mac)
{
	printf(buffer, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}
static char checkKeyboardInput(unsigned long timeout)
{
	int i;
	char rc;

	for (i=0; i < timeout; i++)
	{
		tx_thread_sleep(100);
		rc=0;
		rc= (char) getchar();
		if ((rc != 0xff) && (rc != '\n'))
			return (rc);
	}
	return (rc);
}
static void readLine(char * buf)
{
	char character;
	int i = 0;
	do
	{
		character = getchar();
		if (character == '\b')
		{
			if (i > 0)
			{
				putchar(character);
				putchar(0x20);			/*space*/
				putchar(character);
				--i;
			}
		}

		else if (character != '\n')
		{
			buf[i] = character;
			putchar(character);
			++i;
		}
	}
	while (character != '\r');
	putchar('\n');

	buf[i-1] = '\0';
}
static void readPassword(char * buf)
{
	char character;
	int i = 0;
	do
	{
		character = getchar();
		if (character == '\b')
		{
			if (i > 0)
			{
				putchar(character);
				putchar(0x20);			/*space*/
				putchar(character);
				--i;
			}
		}
		else if ((character != '\r') && (character != '\n'))
		{
			buf[i] = character;
			putchar('*');
			++i;
		}
	}
	while ((character != '\r') && (character != '\n'));
	putchar('\r');
	putchar('\n');

	buf[i] = '\0';
}
/*
*
*  Function: static int netosConfigDialog(int)
*
*  Description:
*
*      This routine sets up stdin, stdout, and stderr to be redirected
*      out the APP_DIALOG_PORT if APP_DIALOG_PORT is defined.
*
*  Parameters:
*
*      0: NONBLOCK I/O
*      1: BLOCK I/O
*
*  Return Values:
*
*      channel ID
*
*/

static int netosConfigDialog (int flag)
{
	int fd;

	/* open device */
	if (flag == 0)
		fd = open (APP_DIALOG_PORT, (O_RDWR | O_NONBLOCK) );
	else
		fd = open (APP_DIALOG_PORT, O_RDWR );

	if (fd <0)
	{
		netosFatalError("APP_DIALOG_PORT improperly defined", ERROR_CLASS_BSP, ERROR_BAD_DIALOG_PORT);
		return -1;
	}
#ifdef FOXJET
	{
		unsigned long lSet;
		int iRet;
		iRet = ioctl( fd, SIO_HW_OPTS_GET, &lSet );
		lSet |= SIO_SW_HANDSHAKING;
		iRet = ioctl( fd, SIO_HW_OPTS_GET, &lSet );
	}
#endif

#ifndef NETOS_GNU_TOOLS
	stdin->io_channel = fd;
	stdout->io_channel = fd;
	stderr->io_channel = fd;
#else
	CHECK_INIT(stdin);
	CHECK_INIT(stdout);
	CHECK_INIT(stderr);

	/* need to override the following so that
	std I/O will call into DDI */
	stdout->_cookie = (_PTR)fd;
	stdin->_cookie = (_PTR)fd;
	stderr->_cookie = (_PTR)fd;
	stdin->_read = (void*)read;
	stdout->_write = (void*)write;
	stderr->_write = (void*)write;
#endif
	return fd;
}
/*
*
*  Function: static int dialogLocal (void)
*
*  Description:
*
*      This routine displays the target configuration and prompts the user
*      for modifications. And it will modify the configuration if the user
*      wants so.
*
*  Parameters:
*
*      none
*
*  Return Values:
*
*      0: timeout or accept the settings
*      1: configuration has been modified
*
*/
static int dialogLocal (void)

{
	int rc,fd;
	char buffer[128];
	char temp, s[20];
	char password[NASYSACC_STRLEN_PASSWORD];
	char mac_addrp[6];
	unsigned long ipaddress, baudrate;
	devBoardParamsType nvParams;
	unsigned inch, outch, errch;
	static int once = 0;

#ifdef FOXJET
	int  i, wait;
#endif

	static int notAuthenticated = 1;
#ifndef NETOS_GNU_TOOLS
	inch = stdin->io_channel;
	outch = stdout->io_channel;
	errch = stderr->io_channel;
#else
	SAVE_IO_FOR_DIALOG
#endif

		/* 
		*  NAReadDevBoardParams() reads the contents of the section of NVRAM that contains
		*  the development board's startup parameters if APP_USE_NVRAM flag is set to a nonzero value.
		*  If APP_USE_NVRAM is 0, NAReadDevBoardParams() reads the default parameters as defined
		*  in appconfig.h. NAReadDevBoardParams() and default data struct are in src\narmcore\narmsrln.c.
		*/
		NAReadDevBoardParams (&nvParams);

	/* if we have bad baudrate by any chance, then set it to default 9600
	to get things going */
	baudrate = nvParams.baudrate;
	if ((baudrate != 1200) && (baudrate != 2400) && (baudrate != 4800) && (baudrate != 9600)
		&& (baudrate != 19200) && (baudrate != 38400) && (baudrate != 57600) && (baudrate != 115200))
	{
		nvParams.baudrate = defaultParams.baudrate;

	}

	/* if we switch the same board for little endian and big endian, we have to be careful
	with the wait time because the change of endianness.
	*/
	if (nvParams.waitTime > 60)
	{
		nvParams.waitTime = 60;			/* default to 60 seconds */
	}

	fd = netosConfigDialog(0);			/* nonblock */
	if (once == 0)
	{
		/* set baudrate */
		rc=ioctl(fd, SIO_BAUD_SET, &nvParams.baudrate);

		if (rc < 0)
		{
			netosFatalError("error when change baud rate in Dialog.c", ERROR_CLASS_BSP, ERROR_INVALID_DIALOG_BAUDRATE);
		}

		once = 1;
	}

#ifdef FOXJET
	// drain any erroneous data off a bad serial input before going into blocked I/O
	{
		temp = 'A';
		while ((temp != 0xff) && (temp != EOF))
			temp = (char)getchar();
	}
#endif
	printf("\r\n");
	printf("\r\n");
	printf("\r\n");
#ifdef FOXJET
	printf("%s    Version %s  Copyright (c) 2002, FoxJet, Inc.\r\n",  "FoxJet", fj_getVersionNumber());
#endif
	printf("NET+WORKS Version %s\r\n", NETOS_VERSION_NUMBER);
	printf("Copyright (c) 2000, NETsilicon, Inc.\r\n");
	printf("\r\n");
	printf("APPLICATION: %s\r\n", APP_DIALOG_APP_NAME);
	printf("-----------------------------------------------------------------------------\r\n");
	printf("NETWORK INTERFACE PARAMETERS:\r\n");

	if (!nvParams.useDhcp)
	{
		printIpAddress("  IP address on LAN is %d.%d.%d.%d\r\n",nvParams.ipAddress);
		printIpAddress("  LAN interface's subnet mask is %d.%d.%d.%d\r\n", nvParams.subnetMask);
		printIpAddress("  IP address of default gateway to other networks is %d.%d.%d.%d\r\n",nvParams.gateway);
	}
	else
	{
		printf("  The board will use DHCP to obtain IP address, subnet mask and default gateway\r\n");
	}

	/*  New PPP parameters allows backward compatibility with older applications    */
#ifndef APP_ENABLE_PPP1
#define APP_ENABLE_PPP1         0
#endif
	if (APP_ENABLE_PPP1)
	{
		printIpAddress("  IP address on COM1 is %d.%d.%d.%d\r\n", nvParams.ipAddrPpp1);

		if (nvParams.ipAddrPpp1peer)
		{
			printIpAddress("  Peer IP address connected to COM1 is %d.%d.%d.%d\r\n", nvParams.ipAddrPpp1peer);
		}
	}

#ifndef APP_ENABLE_PPP2
#define APP_ENABLE_PPP2         0
#endif
	if (APP_ENABLE_PPP2)
	{
		printIpAddress("  IP address on COM2 is %d.%d.%d.%d\r\n", nvParams.ipAddrPpp2);

		if (nvParams.ipAddrPpp2peer)
		{
			printIpAddress("  Peer IP address connected to COM2 is %d.%d.%d.%d\r\n", nvParams.ipAddrPpp2peer);
		}
	}

#ifdef APP_USE_NETWORK_TIME_PROTOCOL
	if (!nvParams.useDhcp)
	{
		if (nvParams.ipAddrSntp1 != 0)
		{
			printIpAddress("  Primary SNTP Server IP address is %d.%d.%d.%d\r\n", nvParams.ipAddrSntp1);
			if (nvParams.ipAddrSntp2 != 0)
			{
				printIpAddress("  Secondary SNTP Server IP address is %d.%d.%d.%d\r\n", nvParams.ipAddrSntp2);
			}
		}
	}
#endif

#ifdef APP_USE_KERBEROS
	printf          ("SECURITY PARAMETERS:\r\n");
	printIpAddress  ("  Kerberos KDC IP Address is %d.%d.%d.%d\r\n", nvParams.krbIpAddr);
	printf          ("  Kerberos realm name is %s\r\n", nvParams.krbRealmname );
	printf          ("  Kerberos User name is %s\r\n", nvParams.krbUsername );
#endif

	printf("HARDWARE PARAMETERS:\r\n");
	printf("  Serial channels will use a baud rate of %d\r\n",nvParams.baudrate);

	memcpy(s,  &nvParams.serialNumber[0],sizeof(nvParams.serialNumber));
	s[8]=0;								/*for correct printf %s format*/
	printf("  This board's serial number is %s\r\n", s);
#ifdef FOXJET
	NASerialnum_to_mac ((char *)&s[0], mac_addrp);
#else if
	NASerialnum_to_mac ((char *)&s[2], mac_addrp);
#endif
	printMacAddress("  This board's MAC Address is %x:%x:%x:%x:%x:%x\r\n",mac_addrp);
	printf("  After board is reset, start-up code will wait %d seconds\r\n", nvParams.waitTime);
	printf("-----------------------------------------------------------------------------\r\n");
	printf("Hit any key to change board settings\r\n");
	if (APP_USE_NVRAM)
	{
		printf("Press any key in %d seconds to change these settings.\r\n", nvParams.waitTime);
	}
	printf("\r\n");
	printf("\r\n");

#ifdef FOXJET
	// drain any erroneous data off a bad serial input before going into blocked I/O
	{
		temp = 'A';
		while ((temp != 0xff) && (temp != EOF))
			temp = (char)getchar();
	}
#endif

#ifdef FOXJET
	wait = 2*nvParams.waitTime;
	/*any key is pressed?*/
	temp = checkKeyboardInput(nvParams.waitTime);
	if (temp != 0xff)
	{
		// check for a special string before going into blocked I/O
		// there might be a scanner or other device sending data on the serial port
		printf("Enter \"ipdialog\" in %d seconds to change these settings.\r\n", 2*nvParams.waitTime);
		printf("\r\n");
		for ( i = 0; i < 20; i++ ) buffer[i] = 0;
		i = 0;

		while ( wait > 0 )
		{
			tx_thread_sleep(100);
			wait--;
			temp = (char)getchar();
			while ((temp != 0xff) && (temp != EOF))
			{
				buffer[i] = temp;
				i++;
				if ( i > 20 )
				{
					wait = 0;
					break;
				}
				else if ( i >= 8 )
				{
					if ( 0 == strncmp(&(buffer[i-8]),"ipdialog",8) )
					{
						bDialogParamChange = TRUE;
						wait = -43;
						break;
					}
				}
				temp = (char)getchar();
			}
		}
	}
	if ( -43 != wait )
	{
#else if
	/*any key is pressed?*/
	temp = checkKeyboardInput(nvParams.waitTime);
	if ((temp != 0xff) && (APP_USE_NVRAM))
	{
		printf("Press A to Accept the settings, or M to Modify [M]?");
	}
	else
	{
#endif
#ifndef NETOS_GNU_TOOLS
		close(stdin->io_channel);
		stdin->io_channel = inch;
		stdout->io_channel = outch;
		stderr->io_channel = errch;
#else
		close(fd);
		RESTORE_IO
#endif
			return 0;				/*time out spared, no modification needed, continue...*/
	}

	/*let's close nonblock i/o, use block i/o*/
#ifndef NETOS_GNU_TOOLS
	close(stdin->io_channel);
#else
	close(fd);
	RESTORE_IO
#endif

		fd = netosConfigDialog(1);
	if (once == 1)
	{
		/*set baudrate*/
		rc= ioctl(fd, SIO_BAUD_SET, &nvParams.baudrate);

		if (rc < 0)
		{
			netosFatalError("error when change baud rate in Dialog.c", ERROR_CLASS_BSP, ERROR_INVALID_DIALOG_BAUDRATE);
		}

		once = 2;
	}

#ifndef FOXJET
	/*"A" or "a", "M" or "m"* is pressed?*/
	for(;;)
	{
		readLine(buffer);
		if (buffer[0] == 'a' || buffer[0] =='A')
		{
			printf("continue...\r\n");
#ifndef NETOS_GNU_TOOLS
			close(stdin->io_channel);

			stdin->io_channel = inch;
			stdout->io_channel = outch;
			stderr->io_channel = errch;
#else
			close (fd);
			RESTORE_IO
#endif
				return 0;			/*no modification needed, user needs to continue...*/
		}
		else if (buffer[0] == 'm' || buffer[0] =='M')
		{
			break;
		}
		else
		{
			printf("Press A to Accept the settings, or M to Modify [M]?");
		}
	}
#endif

	/*  "M" or "m" is pressed   */
	printf("\r\n");
	rc = NAgetSysAccess ("root", &password[0], NULL);
	if ((rc == NASYSACC_LEVEL_ROOT) && (notAuthenticated))
	{
		for(rc = 0; ; rc ++)
		{
			printf("Enter the root password: ");
			readPassword(buffer);
			if (strcmp(buffer, password) == 0)
			{
				/*  OK - this is an authentic administrator */
				notAuthenticated = 0;
				break;
			}
			else
			{
				if (rc < 3)
				{
					printf("FAILURE: root password incorrect\r\n");
				}
				else
				{
					/*  no modification allowed, continue...*/
					printf("FAILURE: root password incorrect\r\ncontinue...\r\n");
#ifndef NETOS_GNU_TOOLS
					close(stdin->io_channel);

					stdin->io_channel = inch;
					stdout->io_channel = outch;
					stderr->io_channel = errch;
#else
					close (fd);
					RESTORE_IO
#endif
						return 0;
				}
			}
		}
	}

	/*  Operator has been authenticated */
	printf("\r\n");
	printf("For each of the following questions, you can press <Return> to ");
	printf("select the value shown in braces, or you can enter a new value.\r\n");

	/*** burl ***/
	printf("\r\n");
	printf("Clear ROM?[N]"); 
	/*"Y" or "y", "N" or "n"* is pressed?*/
	for(;;)
	{
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			fj_writeROMMsg = TRUE;
			goto end_of_params;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			fj_writeROMMsg = FALSE;
			break;
		}
		else
		{
			printf("Clear ROM?[N]");
		}
	}
	/*** burl ***/

	printf("\r\n");
	printf("NETWORK INTERFACE PARAMETERS:\r\n");

	/*change IP address?*/
	printf("\r\n");
	printf("Should this target use DHCP to get its IP settings?[N]");
	/*"Y" or "y", "N" or "n"* is pressed?*/
	for(;;)
	{
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			nvParams.useDhcp = 1;
			break;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			nvParams.useDhcp = 0;
			break;
		}
		else
		{
			printf("Should this target use DHCP to get its IP settings?[N]");
		}
	}

	if (!nvParams.useDhcp)
	{
		for (;;)
		{
			printIpAddress("IP address[%d.%d.%d.%d]? ",nvParams.ipAddress);
			readLine(buffer);
			if (buffer[0] != '\0')
			{
				if(checkIpAddress(buffer))
				{
					ipaddress = NAInet_addr(buffer);
					nvParams.ipAddress = ipaddress;
					printIpAddress("  New IP address is %d.%d.%d.%d\r\n",nvParams.ipAddress);
					break;
				}
				else
				{
					printf("Invalid IP address\r\n");
				}
			}
			else
			{
				break;
			}
		}

		/*change subnet mask?*/
		for (;;)
		{
			printIpAddress("Subnet mask[%d.%d.%d.%d]? ",nvParams.subnetMask);
			readLine(buffer);
			if (buffer[0] != '\0')
			{
				if(checkIpAddress(buffer))
				{
					ipaddress = NAInet_addr(buffer);
					nvParams.subnetMask = ipaddress;
					printIpAddress("  New subnet is %d.%d.%d.%d\r\n",nvParams.subnetMask);
					break;
				}
				else
				{
					printf("Invalid IP address\r\n");
				}
			}
			else
			{
				break;
			}
		}

		/*change gateway?*/
		for (;;)
		{
			printIpAddress("Gateway address[%d.%d.%d.%d]? ",nvParams.gateway);
			readLine(buffer);
			if (buffer[0] != '\0')
			{
				if(checkIpAddress(buffer))
				{
					ipaddress = NAInet_addr(buffer);
					nvParams.gateway = ipaddress;
					printIpAddress("  New gateway address is %d.%d.%d.%d\r\n",nvParams.gateway);
					break;
				}
				else
				{
					printf("Invalid IP address\r\n");
				}
			}
			else
			{
				break;
			}
		}

		if (APP_ENABLE_PPP1)
		{
			/*ppp com1 address*/
			for (;;)
			{
				printIpAddress("COM1 PPP IP address[%d.%d.%d.%d]? ",nvParams.ipAddrPpp1);
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					if(checkIpAddress(buffer))
					{
						ipaddress = NAInet_addr(buffer);
						nvParams.ipAddrPpp1 = ipaddress;
						printIpAddress("  New IP address on COM1 is %d.%d.%d.%d\r\n",nvParams.ipAddrPpp1);
						break;
					}
					else
					{
						printf("Invalid IP address\r\n");
					}
				}
				else
				{
					break;
				}
			}

			/*ppp com1 peer address*/
			for (;;)
			{
				printIpAddress("COM1 PPP Peer IP address[%d.%d.%d.%d]? ",nvParams.ipAddrPpp1peer);
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					ipaddress = NAInet_addr(buffer);
					if(checkIpAddress(buffer) || ipaddress == 0)
					{
						nvParams.ipAddrPpp1peer = ipaddress;
						printIpAddress("  New peer IP address connected to COM1 is %d.%d.%d.%d\r\n",nvParams.ipAddrPpp1peer);
						break;
					}
					else
					{
						printf("Invalid IP address\r\n");
					}
				}
				else
				{
					break;
				}
			}
		}

		if (APP_ENABLE_PPP2)
		{
			/*ppp com2 address*/
			for (;;)
			{
				printIpAddress("COM2 PPP IP address[%d.%d.%d.%d]? ",nvParams.ipAddrPpp2);
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					if(checkIpAddress(buffer))
					{
						ipaddress = NAInet_addr(buffer);
						nvParams.ipAddrPpp2 = ipaddress;
						printIpAddress("  New IP address on COM2 is %d.%d.%d.%d\r\n",nvParams.ipAddrPpp2);
						break;
					}
					else
					{
						printf("Invalid IP address\r\n");
					}
				}
				else
				{
					break;
				}
			}

			/*ppp com2 peer address*/
			for (;;)
			{
				printIpAddress("COM2 PPP Peer IP address[%d.%d.%d.%d]? ",nvParams.ipAddrPpp2peer);
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					ipaddress = NAInet_addr(buffer);
					if(checkIpAddress(buffer) || ipaddress == 0)
					{
						nvParams.ipAddrPpp2peer = ipaddress;
						printIpAddress("  New peer IP address connected to COM2 is %d.%d.%d.%d\r\n",nvParams.ipAddrPpp2peer);
						break;
					}
					else
					{
						printf("Invalid IP address\r\n");
					}
				}
				else
				{
					break;
				}
			}
		}

#ifdef APP_USE_NETWORK_TIME_PROTOCOL
		/*  Primary SNTP Server */
		for (;;)
		{
			printIpAddress("Primary SNTP Server IP Address[%d.%d.%d.%d]? ", nvParams.ipAddrSntp1);
			readLine(buffer);
			if (buffer[0] != '\0')
			{
				if(checkIpAddress(buffer))
				{
					ipaddress = NAInet_addr(buffer);
					nvParams.ipAddrSntp1 = ipaddress;
					printIpAddress("  New Primary SNTP Server IP address is %d.%d.%d.%d\r\n", nvParams.ipAddrSntp1);
					break;
				}
				else
				{
					printf("Invalid IP address\r\n");
				}
			}
			else
			{
				break;
			}
		}

		/*  Secondary SNTP Server */
		if (nvParams.ipAddrSntp1 != 0)
		{
			for (;;)
			{
				printIpAddress("Secondary SNTP Server IP Address[%d.%d.%d.%d]? ", nvParams.ipAddrSntp2);
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					if(checkIpAddress(buffer))
					{
						ipaddress = NAInet_addr(buffer);
						nvParams.ipAddrSntp2 = ipaddress;
						printIpAddress("  New Secondary SNTP Server IP address is %d.%d.%d.%d\r\n", nvParams.ipAddrSntp2);
						break;
					}
					else
					{
						printf("Invalid IP address\r\n");
					}
				}
				else
				{
					break;
				}
			}
		}
#endif
	}								/*end if (! nvParams.useDhcp)*/

#if defined APP_ROOT_PASSWORD || defined APP_ADMINISTRATORS_ACCOUNT || defined APP_FIXED_GATEWAY_KEY || defined APP_USE_KERBEROS
	printf("\r\nSECURITY PARAMETERS:\r\n\r\n");
#endif

#ifdef APP_ROOT_PASSWORD
	/*  root password   */
	for(;;)
	{
		printf("Would you like to update the root password?[N]");
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			for (;;)
			{
				printf("Enter the new root password:    ");
				readPassword(buffer);
				if (buffer[0] != '\0')
				{
					if (strlen(buffer) < NASYSACC_STRLEN_PASSWORD)
					{
						strcpy (password, buffer);
						printf("Re-enter the new root password: ");
						readPassword(buffer);
						if (strcmp (password, buffer) == 0)
						{
							/*  Update the root account */
							if (nvParams.rootPassword[0] == 0)
							{
								NAsetSysAccess (NASYSACC_DEL, "root", APP_ROOT_PASSWORD, NASYSACC_LEVEL_ROOT, 0);
							}
							else
							{
								NAsetSysAccess (NASYSACC_DEL, "root", nvParams.rootPassword, NASYSACC_LEVEL_ROOT, 0);
							}
							NAsetSysAccess (NASYSACC_ADD, "root", password, NASYSACC_LEVEL_ROOT, 0);

							/*  update the nvParams record  */
							memset (nvParams.rootPassword, 0, sizeof(nvParams.rootPassword));
							strcpy (nvParams.rootPassword, password);
							break;
						}
						else
						{
							printf("ERROR: Password entries mismatched.\r\n");
						}
					}
					else
					{
						printf("ERROR: Invalid password.  Length must be less than %d characters.\r\n", NASYSACC_STRLEN_PASSWORD);
					}
				}
				else
				{
					break;
				}
			}
			break;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			break;
		}
	}
#endif

#ifdef APP_ADMINISTRATORS_ACCOUNT
	/*  administrator account password  */
	for(;;)
	{
		printf("Would you like to update the administrator account '%s' password?[N]", APP_ADMINISTRATORS_ACCOUNT);
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			for (;;)
			{
				printf("Enter the new administrator password:    ");
				readPassword(buffer);
				if (buffer[0] != '\0')
				{
					if (strlen(buffer) < NASYSACC_STRLEN_PASSWORD)
					{
						strcpy (password, buffer);
						printf("Re-enter the new administrator password: ");
						readPassword(buffer);
						if (strcmp (password, buffer) == 0)
						{
							/*  Update the nvParams record  */
							memset (nvParams.adminPassword, 0, sizeof(nvParams.adminPassword));
							strcpy (nvParams.adminPassword, password);
							break;
						}
						else
						{
							printf("ERROR: Password entries mismatched.\r\n");
						}
					}
					else
					{
						printf("ERROR: Invalid password.  Length must be less than %d characters.\r\n", NASYSACC_STRLEN_PASSWORD);
					}
				}
				else
				{
					break;
				}
			}
			break;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			break;
		}
	}
#endif

#ifdef APP_FIXED_GATEWAY_KEY
	/*  fixed Key */
	for(;;)
	{
		printf("Would you like to update the fixed key?[N]");
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			for (;;)
			{
				printf("Enter the new key:    ");
				readPassword(buffer);
				if (buffer[0] != '\0')
				{
					if (strlen(buffer) < NASYSACC_STRLEN_PASSWORD)
					{
						strcpy (password, buffer);
						printf("Re-enter the new key: ");
						readPassword(buffer);
						if (strcmp (password, buffer) == 0)
						{
							/*  update the nvParams record  */
							memset (nvParams.FixedKey, 0, sizeof(nvParams.FixedKey));
							strcpy (nvParams.FixedKey, password);
							break;
						}
						else
						{
							printf("ERROR: Key entries mismatched.\r\n");
						}
					}
					else
					{
						printf("ERROR: Invalid key.  Length must be less than %d characters.\r\n", NASYSACC_STRLEN_PASSWORD);
					}
				}
				else
				{
					break;
				}
			}
			break;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			break;
		}
	}
#endif

#ifdef APP_USE_KERBEROS
	for (;;)
	{
		printIpAddress("Kerberos KDC IP Address[%d.%d.%d.%d]? ", nvParams.krbIpAddr);
		readLine(buffer);
		if (buffer[0] != '\0')
		{
			if(checkIpAddress(buffer))
			{
				ipaddress = NAInet_addr(buffer);
				nvParams.krbIpAddr = ipaddress;
				printIpAddress("  New Kerberos KDC IP address is %d.%d.%d.%d\r\n", nvParams.krbIpAddr);
				break;
			}
			else
			{
				printf("ERROR: Invalid IP Address\r\n");
			}
		}
		else
		{
			break;
		}
	}

	for (;;)
	{
		printf("Kerberos realm name[%s]: ", nvParams.krbRealmname);
		readLine(buffer);
		if (buffer[0] != '\0')
		{
			if (strlen(buffer) < NASEC_MAX_KRB_REALM_NAME_LENGTH)
			{
				memset (nvParams.krbRealmname, 0, sizeof(nvParams.krbRealmname));
				strcpy (nvParams.krbRealmname, buffer);
				printf("  New Kerberos Realm name is %s.\r\n", nvParams.krbRealmname);
				break;
			}
			else
			{
				printf("ERROR: Realm name limited to %d characters\r\n", NASEC_MAX_KRB_REALM_NAME_LENGTH);
			}
		}
		else
		{
			break;
		}
	}

	for(;;)
	{
		printf("Would you like to update the Kerberos username '%s'?[N]", nvParams.krbUsername);
		readLine(buffer);
		if (buffer[0] == 'y' || buffer[0] =='Y')
		{
			for (;;)
			{
				printf("Enter the new Kerberos username: ");
				readLine(buffer);
				if (buffer[0] != '\0')
				{
					if (strlen(buffer) < NASEC_MAX_KRB_USERNAME_LENGTH)
					{
						strcpy (nvParams.krbUsername, buffer);
						for (;;)
						{
							printf("Enter the Kerberos password:    ");
							readPassword(buffer);
							if (buffer[0] != '\0')
							{
								if (strlen(buffer) < NASEC_MAX_KRB_PASSWORD_LENGTH)
								{
									strcpy (password, buffer);
									printf("Re-enter the Kerberos password: ");
									readPassword(buffer);
									if (strcmp (password, buffer) == 0)
									{
										/*  update the nvParams record  */
										strcpy (nvParams.krbPassword, password);
										break;
									}
									else
									{
										printf("ERROR: Password entries mismatched.\r\n");
									}
								}
								else
								{
									printf("ERROR: Invalid password.  Length must be less than %d characters.\r\n", NASYSACC_STRLEN_PASSWORD);
								}
							}
							else
							{
								break;
							}
						}
						break;
					}
					else
					{
						printf("ERROR: Invalid username.  Length must be less than %d characters.\r\n", NASYSACC_STRLEN_PASSWORD);
					}
				}
				else
				{
					break;
				}
			}
			break;
		}
		else if (buffer[0] == 'n' || buffer[0] =='N' || buffer[0] == '\0')
		{
			break;
		}
	}
#endif

	printf("\r\n");
	printf("HARDWARE PARAMETERS:\r\n");

	/*set the baudrate*/
	printf("\r\n");

	for (;;)
	{
		printf("Set the baud rate of Serial channels[%d]?",nvParams.baudrate);
		readLine(buffer);
		if (buffer[0] != '\0')
		{
			baudrate = atol(buffer);
			if ((baudrate == 1200)||(baudrate == 2400)||(baudrate == 4800) ||(baudrate == 9600)||
				(baudrate == 19200)||(baudrate == 38400)||(baudrate == 57600)||(baudrate == 115200))
			{
				nvParams.baudrate = baudrate;
				printf("  The new baud rate is %d\r\n",nvParams.baudrate);
				printf("  The baud rate will be changed on next power up\r\n");
				printf("  Please set the baud rate for your terminal accordingly\r\n");
				break;
			}
			else
				printf("The baud rate has to be 1200, 2400, 4800, 9600, 19200, 38400, 57600, or 115200\r\n");
		}
		else
			break;

	}

	printf("\r\n");
	if ( 0 == strncmp(&nvParams.serialNumber[0],APP_BSP_SERIAL_NUM,sizeof(nvParams.serialNumber)) )
	{
		/*set the board's serial number?*/
		printf("The target serial number is used to generate its Ethernet MAC address\r\n");
		printf("Each development board must have a unique serial number\r\n");
		memcpy(s,  &nvParams.serialNumber[0],sizeof(nvParams.serialNumber));
		s[8]=0;						/*for correct printf %s format*/
		for(;;)
		{
			printf("Set the board's serial number[%s]? ", s);
			readLine(buffer);
			if (buffer[0] != '\0')
			{
				if ( checkSerialNumber(buffer))
				{
					memcpy(&nvParams.serialNumber[0], buffer ,sizeof(nvParams.serialNumber));
					memcpy(s,  &nvParams.serialNumber[0],sizeof(nvParams.serialNumber));
					s[8]=0;			/*for correct printf %s format*/
					printf("  The board's new serial number is %s\r\n", s);
					break;
				}
				else
					printf("The board's serial number has to be 00000000-99999999 and 8 digits\r\n");
			}
			else
				break;
		}
	}

#ifdef FOXJET
	NASerialnum_to_mac ((char *)&s[0], mac_addrp);
#else if
	NASerialnum_to_mac ((char *)&s[2], mac_addrp);
#endif
	printMacAddress("  This board's MAC Address is %x:%x:%x:%x:%x:%x\r\n",mac_addrp);

	/*set wait time?*/
	printf("\r\n");
	printf("How long (in seconds) should CPU delay before starting up[%d]? ", nvParams.waitTime);
	readLine(buffer);
	if (buffer[0] != '\0')
	{
		if(buffer[1] == '\0')
		{
			if (buffer[0] < 0x35)
			{
				nvParams.waitTime = 5;
				printf("The smallest wait time is 5 seconds.\r\n");
			}
			else
				nvParams.waitTime = buffer[0]-0x30;
		}
		else
			nvParams.waitTime = (unsigned long)((buffer[0]-0x30)*10) + (unsigned long)(buffer[1]-0x30);

		printf("  After board is reset, start-up code will wait %d seconds.\r\n", nvParams.waitTime);
	}

    end_of_params: // burl
	/* 
	* save all the changes in NVMEM
	* if the APP_USE_NVRAM flag is set to 0, just update default parameters.
	*
	*/
	printf("\r\n");

	if (APP_USE_NVRAM)
		printf("Saving the changes in NV memory...");

	else
		printf("Updating the changes in defaultParams...");

	/* 
	*  NAWriteDevBoardParams() writes development board parameters to NVRAM if APP_USE_NVRAM
	*  flag is set to a nonzero value. If APP_USE_NVRAM is 0, NAReadDevBoardParams() updates
	*  the default parameters as defined in appconfig.h. NAWriteDevBoardParams() and
	*   default data struct are in src\narmcore\narmsrln.c.
	*/
	NAWriteDevBoardParams (&nvParams);

	/*
	* The board's MAC address is dependent upon it's serial number.  Reload the MAC
	* address here in case the user changed our serial number.
	*/
	NASerialnum_to_mac (&nvParams.serialNumber[2], mac_addrp);
	NAInitEthAddress (mac_addrp);	/* Set MAC address in ethernet driver*/

	printf("Done.");
#ifndef NETOS_GNU_TOOLS
	close(stdin->io_channel);

	stdin->io_channel = inch;
	stdout->io_channel = outch;
	stderr->io_channel = errch;
#else
	close (fd);
	RESTORE_IO
#endif
		tx_thread_sleep(25);		/*to see better*/
	return 1;
}
#endif

/*
*
*  Function: void NADialog (void)
*
*  Description:
*
*      This routine sets up configuration dialog if APP_DIALOG_PORT is defined.
*
*  Parameters:
*
*      none
*
*  Return Values:
*
*      none
*
*/

void NADialog (void)
{
#if defined(APP_DIALOG_PORT)

	int rc=0;
	do
	{
		rc= dialogLocal();
	} while (rc != 0);
	return ;

#else
	return ;						/*no APP_DIALOG_PORT defined*/
#endif
}
