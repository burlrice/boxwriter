#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <time.h>
#endif

#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_datamatrix.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_system.h"
#include "iec16022-0.2.1/iec16022ecc200.h"

static char sz [128];

#ifdef _WINDOWS
void Trace (LPCTSTR lpszFile, ULONG lLine, LPCTSTR lpsz)
{
	char sz [512];

	sprintf (sz, "%s(%d): %s\n", lpszFile, lLine, lpsz);
	OutputDebugString (sz);
}

#define TRACEF(sz) Trace ((__FILE__), (__LINE__), (sz));
#endif

// used for MemStor identification
const  CHAR fj_DataMatrixID[]   = "DataMatrix";

extern char lpszWinDynData [FJMESSAGE_DYNAMIC_SEGMENTS][FJTEXT_SIZE + 1];

//
//  FUNCTION: fj_DataMatrixNew()
//
//  PURPOSE:  Create a new FJDATAMATRIX structure
//
FJDLLExport LPFJDATAMATRIX fj_DataMatrixNew( VOID )
{
	LPFJDATAMATRIX pfdb;

	pfdb = (LPFJDATAMATRIX)fj_calloc( 1, sizeof (FJDATAMATRIX) );

	pfdb->lSysBCIndex	= 0;
	pfdb->strText [0]	= 0;
	pfdb->lDynFirstChar	= 0;
	pfdb->lDynNumChar	= 1;
	pfdb->lDynIndex		= 0;
	pfdb->cx		= 12;
	pfdb->cy		= 12;

	#ifdef _DEBUG
	strcpy (pfdb->strText, "TEST");
	#endif

	return( pfdb );
}
//
//  FUNCTION: fj_DataMatrixDestroy()
//
//  PURPOSE:  Destroy a FJDATAMATRIX structure
//
FJDLLExport VOID fj_DataMatrixDestroy( LPFJELEMENT pfe )
{
	LPFJDATAMATRIX pfdb;

	pfdb = (LPFJDATAMATRIX)pfe->pDesc;
	if ( NULL != pfdb)
	{
		fj_free( pfdb );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_DataMatrixDup()
//
//  PURPOSE:  Duplicate a FJDATAMATRIX structure
//
FJDLLExport LPFJDATAMATRIX fj_DataMatrixDup( CLPCFJDATAMATRIX pfdbIn )
{
	LPFJDATAMATRIX pfdbDup;

	pfdbDup = (LPFJDATAMATRIX)fj_malloc( sizeof (FJDATAMATRIX) );
	memcpy( pfdbDup, pfdbIn, sizeof(FJDATAMATRIX) );

	return( pfdbDup );
}

FJDLLExport VOID fj_DataMatrixCreateImage( CLPFJELEMENT pfe )
{
#ifndef _WINDOWS
	LPFJ_BRAM pBRAM			= fj_CVT.pBRAM;
#endif
	LPFJSYSDATAMATRIX pfsysBC;
	LPFJDATAMATRIX	pfdb	= (LPFJDATAMATRIX)pfe->pDesc;
	LPFJIMAGE pfi			= NULL;
	LPFJIMAGE pfiOld		= pfe->pfi;
	LONG lHeightBytes		= (pfdb->cy + 7) / 8;
	LONG lBufferLen			= pfdb->cx * lHeightBytes;
	LPBYTE pBuffer;
	CHAR strText [FJDATAMATRIX_SIZE + 1];
	int i;
	UCHAR c;
	UCHAR * pGrid = NULL;
	char * pEncoding = NULL;
	int nLen = 0, nLenP = 0, nMaxLen = 0, nEccLen = 0;

	pfi = fj_ImageNew (lBufferLen);
	pfsysBC = &(pfe->pfm->pfph->pfsys->bcDataMatrixArray[pfdb->lSysBCIndex]);

	if (pfi) {
		pfe->pActions->fj_DescGetTextString (pfe, strText, FJDATAMATRIX_SIZE);
		nLen = strlen (strText);
		//{ sprintf (sz, "%s(%d): \"%s\" [%d]", __FILE__, __LINE__, strText, nLen); fj_socketSendAll(IPC_STATUS, (LPBYTE)sz, strlen (sz)); }
		pGrid = iec16022ecc200 (&pfdb->cx, &pfdb->cy, &pEncoding, nLen, strText, &nLenP, &nMaxLen, &nEccLen);

		pfi->lHeight	= pfdb->cy;
		pfi->lLength	= lHeightBytes * pfdb->cx;	
		pBuffer		= (LPBYTE)pfi + sizeof(FJIMAGE);

		if (pGrid) {
			int x, y, i, j;
			int nOffset = (((pfdb->cx + 7) / 8) * 8) - pfdb->cx;

			/*
			for (y = 0, j = pfdb->cy - 1; j >= 0 ; j--, y++) {
				for (x = 0, i = 0; i < pfdb->cx; i++, x++) {
					BOOL bBit = pGrid [pfdb->cx * j + i];
					char sz [256];

					//sprintf (sz, "[%d, %d] ", i, j); OutputDebugString (sz);
					OutputDebugString (bBit ? "X" : " ");
				}

				OutputDebugString ("\n");
			}

			OutputDebugString ("\n\n\n");
			*/

			for (y = 0, i = 0; i < pfdb->cx; i++, y++) {
				LONG lBitCount = lHeightBytes * y * 8;

				for (x = 0, j = 0; j < pfdb->cy; j++, x++) {
					BOOL bBit = pGrid [(pfdb->cx * j) + i];
					int nByte, nPixel, nShift;

					nByte	= lBitCount / 8; 
					nPixel	= (pfdb->cx * y) + x;
					nShift	= 7 - (x % 8);

					if (bBit) 
						pBuffer [nByte] |= (1 << nShift); 

					lBitCount++;
				}
			}
		}

		for (i = 0; i < pfi->lLength; i++) {
			if (!pGrid)
				pBuffer [i] = 0xFF;

			c = pBuffer [i];

			pfi->lBits += fj_ImageCountBits [c];
		}

		if (pfsysBC->lWidthBold) pfi = fj_ImageBold (pfi, pfsysBC->lWidthBold);
		if (pfsysBC->lWidth > 1) pfi = fj_ImageWidth (pfi, pfsysBC->lWidth);

		if (pfsysBC->lHeightBold) pfi = fj_ImageHeightBold (pfi, pfsysBC->lHeightBold);
		if (pfsysBC->lHeight > 1) pfi = fj_ImageHeight (pfi, pfsysBC->lHeight);

		free (pGrid);
	}

	pfe->pfi = pfi;

	if (pfiOld != NULL)
		fj_ImageRelease (pfiOld);

	//{ sprintf (sz, "%s(%d): return", __FILE__, __LINE__); fj_socketSendAll(IPC_STATUS, (LPBYTE)sz, strlen (sz)); }
}

//
//   DataMatrix does change
//
FJDLLExport VOID fj_DataMatrixPhotocellBuild( CLPFJELEMENT pfe )
{
	fj_DataMatrixCreateImage( pfe );
	return;
}
//
//  FUNCTION: fj_DataMatrixToStr()
//
//  PURPOSE:  Convert a FJDATAMATRIX structure into a descriptive string
//
FJDLLExport VOID fj_DataMatrixToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPFJDATAMATRIX pfdb = (CLPFJDATAMATRIX)pfe->pDesc;
	STR sName[FJDATAMATRIX_NAME_SIZE*2];

	fj_processParameterStringOutput (sName, pfe->strName);
	sprintf (pStr, "{%s,%s,%f,%d,%c,%c,%c,%c,%d,%d,%d,%s,%d,%d,%d}",
		fj_DataMatrixID,
		sName,
		pfe->fLeft,						
		pfe->lRow,						
		(CHAR)pfe->lPosition,			
		(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F'),
		pfdb->lSysBCIndex,
		pfdb->cx,
		pfdb->cy,
		pfdb->strText,
		pfdb->lDynFirstChar,
		pfdb->lDynNumChar,
		pfdb->lDynIndex);
}
//
//  FUNCTION: fj_DataMatrixFromStr
//
//  PURPOSE:  Convert a descriptive string into a FJDATAMATRIX structure
//
FJDLLExport VOID fj_DataMatrixFromStr( LPFJELEMENT pfe, LPSTR pStr )
{
#define DATAMATRIX_PARAMETERS 15
	LPFJDATAMATRIX pfdb;
	LPSTR  pParams[DATAMATRIX_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfdb = (LPFJDATAMATRIX)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJDATAMATRIX_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJDATAMATRIX_MEMSTOR_SIZE );
	*(pStrM+FJDATAMATRIX_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, DATAMATRIX_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_DataMatrixID ) ) res = 0;

	if ( res == DATAMATRIX_PARAMETERS )
	{
		pfe->lTransform  =  0;
		fj_processParameterStringInput (pParams [1]);
		strncpy( pfe->strName,          pParams [1], FJELEMENT_NAME_SIZE); pfe->strName [FJELEMENT_NAME_SIZE] = 0;
		pfe->fLeft			= (FLOAT)atof (pParams [2]);
		pfe->lRow			= atol (pParams [3]);
		pfe->lPosition		=    * (pParams [4]);
		pfe->lTransform		|=   (*(pParams [5]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		pfe->lTransform		|=   (*(pParams [6]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		pfe->lTransform		|=   (*(pParams [7]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
		pfdb->lSysBCIndex	= atol (pParams [8]);
		pfdb->cx			= atol (pParams [9]);
		pfdb->cy			= atol (pParams [10]);
		strncpy (pfdb->strText, pParams [11], FJDATAMATRIX_SIZE); pfdb->strText[FJDATAMATRIX_SIZE] = 0;
		pfdb->lDynFirstChar = atol (pParams [12]);
		pfdb->lDynNumChar	= atol (pParams [13]);
		pfdb->lDynIndex		= atol (pParams [14]);
	}

	fj_free (pStrM);
}

//
//	FUNCTION: fj_DataMatrixBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJDATAMATRIX from FJMEMSTOR
//
FJDLLExport VOID fj_DataMatrixBuildFromName( CLPCFJMEMSTOR pfms, LPFJELEMENT pfe, LPCSTR pName )
{
	LPSTR       pDataMatrixStr;

	pDataMatrixStr = (LPSTR)fj_MemStorFindElement( pfms, fj_DataMatrixID, pName );
	if ( NULL != pDataMatrixStr )
	{
		fj_DataMatrixFromStr( pfe, pDataMatrixStr );
	}

	return;
}
//
//	FUNCTION: fj_DataMatrixAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJDATAMATRIX element to FJMEMSTOR
//
FJDLLExport BOOL fj_DataMatrixAddToMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	CHAR buff[FJDATAMATRIX_MEMSTOR_SIZE];
	fj_DataMatrixToStr( pfe, buff );

	return( fj_MemStorAddElementString( pfms, buff ) );
}
//
//	FUNCTION: fj_DataMatrixDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Delete FJDATAMATRIX element from FJMEMSTOR
//
FJDLLExport BOOL fj_DataMatrixDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	STR sName[FJDATAMATRIX_NAME_SIZE*2];

	fj_processParameterStringOutput( sName, pfe->strName );
	return( fj_MemStorDeleteElement( pfms, fj_DataMatrixID, sName ) );
}
//
//	FUNCTION: fj_DataMatrixGetType
//
//	Return: BarCode type
//
//
FJDLLExport enum FJELETYPE fj_DataMatrixGetType( VOID )
{

	return( FJ_TYPE_DATAMATRIX );
}
//
//	FUNCTION: fj_DataMatrixGetTypeString
//
//	Return: Pointer to string for BarCode type
//
//
FJDLLExport LPCSTR fj_DataMatrixGetTypeString( VOID )
{

	return( fj_DataMatrixID );
}
//
//	FUNCTION: fj_DataMatrixGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_DataMatrixGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJDATAMATRIX pfdb;
	int i;
	char * pSrc;

	pfdb = (LPFJDATAMATRIX)pfe->pDesc;

	if (pfdb->lDynIndex > 0 && pfdb->lDynIndex <= FJMESSAGE_DYNAMIC_SEGMENTS) {
		memset (pStr, 0, lMaxCount);
		pStr [0] = '0';

		#ifndef _WINDOWS
			pSrc = &fj_CVT.pBRAM->DynamicSegments [pfdb->lDynIndex - 1].strDynData [pfdb->lDynFirstChar];
		#else
			pSrc = &pfdb->strText [pfdb->lDynFirstChar];
		#endif

		for (i = 0; i < max (1, pfdb->lDynNumChar) && pSrc [i]; i++)
			pStr [i] = pSrc [i];
	}
	else
		strncpy (pStr, pfdb->strText, lMaxCount);

	* (pStr + lMaxCount - 1) = 0;
}

FJCELEMACTION fj_DataMatrixActions =
{
	(LPVOID  (*)( VOID ))fj_DataMatrixNew,
	(LPVOID  (*)( LPVOID ))fj_DataMatrixDup,
	fj_DataMatrixDestroy,
	fj_DataMatrixCreateImage,
	fj_DataMatrixPhotocellBuild,
	fj_DataMatrixBuildFromName,
	fj_DataMatrixAddToMemstor,
	fj_DataMatrixDeleteFromMemstor,
	fj_DataMatrixGetTextString,
	fj_DataMatrixGetType,
	fj_DataMatrixGetTypeString
};
