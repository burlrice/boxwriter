/* C file: c:\MyPrograms\FoxJet1\_distributor.c created
 by NET+Works HTML to C Utility on Monday, June 05, 2000 13:03:46 */
#include "fj_defines.h"
#include "fj.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_SNam */
void na_SNam (unsigned long handle);
/*  @! File: na_LNam */
void na_LNam (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_ID */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_TITLE_DIST */
static void na_TITLE_DIST (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_SNamLen */
static void na_SNamLen (unsigned long handle);
/*  @! File: na_LNamLen */
static void na_LNamLen (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\distributor.htm {{ */
void Send_function_12(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_DIST (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Distributor\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<form action=dist_1 method=POST>\n");
	HSSend (handle, "  <center>\n");
	HSSend (handle, "    <table border=0><col align=right><col align=left>\n");
	HSSend (handle, "     <tr><td><input type=text name=short_name size=20 maxlength=");
	na_SNamLen (handle);
	HSSend (handle, " value=\"");
	na_SNam (handle);
	HSSend (handle, "\"></td><td>Short Name *</td></tr>\n");
	HSSend (handle, "     <tr><td><input type=text name=long_name size=20 maxlength=");
	na_LNamLen (handle);
	HSSend (handle, " value=\"");
	na_LNam (handle);
	HSSend (handle, "\"></td><td>Long Name</td></tr>\n");
	HSSend (handle, "     <tr><td colspan=2><center><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, "><input type=submit value=Submit><input type=reset value=Reset></center></td></tr>\n");
	HSSend (handle, "    </table>\n");
	HSSend (handle, "  </center>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "<p>* NOTE: The Short Name is part of the Printer's Internet ID. If the Short Name is changed, \n");
	HSSend (handle, "the Printer will not respond to either its old ID or new ID until it is reset or powered off.</p>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\distributor.htm }} */
/* @! End HTML page   */

void reload_dist(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=distributor.htm");
	}
	else
	{
		fj_reload_page(handle, "distributor.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_dist_1  {{ */
void Post_dist_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL     pGlobal     = pCVT->pFJglobal;
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfphP       = &(pParam->FJPh);
	LPFJPRINTHEAD   pfph        = pCVT->pfph;
	char shortField[FJPH_NAME_SIZE_SHORT+1];
	char longField[FJPH_NAME_SIZE_LONG+1];
	BOOL bROMWait;
	BOOL bChanged;
	int  retrom;
	int  retShort;
	int  retLong;

	bROMWait = FALSE;
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam  );

	bChanged = FALSE;
	if ( 0 == retrom )
	{
		retShort = fj_HSGetValue( handle, "short_name", shortField, FJPH_NAME_SIZE_SHORT+1 );
		if ( 0 < retShort )
		{
			pCVT->fj_PrintHeadSetNameShort( pfphP, shortField );
			bChanged = TRUE;
		}
		retLong = fj_HSGetValue( handle, "long_name", longField, FJPH_NAME_SIZE_LONG+1 );
		if ( 0 < retLong )
		{
			pCVT->fj_PrintHeadSetNameLong( pfphP, longField );
			bChanged = TRUE;
		}
	}
	if ( TRUE == bChanged )
	{
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam , TRUE, NULL, 0 );
		if ( 0 < retShort )
		{
			pCVT->fj_PrintHeadSetNameShort( pfph, shortField );
			pCVT->fj_setID();
		}
		if ( 0 < retLong )
		{
			pCVT->fj_PrintHeadSetNameLong( pfph, longField );
		}
		bROMWait = TRUE;
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	reload_dist(handle,bROMWait);
}										/* @! Function: Post_dist_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_ID  {{ */
/* @! Function: na_SNam  {{ */
/* @! Function: na_LNam  {{ */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_TITLE_DIST  {{ */
void na_TITLE_DIST(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Distributor" );
}										/* @! Function: na_TITLE_DIST  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_SNamLen  {{ */
void na_SNamLen(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(FJPH_NAME_SIZE_SHORT));
}										/* @! Function: na_SNamLen  }} */
/* @! Function: na_LNamLen  {{ */
void na_LNamLen(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(FJPH_NAME_SIZE_LONG));
}										/* @! Function: na_LNamLen  }} */
/* @! End of dynamic functions   */
