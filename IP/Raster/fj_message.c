#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <math.h>
#endif
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_barcode.h"
#include "fj_bitmap.h"
#include "fj_counter.h"
#include "fj_datetime.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

// used for MemStor identification
const  CHAR fj_MessageID[]   = "Message";

//
//  FUNCTION: fj_MessageResetInternal
//
//  PURPOSE:  reset the message info after a change
//
VOID fj_MessageResetInternal( LPFJMESSAGE pfm )
{
	CLPCFJPRINTHEAD pfph = pfm->pfph;
	LPCFJELEMENT    pfe;
	LONG    iSeg;

	pfm->fLeft   = 0.0;
	pfm->fRight  = 0.0;
	pfm->lRow    = 0;
	pfm->lHeight = 0;
	pfm->lBits   = 0;

	for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
	{
		pfe = pfm->papfe[iSeg];
		if ( NULL != pfe )
		{
			if ( (NULL != pfe->pDesc) && (NULL != pfe->pfi) )
			{
				if ( 0 == pfm->lHeight )
				{
					pfm->fLeft   = pfe->fLeft;
					pfm->fRight  = pfe->fLeft + fj_PrintHeadGetElementWidth( pfph, pfe );
					pfm->lRow    = pfe->lRow;
					pfm->lHeight = pfe->lRow + pfe->pfi->lHeight - 1;
					pfm->lBits   = pfe->pfi->lBits;
				}
				else
				{
					pfm->fLeft   = min(pfm->fLeft,pfe->fLeft);
					pfm->fRight  = max(pfm->fRight,(pfe->fLeft + fj_PrintHeadGetElementWidth( pfph, pfe )));
					pfm->lRow    = min(pfm->lRow,pfe->lRow);
					pfm->lHeight = max(pfm->lHeight,(pfe->lRow + pfe->pfi->lHeight - 1));
					pfm->lBits  += pfe->pfi->lBits;
				}
			}
		}
	}
	pfm->lHeight = pfm->lHeight - pfm->lRow + 1;
	return;
}
//
//  FUNCTION: fj_MessageReset()
//
//  PURPOSE:  reset the elements and the message info after a change
//
FJDLLExport VOID fj_MessageReset( LPFJMESSAGE pfm )
{
	LPFJELEMENT    pfe;
	LONG    iSeg;

	for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
	{
		pfe = pfm->papfe[iSeg];
		if ( NULL != pfe )
		{
			fj_ElementReset( pfe );
		}
	}
	fj_MessageResetInternal( pfm );
	return;
}
//
//  FUNCTION: fj_MessagePhotocellBuild()
//
//  PURPOSE:  reset the elements and the message info after a photocell
//
FJDLLExport VOID fj_MessagePhotocellBuild( LPFJMESSAGE pfm )
{
	LPFJELEMENT     pfe;
	LONG    iSeg;

	for ( iSeg = 0; iSeg < pfm->lCount; iSeg++ )
	{
		pfe = pfm->papfe[iSeg];
		if ( NULL != pfe )
		{
			fj_ElementPhotocellBuild( pfe );
		}
	}
	fj_MessageResetInternal( pfm );

	#ifndef _WINDOWS
	fj_onSOP (pfm->pfph);
	#endif
	
	return;
}
//
//  FUNCTION: fj_MessageNew()
//
//  PURPOSE:  Create a new FJMESSAGE structure
//
FJDLLExport LPFJMESSAGE fj_MessageNew( VOID )
{
	LPFJMESSAGE pfm;

	pfm = (LPFJMESSAGE)fj_calloc( 1, sizeof(FJMESSAGE) );

	pfm->fLeft      = 0.0;				// summary info for all segments
	pfm->fRight     = 0.0;				// summary info for all segments
	pfm->lRow       = 0;				// summary info for all segments
	pfm->lHeight    = 0;				// summary info for all segments
	pfm->lBits      = 0;				// summary info for all segments
	pfm->lCount     = 0;				// count of segments
	pfm->strName[0] = 0;
	pfm->papfe      = NULL;
	pfm->pfph       = NULL;
	pfm->pfl        = NULL;
	return( pfm );
}
//
//  FUNCTION: fj_MessageDup()
//
//  PURPOSE:  Duplicate a FJMESSAGE structure
//
FJDLLExport LPFJMESSAGE fj_MessageDup( LPCFJMESSAGE pfmIn )
{
	LPFJMESSAGE pfmDup;
	LPFJELEMENT pfeIn;
	LPFJELEMENT pfeDup;
	int   i;

	pfmDup = NULL;
	if ( NULL != pfmIn )
	{
		pfmDup = (LPFJMESSAGE)fj_calloc( 1, sizeof (FJMESSAGE) );
		memcpy( pfmDup, pfmIn, sizeof(FJMESSAGE) );

		pfmDup->papfe  = NULL;
		pfmDup->lCount = 0;
		for ( i = 0; i < pfmIn->lCount; i++ )
		{
			pfeIn = pfmIn->papfe[i];
			if ( NULL != pfeIn )
			{
				pfeDup = fj_ElementDup( pfeIn );
				if( NULL != pfeDup ) pfeDup->pfm = pfmDup;
			}
			else
			{
				pfeDup = NULL;
			}
			pfmDup->lCount = fj_ElementListAdd( &(pfmDup->papfe), pfmDup->lCount, pfeDup );
		}
	}
	return( pfmDup );
}
//
//  FUNCTION: fj_MessageDestroy()
//
//  PURPOSE:  Destroy a FJMESSAGE structure
//
FJDLLExport VOID fj_MessageDestroy( LPFJMESSAGE pfm )
{
	LPFJELEMENT *papfe;

	if ( NULL != pfm)
	{
		papfe = pfm->papfe;
		if ( NULL != papfe ) fj_ElementListDestroyAll( &(pfm->papfe), pfm->lCount );
		fj_free( pfm );
	}
	return;
}
//
//  FUNCTION: fj_MessageGetText()
//
//  PURPOSE:  Concatenate Message text from the segments
//
FJDLLExport VOID fj_MessageGetText( CLPCFJMESSAGE pfm, LPSTR pString, LONG lStringLength )
{
	LPFJELEMENT pfe;
	LONG  lDone;
	int   iSeg;

	if ( NULL != pfm)
	{
		*pString = 0;
		iSeg = 0;
		while ( (iSeg < pfm->lCount) && (lStringLength > 0) )
		{
			pfe = pfm->papfe[iSeg];

			pfe->pActions->fj_DescGetTextString( pfe, pString, lStringLength );
			lDone = strlen( pString );
			pString += lDone;
			lStringLength -= lDone;
			iSeg++;
		}
	}
	return;
}
//
//	FUNCTION: fj_MessageBuildFromString
//
//	Return: TRUE if success.
//
//  Extract and build a FJMESSAGE from FJMEMSTOR, with its FJSEGMENTs
//
FJDLLExport LPFJMESSAGE fj_MessageBuildFromString( CLPCFJMEMSTOR pfms, LPCSTR pMessage )
{
#define MESSAGE_PARAMETERS 3
	LPFJMESSAGE pfm;
	LPFJELEMENT pfe;
	LPSTR  pParams[MESSAGE_PARAMETERS+FJMESSAGE_NUMBER_SEGMENTS];
	LPSTR  pParamsName[3];
	LPSTR  pStrM;
	STR    strName[FJELEMENT_NAME_SIZE+1];
	int    res;
	int    i;

	pfm = NULL;

	if ( NULL != pMessage )
	{
		res = 0;
		pStrM = (LPSTR)fj_malloc( FJMESSAGE_MEMSTOR_SIZE+1 );
		strncpy( pStrM, pMessage, FJMESSAGE_MEMSTOR_SIZE );
		*(pStrM+FJMESSAGE_MEMSTOR_SIZE ) = 0;

		res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, MESSAGE_PARAMETERS+FJMESSAGE_NUMBER_SEGMENTS );
		if ( 0 != strcmp( pParams[0], fj_MessageID ) ) res = 0;

		if ( res >= MESSAGE_PARAMETERS )// FJ_MESSAGE string must have at least 9 field delimiters!!!!!
		{
			pfm = fj_MessageNew();
			fj_processParameterStringInput( pParams[ 1] );
			strncpy( pfm->strName,           pParams[1], FJMESSAGE_NAME_SIZE ); pfm->strName[FJMESSAGE_NAME_SIZE] = 0;
			pfm->lCount  = atol(pParams[2]);
			if ( pfm->lCount > FJMESSAGE_NUMBER_SEGMENTS ) pfm->lCount = FJMESSAGE_NUMBER_SEGMENTS;

			pfm->papfe = (LPFJELEMENT *)fj_malloc( pfm->lCount * sizeof (LPFJELEMENT) );
			for ( i = 0; i < pfm->lCount; i++)
			{
				if ( NULL == pParams[MESSAGE_PARAMETERS+i] )
				{
					pfm->papfe[i] = NULL;
				}
				else
				{
					strncpy( strName, pParams[MESSAGE_PARAMETERS+i], FJELEMENT_NAME_SIZE ); strName[FJELEMENT_NAME_SIZE] = 0;
					fj_processParameterStringInput( strName );
					// parse type out of middle of name
					res = (int)fj_ParseKeywordsFromStr( strName, '-', pParamsName, 3 );
					if ( res >= 2 )
					{
						pfe = fj_ElementNewFromTypeString( pParamsName[1] );
						pfm->papfe[i] = pfe;
						if ( NULL != pfe )
						{
							pfe->pfm = pfm;
							pfe->pActions->fj_DescBuildFromName( pfms, pfe, pParams[MESSAGE_PARAMETERS+i] );
						}
					}
				}
			}
		}
		fj_free( pStrM );
	}

	return( pfm );
}
//
//	FUNCTION: fj_MessageBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJMESSAGE from FJMEMSTOR, with its FJSEGMENTs
//
FJDLLExport LPFJMESSAGE fj_MessageBuildFromName( CLPCFJMEMSTOR pfms, LPCSTR pName )
{
	LPFJMESSAGE pfm;
	LPSTR       pMsgStr;

	pMsgStr = (LPSTR)fj_MemStorFindElement( pfms, fj_MessageID, pName );
	pfm = fj_MessageBuildFromString( pfms, pMsgStr );

	return( pfm );
}
//
//	FUNCTION: fj_MessageAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJMESSAGE element to FJMEMSTOR, with all the segments
//
FJDLLExport BOOL fj_MessageAddToMemstor( LPFJMEMSTOR pfms, CLPCFJMESSAGE pfm )
{
	LPFJELEMENT pfe;
	CHAR   buff[FJMESSAGE_MEMSTOR_SIZE];
	STR    sName[FJELEMENT_NAME_SIZE*2];
	LPSTR  pTmpStr;
	int    i;
	BOOL   bRetS;
	BOOL   bRetM;

	bRetS = TRUE;
	bRetM = TRUE;

	if ( NULL != pfm )
	{
		// message Name
		fj_processParameterStringOutput( sName, pfm->strName );
		sprintf( buff, "{%s,%s,%d}",
			fj_MessageID,
			sName,
			pfm->lCount);				// count of segments
		pTmpStr = buff + strlen( buff ) - 1;

		for ( i = 0; i < pfm->lCount; i++)
		{
			*pTmpStr++ = ',';
			pfe = pfm->papfe[i];
			if ( NULL != pfe )
			{
				bRetS &= pfe->pActions->fj_DescAddToMemstor( pfms, pfe );
				fj_processParameterStringOutput( pTmpStr, pfe->strName );
				pTmpStr += strlen( pTmpStr );
			}
		}
		*pTmpStr++ = '}';
		*pTmpStr = '\0';
		bRetM = fj_MemStorAddElementString( pfms, buff );
	}

	return( bRetS & bRetM );
}
//
//	FUNCTION: fj_MessageDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Remove FJMESSAGE element from FJMEMSTOR, with all the elementss
//
FJDLLExport BOOL fj_MessageDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJMESSAGE pfm )
{
	LPFJELEMENT pfe;
	STR    sName[FJMESSAGE_NAME_SIZE*2];
	int    i;
	BOOL   bRetS;
	BOOL   bRetM;

	bRetS = TRUE;
	bRetM = TRUE;

	if ( NULL != pfm )
	{
		for ( i = 0; i < pfm->lCount; i++)
		{
			pfe = pfm->papfe[i];
			if ( NULL != pfe )
			{
				bRetS &= pfe->pActions->fj_DescDeleteFromMemstor( pfms, pfe );
			}
		}
		fj_processParameterStringOutput( sName, pfm->strName );
		bRetM = fj_MemStorDeleteElement( pfms, fj_MessageID, sName );
	}

	return( bRetS & bRetM );
}
//
//	FUNCTION: fj_MessageDeleteFromMemstorByName
//
//	Return: TRUE if success.
//
//  Delete a FJMESSAGE from FJMEMSTOR, with its FJSEGMENTs
//
FJDLLExport BOOL fj_MessageDeleteFromMemstorByName( LPFJMEMSTOR pfms, LPCSTR pName )
{
#define MESSAGE_PARAMETERS 3
	LPSTR  pParams[MESSAGE_PARAMETERS+FJMESSAGE_NUMBER_SEGMENTS];
	LPSTR  pParamsName[3];
	LPSTR  pStrM;
	LPSTR  pMsgStr;
	STR    strName[FJELEMENT_NAME_SIZE+1];
	int    res;
	int    i;
	BOOL   ret;
	LONG   lCount = 0;

	ret = FALSE;

	if ( NULL == pName) return(ret);

	pMsgStr = (LPSTR)fj_MemStorFindElement( pfms, fj_MessageID, pName );

	if ( NULL != pMsgStr)
	{

		res = 0;
		pStrM = (LPSTR)fj_malloc( FJMESSAGE_MEMSTOR_SIZE+1 );
		strncpy( pStrM, pMsgStr, FJMESSAGE_MEMSTOR_SIZE );
		*(pStrM+FJMESSAGE_MEMSTOR_SIZE ) = 0;

		res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, MESSAGE_PARAMETERS+FJMESSAGE_NUMBER_SEGMENTS );
		if ( 0 != strcmp( pParams[0], fj_MessageID ) ) res = 0;

		if ( res >= MESSAGE_PARAMETERS )// FJ_MESSAGE string must have at least 9 field delimiters!!!!!
		{
			fj_processParameterStringInput( pParams[ 1] );
			lCount  = atol(pParams[2]);
			for ( i = 0; i < lCount; i++)
			{
				strncpy( strName, pParams[MESSAGE_PARAMETERS+i], FJELEMENT_NAME_SIZE );
				strName[FJELEMENT_NAME_SIZE] = 0;
				fj_processParameterStringInput( strName );
				res = (int)fj_ParseKeywordsFromStr( strName, '-', pParamsName, 3 );
				if ( res >= 2 ) fj_MemStorDeleteElement( pfms, pParamsName[1], pParams[MESSAGE_PARAMETERS+i] );
			}
		}
		fj_free( pStrM );
	}
	ret = fj_MemStorDeleteElement( pfms, fj_MessageID, pName );

	return( ret );
}
