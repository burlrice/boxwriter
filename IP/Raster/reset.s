#
#            Copyright (c) 1998-2000 by NETsilicon Inc.
#
#  This software is copyrighted by and is the sole property of 
#  NETsilicon.  All rights, title, ownership, or other interests
#  in the software remain the property of NETsilicon.  This
#  software may only be used in accordance with the corresponding
#  license agreement.  Any unauthorized use, duplication, transmission,  
#  distribution, or disclosure of this software is expressly forbidden.
#
#  This Copyright notice may not be removed or modified without prior
#  written consent of NETsilicon.
#
#  NETsilicon, reserves the right to modify this software
#  without notice.
#
#  NETsilicon
#  411 Waverley Oaks Road                  USA 781.647.1234
#  Suite 227                               http://www.netsilicon.com
#  Waltham, MA 02452                       AmericaSales@netsilicon.com
#
#************************************************************************
#
#     Module Name: reset.s
#         Version: 1.00
#   Original Date:  7-Dec-09:45
#          Author: 
#        Language: ARM Assembly
# Compile Options: 
# Compile defines: 
#	 Libraries: 
#    Link Options: linker defines include
#
#    Entry Points: 
#
# Description.
# =======================================================================
# Define the ARM7 vector area.  This should be located or copied to 0.  
# !!! Do not change any line in this file !!!
# 
#                                                                        
#                                                                        
# Edit Date/Ver   Edit Description
# ==============  ===================================================
# LZ   04/16/02   Made changes to work with GNU.
# 
#
#
#
    
# reset.s
#
#     when use 32 bit flash and cache is on, there are two bits in Memory
#     Module Configuration Register need to be cleaned upon power up reset, 
#     so we don't have vector table in reset.s, instead, we restore the 
#     vector table in ncc_post.c for POST test and in main() after 
#     C initialization.
#
#
#
#

    .section   .reset 
    .globl  __vectors
    .globl  Reset_Handler_ROM
    .globl  NA_MAIN
    .globl  UNDEFINED
    .globl  SWI
    .globl  PREFETCH
    .globl  ABORT
    .globl  RESERVED
    .globl  NAIRQ
    .globl  FIQ
   
__vectors: 
    LDR    R0, MEM_MMCR
    LDR    R2, [R0]
    LDR    R1, MEM_MMCR_m2
    AND    R2, R2, R1
    STR    R2, [R0]

    LDR    pc, NA_MAIN                        @Jump to NET+ARM  startup
    NOP
    NOP  
   
#######################################################################################
#
#   Note:   the following labels: NA_MAIN, UNDEFINED, SWI, PREFETCH, ABORT, RESERVED, 
#           NAIRQ, and FIQ are wrong due to a GH Assmebler bug.  For some reason, their
#           is an extra 12 bytes of data.  Therefore, do not use any of these labels as
#           addresses. 
#
#           The table prior code starting at __vectors gets overwritten by C functions
#           setupVectorTable(), which places a  "LDR PC, [PC,24]", which loads the PC
#           the contents plus 20 (not 24).  
#
#           Therefore, the vector table begins at 0x20, where:
#               address 0x20 - RESET                    (Reset_Handler_ROM)
#               address 0x24 - UNDEFINED INSTRUCTION    (Undefined_Handler)
#               address 0x28 - SWI                      (SWI_Handler)
#               address 0x2c - PREFETCH                 (Prefetch_Handler)
#               address 0x30 - ABORT                    (Abort_Handler)
#               address 0x34 - RESERVED                 (Address_Error_Handler)
#               address 0x38 - NAIRQ                    (IRQ_Handler)
#               address 0x3c - FIQ                      (FIQ_Handler)
#
#           Note the NAIRQ handler is not overwritten in the setupVectorTable() function,
#           but is instead overwritten by the ThreadX initialization code.
#
#

NA_MAIN:
     .word     Reset_Handler_ROM
UNDEFINED:
    .word      Undefined_Handler                  @ Undefined handler
SWI:
    .word      SWI_Handler                        @ Software interrupt handler
PREFETCH:
    .word      Prefetch_Handler                   @ Prefetch exeception handler
ABORT:
    .word      Abort_Handler                      @Abort exception handler
RESERVED:
    .word      Address_Error_Handler              @ Reserved exception handler
NAIRQ:
    .word      IRQ_Handler                        @ IRQ interrupt handler
FIQ:
    .word      FIQ_Handler                        @ FIQ interrupt handler

MEM_MMCR:
    .word      0xffc00000
MEM_MMCR_m2:     
    .word      0xfff3ffff  
      
