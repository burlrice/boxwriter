/* C file: c:\MyPrograms\FoxJet1\_test.c created
 by NET+Works HTML to C Utility on Tuesday, July 25, 2000 10:50:04 */
#include "fj.h"
#include "fj_printhead.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_TEST_NOZZLES */
static void na_TEST_NOZZLES (unsigned long handle);
/*  @! File: na_TITLE_TEST */
static void na_TITLE_TEST (unsigned long handle);
/*  @! File: na_TEST_NUMNOZZ */
static void na_TEST_NUMNOZZ (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_TEST_PCEN */
static void na_TEST_PCEN (unsigned long handle);
/*  @! File: na_TEST_PCTEST */
static void na_TEST_PCTEST (unsigned long handle);
/*  @! File: na_TEST_PCDIS */
static void na_TEST_PCDIS (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

void Send_function_25bug(unsigned long handle)
{
}
/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\test.htm {{ */
void Send_function_25(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_TEST (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Test\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "function x1(vi) {\n");
	HSSend (handle, "var vo=\"0\";\n");
	HSSend (handle, "if (\"0001\"==vi)vo=\"1\";else if(\"0010\"==vi)vo=\"2\";else if(\"0011\"==vi)vo=\"3\";else if(\"0100\"==vi)vo=\"4\";else if(\"0101\"==vi)vo=\"5\";else if(\"0110\"==vi)vo=\"6\";else if(\"0111\"==vi)vo=\"7\";else if(\"1000\"==vi)vo=\"8\";else if(\"1001\"==vi)vo=\"9\";else if(\"1010\"==vi)vo=\"A\";else if(\"1011\"==vi)vo=\"B\";else if(\"1100\"==vi)vo=\"C\";else if(\"1101\"==vi)vo=\"D\";else if(\"1110\"==vi)vo=\"E\";else if(\"1111\"==vi)vo=\"F\";\n");
	HSSend (handle, "return vo;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function x2(vi) {\n");
	HSSend (handle, "var vo=\"\";\n");
	HSSend (handle, "if (\"0\"==vi)vo=\"0000\";else if(\"1\"==vi)vo=\"0001\";else if(\"2\"==vi)vo=\"0010\";else if(\"3\"==vi)vo=\"0011\";else if(\"4\"==vi)vo=\"0100\";else if(\"5\"==vi)vo=\"0101\";else if(\"6\"==vi)vo=\"0110\";else if(\"7\"==vi)vo=\"0111\";else if(\"8\"==vi)vo=\"1000\";else if(\"9\"==vi)vo=\"1001\";else if(\"A\"==vi)vo=\"1010\";else if(\"B\"==vi)vo=\"1011\";else if(\"C\"==vi)vo=\"1100\";else if(\"D\"==vi)vo=\"1101\";else if(\"E\"==vi)vo=\"1110\";else if(\"F\"==vi)vo=\"1111\";else if(\"a\"==vi)vo=\"1010\";else if(\"b\"==vi)vo=\"1011\";else if(\"c\"==vi)vo=\"1100\";else if(\"d\"==vi)vo=\"1101\";else if(\"e\"==vi)vo=\"1110\";else if(\"f\"==vi)vo=\"1111\";\n");
	HSSend (handle, "return vo;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "var numnozz=");
	na_TEST_NUMNOZZ (handle);
	HSSend (handle, "\n");
	HSSend (handle, "var patold = \"\";\n");
	HSSend (handle, "function tclick() {\n");
	HSSend (handle, "var pat  = \"\";\n");
	HSSend (handle, "var patx = \"\";\n");
	HSSend (handle, "var cb, i;\n");
	HSSend (handle, "\n");
	HSSend (handle, "for (i=0;i<numnozz;i++)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "  cb = document.formtest(\"n\"+i);\n");
	HSSend (handle, "  if (null == cb) break;\n");
	HSSend (handle, "  if (cb.checked) pat += \"1\";\n");
	HSSend (handle, "  else            pat += \"0\";\n");
	HSSend (handle, "}\n");
	HSSend (handle, "while (\"\"!=pat)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "  patx += x1(pat.substr(0,4));\n");
	HSSend (handle, "  pat = pat.substr(4,999);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "document.formtest.pattern.value = patx;\n");
	HSSend (handle, "patold = patx;}\n");
	HSSend (handle, "function pclick() {\n");
	HSSend (handle, "var pat  = \"\";\n");
	HSSend (handle, "var patx = document.formtest.pattern.value;\n");
	HSSend (handle, "var pat1, patnew, cb, i, perror;\n");
	HSSend (handle, "\n");
	HSSend (handle, "perror = false;\n");
	HSSend (handle, "patnew = patx;\n");
	HSSend (handle, "while (\"\"!=patx)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "  pat1 = x2(patx.substr(0,1));\n");
	HSSend (handle, "  if (\"\"==pat1)\n");
	HSSend (handle, "  {\n");
	HSSend (handle, "    pat1 = \"0000\";\n");
	HSSend (handle, "    perror = true;\n");
	HSSend (handle, "  }\n");
	HSSend (handle, "  pat += pat1;\n");
	HSSend (handle, "  patx = patx.substr(1,999);\n");
	HSSend (handle, "}  \n");
	HSSend (handle, "for (i=0;i<numnozz;i++)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "  cb = document.formtest(\"n\"+i);\n");
	HSSend (handle, "  if (null == cb) break;\n");
	HSSend (handle, "  if (\"1\" == pat.substr(i,1)) cb.checked = true;\n");
	HSSend (handle, "  else                        cb.checked = false;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "if (perror){document.formtest.Bpat.value = \"Error: non-hex input\";document.formtest.Bpat.enabled = false;}\n");
	HSSend (handle, "else{document.formtest.Bpat.value = \"Print test pattern\";document.formtest.Bpat.enabled = true;}\n");
	HSSend (handle, "document.formtest.pattern.value = patnew;\n");
	HSSend (handle, "patold = patnew;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function offBut() {\n");
	HSSend (handle, "document.formtest.pattern.value = 0;\n");
	HSSend (handle, "pclick();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function onBut() {\n");
	HSSend (handle, "var pat=\"\";\n");
	HSSend (handle, "for (i=0;i<numnozz;i+=4){pat+=\"F\";}\n");
	HSSend (handle, "document.formtest.pattern.value = pat;\n");
	HSSend (handle, "pclick();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "\n");
	HSSend (handle, "function submitT(sa) {\n");
	HSSend (handle, "  var of=document.formtest;\n");
	HSSend (handle, "  of.action.value=sa;\n");
	HSSend (handle, "  of.submit();  }\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<form name=formtest action=test_1 method=POST>\n");
	HSSend (handle, " <table cellpadding=5>\n");
	HSSend (handle, "  <input type=hidden value=none name=action >\n");
	HSSend (handle, "  <input type=hidden name=pwid   value=");
	na_PWID (handle);
	HSSend (handle, ">\n");
	HSSend (handle, "\n");
	HSSend (handle, "  <tr><td align=center><input type=button name=pcen   value=\"Photocell Enable\" ");
	na_TEST_PCEN (handle);
	HSSend (handle, " onClick='submitT(\"pcen\");'>\n");
	HSSend (handle, "					   <input type=button name=pctest value=\"Photocell Test Mode\" ");
	na_TEST_PCTEST (handle);
	HSSend (handle, " onClick='submitT(\"pctest\");'>\n");
	HSSend (handle, "					   <input type=button name=pcdis  value=\"Photocell Disable\" ");
	na_TEST_PCDIS (handle);
	HSSend (handle, " onClick='submitT(\"pcdis\");'></td></tr>\n");
	HSSend (handle, "  <tr><td align=center><input type=button name=runams value=\"Run AMS\" onClick='submitT(\"ams\");'></td></tr>\n");
	HSSend (handle, "  <tr><td align=center><input type=button name=prmess value=\"Print one message\" onClick='submitT(\"p_message\");'></td></tr>\n");
	HSSend (handle, "  <tr><td align=center><input type=button name=pstep  value=\"Print one step pattern\" onClick='submitT(\"p_step\");'></td></tr>\n");
	HSSend (handle, "  <tr><td align=center><table border=1 frame=border rules=none cellpadding=8>    \n");
	HSSend (handle, "    ");
	na_TEST_NOZZLES (handle);
	HSSend (handle, "\n");
	HSSend (handle, "    </table></td></tr>\n");
	HSSend (handle, "  <tr><td align=center><input type=button name=btga  value=\"Send all Gate Array values\" onClick='submitT(\"ga\");'>\n");
	HSSend (handle, "					   <input type=button name=bteni value=\"Initialize ENI interface\" onClick='submitT(\"eni\");'></td></tr>\n");
	HSSend (handle, " </table>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "pclick();\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\test.htm }} */
/* @! End HTML page   */

static int  tCount;

static void get_parms(unsigned long handle, char *tPattern, int patternSize)
{
	char   formField[16];
	int    ret;

	ret = fj_HSGetValue( handle, "pattern", tPattern, patternSize );
	ret = fj_HSGetValue( handle, "count", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		tCount = atoi(formField);
		if ( tCount > 9999 ) tCount = 9999;
	}
}
void reload_test(unsigned long handle, char *tPattern, int patternSize)
{
	char str[FJSYS_NOZZLES/4 + 48];
	get_parms(handle, tPattern, patternSize);
	sprintf( str, "test.htm?pattern=%s&count=%d", tPattern, tCount );
	fj_reload_page(handle, str);
}
/* @! Begin of post function */
/* @! Function: Post_test_1  {{ */
void Post_test_1(unsigned long handle)
{
	/* add your implementation here: */
	CLPFJ_GLOBAL pGlobal  = pCVT->pFJglobal;
	char str[FJSYS_NOZZLES/4 + 4];
	char formField[80];
	int  iCount;
	int  ret;

	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp( formField, "pcen" ) )
		{
			pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
			pCVT->fj_print_head_SendPhotocellMode();
		}
		else if ( 0 == strcmp( formField, "pctest" ) )
		{
			pGlobal->lPhotocellMode = FJ_PC_MODE_TEST;
			pCVT->fj_print_head_SendPhotocellMode();
		}
		else if ( 0 == strcmp( formField, "pcdis" ) )
		{
			pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
			pCVT->fj_print_head_SendPhotocellMode();
		}
		else if ( 0 == strcmp( formField, "ga" ) )
		{
			pCVT->fj_print_head_SetGAValues();
		}
		else if ( 0 == strcmp( formField, "eni" ) )
		{
			pCVT->fj_print_head_KillENI();
			pCVT->fj_print_head_InitENI();
		}
		else if ( 0 == strcmp( formField, "p_message" ) )
		{
			pCVT->fj_print_head_PhotoCell();
		}
		else if ( 0 == strcmp( formField, "p_step" ) )
		{
			pCVT->fj_print_head_PrintStepPattern();
		}
		else if ( 0 == strcmp( formField, "p_pat" ) )
		{
			ret = fj_HSGetValue( handle, "pattern", str, sizeof(str) );
			ret = fj_HSGetValue( handle, "count", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				iCount = atoi(formField);
				if ( iCount > 9999 ) iCount = 9999;
			}
			pCVT->fj_print_head_PrintTestPattern( str, iCount );
		}
		else if ( 0 == strcmp( formField, "ams" ) )
		{
										// set AMS start time to now
			pCVT->pBRAM->lAMS_SST = pGlobal->lSeconds;
		}
	}
	reload_test(handle, str, sizeof(str)-1);
}										/* @! Function: Post_test_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_TEST_NOZZLES  {{ */
void na_TEST_NOZZLES(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LONG  lNozzles;
	char  strPattern[FJSYS_NOZZLES/4 + 4];
	STR   str[88];
	int   i,n;
	lNozzles = pfph->pfphy->lChannels;
	n = 0;

	get_parms(handle, strPattern, sizeof(strPattern));
	while ( n < lNozzles )
	{
		HSSend (handle, "<tr>");
		for ( i = 0; i < 8; i++ )
		{
			HSSend(handle, "<td><input type=checkbox value=OFF onClick=\"tclick();\" name=n");
			na_Long(handle, n);
			HSSend(handle, ">");
			na_Long(handle, n+1);
			HSSend(handle, "</td>");
			n++;
		}
		HSSend (handle, "</tr>\n");
	}
	HSSend (handle, "<tr><td align=left colspan=2><input type=button value=\"All On\" onClick=\"onBut();\"></td>");
	HSSend (handle, "<td align=right colspan=2>Pattern</td><td align=left colspan=4><input type=text name=pattern size=32 onchange=\"pclick();\" value=");
	HSSend (handle, strPattern);
	HSSend (handle, "></td></tr>\n");
	HSSend (handle, "<tr><td align=left colspan=2><input type=button value=\"All Off\" onClick=\"offBut();\"></td>");
	HSSend (handle, "<td align=right colspan=2>Count</td><td align=left colspan=4><input type=text name=count size=8 value=");
	na_Long (handle, tCount);
	HSSend (handle, "></td></tr>\n");
	HSSend (handle, "<tr><td align=center colspan=8><input type=button value=\"Print test pattern\" onClick='submitT(\"p_pat\");' name=Bpat></td></tr>\n");
}										/* @! Function: na_TEST_NOZZLES  }} */
/* @! Function: na_TEST_NUMNOZZ  {{ */
void na_TEST_NUMNOZZ(unsigned long handle)
{
	/* add your implementation here: */
	na_Long (handle, pCVT->pfph->pfphy->lChannels);
}										/* @! Function: na_TEST_NUMNOZZ  }} */
/* @! Function: na_TITLE_TEST  {{ */
void na_TITLE_TEST(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Test" );
}										/* @! Function: na_TITLE_TEST  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_TEST_PCEN  {{ */
void na_TEST_PCEN(unsigned long handle)
{
	/* add your implementation here: */
	if ( FJ_PC_MODE_ENABLED == pCVT->pFJglobal->lPhotocellMode ) HSSend (handle, "DISABLED");
}										/* @! Function: na_TEST_PCEN  }} */
/* @! Function: na_TEST_PCTEST  {{ */
void na_TEST_PCTEST(unsigned long handle)
{
	/* add your implementation here: */
	if ( FJ_PC_MODE_TEST == pCVT->pFJglobal->lPhotocellMode ) HSSend (handle, "DISABLED");
}										/* @! Function: na_TEST_PCTEST  }} */
/* @! Function: na_TEST_PCDIS  {{ */
void na_TEST_PCDIS(unsigned long handle)
{
	/* add your implementation here: */
	if ( FJ_PC_MODE_DISABLED == pCVT->pFJglobal->lPhotocellMode ) HSSend (handle, "DISABLED");
}										/* @! Function: na_TEST_PCDIS  }} */
/* @! End of dynamic functions   */
