/* C file: c:\MyPrograms\FoxJet1\_printhead.c created
 by NET+Works HTML to C Utility on Monday, May 08, 2000 13:47:06 */
#include "fj.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_VDat */
void na_VDat (unsigned long handle);
/*  @! File: na_VNum */
void na_VNum (unsigned long handle);
/*  @! File: na_DManu */
void na_DManu (unsigned long handle);
/*  @! File: na_DIns */
void na_DIns (unsigned long handle);
/*  @! File: na_SN */
void na_SN (unsigned long handle);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_DManu */
void na_DManu (unsigned long handle);
/*  @! File: na_DIns */
void na_DIns (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_IPA */
void na_IPA (unsigned long handle);
/*  @! File: na_MENUINIT */
void na_MENUINIT (unsigned long handle);
/*  @! File: na_TITLE_PRINTER */
static void na_TITLE_PRINTER (unsigned long handle);
/*  @! File: na_GAVNum */
void na_GAVNum (unsigned long handle);
/*  @! File: na_PW */
static void na_PW (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_STAvail */
void na_STAvail (unsigned long handle);
/*  @! File: na_DGA */
void na_DGA (unsigned long handle);
/*  @! File: na_PHtype */
void na_PHtype (unsigned long handle);

/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\printhead.htm {{ */
void Send_function_20(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<title>");
	na_TITLE_PRINTER (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Printer\"\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p align=\"right\">\n");
	HSSend (handle, "<table border=\"0\">\n");
	HSSend (handle, "<COLGROUP>\n");
	HSSend (handle, "<col align=\"right\">\n");
	HSSend (handle, "<col align=\"left\">\n");
	HSSend (handle, "<col align=\"left\">\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Printhead Type:</td>\n");
	HSSend (handle, "<td colspan=\"2\">");
	na_PHtype (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Version:</td>\n");
	HSSend (handle, "<td>");
	na_VDat (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td><a href=\"VersionInfo.htm?pwid=");
	na_PWID (handle);
	HSSend (handle, "\">");
	na_VNum (handle);
	HSSend (handle, "</a></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Gate Array:</td>\n");
	HSSend (handle, "<td>");
	na_DGA (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td>");
	na_GAVNum (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Available Storage:</td>\n");
	HSSend (handle, "<td>");
	na_STAvail (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td>&nbsp;</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<hr>\n");
	HSSend (handle, "<p align=\"center\">\n");
	HSSend (handle, "<table border=\"0\" cellpadding=\"1\" rules=\"groups\">\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr valign=\"center\">\n");
	HSSend (handle, "<td><left><b>Selected Label Name:</b></td>\n");
	HSSend (handle, "<td><APPLET height=\"14\" width=\"400\" code=\"fjinteger.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_LabSelName\" VIEWASTEXT>\n");
	HSSend (handle, "<PARAM NAME=\"font\" VALUE=\"Dialog\">\n");
	HSSend (handle, "<param name=\"style\" value=\"bold\">\n");
	HSSend (handle, "<PARAM NAME=\"color\" VALUE=\"0x000000\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"12\">\n");
	HSSend (handle, "<param name=\"type\" value=\"text\">\n");
	HSSend (handle, "</APPLET></LEFT></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr valign=\"center\">\n");
	HSSend (handle, "<td><left><b>Selected Label Count:</b></td>\n");
	HSSend (handle, "<td><APPLET height=\"14\" width=\"96\" code=\"fjinteger.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Integer_LabSelCount\" VIEWASTEXT>\n");
	HSSend (handle, "<PARAM NAME=\"font\" VALUE=\"Dialog\">\n");
	HSSend (handle, "<param name=\"style\" value=\"bold\">\n");
	HSSend (handle, "<PARAM NAME=\"color\" VALUE=\"0x000000\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"12\">\n");
	HSSend (handle, "<param name=\"type\" value=\"integer\">\n");
	HSSend (handle, "</APPLET></LEFT></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"4\" rules=\"groups\" with=\"75%\" height=\"140\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "<colgroup span=\"6\"></colgroup><colgroup span=\"10\"></colgroup>\n");
	HSSend (handle, "<thead>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td colspan=\"6\" align=\"middle\"><b>Head</b></td>\n");
	HSSend (handle, "<td colspan=\"10\" align=\"middle\"><b>Board</b></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</thead><tbody>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Temperature</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Ink</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Voltage</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>PhotoCell</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Data</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Shaft Encoder</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Battery</b></td>\n");
	HSSend (handle, "<td colspan=\"2\" align=\"middle\"><b>Clock</b></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Ht1Ready\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Ready</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Ink\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFFFF20\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Level</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_VoltageReady\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Ready</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_PhotoCell\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFFFFFF\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Detect</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_PrintData\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFFFFFF\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Printing</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_SE\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Running</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Battery\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">OK</td>\n");
	HSSend (handle, "<td align=\"middle\" height=\"18\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Clock\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Set</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td height=\"19\" align=\"middle\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_Ht1On\">\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFFFFFF\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Heater On</td>\n");
	HSSend (handle, "<td colspan=\"4\" height=\"18\">&nbsp;</td>\n");
	HSSend (handle, "<td height=\"18\" align=\"middle\"><APPLET height=\"16\" width=\"16\" code=\"fjlight.class\" name=\"");
	na_ID (handle);
	HSSend (handle, " Light_PhotoCellMode\" VIEWASTEXT>\n");
	HSSend (handle, "<PARAM NAME=\"coloroff\" VALUE=\"0xFF0000\">\n");
	HSSend (handle, "<PARAM NAME=\"coloron\" VALUE=\"0x00FF00\">\n");
	HSSend (handle, "<PARAM NAME=\"size\" VALUE=\"11\">\n");
	HSSend (handle, "<PARAM NAME=\"color1\" VALUE=\"0xFFa030\">\n");
	HSSend (handle, "</APPLET><br>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=\"left\" height=\"18\">Enabled</td>\n");
	HSSend (handle, "<td colspan=\"8\" height=\"18\">&nbsp;</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody><tbody>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td colspan=\"16\" height=\"18\">\n");
	HSSend (handle, "<center><APPLET height=\"16\" width=\"580\" code=\"fjsocket.class\" name=\"fjsocket\" VIEWASTEXT><param name=\"id\" value=\"");
	na_ID (handle);
	HSSend (handle, "\">\n");
	HSSend (handle, "</APPLET></center>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<form action=\"pw_1\" method=\"post\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\">\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table border=\"0\">\n");
	HSSend (handle, "<COLGROUP>\n");
	HSSend (handle, "<col align=\"right\">\n");
	HSSend (handle, "<col align=\"left\">\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Enter Password&nbsp;<input type=\"password\" name=\"password\" size=\"20\" value=\"");
	na_PW (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td><center><input class=\"fjbtn1\" type=\"submit\" value=\"Submit\"><input class=\"fjbtn1\" type=\"reset\" value=\"Reset\"></center>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\printhead.htm }} */
/* @! End HTML page   */

/* @! Begin of post function */
/* @! Function: Post_pw_1  {{ */
void Post_pw_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL     pGlobal = pCVT->pFJglobal;
	CLPCFJSYSTEM    pfsys   = pCVT->pfsys;
	CLPCFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSOCKET      pfsocket;
	char   pw[1+FJSYS_PASSWORD_SIZE];
	char   pwid[1+FJSYS_PASSWORD_SIZE];
	char   str[44];
	LONG   lLevel;
	ULONG  ipaddr;
	BOOL   bFound;
	int    i;
	int    ret;

	pw[0] = 0;
	ret = fj_HSGetValue( handle, "password", pw, sizeof(pw) );
	ipaddr = HSRemoteAddress( handle );
	lLevel = pCVT->fj_SystemGetPasswordLevel( pfsys, pw );

	// find any sockets from this IPaddr and set their security level
	bFound = FALSE;
	for ( i = 0; i < FJSYS_NUMBER_SOCKETS; i++ )
	{
		pfsocket = &(fj_asockets[i]);
		if ( ((TRUE == pfsocket->bInUse)||(0 < pfsocket->lSeconds)) && (ipaddr == pfsocket->lIPaddr) )
		{
			pfsocket->lSecLevel = lLevel;
			bFound = TRUE;
		}
	}
	// if not found, make a dummy. a socket connection will follow in a few seconds
	if ( FALSE == bFound )
	{
		for ( i = FJSYS_NUMBER_SOCKETS-1; i >= 0; i-- )
		{
			pfsocket = &(fj_asockets[i]);
			if ( FALSE == fj_asockets[i].bInUse )
			{
				pfsocket->lIPaddr = ipaddr;
				pfsocket->lSeconds = pGlobal->lSeconds;
				pfsocket->lSecLevel = lLevel;
				break;
			}
		}
	}
	pCVT->fj_SystemMakeLevelID( pfph, lLevel, ipaddr, pwid );
	strcpy( str, "printhead.htm?pwid=" );
	strcat( str, pwid );
	fj_reload_page(handle, str);
}										/* @! Function: Post_pw_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_VDat  {{ */
/* @! Function: na_VNum  {{ */
/* @! Function: na_MDat  {{ */
/* @! Function: na_InDat  {{ */
/* @! Function: na_SN  {{ */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_DManu  {{ */
/* @! Function: na_DIns  {{ */
/* @! Function: na_ID  {{ */
/* @! Function: na_IPA  {{ */
/* @! Function: na_IPA  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_STAvail  {{ */
/* @! Function: na_MENUINIT  {{ */
/* @! Function: na_TITLE_PRINTER  {{ */
void na_TITLE_PRINTER(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Printer" );
}										/* @! Function: na_TITLE_PRINTER  }} */
/* @! Function: na_PW  {{ */
void na_PW(unsigned long handle)
{
	/* add your implementation here: */
}										/* @! Function: na_PW  }} */
/* @! Function: na_GAVNum  {{ */
/* @! Function: na_DGA  {{ */
/* @! Function: na_PHtype  {{ */
/* @! End of dynamic functions   */
