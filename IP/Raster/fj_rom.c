/*
 *            Copyright (c) 2000 by FoxJet Inc.
 *
 */

#include "fj.h"
#include "fj_memstorage.h"
#include "fj_printhead.h"

//
// fj_writeROM in fj_writeROM.c does the actual write into the hardware ROM sectors.
//      It calculates and sets the ROM checksum.
//

WORD32 fj_checksumRomEntry( LPCROMENTRY pRomEntry, CLPCBYTE pBuffer )
{
	WORD32 *pCheckSum;
	WORD32  wCheckSum;
	LONG    lLength;
	int     i;

	if ( (int)pRomEntry < 100 )
	{
		pRomEntry = *(fj_CVT.pfj_RomTable) + (int)pRomEntry;
	}

	if ( NULL == pBuffer )
	{
		pCheckSum = (WORD32 *)(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset);
	}
	else
	{
		pCheckSum = (WORD32 *)(pBuffer);
	}
	lLength   = pRomEntry->lMaxLength - 4;
	wCheckSum = 0;
	for ( i = 0; i < lLength; i += 4, pCheckSum++ )
	{
		wCheckSum ^= *pCheckSum;
	}

	return( 0xffffffff - wCheckSum );
}
static int fj_setDefaultFJRomParams( LPFJ_PARAM pFJ_PARAM )
{
	int iRet;

	if (pFJ_PARAM == NULL)
	{
		iRet = -1;
	}
	else
	{
		fj_SystemInit( &(pFJ_PARAM->FJSys) );
		pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
		pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
		pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);
		fj_PrintHeadInit( &(pFJ_PARAM->FJPh) );
		pFJ_PARAM->FJPh.pfsys = &(pFJ_PARAM->FJSys);
		iRet = 0;
	}
	return( iRet );
}
/*
 *
 *  Function: int fj_ReadFJRomParams (LPFJ_PARAM pFJ_PARAM)
 *
 *  Description:
 *
 *      This routine reads the contents of the section of ROM that contains
 *      the FoxJet startup parameters.
 *
 *  Parameters:
 *
 *      pFJ_PARAM          pointer to buffer to read parameters into
 *
 *  Return Values:
 *
 *      0       success
 *      -1      unable to read parameters. default parameters set.
 */

int fj_ReadFJRomParams( LPFJ_PARAM pFJ_PARAM )
{
	LPCROMENTRY pRomEntry;
	LPFJMEMSTOR pfms;
	LPSTR      *papList;
	LPSTR       pStrM;
	LPBYTE      pMemStorROM;
	WORD32 *pCheckSum;
	WORD32  wCheckSum;
	int ret;

	struct tm tm;
	//fj_print_head_GetRTClockTMRegisters( &tm );
	ret = -1;

	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_PARMFJ;

	if ( NULL != pRomEntry )
	{
		if ( 0 != fj_CVT.pFJglobal->semaROMLock.tx_semaphore_count )
		{
			fj_writeInfo("ROM read FoxJet params - semaphore not locked\n" );
		}
		if ( NULL != pFJ_PARAM )
		{
			pCheckSum = (WORD32 *)(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset + pRomEntry->lMaxLength - 4);
			wCheckSum = fj_checksumRomEntry( pRomEntry, NULL );
										// if valid checksum
			if ( wCheckSum == *pCheckSum )
			{
				fj_SystemInit( &(pFJ_PARAM->FJSys) );
				pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
				pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
				pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);
				fj_PrintHeadInit( &(pFJ_PARAM->FJPh) );
				pFJ_PARAM->FJPh.pfsys = &(pFJ_PARAM->FJSys);

				// get ROM address of message store
				pMemStorROM = fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset;
				pfms = fj_CVT.pFJMemStor;
				fj_MemStorLoadFromBuffer( pfms, pMemStorROM );

				papList = fj_MemStorFindAllElements( pfms, "System" );
				if ( NULL != papList ) ret = 0;
				while ( NULL != *papList )
				{
					pStrM = *papList;
					pStrM += 13;
					fj_SystemFromString( &(pFJ_PARAM->FJSys), pStrM );
					papList++;
				}
				papList = fj_MemStorFindAllElements( pfms, "Group" );
				if ( NULL != papList ) ret = 0;
				while ( NULL != *papList )
				{
					pStrM = *papList;
					pStrM += 13;
					fj_SystemGroupFromString( &(pFJ_PARAM->FJSys), pStrM );
					papList++;
				}
				papList = fj_MemStorFindAllElements( pfms, "Printer" );
				if ( NULL != papList ) ret = 0;
				while ( NULL != *papList )
				{
					pStrM = *papList;
					pStrM += 13;
					fj_PrintHeadFromString( &(pFJ_PARAM->FJPh), pStrM );
					papList++;
				}
			}
			if ( ret != 0 )
			{
				// either couldn't read ROM, or the checksum for the data read is invalid.
										// return default parameters.
				fj_setDefaultFJRomParams( pFJ_PARAM );
			}
			pFJ_PARAM->FJPh.pfop  = &(pFJ_PARAM->FJPhOp );
			pFJ_PARAM->FJPh.pfphy = &(pFJ_PARAM->FJPhPhy);
			pFJ_PARAM->FJPh.pfpkg = &(pFJ_PARAM->FJPhPkg);

			// check SE params
			{
				LONG GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
				BYTE GA_Mver = ((GAver>>4)& 0x0F);
				BYTE GA_Lver = (GAver & 0x0F);
				LPFJSYSTEM pfsys = &(pFJ_PARAM->FJSys);

				if ( 1 <= GA_Mver )
				{
					if ( 1 == GA_Mver && 3 > GA_Lver)
						pfsys->fEncoder = pfsys->fEncoderWheel / pfsys->lEncoderDivisor;
					else
						pfsys->fEncoder = pfsys->fEncoderWheel * 2 / pfsys->lEncoderDivisor;
				}
				else
					pfsys->fEncoder = pfsys->fEncoderWheel / pfsys->lEncoderDivisor;
			}

			pFJ_PARAM->FJPh.pfsys = &(pFJ_PARAM->FJSys);
		}
	}
	//pCVT->fj_print_head_SetRTClockTM( &tm );
	return( ret );
}
/*
 *
 *  Function: VOID fj_WriteFJRomParams (LPFJ_PARAM pFJ_PARAM)
 *
 *  Description:
 *
 *      This function writes FoxJet parameters to ROM.
 *
 *  Parameters:
 *
 *      pFJ_PARAM      pointer to structure with parameters
 *
 *
 */

VOID fj_WriteFJRomParams( LPFJ_PARAM pFJ_PARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue )
{
	ROMQUEUE q;
	LPCROMENTRY  pRomEntry;
	LPFJMEMSTOR  pfms;
	LPSTR       *papList;
	LPSTR        pStrM;
	LPBYTE       pBuff;
	LPBYTE       pMemStorROM;
	LONG         lMemStorSize;
	//WORD32       wCheckSum;

	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_PARMFJ;

	// get ROM address of message store
	pMemStorROM = fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset;
										// allow space for checksum
	lMemStorSize  = pRomEntry->lMaxLength - 4;
	pfms = fj_CVT.pFJMemStor;
	fj_MemStorLoadFromBuffer( pfms, pMemStorROM );

	pStrM = (LPSTR)fj_malloc( 24 + FJSYS_MEMSTOR_SIZE );

	strcpy( pStrM, "{System,Syst," );
	fj_SystemToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{System,Time," );
	fj_SystemRomTimeToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{System,Date," );
	fj_SystemDateStringsToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{System,Inet," );
	fj_SystemInternetToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{System,Pass," );
	fj_SystemPasswordToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{System,BarC," );
	fj_SystemBarCodesToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Group,Group," );
	fj_SystemGroupToString( &(pFJ_PARAM->FJSys), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Printer,Prt," );
	fj_PrintHeadToString( &(pFJ_PARAM->FJPh), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Printer,Pkg," );
	fj_PrintHeadPackageToString( &(pFJ_PARAM->FJPh), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Printer,Phy," );
	fj_PrintHeadPhysicalToString( &(pFJ_PARAM->FJPh), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Printer,Ope," );
	fj_PrintHeadOperationalToString( &(pFJ_PARAM->FJPh), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	strcpy( pStrM, "{Printer,Edt," );
	fj_PrintHeadGetEditStatus( &(pFJ_PARAM->FJPh), pStrM+13 );
	fj_MemStorAddElementString( pfms, pStrM );

	fj_free( pStrM );

	q.lRomEntry      = ROM_PARMFJ;
	q.pBuffer        = (LPBYTE)pFJ_PARAM;
	q.pBuffer        = (LPBYTE)pfms->pBuffer;
	q.lBufferSize    = pfms->lSize;
	q.bFreeBuffer    = FALSE;
	q.bFreeLock      = bFreeLock;
	q.pCallBack      = pCallBack;
	q.lCallBackValue = lCallBackValue;
	tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );

	// if pointer is not equal to the shared copy of the ROM data,
	if ( pFJ_PARAM != fj_CVT.pFJparam )
	{
		// copy new data to shared copy
		memcpy( fj_CVT.pFJparam, pFJ_PARAM, sizeof(FJ_PARAM) );
	}

	return;
}
/*
 *
 *  Function: int fj_ReadNETOSRomParams (PFJ_NETOSPARAM pFJ_NETOSPARAM)
 *
 *  Description:
 *
 *      This routine reads the contents of the section of ROM that contains
 *      the NETOS startup parameters.
 *
 *  Parameters:
 *
 *      pFJ_NETOSPARAM          pointer to buffer to read parameters into
 *
 *  Return Values:
 *
 *      0       success
 *      -1      unable to read parameters. default parameters set.
 */

int fj_ReadNETOSRomParams( LPFJ_NETOSPARAM pFJ_NETOSPARAM )
{
	LPCROMENTRY pRomEntry;
	WORD32 *pCheckSum;
	WORD32  wCheckSum;
	int  ret;

	struct tm tm;
	pCVT->fj_SystemGetTime( &tm );

	ret = -1;

	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_PARMNETOS;

	if ( NULL != pRomEntry )
	{
		if ( 0 != fj_CVT.pFJglobal->semaROMLock.tx_semaphore_count )
		{
			fj_writeInfo("ROM read NETOS params - semaphore not locked\n" );
		}
		if ( NULL != pFJ_NETOSPARAM )
		{
			memcpy( (char *)pFJ_NETOSPARAM, (char *)(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset), sizeof(FJ_NETOSPARAM));
			pCheckSum = (WORD32 *)(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset + pRomEntry->lMaxLength - 4);
			wCheckSum = fj_checksumRomEntry( pRomEntry, NULL );
										// if valid checksum
			if ( wCheckSum == *pCheckSum )
			{
				ret = 0;				// then return success
			}
			if ( ret != 0 )
			{
				// either couldn't read ROM, or the checksum for the data read is invalid.
				//		        fj_setDefaultFJRomParams( pFJ_PARAM );	// return default parameters.
			}
		}
	}
	pCVT->fj_print_head_SetRTClockTM( &tm );
	return( ret );
}
/*
 *
 *  Function: VOID fj_WriteNETOSRomParams (LPFJ_NETOSPARAM pFJ_NETOSPARAM)
 *
 *  Description:
 *
 *      This function writes NETOS parameters to ROM.
 *
 *  Parameters:
 *
 *      pFJ_NETOSPARAM      pointer to structure with parameters
 *
 *
 */

VOID fj_WriteNETOSRomParams( VOID *pFJ_NETOSPARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue )
{
	LPFJ_BRAM pBRAM = fj_CVT.pBRAM;
	ROMQUEUE q;
	int iRet;

	// call old routine, now modified, to compute checksum and move to RAM save area
	iRet = NAWriteDevBoardParams( (devBoardParamsType *)pFJ_NETOSPARAM );
	memcpy(&(pBRAM->nvParamsBRAM), pFJ_NETOSPARAM, sizeof(devBoardParamsType));

	// if pointer is not equal to the shared copy of the ROM data,
	if ( pFJ_NETOSPARAM != fj_CVT.pNETOSparam )
	{
		// copy new data to shared copy
		memcpy( fj_CVT.pNETOSparam, pFJ_NETOSPARAM, sizeof(FJ_NETOSPARAM) );
	}

	q.lRomEntry      = ROM_PARMNETOS;
	q.pBuffer        = (LPBYTE)pFJ_NETOSPARAM;
	q.lBufferSize    = sizeof(FJ_NETOSPARAM);
	q.bFreeBuffer    = FALSE;
	q.bFreeLock      = bFreeLock;
	q.pCallBack      = pCallBack;
	q.lCallBackValue = lCallBackValue;
	tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );

	return;
}
//
// convert 8 byte NETsilicon serial number to FJ string.
// strip leading zeros
//
VOID fj_NETtoFJSerial( LPCHAR pNET, LPSTR pFJ )
{
	int  i,j;
	BOOL bAll = FALSE;
	j=0;
	for ( i = 0; i < 8; i++ )
	{
		if ( (TRUE == bAll) || ('0' != pNET[i]) )
		{
			pFJ[j] = pNET[i];
			j++;
			bAll = TRUE;
		}
	}
	pFJ[j] = 0;
}
//
// convert FJ string to 8 byte NETsilicon serial number
// add leading zeros
//
VOID fj_FJtoNETSerial( LPSTR pFJ, LPCHAR pNET )
{
	int  i,j,l;

	l = 8 - strlen( pFJ );
	j=0;
	for ( i = 0; i < 8; i++ )
	{
		if ( i < l )
		{
			pNET[i] = '0';
		}
		else
		{
			pNET[i] = pFJ[j];
			j++;
		}
	}
}
VOID fj_threadRomStart(ULONG in)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	ROMQUEUE    q;
	LPCROMENTRY pRomEntry;
	//int  i;
	LONG lWritten;
	CHAR str[88];
	int  iRet;

	while( TRUE )
	{
		tx_queue_receive( &(pGlobal->qROM), &q, TX_WAIT_FOREVER );

		iRet = -1;
		pRomEntry = *(fj_CVT.pfj_RomTable) + q.lRomEntry;
		if ( NULL != pRomEntry )
		{
			if ( NULL != q.pBuffer )
			{
				if ( (int)pRomEntry < 100 )
				{
					pRomEntry = *(fj_CVT.pfj_RomTable) + (int)pRomEntry;
				}

				if ( NULL != pRomEntry )
				{
					strcpy( str, "ROM write started");
					if ( 0 != fj_CVT.pFJglobal->semaROMLock.tx_semaphore_count )
					{
						strcat( str, " - semaphore not locked\n" );
					}
					else
					{
						strcat( str, " - semaphore locked\n" );
					}
					if ( TRUE == pfph->bInfoROM )
					{
						fj_writeInfo(str);
					}
					lWritten = fj_writeROM( pRomEntry, q.pBuffer, q.lBufferSize );

										// flash written successfully??
					if ( lWritten < q.lBufferSize )
					{
						if ( TRUE == pfph->bInfoROM )
						{
							sprintf(str,"ROM not written correctly - %i bytes out of %i\n",(int)lWritten,(int)q.lBufferSize);
							fj_writeInfo(str);
						}
					}
					else
					{
						if ( TRUE == pfph->bInfoROM )
						{
							sprintf(str,"ROM written correctly - %i bytes\n",(int)q.lBufferSize);
							fj_writeInfo(str);
						}
						iRet = 0;
					}
				}
			}
		}
		if ( NULL != q.pCallBack )
		{
			q.pCallBack( q.lCallBackValue, iRet );
		}
		if ( 0 == iRet )
		{
										// if reboot needed,
			if( TRUE == pRomEntry->bReboot )
			{
				pGlobal->bReset = TRUE;	// stop the watchdog code to let the chip reset
				if ( TRUE == pfph->bInfoROM )
				{
					fj_writeInfo( "Chip Reset\n" );
				}
				fj_print_head_SetStatus();
				fj_print_head_SendStatus();

										// give final messages time to display
				tx_thread_sleep( 1*BSP_TICKS_PER_SECOND );
										// shortest watchdog timer interval
				(*NCC_GEN).scr.bits.swt = 0;
				while ( TRUE )			// blink LED until reset occurs
				{
					NALedBlinkRed(100,1);
				}
			}
			else
			{
				if ( 0 == strcmp(pRomEntry->strFileName,"ga.bin" ))
					pGlobal->bGAUpgrade = TRUE;
			}
			fj_print_head_SetStatus();
			fj_print_head_SendStatus();

										// if executable,
			if( TRUE == pRomEntry->bExecutable )
			{
				// declare address as a function pointer and link to it
				((void (*)())(fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset))();
			}
		}
		if ( TRUE == q.bFreeBuffer )
		{
			fj_free( q.pBuffer );
		}
		if ( TRUE == q.bFreeLock )
		{
			if ( 0 == pGlobal->semaROMLock.tx_semaphore_count )
				tx_semaphore_put( &(pGlobal->semaROMLock) );
		}
	}
	return;
}
//
// Create rom queue.
// Start rom thread.
//
VOID fj_romInit( VOID )
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	UINT  iRet;
	LONG  lSize;
	VOID  *pBuffer;
	LPBYTE pStack;

	lSize = 8 * sizeof(ROMQUEUE);
	pBuffer = fj_memory_getOnce( lSize );
	iRet = tx_queue_create( &(fj_CVT.pFJglobal->qROM), "ROM_QUEUE", TX_8_ULONG, pBuffer, lSize );

	pStack = fj_memory_getStack(FJ_THREAD_ROM_STACK_SIZE);
	// create the thread to get/send external time.
										// control block for ROM thread
	iRet = tx_thread_create (&(pGlobal->thrROM),
		"ROM Thread",					// thread name
		fj_threadRomStart,				// entry function
		0,								// parameter
		pStack,							// start of stack
		FJ_THREAD_ROM_STACK_SIZE,		// size of stack
		FJ_THREAD_ROM_PRIORITY,			// priority
		FJ_THREAD_ROM_PRIORITY,			// preemption threshold
		1,								// time slice threshold
		TX_AUTO_START);					// start immediately

	if (iRet != TX_SUCCESS)				// if couldn't create thread
	{
		netosFatalError ("Unable to create ROM thread", 1, 1);
	}

	return;
}
