#ifndef FJ_PRINTHEAD
#define FJ_PRINTHEAD

#include "fj_base.h"
#include "fj_defines.h"
#include <time.h>

// forward declarations
struct fjmemstor;
struct fjfont;
struct fjimage;
struct fjelement;
struct fjmessage;
struct fjlabel;
struct fjsystem;

// structure for Print Head Operating characteristics
typedef struct fjprinthead_op
{
										// human readable print head name
	CHAR  strName[1+FJPH_TYPE_NAME_SIZE];
	ULONG lPhyTable;					// index into table of physical characteristics
	ULONG lPkgTable;					// index into table of package dimensions
	ULONG lVoltageTable;				// voltage lookup table index
	ULONG lVoltage;						// voltage setting, if no table for resister lookup
	BOOL  bForceHeadPresent;			// TRUE = force HeadPresent. FALSE = test voltage resistor reading to sense HeadPresent.
	FLOAT fCurve1;						// voltage curve - on - microseconds
	FLOAT fCurve2;						// voltage curve - off - microseconds
	FLOAT fCurve3;						// voltage curve - on - microseconds
	BOOL  bSubpulseOn;					// subpulse switch
	FLOAT fSubpulseLength;				// subpulse voltage length - microseconds
	ULONG lSubpulseFreq;				// subpulse frequency
	ULONG lSubpulseOn;					// subpulse start after last print - microseconds
	ULONG lSubpulseOff;					// subpulse stop before next print - microseconds
	ULONG lSubpulseDur;					// subpulse duration - seconds
	ULONG lSubpulseInt;					// subpulse interval - seconds
	BOOL  bHeadInvert;					// FALSE = channel 1 on bottom. TRUE = channel 1 on top
	BOOL  bChipInvert;					// FALSE = channels passed through. TRUE = channels inverted
	LONG  lTempTable;					// tempearture lookup table index
	LONG  lTempRange;					// temperature +/- range for print head
	LONG  lTemp1;						// temperature for 1st heater/sensor; usually print head
	LONG  lTemp2;						// temperature for 2nd heater/sensor; usually hose for wax
	LONG  lTemp3;						// temperature for 3rd heater/sensor; usually tank for wax
	LONG  lCal1;						// calibration adjustment for 1st heater/sensor; usually print head
	LONG  lCal2;						// calibration adjustment for 2nd heater/sensor; usually hose for wax
	LONG  lCal3;						// calibration adjustment for 3rd heater/sensor; usually tank for wax
	BOOL  bAMS;							// FALSE = no AMS. TRUE = AMS present
	LONG  lAMSA1;						// AMS A1 - number of sets of prime pulses
	LONG  lAMSA2;						// AMS A2 - vacuum pump lead time
	LONG  lAMSA3;						// AMS A3 - width of one prime pump pulse
	LONG  lAMSA4;						// AMS A4 - delay between sets of prime pulses
	LONG  lAMSA5;						// AMS A5 - vacuum pump end time
	LONG  lAMSA6;						// AMS A6 - number of prime pulses in one set
	LONG  lAMSA7;						// AMS A7 - delay between prime pulses within one set
	LONG  lAutoPrint;					// Auto print interval (inches) for internal photocell // Burl v 1.1015
} FJPRINTHEAD_OP, *PFJPRINTHEAD_OP, FAR *LPFJPRINTHEAD_OP;

// structure for Print Head Physical characteristics
typedef struct fjprinthead_phy
{
	LONG  lChannels;					// number of addressable printer channels/nozzles/piezos
	LONG  lOrifices;					// number of orifices per channel
	LONG  lInkDrop;						// number of picoliters per channel
	FLOAT fDotHeight;					// height of one print dot
	FLOAT fDotWidth;					// width of one print dot
	FLOAT fAngle;						// angle head is manufactured for
	FLOAT fSpanCenter;					// center-to-center span of orifices
	FLOAT fSpanOuter;					// outer span of orifices
	FLOAT fChChStraight;				// channnel to channel distance straight along head
	FLOAT fChChAngle;					// channnel to channel distance at design angle
	FLOAT fOffsetEvenOdd;				// offset for even/odd channel numbers
	ULONG lUnits;						// measurement system for print head phy values
} FJPRINTHEAD_PHY, *PFJPRINTHEAD_PHY, FAR *LPFJPRINTHEAD_PHY;

// structure for Print Head Housing characteristics
typedef struct fjprinthead_package
{
	FLOAT fLeft;						// left side from nozzle 0
	FLOAT fRight;						// right side from nozzle 0
	FLOAT fTop;							// top from nozzle 0
	FLOAT fBottom;						// bottom from nozzle 0
	ULONG lUnits;						// measurement system for print head pkg values
} FJPRINTHEAD_PKG, *PFJPRINTHEAD_PKG, FAR *LPFJPRINTHEAD_PKG;

#define FJPH_UNITS_ENGLISH_INCHES     2
#define FJPH_UNITS_METRIC_MM          5
#define FJPH_DIRECTION_LEFT_TO_RIGHT  1
#define FJPH_DIRECTION_RIGHT_TO_LEFT -1

// structure for chain of images to print
typedef struct fjprintchain
{
	LONG    lRasterRow;					// row/nozzle to put image in
	LONG    lRasterWait;				// rasters to wait before printing
	LONG    lRasterCount;				// rasters left to print
	LONG    lMessageCounter;			// changed for each message - used for data light
	struct fjimage      FAR * pfi;		// image to print
	struct fjprintchain FAR * pNext;	// next in chain to be printed
} FJPRINTCHAIN, *PFJPRINTCHAIN, FAR *LPFJPRINTCHAIN;

typedef struct fjprinthead
{
	LPFJPRINTHEAD_PKG pfpkg;			// pointer to FJPRINTHEAD_PKG structure
	LPFJPRINTHEAD_PHY pfphy;			// pointer to FJPRINTHEAD_PHY structure
	LPFJPRINTHEAD_OP  pfop;				// pointer to FJPRINTHEAD_OP structure
	LONG   lFJPhOpIndex;				// number of the operational print head descriptor that was selected
	LONG   lDirection;					// direction of package movement across print head
	BOOL   bBiDirectional;
	LONG   lBiDirectionalCount;
	LONG   lSlant;						// slant value for message processing
	FLOAT  fSlantAngle;					// installed angle of slant.
	FLOAT  fHeight0;					// height of nozzle 0 above conveyor
	FLOAT  fDistancePC;					// distance of nozzle 0 downstream from photocell
	FLOAT  fDistancePC1;				
	FLOAT  fDistancePC2;				
	FLOAT  fAMSInterval;				// time in hours between AMS cycles
	LONG   lAMSInactive;				// time in seconds for inactive period to start AMS cycle
	BOOL   bAPSenable;					// APS(AMS) enable/disable
	FLOAT  fStandByInactive;			// time in hours of inactivity to wait before activating StandBy mode
	BOOL   bStandByEnable;				// auto StandBy mode enable/disable
	BOOL   bPhotocellBack;				// TRUE = trigger from back Photocell connector; FALSE = trigger from Photocell connector at print head
	BOOL   bPhotocellInternal;			// TRUE = generate internal photocell
	LONG   lTransformEnv;				// 'environment' transforms to use
	BOOL   bHeadSelected;				// a print head has been selected from menu
	BOOL   bEdit;						// true if edit in progress
	LONG   lEditID;						// id (IP address) of edit
	struct tm tmEditVersion;			// time of last edit
	struct tm tmDateManu;				// date manufactured
	struct tm tmDateInservice;			// date in service
	LONG   lGroupNumber;				// number of this printhead in the group
	LONG   lSerial;						// serial number
										// long name
	CHAR   strNameLong [1+FJPH_NAME_SIZE_LONG];
										// short name - for ID
	CHAR   strNameShort[1+FJPH_NAME_SIZE_SHORT];
										// serial number
	CHAR   strSerial   [1+FJPH_SERIAL_SIZE];
	CHAR   strID       [1+FJPH_ID_SIZE];// print head ID - short name + serial number
	LONG   lIPaddr;						// IP address currently in use
	LONG   lDynimageCount;				// counter/ID for Dynimage
										// scanner default Label ID
	CHAR   strScannerDefaultLabelID[1+FJLABEL_NAME_SIZE];
	BOOL   bSerialScannerLabelUseOnce;			// if true, use the scanner input once, then use default label
	BOOL   bSerialScannerLabel;				// if true, serial port has a scanner to select the print label
	BOOL   bSerialInfo;					// if true, serial port gets debug info
	BOOL   bSerialVarData;					// if true, serial port gets variable data
	BOOL   bSerialDynMem;
	LONG   lSerialVarDataID;				// variable data field ID
	BOOL   bInfoStatus;					// if true, write status info to serial port
	BOOL   bInfoSocket;					// if true, write Socket Connection info to serial port
	BOOL   bInfoRAM;					// if true, write RAM info to serial port
	BOOL   bInfoROM;					// if true, write ROM info to serial port
	BOOL   bInfoName;					// if true, write Name Registration (DNS/NETBIOS) info to serial port
	BOOL   bInfoTime;					// if true, write Time Server info to serial port
	BOOL   bInfoAD;						// if true, write Analog-to-Digital info to serial port
	BOOL   bSOP; 						// start of print notification
	LONG   lSOP; 						// start of print counter
	struct fjimage    FAR  *pfiDynimage;// pointer to dynamic image
	struct fjmemstor  FAR  *pMemStor;	// pointer to RAM MEMSTOR with messages and fonts
										// pointer to RAM MEMSTOR with messages and fonts - copy for editing
	struct fjmemstor  FAR  *pMemStorEdit;
	struct fjmessage  FAR  *pfmScanner;	// pointer to scanner default FJMESSAGE
	struct fjmessage  FAR  *pfmSelected;// pointer to selected FJMESSAGE template
										// pointer to selected FJMESSAGE template - old copy
	struct fjmessage  FAR  *pfmSelected2;
										// pointer to array of pointers to fonts used by selected message
	struct fjfont     FAR **papffSelected;
	LPFJPRINTCHAIN         pfcFirst;	// start of chain of Images to print
	LPFJPRINTCHAIN         pfcLast;		//   end of chain of Images to print
	struct fjsystem  FAR  *pfsys;		// pointer to owning FJSYSTEM structure
} FJPRINTHEAD, FAR *LPFJPRINTHEAD, FAR * const CLPFJPRINTHEAD;
typedef const struct fjprinthead FAR *LPCFJPRINTHEAD, FAR * const CLPCFJPRINTHEAD;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport LPFJPRINTHEAD fj_PrintHeadNew( VOID );
	FJDLLExport VOID          fj_PrintHeadDestroy( LPFJPRINTHEAD pfph );
	FJDLLExport VOID          fj_PrintHeadInit( LPFJPRINTHEAD pfph );
	FJDLLExport VOID          fj_PrintHeadReset( LPFJPRINTHEAD pfph );
	FJDLLExport LONG          fj_PrintHeadListAdd( LPFJPRINTHEAD **ppapfphIn, LONG lCount, CLPCFJPRINTHEAD pfph );
	FJDLLExport VOID          fj_PrintHeadListDestroyAll( LPFJPRINTHEAD **ppapfphIn, LONG lCount );
	FJDLLExport VOID          fj_PrintHeadSetID( LPFJPRINTHEAD pfph );
	FJDLLExport BOOL          fj_PrintHeadSetSerial( LPFJPRINTHEAD pfph, LPCSTR pInput );
	FJDLLExport BOOL          fj_PrintHeadSetNameShort( LPFJPRINTHEAD pfph, LPCSTR pInput );
	FJDLLExport BOOL          fj_PrintHeadSetNameLong( LPFJPRINTHEAD pfph, LPCSTR pInput );
	FJDLLExport VOID          fj_PrintHeadIDToString( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport VOID          fj_PrintHeadToString( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport VOID          fj_PrintHeadPackageToString( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport VOID          fj_PrintHeadPhysicalToString( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport VOID          fj_PrintHeadOperationalToString( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport BOOL          fj_PrintHeadFromString( LPFJPRINTHEAD pfph, LPSTR pString );
	FJDLLExport FLOAT         fj_PrintHeadGetElementWidth( CLPCFJPRINTHEAD pfph, const struct fjelement FAR *pfe );
	FJDLLExport BOOL          fj_PrintHeadIsEdit( CLPCFJPRINTHEAD pfph, LONG lEditID );
	FJDLLExport BOOL          fj_PrintHeadStartEdit( LPFJPRINTHEAD pfph, LONG lEditID );
	FJDLLExport BOOL          fj_PrintHeadSaveEdit( LPFJPRINTHEAD pfph, LONG lEditID, struct tm *pTM );
	FJDLLExport BOOL          fj_PrintHeadCancelEdit( LPFJPRINTHEAD pfph, LONG lEditID );
	FJDLLExport VOID          fj_PrintHeadGetEditStatus( CLPCFJPRINTHEAD pfph, LPSTR pStr );
	FJDLLExport struct fjmessage FAR * fj_PrintHeadBuildSelectedMessage( LPFJPRINTHEAD pfph, struct fjlabel FAR *pfl, LPCSTR pName );
	FJDLLExport struct fjmessage FAR * fj_PrintHeadBuildScannerDefaultMessage( LPFJPRINTHEAD pfph, struct fjlabel FAR *pfl, LPCSTR pName );
	FJDLLExport struct fjfont    FAR * fj_PrintHeadBuildFont( LPFJPRINTHEAD pfph, LPCSTR pFontName );
	FJDLLExport struct fjfont    FAR * fj_PrintHeadGetDefaultFont( LPFJPRINTHEAD pfph );
	FJDLLExport VOID          fj_PrintHeadLoadTestFonts( LPFJPRINTHEAD pfph );
	FJDLLExport FLOAT         fj_PrintHeadGetInkLowVolume (CLPCFJPRINTHEAD pfph );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_PRINTHEAD
