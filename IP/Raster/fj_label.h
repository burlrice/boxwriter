#ifndef FJ_LABEL
#define FJ_LABEL

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjmemstor;

enum FJLABELEXP
{
	FJ_LABEL_EXP_DAYS = 0x01,
	FJ_LABEL_EXP_WEEKS,
	FJ_LABEL_EXP_MONTHS,
	FJ_LABEL_EXP_YEARS
};
typedef struct fjlabel
{
	FLOAT   fBoxWidth;					// width of box
	FLOAT   fBoxHeight;					// height of box
	LONG    lExpValue;					// Datetime - expiration value
	enum FJLABELEXP lExpUnits;			// Datetime - expiration units
	BOOL    bExpRoundUp;				// Datetime - expiration should be round up to top of unit
	BOOL    bExpRoundDown;				
										
	STR     strName[1+FJLABEL_NAME_SIZE];
	// message names for each print head in group
	STR     fmMessageIDs[FJSYS_NUMBER_PRINTERS][1+FJMESSAGE_NAME_SIZE];
} FJLABEL, FAR *LPFJLABEL, FAR * const CLPFJLABEL;
typedef const struct fjlabel FAR *LPCFJLABEL, FAR * const CLPCFJLABEL;

#ifdef  __cplusplus
extern "C"
{
#endif
	// globals
	FJDLLExport LPFJLABEL fj_LabelNew( VOID );
	FJDLLExport LPFJLABEL fj_LabelDup( CLPCFJLABEL pflIn );
	FJDLLExport VOID      fj_LabelDestroy( LPFJLABEL pfl );
	FJDLLExport VOID      fj_LabelToStr( CLPCFJLABEL pfl, LPSTR pStr );
	FJDLLExport LPFJLABEL fj_LabelFromStr( LPCSTR pStr );
	FJDLLExport LPFJLABEL fj_LabelBuildFromString( const struct fjmemstor FAR * const pMemStor, LPCSTR pLabel );
	FJDLLExport LPFJLABEL fj_LabelBuildFromName( const struct fjmemstor FAR * const pMemStor, LPCSTR pName );
	FJDLLExport BOOL      fj_LabelAddToMemstor( struct fjmemstor FAR * pMemStor, CLPCFJLABEL pfl );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_LABEL
