#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#endif
#include <stdio.h>
#include <time.h>

#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_bitmap.h"
#include "fj_dynimage.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"
#include "fj_socket.h"
#include "fj_element.h"					// Burl v 1.1014
#include "fj_counter.h"					// Burl v 1.1014

extern const CHAR fj_FontID[];
extern const CHAR fj_TextID[];
extern const CHAR fj_DynTextID[];
extern const CHAR fj_BarCodeID[];
extern const CHAR fj_DynBarCodeID[];
extern const CHAR fj_BitmapID[];
extern const CHAR fj_CounterID[];
extern const CHAR fj_DatetimeID[];
extern const CHAR fj_DynimageID[];
extern const CHAR fj_MessageID[];
extern const CHAR fj_LabelID[];
extern const CHAR fj_bmpID[];
extern const CHAR fj_DataMatrixID[];

extern ROMENTRY        fj_RomTable[];

void fj_ftp_ROM_callback( LONG l1, LONG l2 );

static int IsBinaryCmd (LONG lCmd) // burl 1.2026
{
    switch (lCmd) {
	case IPC_PUT_BITMAP:
	case IPC_PUT_FONT:
	    return 1;
    }

    return 0;
}

// NOTE: this routine to process socket commands is used in both
//       the print head chip and the external editor.
//       the first parameter is different in the two systems.
//       the socketSend routines must be implemented in both systems.

//
// process a command - received or requested
//
// NOTE: pVoid is different for different systems.
//
FJDLLExport VOID fj_socketCmd( LPFJPRINTHEAD pfph, LPVOID pVoid, ULONG lIP, _FJ_SOCKET_MESSAGE *psmsgIn, _FJ_SOCKET_MESSAGE *psmsgOut, VOID((*socketSend)( LPVOID pVoid, LPBYTE pByte, LONG lLength)) )
{
	ROMQUEUE      q;
	_FJ_SOCKET_MESSAGE *psmsgBig;
	LPFJMEMSTOR pfms;
	LPSTR  *papList;
	LPSTR  *papListSave;
	ULONG  lCmd;
	ULONG  lBatchID;
	ULONG  lDataLength;
	LPBYTE pData;
	BOOL   bIsEdit;
	BOOL   bInvalid;
	BOOL   bSendString;
	BOOL   bSendBinary;
	LPCSTR pID;
	LPCSTR pKeyword;
	LPSTR  pElement;
	LPSTR  apList[10];
	LPSTR  apListCmd[2];
	LPSTR  apListKeyVal[2];
	STR    strLabelName[FJLABEL_NAME_SIZE*2];
	CHAR   parm[88];
	CHAR   str[88];
	LONG   lRet;
	LONG   lParmLen;
	LONG   lParmCount;
	int    i;
	int    index;
	int    retrom;
	BOOL   bROMWait;
	CLPFJ_GLOBAL  pGlobal = pCVT->pFJglobal;
	LPFJ_PARAM    pParam  = pCVT->pFJparam;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJMESSAGE   pfm;
	BOOL   bRet;

	
	// psmsgIn might not be on a LONG boundary
	// move ULONG values from header.
	memcpy( &lCmd,        &(psmsgIn->Header.Cmd),     sizeof(ULONG) );
	memcpy( &lBatchID,    &(psmsgIn->Header.BatchID), sizeof(ULONG) );
	memcpy( &lDataLength, &(psmsgIn->Header.Length),  sizeof(ULONG) );
	pData       = (LPBYTE)&(psmsgIn->Data);
	psmsgOut->Header.BatchID = lBatchID;
	bInvalid    = FALSE;
	bSendString = FALSE;
	bSendBinary = FALSE;

	bIsEdit = fj_PrintHeadIsEdit( pfph, lIP );
	if ( TRUE == bIsEdit ) pfms = pfph->pMemStorEdit;
	else                   pfms = pfph->pMemStor;

	// do some common parsing of the beginning of any parameters.
	// it might not be needed.
	// but it will be done correctly in one location.
	//
	// copy first part of parameters into local buffer so that original string is not modified.
	// parse/find first 2 semi-colon delimited keywords.
	// parse find keyword=value of first keyword.

	if ( 0 != lDataLength )
	{
		strncpy( parm, (LPSTR)pData, sizeof(parm) );
		parm[sizeof(parm)-1] = 0;

		if (!IsBinaryCmd (lCmd)) // burl 1.2026
		    parm [lDataLength] = 0; // burl 1.2025
		
		lParmLen = strlen( parm ) - 1;
		if ( ';' == parm[lParmLen] ) parm[lParmLen] = 0;
		lParmCount = fj_ParseKeywordsFromStr( parm, ';', apListCmd, 2 );
		if ( lParmCount > 0 )
		{
			i = fj_ParseKeywordsFromStr( apListCmd[0], '=', apListKeyVal, 2 );
		}
	}

	switch ( lCmd )
	{
		// these cases, and others, that have no code are not implemented in the common section.
		// look in the socket control modules to find their imlementations.
		// some cases are imlemented in both this common code and the local socket control modules.
		// they are here for completeness - as a check that they were considered and not erroneosly ignored.
		case IPC_COMPLETE:
		case IPC_CONNECTION_CLOSE:
		case IPC_INVALID:
		case IPC_ECHO:
		case IPC_STATUS:
		case IPC_GET_STATUS:
		case IPC_PHOTO_TRIGGER:
			break;

		case IPC_EDIT_STATUS:
			pData[lDataLength] = 0;
			fj_PrintHeadFromString( pfph, (LPSTR)pData );
			break;

		case IPC_GET_EDIT_STATUS:
			psmsgOut->Header.Cmd = IPC_EDIT_STATUS;
			fj_PrintHeadGetEditStatus( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_EDIT_START:
		case IPC_EDIT_SAVE:
		case IPC_EDIT_CANCEL:
			break;

		case IPC_GET_LABEL_IDS:
			psmsgOut->Header.Cmd = IPC_SET_LABEL_IDS;
			pID = fj_LabelID;
			pKeyword = "LABELIDS";
			IDSCommon:
			papListSave = fj_MemStorFindAllElements( pfms, pID );
			papList = papListSave;
			i = 0;
			while ( NULL != *papList )
			{
				i++;
				papList++;
			}
			sprintf( (LPSTR)psmsgOut->Data, "COUNT=%d;%s=", i, pKeyword );
			papList = papListSave;
			i = 0;
			while ( NULL != *papList )
			{
				strncpy( str, *papList, sizeof(str) );
										// guarantee end for partial scan
				str[sizeof(str)-2] = '}';
				str[sizeof(str)-1] = 0;
				lRet = fj_ParseKeywordsFromStr( str, ',', apList, 2 );
				if ( 2 == lRet )
				{
					if ( 0 != i ) strcat( (LPSTR)psmsgOut->Data, "," );
					strcat( (LPSTR)psmsgOut->Data, apList[1] );
					i++;
					papList++;
				}
			}
			strcat( (LPSTR)psmsgOut->Data, ";" );
			bSendString = TRUE;
			break;

		case IPC_SET_LABEL_IDS:
			break;

		case IPC_GET_LABELS:
			psmsgOut->Header.Cmd = IPC_PUT_LABEL;
			pID = fj_LabelID;
			ElementsCommon:
			papList = fj_MemStorFindAllElements( pfms, pID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			// send a null label to signal end of list
			*(psmsgOut->Data) = 0;
			bSendString = TRUE;
			break;

		case IPC_GET_LABEL:
			psmsgOut->Header.Cmd = IPC_PUT_LABEL;
			pID = fj_LabelID;
			pKeyword = "LABELID";
			GetElementCommon:
			bInvalid = TRUE;
			if ( 0 == strcmp( apListKeyVal[0], pKeyword ) )
			{
				pElement = (LPSTR)fj_MemStorFindElement( pfms, pID, apListKeyVal[1] );
				if ( NULL != pElement )
				{
					strcpy( (LPSTR)psmsgOut->Data, pElement );
					bSendString = TRUE;
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_PUT_LABEL:
		case IPC_PUT_MSG:
		case IPC_PUT_BARCODE:
		case IPC_PUT_BITMAP:
		case IPC_PUT_COUNTER:
		case IPC_PUT_DATETIME:
		case IPC_PUT_DYNIMAGE:
		case IPC_PUT_TEXT:
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					pData[lDataLength] = 0;
					fj_MemStorAddElementString( pfph->pMemStorEdit, (LPSTR)pData );
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_DELETE_LABEL:
			if ( TRUE == bIsEdit )
			{
				pKeyword = "LABELID";
				if ( 0 == strcmp( apListKeyVal[0], pKeyword ) )
				{
					pID = fj_LabelID;
					fj_MemStorDeleteElement( pfms, pID, apListKeyVal[1] );
					bRet = fj_MessageDeleteFromMemstorByName( pfms, apListKeyVal[1] );
				}
				else
				{
					bInvalid = TRUE;
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_DELETE_ALL_LABELS:
			if ( TRUE == bIsEdit )
			{
				pID = fj_LabelID;
				papListSave = fj_MemStorFindAllElements( pfms, pID );
				do
				{
					psmsgOut->Data[0] = 0;
					papList = papListSave;
					i = 0;
					while ( NULL != *papList )
					{
						strncpy( str, *papList, sizeof(str) );
										// guarantee end for partial scan
						str[sizeof(str)-2] = '}';
						str[sizeof(str)-1] = 0;
						lRet = fj_ParseKeywordsFromStr( str, ',', apList, 2 );
						if ( 2 == lRet )
						{
							if ( 0 != i ) strcat( (LPSTR)psmsgOut->Data, "," );
							strcat( (LPSTR)psmsgOut->Data, apList[1] );
							i++;
							papList++;
						}
					}
					lRet = fj_ParseKeywordsFromStr( (LPSTR)psmsgOut->Data, ',', apList, 10 );
					for ( i = 0; i < lRet; i ++)
					{
						fj_MemStorDeleteElement( pfms, pID, apList[i] );
						bRet = fj_MessageDeleteFromMemstorByName( pfms, apList[i] );
					}
					papListSave = fj_MemStorFindAllElements( pfms, pID );
				} while ( NULL != *papListSave );
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_LABEL_ID_SELECTED:
			psmsgOut->Header.Cmd = IPC_SET_LABEL_ID_SELECTED;
			strcpy( (LPSTR)psmsgOut->Data, "LABELID=" );
			if ( NULL != pfph->pfsys->pfl )
			{
				// label Name
				fj_processParameterStringOutput( strLabelName, pfph->pfsys->pfl->strName );
				strcat( (LPSTR)psmsgOut->Data, strLabelName );
			}
			strcat( (LPSTR)psmsgOut->Data, ";" );
			bSendString = TRUE;
			break;

		case IPC_SET_LABEL_ID_SELECTED:
		case IPC_SELECT_LABEL:
			pData[lDataLength] = 0;
			str[0] = 0;					// select no label if parameter is bad
			if ( 0 == strcmp( apListKeyVal[0], "LABELID" ) )
			{
				if ( '"' == apListKeyVal[1][0] )
				{
					strcpy( strLabelName, apListKeyVal[1] );
					fj_processParameterStringInput( apListKeyVal[1] );
				}
				else
				{
					// label Name
					fj_processParameterStringOutput( strLabelName, apListKeyVal[1] );
				}

				if ( NULL == fj_MemStorFindElement( pfms, fj_LabelID, strLabelName ) )
				{
										// select no label if label does not exist
					apListKeyVal[1][0] = 0;
				}
			}
			else
			{
				bInvalid = TRUE;
			}
			if (0 != strcmp(pCVT->pBRAM->strLabelName, apListKeyVal[1]))
			{
				pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );

				if (pfph->bBiDirectional)
					pfph->lBiDirectionalCount = 0;
			}
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			fj_SystemBuildSelectedLabel( pfph->pfsys, apListKeyVal[1] );
			// Burl v 1.1015 // pBRAM->lSelectedCount = 0;
			break;

		case IPC_GET_MSG_IDS:
			psmsgOut->Header.Cmd = IPC_SET_MSG_IDS;
			pID = fj_MessageID;
			pKeyword = "MESSAGEIDS";
			goto IDSCommon;
			break;

		case IPC_SET_MSG_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_MSGS:
			psmsgOut->Header.Cmd = IPC_PUT_MSG;
			pID = fj_MessageID;
			goto ElementsCommon;
			break;

		case IPC_GET_MSG:
			psmsgOut->Header.Cmd = IPC_PUT_MSG;
			pID = fj_MessageID;
			pKeyword = "MESSAGEID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_MSG:
			if ( TRUE == bIsEdit )
			{
				pKeyword = "MESSAGEID";
				if ( 0 == strcmp( apListKeyVal[0], pKeyword ) )
				{
					pfm = pCVT->fj_MessageBuildFromName( pfms, apListKeyVal[1] );
					if ( NULL != pfm )
					{
						bRet = pCVT->fj_MessageDeleteFromMemstor( pfms, pfm );
						pCVT->fj_MessageDestroy( pfm );
					}
				}
				else
				{
					bInvalid = TRUE;
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_TEXT_IDS:
			psmsgOut->Header.Cmd = IPC_SET_TEXT_IDS;
			pID = fj_TextID;
			pKeyword = "TEXTIDS";
			goto IDSCommon;
			break;

		case IPC_SET_TEXT_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_TEXT_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_TEXT;
			pID = fj_TextID;
			goto ElementsCommon;
			break;

		case IPC_GET_TEXT:
			psmsgOut->Header.Cmd = IPC_PUT_TEXT;
			pID = fj_TextID;
			pKeyword = "TEXTID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_TEXT:
			pID = fj_TextID;
			pKeyword = "TEXTID";
			DeleteElementCommon:
			if ( TRUE == bIsEdit )
			{
				if ( 0 == strcmp( apListKeyVal[0], pKeyword ) )
				{
					fj_MemStorDeleteElement( pfms, pID, apListKeyVal[1] );
				}
				else
				{
					bInvalid = TRUE;
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_BITMAP_IDS:
			psmsgOut->Header.Cmd = IPC_SET_BITMAP_IDS;
			pID = fj_BitmapID;
			pKeyword = "BITMAPIDS";
			goto IDSCommon;
			break;

		case IPC_SET_BITMAP_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_BITMAP_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_BITMAP;
			pID = fj_BitmapID;
			goto ElementsCommon;
			break;

		case IPC_GET_BITMAP:
			psmsgOut->Header.Cmd = IPC_PUT_BITMAP;
			pID = fj_BitmapID;
			pKeyword = "BITMAPID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_BITMAP:
			pID = fj_BitmapID;
			pKeyword = "BITMAPID";
			goto DeleteElementCommon;
			break;

		case IPC_GET_DYNIMAGE_IDS:
			psmsgOut->Header.Cmd = IPC_SET_DYNIMAGE_IDS;
			pID = fj_DynimageID;
			pKeyword = "DYNIMAGEIDS";
			goto IDSCommon;
			break;

		case IPC_SET_DYNIMAGE_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_DYNIMAGE_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_DYNIMAGE;
			pID = fj_DynimageID;
			goto ElementsCommon;
			break;

		case IPC_GET_DYNIMAGE:
			psmsgOut->Header.Cmd = IPC_PUT_DYNIMAGE;
			pID = fj_DynimageID;
			pKeyword = "DYNIMAGEID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_DYNIMAGE:
			pID = fj_DynimageID;
			pKeyword = "DYNIMAGEID";
			goto DeleteElementCommon;

		case IPC_PUT_DYNIMAGE_DATA:
			fj_DynimagePutData( pfph, pData );
			break;

		case IPC_GET_DATETIME_IDS:
			psmsgOut->Header.Cmd = IPC_SET_DATETIME_IDS;
			pID = fj_DatetimeID;
			pKeyword = "DATETIMEIDS";
			goto IDSCommon;
			break;

		case IPC_SET_DATETIME_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_DATETIME_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_DATETIME;
			pID = fj_DatetimeID;
			goto ElementsCommon;
			break;

		case IPC_GET_DATETIME:
			psmsgOut->Header.Cmd = IPC_PUT_DATETIME;
			pID = fj_DatetimeID;
			pKeyword = "DATETIMEID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_DATETIME:
			pID = fj_DatetimeID;
			pKeyword = "DATETIMEID";
			goto DeleteElementCommon;

		case IPC_GET_COUNTER_IDS:
			psmsgOut->Header.Cmd = IPC_SET_COUNTER_IDS;
			pID = fj_CounterID;
			pKeyword = "COUNTERIDS";
			goto IDSCommon;
			break;

		case IPC_SET_COUNTER_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_COUNTER_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_COUNTER;
			pID = fj_CounterID;
			goto ElementsCommon;
			break;

		case IPC_GET_COUNTER:
			psmsgOut->Header.Cmd = IPC_PUT_COUNTER;
			pID = fj_CounterID;
			pKeyword = "COUNTERID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_COUNTER:
			pID = fj_CounterID;
			pKeyword = "COUNTERID";
			goto DeleteElementCommon;

		case IPC_GET_BARCODE_IDS:
			psmsgOut->Header.Cmd = IPC_SET_BARCODE_IDS;
			pID = fj_BarCodeID;
			pKeyword = "BARCODEIDS";
			goto IDSCommon;
			break;

		case IPC_SET_BARCODE_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_BARCODE_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_BARCODE;
			pID = fj_BarCodeID;
			goto ElementsCommon;
			break;

		case IPC_GET_BARCODE:
			psmsgOut->Header.Cmd = IPC_PUT_BARCODE;
			pID = fj_BarCodeID;
			pKeyword = "BARCODEID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_BARCODE:
			pID = fj_BarCodeID;
			pKeyword = "BARCODEID";
			goto DeleteElementCommon;
			break;

		case IPC_GET_FONT_IDS:
			psmsgOut->Header.Cmd = IPC_SET_FONT_IDS;
			pID = fj_FontID;
			pKeyword = "FONTIDS";
			goto IDSCommon;
			break;

		case IPC_SET_FONT_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_FONTS:
			psmsgOut->Header.Cmd = IPC_PUT_FONT;
			pID = fj_FontID;
			BinaryElementsCommon:
			papList = fj_MemStorFindAllElements( pfms, pID );
			while ( NULL != *papList )
			{
				pElement = *papList;
				memcpy( &lDataLength, (pElement-sizeof(LONG)), sizeof(LONG) );
										// memstor size includes the LONG size
				lDataLength -= sizeof(LONG);
				psmsgBig = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1);
				psmsgBig->Header.Cmd     = psmsgOut->Header.Cmd;
				psmsgBig->Header.BatchID = lBatchID;
				psmsgBig->Header.Length  = lDataLength;
				memcpy( psmsgBig->Data, pElement, lDataLength );
				socketSend( pVoid, (LPBYTE)psmsgBig, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				fj_free( psmsgBig );
				papList++;
			}
			// send a null font to signal end of list
			*(psmsgOut->Data) = 0;
			bSendString = TRUE;
			break;

		case IPC_GET_FONT:
			psmsgOut->Header.Cmd = IPC_PUT_FONT;
			pID = fj_FontID;
			pKeyword = "FONTID";
			GetBinaryElementCommon:
			bInvalid = TRUE;
			if ( 0 == strcmp( apListKeyVal[0], pKeyword ) )
			{
				pElement = (LPSTR)fj_MemStorFindElement( pfms, pID, apListKeyVal[1] );
				if ( NULL != pElement )
				{
					bSendBinary = TRUE;
					bInvalid = FALSE;
				}
			}
			break;

		case IPC_PUT_FONT:
			bInvalid = TRUE;
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					LPFJFONT pff;
					pff = fj_FontFromBuffer( pData );
					if ( NULL != pff )
					{
						fj_MemStorAddElementBinary( pfms, pData, lDataLength );
						bInvalid = FALSE;
						fj_FontDestroy( pff );

						// burl 1.2026
						bSendString = TRUE;
						strcpy (psmsgOut->Data, "IPC_PUT_FONT");
						psmsgOut->Header.Cmd = IPC_PUT_FONT;
						psmsgOut->Header.BatchID = lBatchID;
						psmsgOut->Header.Length = strlen (psmsgOut->Data);
						// burl 1.2026
					}
				}
			}
			break;

		case IPC_PUT_BMP:
			bInvalid = TRUE;
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					pElement = (LPSTR)pData;
					pElement += 1 + strlen( pElement );
					if ( TRUE == fj_bmpCheckFormat((LPBYTE)pElement) )
					{
						fj_MemStorAddElementBinary( pfms, pData, lDataLength );
						bInvalid = FALSE;
					}
				}
			}
			break;

		case IPC_DELETE_FONT:
			pID = fj_FontID;
			pKeyword = "FONTID";
			goto DeleteElementCommon;
			break;

		case IPC_GET_BMP_IDS:
			psmsgOut->Header.Cmd = IPC_SET_BMP_IDS;
			pID = fj_bmpID;
			pKeyword = "BMPIDS";
			goto IDSCommon;
			break;

		case IPC_SET_BMP_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_BMPS:
			psmsgOut->Header.Cmd = IPC_PUT_BMP;
			pID = fj_bmpID;
			goto BinaryElementsCommon;
			break;

		case IPC_GET_BMP:
			psmsgOut->Header.Cmd = IPC_PUT_BMP;
			pID = fj_bmpID;
			pKeyword = "BMPID";
			goto GetBinaryElementCommon;
			break;

		case IPC_DELETE_BMP:
			pID = fj_bmpID;
			pKeyword = "BMPID";
			goto DeleteElementCommon;
			break;

		case IPC_DELETE_ALL_BMPS:
			if ( TRUE == bIsEdit )
			{
				papListSave = fj_MemStorFindAllElements( pfms, fj_bmpID );
				do
				{
					psmsgOut->Data[0] = 0;
					papList = papListSave;
					i = 0;
					while ( NULL != *papList )
					{
						strncpy( str, *papList, sizeof(str) );
										// guarantee end for partial scan
						str[sizeof(str)-2] = '}';
						str[sizeof(str)-1] = 0;
						lRet = fj_ParseKeywordsFromStr( str, ',', apList, 2 );
						if ( 2 == lRet )
						{
							if ( 0 != i ) strcat( (LPSTR)psmsgOut->Data, "," );
							strcat( (LPSTR)psmsgOut->Data, apList[1] );
							i++;
							papList++;
						}
					}
					lRet = fj_ParseKeywordsFromStr( (LPSTR)psmsgOut->Data, ',', apList, 10 );
					for ( i = 0; i < lRet; i ++)
					{
						fj_MemStorDeleteElement( pfms, fj_bmpID, apList[i] );
					}
					papListSave = fj_MemStorFindAllElements( pfms, fj_bmpID );
				} while ( NULL != *papListSave );
			}
			else bInvalid = TRUE;

			break;

		case IPC_GET_DYNTEXT_IDS:
			psmsgOut->Header.Cmd = IPC_SET_DYNTEXT_IDS;
			pID = fj_TextID;
			pKeyword = "DYNTEXTIDS";
			goto IDSCommon;
			break;

		case IPC_SET_DYNTEXT_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_DYNTEXT_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_DYNTEXT;
			pID = fj_DynTextID;
			goto ElementsCommon;
			break;

		case IPC_GET_DYNTEXT:
			psmsgOut->Header.Cmd = IPC_PUT_DYNTEXT;
			pID = fj_DynTextID;
			pKeyword = "DYNTEXTID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_DYNTEXT:
			pID = fj_DynTextID;
			pKeyword = "DYNTEXTID";
			goto DeleteElementCommon;
			break;

		case IPC_GET_DYNBARCODE_IDS:
			psmsgOut->Header.Cmd = IPC_SET_DYNBARCODE_IDS;
			pID = fj_DynBarCodeID;
			pKeyword = "DYNBARCODEIDS";
			goto IDSCommon;
			break;

		case IPC_SET_DYNBARCODE_IDS:
			pData[lDataLength] = 0;
			break;

		case IPC_GET_DYNBARCODE_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_DYNBARCODE;
			pID = fj_DynBarCodeID;
			goto ElementsCommon;
			break;

		case IPC_GET_DYNBARCODE:
			psmsgOut->Header.Cmd = IPC_PUT_DYNBARCODE;
			pID = fj_DynBarCodeID;
			pKeyword = "DYNBARCODEID";
			goto GetElementCommon;
			break;

		case IPC_DELETE_DYNBARCODE:
			pID = fj_DynBarCodeID;
			pKeyword = "DYNBARCODEID";
			goto DeleteElementCommon;
			break;

		case IPC_GET_GROUP_INFO:
			psmsgOut->Header.Cmd = IPC_SET_GROUP_INFO;
			fj_SystemGroupToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_INTERNET_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemInternetToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_TIME_INFO:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemTimeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_DATE_STRINGS:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemDateStringsToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_PASSWORDS:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemPasswordToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_SYSTEM_BARCODES:
			psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
			fj_SystemBarCodesToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_SYSTEM_INFO:
			pData[lDataLength] = 0;
			fj_SystemFromString( pfph->pfsys, (LPSTR)pData );
			break;

		case IPC_GET_PRINTER_ID:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadIDToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_PKG_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadPackageToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_PHY_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadPhysicalToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_GET_PRINTER_OP_INFO:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
			fj_PrintHeadOperationalToString( pfph, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_PRINTER_INFO:
			pData[lDataLength] = 0;
			fj_PrintHeadFromString( pfph, (LPSTR)pData );
			fj_PrintHeadReset( pfph );
			break;

		case IPC_GET_PRINTER_ELEMENTS:
			psmsgOut->Header.Cmd = IPC_PUT_PRINTER_ELEMENT;
			papList = fj_MemStorFindAllElements( pfms, fj_FontID );
			while ( NULL != *papList )
			{
				pElement = *papList;
				memcpy( &lDataLength, (pElement-sizeof(LONG)), sizeof(LONG) );
				psmsgBig = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1);
				psmsgBig->Header.Cmd     = IPC_PUT_PRINTER_ELEMENT;
				psmsgBig->Header.BatchID = lBatchID;
				psmsgBig->Header.Length  = lDataLength;
				memcpy( psmsgBig->Data, pElement, lDataLength );
				socketSend( pVoid, (LPBYTE)psmsgBig, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				fj_free( psmsgBig );
				papList++;
			}

			papList = fj_MemStorFindAllElements( pfms, fj_bmpID );
			while ( NULL != *papList )
			{
				pElement = *papList;
				memcpy( &lDataLength, (pElement-sizeof(LONG)), sizeof(LONG) );
				psmsgBig = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1);
				psmsgBig->Header.Cmd     = IPC_PUT_PRINTER_ELEMENT;
				psmsgBig->Header.BatchID = lBatchID;
				psmsgBig->Header.Length  = lDataLength;
				memcpy( psmsgBig->Data, pElement, lDataLength );
				socketSend( pVoid, (LPBYTE)psmsgBig, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				fj_free( psmsgBig );
				papList++;
			}

			papList = fj_MemStorFindAllElements( pfms, fj_TextID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_BarCodeID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_BitmapID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_DynBarCodeID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_DynTextID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_DynimageID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_DatetimeID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_CounterID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_DataMatrixID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_MessageID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			papList = fj_MemStorFindAllElements( pfms, fj_LabelID );
			while ( NULL != *papList )
			{
				strcpy( (LPSTR)psmsgOut->Data, *papList );
				lDataLength = strlen( (LPSTR)psmsgOut->Data );
				psmsgOut->Header.Length = lDataLength;
				socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
				papList++;
			}
			// send out a null entry to signal end of elements
			*(psmsgOut->Data) = 0;
			bSendString = TRUE;
			break;

		case IPC_PUT_PRINTER_ELEMENT:
			if ( TRUE == bIsEdit )
			{
				LPSTR pElementTest;
				LPSTR pElementEnd;
				LPSTR pQuoteEnd;
				CHAR  c;
				pElement = (LPSTR)pData;
				i = 0;
				while ( (i < (LONG)lDataLength) && (FALSE == bInvalid) )
				{
					pElementEnd = NULL;
					pElementTest = pElement;
					while ( i < (LONG)lDataLength )
					{
						c = *pElementTest;
						if ( '"' == c )
						{
							pQuoteEnd = fj_FindEndQuotedString( pElementTest );
							if ( NULL != pQuoteEnd )
							{
								pElementTest = pQuoteEnd;
								i = pElementTest - (LPSTR)pData;
							}
						}
						else if ( '}' == c )
						{
							pElementEnd = pElementTest;
							break;
						}
						pElementTest++;
						i++;
					}

					//					pElementEnd = strchr( pElement, '}' );
					if ( NULL != pElementEnd )
					{
						strncpy( str, pElement, sizeof(str) );
						i = 1 + (pElementEnd - pElement);
						if ( i < sizeof(str) )
						{
							str[i] = 0;
						}
						else
						{
										// guarantee end for partial scan
							str[sizeof(str)-2] = '}';
							str[sizeof(str)-1] = 0;
						}
						fj_ParseKeywordsFromStr( str, ',', apList, 3 );
						if ( (0 == strcmp(fj_FontID, apList[0])) ||
							(0 == strcmp(fj_bmpID,  apList[0])) )
						{
							bInvalid = !fj_MemStorAddElementBinary( pfph->pMemStorEdit, (LPBYTE)pElement, lDataLength );
							// only one binary per command
							break;		// end the while statement
						}
						else if ( (0 == strcmp(fj_TextID,    apList[0])) ||
							(0 == strcmp(fj_DynTextID, apList[0])) ||
							(0 == strcmp(fj_DynBarCodeID, apList[0])) ||
							(0 == strcmp(fj_BarCodeID, apList[0])) ||
							(0 == strcmp(fj_BitmapID,  apList[0])) ||
							(0 == strcmp(fj_DynimageID,apList[0])) ||
							(0 == strcmp(fj_DatetimeID,apList[0])) ||
							(0 == strcmp(fj_CounterID, apList[0])) ||
							(0 == strcmp(fj_DataMatrixID, apList[0])) ||
							(0 == strcmp(fj_MessageID, apList[0])) ||
							(0 == strcmp(fj_LabelID,   apList[0])) )
						{
							pElementEnd++;
							*pElementEnd = 0;
							bInvalid = !fj_MemStorAddElementString( pfph->pMemStorEdit,  (LPSTR)pElement );
							pElement = pElementEnd;
							i = pElement - (LPSTR)pData;
							if ( i < (LONG)lDataLength )
								*pElement = '{';
						}
						else bInvalid = TRUE;
					}
					else
					{
						break;			// end the while statement
					}
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_DELETE_ALL_PRINTER_ELEMENTS:
			if ( TRUE == bIsEdit )
			{
				fj_MemStorClear( pfph->pMemStorEdit );
			}
			else bInvalid = TRUE;
			break;

		case IPC_FIRMWARE_UPGRADE:
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					// set lock to write ROM
					tx_semaphore_get( &(fj_CVT.pFJglobal->semaROMLock), TX_WAIT_FOREVER );
					q.lRomEntry   = ROM_RAMBIN;
					q.pBuffer     = pData;
					q.lBufferSize = lDataLength;
					q.bFreeBuffer = FALSE;
					q.bFreeLock   = TRUE;
					q.pCallBack   = fj_ftp_ROM_callback;
					tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );
					strcpy( (LPSTR)psmsgOut->Data, "ROM write strarted");
					bSendString = FALSE;
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_SET_PH_TYPE:
			pData[lDataLength] = 0;
			index = atoi(pData);
			if ( index < pCVT->lFJPhOpCount )
			{
				// get ROM structure
				pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
				retrom = pCVT->fj_ReadFJRomParams( pParam );
				if ( 0 == retrom  )
				{
					pfph->lFJPhOpIndex = index;
					pParam->FJPh.lFJPhOpIndex = index;
					pfph->bHeadSelected = TRUE;
					pParam->FJPh.bHeadSelected = TRUE;
					memcpy( &(pParam->FJPhOp ), &(*(pCVT->paFJPhOp ))[index], sizeof(FJPRINTHEAD_OP ) );
					index = pParam->FJPhOp.lPhyTable;
					memcpy( &(pParam->FJPhPhy), &(*(pCVT->paFJPhPhy))[index], sizeof(FJPRINTHEAD_PHY) );
					index = pParam->FJPhOp.lPkgTable;
					memcpy( &(pParam->FJPhPkg), &(*(pCVT->paFJPhPkg))[index], sizeof(FJPRINTHEAD_PKG) );
					// write and free ROM lock
					pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
					bROMWait = TRUE;
					retrom = 0;
					if ( 0 == retrom )
					{
						memcpy( pfph->pfop , &(pParam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
						memcpy( pfph->pfphy, &(pParam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
						memcpy( pfph->pfpkg, &(pParam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );
						pCVT->fj_PrintHeadReset( pfph );
						pCVT->fj_print_head_setPWMVoltageValue();
						pCVT->fj_print_head_KillENI();
						pCVT->fj_print_head_SetGAValues();
						pCVT->fj_print_head_InitENI();
						pCVT->fj_print_head_SetStatus();
						pCVT->fj_print_head_SendInfoAll();
					}
					else
					{
						pfph->bHeadSelected = FALSE;
						pParam->FJPh.bHeadSelected = FALSE;
					}
					pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
					pCVT->fj_SystemBuildSelectedLabel( pfsys, pBRAM->strLabelName );
				}
				else
				{
					pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_PRINTER_MODE:
			psmsgOut->Header.Cmd = IPC_SET_PRINTER_MODE;
			fj_PrintModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_PRINTER_MODE:
			pData[lDataLength] = 0;
			fj_PrintModeFromString( pfph->pfsys, (LPSTR)pData );
			break;

		case IPC_SET_SELECTED_COUNT:
			if ( 0 == strcmp(apListKeyVal[0],"CountSel"))
			{
				/* Burl - begin - v 1.1014 */
				LPFJMESSAGE pfm = NULL;
				LPFJELEMENT pfe = NULL;
				LPFJCOUNTER pfc = NULL;
				int iSeg;
				int iSetCount;
				/* Burl - end - v 1.1014 */

				index = atoi(apListKeyVal[1]);
				if ( 0 != pBRAM->strLabelName[0] )
				{
					pBRAM->lSelectedCount = index;
				}

				/* Burl - begin - v 1.1014 */
				pfm = pfph->pfmSelected;

				for (iSeg = 0; iSeg < pfm->lCount; iSeg++)
				{
					pfe = pfm->papfe [iSeg];

					if (pfe->pActions->fj_DescGetType () == FJ_TYPE_COUNTER)
					{
						pfc = (LPFJCOUNTER)pfe->pDesc;

						pfc->lOldValue    =  pfc->lStart;
						pfc->lCurValue    =  pfc->lStart;
						pfc->lCurRepeat   =  1;

						for (iSetCount = 0; iSetCount < pBRAM->lSelectedCount; iSetCount++)
							fj_CounterUpdate (pfc);
					}
				}
				/* Burl - end - v 1.1014 */
			}
			//break;
		case IPC_GET_SELECTED_COUNT:
			pCVT->fj_print_head_SendSelectedCNT();
			break;

		case IPC_SELECT_VARDATA_ID:
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{
				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)  pfph->lSerialVarDataID = index;
				else bInvalid = TRUE;
			} else bInvalid = TRUE;
			break;
		case IPC_GET_VARDATA_ID:
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{
				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)
				{
					psmsgOut->Header.Cmd = IPC_SET_VARDATA_ID;
					sprintf( (LPSTR)psmsgOut->Data, "ID=%d;DATA=\"%s\";",
						index+1,
						pBRAM->DynamicSegments[index].strDynData );
					bSendString = TRUE;
				} else bInvalid = TRUE;
			} else bInvalid = TRUE;
			break;
		case IPC_SET_VARDATA_ID:
			if ( 0 == strcmp(apListKeyVal[0],"ID"))
			{

				index = atoi(apListKeyVal[1]);
				index--;
				if ( index >= 0 && index < FJMESSAGE_DYNAMIC_SEGMENTS)
				{
				    memset (pBRAM->DynamicSegments[index].strDynData, 0, FJTEXT_SIZE); // 1.2029
				    
					i = fj_ParseKeywordsFromStr( apListCmd[1], '=', apListKeyVal, 2 );
					if ( 0 == strcmp(apListKeyVal[0],"DATA"))
					{
						lDataLength = strlen( apListKeyVal[1] );
						if ( lDataLength > FJTEXT_SIZE ) lDataLength = FJTEXT_SIZE;
										// empty str should be ""
						if ( 2 < lDataLength )
						{
							pData = apListKeyVal[1];
							*(pData + lDataLength - 1) = 0;
							strcpy( pBRAM->DynamicSegments[index].strDynData, (pData+1));
							fj_writeInfo ((pData+1)); fj_writeInfo (" " /* "\n\r" */);
							pBRAM->DynamicSegments[index].bChanged = TRUE;
						}
					}
					else bInvalid = TRUE;
				} else bInvalid = TRUE;
			} else bInvalid = TRUE;
			break;

		case IPC_SAVE_ALL_PARAMS:
			{
				LPFJ_PARAM pParam = fj_CVT.pFJparam;
				LPFJSYSTEM         pfsys;
				LPFJSYSTEM         pfsysNew;
				LPFJPRINTHEAD      pfph;
				LPFJPRINTHEAD_OP   pfop;
				LPFJPRINTHEAD_PHY  pfphy;
				LPFJPRINTHEAD_PKG  pfpkg;

				pfsys = fj_CVT.pfsys;
				pfph  = fj_CVT.pfph;
				pfop  = pfph->pfop;
				pfphy = pfph->pfphy;
				pfpkg = pfph->pfpkg;


				memcpy( &(fj_CVT.pFJparam->FJSys ), pfsys, sizeof(FJSYSTEM) );
				memcpy( &(fj_CVT.pFJparam->FJPhOp ), pfop, sizeof(FJPRINTHEAD_OP ) );
				memcpy( &(fj_CVT.pFJparam->FJPhPhy), pfphy, sizeof(FJPRINTHEAD_PHY) );
				memcpy( &(fj_CVT.pFJparam->FJPhPkg), pfpkg, sizeof(FJPRINTHEAD_PKG) );
				memcpy( &(fj_CVT.pFJparam->FJPh   ), pfph, sizeof(FJPRINTHEAD    ) );

				//if ( TRUE == bIsEdit )
				//{
				pfsysNew = &(fj_CVT.pFJparam->FJSys );
				pfsysNew->lActiveCount = 0;
				pCVT->tx_semaphore_get( &(pCVT->pFJglobal->semaROMLock), TX_WAIT_FOREVER );
				// write and free ROM lock
				pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
				pCVT->fj_PrintHeadReset( pfph );
				pCVT->fj_print_head_KillENI();
				pCVT->fj_print_head_SetGAEncoder();
				// set encoder switch
				pCVT->fj_print_head_SetGAWriteSwitches();
				pCVT->fj_print_head_InitENI();
				pCVT->fj_print_head_SetGAValues();
				pCVT->fj_print_head_setPWMVoltageValue();
				pCVT->fj_print_head_SetStatus();
				pCVT->fj_print_head_SendInfoAll();
				pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
				pCVT->fj_SystemBuildSelectedLabel( pCVT->pfsys, pCVT->pBRAM->strLabelName );
				pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
				//}
				//else bInvalid = TRUE;
			}
			break;

		case IPC_GET_SW_VER_INFO:
			psmsgOut->Header.Cmd = IPC_STATUS;
			sprintf((LPSTR)psmsgOut->Data, "SW_VER=%s;", pCVT->fj_getVersionNumber());
			bSendString = TRUE;
			break;

		case IPC_GET_GA_VER_INFO:
		{
			LONG GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
			BYTE GA_Mver = ((GAver>>4)& 0x0F);
			BYTE GA_Lver = (GAver & 0x0F);
			psmsgOut->Header.Cmd = IPC_STATUS;
			sprintf((LPSTR)psmsgOut->Data, "GA_VER=%d.%d;",GA_Mver,GA_Lver);
			bSendString = TRUE;
		}
		break;

		case IPC_GA_UPGRADE:
			if ( TRUE == bIsEdit )
			{
				if ( 0 != lDataLength)
				{
					// add length of file as last word of rom space
					BYTE *pByte = pData + (&(fj_RomTable[3]))->lMaxLength - 4;
					*((LONG *)(pByte- 4)) = (LONG)lDataLength;
					// set lock to write ROM
					tx_semaphore_get( &(fj_CVT.pFJglobal->semaROMLock), TX_WAIT_FOREVER );
					q.lRomEntry   = ROM_GABIN;
					q.pBuffer     = pData;
					q.lBufferSize = (&(fj_RomTable[3]))->lMaxLength;
					q.bFreeBuffer = FALSE;
					q.bFreeLock   = TRUE;
					q.pCallBack   = fj_ftp_ROM_callback;
					tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );
					strcpy( (LPSTR)psmsgOut->Data, "ROM write strarted");
					bSendString = FALSE;
				}
			}
			else bInvalid = TRUE;
			break;

		case IPC_GET_STATUS_MODE:
			psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
			fj_StatusModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		case IPC_SET_STATUS_MODE:
			psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
			pData[lDataLength] = 0;
			fj_StatusModeFromString( pfph->pfsys, (LPSTR)pData );
			fj_StatusModeToString( pfph->pfsys, (LPSTR)psmsgOut->Data );
			bSendString = TRUE;
			break;

		default:
			bInvalid = TRUE;
			break;
	}
	if ( TRUE == bInvalid )
	{
		psmsgOut->Header.Cmd = IPC_INVALID;
		*(psmsgOut->Data) = 0;
		bSendString = TRUE;
	}
	if ( TRUE == bSendString )
	{
		lDataLength = strlen( (LPSTR)psmsgOut->Data );
		psmsgOut->Header.Length = lDataLength;
		socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
	}
	if ( TRUE == bSendBinary )
	{
		memcpy( &lDataLength, (pElement-sizeof(LONG)), sizeof(LONG) );
		psmsgBig = fj_malloc( sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lDataLength + 1);
		psmsgBig->Header.Cmd     = psmsgOut->Header.Cmd;
		psmsgBig->Header.BatchID = lBatchID;
		psmsgBig->Header.Length  = lDataLength;
		memcpy( psmsgBig->Data, pElement, lDataLength );
		socketSend( pVoid, (LPBYTE)psmsgBig, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
		fj_free( psmsgBig );
	}

	return;
}
//
// send the high voltage info
//
VOID fj_socketCmdSendVoltageInfo( LPFJPRINTHEAD pfph, LPVOID pVoid, _FJ_SOCKET_MESSAGE *psmsgOut, VOID((*socketSend)( LPVOID pVoid, LPBYTE pByte, LONG lLength))  )
{
	LONG  lDataLength;
	LONG  lVoltage = 145;

	psmsgOut->Header.Cmd = IPC_STATUS;
	//    if ( (TRUE == fj_CVT.pfph->bHeadSelected) &&
	//	     (TRUE == fj_CVT.pFJglobal->bADReady )  )
	if ( (TRUE == pfph->bHeadSelected) )
	{
		sprintf( (LPSTR)psmsgOut->Data, "VoltReady=ON;Voltage=%d;", (int)lVoltage );
		//		if ( TRUE == fj_CVT.pFJglobal->bHeater1Present )
		{
			lDataLength = strlen( (LPSTR)psmsgOut->Data );
			psmsgOut->Header.Length = lDataLength;
			socketSend( pVoid, (LPBYTE)psmsgOut, sizeof(_FJ_SOCKET_MESSAGE_HEADER)+lDataLength );
		}
	}

	return;
}
