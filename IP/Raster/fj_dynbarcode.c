#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <time.h>
#endif

#include "barcode.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_dynbarcode.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_system.h"

#ifdef _WINDOWS
//static CHAR szWinDynData [FJBARCODE_SIZE + 1] = { 0 };
//static const LPCTSTR szWinDefDynData = "000000";
extern char lpszWinDynData [FJMESSAGE_DYNAMIC_SEGMENTS][FJTEXT_SIZE + 1];

FJDLLExport VOID fj_BearerVDraw(LPUCHAR pBuffer, UCHAR *VBar, LONG lHeightBytes, LONG lBarWidth );
FJDLLExport VOID fj_BearerHDraw (LPUCHAR pBuffer, LONG lHeight, LONG lWidth, LONG lBarHeight, LONG lTextHeight);
#endif									//_WINDOWS

// used for MemStor identification
const  CHAR fj_DynBarCodeID[]   = "Dynbarcode";

//
//  FUNCTION: fj_DynBarCodeNew()
//
//  PURPOSE:  Create a new FJDYNBARCODE structure
//
FJDLLExport LPFJDYNBARCODE fj_DynBarCodeNew( VOID )
{
	LPFJDYNBARCODE pfdb;

	pfdb = (LPFJDYNBARCODE)fj_calloc( 1, sizeof (FJDYNBARCODE) );

	pfdb->lSysBCIndex  = 0;				// index of global barcode definition
	pfdb->lBoldValue   = 0;				// amount of boldness. 0 origin.
	pfdb->lWidthValue  = 1;				// amount of width stretch. 1 origin.
	pfdb->bHROutput    = TRUE;			// true to text output into barcode
	pfdb->cHRAlign     ='C';			// text output alignment ( L, R, C)
	pfdb->lHRBold      = 2;				// text output Bold value
	pfdb->lFirstChar   = 0;				// start pos in dinamic field
	pfdb->lNumChar     = 1; // Burl 1.2009				// number of charecters to use in dinamic field. 0 = 'natural' ( no alteration )
	pfdb->lDynamicID   = 0;				// ID of dinamic text segment
	pfdb->pImageHRText = NULL;			// pointer to image location for Human Readable Text
	pfdb->pBC          = NULL;			// pointer to BC Lib structure
	pfdb->pfeText      = NULL;			// element for text generation

	return( pfdb );
}
//
//  FUNCTION: fj_DynBarCodeDestroy()
//
//  PURPOSE:  Destroy a FJDYNBARCODE structure
//
FJDLLExport VOID fj_DynBarCodeDestroy( LPFJELEMENT pfe )
{
	LPFJDYNBARCODE pfdb;
	LPFJELEMENT pfeText;

	pfdb = (LPFJDYNBARCODE)pfe->pDesc;
	if ( NULL != pfdb)
	{
		if ( NULL != pfdb->pBC      ) Barcode_Delete( pfdb->pBC );
		pfeText = pfdb->pfeText;
		if ( NULL != pfeText )
		{
			fj_ElementDestroy( pfeText );
		}
		fj_free( pfdb );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_DynBarCodeDup()
//
//  PURPOSE:  Duplicate a FJDYNBARCODE structure
//
FJDLLExport LPFJDYNBARCODE fj_DynBarCodeDup( CLPCFJDYNBARCODE pfdbIn )
{
	LPFJDYNBARCODE pfdbDup;

	pfdbDup = (LPFJDYNBARCODE)fj_malloc( sizeof (FJDYNBARCODE) );
	memcpy( pfdbDup, pfdbIn, sizeof(FJDYNBARCODE) );
	pfdbDup->pBC     = NULL;
	pfdbDup->pfeText = NULL;

	return( pfdbDup );
}
//
//  FUNCTION: fj_DynBarCodeEncode()
//
//  PURPOSE:  Encode Barcode and prepare a FJDYNBARCODE structure
//
VOID fj_DynBarCodeEncode( CLPFJELEMENT pfe )
{
#ifndef _WINDOWS
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
#endif								//_WINDOWS
	LPFJSYSBARCODE pfsysBC;
	LPFJDYNBARCODE    pfdb;
	STR   strText[1+FJBARCODE_SIZE];	// text string without blanks
	STR   pTempStr[1+FJTEXT_SIZE];
	LPSTR pTextIn;
	LPSTR pTextNoBlanks;
	LONG  in_length;
	int   iBCFlags;
	int   error;
	CHAR  c;

	pfdb     = (LPFJDYNBARCODE)pfe->pDesc;
	pfsysBC = &(pfe->pfm->pfph->pfsys->bcArray[pfdb->lSysBCIndex]);

	if (NULL != pfdb->pBC) Barcode_Delete (pfdb->pBC);

	// move text to barcode structure
	// if barcode is I 2 of 5, strip out blanks used for positionin in HR text
#ifndef _WINDOWS
	in_length = strlen(pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData);
	if ( in_length > pfdb->lFirstChar) strcpy(pTempStr, pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData + pfdb->lFirstChar);
	else strcpy(pTempStr, pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData);
	pTextIn = pTempStr;
#else
	//in_length = strlen(szWinDefDynData); // Burl 1.2009
	//if ( in_length > pfdb->lFirstChar) strcpy(pTempStr, szWinDefDynData + pfdb->lFirstChar); // Burl 1.2009
	//else strcpy(pTempStr, szWinDefDynData); // Burl 1.2009
	in_length = strlen(lpszWinDynData [pfdb->lDynamicID]);
	if ( in_length > pfdb->lFirstChar)
		strncpy(pTempStr, (lpszWinDynData [pfdb->lDynamicID]) + pfdb->lFirstChar, pfdb->lNumChar);
	pTextIn = pTempStr; // Burl 1.2009
#endif								//_WINDOWS
	if ( FJTEXT_SIZE > pfdb->lNumChar && 0 != pfdb->lNumChar ) pTempStr[pfdb->lNumChar] = 0;

	pTextNoBlanks = strText;
	while ( 0 != (c = *pTextIn++) )
	{
		*pTextNoBlanks = c;
		if ( BARCODE_I25 == (pfsysBC->lBCFlags&BARCODE_ENCODING_MASK) )
		{
			if ( ' ' != c ) pTextNoBlanks++;
		}
		else pTextNoBlanks++;
	}
	*pTextNoBlanks = 0;
	pfdb->pBC = Barcode_Create (strText);
	if (NULL == pfdb->pBC)
	{
		error = 43;						// place to stop !!!!
	}
	else
	{
		iBCFlags = pfsysBC->lBCFlags | BARCODE_NO_ASCII;
		if ( FALSE == pfsysBC->bCheckDigit ) iBCFlags |= BARCODE_NO_CHECKSUM;
		error = Barcode_Encode( pfdb->pBC, iBCFlags );
		if (0 != error)
		{
			error = 43;					// place to stop !!!!
		}
	}
	return;
}
//
//  FUNCTION: fj_BarCodeDraw()
//
//  PURPOSE:  Draw Barcode
//
VOID fj_DynBarCodeDraw( CLPFJELEMENT pfe )
{
#ifndef _WINDOWS
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
#endif								//_WINDOWS
	register int    i;
	register int    len;
	register int    x;
	register LONG   lBits;
	register LONG   lBytesBC;
	register LPBYTE pBuffer;
	register LONG   lBleed;

	LPFJSYSTEM     pfsys;
	LPFJSYSBARCODE pfsysBC;
	LPFJPRINTHEAD  pfph;
	LPFJDYNBARCODE    pfdb;
	LPFJIMAGE      pfi;
	LPFJIMAGE      pfiOld;
	LPFJIMAGE      pfiHR;
	LPFJTEXT       pftHR;
	LPFJELEMENT    pfeHR;
	LPBYTE  pImage, pBufferText;
	STR     pTempStr[1+FJTEXT_SIZE];
	LONG    in_length;
	LPSTR   pStr;
	LPSTR   pStrTemp;
	LPSTR   pDynStr;
	LONG    lWidthBars;
	LONG    lWidthBC;
	LONG    lWidthHR;
	LONG    lWidthImage;
	BOOL    if_bar;
	BOOL    bHROutput;
	LONG    lHeightHRPlus;
	LONG    lHeightBC;
	LONG    lBytesHR;
	unsigned char c;
	FLOAT   fRes;
	FLOAT   fX;
	LONG    lBarWidth[5];
	UCHAR   VBar[(FJSYS_NOZZLES+7)/8];
	UCHAR   VBarA[(FJSYS_NOZZLES+7)/8];
	UCHAR   mask;
	UCHAR   HR_Format[] = { 3,5,5,0 };
	UCHAR   HR_Fid, HR_Fnum;
	LPSTR   pHRStr;
	LPSTR   pBCFontName;

	LPSTR pList1[40];
	LPSTR pList2[3];
	LONG lCount;
	LONG lCount2;
	BOOL bGBarDraw;
	pfeHR = NULL;

	pBCFontName = (LPSTR)fj_malloc( FJFONT_NAME_SIZE + 1 );

	pfph  = pfe->pfm->pfph;
	pfsys = pfph->pfsys;
	pfdb   = (LPFJDYNBARCODE)pfe->pDesc;
	pfsysBC = &(pfsys->bcArray[pfdb->lSysBCIndex]);

#ifndef _WINDOWS
	in_length = strlen(pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData);
	if ( in_length > pfdb->lFirstChar) strcpy(pTempStr, pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData + pfdb->lFirstChar);
	else strcpy(pTempStr, pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData);
	pDynStr = pTempStr;
#else
	//in_length = strlen(szWinDefDynData); // Burl 1.2009
	//if ( in_length > pfdb->lFirstChar) strcpy(pTempStr, szWinDefDynData + pfdb->lFirstChar); // Burl 1.2009
	//else strcpy(pTempStr, szWinDefDynData); // Burl 1.2009
	in_length = strlen(lpszWinDynData [pfdb->lDynamicID]);
	if ( in_length > pfdb->lFirstChar)
		strncpy(pTempStr, (lpszWinDynData [pfdb->lDynamicID]) + pfdb->lFirstChar, pfdb->lNumChar);
	pDynStr = pTempStr; // Burl 1.2009
#endif								//_WINDOWS
        if ( FJTEXT_SIZE > pfdb->lNumChar && 0 != pfdb->lNumChar ) pTempStr[pfdb->lNumChar] = 0;
	
	if (BARCODE_EAN == (pfsysBC->lBCFlags & BARCODE_ENCODING_MASK) || BARCODE_UPC == (pfsysBC->lBCFlags & BARCODE_ENCODING_MASK))
	{
		bGBarDraw = TRUE;
	}
	else bGBarDraw = FALSE;

	if ( TRUE == pfsys->bShaftEncoder ) fRes = pfsys->fEncoder;
	else                                fRes = pfsys->fPrintResolution;
	// fRes is Dots Per Inch
	// fX is number of dots per narrow bar
	// pfsysBC->lBarVal is narrow bar width in thousandths of an inch
	fX = fRes * ((FLOAT)pfsysBC->lBarVal[0])/ (FLOAT)1000.;
	lBarWidth[0] = 0;
	lBarWidth[1] = (LONG)(fX * 1.); if ( ((fX*1.) - (FLOAT)(lBarWidth[1])) >= .5 ) lBarWidth[1]++;
	if ( TRUE == bGBarDraw )
	{
		lBarWidth[2] = lBarWidth[1] * 2;
		lBarWidth[3] = lBarWidth[1] * 3;
		lBarWidth[4] = lBarWidth[1] * 4;
	}
	else
	{
		lBarWidth[2] = (LONG)(fX * 2.); if ( ((fX*2.) - (FLOAT)(lBarWidth[2])) >= .5 ) lBarWidth[2]++;
		// the barcode engine makes I 2 of 5 with a widebar width of 3.
		// we need to use 2.5 most of the time
		if ( BARCODE_I25 == (pfsysBC->lBCFlags & BARCODE_ENCODING_MASK) )
		{
			lBarWidth[3] = (LONG)(fX * 2.5); if ( ((fX*2.5) - (FLOAT)(lBarWidth[3])) >= .5 ) lBarWidth[3]++;
		}
		else
		{
			lBarWidth[3] = (LONG)(fX * 3.); if ( ((fX*3.) - (FLOAT)(lBarWidth[3])) >= .5 ) lBarWidth[3]++;
		}
		lBarWidth[4] = (LONG)(fX * 4.); if ( ((fX*4.) - (FLOAT)(lBarWidth[4])) >= .5 ) lBarWidth[4]++;
	}

	bHROutput = pfdb->bHROutput;

	lHeightBC = pfsysBC->lBCHeight;
	lBytesBC = (lHeightBC + 7) / 8;

	pStr = NULL;
	if ( NULL != pfdb->pBC )
	{
		x = 0;
		if ( TRUE == bGBarDraw )
		{
			if (pfdb->pBC && pfdb->pBC->encoding) {
				i = strlen(pDynStr);
				if  (0 == strcmp(pfdb->pBC->encoding,"EAN-13") && 12 != i) x =1;
				else if (0 == strcmp(pfdb->pBC->encoding,"EAN-8") && 7 != i) x =1;
				else if (0 == strcmp(pfdb->pBC->encoding,"UPC-A") && 11 != i) x =1;
				else if (0 == strcmp(pfdb->pBC->encoding,"UPC-E") && 6 != i) x =1;
			}
		}
		if (0 == x) pStr = pfdb->pBC->partial;
	}

	// calculate with of barcode
	lWidthBars = 0;
	if ( NULL != pStr )
	{
		if ( '0' == *pStr ) pStr++;		// Skip text margin from barcode library
		pStrTemp = pStr;
		while (c = *pStrTemp)
		{
			if      (isdigit(c)) c = c - '0';
			else if (islower(c)) c = c - 'a' + 1;
			else                 c = 0;
			if (9 == c) lWidthBars += lBarWidth[1] * 9;
			else lWidthBars += lBarWidth[c];
			pStrTemp++;
		}
	}
	else
	{
		bHROutput = TRUE;				// if Barcode_Encode error, display text
		bGBarDraw = FALSE;
	}

	// generate text image
	lHeightHRPlus = 0;
	lWidthHR = 0;
	if ( TRUE == bHROutput )
	{
		pfeHR = pfdb->pfeText;
		if ( NULL == pfeHR )
		{
			pfeHR = fj_ElementTextNew();
			pfdb->pfeText = pfeHR;
		}
		pftHR = (LPFJTEXT)pfeHR->pDesc;
		pfeHR->pfm = pfe->pfm;
		if ( 32 < pfph->pfphy->lChannels ) strcpy ( pBCFontName, BARCODE_FONT256 );
		else strcpy ( pBCFontName, BARCODE_FONT );
		//		strcpy ( pBCFontName, BARCODE_FONT );

		if (NULL != fj_PrintHeadBuildFont( pfph, pBCFontName ) )
		{
			strcpy ( pftHR->strFontName, pBCFontName );
			if ( BARCODE_I25 == (pfsysBC->lBCFlags & BARCODE_ENCODING_MASK) )
			{
				pHRStr = pftHR->strText;
				HR_Fid  = 0;
				HR_Fnum = HR_Format[HR_Fid];
				len = strlen(pDynStr);

				for ( i = 0; i < len; i++)
				{
					if ( 0 == HR_Fnum )
					{
						*pHRStr = ' ';
						pHRStr++;
						HR_Fnum = HR_Format[++HR_Fid];
						if ( 0 == HR_Fnum) HR_Fnum = 99;
					}
					*pHRStr = *( pDynStr + i );
					pHRStr++;
					HR_Fnum--;
				}
				*pHRStr = 0;
			}
			else strcpy ( pftHR->strText, pDynStr );

			// if a checkdigit was added when the barcode was generated, get it and add it to the HR string
			if ( TRUE == pfsysBC->bCheckDigit )
			{
				pStrTemp = NULL;
				if ( NULL != pfdb->pBC ) pStrTemp = pfdb->pBC->textinfo;
				if ( NULL != pStrTemp )
				{
					pStrTemp = strrchr( pStrTemp, ':' );
					if ( NULL != pStrTemp )
					{
						c = *(pStrTemp+1);
						pStrTemp = pftHR->strText + strlen(pftHR->strText);
						*pStrTemp = ' ';
						*(pStrTemp+1) = c;
						*(pStrTemp+2) = 0;
					}
				}
			}

			pftHR->lBoldValue = pfdb->lHRBold;

			if ( TRUE == bGBarDraw )
			{
				pStrTemp = pfdb->pBC->textinfo;
				lCount = fj_ParseKeywordsFromStr(pStrTemp, ' ', pList1, 40);
				for ( i=lCount-1; i >= 0; i--)
				{
					lCount2 = fj_ParseKeywordsFromStr( pList1[i], ':', pList2, 3 );
					if ( lCount2 == 3 )
					{
						strcpy ( pftHR->strText, pList2[2] );
						pfeHR->pActions->fj_DescCreateImage( pfeHR );
						lBytesHR = (pfeHR->pfi->lHeight + 7) / 8;
						if ( i == lCount-1 )
						{
							lWidthHR = pfeHR->pfi->lLength / lBytesHR;
							// Last char pos + width of Last char
							lWidthHR += ((atoi(pList2[0])+1)*lBarWidth[1]);
							// Real image for HR
							pfiHR =  fj_ImageNew( lWidthHR * lBytesHR );
							if (NULL != pfiHR) pfiHR->lHeight = pfeHR->pfi->lHeight;
						}
						if (NULL != pfiHR)
						{
							pBuffer = (LPBYTE)pfiHR + sizeof(FJIMAGE);
							pImage = (LPBYTE)pfeHR->pfi + sizeof(FJIMAGE);
							// set ptr to char pos
							pBuffer += (atoi(pList2[0])+1) * lBarWidth[1]* lBytesHR;
							memcpy( pBuffer, pImage, pfeHR->pfi->lLength );
						}
					}
				}
				fj_ImageRelease( pfeHR->pfi );
				pfeHR->pfi = pfiHR;
			}
			else
			{
				pfeHR->pActions->fj_DescCreateImage( pfeHR );
				pfiHR = pfeHR->pfi;
			}

			if ( NULL != pfiHR )
			{
				lHeightHRPlus = pfiHR->lHeight + 1;
				lBytesHR = (pfiHR->lHeight + 7) / 8;
				lWidthHR = pfiHR->lLength / lBytesHR;
			}
		}
		else
			bHROutput = FALSE;
	}

	fj_free( pBCFontName );
	// add width of vertical bearer bars and quiet zones
	if ( FALSE == bGBarDraw )
		lWidthBC = lWidthBars + pfsysBC->lQuietZone * 2 + pfsysBC->lVer_Bearer * 2;
	else
		lWidthBC = lWidthBars;

	// get image structure and buffer
	lWidthImage = lWidthBC;
										// use widest width
	if( lWidthHR > lWidthImage ) lWidthImage = lWidthHR ;
	pfi = fj_ImageNew( lWidthImage * lBytesBC );
	if ( NULL != pfi )
	{
		pfi->lHeight = lHeightBC;
		pImage = (LPBYTE)pfi + sizeof(FJIMAGE);

		// build one raster
		mask = 0;
		x = -1;
		for ( i = 0; i < pfi->lHeight; i++ )
		{
			if ( 0 == mask )
			{
				x++;
				VBar[x] = 0;
				VBarA[x] = 0;
				mask = 0x80;
			}
			if ( i >= lHeightHRPlus ) VBar[x] |= mask;
			if ( i >= lHeightHRPlus/2 ) VBarA[x] |= mask;
			mask = mask >> 1;
		}

		// make the barcode from the string built by the barcode engine
		x = lBytesBC;
		if ( FALSE == bGBarDraw )
			pBuffer = pImage + (pfsysBC->lQuietZone + pfsysBC->lVer_Bearer) * lBytesBC;
		else
			pBuffer = pImage;

		if ( NULL != pStr )
		{
			if_bar = TRUE;				// first caracter in string if for a bar
			for ( pStrTemp = pStr; c = *pStrTemp; pStrTemp++)
			{
				if      (isdigit(c)) c = c - '0';
				else if (islower(c)) c = c - 'a' + 1;
				else                 c = 0;
				if (9 == c)				// temp bypass for UPC-A and EAN-13
				{
					if_bar = FALSE;
					i = lBarWidth[1] * 9;
				}
				else i = lBarWidth[c];	// bar width in rasters

				if (if_bar)
				{
										// number of rasters to skip for ink bleed
					lBleed = pfsysBC->lBarVal[c];
										// skip 1/2 bleed rasters at front
					pBuffer += lBytesBC * (lBleed/2);
					i -= lBleed;		// rasters - bleed value
					for ( ; i > 0; i-- )
					{
						for ( x = 0; x < lBytesBC; x++ )
						{
							c = *pStrTemp;
							if (islower(c) &&  TRUE == bGBarDraw ) *pBuffer++ = VBarA[x];
							else *pBuffer++ = VBar[x];
						}
					}
					// skip remainder of bleed rasters
					pBuffer += lBytesBC * (lBleed - lBleed/2);
				}
				else
										// skip space
						pBuffer += i * lBytesBC;

				if_bar = !if_bar;
			}
		}

		// process bearer bars
		if ( 0 != pfsysBC->lVer_Bearer && FALSE == bGBarDraw )
		{
			fj_BearerVDraw( pImage, VBar, lBytesBC, pfsysBC->lVer_Bearer );
			pBuffer = pImage + (lWidthBC  - pfsysBC->lVer_Bearer) * lBytesBC;
			fj_BearerVDraw( pBuffer, VBar, lBytesBC, pfsysBC->lVer_Bearer );
		}
		if ( 0 != pfsysBC->lHor_Bearer &&  FALSE == bGBarDraw )
			fj_BearerHDraw( pImage, lHeightBC, lWidthBC, pfsysBC->lHor_Bearer, lHeightHRPlus);

		// process HR text
		if ( (TRUE == bHROutput) || (TRUE == pfe->bEdit) )
		{
			pBufferText = (LPBYTE)pfiHR + sizeof(FJIMAGE);
			pBuffer    = pImage;

			if ( FALSE == bGBarDraw )
			{
				i = lWidthBC - lWidthHR;
				if ( i > 0 )
				{
					if (pfdb->cHRAlign == 'R')
						pBuffer += i * lBytesBC;
					if (pfdb->cHRAlign == 'C')
						pBuffer += i/2 * lBytesBC;
				}
			}
										// save for possible editing or other use
			pfdb->pImageHRText = pBuffer;

			for ( i = lWidthHR; i > 0; i--)
			{
				for ( x = lBytesHR; x > 0; x--)
					*pBuffer++ |= *pBufferText++;
				pBuffer += lBytesBC - lBytesHR;

				if ((pBuffer - pImage) >= pfi->lLength)
					break;
			}
		}

		// count bits in barcode
		lBits = 0;
		pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);
		for ( i = 0; i < pfi->lLength; i++ )
			lBits += fj_ImageCountBits[*(pBuffer + i)];
		pfi->lBits = lBits;
	}

	pfiOld = pfe->pfi;					// do not reuse image
	pfe->pfi = pfi;						// put new image into pfe
	if ( NULL != pfiOld )				// release old one
	{
		fj_ImageRelease( pfiOld );
	}
#ifndef _WINDOWS
	pBRAM->DynamicSegments[pfdb->lDynamicID].bChanged = FALSE;
#endif								//_WINDOWS
	return;
}
FJDLLExport VOID fj_DynBarCodeCreateImage( CLPFJELEMENT pfe )
{
	//LPFJPRINTHEAD pfph;
	LPFJDYNBARCODE   pfdb;

	//pfph = pfe->pfm->pfph;
	pfdb  = (LPFJDYNBARCODE)pfe->pDesc;

	fj_DynBarCodeEncode( pfe );			// Encode barcode text
	fj_DynBarCodeDraw( pfe );			// Draw barcode

	// bold and width are done in each element's routines.
	// they are sometimes omitted, depending on the element.
	// slant and transforms are usually 'global' and usually part of the 'environment',
	//    and they are not wanted for some special cases; e.g., text in a barcode.
	// bold first
	//if ( 0 < pfb->lBoldValue  ) pfe->pfi = fj_ImageBold(  pfe->pfi, pfb->lBoldValue );
	//if ( 1 < pfb->lWidthValue ) pfe->pfi = fj_ImageWidth( pfe->pfi, pfb->lWidthValue );

	return;
}
//
//   DynBarCode does change
//
FJDLLExport VOID fj_DynBarCodePhotocellBuild( CLPFJELEMENT pfe )
{
	fj_DynBarCodeCreateImage( pfe );
	return;
}
//
//  FUNCTION: fj_DynBarCodeToStr()
//
//  PURPOSE:  Convert a FJDYNBARCODE structure into a descriptive string
//
FJDLLExport VOID fj_DynBarCodeToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJDYNBARCODE pfdb = (LPFJDYNBARCODE)pfe->pDesc;
	STR sName[FJBARCODE_NAME_SIZE*2];

										// barcode element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	sprintf (pStr, "{%s,%s,%f,%d,%c,%d,%d,%c,%c,%c,%d,%c,%c,%d,%d,%d,%d}",
		fj_DynBarCodeID,
		sName,
		pfe->fLeft,						// distance from left edge of box
		pfe->lRow,						// row number to start pixels - 0 = bottom of print head
		(CHAR)pfe->lPosition,			// type of positioning
		pfdb->lBoldValue,				// amount of boldness. 0 origin.
		pfdb->lWidthValue,				// amount of width stretch. 1 origin.
										// true for invert
		(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F'),
		pfdb->lSysBCIndex,				// index of global barcode definition
										// true to text output into barcode
		(pfdb->bHROutput == TRUE ? 'T' : 'F'),
		pfdb->cHRAlign,					// text output alignment ( L, R, C)
		pfdb->lHRBold,					// text output Bold value
		pfdb->lFirstChar,				// start pos in dinamic field
		pfdb->lNumChar,					// number of charecters to use in dinamic field. 0 = 'natural' ( no alteration )
		pfdb->lDynamicID);				// ID of dinamic text segment

	return;
}
//
//  FUNCTION: fj_DynBarCodeFromStr
//
//  PURPOSE:  Convert a descriptive string into a FJDYNBARCODE structure
//
FJDLLExport VOID fj_DynBarCodeFromStr( LPFJELEMENT pfe, LPSTR pStr )
{
#define BARCODE_PARAMETERS 17
	LPFJDYNBARCODE pfdb;
	LPSTR  pParams[BARCODE_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfdb = (LPFJDYNBARCODE)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJBARCODE_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJBARCODE_MEMSTOR_SIZE );
	*(pStrM+FJBARCODE_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, BARCODE_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_DynBarCodeID ) ) res = 0;

	if ( res == BARCODE_PARAMETERS )	// FJ_BARCODE string must have 17 field delimiters!!!!!
	{
		pfe->lTransform  =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft       = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow        = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition   =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfdb->lBoldValue  = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfdb->lWidthValue = atol(pParams[ 6]);
		// true for inverse
		pfe->lTransform |=   (*(pParams[ 7]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
										// index of global barcode definition
		pfdb->lSysBCIndex = atol(pParams[10]);
		// true to text output into barcode
		pfdb->bHROutput   =   (*(pParams[11]) == 'T' ? TRUE : FALSE);
										// text output alignment ( L, R, C)
		pfdb->cHRAlign    =    *(pParams[12]);
										// text output Bold value
		pfdb->lHRBold     = atol(pParams[13]);
										// start pos in dinamic field
		pfdb->lFirstChar  = atol(pParams[14]);
										// number of charecters to use in dinamic field. 0 = 'natural' ( no alteration )
		pfdb->lNumChar    =  atol(pParams[15]);
										// ID of dinamic text segment
		pfdb->lDynamicID  = atol(pParams[16]);
	}
	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_DynBarCodeBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJDYNBARCODE from FJMEMSTOR
//
FJDLLExport VOID fj_DynBarCodeBuildFromName( CLPCFJMEMSTOR pfms, LPFJELEMENT pfe, LPCSTR pName )
{
	LPSTR       pDynBarCodeStr;

	pDynBarCodeStr = (LPSTR)fj_MemStorFindElement( pfms, fj_DynBarCodeID, pName );
	if ( NULL != pDynBarCodeStr )
	{
		fj_DynBarCodeFromStr( pfe, pDynBarCodeStr );
	}

	return;
}
//
//	FUNCTION: fj_DynBarCodeAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJDYNBARCODE element to FJMEMSTOR
//
FJDLLExport BOOL fj_DynBarCodeAddToMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	CHAR buff[FJBARCODE_MEMSTOR_SIZE];
	fj_DynBarCodeToStr( pfe, buff );

	return( fj_MemStorAddElementString( pfms, buff ) );
}
//
//	FUNCTION: fj_DynBarCodeDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Delete FJDYNBARCODE element from FJMEMSTOR
//
FJDLLExport BOOL fj_DynBarCodeDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	STR sName[FJBARCODE_NAME_SIZE*2];

										// barcode element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	return( fj_MemStorDeleteElement( pfms, fj_DynBarCodeID, sName ) );
}
//
//	FUNCTION: fj_DynBarCodeGetType
//
//	Return: BarCode type
//
//
FJDLLExport enum FJELETYPE fj_DynBarCodeGetType( VOID )
{

	return( FJ_TYPE_DYNBARCODE );
}
//
//	FUNCTION: fj_DynBarCodeGetTypeString
//
//	Return: Pointer to string for BarCode type
//
//
FJDLLExport LPCSTR fj_DynBarCodeGetTypeString( VOID )
{

	return( fj_DynBarCodeID );
}
//
//	FUNCTION: fj_DynBarCodeGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_DynBarCodeGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
#ifndef _WINDOWS
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
	LPFJDYNBARCODE pfdb;

	pfdb = (LPFJDYNBARCODE)pfe->pDesc;
	strncpy( pStr, pBRAM->DynamicSegments[pfdb->lDynamicID].strDynData, lMaxCount );
#else
	LPFJDYNBARCODE pfdb;

	pfdb = (LPFJDYNBARCODE)pfe->pDesc;

	//strncpy( pStr, szWinDynData, lMaxCount );
	strncpy (pStr, lpszWinDynData [pfdb->lDynamicID], lMaxCount);
#endif								// _WINDOWS

	*(pStr+lMaxCount-1) = 0;
	return;
}
FJCELEMACTION fj_DynBarCodeActions =
{
	(LPVOID  (*)( VOID ))fj_DynBarCodeNew,
	(LPVOID  (*)( LPVOID ))fj_DynBarCodeDup,
	fj_DynBarCodeDestroy,
	fj_DynBarCodeCreateImage,
	fj_DynBarCodePhotocellBuild,
	fj_DynBarCodeBuildFromName,
	fj_DynBarCodeAddToMemstor,
	fj_DynBarCodeDeleteFromMemstor,
	fj_DynBarCodeGetTextString,
	fj_DynBarCodeGetType,
	fj_DynBarCodeGetTypeString
};
