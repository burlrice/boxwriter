#include "fj_netsilicon.h"
#include <appconf.h>					// for DEFINEd application options
#include <narmsrln.h>					// for nvparams & dialog port output
#include <netosIo.h>					// for OPEN options
#include <netoserr.h>
#include <nareset.h>
#include <narmled.h>
#include <na_isr.h>
#include <sysClock.h>
#include <mailcapi.h>
#include <hservapi.h>
#include "reg_def.h"

#include <stdio.h>

#include "fj.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_image.h"
#include "fj_element.h"

void fj_AppSearchURL(unsigned long handle, char *URLp )
{
	if ( NULL != fj_CVT.fj_1AppSearchURL )
	{
		fj_CVT.fj_1AppSearchURL( handle, URLp );
	}
	else
	{
		AppSearchURL( handle, URLp );
	}
}
void fj_AppPreprocessURLxxx(unsigned long handle, char *URLp )
{
}
void fj_AppPreprocessURL(unsigned long handle, char *URLp )
{
	if ( NULL != fj_CVT.fj_1AppPreprocessURL )
	{
		fj_CVT.fj_1AppPreprocessURL( handle, URLp );
	}
	else
	{
		AppPreprocessURL( handle, URLp );
	}
}
// used in marmsrln.c to keep the NETOS parameters in RAM instead of NVRAM
// we do not have a NVRAM.
// used in fj_init.c to write them to ROM
extern    BOOL    bDialogParamChange;
extern    BOOL    fj_bNetosParamsChanged;
extern    ULONG   ldhcpAddr;
extern    ULONG   ldhcpMask;
extern    ULONG   ldhcpGateway;

// __ghsend_free_mem is set up by the Green Hills initialization software to point to
// the end of what is linked and loaded.
// the NET+OS standard link files pads out the free_mem segment to 64k.
extern      BYTE __ghsend_free_mem;

//
// global info
//
BOOL      fj_watchDog    = FALSE;		//TRUE;			// if true, use watchdog timer, if other conditions are OK
BOOL      fj_noPrintDev  = FALSE;		// if true, do not use any FoxJet chips and/or devices
BOOL      fj_writeROMMsg = FALSE;		// if true, always reset ROM messages on boot up
BOOL      fj_rasterLoop  = FALSE;		// if true, have a background loop running to generate a load and measure throughput
BOOL      fj_bootInfo    = TRUE;		// if true, writes boot-time info to the serial port

// common snoop routine to listen to various ports
extern    VOID fj_threadListenStart(ULONG port_number);
// threads for listening
TX_THREAD thr37;
TX_THREAD thr53;
TX_THREAD thr123;
TX_THREAD thr525;
// thread for simulating a full work load.
// makes and deletes rasters in a loop
TX_THREAD thr_raster;

// thread routine to loop making rasters
VOID fj_threadRasterStart(ULONG param)
{
	BOOL   bLoop = TRUE;
	struct tm tm;

	while( TRUE == bLoop )
	{
										// if flag is not already on,
		if ( FALSE == fj_CVT.pFJglobal->bPrintTestLoop )
		{
			fj_SystemGetTime( &(fj_CVT.pfsys->tmPhotoCell) );
										// start a dummy one
			fj_CVT.pFJglobal->bPrintTestLoop = TRUE;
			tx_thread_resume( &(fj_CVT.pFJglobal->thrPrint) );
		}
	}
	return;
}
VOID fj_startThreads(VOID)
{
	CLPCFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	int    ret;
	LPBYTE pStack;

	if ( TRUE == fj_rasterLoop )
	{
		pStack = fj_memory_getStack(FJ_THREAD_NAMES_STACK_SIZE);
		// create the thread to simulate printing.
										// control block for thread
		ret = tx_thread_create (&thr_raster,
			"Raster Loop Thread",		// thread name
			fj_threadRasterStart,		// entry function
			0,							// parameter
			pStack,						// start of stack
			FJ_THREAD_NAMES_STACK_SIZE,	// size of stack
			FJ_THREAD_LOWEST_PRIORITY,	// priority
			FJ_THREAD_LOWEST_PRIORITY,	// preemption threshold
			1,							// time slice threshold
			TX_AUTO_START);				// start immediately

		if (ret != TX_SUCCESS)			// if couldn't create thread
		{
			netosFatalError ("Unable to create raster loop thread", 1, 1);
		}
	}

	return;
}
static BOOL fj_bHeartbeat = FALSE;
static TX_TIMER fj_timerSeconds;		// timer for seconds counter

LONG   fj_lRastersPerSecond;
LONG   fj_lHSClocksPerMillisecond = 0;
//
// small timer function to count seconds
//
static void fj_timerSecondsFunc(ULONG in)
{
	volatile LPGA   pga = fj_CVT.pGA;	// get base address
	static   WORD16 wRCPrev;
	static   WORD16 wHSPrev;
	WORD16 wRCNow, wHSNow;

	fj_CVT.pFJglobal->lSeconds++;

	fj_bHeartbeat = !fj_bHeartbeat;
	if ( TRUE == fj_bHeartbeat )
	{
		NALedGreenOn();					// turn on the green LED
		//		fj_socket_IOBoxGreenOn();
	}
	else
	{
		NALedGreenOff();				// turn off the green LED
		//		fj_socket_IOBoxGreenOff();
	}

										// read
	wHSNow = fj_CVT.pGA->rw.read.HStimer;
										// save
	fj_CVT.pFJglobal->gaRead.rw.read.HStimer = wHSNow;
										// read
	wRCNow = fj_CVT.pGA->rw.read.rastercounter;
										// save
	fj_CVT.pFJglobal->gaRead.rw.read.rastercounter = wRCNow;
	if ( wRCNow > wRCPrev )
	{
		fj_lRastersPerSecond = wRCNow - wRCPrev;
	}
	else
	{
		fj_lRastersPerSecond = 0x10000 + wRCNow - wRCPrev;
	}
	wRCPrev = wRCNow;

	if ( 0 == fj_lHSClocksPerMillisecond )
	{
		ULONG hi, low, lowPrev;
		ULONG lHSwrap = 0;
		BOOL  bHi = TRUE;

		NATotalTicks( &hi, &low );
		lowPrev = low;
		while ( low <= lowPrev ) NATotalTicks( &hi, &low );
										// read
		wHSPrev = fj_CVT.pGA->rw.read.HStimer;
										// save
		fj_CVT.pFJglobal->gaRead.rw.read.HStimer = wHSPrev;
		lowPrev = low;
		while ( low <= lowPrev )
		{
										// read
			wHSNow = fj_CVT.pGA->rw.read.HStimer;
			// save
			fj_CVT.pFJglobal->gaRead.rw.read.HStimer = wHSNow;
			if ( wHSNow < wHSPrev )
			{
				if ( TRUE == bHi ) lHSwrap += 0x10000;
				bHi = FALSE;
			}
			else bHi = TRUE;
			NATotalTicks( &hi, &low );
		}
		fj_lHSClocksPerMillisecond = ((LONG)(lHSwrap + wHSNow - wHSPrev)) * BSP_TICKS_PER_SECOND / 1000;
	}

	return;
}
WORD16 w1=0;
WORD16 w2=0;
WORD16 ws=0xffff;
WORD16 wl=0;

//
// run everything
//
VOID fj_run(VOID)
{
	UINT    iOldPriority;
	BOOL    bLoop;

	tx_thread_priority_change(tx_thread_identify(), FJ_THREAD_LOW_PRIORITY, &iOldPriority);
	bLoop = TRUE;
	while ( TRUE == bLoop )
	{
		tx_thread_sleep(1);				// short delay
		if (fj_bStrokeWatchdog && FALSE == fj_CVT.pFJglobal->bReset )
		{
			(*NCC_GEN).swsr.reg = 0x5a;	// stroke the watchdog timer
			(*NCC_GEN).swsr.reg = 0xa5;	// stroke the watchdog timer
		}
		fj_print_head_Check();
										// short delay
		if ( TRUE == fj_rasterLoop ) tx_thread_sleep(1);
		fj_nameCheck();
										// short delay
		if ( TRUE == fj_rasterLoop ) tx_thread_sleep(1);
		fj_timeCheck();
										// short delay
		if ( TRUE == fj_rasterLoop ) tx_thread_sleep(1);
		fj_socketCheck();
										// short delay
		if ( TRUE == fj_rasterLoop ) tx_thread_sleep(1);
		fj_checkSerialPort();
										// short delay
		if ( TRUE == fj_rasterLoop ) tx_thread_sleep(1);

		if ( 0 == w1 )
		{
			w1 = fj_CVT.pGA->rw.read.HStimer;
		}
		else
		{
			w2 = fj_CVT.pGA->rw.read.HStimer;
			if ( w2 >= w1 )
			{
				WORD16 w3 = w2 - w1;
				if ( w3 < ws ) ws = w3;
				if ( w3 > wl ) wl = w3;
			}
			w1 = w2;
		}
	}
	return;
}
byte bm[] =								// FoxJet logo in dynimage/raster image format
{
	0x18,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x3C,0x00,0x00,0x00,
	0x7E,0x00,0x00,0x00,
	0x7E,0x00,0x00,0x00,
	0x7E,0x7F,0xFF,0x80,
	0x7E,0x7F,0xFF,0xC0,
	0x7E,0x7F,0xFF,0xE0,
	0x7E,0x7F,0xFF,0xE0,
	0xFF,0x7F,0xFF,0xF0,
	0xFF,0x7F,0xFF,0xF0,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xF8,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFC,
	0xFF,0x7F,0xFF,0xFE,
	0xFF,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x7F,0xFF,0xFE,
	0x7E,0x00,0x07,0xBE,
	0x7E,0x00,0x07,0xBE,
	0x7E,0x00,0x07,0xBE,
	0x3C,0x00,0x07,0xBE,
	0x3C,0x00,0x07,0xBF,
	0x3C,0x00,0x07,0xBF,
	0x18,0x00,0x07,0xBF,
	0x00,0x00,0x07,0x9F,
	0x00,0x00,0x07,0x9F,
	0x40,0x00,0x07,0x9F,
	0x40,0x00,0x0F,0x9F,
	0x64,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x7C,0x00,0x0F,0x9F,
	0x3E,0x00,0x0F,0x9F,
	0x3E,0x00,0x0F,0x9F,
	0x3E,0x00,0x1F,0x9F,
	0x3E,0x00,0x1F,0x80,
	0x3E,0x00,0x1F,0x80,
	0x3F,0x00,0x1F,0x80,
	0x3F,0x00,0x1F,0x80,
	0x1F,0x00,0x3F,0x80,
	0x1F,0x80,0x3F,0x80,
	0x1F,0x80,0x3F,0x80,
	0x1F,0xC0,0x7F,0x80,
	0x1F,0xE0,0xFF,0x80,
	0x0F,0xF1,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x0F,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x07,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x03,0xFF,0xFF,0x80,
	0x01,0xFF,0xFF,0x80,
	0x01,0xFF,0xFF,0x80,
	0x00,0xFF,0xEF,0x80,
	0x00,0xFF,0xEF,0x80,
	0x00,0x7F,0xCF,0x80,
	0x00,0x7F,0xCF,0x80,
	0x00,0x3F,0x8F,0x80,
	0x00,0x1F,0x0F,0x80,
	0x00,0x04,0x00,0x00,
	0x00,0x00,0x00,0x00
};

//
// small routine to send status during FPGA download
//
VOID fj_sendFPGAStatus( BYTE data, BYTE mask, LPSTR pName )
{
	CHAR  str[32];
	CHAR *pStr;

	if ( TRUE == fj_bootInfo )
	{
		strcpy( str, "FPGA " );
		strcat( str, pName );
		if ( 0 == (data & mask) ) pStr = " is 0\n";
		else                      pStr = " is 1\n";
		strcat( str, pStr );
		fj_writeInfo( str );
	}
	return;
}
//
//  test FPGA
//
BOOL fj_testFPGA( VOID )
{
	BOOL   bRet = TRUE;
	WORD8  wVersion;
	WORD16 a,b,c;
	int    i, j;

	fj_CVT.pGA->rw.read.version = 4096;	// just to put something illegal on the bus
										// get version id
	wVersion = fj_CVT.pGA->rw.read.version;
	if ( (wVersion < 1) || (wVersion > 255) )
	{
		bRet  = FALSE;					// not legal
	}
	else
	{
		bRet  = FALSE;					// assume this will fail
		fj_CVT.pGA->rw.read.HStimer = 4096;
		i = 0;
		while ( (i < 256) && (FALSE == bRet ) )
		{
			a = fj_CVT.pGA->rw.read.HStimer;
			if ( a != 0xffff )
			{
				j = 0;
				while ( (j < 256) && (FALSE == bRet ) )
				{
					b = fj_CVT.pGA->rw.read.HStimer;
					c = b - a;
					if ( 0 != c )
					{
										// if 1 to 8 microsecs, its a good counter
						if ( (0<c) && (c<9) ) bRet  = TRUE;
						else break;
					}
					j++;
				}
			}
			i++;
		}
	}
	return ( bRet );
}
//
// initialize everything
//
VOID fj_init(VOID)
{
	volatile    LPGA pga;
	volatile    WORD32 gaRead;
	volatile    WORD32 csar4;
	volatile    WORD32 csor4;
	volatile    WORD32 csar2;
	volatile    WORD32 csor2;
	volatile    BOOL bUseGA;
										// pointer to get rid of 'const' attribute
	LPFJ_CVT           pCVTrw = (LPFJ_CVT)&fj_CVT;
	LPFJ_GLOBAL        pGlobal = fj_CVT.pFJglobal;
	LPFJ_BRAM          pBRAM   = fj_CVT.pBRAM;
										// invalid pointer until set up
	LPFJSYSTEM         pfsys   = (LPFJSYSTEM)0xfffff000;
	LPFJSYSGROUPMEM    pfsm;
										// invalid pointer until set up
	LPFJPRINTHEAD      pfph    = (LPFJPRINTHEAD)0xfffff000;
	LPFJPRINTHEAD_OP   pfop;
	LPFJPRINTHEAD_PHY  pfphy;
	LPFJPRINTHEAD_PKG  pfpkg;
	LPFJMEMSTOR        pfjmem;
	LPFJIMAGE          pfi;
	LPCROMENTRY        pRomEntry;
	LPBYTE             pMemStorROM;
	LONG               lMemStorSize;
	devBoardParamsType nvParams;
	WORD32 *pwBootFlag = (WORD32 *)0x03ffc;
	struct tm tm;
	LONG    lRet;
	int     retromNet;
	int     retromFJ;
	int     i;
	int     j;
	STRING  str[80];
	LPSTR   pPassword;
	LPBYTE  pByte;
	LPBYTE  pGAByteH;
	LPBYTE  pGAByte;
	LPSTR   pGADate;
	LONG    lGALen;
	WORD32  w32GA;
	WORD16  w16GA;
	WORD8   w8GA;
	BYTE    port;
	BOOL    bLoaded;
	BOOL    bFileGood;

	ULONG   hMailRead;
	ULONG   hMailWrite;
	int     iMailStatus;
	//STRING  sMailMsg[] = "The FJ1703 IP Printer is at temperature and ready to print.\r\n.\r\n";
	int phy_addr;
	BOOL bInitBRAMprams = FALSE;

										// just for looking
	i = (*(NARM_MEM)).cs0.csor.bits.wait;
	i = (*NCC_GEN).pllcr.bits.pllcnt;	// just for looking

	// we need to turn on some circuitry that will prevent a memeory hang up in case of power problems.
	// the hardware logic uses port c3 to turn this on.
										// PORTC3 - 0 = GPIO mode
	(*(NARM_GEN)).portc.bits.mode &= ~0x08;
										// PORTC3 - 1 = output
	(*(NARM_GEN)).portc.bits.dir  |=  0x08;
										// PORTC3 - data = 1
	(*(NARM_GEN)).portc.bits.data |=  0x08;

	NALedGreenOff();					// turn off the green LED
	NALedRedOff();						// turn off the red/yellow LED

	pGlobal->fj_ftp_lRamBuffer = NULL;	// buffer for ftp download into ram
										// set from start of read until end of write into ROM

	fj_bKeepAlive = FALSE;
	fj_bStrokeWatchdog = TRUE;
	
	pGlobal->fj_ftp_bBufferInUse = FALSE;
	pGlobal->fj_ftp_lRamBufferSize = 0;

	// init some global values with info from NETsilicon
	// low address of ROM
	pGlobal->pROMLow  = (LPBYTE)((*(NARM_MEM)).cs0.csar.bits.base << 12);
	// size of ROM
	pGlobal->lROMSize = physical_size((*(NARM_MEM)).cs0.csor.bits.mask << 12);
	// high address of ROM
	pGlobal->pROMHigh = pGlobal->pROMLow + pGlobal->lROMSize - 1;
	// low address of RAM
	pGlobal->pRAMLow  = (LPBYTE)((*(NARM_MEM)).cs1.csar.bits.base << 12);
	// size of RAM
	pGlobal->lRAMSize = physical_size((*(NARM_MEM)).cs1.csor.bits.mask << 12);
	// high address of RAM
	pGlobal->pRAMHigh = pGlobal->pRAMLow + pGlobal->lRAMSize - 1;

	pGlobal->bPrint = FALSE;			// print the current message
	pGlobal->bPrintTestLoop = FALSE;	// buld the current message, but don't print it
	pGlobal->bSendDataReady = FALSE;	// send a DataReady status message
	pGlobal->bSendPhotocell = FALSE;	// send a Photocell status message
										// send a Photocell Detect message
	pGlobal->bSendPhotocellDetect = FALSE;
	pGlobal->bBuildFailure  = FALSE;	// set when a message can not be built between the photocell and the print head
	pGlobal->bBufferFailure = FALSE;	// set when a new ENI FIFO buffer can not be filled before the active one is finished
	pGlobal->bDataReady = FALSE;		// TRUE when data queued for printing
	pGlobal->bPhotocell = FALSE;		// TRUE when photocell detected
	pGlobal->bPhotocellInt = FALSE;		// TRUE when photocell interrupt wanted from GA. set when ENI is initialized.
										// enabled/disabled/test - test will print but not change date/time/counters
	pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
	pGlobal->bStatusReport = TRUE;
	pGlobal->bInitComplete = FALSE;		// FALSE until end of this routine
	pGlobal->bReset = FALSE;			// TRUE = reset the chip. stops the WatchDog strobe to cause a chip reset.
	pGlobal->bGAUpgrade = FALSE;		// TRUE = GA code has been upgrade. Need to notify everybody.
	pGlobal->bReady = FALSE;			// FALSE until everything ready
	pGlobal->bADReady = FALSE;			// FALSE until AD average values computed
	pGlobal->bPrintHeadPresent = FALSE;	// FALSE until print head detected
	pGlobal->bBatteryOK = FALSE;		// FALSE until battery reported OK by RTC chip
	pGlobal->bClockOK = FALSE;			// FALSE unless clock OK at poewr on - or clock set after poewr on
										// FALSE until shaft encoder is generating pulses
	pGlobal->bShaftEncoderRunning = FALSE;
	pGlobal->bHighVoltageOn = FALSE;	// FALSE until we want the high voltage on
	pGlobal->bStandByOn = FALSE;		// FALSE until user wants the StandBy mode on
	pGlobal->lHighVoltageWanted = 0;	// 0 until head detected
	pGlobal->lHighVoltageActual = 0;	// 0 until head detected
	pGlobal->lPWMHV = 0;				// 0 until head detected
	pGlobal->lBoardTemp = 0;			// Board temperature
	pGlobal->tmNewManu.tm_year = 0;		// new manufacture date
	pGlobal->tmNewInservice.tm_year = 0;// new inservice date
	pGlobal->tmGA.tm_year = 0;			// FPGA program date
										// save first memory that we can use
	pGlobal->pRAMUnused = &__ghsend_free_mem;
	pGlobal->pRAMUnused += 0x40000;		// NetSilicon uses more than they say
										// internal chip frequency for GEN and SER modules
	pGlobal->lXtalFreq = NAgetXtalFreq();
										// system clock value
	pGlobal->lCPUClockFreq = NAgetSysClkFreq();
	// gate array clock is divided by scale factor
	pGlobal->lGAClockFreq1 = pGlobal->lCPUClockFreq / GA_CLOCK1_SCALE;

	// the loader sets location 0x3ffc to a zero
	// it is incremented after this test
	// assuming that the ROM version of this app ran this far before the debugger took control,
	// a non-zero means that this app was loaded by the debugger
	pGlobal->bDebug = FALSE;			// TRUE = debug in RAM. FALSE = ROM.
	if ( 0 != *pwBootFlag )
	{
		pGlobal->bDebug = TRUE;
	}
	(*pwBootFlag)++;					// change it for next debug execution

	// do not use watchdog unless 50-1 bug is fixed
	//	if ( NETA50_1 >= (*(NARM_GEN)).ssr.bits.rev ) fj_watchDog = FALSE;
	// do not use watchdog if in debug mode
	if ( TRUE == pGlobal->bDebug ) fj_watchDog = FALSE;

	// start the watchdog timer
	if ( TRUE == fj_watchDog )
	{
		(*NCC_GEN).scr.bits.swt = 1;	// watchdog timer interval of approx 1 sec
		(*NCC_GEN).scr.bits.swt = 3;	// watchdog timer interval of approx 6 sec
		(*NCC_GEN).scr.bits.swri = 2;	// watchdog timer causes a RESET
		(*NCC_GEN).scr.bits.swe = 1;	// enable watchdog timer
	}

	// initialize chip select 2 for the battery backed up RAM
	// the wait states are set to 9 because -
	// board 1, 7 wait states failed and 8 wait states worked. this board had other problems.
	// board 2, 5 wait states failed and 6 wait states worked.
	// I was conservative and used 9 just in case other RAM chips have some variation.
	csar2 = (*(NARM_MEM)).cs2.csar.reg;
	csor2 = (*(NARM_MEM)).cs2.csor.reg;
	(*(NARM_MEM)).cs2.csar.reg = 0;
	(*(NARM_MEM)).cs2.csor.reg = 0;
										// base address
	(*(NARM_MEM)).cs2.csar.bits.base  = BRAM_BASE >> 12;
	//	(*(NARM_MEM)).cs2.csar.bits.dmode = 2;			// synchronous dram
										// internal dram multiplexer
	(*(NARM_MEM)).cs2.csar.bits.dmuxs = 0;
										// balanced ras/cas
	(*(NARM_MEM)).cs2.csar.bits.dmuxm = 0;
										// idle = 0
	(*(NARM_MEM)).cs2.csar.bits.idle  = 0;
										// dram select disabled
	(*(NARM_MEM)).cs2.csar.bits.drse  = 0;
										// no burst
	(*(NARM_MEM)).cs2.csar.bits.burst = 0;
										// not write protected
	(*(NARM_MEM)).cs2.csar.bits.wp    = 0;
										// 15 wait states
	(*(NARM_MEM)).cs2.csor.bits.wait  = 15;
										//  9 wait states
	(*(NARM_MEM)).cs2.csor.bits.wait  =  9;
										// 1 clock burst length
	(*(NARM_MEM)).cs2.csor.bits.bcyc  = 0;
										// 2 bus cycles in burst
	(*(NARM_MEM)).cs2.csor.bits.bsize = 0;
										//  8-bit port size
	(*(NARM_MEM)).cs2.csor.bits.ps    = 2;
										// dram read  - asynch
	(*(NARM_MEM)).cs2.csor.bits.rsync = 0;
										// dram write - asynch
	(*(NARM_MEM)).cs2.csor.bits.wsync = 0;
										// 512k
	(*(NARM_MEM)).cs2.csor.bits.mask  = 0xFFF80;
										// enable chip select
	(*(NARM_MEM)).cs2.csar.bits.v     = 1;
	csar2 = (*(NARM_MEM)).cs2.csar.reg;
	csor2 = (*(NARM_MEM)).cs2.csor.reg;

	// low address of BRAM
	pGlobal->pBRAMLow  = (LPBYTE)((*(NARM_MEM)).cs2.csar.bits.base << 12);
	// size of BRAM
	pGlobal->lBRAMSize = physical_size((*(NARM_MEM)).cs2.csor.bits.mask << 12);
	// high address of BRAM
	pGlobal->pBRAMHigh = pGlobal->pBRAMLow + pGlobal->lBRAMSize - 1;
	pGlobal->pBRAMUnused = pGlobal->lBRAMSize - sizeof(FJ_BRAM);

	if ( 0x5555AAAA == pBRAM->wID )
	{
		for ( i = 0; i < 16; i++ )
		{
			if ( pBRAM->w32Test[i] != i )
			{
				pBRAM->wID = 43;
				break;
			}
			if ( pBRAM->w8Test[i] != i )
			{
				pBRAM->wID = 43;
				break;
			}
		}
	}
	if ( 0x5555AAAA != pBRAM->wID )
	{
		memset( pBRAM, 0, pGlobal->lBRAMSize );
		pBRAM->wID = 0x5555AAAA;
		for ( i = 0; i < 16; i++ )
		{
			pBRAM->w32Test[i] = i;
			pBRAM->w8Test[i] = i;
		}
		pBRAM->lAMS_SST      = 0;		// seconds - time for next AMS to be scheduled
		pBRAM->lAMS_SST_wait = 0;		// seconds - time for next AMS to actually start, after inactive delay
		pBRAM->bAMS_wait     = FALSE;	// waiting for inactive period
		pBRAM->bAMS_on       = FALSE;	// AMS wanted on - relayed to GA in common subroutine
		pBRAM->lSTDB_SST     = 0;

		pBRAM->bInkLow = FALSE;			// ink low indicator. derived from A/D value.
		pBRAM->dInkLowML = 0.0;			// ink low ML counter
		pBRAM->bInkLowMax= FALSE;
		pBRAM->bPrintIdle= FALSE;
		// the following routine uses only the cvt pBRAM pointer
		fj_print_head_CountsResetAll();
		for ( i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++ )
		{
			DYNSEGMENT DynSeg = pBRAM->DynamicSegments[i];
			DynSeg.bChanged = FALSE;
		}
		fj_CVT.fj_ResetBRAMCounters( pBRAM );
		bInitBRAMprams = TRUE;
		fj_writeInfo( "Battery RAM initialized to zero\n" );
	}

	// set up CS 4 for the Gate Array
	pGlobal->bGAAvail = FALSE;
	if ( TRUE != fj_noPrintDev )		// if true, do not use any FoxJet chips and/or devices
	{
		// initialize chip select 4 for the gate array interface
		csar4 = (*(NARM_MEM)).cs4.csar.reg;
		csor4 = (*(NARM_MEM)).cs4.csor.reg;
		(*(NARM_MEM)).cs4.csar.reg = 0;
		(*(NARM_MEM)).cs4.csor.reg = 0;
										// base address
		(*(NARM_MEM)).cs4.csar.bits.base  = GA_BASE >> 12;
										// smallest addressable range = 4k
		(*(NARM_MEM)).cs4.csor.bits.mask  = 0xFFFFF;
										// 15 wait states
		(*(NARM_MEM)).cs4.csor.bits.wait  = 15;
										//  8-bit port size - for FoxJet board
		(*(NARM_MEM)).cs4.csor.bits.ps    = 2;
										// enable chip select
		(*(NARM_MEM)).cs4.csar.bits.v     = 1;
		csar4 = (*(NARM_MEM)).cs4.csar.reg;
		csor4 = (*(NARM_MEM)).cs4.csor.reg;
		pGlobal->bGAAvail = TRUE;
	}

	// all chip selects have been set up

	// some of the following routines use the cvt pointers to pfsys and pfph
	// we have to set them up next
	// an example is the FPGA download code that writes info message and therefore depends on switches in pfph

	// initialize FJ memory allocation
	fj_memory_init();

	// build a memstor for the system and printhead parameters
	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_PARMFJ;
										// get ROM address of message store
	pMemStorROM = pGlobal->pROMLow + pRomEntry->lOffset;
										// allow space for checksum
	lMemStorSize  = pRomEntry->lMaxLength - 4;
	pByte = fj_memory_getOnce( lMemStorSize );
	pfjmem = fj_MemStorNew( pByte, lMemStorSize );
										// copy structure with buffer pointer
	memcpy( fj_CVT.pFJMemStor, pfjmem, sizeof(FJMEMSTOR) );
	// we just wasted the allocated space for 1 FJMEMSTOR structure

	// alocate buffers for customer memstor
	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_FONTMSG;
										// allow space for checksum
	lMemStorSize  = pRomEntry->lMaxLength - 4;
	pGlobal->pMemStorBuffer     = fj_memory_getOnce( lMemStorSize );
	pGlobal->pMemStorBufferEdit = fj_memory_getOnce( lMemStorSize );
	memset( pGlobal->pMemStorBuffer,     0, lMemStorSize );
	memset( pGlobal->pMemStorBufferEdit, 0, lMemStorSize );

	// create global semaphores
	tx_semaphore_create( &(pGlobal->semaROMLock), "FJ ROM access lock", 1 );
	tx_semaphore_create( &(pGlobal->semaEditLock), "FJ Edit lock", 1 );
	tx_semaphore_create( &(pGlobal->semaPrintChainLock), "FJ Print Chain lock", 1 );

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	// check to see if there is a file to download to the FPGA
	// save the file date if we find it
	bFileGood = FALSE;

	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_GABIN;
	pGADate = NULL;
	pGAByte = NULL;
										// get ROM address of GA program
	pGAByteH = pGlobal->pROMLow + pRomEntry->lOffset;
	lGALen = pRomEntry->lMaxLength - 8;	// get max length of storage area
										// get length of file
	memcpy( &w32GA, (pGAByteH+lGALen), sizeof(w32GA) );
	lGALen = w32GA;
	pByte = pGAByteH;
	j = lGALen;
	w32GA = 0xFFFFFFFF;
	while ( 0 < j )						// search file for FF FF FF FF
	{
		if ( 0 == memcmp(pByte, &w32GA, 4) ) break;
		pByte++;
		j--;
	}
	if ( 0 < j )						// if FF FF FF FF found
	{
		pGAByte = pByte;				// set
		lGALen = j;						// set
		pByte = pGAByteH;
		pByte += 2+9+2;
		w32GA = lGALen;					// set default for FPGA compare
		while ( pByte < pGAByte )
		{
			w8GA = *pByte;
			if ( 0x63 == w8GA )
			{
										// save date pointer
				pGADate = (LPSTR)(pByte + 3);
			}
			if ( (0x61 == w8GA) || (0x62 == w8GA) || (0x63 == w8GA) || (0x64 == w8GA) )
			{
										// get length of string
				memcpy( &w16GA, (pByte+1), sizeof(w16GA) );
				pByte += 3 + w16GA;
			}
			else if ( 0x65 == w8GA )
			{
										// get length of FPGA data
				memcpy( &w32GA, (pByte+1), sizeof(w32GA) );
				pByte += 5;
			}
		}
		if ( w32GA == lGALen )
		{
			bFileGood = TRUE;
		}
	}

#define GA_WRITE    0x01
#define GA_INIT     0x02
#define GA_DONE     0x04
#define GA_PROGRAM  0x40
	// load the GA program
	if ( TRUE == pGlobal->bGAAvail )
	{
		bLoaded = TRUE;
										// set GPIO mode
		(*(NARM_GEN)).portb.bits.mode &= ~GA_INIT;
										// set GPIO mode
		(*(NARM_GEN)).portb.bits.mode &= ~GA_DONE;
										// INIT    as input
		(*(NARM_GEN)).portb.bits.dir  &= ~GA_INIT;
										// DONE    as intput
		(*(NARM_GEN)).portb.bits.dir  &= ~GA_DONE;
		port = (*(NARM_GEN)).portb.bits.data;
		if ( 0 == (port & GA_DONE)  ) bLoaded = FALSE;
		if ( FALSE == fj_testFPGA() ) bLoaded = FALSE;

		// temp to force the FPGA load everytime
		//bLoaded = FALSE;

		if ( FALSE == bLoaded )
		{
			if ( TRUE == bFileGood )
			{
				BOOL bSuccess = FALSE;
				// set GPIO mode
				(*(NARM_GEN)).portb.bits.mode &= ~GA_PROGRAM;
										// set GPIO mode
				(*(NARM_GEN)).portb.bits.mode &= ~GA_WRITE;

										// set WRITE   high before we set the line to output
				(*(NARM_GEN)).portb.bits.data |= GA_WRITE;
										// WRITE   as output
				(*(NARM_GEN)).portb.bits.dir  |= GA_WRITE;
										// set WRITE   high again
				(*(NARM_GEN)).portb.bits.data |= GA_WRITE;
				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA WRITE   set high\n" );

										// set PROGRAM high before we set the line to output
				(*(NARM_GEN)).portb.bits.data |= GA_PROGRAM;
										// PROGRAM as output
				(*(NARM_GEN)).portb.bits.dir  |= GA_PROGRAM;
										// set PROGRAM high again
				(*(NARM_GEN)).portb.bits.data |= GA_PROGRAM;
				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA PROGRAM set high\n" );

				port = (*(NARM_GEN)).portb.bits.data;
				fj_sendFPGAStatus( port, GA_DONE, "DONE   " );
				fj_sendFPGAStatus( port, GA_INIT, "INIT   " );

				// set PROGRAM low and wait for INIT to go low
				if ( TRUE == fj_bootInfo ) { fj_writeInfo( "FPGA PROGRAM set low\n" ); }
				// set PROGRAM low
				(*(NARM_GEN)).portb.bits.data &= ~GA_PROGRAM;
				i = 1400000;			// about a second on the -40
				while ( 0 < i )
				{
					port = (*(NARM_GEN)).portb.bits.data;
					if ( 0 == (port & GA_INIT) ) break;
					i--;
				}
				fj_sendFPGAStatus( port, GA_DONE, "DONE   " );
				fj_sendFPGAStatus( port, GA_INIT, "INIT   " );

				// if INIT went low
				if ( 0 == (port & GA_INIT) )
				{
					// set PROGRAM high and wait for INIT to go high
					// INIT goes high after FPGA memory is cleared
					if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA PROGRAM set high\n" );
					// set PROGRAM high
					(*(NARM_GEN)).portb.bits.data |=  GA_PROGRAM;
					i = 1400000;		// about a second on the -40
					while ( 0 < i )
					{
						port = (*(NARM_GEN)).portb.bits.data;
						if ( 0 != (port & GA_INIT) ) break;
						i--;
					}
					fj_sendFPGAStatus( port, GA_DONE, "DONE   " );
					fj_sendFPGAStatus( port, GA_INIT, "INIT   " );

					// if INIT went high
					if ( 0 != (port & GA_INIT) )
					{
						// set write low and program the chip
						if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA WRITE   set low\n" );
						// set WRITE low
						(*(NARM_GEN)).portb.bits.data &= ~GA_WRITE;
						i = 0;
						pByte = pGAByte;
						while ( i < lGALen )
						{
							*((LPBYTE)GA_BASE) = *pByte;
							pByte++;
							i++;
							port = (*(NARM_GEN)).portb.bits.data;
							if ( 0 == (port & GA_INIT) )
							{
								j = 43;
								//				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA INIT    went low during write - abort\n" );
								break;	// if INIT went low, error
							}
							if ( 0 != (port & GA_DONE) )
							{
								j = 43;
								//				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA PROGRAM went high during write - complete\n" );
								//				break;				// if DONE went high, finished
							}
						}
						if ( TRUE == fj_bootInfo ) { sprintf( str, "FPGA %i bytes written\n", (int)i ); fj_writeInfo( str ); }

						if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA WRITE   set high\n" );
						// set WRITE   high
						(*(NARM_GEN)).portb.bits.data |=  GA_WRITE;

						j = 1400000;	// about a second on the -40
						while ( 0 < j )
						{
							port = (*(NARM_GEN)).portb.bits.data;
							if ( 0 == (port & GA_INIT) ) break;
							j--;
						}

						port = (*(NARM_GEN)).portb.bits.data;
						fj_sendFPGAStatus( port, GA_DONE, "DONE   " );
						fj_sendFPGAStatus( port, GA_INIT, "INIT   " );
										// if DONE went high
						if ( 0 != (port & GA_DONE) )
						{
										// if INIT stayed high
							if ( 0 != (port & GA_INIT) )
							{
										// FPGA is succesfully programmed
								bSuccess = TRUE;
							}
						}
					}
				}

				if ( FALSE == bSuccess )
				{
					if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA error - not loaded\n" );
					// set PROGRAM low
					(*(NARM_GEN)).portb.bits.data &= ~GA_PROGRAM;
					// set PROGRAM high
					(*(NARM_GEN)).portb.bits.data |=  GA_PROGRAM;
					if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA PROGRAM set low then high\n" );
				}
				else
				{
					if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA successfully loaded\n" );
				}

				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA WRITE   set high\n" );
										// set WRITE   high
				(*(NARM_GEN)).portb.bits.data |=  GA_WRITE;
				// PROGRAM as input so it cannot be written again
				(*(NARM_GEN)).portb.bits.dir  &= ~GA_PROGRAM;
										// WRITE   as input so it cannot be written again
				(*(NARM_GEN)).portb.bits.dir  &= ~GA_WRITE;
			}
			else
			{
				if ( TRUE == fj_bootInfo ) fj_writeInfo( "FPGA file nor found or not valid\n" );
			}
		}
	}

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	if ( TRUE == pGlobal->bGAAvail )
	{
		pGlobal->bGAAvail = fj_testFPGA();
	}
	if ( FALSE == pGlobal->bGAAvail )
	{
		(LPGA)pCVTrw->pGA = (LPGA)calloc(1, sizeof(GA));
		pGlobal->lGASeconds = 0;
		tm.tm_year = 102;				// 2002
		tm.tm_mon  = 0;					// January
		tm.tm_mday = 1;					// 1st
		tm.tm_hour = 0;					// hh
		tm.tm_min  = 0;					// mm
		tm.tm_sec  = 0;					// ss
		tm.tm_wday = 0;					// set to 0
		fj_mktime( &tm );				// normalize everyting - DST etc.
										// set into saved clock
		fj_print_head_SetRTClockTM( &tm );
	}
	// initialize the gate array memory
	pga = fj_CVT.pGA;					// get base address
	// for FoxJet board
	pga->rw.write.curve1     = 0;		// curve 1 time on  - in clock1 units
	pga->rw.write.curve2     = 0;		// curve 2 time off - in clock1 units
	pga->rw.write.curve3     = 0;		// curve 3 time on  - in clock1 units
	pga->rw.write.encoder    = 0;		// encoder scale = clock1 / frequency wanted.  0 = off.
	pga->rw.write.vibration  = 0;		// vibration value = clock1 / frequency wanted.  0 = off.
	pga->rw.write.vib_curve  = 0;		// vibration curve time on  - in clock1 units
	pga->rw.write.pwmHV      = 0;		// pwm scale factor.
	fj_print_head_SetGAWriteSwitches();
	memset( &(pGlobal->gaRead),  0, sizeof(GA) );
	memset( &(pGlobal->gaWrite), 0, sizeof(GA) );

	// save
	fj_CVT.pFJglobal->gaRead.rw.read.version = fj_CVT.pGA->rw.read.version;

	// set RTC into the correct mode
	// Oscillator on | no dividers | Bank 1
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_A;
	pga->rw.write.RTclockData = FJ_RT_REGISTER_A_DV1 | FJ_RT_REGISTER_A_DV0;
	// SET | DataBinary | 24-hour | no daylight savings | no interrupts
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_B;
	pga->rw.write.RTclockData = FJ_RT_REGISTER_B_SET | FJ_RT_REGISTER_B_DATA_B | FJ_RT_REGISTER_B_24;
	// Clear extended register 4A
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_4A;
	pga->rw.write.RTclockData = 0;
	// Set Crystal Select for 12.5 pF capacitance
	pga->rw.write.RTclockAdd  = FJ_RT_REGISTER_4B;
	pga->rw.write.RTclockData = FJ_RT_REGISTER_4B_CS;

	fj_print_head_GetRTClockBattery();
	if( FALSE == pGlobal->bBatteryOK )	// if battery went bad
	{
										// force a bad century of 0
		pga->rw.write.RTclockAdd  = FJ_RT_CENTURY;
		pga->rw.write.RTclockData = 0;
		fj_writeInfo( "Battery is bad\n" );
	}

	// set lock to avoid error message
	tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	// the param routines need the ROM address that was just set into the GLOBAL area
	// read NETsilicon parameters
	// NOTE: this uses a modified NetSilicon routine to get parameters possibly changed by the Dialog()
	retromNet = NAReadDevBoardParams( &nvParams );
	// read FoxJet NV parameters
	retromFJ = fj_ReadFJRomParams( fj_CVT.pFJparam );
	tx_semaphore_put( &(pGlobal->semaROMLock) );

	if ( TRUE == bInitBRAMprams )
	{
		memcpy( &(pBRAM->nvParamsBRAM), &nvParams, sizeof(devBoardParamsType) );
	}
	if ( FALSE == bDialogParamChange )
	{
		if (0 != memcmp( &nvParams, &(pBRAM->nvParamsBRAM), sizeof(devBoardParamsType)) )
		{
			memcpy( &nvParams, &(pBRAM->nvParamsBRAM), sizeof(devBoardParamsType) );
			NAWriteDevBoardParams( &nvParams );
		}
	}


	// one of the two above routines destroys optimized register settings.
	// reload some pointers
	pGlobal = fj_CVT.pFJglobal;			// set local pointer

	if ( 0 == nvParams.useDhcp )
	{
										// get IP address
		pGlobal->ipaddress          = nvParams.ipAddress;
										// get subnet mask
		pGlobal->ipsubnet           = nvParams.subnetMask;
										// get default gateway
		pGlobal->ipdefgateway       = nvParams.gateway;
	}
	else
	{
										// get IP address
		pGlobal->ipaddress          = ldhcpAddr;
										// get subnet mask
		pGlobal->ipsubnet           = ldhcpMask;
										// get default gateway
		pGlobal->ipdefgateway       = ldhcpGateway;
	}
										// get IP address for loopback
	pGlobal->iploopback         = 0x7f000001;
										// NETsilicon bug
	pGlobal->iploopback         = pGlobal->ipaddress;
										// get subnet broadcast address
	pGlobal->ipbroadcastsubnet  = 0xffffffff;
	// get net broadcast address
	pGlobal->ipbroadcastnet     = pGlobal->ipaddress | (~pGlobal->ipsubnet);

	// save MAC address
	NASerialnum_to_mac( nvParams.serialNumber, (char*)pGlobal->macaddr );
	NAInitEthAddress ( (char*)pGlobal->macaddr );

	// copy FJSYSTEM from parameters into RAM
	pfsys = fj_CVT.pfsys;
	memcpy( pfsys, &(fj_CVT.pFJparam->FJSys ), sizeof(FJSYSTEM) );
	//  set manufacturing password
	strcpy( pfsys->aPassword[FJSYS_PRIORITY_MANUFACTURING], "71043bob68" );

	// copy FJPRINTHEAD from parameters into RAM
	pfph  = fj_CVT.pfph;
	pfop  = pfph->pfop;
	pfphy = pfph->pfphy;
	pfpkg = pfph->pfpkg;
	memcpy( pfop,  &(fj_CVT.pFJparam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
	memcpy( pfphy, &(fj_CVT.pFJparam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
	memcpy( pfpkg, &(fj_CVT.pFJparam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );
	memcpy( pfph,  &(fj_CVT.pFJparam->FJPh   ), sizeof(FJPRINTHEAD    ) );
	pfph->pfop  = pfop;
	pfph->pfphy = pfphy;
	pfph->pfpkg = pfpkg;

	// link the FJPRINTHEAD to the FJSYSTEM
	pfph->pfsys = pfsys;

	pfph->lIPaddr = nvParams.ipAddress;	// get IP address
	if ( 0 == *(pfsys->strID) ) strcpy( pfsys->strID, pfph->strID );

	fj_PrintHeadReset( pfph );

	// this must be done here in case the serial number was changed by the boot dialog.
	// convert serial number
	fj_NETtoFJSerial( nvParams.serialNumber, pfph->strSerial );
	pfph->lSerial = atoi(pfph->strSerial);
	fj_setID();

	pfph->bEdit = FALSE;

	// add print head to group
	// temp until we add a group to permanent storage
	pfsys->lActiveCount = 0;
	pfsm  = &(pfsys->gmMembers[pfsys->lActiveCount]);
	pfsys->lActiveCount++;
	pfsm->pfph = pfph;
	strcpy( pfsm->strID, pfph->strID );
	pfsm->lIPaddr = pfph->lIPaddr;
	// need to set group number here. default is zero - correct for one head system.

	pfph->lEditID      = 0;				// IP address of connection with lock

	// pfsys and pfph are now set up with info from the system memstor in ROM

	// redo serial port according to pfph
	fj_closeSerialPort();
	fj_openSerialPort( pfph );

	// now that clock is running
	// and now that pfsys and pfph are set up
	// parse any saved date for the FPGA
	if ( NULL != pGADate )
	{
		struct tm *pTM;
		strcpy( str, pGADate );
		pTM = fj_SystemParseDateTime( str, &tm );
		if ( (NULL != pTM) && ( 100 < tm.tm_year)  )
		{
			// another thread will update ROM
			memcpy( &(pGlobal->tmFPGAProgram), pTM, sizeof(struct tm) );
		}
	}

	// get the time once
	fj_SystemGetTime( &tm );
	fj_SystemGetTime( &(pfsys->tmPhotoCell) );

	// create the seconds timer
	lRet = tx_timer_create( &fj_timerSeconds,
		"seconds",
		fj_timerSecondsFunc,
		0x234,							//id
		1*BSP_TICKS_PER_SECOND,
		1*BSP_TICKS_PER_SECOND,
		TX_AUTO_ACTIVATE );

	// create pfi for Dynamic Image
	pfi = fj_ImageNew( sizeof(bm) );
	pfph->pfiDynimage = pfi;
	if ( NULL != pfi )
	{
		pfi->lHeight  = 32;				// height of bitmap in pixels
		memcpy( (LPBYTE)pfi + sizeof(FJIMAGE), bm, pfi->lLength );
	}

	// allocate buffer for web page logo
	pRomEntry = *(fj_CVT.pfj_RomTable) + ROM_LOGOJPG;
	pGlobal->pLogo       = fj_memory_getOnce( pRomEntry->lMaxLength );
	pGlobal->lLogoSize   = 1;
	pGlobal->lLogoWidth  = 160;
	pGlobal->lLogoHeight = 60;
										// get ROM address of logo
	pByte = fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset;
	if ( (0xff == *pByte) && (0xd8 == *(pByte+1)) )
	{
		pByte += pRomEntry->lMaxLength - 4;
		i = *((LONG *)(pByte-4));		// get size
		if ( (0<i) && (i<pRomEntry->lMaxLength) )
		{
			pGlobal->lLogoSize = i;
										// get width
			pGlobal->lLogoWidth  = *((LONG *)(pByte-8));
										// get height
			pGlobal->lLogoHeight = *((LONG *)(pByte-12));
			memcpy( pGlobal->pLogo, fj_CVT.pFJglobal->pROMLow + pRomEntry->lOffset, i );
		}
	}

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	strcpy(str,"FoxJet started. V");
	strcat(str,fj_getVersionNumber());
	strcat(str," ");
	strcat(str,fj_getVersionDate());
	strcat(str,"\n");
	fj_writeInfo(str);

	fj_writeInfo("\nActual IP values being used - \n");
	sprintf( str, "IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipaddress));
	fj_writeInfo(str);
	sprintf( str, "Subnet mask = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipsubnet));
	fj_writeInfo(str);
	sprintf( str, "Def Gateway = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipdefgateway));
	fj_writeInfo(str);
	sprintf( str, "Network broadcast IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipbroadcastnet));
	fj_writeInfo(str);
	sprintf( str, "Sub Network broadcast IP address  = %d.%d.%d.%d\n",PUSH_IPADDR(pGlobal->ipbroadcastsubnet));
	fj_writeInfo(str);

	fj_writeInfo("\nLast Reset - ");
	if ( TRUE == pGlobal->bDebug ) fj_writeInfo("Debug\n");
	else
	{
		if ( 0 != (*NCC_GEN).ssr.bits.ext ) fj_writeInfo("External Reset\n");
		if ( 0 != (*NCC_GEN).ssr.bits.pow ) fj_writeInfo("Power On\n");
		if ( 0 != (*NCC_GEN).ssr.bits.sw  ) fj_writeInfo("Watch-Dog Timer\n");
	}

	for ( i = BOOT_NUMBER-1; i > 0; i-- )
	{
		memcpy( &(pBRAM->aBoots[i]), &(pBRAM->aBoots[i-1]), sizeof(BOOTINFO) );
	}
	memcpy( &(pBRAM->aBoots[0].tmBoot), &(tm), sizeof(struct tm) );
	pBRAM->aBoots[0].bBatt = pGlobal->bBatteryOK;
	pBRAM->aBoots[0].bClock = ( 100 < tm.tm_year );
	if ( TRUE == pGlobal->bDebug ) pBRAM->aBoots[0].lReason = BOOT_REASON_DEBUG;
	else
	{
		if ( 0 != (*NCC_GEN).ssr.bits.ext ) pBRAM->aBoots[0].lReason = BOOT_REASON_EXTERAL;
		if ( 0 != (*NCC_GEN).ssr.bits.sw  ) pBRAM->aBoots[0].lReason = BOOT_REASON_WATCHDOG;
	}

	i = mii_check_speed();
	fj_writeInfo("\nEthernet speed is ");
	if      ( 0 == i ) fj_writeInfo("10Mbit\n");
	else if ( 1 == i ) fj_writeInfo("100Mbit\n");
	else               fj_writeInfo("unknown\n");
	i = mii_check_duplex();
	fj_writeInfo("Ethernet mode is ");
	if      ( 0 == i ) fj_writeInfo("half duplex\n");
	else if ( 1 == i ) fj_writeInfo("full duplex\n");
	else               fj_writeInfo("unknown\n");

	// the following code is copied from NetSilicon
	// ask chip to do something
	//(*NCC_EFE).miiar.bits.madr  = 0x401;
	phy_addr = 0x100;					/* assume address 1 change if different on your board */
	narm_write_reg (NARM_EMAR_REG, NARM_EMAR_ADDR, madr, phy_addr + 0x001);
	//(*NCC_EFE).miicr.bits.rstat = 1;
	narm_write_reg (NARM_EMIC_REG, NARM_EMIC_ADDR, rstat, 1);
	// wait for the chip to do it
	mii_poll_busy();
	// read the result
	fj_writeInfo("Ethernet link is ");
	if ((narm_read_reg (NARM_EMRD_REG, NARM_EMRD_ADDR, mrdd) & 0x0004) == 0)
		fj_writeInfo("DOWN\n");
	//	if (((*NCC_EFE).miirdr.bits.mrdd & 0x0004) == 0) fj_writeInfo("DOWN\n");
	else fj_writeInfo("UP\n");

	fj_romInit();						// create queue and thread for writing ROM

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	// check for good read of ROM
	if ( 0 != retromFJ )
	{
		if ( (0 == fj_CVT.pfph->tmDateManu.tm_year) && (0 == fj_CVT.pfph->tmDateInservice.tm_year) )
		{
			// assume first start up and write out default info with good checksum
			// set lock to avoid error message
			tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
			// write and free lock
			fj_WriteFJRomParams( fj_CVT.pFJparam, TRUE, NULL, 0 );
		}
	}

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	// initialize everything
	fj_socketInit();
	fj_timeInit();
	fj_print_head_InitEmail();
	fj_print_head_Init();
	fj_nameInit();

	fj_SystemGetTime( &tm );

	// schedule and AMS start from now.
	// a design needs to be done to figure out how/when to schedule AMS after power up.
	fj_print_head_ScheduleAMS();

	// start the raster loop thread and listen threads
	thr_raster.tx_thread_id = 0;
	fj_startThreads();

	// set lock to avoid error message
	tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	// before we start everything, test to see if the NETOS parameters have changed
	// if so, write them to ROM
	if( 0 != retromNet )				// if we had to use the default parameters,
	{
										//    call the old routine to move data to fj_nvparams
		NAWriteDevBoardParams( &nvParams );
	}
	if( TRUE == fj_bNetosParamsChanged )
	{
		// write and free ROM lock
		fj_WriteNETOSRomParams( fj_CVT.pNETOSparam, TRUE, NULL, 0 );
		// wait for ROM write to complete so we can read it back again
		tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	}
	fj_ReadNETOSRomParams( fj_CVT.pNETOSparam );
	tx_semaphore_put( &(pGlobal->semaROMLock) );

	// initialize HTML server
	if ( (FJSYS_INTERNET_FULL == pfsys->cInternetControl) || (FJSYS_INTERNET_HTML_FTP == pfsys->cInternetControl) )
	{
		HSRegisterSearchFunction(fj_AppSearchURL, fj_AppPreprocessURL);
		//		HSProperties ( "HSV", HS_NOT_DEFINED, 8444, 8888, HS_NOT_DEFINED, HS_NOT_DEFINED );
		HSProperties ( "HSV", 30, 8444, 8888, HS_NOT_DEFINED, HS_NOT_DEFINED );
		HSStartServer();
	}

	// set up FTP server for possible download
	// ftp will use a socket slot, so this must come after fj_socketInit
	fj_ftp_init();
	fj_print_head_ScheduleStandBy();

	pGlobal->bInitComplete = TRUE;		// FALSE until end of this routine
	fj_writeInfo("Init complete\n");
	fj_writeInfo("\n");

	//	fj_print_head_SendEmailService( sMailMsg, "Service: up and running" );

	if (fj_bStrokeWatchdog) {
	    (*NCC_GEN).swsr.reg = 0x5a;			// stroke the watchdog timer
	    (*NCC_GEN).swsr.reg = 0xa5;			// stroke the watchdog timer
	}
	
	// loop to keep everything running
	NACacheSetDefaults();				/* turn cache on*/
	fj_run();

	return;
}
