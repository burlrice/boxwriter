/*
 *            Copyright (c) 1998-2000 by NETsilicon Inc.
 *
 *  This software is copyrighted by and is the sole property of
 *  NETsilicon.  All rights, title, ownership, or other interests
 *  in the software remain the property of NETsilicon.  This
 *  software may only be used in accordance with the corresponding
 *  license agreement.  Any unauthorized use, duplication, transmission,
 *  distribution, or disclosure of this software is expressly forbidden.
 *
 *  This Copyright notice may not be removed or modified without prior
 *  written consent of NETsilicon.
 *
 *  NETsilicon, reserves the right to modify this software
 *  without notice.
 *
 *  NETsilicon
 *  411 Waverley Oaks Road                  USA 781.647.1234
 *  Suite 227                               http://www.netsilicon.com
 *  Waltham, MA 02452                       AmericaSales@netsilicon.com
 *
 *************************************************************************
 *
 *     Module Name: ftpsvr.h
 *         Version: 1.00
 *   Original Date:  5-Aug-1999
 *          Author:
 *        Language: Ansi C
 * Compile Options:
 * Compile defines:
 *       Libraries:
 *    Link Options:
 *
 *    Entry Points:
 *
 * Description.
 * =======================================================================
 * This file contains definitions of constants and structures
 * used by the NET+ARM FTP Server.
 *
 *
 * Edit Date/Ver   Edit Description
 * ==============  ===================================================
 *
 *
 */

#include <narmerr.h>

/*
 * There is some macro for m_flags in VxWorks which interferes with our structure definition.  I don't
 * know where it is, but for know, just remove it.
 */
#ifdef m_flags
#undef m_flags
#endif

/*
 * Definitions
 */

#ifdef NET_OS
typedef void * (*FUNCPTR) (void *);
#endif

#define  UC(b)  (((int)b)&0xff)

#define FTP_SERVER_PORT     (21)
#define FTP_DATA_PORT       (20)
#define FTP_PI_BUF_SIZE   (1024)
#define FTP_MAXARGLEN      (256)

#define FTP_CLIENT_BASE   (6000)		/* Used for allocating client connection port numbers. */

/* 
 * User states
 */

#define FTP_IDLE                (0x00)	/* idle, no user connected */
#define FTP_PASSWORD            (0x01)	/* got user, waiting for passwd */
#define FTP_PROCESS_CMD         (0x02)	/* server pi is ready to process cmds */
#define FTP_REL_DATA_CONN       (0x08)	/* releasing conn after retr/lst cmd */
#define FTP_PROCESS_STOR       (0x100)	/* processing stor cmd */
#define FTP_PROCESS_RETR       (0x200)	/* processing retr cmd */
#define FTP_PROCESS_LST        (0x400)	/* processing list or nlst cmd */
#define FTP_PASV_DATA          (0x800)	/* waiting on the client to connect the data socket */
										/* waiting for connect to complete */
#define FTP_WAITING_FOR_DATA_CONN (0x900)

/* 
 * FTP Data routine types
 */

#define FTP_CMD_NONE     (0)
#define FTP_CMD_STOR     (1)
#define FTP_CMD_RETR     (2)
#define FTP_CMD_LIST     (3)
#define FTP_CMD_NLST     (4)
#define FTP_CMD_CWD      (5)
#define FTP_CMD_SYST     (6)
#define FTP_CMD_PWD      (7)

/*
 * FTP Timer interval
 */
#if defined PSOS_OS || defined NET_OS
#define FTP_TIME_SLICE   (500)			/* 5 seconds 100 ticks equals 1 second */
#else
#define FTP_TIME_SLICE   5
#endif

/*
 * Structure Definitions
 */

typedef struct ftp_user_info
{
	int state;							/* state of the user. */
	int cntlsocket;						/* Socket of accepted user control connection. */
	int cntlstate;						/* State of control socket connection. */
	int datasocket;						/* Socket used for transferring data. */
	unsigned long cntl_ip_addr;			/* IP address of client. */
	unsigned long data_ip_addr;			/* Specifies remote data IP address. */
	int dataport;						/* Specifies data port. */
	int localport;						/* Specifies local port of data connection. */
	int active;							/* Specifies if the data connection active or passive. */
	int timeout;						/* In activity timeout in seconds. */
	int sequenceno;
	/* For calling certain user routines this is a count
							of howmany times the routine has been called. */

	char username[FS_MAXUSERLEN];
	char password[FS_MAXUSERLEN];
	char curarg[FTP_MAXARGLEN];			/* Holds parameter for current command. */

	unsigned char connected;			/* True if connected */
	int      cmd;						/* Save command while connecting */

} ftp_user_info_t;

typedef struct ftp_user_table
{
	char username[FS_MAXUSERLEN];
	char password[FS_MAXUSERLEN];
} ftp_user_table_t;

typedef struct ftp_config_info
{
	char m_name[4];						/* Task name of FTP server. */
	int m_majversion;					/* Version number of FTP server. */
	int m_minversion;					/* Minor number of version. */
	unsigned long m_tid;				/* Task identifier of FTP server. */
	int m_waitorg;						/* Specifies timeout for select call in the main server thread. */
	int m_waittime;						/* Specifies current timeout for select call in the main server thread. */
	int m_cltimeout;					/* Specifies timeout in seconds for client connections. */

	int m_maxusers;						/* Specifies number of concurrent connections. */

	unsigned long m_priority;			/* Task priority of the FTP server task. */
	unsigned long m_sysstack;			/* Size of system stack. */
	unsigned long m_usrstack;			/* Size of user stack */
	unsigned long m_mode;				/* Mode setting for task start */
	unsigned long m_flags;				/* Flags for task creation */
	int m_started;						/* Specifies if FTP server task has started */
	ftp_user_table_t *m_usertablep;		/* Specifies list of possible users */

	int m_curusers;						/* Specifies current number of active users. */
	ftp_user_info_t *m_usersp;			/* Pointer to user structures. */
	char         *m_datap;				/* Buffer for receiving data. */
	int           m_size;				/* Size of data buffer. */
	unsigned long m_timer;				/* Hold FTP timer identifier. */
	unsigned long m_lasterror;			/* Holds last error information */
	int           m_localport;			/* Starting port for local ports for data connections. */
										/* Holds user STOR routine. */
	int (*m_usr_storp)(char *, int, char *, unsigned long);
										/* Holds user RETR routine. */
	int (*m_usr_retrp)(char *, unsigned long);
										/* Hold user LIST routine. */
	int (*m_usr_listp)(char *, unsigned long);
	int (*m_usr_nlstp)(char *, unsigned long);
	int (*m_usr_pwdp) (char *, unsigned long, char *);
	int (*m_usr_systp)(char *, char *, unsigned long);
	int (*m_usr_helpp)(char *, unsigned long);
	int (*m_usr_typep)(char *, unsigned long);
	int (*m_usr_aborp)(char *, unsigned long);
	int (*m_usr_acctp)(char *, unsigned long);
	int (*m_usr_allop)(char *, unsigned long);
	int (*m_usr_appep)(char *, unsigned long);
	int (*m_usr_cdupp)(char *, unsigned long);
	int (*m_usr_cwdp) (char *, unsigned long);
	int (*m_usr_delep)(char *, unsigned long);
	int (*m_usr_mkdp) (char *, unsigned long);
	int (*m_usr_modep)(char *, unsigned long);
	int (*m_usr_noopp)(char *, unsigned long);
	int (*m_usr_reinp)(char *, unsigned long);
	int (*m_usr_restp)(char *, unsigned long);
	int (*m_usr_rmdp) (char *, unsigned long);
	int (*m_usr_rnfrp)(char *, unsigned long);
	int (*m_usr_rntop)(char *, unsigned long);
	int (*m_usr_sitep)(char *, unsigned long);
	int (*m_usr_smntp)(char *, unsigned long);
	int (*m_usr_statp)(char *, unsigned long);
	int (*m_usr_stoup)(char *, unsigned long);
	int (*m_usr_strup)(char *, unsigned long);
										/* Data close callback */
	int (*m_usr_dresetp)(char *, unsigned long);
										/* Control close callback */
	int (*m_usr_cresetp)(char *, unsigned long);
										/* User/Password Validation */
	int (*m_usr_validationp)(char *, char *);
} ftp_config_info_t;

/*
 * FTP Server error codes
 *
 */

#define FTPS_ERRBASE   (0x1000)
#define FTPS_NOTIMER   (0x1001)			/* Could not initialize a count down timer. */
#define FTPS_EVENTERR  (0x1002)			/* A failure occurred checking the timer event. */
#define FTPS_BADSOCKET (0x1003)			/* A socket creation failure occurred. */
#define FTPS_NONBLOCK  (0x1004)			/* A failure occurred setting the socket in no-block mode. */
#define FTPS_SOCKETOPT (0x1005)			/* A failure occurred setting a socket option. */
#define FTPS_BINDERR   (0x1006)			/* A failure occurred binding a socket. */
#define FTPS_LINGERERR (0x1007)			/* The LINGER option could not be set properly. */
#define FTPS_TCPMSL    (0x1008)			/* The TCP_MSL option could not be set properly. */

int naftp_init_select (fd_set *read_maskp, fd_set *except_maskp,fd_set *write_maskp);
int naftp_data_connections ();
int naftp_resolve_user (fd_set *readmaskp, fd_set *exceptmaskp, fd_set *writemaskp);
int naftp_prepare_user (int client_socket);
int naftp_send_reply(int fd, int code, char *info);
int naftp_parse (int userindex);
int naftp_cmd_cwd (ftp_user_info_t *userp);
int naftp_cmd_syst (ftp_user_info_t *userp);
int naftp_cmd_pwd (ftp_user_info_t *userp);
