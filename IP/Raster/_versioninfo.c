/* C file: c:\MyPrograms\FoxJet1\_versioninfo.c created
 by NET+Works HTML to C Utility on Friday, May 12, 2000 14:06:24 */
#include "fj.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_get_version_info */
static void na_get_version_info (unsigned long handle);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_ID: */
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\versioninfo.htm {{ */
void Send_function_2(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>Version Info</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Version Info\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "\n");
	HSSend (handle, "<p>");
	na_get_version_info (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\versioninfo.htm }} */
/* @! End HTML page   */

/* @! Begin of dynamic functions */
/* @! Function: na_get_version_info  {{ */
void na_get_version_info(unsigned long handle)
{
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table width='90%' cellpadding='4' border='1' bgcolor='#f5f5f5'>\n");
	HSSend (handle, "<thead><tr><td><b>#</b></td><td align=center><b>Date</b></td><td><b>Comment</b></td></tr></thead>\n");
	HSSend (handle, "<tr><td>1.00</td><td>02/20/2004</td><td>Final release.</td></tr>\n");
	HSSend (handle, "<tr><td>1.01</td><td>03/05/2004</td><td>Update for final release.</td></tr>\n");
	HSSend (handle, "<tr><td>1.011</td><td>03/09/2004</td><td>Update the web interface.</td></tr>\n");
	HSSend (handle, "<tr><td>1.012</td><td>03/16/2004</td><td>Update WaxJet parameters.</td></tr>\n");
	HSSend (handle, "<tr><td>1.013</td><td>03/16/2004</td><td>More updates of WaxJet parameters.</td></tr>\n");
	HSSend (handle, "<tr><td>1.014</td><td>03/16/2004</td><td>Fix the StandBy mode for WaxJet PH.</td></tr>\n");
	HSSend (handle, "<tr><td>1.015</td><td>03/26/2004</td><td>Change PH name WaxJet 192/32 to ProSeries 192NP. Update PH index.</td></tr>\n");
	HSSend (handle, "<tr><td>1.016</td><td>04/05/2004</td><td>Bugfix update.</td></tr>\n");
	HSSend (handle, "<tr><td>1.018</td><td>04/15/2004</td><td>Fix Bitmap negative print bug.</td></tr>\n");
	HSSend (handle, "<tr><td>1.019</td><td>04/28/2004</td><td>Fix element mapping algorithm.</td></tr>\n");
	HSSend (handle, "<tr><td>1.020</td><td>05/11/2004</td><td>Fix statistics info processing.</td></tr>\n");
	HSSend (handle, "<tr><td>1.021</td><td>05/18/2004</td><td>Add code to support 192NP StandBy mode over socket interface.</td></tr>\n");
	HSSend (handle, "<tr><td>1.022</td><td>05/21/2004</td><td>Fix Image inverse algorithm.</td></tr>\n");
	HSSend (handle, "<tr><td>1.023</td><td>05/24/2004</td><td>Fix rasterwait calculation algorithm.</td></tr>\n");
	HSSend (handle, "<tr><td>1.024</td><td>06/14/2004</td><td>Add dynamic text and dynamic barcode elements. Modify BRAM structure. Beta 1.</td></tr>\n");
	HSSend (handle, "<tr><td>1.025</td><td>06/15/2004</td><td>Add new socket commands: IPC_DELETE_ALL_BMPS and IPC_DELETE_ALL_LABELS.</td></tr>\n");
	HSSend (handle, "<tr><td>1.026</td><td>06/16/2004</td><td>Add new web page 'Variable Data'. Rename 'Label & Fonts' submenu to 'User Data'.</td></tr>\n");
	HSSend (handle, "<tr><td>1.027</td><td>06/18/2004</td><td>Add new socket commands: IPC_SET_SELECTED_COUNT and IPC_GET_SELECTED_COUNT.</td></tr>\n");
	HSSend (handle, "<tr><td>1.028</td><td>06/21/2004</td><td>Add new serial port property. Add select variable data field to serial port.(**WEB ONLY)</td></tr>\n");
	HSSend (handle, "<tr><td>1.029</td><td>06/23/2004</td><td>Add new socket commands: IPC_SELECT_VARDATA_ID, IPC_GET_VARDATA_ID, IPC_SET_VARDATA_ID, IPC_GET_SW_VER_INFO and IPC_GET_GA_VER_INFO.</td></tr>\n");
	HSSend (handle, "<tr><td>1.030</td><td>06/28/2004</td><td>Update System Internet page - change Net IO Box to Marksman Hub.</td></tr>\n");
	HSSend (handle, "<tr><td>1.031</td><td>06/30/2004</td><td>Fix RunAPS button action.</td></tr>\n");
	HSSend (handle, "<tr><td>1.032</td><td>07/02/2004</td><td>Add StandBy auto mode for 192NP. (Web only, without ROM support).</td></tr>\n");
	HSSend (handle, "<tr><td>1.033</td><td>07/06/2004</td><td>Add complete system support of StandBy auto mode for 192NP.</td></tr>\n");
	HSSend (handle, "<tr><td>1.034</td><td>07/07/2004</td><td>Remove Installation Angle from Printer Configuration web page.</td></tr>\n");
	HSSend (handle, "<tr><td>1.035</td><td>07/09/2004</td><td>Fix keywords table - socket interface.</td></tr>\n");
	HSSend (handle, "<tr><td>1.036</td><td>07/23/2004</td><td>Fix processing of the selected label counter.</td></tr>\n");
	HSSend (handle, "<tr><td>1.037</td><td>08/04/2004</td><td>Fix auto StandBy mode hadler.</td></tr>\n");
	HSSend (handle, "<tr><td>1.038</td><td>08/06/2004</td><td>Rename PH ProSeries 192NP to ProSeries NP192. Add new PH ProSeries AC.</td></tr>\n");
	HSSend (handle, "<tr><td>1.039</td><td>08/11/2004</td><td>Add new socket command: IPC_GA_UPGRADE.</td></tr>\n");
	HSSend (handle, "<tr><td>1.040</td><td>08/18/2004</td><td>Add code to determine encoder resolution based on GA version.</td></tr>\n");
	HSSend (handle, "<tr><td>1.041</td><td>08/23/2004</td><td>Final code clean up before release of ver. 1.1.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1001</td><td>09/07/2004</td><td>Add status info for GA upgrade.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1002</td><td>10/18/2004</td><td>Fix processing encoder resolution value based on GA version.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1003</td><td>10/22/2004</td><td>Update socket commands to process new dynamic elements.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1004</td><td>10/27/2004</td><td>Redesign initial sequence in fj_init subroutine.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1005</td><td>11/04/2004</td><td>Fix dynbarcode HR text processing in draw function.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1006</td><td>11/12/2004</td><td>Fix problem in ENI code that apears after cleanup procedure.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1007</td><td>11/16/2004</td><td>Change text in SE table on Printer Configuration page. Update code on Printer slant page.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1008</td><td>11/17/2004</td><td>Fix processing SE settings on Printer Configuration page.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1009</td><td>11/19/2004</td><td>Fix printchain analizing algorithm in ENITransmit subroutine.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1010</td><td>11/24/2004</td><td>Fix status info processing after firmware update.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1011</td><td>12/16/2004</td><td>Change MAX_DIST variable from 7 to 10 feet. Increase max number of elements per label to 50.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1012</td><td>12/17/2004</td><td>Fix dynamic element parsing code for socket interface.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1012</td><td>12/17/2004</td><td>Fix SE settings update through socket interface.</td></tr>\n");
	HSSend (handle, "<tr><td>1.1015</td><td>3/03/2005</td><td>IPC_SET_SELECTED_COUNT corrected to reset count elements</td></tr>\n");
	HSSend (handle, "<tr><td>1.1015</td><td>3/03/2005</td><td>Added internal encoder and auto print</td></tr>\n");
	HSSend (handle, "<tr><td>1.2000</td><td>04/22/2005</td><td>Fix processing of variable data coming in the serial port and thru sockect interface.<br>");
	HSSend (handle, "Add implementation for socket command IPC_SAVE_ALL_PARAMS.<br>\n");
	HSSend (handle, "Add new socket commands: IPC_SET_STATUS_MODE and IPC_GET_STATUS_MODE.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2002</td><td>04/26/2005</td><td>Update PhotocellBuild method for dynamic elements.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2003</td><td>05/03/2005</td><td>Add code to copy of counter element values in BRAM, and restore them at boot time.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2004</td><td>05/07/2005</td><td>Add code to copy board params in BRAM, and validate them at boot time.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2005</td><td>05/08/2005</td><td>Fix start of APS cycle after click on Submit button on System configuration page when APS is disable.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2006</td><td>05/10/2005</td><td>Add new parameters to dynamic barcode element.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2007</td><td>05/11/2005</td><td>Update code for new dynamic barcode parameters.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2008</td><td>05/12/2005</td><td>Add code to restore counter element values in selected label after save of Edit session.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2016</td><td>06/12/2005</td><td>Fixed horizonal alignment issue (slanted heads).</td></tr>\n");
	HSSend (handle, "<tr><td>1.2018</td><td>06/12/2005</td><td>Added test pattern.</td></tr>\n");
	HSSend (handle, "<tr><td>1.2018</td><td>07/10/2005</td><td>Changed fj_processParameterStringInput, fj_processParameterStringOutput to handle '{' and '}' chars</td></tr>\n");
	HSSend (handle, "<tr><td>1.2019</td><td>07/22/2005</td><td>Corrected ink usage for AC printhead</td></tr>\n");
	HSSend (handle, "<tr><td>1.2020</td><td>07/25/2005</td><td>Ignore photocell during print cycle</td></tr>\n");
	HSSend (handle, "<tr><td>1.2021</td><td>04/07/2006</td><td>C128/EAN128</td></tr>\n");
	HSSend (handle, "<tr><td>1.2022</td><td>04/11/2006</td><td>AC offset</td></tr>\n");
	HSSend (handle, "<tr><td>1.2023</td><td>04/20/2006</td><td>Paragraph mode</td></tr>\n");
	HSSend (handle, "<tr><td>1.2024</td><td>04/20/2006</td><td>Clear ROM?</td></tr>\n");
	HSSend (handle, "<tr><td>1.2025</td><td>04/25/2006</td><td>fj_socketCmd fix</td></tr>\n");
	HSSend (handle, "<tr><td>1.2026</td><td>04/25/2006</td><td>fj_socketCmd/sync fix</td></tr>\n");
	HSSend (handle, "<tr><td>1.2027</td><td>10/26/2006</td><td>added 115200 baud rate</td></tr>\n");
	HSSend (handle, "<tr><td>1.2028</td><td>10/26/2006</td><td>APP_VER (for BW)</td></tr>\n");
	HSSend (handle, "<tr><td>1.2028</td><td>11/20/2006</td><td>ProSeries 768 Lite</td></tr>\n");
	HSSend (handle, "<tr><td>1.2029</td><td>12/08/2006</td><td>fixed IPC_SET_VARDATA_ID zero length bug</td></tr>\n");
	HSSend (handle, "<tr><td>1.2031</td><td>12/08/2006</td><td>768 Lite fix</td></tr>\n");
	HSSend (handle, "<tr><td>1.2032</td><td>2/09/2007</td><td>Changed ink usage for 192NP</td></tr>\n");
	HSSend (handle, "<tr><td>1.2034</td><td>2/23/2007</td><td>fj_serialPortReadBuffer [512]</td></tr>\n");
	HSSend (handle, "<tr><td>1.2035</td><td>2/29/2007</td><td>sw0852</td></tr>\n");
	HSSend (handle, "<tr><td>1.2036</td><td>3/20/2007</td><td>IPC_GET_STATUS fix (didn't work when STATUS_MODE=OFF)</td></tr>\n");
	HSSend (handle, "<tr><td>1.2037</td><td>3/20/2007</td><td>STATUS_MODE=OFF fix (webpage didn't update)</td></tr>\n");
	HSSend (handle, "<tr><td>1.2040</td><td>9/20/2007</td><td>STATUS_MODE=KEEP_ALIVE</td></tr>\n");
	HSSend (handle, "<tr><td>1.2041</td><td>10/16/2007</td><td>STATUS_MODE=KEEP_ALIVE (watchdog fix)</td></tr>\n");
	HSSend (handle, "<tr><td>1.2042</td><td>11/08/2007</td><td>Start-of-print notification</td></tr>\n");
	HSSend (handle, "<tr><td>3.0000</td><td>12/03/2007</td><td>BW NET 3.00 standard release</td></tr>\n");
	HSSend (handle, "<tr><td>3.0001</td><td>1/15/2008</td><td>Start-of-print revision</td></tr>\n");
	HSSend (handle, "<tr><td>3.0002</td><td>1/21/2008</td><td>added BAUD_RATE; added Manufacturing, Maintenance pages</td></tr>\n");
	HSSend (handle, "<tr><td>3.0003</td><td>4/8/2008</td><td>added DataMatrix.  fixed: None/Variable data bug, serial I/O input bug, \"Use scanner input once\"</td></tr>\n");
	HSSend (handle, "</table>\n");
}										/* @! Function: na_get_version_info  }} */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_ID  {{ */
/* @! End of dynamic functions   */
