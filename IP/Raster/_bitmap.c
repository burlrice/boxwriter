/* C file: C:\MyPrograms\IP_APP_v01\_bitmap.c created
 by NET+Works HTML to C Utility on Thursday, February 14, 2002 10:43:27 */
#include "fj.h"
#include "fj_bitmap.h"
#include "fj_memstorage.h"
#include "fj_misc.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_EDIT_BUTTONS */
void na_EDIT_BUTTONS (unsigned long handle);
/*  @! File: na_BITMAP_TABLE */
static void na_BITMAP_TABLE (unsigned long handle);
/*  @! File: na_TITLE_BITMAPS */
static void na_TITLE_BITMAPS (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_STAvailEdit */
void na_STAvailEdit (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\bitmap.htm {{ */
void Send_function_28(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_BITMAPS (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Bitmaps\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "var of=document.bitmap1;\n");
	HSSend (handle, "of.action.value=sa; of.item.value=si;\n");
	HSSend (handle, "of.submit(); }\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p>\n");
	HSSend (handle, "<table border=\"0\" width=\"99%\">\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td>&nbsp;</td>\n");
	HSSend (handle, "<td align=right><table cellpadding=\"4\"><tr><td><b>Available Storage: ");
	na_STAvailEdit (handle);
	HSSend (handle, "</b></td>\n");
	HSSend (handle, "<td align=right>");
	na_EDIT_BUTTONS (handle);
	HSSend (handle, "</td></tr></table></td></tr></table>\n");
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"2\" width=\"60%\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "");
	na_BITMAP_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<form name=\"bitmap1\" action=\"bitmap_input\" method=\"POST\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" value=\"none\" name=\"action\">\n");
	HSSend (handle, "<input type=\"hidden\" value=\"none\" name=\"item\">\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\bitmap.htm }} */
/* @! End HTML page   */

void reload_bitmap(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=bitmap.htm");
	}
	else
	{
		fj_reload_page(handle, "bitmap.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_bitmap_input  {{ */
void Post_bitmap_input(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	char   formField[FJBMP_NAME_SIZE+1];
	STR    sBmpName[FJBMP_NAME_SIZE*2];
	ULONG  ipAdd;
	BOOL   bROMWait;
	BOOL   bRet;
	int    ret;

	bROMWait = FALSE;
	ipAdd = HSRemoteAddress(handle);
	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"edit_start") )
		{
			pCVT->fj_print_head_EditStart( ipAdd );
		}
		else if ( 0 == strcmp(formField,"edit_save") )
		{
			pCVT->fj_print_head_EditSave( ipAdd );
			bROMWait = TRUE;
		}
		else if ( 0 == strcmp(formField,"edit_cancel") )
		{
			pCVT->fj_print_head_EditCancel( ipAdd );
		}
		else if ( 0 == strcmp(formField,"delete") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				bRet = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );
				if ( TRUE == bRet )
				{
					//  Name
					pCVT->fj_processParameterStringOutput( sBmpName, formField );
					bRet = pCVT->fj_MemStorDeleteElement( pfph->pMemStorEdit, pCVT->pfj_bmpID, sBmpName );
				}
			}
		}
	}
	reload_bitmap(handle,bROMWait);
}										/* @! Function: Post_bitmap_input  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_EDIT_BUTTONS  {{ */
/* @! Function: na_BITMAP_TABLE  {{ */
void na_BITMAP_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM  pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph  = pCVT->pfph;
	LPFJMEMSTOR pfms;
	LPSTR      *papBmp;
	LPSTR       pBmp;
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LPSTR       pParams[3];
	STR         sBmpName[FJBMP_NAME_SIZE*2];
	CHAR        str[100];
	BOOL bEdit;
	int i, j;
	int jSeg;

	bEdit = pCVT->fj_PrintHeadIsEdit( pfph, HSRemoteAddress(handle) );
	if ( TRUE == bEdit ) HSSend(handle, "<col align=center>\n");
	HSSend(handle, "<col align=center><col align=center><col align=center><col align=right>\n");
	HSSend(handle, "<tr><thead>");
	if ( TRUE == bEdit )
	{
		pfms = pfph->pMemStorEdit;
		HSSend(handle, "<td>&nbsp;</td>");
	}
	else
	{
		pfms = pfph->pMemStor;
	}
	HSSend(handle, "<td><b>Bitmap Name</b></td>");
	HSSend(handle, "<td><b>Height</b></td>");
	HSSend(handle, "<td><b>Width</b></td>");
	HSSend(handle, "<td><b>Size</b></td>");
	HSSend(handle, "</tr></thead><tbody>\n");

	papBmp = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_bmpID );
	for ( i = 0; NULL != *(papBmp+i); i++ )
	{
		pBmp = *(papBmp+i);
										// copy part of string
		strncpy( str, pBmp, sizeof(str) );
		str[sizeof(str)-2] = '}';		// guarantee end for partial scan
		str[sizeof(str)-1] = 0;
		pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 3 );
		pCVT->fj_processParameterStringInput( pParams[1] );

		pBmp += strlen(pBmp) + 1;
		pbmfh = (BITMAPFILEHEADER *)pBmp;
		pbmih = (BITMAPINFOHEADER *)(pBmp+sizeof(BITMAPFILEHEADER));
		{
			HSSend(handle, "<tr>");
			if ( TRUE == bEdit )
			{
				HSSend(handle, "<td><button class=fjbtn1 onClick='submit1(\"delete\",\"");
				na_String(handle, pParams[1]);
				HSSend(handle, "\")'>Delete</button></td>");
			}
			HSSend(handle, "<td>");
			na_String(handle, pParams[1]);
			HSSend(handle, "</td>");

			HSSend(handle, "<td>");
			na_Long(handle, LOADREVERSEWORD32(&pbmih->biHeight) );
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, LOADREVERSEWORD32(&pbmih->biWidth) );
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, LOADREVERSEWORD32(&pbmfh->bfSize) );
			HSSend(handle, "</td>");

			HSSend(handle, "</tr>\n");
		}
	}
	HSSend(handle, "</tbody>\n");
}										/* @! Function: na_BITMAP_TABLE  }} */
/* @! Function: na_TITLE_BITMAPS  {{ */
void na_TITLE_BITMAPS(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Bitmaps" );
}										/* @! Function: na_TITLE_BITMAPS  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_STAvailEdit  {{ */
/* @! End of dynamic functions   */
