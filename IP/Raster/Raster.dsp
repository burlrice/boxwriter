# Microsoft Developer Studio Project File - Name="Raster" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Raster - Win32 IP Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Raster.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Raster.mak" CFG="Raster - Win32 IP Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Raster - Win32 IP Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Raster - Win32 IP Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 1
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Raster - Win32 IP Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Raster___Win32_IP_Release"
# PROP BASE Intermediate_Dir "Raster___Win32_IP_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "IP_Release"
# PROP Intermediate_Dir "IP_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /I "Barcode" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RASTER_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /O2 /I "Barcode" /I "..\..\Editor\DLL\Common" /D "NDEBUG" /D "FOXJET" /D "_USRDLL" /D "RASTER_EXPORTS" /D "__IPPRINTER__" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /libpath:"Barcode\Release"
# Begin Custom Build - Updating $(TargetName)
TargetPath=.\IP_Release\Raster.dll
TargetName=Raster
InputPath=.\IP_Release\Raster.dll
SOURCE="$(InputPath)"

"\Foxjet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \Foxjet\IP

# End Custom Build

!ELSEIF  "$(CFG)" == "Raster - Win32 IP Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Raster___Win32_IP_Debug"
# PROP BASE Intermediate_Dir "Raster___Win32_IP_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "IP_Debug"
# PROP Intermediate_Dir "IP_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "Barcode" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "RASTER_EXPORTS" /FR /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /GX /ZI /Od /I "Barcode" /I "..\..\Editor\DLL\Common" /D "_DEBUG" /D "FOXJET" /D "_USRDLL" /D "RASTER_EXPORTS" /D "__IPPRINTER__" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept /libpath:"Barcode\Debug"
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept /libpath:"Barcode\Debug"
# Begin Custom Build - Updating $(TargetName)
TargetPath=.\IP_Debug\Raster.dll
TargetName=Raster
InputPath=.\IP_Debug\Raster.dll
SOURCE="$(InputPath)"

"\Foxjet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \Foxjet\IP

# End Custom Build

!ENDIF 

# Begin Target

# Name "Raster - Win32 IP Release"
# Name "Raster - Win32 IP Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\arialttf.c
# End Source File
# Begin Source File

SOURCE=.\barcode.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\codabar.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\code128.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\code39.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\ean.c
# End Source File
# Begin Source File

SOURCE=.\fj_barcode.c
# End Source File
# Begin Source File

SOURCE=.\fj_bitmap.c
# End Source File
# Begin Source File

SOURCE=.\fj_counter.c
# End Source File
# Begin Source File

SOURCE=.\fj_datamatrix.c
# End Source File
# Begin Source File

SOURCE=.\fj_datetime.c
# End Source File
# Begin Source File

SOURCE=.\fj_dynbarcode.c
# End Source File
# Begin Source File

SOURCE=.\fj_dynimage.c
# End Source File
# Begin Source File

SOURCE=.\fj_dyntext.c
# End Source File
# Begin Source File

SOURCE=.\fj_element.c
# End Source File
# Begin Source File

SOURCE=.\fj_font.c
# End Source File
# Begin Source File

SOURCE=.\fj_font_disk.c
# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\fj_image.c
# End Source File
# Begin Source File

SOURCE=.\fj_info_soft.c
# End Source File
# Begin Source File

SOURCE=.\fj_label.c
# End Source File
# Begin Source File

SOURCE=.\fj_mem.c
# End Source File
# Begin Source File

SOURCE=.\fj_memstorage.c
# End Source File
# Begin Source File

SOURCE=.\fj_message.c
# End Source File
# Begin Source File

SOURCE=.\fj_misc.c
# End Source File
# Begin Source File

SOURCE=.\fj_printhead.c
# End Source File
# Begin Source File

SOURCE=.\fj_system.c
# End Source File
# Begin Source File

SOURCE=.\fj_test_pattern.c
# End Source File
# Begin Source File

SOURCE=.\fj_text.c
# End Source File
# Begin Source File

SOURCE=.\fx16x12.c
# End Source File
# Begin Source File

SOURCE=.\fx32x32.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\i25.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\iec16022.c
# End Source File
# Begin Source File

SOURCE=".\iec16022-0.2.1\iec16022ecc200.c"
# End Source File
# Begin Source File

SOURCE=.\BarCode\library.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\msi.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\plessey.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\ps.c
# End Source File
# Begin Source File

SOURCE=.\Raster.cpp
# End Source File
# Begin Source File

SOURCE=.\Raster.def
# End Source File
# Begin Source File

SOURCE=.\BarCode\reedsol.c
# End Source File
# Begin Source File

SOURCE=.\BarCode\strdup.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Barcode\barcode.h
# End Source File
# Begin Source File

SOURCE=.\fj.h
# End Source File
# Begin Source File

SOURCE=.\fj_barcode.h
# End Source File
# Begin Source File

SOURCE=.\fj_base.h
# End Source File
# Begin Source File

SOURCE=.\fj_bitmap.h
# End Source File
# Begin Source File

SOURCE=.\fj_counter.h
# End Source File
# Begin Source File

SOURCE=.\fj_datamatrix.h
# End Source File
# Begin Source File

SOURCE=.\fj_datetime.h
# End Source File
# Begin Source File

SOURCE=.\fj_defines.h
# End Source File
# Begin Source File

SOURCE=.\fj_dynbarcode.h
# End Source File
# Begin Source File

SOURCE=.\fj_dynimage.h
# End Source File
# Begin Source File

SOURCE=.\fj_dyntext.h
# End Source File
# Begin Source File

SOURCE=.\fj_element.h
# End Source File
# Begin Source File

SOURCE=.\fj_font.h
# End Source File
# Begin Source File

SOURCE=.\fj_image.h
# End Source File
# Begin Source File

SOURCE=.\fj_label.h
# End Source File
# Begin Source File

SOURCE=.\fj_mem.h
# End Source File
# Begin Source File

SOURCE=.\fj_memstorage.h
# End Source File
# Begin Source File

SOURCE=.\fj_message.h
# End Source File
# Begin Source File

SOURCE=.\fj_misc.h
# End Source File
# Begin Source File

SOURCE=.\fj_printhead.h
# End Source File
# Begin Source File

SOURCE=.\fj_socket.h
# End Source File
# Begin Source File

SOURCE=.\fj_system.h
# End Source File
# Begin Source File

SOURCE=.\fj_text.h
# End Source File
# Begin Source File

SOURCE=.\BarCode\iec16022ecc200.h
# End Source File
# Begin Source File

SOURCE=.\BarCode\image.h
# End Source File
# Begin Source File

SOURCE=.\BarCode\reedsol.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
