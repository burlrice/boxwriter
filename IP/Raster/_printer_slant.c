/* C file: C:\MyPrograms\IP_APP_v01\_printer_slant.c created
 by NET+Works HTML to C Utility on Friday, March 15, 2002 09:48:22 */
#include <hservapi.h>
#include <math.h>
#include "fj.h"

#define RADIANS 1.570796326794896619231

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_SLANT_title */
static void na_SLANT_title (unsigned long handle);
/*  @! File: na_SL_SE */
static void na_SL_SE (unsigned long handle);
/*  @! File: na_SL_HEIGHT */
static void na_SL_HEIGHT (unsigned long handle);
/*  @! File: na_SL_ANG_DES */
static void na_SL_ANG_DES (unsigned long handle);
/*  @! File: na_SL_ANG_INST */
static void na_SL_ANG_INST (unsigned long handle);
/*  @! File: na_SL_SPAN_OUTER */
static void na_SL_SPAN_OUTER (unsigned long handle);
/*  @! File: na_SL_SPAN_INNER */
static void na_SL_SPAN_INNER (unsigned long handle);
/*  @! File: na_SL_NOZZLES */
static void na_SL_NOZZLES (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\printer_slant.htm {{ */
void Send_function_30(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<title>Printer Slant</title>\n");
	HSSend (handle, "<link rel=\"stylesheet\" href=\"fjipstyle1.css\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body onload=\"settable()\">\n");
	HSSend (handle, "<p align=\"left\" class=\"fjh2\">");
	na_ID (handle);
	HSSend (handle, " : ");
	na_SLANT_title (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var ns=(document.layers);\n");
	HSSend (handle, "var ie=(document.all);\n");
	HSSend (handle, "var layertype=\"div\";\n");
	HSSend (handle, "if(ns){\n");
	HSSend (handle, "layertype=\"ilayer\";\n");
	HSSend (handle, "}\n");
	HSSend (handle, "if (!ns && !ie) ns = \"ns\";\n");
	HSSend (handle, "var maxslant=20;\n");
	HSSend (handle, "var spanOuter=");
	na_SL_SPAN_OUTER (handle);
	HSSend (handle, " ;\n");
	HSSend (handle, "var spanInner=");
	na_SL_SPAN_INNER (handle);
	HSSend (handle, " ;\n");
	HSSend (handle, "var nozzles=");
	na_SL_NOZZLES (handle);
	HSSend (handle, " ;\n");
	HSSend (handle, "var ccStraight=spanInner/(nozzles-1);\n");
	HSSend (handle, "var radians=1.570796326794896619231;\n");
	HSSend (handle, "function trunc(str,before,after) {\n");
	HSSend (handle, "point=str.indexOf('.');\n");
	HSSend (handle, "if(point < 0) str=str+\".00000\";\n");
	HSSend (handle, "point=str.indexOf('.');\n");
	HSSend (handle, "if(point<before)str=\" \"+str;\n");
	HSSend (handle, "point=str.indexOf('.');\n");
	HSSend (handle, "str=str.substring(point-before,point+after+1);\n");
	HSSend (handle, "return str;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function findo(name)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "var o;\n");
	HSSend (handle, "if(document.all) o = document.all.name;\n");
	HSSend (handle, "else if(document.layers) o = document.layers[name];\n");
	HSSend (handle, "else if(document.getElementById) o = document.getElementById(name);\n");
	HSSend (handle, "return o;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function replaceContent( name, content )\n");
	HSSend (handle, "{\n");
	HSSend (handle, "var obj = findo(name);\n");
	HSSend (handle, "if (document.layers)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "obj.document.open();\n");
	HSSend (handle, "obj.document.writeln( 123 );\n");
	HSSend (handle, "obj.document.close();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "else if (document.all)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "var str = \"document.all.\" + name + \".innerHTML = ' \" + content + \"'\";\n");
	HSSend (handle, "eval(str);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "else if(document.getElementById) obj.innerHTML = content;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "\n");
	HSSend (handle, "function settable()\n");
	HSSend (handle, "{\n");
	HSSend (handle, "var o;\n");
	HSSend (handle, "if(ie)se=document.forms(\"seform\").elements(\"se\").value;\n");
	HSSend (handle, "if(ns)se=document.forms.seform.elements.se.value;\n");
	HSSend (handle, "for (i=0; i < maxslant; i++)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "straa=\"&nbsp;\";\n");
	HSSend (handle, "stras=\"&nbsp;\";\n");
	HSSend (handle, "strah=\"&nbsp;\";\n");
	HSSend (handle, "stree=\"&nbsp;\";\n");
	HSSend (handle, "esin=(0.0+i)/ccStraight/se;\n");
	HSSend (handle, "if(esin <= 1.0)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "stras=i;\n");
	HSSend (handle, "angleR=Math.asin(esin);\n");
	HSSend (handle, "angleD=90.0-angleR*90./radians;\n");
	HSSend (handle, "straa=\" \"+angleD;\n");
	HSSend (handle, "straa=trunc(straa,2,1);\n");
	HSSend (handle, "strah=\" \"+spanOuter*Math.cos(angleR);\n");
	HSSend (handle, "strah=trunc(strah,1,2);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "replaceContent('aa'+i,straa);\n");
	HSSend (handle, "replaceContent('as'+i,stras);\n");
	HSSend (handle, "replaceContent('ah'+i,strah);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p> Printer&nbsp;resolution value is ");
	na_SL_SE (handle);
	HSSend (handle, " dots per inch.</p>\n");
	HSSend (handle, "<p>At the design angle of ");
	na_SL_ANG_DES (handle);
	HSSend (handle, ", the image is ");
	na_SL_HEIGHT (handle);
	HSSend (handle, " high.</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<form name=\"seform\">\n");
	HSSend (handle, "<table border=\"0\" cellpadding=\"1\" width=\"80%\">\n");
	HSSend (handle, "<colgroup span=\"3\" width=\"20%\"></colgroup>\n");
	HSSend (handle, "<tr><td colspan=\"3\"><center>For the listed Slant values</center></td></tr>\n");
	HSSend (handle, "<tr><td colspan=\"3\"><center>Possible install angles</center></td></tr>\n");
	HSSend (handle, "<tr><td colspan=\"2\" align=\"right\">with Shaft Encoder of</td><td align=\"left\"><input type=\"text\" name=\"se\" value=\"");
	na_SL_SE (handle);
	HSSend (handle, "\" size=\"5\" onchange=\"settable();\"></td></tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<table border=1 cellpadding=\"2\" width=\"80%\" bgcolor=\"#f5f5f5\" ID=\"Table1\">\n");
	HSSend (handle, "<colgroup span=\"3\" width=\"20%\"></colgroup>\n");
	HSSend (handle, "<thead><tr><td><center>Height</center></td><td><center>Angle</center></td><td><center>Slant</center></td></tr></thead>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "layerstart='<'+layertype+' id=';\n");
	HSSend (handle, "layerend='</'+layertype+'>';\n");
	HSSend (handle, "for (i=0; i < maxslant; i++)\n");
	HSSend (handle, "{\n");
	HSSend (handle, "document.write('<tr><td><center>'+layerstart+'\"ah'+i+'\">x'+layerend);\n");
	HSSend (handle, "document.write('</center></td><td><center>'+layerstart+'\"aa'+i+'\">x'+layerend);\n");
	HSSend (handle, "document.write('</center></td><td><center>'+layerstart+'\"as'+i+'\">x'+layerend);\n");
	HSSend (handle, "document.writeln('</center></td></tr>');\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</tr></table></form>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\printer_slant.htm }} */
/* @! End HTML page   */

/* @! Begin of dynamic functions */
/* @! Function: na_ID  {{ */
/* @! Function: na_SLANT_title  {{ */
void na_SLANT_title(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, "Slant / Angle Table");
}										/* @! Function: na_SLANT_title  }} */
/* @! Function: na_SL_SE  {{ */
void na_SL_SE(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pfsys->fEncoder, 2 );
}										/* @! Function: na_SL_SE  }} */
/* @! Function: na_SL_HEIGHT  {{ */
void na_SL_HEIGHT(unsigned long handle)
{
	/* add your implementation here: */
	na_Double( handle, (DOUBLE)(pCVT->pfph->pfphy->fSpanOuter * sin(pCVT->pfph->pfphy->fAngle*RADIANS/90.0)), 2 );
}										/* @! Function: na_SL_HEIGHT  }} */
/* @! Function: na_SL_ANG_DES  {{ */
void na_SL_ANG_DES(unsigned long handle)
{
	/* add your implementation here: */
	na_Double( handle, (DOUBLE)pCVT->pfph->pfphy->fAngle, 2 );
}										/* @! Function: na_SL_ANG_DES  }} */
/* @! Function: na_SL_ANG_INST  {{ */
void na_SL_ANG_INST(unsigned long handle)
{
	/* add your implementation here: */
	na_Double( handle, (DOUBLE)pCVT->pfph->fSlantAngle, 2 );
}										/* @! Function: na_SL_ANG_INST  }} */
/* @! Function: na_SL_SPAN_OUTER  {{ */
void na_SL_SPAN_OUTER(unsigned long handle)
{
	/* add your implementation here: */
	na_Double( handle, (DOUBLE)pCVT->pfph->pfphy->fSpanOuter, 12 );
}										/* @! Function: na_SL_SPAN_OUTER  }} */
/* @! Function: na_SL_SPAN_INNER  {{ */
void na_SL_SPAN_INNER(unsigned long handle)
{
	/* add your implementation here: */
	na_Double( handle, (DOUBLE)pCVT->pfph->pfphy->fSpanCenter, 12 );
}										/* @! Function: na_SL_SPAN_INNER  }} */
/* @! Function: na_SL_NOZZLES  {{ */
void na_SL_NOZZLES(unsigned long handle)
{
	/* add your implementation here: */
	na_Long( handle, pCVT->pfph->pfphy->lChannels );
}										/* @! Function: na_SL_NOZZLES  }} */
/* @! End of dynamic functions   */
