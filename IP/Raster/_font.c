/* C file: C:\MyPrograms\IP_APP_v01\_font.c created
 by NET+Works HTML to C Utility on Monday, February 11, 2002 10:09:05 */
#include "fj.h"
#include "fj_font.h"
#include "fj_memstorage.h"
#include "fj_misc.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_EDIT_BUTTONS */
void na_EDIT_BUTTONS (unsigned long handle);
/*  @! File: na_FONT_TABLE */
static void na_FONT_TABLE (unsigned long handle);
/*  @! File: na_TITLE_FONTS */
static void na_TITLE_FONTS (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_STAvailEdit */
void na_STAvailEdit (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\font.htm {{ */
void Send_function_1(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_FONTS (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Fonts\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "var of=document.font1;\n");
	HSSend (handle, "of.action.value=sa; of.item.value=si;\n");
	HSSend (handle, "of.submit(); }\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p>\n");
	HSSend (handle, "<table border=\"0\" width=\"99%\">\n");
	HSSend (handle, "<tr><td>&nbsp;</td><td>&nbsp;</td>\n");
	HSSend (handle, "<td align=right><table cellpadding=\"4\"><tr><td><b>Available Storage: ");
	na_STAvailEdit (handle);
	HSSend (handle, "</b></td>\n");
	HSSend (handle, "<td align=right>");
	na_EDIT_BUTTONS (handle);
	HSSend (handle, "</td></tr></table></td></tr></table>\n");
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"2\" width=\"60%\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "");
	na_FONT_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<form name=\"font1\" action=\"font_input\" method=\"POST\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" value=\"none\" name=\"action\">\n");
	HSSend (handle, "<input type=\"hidden\" value=\"none\" name=\"item\">\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</html>");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\font.htm }} */
/* @! End HTML page   */

void reload_font(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=font.htm");
	}
	else
	{
		fj_reload_page(handle, "font.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_font_input  {{ */
void Post_font_input(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	char   formField[FJFONT_NAME_SIZE+1];
	STR    sFontName[FJFONT_NAME_SIZE*2];
	ULONG  ipAdd;
	BOOL   bROMWait;
	BOOL   bRet;
	int    ret;

	bROMWait = FALSE;
	ipAdd = HSRemoteAddress(handle);
	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"edit_start") )
		{
			pCVT->fj_print_head_EditStart( ipAdd );
		}
		else if ( 0 == strcmp(formField,"edit_save") )
		{
			pCVT->fj_print_head_EditSave( ipAdd );
			bROMWait = TRUE;
		}
		else if ( 0 == strcmp(formField,"edit_cancel") )
		{
			pCVT->fj_print_head_EditCancel( ipAdd );
		}
		else if ( 0 == strcmp(formField,"delete") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				bRet = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );
				if ( TRUE == bRet )
				{
					// label Name
					pCVT->fj_processParameterStringOutput( sFontName, formField );
					bRet = pCVT->fj_MemStorDeleteElement( pfph->pMemStorEdit, pCVT->pfj_FontID, sFontName );
				}
			}
		}
	}
	reload_font(handle,bROMWait);
}										/* @! Function: Post_font_input  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_EDIT_BUTTONS  {{ */
/* @! Function: na_FONT_TABLE  {{ */
void na_FONT_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM  pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph  = pCVT->pfph;
	LPFJMEMSTOR pfms;
	LPFJFONT    pff;
	LPSTR      *papFont;
	STR         sFontName[FJFONT_NAME_SIZE*2];
	CHAR        str[100];
	BOOL bEdit;
	int i, j;
	int jSeg;

	bEdit = pCVT->fj_PrintHeadIsEdit( pfph, HSRemoteAddress(handle) );
	if ( TRUE == bEdit ) HSSend(handle, "<col align=center>\n");
	HSSend(handle, "<col align=center><col align=center><col align=center><col align=center><col align=center><col align=center><col align=right>\n");
	HSSend(handle, "<thead><tr>");
	if ( TRUE == bEdit )
	{
		pfms = pfph->pMemStorEdit;
		HSSend(handle, "<td>&nbsp;</td>");
	}
	else
	{
		pfms = pfph->pMemStor;
	}
	HSSend(handle, "<td><b>Font Name</b></td>");
	HSSend(handle, "<td><b>Font Type</b></td>");
	HSSend(handle, "<td><b>Height</b></td>");
	HSSend(handle, "<td><b>Width</b></td>");
	HSSend(handle, "<td><b>First Character</b></td>");
	HSSend(handle, "<td><b>Last Character</b></td>");
	HSSend(handle, "<td><b>Size</b></td>");
	HSSend(handle, "</tr></thead><tbody>\n");

	papFont = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_FontID );
	for ( i = 0; NULL != *(papFont+i); i++ )
	{
		pff = pCVT->fj_FontFromBuffer( (LPBYTE)*(papFont+i) );
		if ( NULL != pff )
		{
			HSSend(handle, "<tr>");
			if ( TRUE == bEdit )
			{
				HSSend(handle, "<td><button class=fjbtn1 onClick='submit1(\"delete\",\"");
				na_String(handle, pff->strName);
				HSSend(handle, "\")'>Delete</button></td>");
			}
			HSSend(handle, "<td>");
			na_String(handle, pff->strName);
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			if ( FJFONT_BITMAP   == pff->lType ) HSSend(handle, "Bitmap");
			if ( FJFONT_TRUETYPE == pff->lType ) HSSend(handle, "TrueType");
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, pff->lHeight);
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, pff->lWidth);
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, pff->lFirstChar);
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, pff->lLastChar);
			HSSend(handle, "</td>");
			HSSend(handle, "<td>");
			na_Long(handle, pff->lBufLen);
			HSSend(handle, "</td>");
			HSSend(handle, "</tr>\n");
			pCVT->fj_FontDestroy( pff );
		}
		else
		{
			// I don't think that this can happen now.
			// fonts should be tested before they are stored in MemStor.
			LPSTR  pParams[2];
			strncpy( str, (LPSTR)*(papFont+i), sizeof(str) );
			str[sizeof(str)-1] = 0;
			j = strlen( str ) - 1;
			if ( ';' == str[j] ) str[j] = 0;
			j = pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 2 );
			pCVT->fj_processParameterStringInput( pParams[1] );
			HSSend(handle, "<tr>");
			if ( TRUE == bEdit )
			{
				HSSend(handle, "<td><button class=fjbtn1 onClick='submit1(\"delete\",\"");
				na_String(handle, pParams[1]);
				HSSend(handle, "\")'>Delete</button></td>");
			}
			HSSend(handle, "<td>");
			na_String(handle,  pParams[1]);
			HSSend(handle, "</td><td>invalid font</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
		}
	}
	HSSend(handle, "</tbody>\n");

}										/* @! Function: na_FONT_TABLE  }} */
/* @! Function: na_TITLE_FONTS  {{ */
void na_TITLE_FONTS(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Fonts" );
}										/* @! Function: na_TITLE_FONTS  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_STAvailEdit  {{ */
/* @! End of dynamic functions   */
