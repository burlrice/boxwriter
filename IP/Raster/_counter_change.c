/* C file: C:\MyPrograms\IP_APP_v01\_counter_change.c created
 by NET+Works HTML to C Utility on Thursday, August 08, 2002 11:52:34 */
#include "fj.h"
#include "fj_counter.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_CC_title */
static void na_CC_title (unsigned long handle);
/*  @! File: na_CC_TABLE */
static void na_CC_TABLE (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\counter_change.htm {{ */
void Send_function_33(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<link rel=\"stylesheet\" href=\"fjipstyle1.css\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>Change Counters</title>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "function check_cur(formObj){parmCheck(formObj,0,999999999,\"Current Value\")}\n");
	HSSend (handle, "function check_rep(formObj){parmCheck(formObj,0,999999999,\"Current Repeat Value\")}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<p class=\"fjh2\" align=\"left\">");
	na_ID (handle);
	HSSend (handle, " : ");
	na_CC_title (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<form name=\"form_cc\" action=\"cc_1\" method=\"POST\">\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"2\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "");
	na_CC_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<p><input class=\"fjbtn1\" type=\"submit\" value=\"Submit\"><input class=\"fjbtn1\" type=\"reset\" value=\"Reset\"></p>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</body>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\counter_change.htm }} */
/* @! End HTML page   */

/* @! Begin of post function */
/* @! Function: Post_cc_1  {{ */
void Post_cc_1(unsigned long handle)
{
	/* add your implementation here: */
	int i;
	LPFJMESSAGE pfm;
	LPFJELEMENT pfe;
	LPFJCOUNTER pfc;
	char   FieldNumber[4];
	char   formField[20];
	char   FieldName[20];
	LONG   lValue;
	int    ret;

	i = 0;
	pfm = pCVT->pfph->pfmSelected;
	if ( NULL != pfm )
	{
		while ( i < pfm->lCount )
		{
			pfe = pfm->papfe[i];
			if ( FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType() )
			{
				pfc = (LPFJCOUNTER)pfe->pDesc;

				sprintf( FieldNumber, "%i", i );

				strcpy( FieldName, "cv" );
				strcat( FieldName, FieldNumber );
				ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
				if ( 0 < ret )
				{
					pfc->lCurValue = atoi( formField );
				}
				strcpy( FieldName, "cvr" );
				strcat( FieldName, FieldNumber );
				ret = fj_HSGetValue( handle, FieldName, formField, 20 );
				if ( 0 < ret )
				{
					pfc->lCurRepeat = atoi( formField );
				}
			}
			i++;
		}
	}

	//	HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">opener.location.replace(\"label.htm?pwid=");
	//	HSSend(handle, pwid);
	//	HSSend(handle, "\");</SCRIPT>");
	HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">window.close();</SCRIPT>");
}										/* @! Function: Post_cc_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_ID  {{ */
/* @! Function: na_CC_title  {{ */
void na_CC_title(unsigned long handle)
{
	/* add your implementation here: */
	HSSend(handle, "Change Counters");
}										/* @! Function: na_CC_title  }} */
/* @! Function: na_CC_TABLE  {{ */
void na_CC_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMESSAGE pfm;
	LPFJELEMENT pfe;
	LPFJCOUNTER pfc;
	char FieldNumber[4];
	int  i;

	HSSend(handle, "<colgroup span=8 align=center></colgroup>\n");
	HSSend(handle, "<thead><tr><td><b>Start</b></td><td><b>Current Value</b></td><td><b>Limit</b></td><td><b>Increment</b></td><td><b>Repeat</b></td><td><b>Current Repeat Value</b></td></tr></thead><tbody>\n");

	i = 0;
	pfm = pfph->pfmSelected;
	if ( NULL != pfm )
	{
		while ( i < pfm->lCount )
		{
			pfe = pfm->papfe[i];
			if ( FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType() )
			{
				pfc = (LPFJCOUNTER)pfe->pDesc;

				sprintf( FieldNumber, "%i", i );

				HSSend(handle, "<tr><td>");
				na_Long(handle, pfc->lStart );
				HSSend(handle, "</td><td><input type=text name=cv");
				HSSend(handle, FieldNumber);
				HSSend(handle, " size=10 maxlength=10 onchange=\"check_cur(this);\") value=\"");
				na_Long(handle, pfc->lCurValue );
				HSSend (handle, "\"></td><td>");
				na_Long(handle, pfc->lLimit );
				HSSend(handle, "</td><td>");
				na_Long(handle, pfc->lChange );
				HSSend(handle, "</td><td>");
				na_Long(handle, pfc->lRepeat );
				HSSend(handle, "</td><td><input type=text name=crv");
				HSSend(handle, FieldNumber);
				HSSend(handle, " size=10 maxlength=10 onchange=\"check_rep(this);\" value=\"");
				na_Long(handle, pfc->lCurRepeat );
				HSSend (handle, "\"></td><td>");
				HSSend(handle, "</td></tr>\n");
			}
			i++;
		}
	}
	HSSend(handle, "</tbody>\n");
}										/* @! Function: na_CC_TABLE  }} */
/* @! End of dynamic functions   */
