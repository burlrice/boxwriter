#ifndef FJ_DATAMATRIX
#define FJ_DATAMATRIX

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjelement;

#define FJDATAMATRIX_WITDH  144
#define FJDATAMATRIX_HEIGHT 144

typedef struct fjdatamatrix
{
	LONG    lSysBCIndex;					// index of global barcode definition
	LONG	cx;
	LONG	cy;

	STR     strText[1+FJDATAMATRIX_SIZE];	// text string

	LONG    lDynFirstChar;					// start pos in dynamic field
	LONG    lDynNumChar;					// number of charecters to use in dynamic field
	LONG    lDynIndex;						// ID of dynamic text segment

} FJDATAMATRIX, FAR *LPFJDATAMATRIX, FAR * const CLPFJDATAMATRIX;
typedef const struct fjdatamatrix FAR *LPCFJDATAMATRIX, FAR * const CLPCFJDATAMATRIX;

#endif // FJ_DATAMATRIX
