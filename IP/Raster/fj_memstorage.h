#ifndef FJ_MEMSTORAGE
#define FJ_MEMSTORAGE

#include "fj_base.h"

// NOTE:  as of 9/6/02, the memstor buffer is not allocated/freed by the memstor code.
//        the buffer must be supplied in the call to build the memstor

typedef struct fjmemstorlist			// list of all elements of a particular type
{
	//	FAR struct fjmemstorlist *pPrev;		// Pointer to prev list
	FAR struct fjmemstorlist *pNext;	// Pointer to next list
	CHAR                     sName[16];	// Name string of element type
	LONG                     lCount;	// Number of element pointers in list - the actual non-NULL entries
	LONG                     lMaxCount;	// Number of element pointers in list - the actual size of the list
										// Pointer to array of pointers to elements in MemStor
	LPSTR                    *papElements;
} FJMEMSTORLIST, *PFJMEMSTORLIST, FAR *LPFJMEMSTORLIST;

typedef struct fjmemstor
{
	LPBYTE      pBuffer;				// Pointer to actual buffer
	LONG        lSize;					// size of buffer
	BOOL        bDoNotFreeBuffer;		// set TRUE in IP. default FALSE for use in an Editor or other program emulating IP on Windows or other systems.
	LONG        lNext;					// offset of next available byte
	LPFJMEMSTORLIST pList;				// pointer to first list of elements
} FJMEMSTOR, *PFJMEMSTOR, FAR *LPFJMEMSTOR;
typedef const struct fjmemstor FAR *LPCFJMEMSTOR, FAR * const CLPCFJMEMSTOR;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	// Memory Storage subroutines for storage of Fonts and Messages
	// Allocate MemStor
	FJDLLExport LPFJMEMSTOR fj_MemStorNew( LPBYTE pMemStorBuffer, LONG lLength );
	FJDLLExport LPFJMEMSTOR fj_MemStorNewFromBuffer( LPBYTE pMemStorBuffer, LONG lLength, LPBYTE pInBuffer );
	FJDLLExport LPFJMEMSTOR fj_MemStorLoadFromBuffer( LPFJMEMSTOR pfms, LPBYTE pBuffer );
	FJDLLExport VOID        fj_MemStorClear( LPFJMEMSTOR pfms );
	// Deallocate MemStor
	FJDLLExport VOID        fj_MemStorDestroy( LPFJMEMSTOR pfms );
	FJDLLExport BOOL        fj_MemStorAddElementString( LPFJMEMSTOR pfms, LPCSTR pStr );
	FJDLLExport BOOL        fj_MemStorAddElementBinary( LPFJMEMSTOR pfms, LPBYTE pData, LONG lDataLen );
	FJDLLExport BOOL        fj_MemStorDeleteElement( LPFJMEMSTOR pfms, LPCSTR pType, LPCSTR pName );
	FJDLLExport LPBYTE      fj_MemStorFindElement( CLPCFJMEMSTOR pfms, LPCSTR pType, LPCSTR pName );
	FJDLLExport LPSTR *     fj_MemStorFindAllElements( CLPCFJMEMSTOR pfms, LPCSTR pType );
#ifdef  __cplusplus
}
#endif
#endif									// ifndef FJ_MEMSTORAGE
