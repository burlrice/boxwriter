/* C file: c:\MyPrograms\FoxJet1\_message.c created
 by NET+Works HTML to C Utility on Friday, February 09, 2001 17:02:12 */
#include "fj.h"
#include "fj_image.h"
#include "fj_dyntext.h"
#include "fj_text.h"
#include "fj_dynbarcode.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_memstorage.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_MSG_TABLE */
static void na_MSG_TABLE (unsigned long handle);
/*  @! File: na_DEF_MSG_ID */
static void na_DEF_MSG_ID (unsigned long handle);
/*  @! File: na_MSG_CT */
static void na_MSG_CT (unsigned long handle);
/*  @! File: na_EDIT_BUTTONS */
void na_EDIT_BUTTONS (unsigned long handle);
/*  @! File: na_TITLE_MESSAGES */
static void na_TITLE_MESSAGES (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_STAvailEdit */
void na_STAvailEdit (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\message.htm {{ */
void Send_function_17(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_MESSAGES (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Messages\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "  var of=document.formedit1;\n");
	HSSend (handle, "  of.action.value=sa;  of.item.value=si;  \n");
	HSSend (handle, "  of.submit();  }\n");
	HSSend (handle, "function startedit(msgnum) {\n");
	HSSend (handle, "  var str = \"message_edit.htm?msg_edit=\"+msgnum+\"&pwid=");
	na_PWID (handle);
	HSSend (handle, "\";\n");
	HSSend (handle, "  window.open(str,\"Editor\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1000,height=600\");\n");
	HSSend (handle, " }\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p>\n");
	HSSend (handle, "  <table border=0 width=100%>\n");
	HSSend (handle, "    <tr valign=bottom><td align=left><b>Selected Message Name: ");
	na_DEF_MSG_ID (handle);
	HSSend (handle, "</b></td><td align=left><b>Number of Messages: ");
	na_MSG_CT (handle);
	HSSend (handle, "</b></td><td align=right><b>Available Storage: ");
	na_STAvailEdit (handle);
	HSSend (handle, " &nbsp;&nbsp;&nbsp;&nbsp;</b>");
	na_EDIT_BUTTONS (handle);
	HSSend (handle, "</td></tr>\n");
	HSSend (handle, "  </table>\n");
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "  <table border=1>\n");
	HSSend (handle, "    ");
	na_MSG_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "  </table>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<form name=formedit1 action=msg_input method=POST>\n");
	HSSend (handle, "  <input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, ">\n");
	HSSend (handle, "  <input type=hidden value=none name=action >\n");
	HSSend (handle, "  <input type=hidden value=none name=item >\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\message.htm }} */
/* @! End HTML page   */

void reload_message(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=message.htm");
	}
	else
	{
		fj_reload_page(handle, "message.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_msg_input  {{ */
void Post_msg_input(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMESSAGE   pfm;
	char   formField[50];
	STR    sMsgName[FJMESSAGE_NAME_SIZE*2];
	ULONG  ipAdd;
	BOOL   bROMWait;
	BOOL   bEdit;
	BOOL   bRet;
	int    ret;

	bROMWait = FALSE;
	ipAdd = HSRemoteAddress(handle);
	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"edit_start") )
		{
			pCVT->fj_print_head_EditStart( ipAdd );
		}
		else if ( 0 == strcmp(formField,"edit_save") )
		{
			pCVT->fj_print_head_EditSave( ipAdd );
			bROMWait = TRUE;
		}
		else if ( 0 == strcmp(formField,"edit_cancel") )
		{
			pCVT->fj_print_head_EditCancel( ipAdd );
		}
		else if ( 0 == strcmp(formField,"delete") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				bEdit = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );
				if ( TRUE == bEdit )
				{
					// message Name
					pCVT->fj_processParameterStringOutput( sMsgName, formField );
					pfm = pCVT->fj_MessageBuildFromName( pfph->pMemStorEdit, sMsgName );
					if ( NULL != pfm )
					{
						bRet = pCVT->fj_MessageDeleteFromMemstor( pfph->pMemStorEdit, pfm );
						pCVT->fj_MessageDestroy( pfm );
					}
				}
			}
		}
	}
	reload_message(handle,bROMWait);
}										/* @! Function: Post_msg_input  }} */
/* @! Function: Post_msg_edit  {{ */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_ID  {{ */
/* @! Function: na_MSG_TABLE  {{ */
void na_MSG_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMEMSTOR pfms;
	LPFJMESSAGE pfmSelected;
	LPFJMESSAGE pfm;
	LPFJELEMENT pfe;
	LPSTR      *papMsg;
	LPSTR       pSt[3];
	STR         strText[3][33];
	BOOL        bEdit;
	ULONG       ipAdd;
	LONG        lLevel;
	int i, j;
	int jSeg;

	ipAdd = HSRemoteAddress(handle);
	lLevel = fj_get_level(handle);
	bEdit = pCVT->fj_PrintHeadIsEdit(pfph, ipAdd);
	HSSend(handle, "<tr align=center>");
	if ( TRUE == bEdit )
	{
		pfms = pfph->pMemStorEdit;
		HSSend(handle, "<td colspan=2><button onClick=\"startedit('-1');\">Create New Message</button></td>");
	}
	else
	{
		pfms = pfph->pMemStor;
		HSSend(handle, "<td>&nbsp;</td><td>&nbsp;</td>");
	}
	HSSend(handle, "<td><b>Message Name</b></td>");
	HSSend(handle, "<td><b>Element Count</b></td>");
	HSSend(handle, "<td><b>Element 1</b></td>");
	HSSend(handle, "<td><b>Element 2</b></td>");
	HSSend(handle, "<td><b>Element 3</b></td></tr>\n");

	pfmSelected = pfph->pfmSelected;
	papMsg = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_MessageID );
	for ( i = 0; NULL != *(papMsg+i); i++ )
	{
		pfm = pCVT->fj_MessageBuildFromString( pfms, *(papMsg+i) );
		if ( NULL != pfm )
		{
			pfm->pfph = pfph;
			HSSend(handle, "<tr>");
			if ( TRUE == bEdit )
			{
				HSSend(handle, "<td><center><button onClick='submit1(\"delete\",\"");
				na_String(handle, pfm->strName);
				HSSend(handle, "\")'>Delete</button></center></td>\n");

				HSSend(handle, "<td><center><button onClick=\"startedit('");
				na_String(handle, pfm->strName);
				HSSend(handle, "')\">Edit</button></center></td>");
			}
			else
			{
				if ( (NULL != pfmSelected) && (0 == strcmp(pfmSelected->strName, pfm->strName)) )
				{
					HSSend(handle, "<td><center>Selected</center></td>");
				}
				else
				{
					HSSend(handle, "<td>&nbsp;</td>");
				}
				HSSend(handle, "<td><center><button onClick=\"startedit('");
				na_String(handle, pfm->strName);
				HSSend(handle, "')\">Details</button></center></td>");
			}
			HSSend(handle, "<td><center>");
			na_String(handle, pfm->strName);
			HSSend(handle, "</center></td>\n");

			HSSend(handle, "<td><center>");
			na_Long(handle, pfm->lCount);
			HSSend(handle, "</center></td>");
			pSt[0] = pSt[1] = pSt[2] = NULL;
			for ( j = 0 ; ((j<pfm->lCount) && (j<3)); j++ )
			{
				pfe = pfm->papfe[j];
				if ( NULL != pfe )
				{
					pfe->pActions->fj_DescGetTextString( pfe, strText[j], 32 );
					if ( 0 != strText[j][0] ) pSt[j] = strText[j];
				}
			}
			for ( j = 0 ; j < 3; j++ )
			{
				HSSend(handle, "<td>");
				if ( NULL == pSt[j] ) HSSend(handle, "&nbsp;");
				else                  na_String(handle, pSt[j]);
				HSSend(handle, "</td>");
			}
			HSSend(handle, "</tr>\n");
			pCVT->fj_MessageDestroy( pfm );
		}
	}
}										/* @! Function: na_MSG_TABLE  }} */
/* @! Function: na_DEF_MSG_ID  {{ */
void na_DEF_MSG_ID(unsigned long handle)
{
	/* add your implementation here: */
	if ( NULL != pCVT->pfph->pfmSelected ) na_String(handle, pCVT->pfph->pfmSelected->strName);
	else                                   HSSend(handle, "(none)" );
}										/* @! Function: na_DEF_MSG_ID  }} */
/* @! Function: na_MSG_CT  {{ */
void na_MSG_CT(unsigned long handle)
{
	/* add your implementation here: */
	LPFJMEMSTOR pfms;
	LPSTR *papMsg;
	int i = 0;

	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pCVT->pfph, HSRemoteAddress(handle)) )
		pfms = pCVT->pfph->pMemStorEdit;
	else pfms = pCVT->pfph->pMemStor;
	papMsg = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_MessageID );
	while ( NULL != *(papMsg+i) ) i++;
	na_Long(handle, i);
}										/* @! Function: na_MSG_CT  }} */
/* @! Function: na_EDIT_BUTTONS  {{ */
/* @! Function: na_TITLE_MESSAGES  {{ */
void na_TITLE_MESSAGES(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Messages" );
}										/* @! Function: na_TITLE_MESSAGES  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_STAvailEdit  {{ */
/* @! End of dynamic functions   */
