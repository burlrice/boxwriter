/* C file: C:\netos\src\examples\nahttp\url.c created
 by NET+Work Web to C Utility on Tuesday, January 11, 2000 14:04:08 */

#include <string.h>

#include <hservapi.h>
#include "fj.h"
#include "fj_system.h"

/* @! Begin Function Prototypes */
extern void Send_function_0 (unsigned long);
extern void Send_function_1 (unsigned long);
extern void Send_function_2 (unsigned long);
extern void Send_function_3 (unsigned long);
extern void Send_function_4 (unsigned long);
extern void Send_function_5 (unsigned long);
extern void Send_function_6 (unsigned long);
extern void Send_function_7 (unsigned long);
extern void Send_function_8 (unsigned long);
extern void Send_function_9 (unsigned long);
extern void Send_function_10 (unsigned long);
extern void Send_function_11 (unsigned long);
extern void Send_function_12 (unsigned long);
extern void Send_function_13 (unsigned long);
extern void Send_function_14 (unsigned long);
extern void Send_function_15 (unsigned long);
extern void Send_function_16 (unsigned long);
extern void Send_function_17 (unsigned long);
extern void Send_function_18 (unsigned long);
extern void Send_function_19 (unsigned long);
extern void Send_function_20 (unsigned long);
extern void Send_function_21 (unsigned long);
extern void Send_function_22 (unsigned long);
extern void Send_function_23 (unsigned long);
extern void Send_function_24 (unsigned long);
extern void Send_function_25 (unsigned long);
extern void Send_function_26 (unsigned long);
extern void Send_function_27 (unsigned long);
extern void Send_function_28 (unsigned long);
extern void Send_function_29 (unsigned long);
extern void Send_function_30 (unsigned long);
extern void Send_function_31 (unsigned long);
extern void Send_function_32 (unsigned long);
extern void Send_function_33 (unsigned long);
extern void Send_function_34 (unsigned long);
//extern void Post_manu_1 (unsigned long);
extern void Post_manu_2 (unsigned long);
extern void Post_manu_3 (unsigned long);
//extern void Post_manu_4 (unsigned long);
extern void Post_dist_1 (unsigned long);
extern void Post_stat_1 (unsigned long);
extern void Post_systemlayout_1 (unsigned long);
extern void Post_printerlayout_1 (unsigned long);
extern void Post_test_1 (unsigned long);
extern void Post_msgedit_1 (unsigned long);
extern void Post_info_1 (unsigned long);
extern void Post_label_input (unsigned long);
extern void Post_msg_input (unsigned long);
extern void Post_msg_edit (unsigned long);
extern void Post_msg_edit_input (unsigned long);
extern void Post_msgedit_delete (unsigned long);
extern void Post_lbledit_1 (unsigned long);
extern void Post_lbledit_delete (unsigned long);
extern void Post_msgadd_1 (unsigned long);
extern void Post_msgcopy_1 (unsigned long);
extern void Post_time_1 (unsigned long);
extern void Post_time_a (unsigned long);
extern void Post_font_input (unsigned long);
extern void Post_bitmap_input (unsigned long);
extern void Post_eth_1 (unsigned long);
extern void Post_pw_1 (unsigned long);
extern void Post_pw_a (unsigned long);
extern void Post_stat_2 (unsigned long);
extern void Post_cc_1 (unsigned long);
extern void Post_manu_4 (unsigned long);
extern void Post_vardata_input (unsigned long);
extern void Send_function_35 (unsigned long);
/* @! End Function Prototypes   */

typedef void (*SendFn)(unsigned long handle);
typedef void (*HSTypeFnHtml)(unsigned long);
typedef void (*HSTypeFnBinary)(unsigned long, int);

void *HSTypeFJLogo(unsigned long handle, int size)
{
	HSTypeJpeg(handle, pCVT->pFJglobal->lLogoSize);
}
void Send_function_FJLogo(unsigned long handle)
{
	HSSendBinary (handle, (char *)pCVT->pFJglobal->pLogo, pCVT->pFJglobal->lLogoSize);
}
#define Unprotected (0x0000)
#define Realm1     (0x0001)
#define Realm2     (Realm1 << 1)
#define Realm3     (Realm2 << 1)
#define Realm4     (Realm3 << 1)
#define Realm5     (Realm4 << 1)
#define Realm6     (Realm5 << 1)
#define Realm7     (Realm6 << 1)
#define Realm8     (Realm7 << 1)

#define AllRealms  (0xFF)

#define REALM1     0
#define REALM2     1
#define REALM3     2
#define REALM4     3
#define REALM5     4
#define REALM6     5
#define REALM7     6
#define REALM8     7

typedef struct
{
	char*          url;
	unsigned       html;				/* use HSTypeHtml if 1 */
	SendFn         fun;					/* Send_Function_XXX or Post_XXX */
	void           (*hstype)();			/* HSType function to call */
	int            len;					/* size of binary data */
	unsigned short realm;				/* security realm */
} URLTableEntry;

/* Warning: each entry MUST be on the same line */
/* @! Begin of Table for all URLs */
URLTableEntry URLTable[] =
{
	"/", 1, Send_function_34, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_FIRST,
	"/printhead.htm", 1, Send_function_20, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_FIRST,
	"/header.htm", 1, Send_function_0, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/font.htm", 1, Send_function_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/versioninfo.htm", 1, Send_function_2, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/fjlogo.jpg", 0, Send_function_FJLogo, (HSTypeFnBinary)HSTypeFJLogo, 2074, 0,
	"/fjsocket.class", 0, Send_function_4, (HSTypeFnBinary)HSTypeApplet, 10554, 0,
	"/fjlight.class", 0, Send_function_5, (HSTypeFnBinary)HSTypeApplet, 3390, 0,
	"/fjparameter.class", 0, Send_function_6, (HSTypeFnBinary)HSTypeApplet, 2708, 0,
	"/fjipstyle1.css", 0, Send_function_7, (HSTypeFnBinary)HSTypeApplet, 1846, 0,
	"/fjscript_edit.htm", 1, Send_function_8, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/fjscript_layers.htm", 1, Send_function_9, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/fjscript_menu.htm", 1, Send_function_10, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/fjscript_menu_func.htm", 1, Send_function_11, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/distributor.htm", 1, Send_function_12, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_DISTRIBUTOR,
	"/ethernet.htm", 1, Send_function_13, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/label.htm", 1, Send_function_14, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/label_edit.htm", 1, Send_function_15, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	//FJSYS_PRIORITY_MANUFACTURING,
	"/manufacturing.htm", 1, Send_function_16, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/message.htm", 1, Send_function_17, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/message_edit.htm", 1, Send_function_18, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/printerconfig.htm", 1, Send_function_19, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/romwait.htm", 1, Send_function_21, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/statistics.htm", 1, Send_function_22, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/systeminfo.htm", 1, Send_function_24, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/test.htm", 1, Send_function_25, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_TEST,
	"/time.htm", 1, Send_function_26, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/fjinteger.class", 0, Send_function_27, (HSTypeFnBinary)HSTypeApplet, 2377, 0,
	"/bitmap.htm", 1, Send_function_28, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_VIEW_ALL,
	"/maintenance.htm", 1, Send_function_29, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_TEST,
	"/printer_slant.htm", 1, Send_function_30, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/notauthorized.htm", 1, Send_function_31, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/password.htm", 1, Send_function_32, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_SECURITY,
	"/counter_change.htm", 1, Send_function_33, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/systemlayout_1", 1, Post_systemlayout_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/printerlayout_1", 1, Post_printerlayout_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	//"/manu_1", 1, Post_manu_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MANUFACTURING,
	//FJSYS_PRIORITY_MANUFACTURING,
	"/manu_2", 1, Post_manu_2, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	//FJSYS_PRIORITY_MANUFACTURING,
	"/manu_3", 1, Post_manu_3, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	//"/manu_4", 1, Post_manu_4, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MANUFACTURING,
	"/dist_1", 1, Post_dist_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_DISTRIBUTOR,
	"/stat_1", 1, Post_stat_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_COUNTERS,
	"/test_1", 1, Post_test_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_TEST,
	"/msgedit_1", 1, Post_msgedit_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	"/info_1", 1, Post_info_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	// 0 level is OK. Buttons to get here are not enable unless security level is correct.
	"/label_input", 1, Post_label_input, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	// 0 level is OK. Buttons to get here are not enable unless security level is correct.
	"/msg_input", 1, Post_msg_input, (HSTypeFnHtml)HSTypeHtml, 0, 0,
	"/lbledit_1", 1, Post_lbledit_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	"/time_1", 1, Post_time_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/time_a", 1, Post_time_a, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/font_input", 1, Post_font_input, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	"/bitmap_input", 1, Post_bitmap_input, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	"/eth_1", 1, Post_eth_1, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_CONFIGURATION,
	"/pw_1", 1, Post_pw_1, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/pw_a", 1, Post_pw_a, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/stat_2", 1, Post_stat_2, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/cc_1", 1, Post_cc_1, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/manu_4", 1, Post_manu_4, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/manufacturing.htm", 1, Send_function_16, (HSTypeFnHtml)HSTypeHtml, 0, Unprotected,
	"/index.htm", 1, Send_function_34, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_FIRST,
	"/vardata_input", 1, Post_vardata_input, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	"/vardata.htm", 1, Send_function_35, (HSTypeFnHtml)HSTypeHtml, 0, FJSYS_PRIORITY_MESSAGE_EDIT,
	NULL								/* last entry MUST be NULL */
};
/* @! End of Table for all URLs   */

/*
 * fj_SearchURL
 *
 * This is an application URL search routine.
 * Returns -1 if the url is not found.
 * Returns the index number into the URL table, if found.
 *
 */
int fj_SearchURL (unsigned long handle, char *URLp)
{
	char lower[280];
	char lower2[280];
	int len;
	int i;
	int ret = -1;

	strcpy( lower, URLp );
	lower2[0] = 0;
	len = strlen( lower );
	for ( i = 0; i < len; i++ )
	{
		lower[i] = tolower( lower[i] );
	}
										// if it ends in .html,
	if ( 0 == strcmp( ".html", &(lower[len-5]) ) )
	{
		len--;							//      remove the l
		lower[len] = 0;
	}
	if ( len > 1 )
	{
		if ( 0 == strchr( lower, '.' ) )// if no .,
		{
			strcpy( lower2, lower );	//    add .htm
			strcat( lower2, ".htm" );	//    add .htm
		}
	}

	i = 0;
	while ( (URLTable[i].url) && (-1==ret) )
	{
		if ( 0 == strcmp(URLTable[i].url, lower) )
		{
			ret = i;
		}
		else if ( 0 == strcmp(URLTable[i].url, lower2) )
		{
			ret = i;
		}
		i++;
	}

	return ret;
}										/* @! End FoxJet search URL function    */
/* @! Begin pure search URL function  */
/*
 * SearchURL
 *
 * This is an application URL search routine.
 * Returns 1 if the url is found, 0 otherwise.
 *
 * modified to use FoxJet search but return old values
 *
 */
int SearchURL (unsigned long handle, char *URLp)
{
	int i;
	i = fj_SearchURL( handle, URLp );
	if ( -1 == i ) i = 0;
	else           i = 1;
	return i;
}										/* @! End pure search URL function    */
/* @! Begin search URL function  */
/*
 * AppSearchURL
 *
 * This is an application URL search routine.
 * It will also send data back to the browser.
 *
 * modified for FoxJet
 *
 */
void AppSearchURL (unsigned long handle, char *URLp)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;

	char pwid[1+FJSYS_PASSWORD_SIZE];
	unsigned long ipaddr;
	long level;
	int ret;
	int i;
	i = fj_SearchURL( handle, URLp );
	if ( -1 != i )
	{
		pwid[0] = 0;
		ret = fj_HSGetValue( handle, "pwid", pwid, 1+FJSYS_PASSWORD_SIZE );
		if ( (5==i) || (6==i) || (7==i) || (8==i) || (9==i) || (10==i) || (11==i) || (13==i) || (28==i) )
		{
			ret = 43;
		}
		else
		{
			if ( 0 == pwid[0] )
			{
				ret = 43 + *URLp;
			}
		}
		ipaddr = HSRemoteAddress(handle);
		level = pCVT->fj_SystemGetLevelID( pCVT->pfph, ipaddr, pwid );
		if ( (level < URLTable[i].realm) || (TRUE == pGlobal->bReset) )
		{
			i = fj_SearchURL( handle, "/notauthorized.htm" );
		}
		if ( -1 != i )
		{
			(*URLTable[i].fun) (handle);
		}
	}
	return;
}										/* @! End search URL function    */
/* @! Begin pre-process function */
/*
 * AppPreprocessURL
 *
 * This routine does all the pre-processing for the application's pages.
 *
 * modified for FoxJet
 *
 */
void AppPreprocessURL (unsigned long handle, char *URLp)
{
	int i;
	i = fj_SearchURL( handle, URLp );
	if ( -1 != i )
	{
		char str[88];
		strcpy(str,"URL fetch = ");
		strcat(str,URLTable[i].url);
		strcat(str,"\n");
		//					    fj_writeInfo(str);
		//		HSPageAuthenticate (handle, URLTable[i].realm);
		if (URLTable[i].html)
			(*URLTable[i].hstype) (handle);
		else
			(*URLTable[i].hstype) (handle, URLTable[i].len);
		/* put your own pre-processing here: */
	}
	return;
}										/* @! End pre-process function   */
/* @! Begin of Setup Security Table */
/*
 * HSInitSecurityTable
 *
 * This will be called once when the web server starts.
 * You should setup realm's user/password here.
 *
 */
int HSInitSecurityTable ()
{
	/* @! Begin of HSSetRealm calls */
	//    HSSetRealm (REALM1, "OurRealm", "Netsilicon", "sysadm");
	/* @! End   of HSSetRealm calls */
}										/* @! End   of Setup Security Table */
