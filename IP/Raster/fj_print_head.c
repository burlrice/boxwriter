#include "fj_netsilicon.h"
#include <narmled.h>
#include <na_isr.h>
#include <narmsrln.h>					// for NETOS ROM parameters
#include "fj.h"
#include "fj_memstorage.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_counter.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"

FLOAT fj_PrintHeadGetInkLowVolume (CLPCFJPRINTHEAD pfph)
{
    if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
	return FJSYS_MAX_INKLOW_UJ2_192_32NP_ML;

    return FJSYS_MAX_INKLOW_ML;
}

//
// set the print head status for a heater
//
BOOL fj_print_head_SetStatusHeater( LPHEATER pHeater )
{
	BOOL     bStatusReady;

	bStatusReady = TRUE;
	return( bStatusReady );
}
//
// set the print head status
//
VOID fj_print_head_SetStatus(VOID)
{
	CLPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD  pfph    = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop    = fj_CVT.pfph->pfop;
	LPFJ_BRAM        pBRAM   = fj_CVT.pBRAM;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	LPSTRING pStatus;
	BOOL     bStatusReady;

	pStatus = "Ready";
	bStatusReady = TRUE;
	if ( TRUE == pGlobal->bReset )
	{
		bStatusReady = FALSE;
		pStatus = "Please reboot printer now!";
	}
	else if ( FALSE == pfph->bHeadSelected )
	{
		bStatusReady = FALSE;
		pStatus = "Head type not selected";
	}
	else if ( FALSE == pGlobal->bADReady )
	{
		bStatusReady = FALSE;
		pStatus = "A/D values not read";
	}
	else if ( FALSE == pGlobal->bPrintHeadPresent )
	{
		bStatusReady = FALSE;
		pStatus = "Print Head not present";
	}
	else if ( FALSE == wStatus.bits.AtTemp )
	{
		bStatusReady = FALSE;
		pStatus = "Print Head not at operating temperature";
	}
	else if (fj_PrintHeadGetInkLowVolume (pfph) <= pBRAM->dInkLowML )
	{
		bStatusReady = FALSE;
		pStatus = "Printing halted due to Out of Ink condition. Replace Inkbottle.";
	}
	// Bypass for old board shoud be here!!!!!!!!!
	else if ( FALSE == pGlobal->bVoltageReady )
	{
		bStatusReady = FALSE;
		pStatus = "High Voltage not present";
		fj_print_head_SendVoltageInfo();
	}
	else if ( TRUE == pGlobal->bStandByOn )
	{
		pStatus = "StandBy";
	}
	else if ( TRUE == pBRAM->bPrintIdle )
	{
		pStatus = "Idle";
	}
	else if ( TRUE == pBRAM->bInkLow )
	{
		pStatus = "Low Ink";
	}

	strcpy( pGlobal->strHeadStatus, pStatus );

	if ( TRUE == pGlobal->bGAUpgrade )
	{
		strcat(pGlobal->strHeadStatus, ". GA has been updated.");
	}

	if ( (TRUE == bStatusReady) && (0 != pfop->lTemp1) )
	{
		bStatusReady = fj_print_head_SetStatusHeater( &(pGlobal->heater1) );
	}
	pGlobal->bReady = bStatusReady;
	fj_print_head_SetGAWriteSwitches();
	return;
}
//
// start an edit session
//
BOOL fj_print_head_EditStart( ULONG ipAdd )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;
	BOOL   bRet;

	bRet = FALSE;
	if ( TX_SUCCESS == tx_semaphore_get( &(pGlobal->semaEditLock), TX_NO_WAIT ) )
	{
										// set up MemStor for editing
		bRet = fj_PrintHeadStartEdit( pfph, ipAdd );
		if ( FALSE == bRet )
		{
			tx_semaphore_put( &(pGlobal->semaEditLock) );
		}
		fj_print_head_SendEditStatus();
	}
	return ( bRet );
}
//
// cancel an edit session
//
VOID fj_print_head_EditCancel( ULONG ipAdd )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;

	if ( 0 == pGlobal->semaEditLock.tx_semaphore_count )
	{
		if ( TRUE == fj_PrintHeadCancelEdit( pfph, ipAdd ) )
		{
			tx_semaphore_put( &(pGlobal->semaEditLock) );
			fj_print_head_SendEditStatus();
		}
	}
}
//
// save an edit session
//
VOID fj_print_head_EditSave( ULONG ipAdd )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;
	LPFJSYSTEM    pfsys   = fj_CVT.pfsys;
	LPFJ_PARAM    pParam  = fj_CVT.pFJparam;
	LPFJPRINTHEAD pfphP   = &(pParam->FJPh);
	ROMQUEUE      q;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJMESSAGE pfm;
	LPFJELEMENT pfe;
	LPFJCOUNTER pfc;
	int    retrom,i,x;
	struct tm tm;

	if ( 0 == pGlobal->semaEditLock.tx_semaphore_count )
	{
		tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
		pCVT->fj_SystemGetTime( &tm );
		if ( TRUE == fj_PrintHeadSaveEdit( pfph, ipAdd, &tm ) )
		{
			fj_SystemBuildSelectedLabel( fj_CVT.pfsys, fj_CVT.pBRAM->strLabelName );
			pCVT->fj_RestoreBRAMCounters( pfph->pfmSelected );
/*
			if ( 0 != pBRAM->strLabelName[0] )
			{
				for ( i = 0; i < FJSYS_NUMBER_LABEL_COUNTERS; i++ )
				{
					if ( 0 == strcmp( pBRAM->LabelCounters[i].cLabelName, pBRAM->strLabelName))
					{
						// Burl v 1.1015 // pBRAM->lSelectedCount = 0;
						x = 0;
						pfm = pfph->pfmSelected;
						if ( NULL != pfm )
						{
							while ( x < pfm->lCount )
							{
								pfe = pfm->papfe[x];
								if ( FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType() )
								{
									pfc = (LPFJCOUNTER)pfe->pDesc;
									pfc->lCurValue = pfc->lStart;
								}
								x++;
							}
						}
						break;
					}
				}
			}
			*/
			fj_print_head_SendEditStatus();

			// write edit time
			retrom = fj_ReadFJRomParams( pParam );
			if ( 0 == retrom  )
			{
				memcpy( &(pfphP->tmEditVersion), &tm, sizeof(struct tm) );
				// write and keep ROM lock
				fj_WriteFJRomParams( pParam, FALSE, NULL, 0 );
			}
			
			// write MemStor
			q.lRomEntry   = ROM_FONTMSG;
			q.pBuffer     = pfph->pMemStor->pBuffer;
			q.lBufferSize = pfph->pMemStor->lSize;
			q.bFreeBuffer = FALSE;
			q.bFreeLock   = TRUE;
			q.pCallBack   = NULL;
			// write and free ROM lock
			tx_queue_send( &(pGlobal->qROM), &q, TX_WAIT_FOREVER );

			tx_semaphore_put( &(pGlobal->semaEditLock) );
		}
		else
		{
			tx_semaphore_put( &(pGlobal->semaROMLock) );
		}
	}
}
//
// schedule AMS
//
VOID fj_print_head_ScheduleAMS( VOID )
{
	// seconds - time for next AMS to be scheduled
	fj_CVT.pBRAM->lAMS_SST = fj_CVT.pFJglobal->lSeconds + (LONG)(fj_CVT.pfph->fAMSInterval*60.*60.);
	return;
}
VOID fj_print_head_ScheduleStandBy( VOID )
{
	// seconds - time for next AMS to be scheduled
	fj_CVT.pBRAM->lSTDB_SST = fj_CVT.pFJglobal->lSeconds + (LONG)(fj_CVT.pfph->fStandByInactive*60.*60.);
	return;
}
//
// check if time for AMS cycle
//
static BOOL bSetAMS_PC_Dis = FALSE;
VOID fj_print_head_AMSCheck( VOID )
{
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_BRAM       pBRAM    = fj_CVT.pBRAM;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;

	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfphP       = &(pParam->FJPh);

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;
										// purge in progress?
	if ( 0 != pGlobal->gaRead.rw.read.status.bits.AMSpurge )
	{
		if ( NULL != pfph->pfcFirst )	// yes, any printing?
		{
			pBRAM->bAMS_on = FALSE;		// stop purge!!!!!!
			fj_CVT.pGA->rw.write.AMScontrol = GA_AMS_STOP;
			// save
			fj_CVT.pFJglobal->gaWrite.rw.write.AMScontrol = GA_AMS_STOP;
		}
		if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
		{
			pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
			bSetAMS_PC_Dis = TRUE;
			pCVT->fj_print_head_SendPhotocellMode();
		}
	}
	else								// purge not in progress
	{
		if ( FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode && bSetAMS_PC_Dis != FALSE)
		{
			pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
			pCVT->fj_print_head_SendPhotocellMode();
			bSetAMS_PC_Dis = FALSE;
		}
		pBRAM->bAMS_on = FALSE;			// turn off our flag in case an AMS cycle just ended
										// is it time for a AMS purge?
		if ( pBRAM->lAMS_SST <= pGlobal->lSeconds )
		{
			if (TRUE == pfphP->bAPSenable)
			{
				pBRAM->bAMS_wait = TRUE;// yes, start wait for inactivity
				pBRAM->lAMS_SST_wait = pGlobal->lSeconds + pfph->lAMSInactive;
										// reschedule new AMS cycle
				fj_print_head_ScheduleAMS();
			}
			else
			{
				pBRAM->bAMS_wait = FALSE;
			}
		}
		if ( TRUE == pBRAM->bAMS_wait )	// waiting for an inactive period?
		{
										// any activity?
			if ( NULL != pfph->pfcFirst )
			{
				// yes, reschedule
				pBRAM->lAMS_SST_wait = pGlobal->lSeconds + pfph->lAMSInactive;
			}
			else						// no activity
			{
				// is inactive period complete?
				if ( pBRAM->lAMS_SST_wait <= pGlobal->lSeconds )
				{
					if ( FALSE == pBRAM->bInkLow)
					{
										// yes, start AMS purge
						pBRAM->bAMS_on = TRUE;
						pBRAM->bAMS_wait = FALSE;
						fj_CVT.pGA->rw.write.AMScontrol = GA_AMS_START;
						// save
						fj_CVT.pFJglobal->gaWrite.rw.write.AMScontrol = GA_AMS_START;
					}
					else
						// reschedule
						pBRAM->lAMS_SST_wait = pGlobal->lSeconds + pfph->lAMSInactive;
				}
			}
		}
	}

	return;
}
//
// Check the ink level
//
VOID fj_print_head_InkCheck(VOID)
{
	CLPFJ_GLOBAL pGlobal   = fj_CVT.pFJglobal;
	CLPFJ_BRAM   pBRAM     = fj_CVT.pBRAM;
	CLPCFJ_PARAM pFJparam  = fj_CVT.pFJparam;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;
	STR str[88];

	//	if ( 512 < pGlobal->adValues.wInkLevel )	// high number is HIGH
	if ( 0 != wStatus.bits.InkLevelLow )// high number is HIGH
	{
		if ( TRUE == pBRAM->bInkLow )	// was LOW,
		{
			pBRAM->bInkLow = FALSE;		// now HIGH
										// send Low Ink flag to GA
			fj_print_head_SetGAWriteSwitches();

			if ( FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}

										// ZAP countInkLow
			pBRAM->countInkLow.lMessages     = 0;
			pBRAM->countInkLow.lDotsHi       = 0;
			pBRAM->countInkLow.lDotsLo       = 0;
			pBRAM->dInkLowML = 0.0;
			pBRAM->bInkLowMax= FALSE;
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();

			fj_SystemGetTime( &(pBRAM->tmInkFull) );
		}
	}
	else
	{
		if ( FALSE == pBRAM->bInkLow )	// was HIGH,
		{
			pBRAM->bInkLow = TRUE;		// now LOW
										// send Low Ink flag to GA
			fj_print_head_SetGAWriteSwitches();
			fj_SystemGetTime( &(pBRAM->tmInkLow) );
			fj_print_head_IOBoxInfo();
			sprintf( str, "Printer %s in group %s at %u.%u.%u.%u is low on ink.", fj_CVT.pfph->strID, fj_CVT.pfsys->strID, PUSH_IPADDR(((ULONG)fj_CVT.pfph->lIPaddr)) );
			fj_print_head_SendEmailInk( str, "Low Ink" );
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();
		}
		else if ( (fj_PrintHeadGetInkLowVolume (fj_CVT.pfph) <= pBRAM->dInkLowML) && ( FALSE == pBRAM->bInkLowMax ))
		{
			pBRAM->bInkLowMax = TRUE;
			if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
				pCVT->fj_print_head_SendPhotocellMode();
				sprintf( str, "Printer %s in group %s at %u.%u.%u.%u is out of ink.", fj_CVT.pfph->strID, fj_CVT.pfsys->strID, PUSH_IPADDR(((ULONG)fj_CVT.pfph->lIPaddr)) );
				fj_print_head_SendEmailInk( str, "Out of Ink" );
			}
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();
		}
	}
	return;
}
//
// Check the board temperature
//
VOID fj_print_head_BoardTempCheck(VOID)
{
	CLPFJ_GLOBAL pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM pFJparam = fj_CVT.pFJparam;
	LPCTEMPINFO  *paTempInfo;
	LPCTEMPINFO  pTempInfo;
	WORD16 (*pawTempTable)[];			// pointer to array of A/D values
	WORD16  wFirstTemp;					// first temp in table
	WORD16  wLastTemp;					// last temp in table
	LONG    lTemp;						// current temp - degrees C
	int     i,l;

	paTempInfo = fj_CVT.paTempInfo;
	pTempInfo = paTempInfo[0];
										// board thermistor is same as 60 degree head
	pawTempTable = pTempInfo->pawTempTable;
	wFirstTemp = pTempInfo->wFirstTemp;
	wLastTemp  = pTempInfo->wLastTemp;
	l = wLastTemp - wFirstTemp;
	// don't use first and last entry in table.
	// the algorithms will look at the entries before and after the one found.
	for ( i = 1; i < l; i++ )
	{
		// temperature found
		if ( pGlobal->adValues.wTempBoard >= (*pawTempTable)[i] )
		{
										// get current time
			ULONG lNow = pGlobal->lSeconds;
			static LONG lTimePrev = 0;
			lTemp = wFirstTemp + i;
			if ( lTemp != pGlobal->lBoardTemp )
			{
				if ( lNow > (lTimePrev + 2) )
				{
					lTimePrev = lNow;
					pGlobal->lBoardTemp = lTemp;
					fj_print_head_SendBoardTempInfo();
				}
			}
			else
			{
				lTimePrev = lNow;
			}
			break;
		}
	}
	return;
}
// forward declarations
const VOLTINFO fj_aVoltInfo[];

VOID fj_print_head_setPWMVoltageValue( VOID )
{
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM     pFJparam = fj_CVT.pFJparam;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop     = fj_CVT.pfph->pfop;
	LPCVOLTINFO  *paVoltInfo;
	LPCVOLTINFO  pVoltInfo;

	WORD16 (*pawVoltTable)[];			// pointer to array of A/D values
	LONG    lTableNumber;				// table number to convert reading to PWM value
	LONG    lVoltage;					// set voltage
	WORD16  wFirstVolt;					// first voltage entry in table
	WORD16  wLastVolt;					// last voltage entry in table
	int i,l;

	lVoltage = pfop->lVoltage;
	lTableNumber = pfop->lVoltageTable;
	if ( 0 != lTableNumber )
	{
		//		paVoltInfo = fj_CVT.paVoltInfo;
		//		pVoltInfo = paVoltInfo[lTableNumber];
		pVoltInfo = &(fj_aVoltInfo[lTableNumber]);
		pawVoltTable = pVoltInfo->pawVoltTable;
		wFirstVolt = pVoltInfo->wFirstVolt;
		wLastVolt  = pVoltInfo->wLastVolt;
		if ( 0 == lVoltage )
		{
			l = wLastVolt - wFirstVolt;
			// don't use first and last entry in table.
			// the algorithms will look at the entries before and after the one found.
			for ( i = 2; i < l*2; i+=2 )
			{
				// voltage entry found
				if ( pGlobal->adValues.wHeadVoltRequired <= (*pawVoltTable)[i+1] )
				{
					i -= 2;				// we want lower than this table entry
					pGlobal->lHighVoltageWanted = wFirstVolt + i/2;
										// pwm scale factor.
					pGlobal->lPWMHV = (*pawVoltTable)[i];
					break;
				}
			}
		}
		else
		{
			if ( lVoltage < wFirstVolt ) lVoltage = wFirstVolt;
			if ( lVoltage > wLastVolt  ) lVoltage = wLastVolt;
			// pwm scale factor.
			pGlobal->lPWMHV = (*pawVoltTable)[(lVoltage - wFirstVolt)*2];
			pGlobal->lHighVoltageWanted = pfop->lVoltage;
		}
	}
	else
	{
	}

	return;
}
//
// Check the print head high voltage
//
VOID fj_print_head_VoltageCheck(VOID)
{
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	LONG lVoltage;
	BOOL bVoltageReady;
	ULONG lNow;
	int   i;
	static LONG lTimePrev = 0;
	static LONG lTimePrevAdj = 0;

	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;

	if ( 0 == wStatus.bits.HVStatus )
	{
		bVoltageReady = FALSE;			// set not ready
	}
	else
	{
		bVoltageReady = TRUE;			// if reeady (within tolerance)
										// make it look right on to avoid reports +- one volt
		lVoltage = pGlobal->lHighVoltageWanted;
	}

	// set global status and report it, if READY condition has changed
	if ( bVoltageReady != pGlobal->bVoltageReady )
	{
		pGlobal->bVoltageReady = bVoltageReady;
		fj_print_head_SetStatus();
		fj_print_head_SendInfoAll();
	}
	//		}
	//	}
	return;
}
//
// Call the proper routine depending on which channel was last read
//
VOID fj_print_head_ADHeaterCheck( LPHEATER pHeater )
{
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM     pFJparam = fj_CVT.pFJparam;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop     = fj_CVT.pfph->pfop;
	BOOL  bHeaterOn;
	BOOL  bHeaterReady;
	ULONG lNow;
	int   i;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;
	if ( 0 != wStatus.bits.HeaterOn )   bHeaterOn = TRUE;
	else                    bHeaterOn = FALSE;
	if ( 0 != wStatus.bits.AtTemp )     bHeaterReady = TRUE;
	else                    bHeaterReady = FALSE;

	pHeater->bHeaterPresent = TRUE;		// force AD check

	if ( (bHeaterOn != pHeater->bHeaterOn) || (bHeaterReady != pHeater->bReady))
	{
		pHeater->bHeaterOn = bHeaterOn;
		pHeater->bReady    = bHeaterReady;
		fj_print_head_SetStatus();
		fj_print_head_SendInfoAll();
	}
	return;
}
//
// Call the proper routine depending on which channel was last read
//
VOID fj_print_head_ADCheck(VOID)
{
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM     pFJparam = fj_CVT.pFJparam;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop     = fj_CVT.pfph->pfop;
	LPHEATER         pHeater;
	//BOOL    bSendStatus;
	BOOL    bPresent;
	LONG    lChannel;
	//GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;

	if ( (TRUE == pGlobal->bADReady) && (TRUE == pfph->bHeadSelected) )
	{
		bPresent = TRUE;
		if ( TRUE == pfop->bForceHeadPresent )
		{
			if ( FALSE == pGlobal->bGAAvail ) bPresent = FALSE;
		}
		else
		{
			//			if ( 0 == wStatus.bits.HeadPresent )
			//			{
			//				bPresent = FALSE;
			//			}
		}
		if ( FALSE == bPresent )
		{
			if ( TRUE == pGlobal->bPrintHeadPresent )
			{
				if ( TRUE == pfph->bInfoStatus )
				{
					fj_writeInfo( "Head not present\n" );
				}
				pGlobal->bPrintHeadPresent = FALSE;
				fj_print_head_SetStatus();
				fj_print_head_SendInfoAll();
			}
		}
		else
		{
			if ( FALSE == pGlobal->bPrintHeadPresent )
			{
				if ( TRUE == pfph->bInfoStatus )
				{
					fj_writeInfo( "Head present\n" );
				}
				pGlobal->bPrintHeadPresent = TRUE;
				fj_print_head_SetStatus();
				fj_print_head_SendInfoAll();
			}
		}

		// the data returned during the last write was for the previous channel
		lChannel = pGlobal->lChannel - 1;
		if ( lChannel < 0 ) lChannel = 7;

		if ( 0 == lChannel )			// board temperature
		{
			//fj_print_head_BoardTempCheck();
		}

		if ( TRUE == pGlobal->bPrintHeadPresent )
		{
			if ( 1 == lChannel )		// head voltage reading
			{

			}

			else if ( 2 == lChannel )	// head voltage setting
			{
				fj_print_head_VoltageCheck();
			}

			else if ( 3 == lChannel )	// heater 1 temperature
			{
				pHeater = &(pGlobal->heater1);
				pHeater->wADValue = pGlobal->adValues.wTemp1;
				fj_print_head_ADHeaterCheck( pHeater );
			}
			//else if ( 4 == lChannel )			// ink low
			//{
			fj_print_head_InkCheck();
			//}
		}
	}
	return;
}
//
// check if time to activate StandBy mode
//
fj_print_head_StandByCheck( VOID )
{
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPCFJPRINTHEAD  pfph  = fj_CVT.pfph;

	if ( TRUE == pfph->bStandByEnable && FALSE == pGlobal->bStandByOn )
	{
		if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
		{
										// any activity?
			if ( NULL != pfph->pfcFirst )
			{
				fj_print_head_ScheduleStandBy();
			}
										// is it time
			if ( pBRAM->lSTDB_SST <= pGlobal->lSeconds )
			{
				pGlobal->bStandByOn = TRUE;
				if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
				{
					pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
					pCVT->fj_print_head_SendPhotocellMode();
				}
				fj_print_head_ScheduleStandBy();
			}
			fj_print_head_SetGAWriteSwitches();
		}
	}
}
//
// check the print head
//
VOID fj_print_head_Check(VOID)
{
	LPFJPRINTHEAD  pfph = fj_CVT.pfph;
        struct tm tmNow;
	time_t now;
	time_t last;
	double dDiff;

	if (!pfph->bEdit && fj_bKeepAlive) {
	    pCVT->fj_SystemGetTime(&tmNow);
	    now = mktime (&tmNow);
	    last = mktime (&fj_tmKeepAlive);
	    dDiff = difftime (now, last);

	    if (dDiff >= 60.0) {
		fj_reboot ();
		return;
	    }
	}
		
	fj_print_head_PhotoCellCheck();
	fj_print_head_RasterCheck();
	fj_print_head_AMSCheck();
	fj_print_head_StandByCheck();
	
	if ( TRUE != fj_noPrintDev )		// if true, do not use any FoxJet chips and/or devices
	{
		fj_print_head_ADRead();
		fj_print_head_ADCheck();
		fj_print_head_ShaftEncoderCheck();
	}
	if ( TRUE == fj_CVT.pFJglobal->bBuildFailure )
	{
		fj_CVT.pFJglobal->bBuildFailure = FALSE;
		fj_writeInfo( "Message not built in time. Not enough distance between PhotoCell and PrintHead.\n" );
	}
	if ( TRUE == fj_CVT.pFJglobal->bBufferFailure )
	{
		fj_CVT.pFJglobal->bBufferFailure = FALSE;
		fj_writeInfo( "FIFO buffer not filled in time. Message is too complex. Reduce DPI or FPM.\n" );
	}

	return;
}
