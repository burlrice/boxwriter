#ifndef FJ_DYNTEXT
#define FJ_DYNTEXT

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjfont;

typedef struct fjdyntext
{
	LONG     lBoldValue;				// amount of boldness. 0 origin.
	LONG     lWidthValue;				// amount of width stretch. 1 origin.
	LONG     lGapValue;					// amount of gap between characters
	LONG     lDynamicID;				// ID of dinamic text segment
	LONG     lLength;					// length of string output. 0 = 'natural' ( no alteration )
	LONG     lFirstChar;				// start pos in dinamic field
	LONG     lNumChar;					// number of charecters to use dinamic field
	CHAR     cAlign;					// alignment of value in larger length - L/R
	CHAR     cFill;						// fill character. 0 is no fill.
										// font name
	STR      strFontName[1+FJFONT_NAME_SIZE];
	struct fjfont FAR * pff;			// pointer to font structure
} FJDYNTEXT, FAR *LPFJDYNTEXT, FAR * const CLPFJDYNTEXT;
typedef const struct fjdyntext FAR *LPCFJDYNTEXT, FAR * const CLPCFJDYNTEXT;

#ifdef _WINDOWS
	#ifdef  __cplusplus
		extern "C"
		{
	#endif
			FJDLLExport BOOL fj_setWinDynData (int nIndex, LPCSTR lpsz);
			FJDLLExport BOOL fj_getWinDynData (int nIndex, LPSTR lpsz);
			FJDLLExport VOID fj_initWinDynData ();
	#ifdef  __cplusplus
		}
	#endif
#endif

#endif									// ifndef FJ_DYNTEXT
