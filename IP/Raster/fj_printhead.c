#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <crtdbg.h>
#endif
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_barcode.h"
#include "fj_bitmap.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"

extern const CHAR fj_FontID[];

//
//  FUNCTION: fj_PrintHeadSetEnv()
//
//  PURPOSE:  Set the Environment Transforms in a FJPRINTHEAD structure
//
VOID fj_PrintHeadSetEnv( LPFJPRINTHEAD pfph )
{
	BOOL  bTransformEnv;

	pfph->lTransformEnv = 0;

	// inverse
	bTransformEnv = FALSE;
	if ( TRUE == pfph->pfop->bHeadInvert ) bTransformEnv = !bTransformEnv;
	if ( TRUE == pfph->pfop->bChipInvert ) bTransformEnv = !bTransformEnv;
	if ( TRUE == bTransformEnv ) pfph->lTransformEnv |=  FJ_TRANS_INVERSE;
	else                         pfph->lTransformEnv &= ~FJ_TRANS_INVERSE;
	//  if ( pfph->fSlantAngle < 0.0 ) pfi->lTransformEnv |= FJ_TRANS_INVERSE;

	// reverse
	if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) pfph->lTransformEnv |=  FJ_TRANS_REVERSE;
	else                                                    pfph->lTransformEnv &= ~FJ_TRANS_REVERSE;

	if ( 0 == pfph->lTransformEnv ) pfph->lTransformEnv = FJ_TRANS_NORMAL;

	return;
}
//
//  FUNCTION: fj_PrintHeadInit()
//
//  PURPOSE:  Initialize a new FJPRINTHEAD structure
//
FJDLLExport VOID fj_PrintHeadInit( LPFJPRINTHEAD pfph )
{
	LPFJPRINTHEAD_OP  pfop;
	LPFJPRINTHEAD_PHY pfphy;
	LPFJPRINTHEAD_PKG pfpkg;

	pfop  = pfph->pfop;
	pfphy = pfph->pfphy;
	pfpkg = pfph->pfpkg;

	memset( pfop  , 0, sizeof(FJPRINTHEAD_OP ) );
	memset( pfphy , 0, sizeof(FJPRINTHEAD_PHY) );
	memset( pfpkg , 0, sizeof(FJPRINTHEAD_PKG) );
	memset( pfph  , 0, sizeof(FJPRINTHEAD    ) );

	pfph->pfop  = pfop;
	pfph->pfphy = pfphy;
	pfph->pfpkg = pfpkg;
	
	pfph->bSOP 		 = FALSE;
	pfph->lSOP 		 = 0;

	pfpkg->fLeft       = 1.0;			// left side from nozzle 0
	pfpkg->fRight      = 1.0;			// right side from nozzle 0
	pfpkg->fTop        = 1.0;			// top from nozzle 0
	pfpkg->fBottom     = 1.0;			// bottom from nozzle 0
	pfpkg->lUnits      = FJPH_UNITS_ENGLISH_INCHES;

										// name
	strcpy( pfph->pfop->strName , "Unknown" );
	pfphy->fSpanCenter = 1.0;			// distance from nozzle 0 to last nozzle
	pfphy->fDotHeight  = 1.0;			// height of one print dot
	pfphy->fDotWidth   = 1.0;			// width of one print dot
	pfphy->lInkDrop    = 1;				// volume of ink in one print dot
	pfphy->lUnits      = FJPH_UNITS_ENGLISH_INCHES;

										// direction of package movement across print head
	pfph->lDirection         = FJPH_DIRECTION_RIGHT_TO_LEFT;
	pfph->lSlant             = 0;		// slant value for message processing
	pfph->fSlantAngle        = 90.0;	// installed angle of slant.
	pfph->fHeight0           = 1.0;		// height of nozzle 0 above conveyor
	pfph->fDistancePC        = 1.0;		// distance of nozzle 0 downstream from photocell
	pfph->fDistancePC1 = pfph->fDistancePC2 = pfph->fDistancePC;
	pfph->fAMSInterval       = 4.0;		// time in hours between AMS cycles
	pfph->lAMSInactive       = 10;		// time in seconds for inactive period to start AMS cycle
	pfph->bAPSenable         = TRUE;	// enable APS(AMS)
	pfph->fStandByInactive   = 4.0;		// time in hours of inactivity to wait before activating StandBy mode
	pfph->bStandByEnable     = TRUE;	// auto StandBy mode enable/disable
										// 'environment' transforms to use

	pfph->bBiDirectional = FALSE;
	pfph->lBiDirectionalCount = 0;

	pfph->lTransformEnv      = FJ_TRANS_NORMAL;
	pfph->bPhotocellBack     = FALSE;	// TRUE = trigger from back Photocell connector; FALSE = trigger from Photocell connector at print head
	pfph->bPhotocellInternal = FALSE;	// TRUE = generate internal photocell
	pfph->bHeadSelected      = FALSE;	// a print head has been selected from menu
	pfph->bEdit              = FALSE;	// true if edit in progress
	pfph->lEditID            = 0;		// id (IP address) of edit
	pfph->tmEditVersion.tm_year = 0;	// time of last edit
	pfph->tmDateManu.tm_year = 0;		// date manufactured
	pfph->tmDateInservice.tm_year = 0;	// date in service
	pfph->lFJPhOpIndex       = 999;		// number of the operational print head descriptor that was selected
	pfph->lGroupNumber       = 0;		// number of this printhead in the group
	pfph->lSerial            = 0;		// serial number
										// long name
	strcpy( pfph->strNameLong , "FoxJet" );
										// short name - for ID
	strcpy( pfph->strNameShort, "FJ"     );
	pfph->lIPaddr            = 0;		// IP address currently in use
	pfph->lDynimageCount     = 1;		// counter/ID for Dynimage
										// scanner default Label ID
	pfph->strScannerDefaultLabelID[0] = 0;
										// if true, use the scanner input once, then use default label
	pfph->bSerialScannerLabelUseOnce = FALSE;
	pfph->bSerialScannerLabel = FALSE;	// if true, serial port has a scanner to select the print label
	pfph->bSerialVarData     = FALSE;	// if true, serial port gets variable data
	pfph->bSerialDynMem	 = FALSE;
	pfph->lSerialVarDataID   = 0;		// variable data field ID
	pfph->bSerialInfo        = FALSE;	// if true, serial port gets debug info
	pfph->bInfoStatus        = FALSE;	// if true, write status info to serial port
	pfph->bInfoSocket        = FALSE;	// if true, write Socket Connection info to serial port
	pfph->bInfoRAM           = FALSE;	// if true, write RAM info to serial port
	pfph->bInfoROM           = FALSE;	// if true, write ROM info to serial port
	pfph->bInfoName          = FALSE;	// if true, write Name Registration (DNS/NETBIOS) info to serial port
	pfph->bInfoTime          = FALSE;	// if true, write Time Server info to serial port
	pfph->bInfoAD            = FALSE;	// if true, write Analog-to-Digital info to serial port
	pfph->pfiDynimage        = NULL;	// pointer to dynamic image
	pfph->pMemStor           = NULL;	// pointer to RAM MEMSTOR with messages and fonts
	pfph->pMemStorEdit       = NULL;	// pointer to RAM MEMSTOR with messages and fonts - copy for editing
	pfph->pfmScanner         = NULL;	// pointer to scanner default FJMESSAGE
	pfph->pfmSelected        = NULL;	// pointer to selected FJMESSAGE template
	pfph->pfmSelected2       = NULL;	// pointer to selected FJMESSAGE template - old copy
	pfph->papffSelected      = NULL;	// pointer to array of pointers to fonts used by selected message
	pfph->pfcFirst           = NULL;	// start of chain of Images to print
	pfph->pfcLast            = NULL;	//   end of chain of Images to print
	pfph->pfsys              = NULL;	// pointer to owning FJSYSTEM structure

	return;
}
//
//  FUNCTION: fj_PrintHeadNew()
//
//  PURPOSE:  Create a new FJPRINTHEAD structure
//
FJDLLExport LPFJPRINTHEAD fj_PrintHeadNew( VOID )
{
	LPFJPRINTHEAD     pfph;

	pfph        = (LPFJPRINTHEAD    )fj_malloc( sizeof (FJPRINTHEAD    ) );
	pfph->pfop  = (LPFJPRINTHEAD_OP )fj_malloc( sizeof (FJPRINTHEAD_OP ) );
	pfph->pfphy = (LPFJPRINTHEAD_PHY)fj_malloc( sizeof (FJPRINTHEAD_PHY) );
	pfph->pfpkg = (LPFJPRINTHEAD_PKG)fj_malloc( sizeof (FJPRINTHEAD_PKG) );

	fj_PrintHeadInit( pfph );

	return( pfph );
}
//
//  FUNCTION: fj_PrintHeadDestroy()
//
//  PURPOSE:  Destroy a FJPRINTHEAD structure
//
FJDLLExport VOID fj_PrintHeadDestroy( LPFJPRINTHEAD pfph )
{
	LPFJPRINTCHAIN pfc;
	LPFJPRINTCHAIN pfcNext;
	LPFJFONT  pff;
	int i;

	if ( NULL != pfph->pfop )         fj_free( pfph->pfop  );
	if ( NULL != pfph->pfphy )        fj_free( pfph->pfphy );
	if ( NULL != pfph->pfpkg )        fj_free( pfph->pfpkg );
	if ( NULL != pfph->pMemStor )     fj_MemStorDestroy( pfph->pMemStor );
	if ( NULL != pfph->pMemStorEdit ) fj_MemStorDestroy( pfph->pMemStorEdit );
	if ( NULL != pfph->pfiDynimage )  fj_ImageRelease( pfph->pfiDynimage );
	if ( NULL != pfph->papffSelected)
	{
		i = 0;
		while ( NULL != (pff = *(pfph->papffSelected+i++)) )
		{
			fj_FontDestroy( pff );
		}
		fj_free( pfph->papffSelected );
	}
	if ( NULL != pfph->pfmScanner   ) fj_MessageDestroy( pfph->pfmScanner   );
	if ( NULL != pfph->pfmSelected  ) fj_MessageDestroy( pfph->pfmSelected  );
	if ( NULL != pfph->pfmSelected2 ) fj_MessageDestroy( pfph->pfmSelected2 );
	if ( NULL != pfph->pfcFirst )
	{
		pfc = pfph->pfcFirst;
		while ( NULL != pfc )
		{
			pfcNext = pfc->pNext;
			fj_ImageRelease( pfc->pfi );
			fj_free( pfc );
			pfc = pfcNext;
		}
	}
	fj_free( pfph );
	return;
}
//
//  FUNCTION: fj_PrintHeadReset()
//
//  PURPOSE:  Reset a FJPRINTHEAD structure
//
FJDLLExport VOID fj_PrintHeadReset( LPFJPRINTHEAD pfph )
{
	fj_PrintHeadSetEnv( pfph );
	return;
}
//
//  FUNCTION: fj_PrintHeadSetID()
//
//  PURPOSE:  id is set from short name + serial number
//
FJDLLExport VOID fj_PrintHeadSetID( LPFJPRINTHEAD pfph )
{
	strcpy( pfph->strID, pfph->strNameShort );
	strcat( pfph->strID, pfph->strSerial );
	return;
}
//
//  FUNCTION: fj_PrintHeadSetSerial()
//
//  PURPOSE:  set serial number
//
// serial number must be greater than FJ_SYS_SERIAL_MIN and less than FJ_SYS_SERIAL_MAX.
// or, for testing, serial can be less than FJ_SYS_SERIAL_TEST if previous serial is less than FJ_SYS_SERIAL_TEST.
//
FJDLLExport BOOL fj_PrintHeadSetSerial( LPFJPRINTHEAD pfph, LPCSTR pInput )
{
	int  iLen;
	LONG lInput;
	LONG lSerial;
	BOOL bRet = FALSE;

	iLen = strlen( pInput );
	if ( (0 < iLen) && (iLen <= FJPH_SERIAL_SIZE ) )
	{
		lInput  = atoi( pInput );
		lSerial = pfph->lSerial;
		if ( ((FJ_SYS_SERIAL_MIN <= lInput) && (lInput < FJ_SYS_SERIAL_MAX)) ||
			((FJ_SYS_SERIAL_TEST > lInput) && (FJ_SYS_SERIAL_TEST  > lSerial )) )
		{
			pfph->lSerial = lSerial;
			strcpy( pfph->strSerial, pInput );
			fj_PrintHeadSetID( pfph );
			bRet = TRUE;
		}
	}
	return( bRet );
}
//
//  FUNCTION: fj_PrintHeadSetNameShort()
//
//  PURPOSE:  set the short name
//
FJDLLExport BOOL fj_PrintHeadSetNameShort( LPFJPRINTHEAD pfph, LPCSTR pInput )
{
	int   iLen;
	BOOL  bRet = FALSE;

	iLen = strlen( pInput );
	if ( (0 < iLen) && (iLen <= FJPH_NAME_SIZE_SHORT ) )
	{
		memset( pfph->strNameShort, 0, sizeof(pfph->strNameShort) );
		strcpy( pfph->strNameShort, pInput );
		fj_PrintHeadSetID( pfph );
		bRet = TRUE;
	}
	return( bRet );
}
//
//  FUNCTION: fj_PrintHeadSetNameLong()
//
//  PURPOSE:  set the long name
//
FJDLLExport BOOL fj_PrintHeadSetNameLong( LPFJPRINTHEAD pfph, LPCSTR pInput )
{
	int   iLen;
	BOOL  bRet = FALSE;

	iLen = strlen( pInput );
	if ( (0 < iLen) && (iLen <= FJPH_NAME_SIZE_LONG ) )
	{
		memset( pfph->strNameLong, 0, sizeof(pfph->strNameLong) );
		strcpy( pfph->strNameLong, pInput );
		bRet = TRUE;
	}
	return( bRet );
}
struct fjphtable
{
	LPSTR pStr;
	VOID (*pGet)( CLPCFJPRINTHEAD pfph, LPSTR  pString );
	VOID (*pSet)( CLPFJPRINTHEAD  pfph, LPCSTR pString );
};

FJDLLExport VOID fj_PHGetID        ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_processParameterStringOutput( pStr, pfph->strID );}
FJDLLExport VOID fj_PHGetLongName  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_processParameterStringOutput( pStr, pfph->strNameLong );}
FJDLLExport VOID fj_PHGetShortName ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_processParameterStringOutput( pStr, pfph->strNameShort );}
FJDLLExport VOID fj_PHGetSerial    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){strcpy( pStr, pfph->strSerial );}
FJDLLExport VOID fj_PHGetDirection ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){if ( FJPH_DIRECTION_LEFT_TO_RIGHT == pfph->lDirection ) strcpy( pStr, "LEFT_TO_RIGHT" ); else strcpy( pStr, "RIGHT_TO_LEFT" ); }
FJDLLExport VOID fj_PHGetSlant     ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->lSlant );}
FJDLLExport VOID fj_PHGetSlantAngle( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fSlantAngle );}
FJDLLExport VOID fj_PHGetHeight    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fHeight0 );}
FJDLLExport VOID fj_PHGetDistance  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fDistancePC );}
FJDLLExport VOID fj_PHGetDistance2 ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fDistancePC2 );}
FJDLLExport VOID fj_PHGetAMSInter  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fAMSInterval );}
FJDLLExport VOID fj_PHGetStandByInt( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->fStandByInactive );}
FJDLLExport VOID fj_PHGetAMSInact  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->lAMSInactive );}
FJDLLExport VOID fj_PHGetPCBack    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bPhotocellBack );}
FJDLLExport VOID fj_PHGetMsgStorSize(CLPCFJPRINTHEAD pfph, LPSTR pStr ){LONG lSize=0;if(NULL!=pfph->pMemStor)lSize=pfph->pMemStor->lSize;fj_LongToStr(pStr,lSize);}
FJDLLExport VOID fj_PHGetOPIndex   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->lFJPhOpIndex );}
FJDLLExport VOID fj_PHGetManuTime  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_SystemGetDateTimeString( pStr, &(pfph->tmDateManu) );}
FJDLLExport VOID fj_PHGetServTime  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_SystemGetDateTimeString( pStr, &(pfph->tmDateInservice) );}
FJDLLExport VOID fj_PHGetScannerOnce( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bSerialScannerLabelUseOnce );}
FJDLLExport VOID fj_PHGetSOP (CLPCFJPRINTHEAD pfph, LPSTR  pStr) { fj_BoolToStr( pStr, pfph->bSOP ); }
FJDLLExport VOID fj_PHGetBiDirectional ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr (pStr, pfph->bBiDirectional); }

FJDLLExport VOID fj_PHGetBaudRate (CLPCFJPRINTHEAD pfph, LPSTR  pStr)
{
#ifndef _WINDOWS
    LPFJ_NETOSPARAM pParamNETOS = pCVT->pNETOSparam;

    fj_LongToStr (pStr, pCVT->pNETOSparam->baudrate);
#endif
}

FJDLLExport VOID fj_PHSetBaudRate (CLPFJPRINTHEAD  pfph, LPCSTR pStr)
{
#ifndef _WINDOWS
    LPFJ_NETOSPARAM pParamNETOS = pCVT->pNETOSparam;
    devBoardParamsType nvParams;
    LONG lBaud;

    lBaud = (LONG)atoi (pStr);

    if (pCVT->pNETOSparam->baudrate != lBaud) {
	NAReadDevBoardParams (&nvParams);

	pCVT->pNETOSparam->baudrate = nvParams.baudrate = lBaud;

	NAWriteDevBoardParams (&nvParams);
	pCVT->fj_WriteNETOSRomParams (pParamNETOS, FALSE, NULL, 0);

	fj_closeSerialPort ();
	fj_openSerialPort (pfph);
    }
#endif
}


FJDLLExport VOID fj_PHGetStandByAuto( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bStandByEnable );}
FJDLLExport VOID fj_PHGetSerialPort( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	if      ( TRUE == pfph->bSerialScannerLabel ) strcpy( pStr, "SCANNER_LABEL" );
	else if ( TRUE == pfph->bSerialInfo         ) strcpy( pStr, "INFO" );
	else if ( TRUE == pfph->bSerialVarData      ) strcpy( pStr, "VARDATA" );
	else if ( TRUE == pfph->bSerialDynMem	    ) strcpy( pStr, "DYNMEM" );
	else                                          strcpy( pStr, "NONE" );
}
FJDLLExport VOID fj_PHGetInfoStatus( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoStatus );}
FJDLLExport VOID fj_PHGetInfoSocket( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoSocket );}
FJDLLExport VOID fj_PHGetInfoRAM   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoRAM    );}
FJDLLExport VOID fj_PHGetInfoROM   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoROM    );}
FJDLLExport VOID fj_PHGetInfoName  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoName   );}
FJDLLExport VOID fj_PHGetInfoTime  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoTime   );}
FJDLLExport VOID fj_PHGetInfoAD    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->bInfoAD     );}
FJDLLExport VOID fj_PHGetPkgLeft   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfpkg->fLeft );}
FJDLLExport VOID fj_PHGetPkgRight  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfpkg->fRight );}
FJDLLExport VOID fj_PHGetPkgTop    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfpkg->fTop );}
FJDLLExport VOID fj_PHGetPkgBottom ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfpkg->fBottom );}
FJDLLExport VOID fj_PHGetPkgUnits  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){if ( FJPH_UNITS_ENGLISH_INCHES == pfph->pfpkg->lUnits ) strcpy( pStr, "ENGLISH_INCHES" ); else  strcpy( pStr, "METRIC_MM" );}
FJDLLExport VOID fj_PHGetPhyChan   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfphy->lChannels);}
FJDLLExport VOID fj_PHGetPhyOrif   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfphy->lOrifices);}
FJDLLExport VOID fj_PHGetPhyInkDrop( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfphy->lInkDrop);}
FJDLLExport VOID fj_PHGetPhyDotHght( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fDotHeight );}
FJDLLExport VOID fj_PHGetPhyDotWid ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fDotWidth );}
FJDLLExport VOID fj_PHGetPhyAngle  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fAngle );}
FJDLLExport VOID fj_PHGetPhySpCent ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fSpanCenter );}
FJDLLExport VOID fj_PHGetPhySpOut  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fSpanOuter );}
FJDLLExport VOID fj_PHGetPhyChChStr( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fChChStraight );}
FJDLLExport VOID fj_PHGetPhyChChAng( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fChChAngle );}
FJDLLExport VOID fj_PHGetPhyEvenOdd( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfphy->fOffsetEvenOdd);}
FJDLLExport VOID fj_PHGetPhyUnits  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){if ( FJPH_UNITS_ENGLISH_INCHES == pfph->pfphy->lUnits ) strcpy( pStr, "ENGLISH_INCHES" ); else strcpy( pStr, "METRIC_MM" );}
FJDLLExport VOID fj_PHGetOpName    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){STR sName[FJPH_TYPE_NAME_SIZE*2];fj_processParameterStringOutput( sName, pfph->pfop->strName );strcpy( pStr, sName );}
FJDLLExport VOID fj_PHGetOpPhyTable( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lPhyTable );}
FJDLLExport VOID fj_PHGetOpPkgTable( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lPkgTable );}
FJDLLExport VOID fj_PHGetOpVltTable( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lVoltageTable );}
FJDLLExport VOID fj_PHGetOpVoltage ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lVoltage );}
FJDLLExport VOID fj_PHGetOpHeadPres( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->pfop->bForceHeadPresent );}
FJDLLExport VOID fj_PHGetOpCurve1  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfop->fCurve1 );}
FJDLLExport VOID fj_PHGetOpCurve2  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfop->fCurve2 );}
FJDLLExport VOID fj_PHGetOpCurve3  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfop->fCurve3 );}
FJDLLExport VOID fj_PHGetOpSubPulse( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->pfop->bSubpulseOn );}
FJDLLExport VOID fj_PHGetOpSubLen  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_DoubleToStr( pStr, pfph->pfop->fSubpulseLength );}
FJDLLExport VOID fj_PHGetOpSubFreq ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lSubpulseFreq );}
FJDLLExport VOID fj_PHGetOpSubOn   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lSubpulseOn );}
FJDLLExport VOID fj_PHGetOpSubOff  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lSubpulseOff );}
FJDLLExport VOID fj_PHGetOpSubDur  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lSubpulseDur );}
FJDLLExport VOID fj_PHGetOpSubInt  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lSubpulseInt );}
FJDLLExport VOID fj_PHGetOpHeadInv ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->pfop->bHeadInvert );}
FJDLLExport VOID fj_PHGetOpChipInv ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->pfop->bChipInvert );}
FJDLLExport VOID fj_PHGetOpTempTabl( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lTempTable );}
FJDLLExport VOID fj_PHGetOpTempRang( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lTempRange );}
FJDLLExport VOID fj_PHGetOpTemp1   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lTemp1 );}
FJDLLExport VOID fj_PHGetOpTemp2   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lTemp2 );}
FJDLLExport VOID fj_PHGetOpTemp3   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lTemp3 );}
FJDLLExport VOID fj_PHGetOpCal1    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lCal1 );}
FJDLLExport VOID fj_PHGetOpCal2    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lCal2 );}
FJDLLExport VOID fj_PHGetOpCal3    ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lCal3 );}
FJDLLExport VOID fj_PHGetOpAMS     ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_BoolToStr( pStr, pfph->pfop->bAMS );}
FJDLLExport VOID fj_PHGetOpAMSA1   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA1 );}
FJDLLExport VOID fj_PHGetOpAMSA2   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA2 );}
FJDLLExport VOID fj_PHGetOpAMSA3   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA3 );}
FJDLLExport VOID fj_PHGetOpAMSA4   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA4 );}
FJDLLExport VOID fj_PHGetOpAMSA5   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA5 );}
FJDLLExport VOID fj_PHGetOpAMSA6   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA6 );}
FJDLLExport VOID fj_PHGetOpAMSA7   ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_LongToStr( pStr, pfph->pfop->lAMSA7 );}
FJDLLExport VOID fj_PHGetEditLock  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){if ( TRUE == pfph->bEdit ) strcpy( pStr, "SET" ); else strcpy( pStr, "FREE" );}
FJDLLExport VOID fj_PHGetEditIPAddr( CLPCFJPRINTHEAD pfph, LPSTR pStr ){sprintf( pStr, "%d.%d.%d.%d", (int)((unsigned)((pfph->lEditID>>24)&0xff)),(int)((unsigned)((pfph->lEditID>>16)&0xff)),(int)((unsigned)((pfph->lEditID>>8)&0xff)),(int)((unsigned)(pfph->lEditID&0xff)) );}
FJDLLExport VOID fj_PHGetEditTime  ( CLPCFJPRINTHEAD pfph, LPSTR pStr ){fj_SystemGetDateTimeString( pStr, &(pfph->tmEditVersion) );}
										// Burl v 1.1015
FJDLLExport VOID fj_PHGetAutoPrint (CLPCFJPRINTHEAD pfph, LPSTR  pStr)
{
	fj_LongToStr( pStr, pfph->pfop->lAutoPrint );
}
										// Burl v 1.1015
FJDLLExport VOID fj_PHGetPCInternal(CLPCFJPRINTHEAD pfph, LPSTR  pStr)
{
	fj_BoolToStr( pStr, pfph->bPhotocellInternal );
}

FJDLLExport VOID fj_PHGetAPSMode (CLPCFJPRINTHEAD pfph, LPSTR  pStr) // Burl v 1.200.beta5
{
	fj_BoolToStr( pStr, pfph->bAPSenable );
}

FJDLLExport VOID fj_PHSetAPSMode (CLPFJPRINTHEAD  pfph, LPCSTR pStr) // Burl v 1.200.beta5
{
	pfph->bAPSenable = (*pStr == 'T' ? TRUE : FALSE);
}

FJDLLExport VOID fj_PHSetLongName       (CLPFJPRINTHEAD pfph, LPCSTR pStr){fj_processParameterStringInput((LPSTR)pStr);fj_PrintHeadSetNameLong( pfph, pStr );}
FJDLLExport VOID fj_PHSetShortName      (CLPFJPRINTHEAD pfph, LPCSTR pStr){fj_processParameterStringInput((LPSTR)pStr);fj_PrintHeadSetNameShort( pfph, pStr );}
FJDLLExport VOID fj_PHSetSerial         (CLPFJPRINTHEAD pfph, LPCSTR pStr){fj_processParameterStringInput((LPSTR)pStr);fj_PrintHeadSetSerial( pfph, pStr );}
FJDLLExport VOID fj_PHSetDirection      (CLPFJPRINTHEAD pfph, LPCSTR pStr){if (0 == strcmp("LEFT_TO_RIGHT",pStr)) pfph->lDirection = FJPH_DIRECTION_LEFT_TO_RIGHT; else pfph->lDirection = FJPH_DIRECTION_RIGHT_TO_LEFT; }
FJDLLExport VOID fj_PHSetSlantAngle     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fSlantAngle          = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetSlant          (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->lSlant               =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetHeight         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fHeight0             = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetDistance       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fDistancePC = pfph->fDistancePC1 = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetDistance2      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fDistancePC2         = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetAMSInter       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fAMSInterval         = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetStandByInt     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->fStandByInactive     = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetAMSInact       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->lAMSInactive         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetPCBack         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bPhotocellBack       = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOPIndex        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->lFJPhOpIndex         =  (LONG)atoi(pStr);pfph->bHeadSelected = TRUE;}
FJDLLExport VOID fj_PHSetManuTime       (CLPFJPRINTHEAD pfph, LPCSTR pStr){struct tm tm;struct tm *pTM;STR str[32];strncpy( str, pStr, sizeof(str) );pTM = fj_SystemParseDateTime( str, &tm );if (NULL != pTM)memcpy(&(pfph->tmDateManu),&tm,sizeof(struct tm));}
FJDLLExport VOID fj_PHSetServTime       (CLPFJPRINTHEAD pfph, LPCSTR pStr){struct tm tm;struct tm *pTM;STR str[32];strncpy( str, pStr, sizeof(str) );pTM = fj_SystemParseDateTime( str, &tm );if (NULL != pTM)memcpy(&(pfph->tmDateInservice),&tm,sizeof(struct tm));}
FJDLLExport VOID fj_PHSetScannerOnce    (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bSerialScannerLabelUseOnce = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetSOP 			(CLPFJPRINTHEAD pfph, LPCSTR pStr)
{
    pfph->bSOP = (*pStr == 'T' ? TRUE : FALSE);

    if (!pfph->bSOP)
	pfph->lSOP = 0;
}
FJDLLExport VOID fj_PHSetBiDirectional (CLPFJPRINTHEAD  pfph, LPCSTR pStr){pfph->bBiDirectional = (*pStr == 'T' ? TRUE : FALSE); }
FJDLLExport VOID fj_PHSetStandByAuto    (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bStandByEnable = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetSerialPort     (CLPFJPRINTHEAD pfph, LPCSTR pStr)
{
	pfph->bSerialScannerLabel = FALSE;
	pfph->bSerialInfo         = FALSE;
	pfph->bSerialVarData      = FALSE;
	pfph->bSerialDynMem       = FALSE;
	
	if      (0 == strcmp("VARDATA",       pStr)) pfph->bSerialVarData      = TRUE;
	else if (0 == strcmp("DYNMEM",        pStr)) pfph->bSerialDynMem       = TRUE;
	else if (0 == strcmp("SCANNER_LABEL", pStr)) pfph->bSerialScannerLabel = TRUE;
	else if (0 == strcmp("INFO",          pStr)) pfph->bSerialInfo         = TRUE;
/* TODO: rem
{ char sz [256];
    sprintf (sz, "%s(%d): fj_PHSetSerialPort: %s%s%s%s %s", __FILE__, __LINE__,
	pfph->bSerialScannerLabel ? "bSerialScannerLabel " : "",
	pfph->bSerialInfo         ? "bSerialInfo " : "",
	pfph->bSerialVarData      ? "bSerialVarData " : "",
	pfph->bSerialDynMem       ? "bSerialDynMem " : "",
	pStr);
    fj_socketSendAll (IPC_STATUS, (LPBYTE)sz, strlen (sz));
}
*/
}
FJDLLExport VOID fj_PHSetInfoStatus     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoStatus = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoSocket     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoSocket = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoRAM        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoRAM    = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoROM        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoROM    = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoName       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoName   = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoTime       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoTime   = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetInfoAD         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->bInfoAD     = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetPkgLeft        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfpkg->fLeft         = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPkgRight       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfpkg->fRight        = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPkgTop         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfpkg->fTop          = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPkgBottom      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfpkg->fBottom       = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPkgUnits       (CLPFJPRINTHEAD pfph, LPCSTR pStr){if (0 == strcmp("ENGLISH_INCHES", pStr)) pfph->pfpkg->lUnits = FJPH_UNITS_ENGLISH_INCHES;if (0 == strcmp("METRIC_MM", pStr)) pfph->pfpkg->lUnits = FJPH_UNITS_METRIC_MM;}
FJDLLExport VOID fj_PHSetPhyChan        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->lChannels     =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetPhyOrif        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->lOrifices     =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetPhyInkDrop     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->lInkDrop      =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetPhyDotHght     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fDotHeight    = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyDotWid      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fDotWidth     = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyAngle       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fAngle        = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhySpCent      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fSpanCenter   = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhySpOut       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fSpanOuter    = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyChChStr     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fChChStraight = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyChChAng     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fChChAngle    = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyEvenOdd     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfphy->fOffsetEvenOdd= (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetPhyUnits       (CLPFJPRINTHEAD pfph, LPCSTR pStr){if (0 == strcmp("ENGLISH_INCHES", pStr)) pfph->pfphy->lUnits = FJPH_UNITS_ENGLISH_INCHES;if (0 == strcmp("METRIC_MM", pStr)) pfph->pfphy->lUnits = FJPH_UNITS_METRIC_MM;}
FJDLLExport VOID fj_PHSetOpName         (CLPFJPRINTHEAD pfph, LPCSTR pStr){fj_processParameterStringInput((LPSTR)pStr);strncpy(pfph->pfop->strName, pStr, FJPH_TYPE_NAME_SIZE);pfph->pfop->strName[FJPH_TYPE_NAME_SIZE] = 0;}
FJDLLExport VOID fj_PHSetOpPhyTable     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lPhyTable      =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpPkgTable     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lPkgTable      =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpVltTable     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lVoltageTable  =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpVoltage      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lVoltage       =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpHeadPres     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->bForceHeadPresent = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOpCurve1       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->fCurve1        = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetOpCurve2       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->fCurve2        = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetOpCurve3       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->fCurve3        = (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetOpSubPulse     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->bSubpulseOn    = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOpSubLen       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->fSubpulseLength= (FLOAT)atof(pStr);}
FJDLLExport VOID fj_PHSetOpSubFreq      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lSubpulseFreq  =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpSubOn        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lSubpulseOn    =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpSubOff       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lSubpulseOff   =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpSubDur       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lSubpulseDur   =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpSubInt       (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lSubpulseInt   =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpHeadInv      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->bHeadInvert    = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOpChipInv      (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->bChipInvert    = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOpTempTabl     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lTempTable     =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpTempRang     (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lTempRange     =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpTemp1        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lTemp1         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpTemp2        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lTemp2         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpTemp3        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lTemp3         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpCal1         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lCal1          =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpCal2         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lCal2          =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpCal3         (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lCal3          =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMS          (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->bAMS           = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_PHSetOpAMSA1        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA1         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA2        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA2         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA3        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA3         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA4        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA4         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA5        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA5         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA6        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA6         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetOpAMSA7        (CLPFJPRINTHEAD pfph, LPCSTR pStr){pfph->pfop->lAMSA7         =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_PHSetEditLock       (CLPFJPRINTHEAD pfph, LPCSTR pStr)
{
    if (0 == strcmp("SET", pStr))
		pfph->bEdit = TRUE;
    else {
		#ifndef _WINDOWS
		pCVT->fj_SystemGetTime(&fj_tmKeepAlive);
		#endif
		pfph->bEdit = FALSE;
    }
}

FJDLLExport VOID fj_PHSetEditIPAddr     (CLPFJPRINTHEAD pfph, LPCSTR pStr){unsigned int n1,n2,n3,n4;pfph->lEditID = 0;sscanf( pStr, "%d.%d.%d.%d;", &n1,&n2,&n3,&n4 );pfph->lEditID = ((n1<<24)&0xff000000)|((n2<<16)&0x00ff0000)|((n3<<8)&0x0000ff00)|((n4&0xff));}
FJDLLExport VOID fj_PHSetEditTime       (CLPFJPRINTHEAD pfph, LPCSTR pStr){struct tm tm;struct tm *pTM;STR str[32];strncpy( str, pStr, sizeof(str) );pTM = fj_SystemParseDateTime( str, &tm );if (NULL != pTM)memcpy(&(pfph->tmEditVersion),&tm,sizeof(struct tm));}
FJDLLExport VOID fj_PHSetMsgStorSize( CLPFJPRINTHEAD pfph, LPCSTR pStr )
{
	LONG   lSize = (LONG)atoi(pStr);
	LPBYTE pByte;
	if ( NULL != pfph->pMemStor )
	{
		if ( FALSE == pfph->pMemStor->bDoNotFreeBuffer )
		{
			fj_MemStorDestroy( pfph->pMemStor );
			pfph->pMemStor = NULL;
		}
	}
	if ( NULL == pfph->pMemStor )
	{
		pByte = (LPBYTE)fj_calloc( 1, lSize );
		pfph->pMemStor = fj_MemStorNew( pByte, lSize  );
	}
	return;
}
										// Burl v 1.1015
FJDLLExport VOID fj_PHSetAutoPrint ( CLPFJPRINTHEAD pfph, LPCSTR pStr )
{
	LONG lAutoPrint;

	lAutoPrint = (LONG)atoi (pStr);
	pfph->pfop->lAutoPrint = lAutoPrint;

#ifdef NET_OS
	{
		volatile LPGA pga = fj_CVT.pGA;

		pga->rw.write.AutoPrint = lAutoPrint;
										// save
		fj_CVT.pFJglobal->gaWrite.rw.write.AutoPrint = lAutoPrint;
	}
#endif

}
										// Burl v 1.1015
FJDLLExport VOID fj_PHSetPCInternal(CLPFJPRINTHEAD  pfph, LPCSTR pStr)
{
	pfph->bPhotocellInternal = (*pStr == 'T' ? TRUE : FALSE);
}

struct fjphtable fj_PHParams[] =
{
#define PH_PARAM_ID     0
#define PH_PARAM_FIRST  0
/* 0  */	"PRINTER_ID",             fj_PHGetID,             NULL,
/* 1  */	"LONGNAME",               fj_PHGetLongName,       fj_PHSetLongName,
/* 2  */	"SHORTNAME",              fj_PHGetShortName,      fj_PHSetShortName,
/* 3  */	"SERIAL",                 fj_PHGetSerial,         fj_PHSetSerial,
/* 4  */	"DIRECTION",              fj_PHGetDirection,      fj_PHSetDirection,
/* 5  */	"SLANTANGLE",             fj_PHGetSlantAngle,     fj_PHSetSlantAngle,
/* 6  */	"SLANTVALUE",             fj_PHGetSlant,          fj_PHSetSlant,
/* 7  */	"HEIGHT",                 fj_PHGetHeight,         fj_PHSetHeight,
/* 8  */	"DISTANCE",               fj_PHGetDistance,       fj_PHSetDistance,
/* 9  */	"AMS_INTERVAL",           fj_PHGetAMSInter,       fj_PHSetAMSInter,
/* 10  */	"AMS_INACTIVE",           fj_PHGetAMSInact,       fj_PHSetAMSInact,
/* 11  */	"STANDBY_INTERVAL",       fj_PHGetStandByInt,     fj_PHSetStandByInt,
/* 12  */	"STANDBY_AUTO",           fj_PHGetStandByAuto,    fj_PHSetStandByAuto,
/* 13  */	"PHOTOCELL_BACK",         fj_PHGetPCBack,         fj_PHSetPCBack,
/* 14  */	"MSGSTORE_SIZE",          fj_PHGetMsgStorSize,    fj_PHSetMsgStorSize,
/* 15  */	"TABLE_INDEX",            fj_PHGetOPIndex,        fj_PHSetOPIndex,
/* 16  */	"DATE_MANU",              fj_PHGetManuTime,       fj_PHSetManuTime,
/* 17  */	"DATE_SERVICE",           fj_PHGetServTime,       fj_PHSetServTime,
/* 18  */	"SCANNER_USE_ONCE",       fj_PHGetScannerOnce,    fj_PHSetScannerOnce,
/* 19  */	"SERIAL_PORT",            fj_PHGetSerialPort,     fj_PHSetSerialPort,
/* 20  */	"INFO_STATUS",            fj_PHGetInfoStatus,     fj_PHSetInfoStatus,
/* 21  */	"INFO_SOCKET",            fj_PHGetInfoSocket,     fj_PHSetInfoSocket,
/* 22  */	"INFO_RAM",               fj_PHGetInfoRAM,        fj_PHSetInfoRAM,
/* 23  */	"INFO_ROM",               fj_PHGetInfoROM,        fj_PHSetInfoROM,
/* 24  */	"INFO_NAME",              fj_PHGetInfoName,       fj_PHSetInfoName,
/* 25  */	"INFO_TIME",              fj_PHGetInfoTime,       fj_PHSetInfoTime,
/* 26  */	"INFO_AD",                fj_PHGetInfoAD,         fj_PHSetInfoAD,
/* 27  */	"PHOTOCELL_INTERNAL",     fj_PHGetPCInternal,     fj_PHSetPCInternal,	// Burl v 1.1015
/* 28  */	"APS_MODE",               fj_PHGetAPSMode,        fj_PHSetAPSMode, 	// Burl v 1.200.beta5
/* 29  */	"SOP",					  fj_PHGetSOP,			  fj_PHSetSOP,
/* 30  */	"BAUD_RATE",			  fj_PHGetBaudRate,	      fj_PHSetBaudRate,
/* 31  */	"DISTANCE2",              fj_PHGetDistance2,      fj_PHSetDistance2,
/* 32  */	"BIDIRECTIONAL",          fj_PHGetBiDirectional,  fj_PHSetBiDirectional,
#define PH_PARAM_LAST 32
#define PH_PKG_FIRST  33
/* 33  */	"PKG_LEFT",               &fj_PHGetPkgLeft,        fj_PHSetPkgLeft,
/* 34  */	"PKG_RIGHT",              &fj_PHGetPkgRight,       fj_PHSetPkgRight,
/* 35  */	"PKG_TOP",                &fj_PHGetPkgTop,         fj_PHSetPkgTop,
/* 36  */	"PKG_BOTTOM",             &fj_PHGetPkgBottom,      fj_PHSetPkgBottom,
/* 37  */	"PKG_UNITS",              &fj_PHGetPkgUnits,       fj_PHSetPkgUnits,
#define PH_PKG_LAST   37
#define PH_PHY_FIRST  38
/* 39  */	"PHY_CHANNELS",           &fj_PHGetPhyChan,        fj_PHSetPhyChan,
/* 40  */	"PHY_ORIFICES",           &fj_PHGetPhyOrif,        fj_PHSetPhyOrif,
/* 41  */	"PHY_INKDROP",            &fj_PHGetPhyInkDrop,     fj_PHSetPhyInkDrop,
/* 42  */	"PHY_DOTHEIGHT",          &fj_PHGetPhyDotHght,     fj_PHSetPhyDotHght,
/* 43  */	"PHY_DOTWIDTH",           &fj_PHGetPhyDotWid,      fj_PHSetPhyDotWid,
/* 44  */	"PHY_ANGLE",              &fj_PHGetPhyAngle,       fj_PHSetPhyAngle,
/* 45  */	"PHY_SPAN_CENTER",        &fj_PHGetPhySpCent,      fj_PHSetPhySpCent,
/* 46  */	"PHY_SPAN_OUTER",         &fj_PHGetPhySpOut,       fj_PHSetPhySpOut,
/* 47  */	"PHY_CH_STRAIGHT",        &fj_PHGetPhyChChStr,     fj_PHSetPhyChChStr,
/* 48  */	"PHY_CH_ANGLE",           &fj_PHGetPhyChChAng,     fj_PHSetPhyChChAng,
/* 49  */	"PHY_OFF_EVENODD",        &fj_PHGetPhyEvenOdd,     fj_PHSetPhyEvenOdd,
/* 50  */	"PHY_UNITS",              &fj_PHGetPhyUnits,       fj_PHSetPhyUnits,
#define PH_PHY_LAST   49
#define PH_OP_FIRST   50
/* 50  */	"OP_NAME",                &fj_PHGetOpName,         fj_PHSetOpName,
/* 51  */	"OP_PHY_TABLE",           &fj_PHGetOpPhyTable,     fj_PHSetOpPhyTable,
/* 52  */	"OP_PKG_TABLE",           &fj_PHGetOpPkgTable,     fj_PHSetOpPkgTable,
/* 53  */	"OP_VOLT_TABLE",          &fj_PHGetOpVltTable,     fj_PHSetOpVltTable,
/* 54  */	"OP_VOLTAGE",             &fj_PHGetOpVoltage,      fj_PHSetOpVoltage,
/* 55  */	"OP_FORCEHEADPRESENT",    &fj_PHGetOpHeadPres,     fj_PHSetOpHeadPres,
/* 56  */	"OP_CURVE1",              &fj_PHGetOpCurve1,       fj_PHSetOpCurve1,
/* 57  */	"OP_CURVE2",              &fj_PHGetOpCurve2,       fj_PHSetOpCurve2,
/* 58  */	"OP_CURVE3",              &fj_PHGetOpCurve3,       fj_PHSetOpCurve3,
/* 59  */	"OP_SUBPULSE",            &fj_PHGetOpSubPulse,     fj_PHSetOpSubPulse,
/* 60  */	"OP_SUBPULSE_LENGTH",     &fj_PHGetOpSubLen,       fj_PHSetOpSubLen,
/* 61  */	"OP_SUBPULSE_FREQ",       &fj_PHGetOpSubFreq,      fj_PHSetOpSubFreq,
/* 62  */	"OP_SUBPULSE_ON",         &fj_PHGetOpSubOn,        fj_PHSetOpSubOn,
/* 63  */	"OP_SUBPULSE_OFF",        &fj_PHGetOpSubOff,       fj_PHSetOpSubOff,
/* 64  */	"OP_SUBPULSE_DUR",        &fj_PHGetOpSubDur,       fj_PHSetOpSubDur,
/* 65  */	"OP_SUBPULSE_INT",        &fj_PHGetOpSubInt,       fj_PHSetOpSubInt,
/* 66  */	"OP_HEAD_INVERT",         &fj_PHGetOpHeadInv,      fj_PHSetOpHeadInv,
/* 67  */	"OP_CHIP_INVERT",         &fj_PHGetOpChipInv,      fj_PHSetOpChipInv,
/* 68  */	"OP_TEMP_TABLE",          &fj_PHGetOpTempTabl,     fj_PHSetOpTempTabl,
/* 69  */	"OP_TEMP_RANGE",          &fj_PHGetOpTempRang,     fj_PHSetOpTempRang,
/* 70  */	"OP_TEMP_1",              &fj_PHGetOpTemp1,        fj_PHSetOpTemp1,
/* 71  */	"OP_TEMP_2",              &fj_PHGetOpTemp2,        fj_PHSetOpTemp2,
/* 72  */	"OP_TEMP_3",              &fj_PHGetOpTemp3,        fj_PHSetOpTemp3,
/* 73  */	"OP_TEMP_CAL_1",          &fj_PHGetOpCal1,         fj_PHSetOpCal1,
/* 74  */	"OP_TEMP_CAL_2",          &fj_PHGetOpCal2,         fj_PHSetOpCal2,
/* 75  */	"OP_TEMP_CAL_3",          &fj_PHGetOpCal3,         fj_PHSetOpCal3,
/* 76  */	"OP_AMS",                 &fj_PHGetOpAMS,          fj_PHSetOpAMS,
/* 77  */	"OP_AMSA1",               &fj_PHGetOpAMSA1,        fj_PHSetOpAMSA1,
/* 78  */	"OP_AMSA2",               &fj_PHGetOpAMSA2,        fj_PHSetOpAMSA2,
/* 79  */	"OP_AMSA3",               &fj_PHGetOpAMSA3,        fj_PHSetOpAMSA3,
/* 80  */	"OP_AMSA4",               &fj_PHGetOpAMSA4,        fj_PHSetOpAMSA4,
/* 81  */	"OP_AMSA5",               &fj_PHGetOpAMSA5,        fj_PHSetOpAMSA5,
/* 82  */	"OP_AMSA6",               &fj_PHGetOpAMSA6,        fj_PHSetOpAMSA6,
/* 83  */	"OP_AMSA7",               &fj_PHGetOpAMSA7,        fj_PHSetOpAMSA7,
/* 84  */	"OP_AUTOPRINT",       	  &fj_PHGetAutoPrint,      fj_PHSetAutoPrint, // Burl v 1.1015
#define PH_OP_LAST    84
#define PH_EDIT_FIRST 85
/* 86  */	"EDIT_LOCK",              &fj_PHGetEditLock,       fj_PHSetEditLock,
/* 87  */	"EDIT_IP",                &fj_PHGetEditIPAddr,     fj_PHSetEditIPAddr,
/* 88  */	"EDIT_TIME",              &fj_PHGetEditTime,       fj_PHSetEditTime
#define PH_EDIT_LAST  87
#define PH_LAST 87
};
LONG fj_PHParamCount = sizeof(fj_PHParams)/sizeof(struct fjphtable);

//
//  FUNCTION: fj_PrintHeadMakeParameterString()
//
//  PURPOSE:  make a string with print head parameters
//
FJDLLExport VOID fj_PrintHeadMakeParameterString( LONG lTableFirst, LONG lTableLast, CLPCFJPRINTHEAD pfph, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_PHParams, lTableFirst, lTableLast, (LPVOID)pfph, pString );
	return;
}
//
//  FUNCTION: fj_PrintHeadIDToStringr()
//
//  PURPOSE:  Convert a FJPRINTHEAD ID into a descriptive string
//
FJDLLExport VOID fj_PrintHeadIDToString( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_PARAM_ID, PH_PARAM_ID, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadToString()
//
//  PURPOSE:  Convert a FJPRINTHEAD structure into a descriptive string
//
FJDLLExport VOID fj_PrintHeadToString( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_PARAM_FIRST, PH_PARAM_LAST, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadPackageToString()
//
//  PURPOSE:  Convert a FJPRINTHEAD_PKG structure into a descriptive string
//
FJDLLExport VOID fj_PrintHeadPackageToString( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_PKG_FIRST, PH_PKG_LAST, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadPhysicalToString()
//
//  PURPOSE:  Convert a FJPRINTHEAD_PHY structure into a descriptive string
//
FJDLLExport VOID fj_PrintHeadPhysicalToString( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_PHY_FIRST, PH_PHY_LAST, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadOperationalToString()
//
//  PURPOSE:  Convert a FJPRINTHEAD_OP structure into a descriptive string
//
FJDLLExport VOID fj_PrintHeadOperationalToString( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_OP_FIRST, PH_OP_LAST, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadFromString()
//
//  PURPOSE:  Set parameters in a descriptive string into a FJPRINTHEAD structure
//
FJDLLExport BOOL fj_PrintHeadFromString( LPFJPRINTHEAD pfph, LPSTR pString )
{
	BOOL bRet;

	bRet = fj_SetParametersFromString( (struct fjtable *)fj_PHParams, 0, PH_LAST, (LPVOID)pfph, pString );

	return( bRet );
}
//
//  FUNCTION: fj_PrintHeadListAdd()
//
//  PURPOSE:  Add a FJPRINTHEAD structure to the array of pointers
//
FJDLLExport LONG fj_PrintHeadListAdd( LPFJPRINTHEAD **ppapfphIn,
LONG            lCount,
CLPCFJPRINTHEAD pfph )
{
	LPFJPRINTHEAD *papfphOld;
	LPFJPRINTHEAD *papfphNew;
	LONG  lCountNew;
	int   i;

	papfphOld  = *ppapfphIn;
	lCountNew = lCount + 1;
	papfphNew = (LPFJPRINTHEAD *)fj_malloc( lCountNew * sizeof (LPFJPRINTHEAD) );
	for ( i = 0; i < lCount; i++ )
	{
		papfphNew[i] = papfphOld[i];
	}
	papfphNew[lCount] = (LPFJPRINTHEAD)pfph;

	if ( NULL != papfphOld ) fj_free( papfphOld );
	*ppapfphIn = papfphNew;
	return( lCountNew );
}
//
//  FUNCTION: fj_PrintHeadListDestroyAll()
//
//  PURPOSE:  Destroy all FJPRINTHEAD structures and their list
//
FJDLLExport VOID fj_PrintHeadListDestroyAll( LPFJPRINTHEAD **ppapfphIn, LONG lCount )
{
	LPFJPRINTHEAD *papfphIn;
	LPFJPRINTHEAD  pfph;
	int i;

	papfphIn   = *ppapfphIn;
	if ( NULL != papfphIn )
	{
		*ppapfphIn = NULL;
		for ( i = 0; i < lCount; i++ )
		{
			pfph = papfphIn[i];
			if ( NULL != pfph )
			{
				papfphIn[i] = NULL;
				fj_PrintHeadDestroy( pfph );
			}
		}
		fj_free( papfphIn );
	}

	return;
}
//
//  FUNCTION: fj_PrintHeadGetElementWidth()
//
//  PURPOSE:  Calculate the width of a element
//
FJDLLExport FLOAT fj_PrintHeadGetElementWidth( CLPCFJPRINTHEAD pfph, LPCFJELEMENT pfe )
{
	CLPCFJSYSTEM  pfsys = pfph->pfsys;
	LPFJIMAGE     pfi;
	DOUBLE dAngle, dCos;
	LONG   lWidth;
	LONG   lWidthSlant;
	LONG   lWidthBmp;
	LONG   lWidthHead;
	FLOAT  fWidthHead;
	FLOAT  fWidth;

	pfi   = pfe->pfi;

	dAngle = pfph->fSlantAngle * 3.1415926535 / 180.;
	dCos   = cos( dAngle );

	lWidth = 0;
	if ( 0 != pfi->lHeight )
	{
		lWidth      = pfi->lLength / ((pfi->lHeight + 7) / 8);
										// extra rasters due to slant. positive is to left.
		lWidthSlant = (pfi->lHeight-1) * pfph->lSlant;
										// rasters if no slant
		lWidth      = lWidth - abs(lWidthSlant);
	}

	// calculate virtual rasters added due to print head angle. positive is to right.
	fWidthHead  = pfph->pfphy->fSpanCenter*(FLOAT)dCos*((FLOAT)(pfi->lHeight-1))/((FLOAT)(pfph->pfphy->lChannels-1))*(pfsys->fEncoder);
	lWidthHead  = (LONG)fWidthHead;

	lWidthBmp = lWidthHead - lWidthSlant;
	if ( lWidthBmp > 0 )
	{
		lWidth = lWidth + lWidthBmp;
	}
	else
	{
		lWidth = lWidth - lWidthBmp;
	}

	fWidth = ((FLOAT)(lWidth ))/pfsys->fEncoder;
	return( fWidth );
}
//
//	FUNCTION: fj_PrintHeadIsEdit
//
//	Return: TRUE if edit in progrees for this IP.
//
//  test the edit flag and edit id to see if an edit session is in progress
//
FJDLLExport BOOL fj_PrintHeadIsEdit( CLPCFJPRINTHEAD pfph, LONG lEditID )
{
	BOOL   bRet = FALSE;

	if ( TRUE == pfph->bEdit )
	{
		if ( lEditID == pfph->lEditID )
		{
			bRet = TRUE;				// true
		}
	}
	return( bRet );

}
//
//	FUNCTION: fj_PrintHeadStartEdit
//
//	Return: TRUE if edit started.
//
//  test for no edit in progress, set edit in progress, copy MemStor
//
FJDLLExport BOOL fj_PrintHeadStartEdit( LPFJPRINTHEAD pfph, LONG lEditID )
{
	LPBYTE pByte;
	BOOL   bRet = FALSE;

	if ( FALSE == pfph->bEdit )
	{
		bRet = TRUE;
		pfph->bEdit = TRUE;
		pfph->lEditID = lEditID;
#ifdef NET_OS
		pByte = fj_CVT.pFJglobal->pMemStorBufferEdit;
#else if
		pByte = fj_malloc( pfph->pMemStor->lSize );
#endif
		// saved pre-edit copy of memstor
		pfph->pMemStorEdit = fj_MemStorNewFromBuffer( pByte, pfph->pMemStor->lSize, pfph->pMemStor->pBuffer );
#ifdef NET_OS
		pfph->pMemStorEdit->bDoNotFreeBuffer = TRUE;
#endif
	}
	return( bRet );
}
//
//	FUNCTION: fj_PrintHeadSaveEdit
//
//
//  if edit in progress, copy MemStorEdit to MemStor, end Edit
//
FJDLLExport BOOL fj_PrintHeadSaveEdit( LPFJPRINTHEAD pfph, LONG lEditID, struct tm *pTM )
{
	LPFJMEMSTOR pfmsOld;
	BOOL bRet;

	bRet = fj_PrintHeadIsEdit( pfph, lEditID );
	if ( TRUE == bRet )
	{
#ifdef NET_OS
		{
			LPBYTE pByte;
			pByte = fj_CVT.pFJglobal->pMemStorBufferEdit;
			fj_CVT.pFJglobal->pMemStorBufferEdit = fj_CVT.pFJglobal->pMemStorBuffer;
			fj_CVT.pFJglobal->pMemStorBuffer     = pByte;
		}
#endif
		pfmsOld = pfph->pMemStor;		// save current memstor
										// start using edit copy
		pfph->pMemStor = pfph->pMemStorEdit;
		pfph->pMemStorEdit = NULL;		// clear edit pointer
		fj_MemStorDestroy( pfmsOld );	// destroy previous memstor

		memcpy( &(pfph->tmEditVersion), pTM, sizeof(struct tm) );
		pfph->lEditID = 0;
		
		#ifndef _WINDOWS
		pCVT->fj_SystemGetTime(&fj_tmKeepAlive);
		#endif 

		pfph->bEdit = FALSE;
	}
	return ( bRet );
}
//
//	FUNCTION: fj_PrintHeadCancelEdit
//
//
//  if edit in progress, end it, discard memstoreedit
//
FJDLLExport BOOL fj_PrintHeadCancelEdit( LPFJPRINTHEAD pfph, LONG lEditID )
{
	LPFJMEMSTOR pfmsOld;
	BOOL  bRet;

	bRet = fj_PrintHeadIsEdit( pfph, lEditID );
	if ( TRUE == bRet )
	{
		pfmsOld = pfph->pMemStorEdit;	// save edit memstor
		pfph->pMemStorEdit = NULL;		// clear edit pointer
		fj_MemStorDestroy( pfmsOld );	// destroy edit memstor

		pfph->lEditID = 0;

		#ifndef _WINDOWS
		pCVT->fj_SystemGetTime(&fj_tmKeepAlive);
		#endif

		pfph->bEdit = FALSE;
	}
	return ( bRet );
}
//
//	FUNCTION: fj_PrintHeadGetEditStatus
//
//	Return: Edit status in string
//
FJDLLExport VOID fj_PrintHeadGetEditStatus( CLPCFJPRINTHEAD pfph, LPSTR pStr )
{
	fj_PrintHeadMakeParameterString( PH_EDIT_FIRST, PH_EDIT_LAST, pfph, pStr );
	return;
}
//
//  FUNCTION: fj_PrintHeadBuildSelectedMessage()
//
//  PURPOSE:  Build a message with its segments and fonts
//
//  NOTE: if the name is null or cannot be found, the old selected message will be destroyed,
//             and there will be no selected message.
//
FJDLLExport LPFJMESSAGE fj_PrintHeadBuildSelectedMessage( LPFJPRINTHEAD pfph, LPFJLABEL pfl, LPCSTR pName )
{
	#ifndef _WINDOWS
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	#endif
	
	LPFJMESSAGE  pfm;
	LPFJMESSAGE  pfmOld;
	LPFJMESSAGE  pfmOld2;
	LPFJFONT    *papff;
	LPFJFONT    *papffOld;
	STR sName[FJMESSAGE_NAME_SIZE*2];

	pfm      = NULL;
	// save current message and fonts
	pfmOld   = pfph->pfmSelected;
	pfmOld2  = pfph->pfmSelected2;
	papffOld = pfph->papffSelected;
	papffOld=NULL;

	// destroy old ones FIRST!!!
	if ( NULL != pfmOld2 )
	{
		fj_MessageDestroy( pfmOld2 );
	}
	pfph->pfmSelected2 = pfmOld;

	if ( NULL != pName )
	{
		// build new message
										// message Name
		fj_processParameterStringOutput( sName, pName );
		pfm = fj_MessageBuildFromName( pfph->pMemStor, sName );
	}

	if ( NULL != pfm )
	{
		pfm->pfph = pfph;
		pfm->pfl  = pfl;
		// do message reset to get everything built and rasters built once
		fj_MessageReset( pfm );
	}
	// update to new message
	pfph->pfmSelected   = pfm;

	#ifndef _WINDOWS
	if (pGlobal->bPhotocellInt == FALSE) { // burl
	    pGlobal->bPhotocellInt = TRUE;
	    fj_print_head_SetGAWriteSwitches ();
	}
	#endif
	
	if ( NULL != papffOld )
	{
		papff = papffOld;
		while ( NULL != *papff )
		{
			fj_FontDestroy( *papff );
			papff++;
		}
		fj_free( papffOld );
	}

	return( pfm );
}
//
//  FUNCTION: fj_PrintHeadBuildScannerDefaultMessage()
//
//  PURPOSE:  Build a message with its segments and fonts, save as scanner default message
//
FJDLLExport LPFJMESSAGE fj_PrintHeadBuildScannerDefaultMessage( LPFJPRINTHEAD pfph, LPFJLABEL pfl, LPCSTR pName )
{
	LPFJMESSAGE  pfm;
	LPFJMESSAGE  pfmOld;
	STR sName[FJMESSAGE_NAME_SIZE*2];

	pfm      = NULL;
	// save current message and fonts
	pfmOld   = pfph->pfmScanner;

	if ( NULL != pName )
	{
		// build new message
										// message Name
		fj_processParameterStringOutput( sName, pName );
		pfm = fj_MessageBuildFromName( pfph->pMemStor, sName );
	}
	if ( NULL != pfm )
	{
		pfm->pfph = pfph;
		pfm->pfl  = pfl;
		// do message reset to get everything built and rasters built once
		fj_MessageReset( pfm );
		// update to new message
		pfph->pfmScanner = pfm;

		// destroy old ones
		if ( NULL != pfmOld )
		{
			fj_MessageDestroy( pfmOld );
		}
	}

	return( pfm );
}
//
//  FUNCTION: fj_PrintHeadBuildFont()
//
//  PURPOSE:  Build a font and save its pointer
//
FJDLLExport LPFJFONT fj_PrintHeadBuildFont( LPFJPRINTHEAD pfph, LPCSTR pFontName )
{
	LPFJFONT *papff;
	LPFJFONT *papffOld;
	LPFJFONT  pff;
	STR       sName[FJFONT_NAME_SIZE*2];
	LONG      lCount;
	LONG      i;

	pff = NULL;
	papff = pfph->papffSelected;
	lCount = 0;
	if ( NULL != papff )
	{
		while ( NULL != *papff )
		{
			pff = *papff;
			if ( 0 == strcmp(pFontName, pff->strName) ) break;
			pff = NULL;
			papff++;
			lCount++;
		}
	}
	if ( NULL == pff )
	{
		fj_processParameterStringOutput( sName, pFontName );
		pff = fj_FontBuildFromName( pfph->pMemStor, sName );
		if ( NULL != pff )
		{
			papff = fj_calloc( lCount+2, sizeof(LPFJFONT) );
			for ( i = 0; i < lCount; i++ )
			{
				*(papff+i) = *(pfph->papffSelected+i);
			}
			*(papff+lCount) = pff;
			papffOld = pfph->papffSelected;
			pfph->papffSelected = papff;
			if ( NULL != papffOld ) fj_free( papffOld );
		}
	}

	return( pff );
}
//
//  FUNCTION: fj_PrintHeadGetDefaultFont()
//
//  PURPOSE:  return pointer to the default font
//
FJDLLExport LPFJFONT fj_PrintHeadGetDefaultFont( LPFJPRINTHEAD pfph )
{
	LPFJFONT pff;
	LPSTR *papList;
	LPSTR  apList[3];
	STR    str[64];
	int    k;

	pff = fj_PrintHeadBuildFont( pfph, "fx32x32" );
	if ( NULL == pff ) pff = fj_PrintHeadBuildFont( pfph, "fx16x12" );
	if ( NULL == pff )
	{
		papList = fj_MemStorFindAllElements( pfph->pMemStor, fj_FontID );
		while ( NULL != *papList )
		{
			strncpy( str, *papList, sizeof(str) );
			str[sizeof(str)-2] = '}';	// guarantee end for partial scan
			str[sizeof(str)-1] = 0;
			k = fj_ParseKeywordsFromStr( str, ',', apList, 2 );
			if ( k >= 2 )
			{
				fj_processParameterStringInput( apList[1] );
				pff = fj_PrintHeadBuildFont( pfph, apList[1] );
				break;
			}
			papList++;
		}
	}
	return( pff );
}
extern VOID FontLoadDiskFonts( LPFJPRINTHEAD pfph );
extern BYTE ArialTTFBuffer[] ;

extern BYTE fx32x32_buffer[];
extern BYTE fx16x12_buffer[];
extern BYTE barcode_buffer32[];

//
//  FUNCTION: fj_PrintHeadLoadTestFonts()
//
//  PURPOSE:  Initialize fonts in MemStor
//
FJDLLExport VOID fj_PrintHeadLoadTestFonts( LPFJPRINTHEAD pfph )
{
	LPFJMEMSTOR pfms;
	LPFJFONT pff;
	BOOL bRet;

	pfms = pfph->pMemStorEdit;

	pff = fj_FontFromBuffer( fx32x32_buffer );
	if ( (NULL != pfms) && (NULL != pff) )
	{
		bRet = fj_FontAddToMemstor( pfms, pff );
	}
	fj_FontDestroy( pff );

	pff = fj_FontFromBuffer( fx16x12_buffer );
	if ( (NULL != pfms) && (NULL != pff) )
	{
		bRet = fj_FontAddToMemstor( pfms, pff );
	}
	fj_FontDestroy( pff );

	pff = fj_FontFromBuffer( barcode_buffer32 );
	if ( (NULL != pfms) && (NULL != pff) )
	{
		bRet = fj_FontAddToMemstor( pfms, pff );
	}
	fj_FontDestroy( pff );

#ifdef TRUETYPE
	// temp setup 6th font in Print Head   TTF !!!!!!!!!
	pff = fj_FontNew();

										// font type
	pff->lType         = FJFONT_TRUETYPE;
	pff->lHeight       =  32;			// height of font charascter in pixels  - depends on font
	pff->lWidth        =  32;			// width of font character in pixels - depends on font
	pff->lFirstChar    =  32;			// first defined character in font
	pff->lLastChar     = 255;			// last defined character in font
	pff->lBufLen = 273020;				// Length of font rasters buffer
	pff->pWBuffer = NULL;

										// pointer to font rasters
	pff->pBuffer = fj_malloc( pff->lBufLen );
	if ( NULL != pff->pBuffer )
	{
		memcpy( pff->pBuffer, ArialTTFBuffer, pff->lBufLen );
		strcpy( pff->strName,       "Arial Regular MEM" );
		strcpy( pff->pFontFileName, "arial.mem.ttf" );
	}
#endif TRUETYPE

#ifndef NET_OS
	//  FontLoadDiskFonts( pfph );
#endif

#ifdef _WIN32
	_CrtCheckMemory();
#endif

	return;
}
