#ifndef FJ_BARCODE
#define FJ_BARCODE

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct Barcode_Item;
struct fjelement;
struct fjsysbarcode;

typedef struct fjbarcode
{
	LONG    lSysBCIndex;				// index of global barcode definition
	LONG    lBoldValue;					// amount of boldness. 0 origin.
	LONG    lWidthValue;				// amount of width stretch. 1 origin.
	STR     strText[1+FJBARCODE_SIZE];	// text string
	BOOL    bHROutput;					// true to text output into barcode
	CHAR    cHRAlign;					// text output alignment ( L, R, C)
	LONG    lHRBold;					// text output Bold value
	LPBYTE  pImageHRText;				// pointer to image location for Human Readable Text
	struct Barcode_Item * pBC;			// pointer to BC Lib structure
	struct fjelement FAR *pfeText;		// element for text generation
} FJBARCODE, FAR *LPFJBARCODE, FAR * const CLPFJBARCODE;
typedef const struct fjbarcode FAR *LPCFJBARCODE, FAR * const CLPCFJBARCODE;

//#define BARCODE_FONT "barcode"
#define BARCODE_FONT  "barcode"
#define BARCODE_FONT256 "barcode256"
#endif									// ifndef FJ_BARCODE
