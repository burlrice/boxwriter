#ifndef FJ_BITMAP
#define FJ_BITMAP

#include "fj_base.h"
#include "fj_defines.h"

// forward declarations
struct fjmemstor;

//
// A bitmap element uses one color bitmaps that are independently stored in MemStor.
//

typedef struct fjbitmap
{
	LONG    lBoldValue;					// amount of boldness. 0 origin.
	LONG    lWidthValue;				// amount of width stretch. 1 origin.
	LONG    lbmpSize;					// size of bmp in bytes
	LPBYTE  pbmp;						// pointer to bmp
	STR     strName[1+FJBMP_NAME_SIZE];	// bmp name
} FJBITMAP, FAR *LPFJBITMAP, FAR * const CLPFJBITMAP;
typedef const struct fjbitmap FAR *LPCFJBITMAP, FAR * const CLPCFJBITMAP;

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport BOOL fj_bmpCheckFormat( LPBYTE pbmp );
	FJDLLExport BOOL fj_bmpAddToMemstor( struct fjmemstor FAR *pfms, LPBYTE pbmp, LPCSTR pName );
#ifdef  __cplusplus
}
#endif

//
// the following bitmap structures are copied from Windows
//
#ifdef NET_OS
typedef unsigned short  WORD;
typedef unsigned long  DWORD;
#pragma pack(1)
typedef struct tagBITMAPFILEHEADER		// bmfh
{
	WORD    bfType;
	DWORD   bfSize;
	WORD    bfReserved1;
	WORD    bfReserved2;
	DWORD   bfOffBits;
} BITMAPFILEHEADER;
typedef struct tagBITMAPINFOHEADER		// bmih
{
	DWORD  biSize;
	LONG   biWidth;
	LONG   biHeight;
	WORD   biPlanes;
	WORD   biBitCount;
	DWORD  biCompression;
	DWORD  biSizeImage;
	LONG   biXPelsPerMeter;
	LONG   biYPelsPerMeter;
	DWORD  biClrUsed;
	DWORD  biClrImportant;
} BITMAPINFOHEADER;
#pragma pack()
#endif
#endif									// ifndef FJ_BITMAP
