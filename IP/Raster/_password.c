/* C file: C:\MyPrograms\IP_APP_v01\_password.c created
 by NET+Works HTML to C Utility on Friday, June 28, 2002 11:28:09 */
#include "fj.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_TITLE_PRINTER */
static void na_TITLE_PRINTER (unsigned long handle);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_PWDIST */
static void na_PWDIST (unsigned long handle);
/*  @! File: na_PWSEC */
static void na_PWSEC (unsigned long handle);
/*  @! File: na_PWCONF */
static void na_PWCONF (unsigned long handle);
/*  @! File: na_PWSAVE */
static void na_PWSAVE (unsigned long handle);
/*  @! File: na_PWMSGEDIT */
static void na_PWMSGEDIT (unsigned long handle);
/*  @! File: na_PWCOUNT */
static void na_PWCOUNT (unsigned long handle);
/*  @! File: na_PWTEST */
static void na_PWTEST (unsigned long handle);
/*  @! File: na_PWMSGSEL */
static void na_PWMSGSEL (unsigned long handle);
/*  @! File: na_PWVIEW */
static void na_PWVIEW (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\password.htm {{ */
void Send_function_32(unsigned long handle)
{
	HSSend (handle, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_PRINTER (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Security\"\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "var message=\"WARNING ! This page's source is not viewable.\";\n");
	HSSend (handle, "var errormsg=\"Invalid password conformation for \";\n");
	HSSend (handle, "function click(e) {\n");
	HSSend (handle, "if (document.all) { if (event.button==2||event.button==3) { alert(message); return false; } }\n");
	HSSend (handle, "if (document.layers) { if (e.which == 3) { alert(message); return false; } }\n");
	HSSend (handle, "}\n");
	HSSend (handle, "\n");
	HSSend (handle, "if (document.layers) { document.captureEvents(Event.MOUSEDOWN); }\n");
	HSSend (handle, "document.onmousedown=click;\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<script type=\"text/javascript\">\n");
	HSSend (handle, "function checkpwd(otp,otc,fld) {\n");
	HSSend (handle, "var of=document.formpwd;\n");
	HSSend (handle, "var ret = 1;\n");
	HSSend (handle, "if (otp.value != otc.value){\n");
	HSSend (handle, "alert(errormsg+fld);\n");
	HSSend (handle, "otp.focus();\n");
	HSSend (handle, "otp.select();\n");
	HSSend (handle, "of.pwdstatus.value = \"F\";\n");
	HSSend (handle, "ret = 1;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "else\n");
	HSSend (handle, "if (of.pwdstatus.value != \"F\")ret = 0;\n");
	HSSend (handle, "return ret;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function submitpwd() {\n");
	HSSend (handle, "var of=document.formpwd;\n");
	HSSend (handle, "of.pwdstatus.value = \"T\";\n");
	HSSend (handle, "if (1 == checkpwd(of.pwDist,of.pwDist1,\"Distributor\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwSec,of.pwSec1,\"Security\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwConf,of.pwConf1,\"Configuration\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwSave,of.pwSave1,\"Save/Restore\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwMsgEdit,of.pwMsgEdit1,\"Message Edit\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwCount,of.pwCount1,\"Counters\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwTest,of.pwTest1,\"Test\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwMsgSel,of.pwMsgSel1,\"Message Select\")) return false;\n");
	HSSend (handle, "else if (1 == checkpwd(of.pwView,of.pwView1,\"View All Pages\")) return false;\n");
	HSSend (handle, "if (of.pwdstatus.value != \"F\") of.submit();\n");
	HSSend (handle, "return true;\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</script>\n");
	HSSend (handle, "<p style=\"text-align: center;\"><big><span style=\"font-weight: bold; color: rgb(255, 0, 0);\">&nbsp;WARNING</span>:\n");
	HSSend (handle, "<span style=\"text-decoration: underline; color: rgb(0, 0, 153);\">\"SECURITY\"\n");
	HSSend (handle, "PASSWORD SHOULD BE ESTABLISHED BEFORE ASSIGNING PASSWORDS TO ANY OTHER\n");
	HSSend (handle, "LEVEL!</span></big><br>\n");
	HSSend (handle, "</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p align=\"center\">\n");
	HSSend (handle, "</p>\n");
	HSSend (handle, "<form name=\"formpwd\" action=\"pw_a\" method=\"post\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" name=\"pwdstatus\" value=\"0\">\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table cellpadding=\"8\" border=\"0\">\n");
	HSSend (handle, "<col align=\"right\">\n");
	HSSend (handle, "<col align=\"left\">\n");
	HSSend (handle, "<tbody>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td><b>Security Level</b></td>\n");
	HSSend (handle, "<td><b>Password</b></td>\n");
	HSSend (handle, "<td><b>Confirm Password</b></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Distributor</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwDist\" size=\"20\" value=\"");
	na_PWDIST (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwDist1\" size=\"20\" value=\"");
	na_PWDIST (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Security</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwSec\" size=\"20\" value=\"");
	na_PWSEC (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwSec1\" size=\"20\" value=\"");
	na_PWSEC (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Configuration</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwConf\" size=\"20\" value=\"");
	na_PWCONF (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwConf1\" size=\"20\" value=\"");
	na_PWCONF (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Save / Restore</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwSave\" size=\"20\" value=\"");
	na_PWSAVE (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwSave1\" size=\"20\" value=\"");
	na_PWSAVE (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Message Edit</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwMsgEdit\" size=\"20\" value=\"");
	na_PWMSGEDIT (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwMsgEdit1\" size=\"20\" value=\"");
	na_PWMSGEDIT (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Counters</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwCount\" size=\"20\" value=\"");
	na_PWCOUNT (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwCount1\" size=\"20\" value=\"");
	na_PWCOUNT (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Test</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwTest\" size=\"20\" value=\"");
	na_PWTEST (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwTest1\" size=\"20\" value=\"");
	na_PWTEST (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>Message Select</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwMsgSel\" size=\"20\" value=\"");
	na_PWMSGSEL (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwMsgSel1\" size=\"20\" value=\"");
	na_PWMSGSEL (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td>View All Pages</td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwView\" size=\"20\" value=\"");
	na_PWVIEW (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "<td><input type=\"password\" name=\"pwView1\" size=\"20\" value=\"");
	na_PWVIEW (handle);
	HSSend (handle, "\"></td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "<tr>\n");
	HSSend (handle, "<td colspan=\"3\">\n");
	HSSend (handle, "<center><button class=\"fjbtn1\" onclick=\"return submitpwd();\">Submit</button><input type=\"reset\" class=\"fjbtn1\" value=\"Reset\"></center>\n");
	HSSend (handle, "</td>\n");
	HSSend (handle, "</tr>\n");
	HSSend (handle, "</tbody>\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "All passwords must be different. Access is granted by matching the entered\n");
	HSSend (handle, "password.\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "All lower levels up to the first level with a password are available to anyone.\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\password.htm }} */
/* @! End HTML page   */

void reload_password(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=password.htm");
	}
	else
	{
		fj_reload_page(handle, "password.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_pw_a  {{ */
void Post_pw_a(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_PARAM pParam = pCVT->pFJparam;
	LPFJSYSTEM pfsys  = pCVT->pfsys;
	LPFJSYSTEM pfsysP = &(pParam->FJSys);
	char   formField[1+FJSYS_PASSWORD_SIZE];
	LPSTR  pPasswordDist;
	LPSTR  pPasswordSec;
	LPSTR  pPasswordConf;
	LPSTR  pPasswordSave;
	LPSTR  pPasswordMsgEdit;
	LPSTR  pPasswordCount;
	LPSTR  pPasswordTest;
	LPSTR  pPasswordMsgSel;
	LPSTR  pPasswordView;
	LPSTR  pPassword;
	int    retrom;
	BOOL   bChanged;
	BOOL   bROMWait;
	int    ret;
	bROMWait = FALSE;

	pCVT->tx_semaphore_get( &(pCVT->pFJglobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	if ( 0 == retrom  )
	{
		pPasswordDist    = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_DISTRIBUTOR );
		pPasswordSec     = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_SECURITY );
		pPasswordConf    = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_CONFIGURATION );
		pPasswordSave    = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_RESTORE );
		pPasswordMsgEdit = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_MESSAGE_EDIT );
		pPasswordCount   = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_COUNTERS );
		pPasswordTest    = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_TEST );
		pPasswordMsgSel  = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_MESSAGE_SELECT );
		pPasswordView    = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsysP, FJSYS_PRIORITY_VIEW_ALL );
		ret = fj_HSGetValue( handle, "pwDist",    pPasswordDist,    1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwSec",     pPasswordSec,     1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwConf",    pPasswordConf,    1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwSave",    pPasswordSave,    1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwMsgEdit", pPasswordMsgEdit, 1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwCount",   pPasswordCount,   1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwTest",    pPasswordTest,    1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwMsgSel",  pPasswordMsgSel,  1+FJSYS_PASSWORD_SIZE );
		ret = fj_HSGetValue( handle, "pwView",    pPasswordView,    1+FJSYS_PASSWORD_SIZE );
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		bROMWait = TRUE;
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_DISTRIBUTOR );
		strcpy( pPassword, pPasswordDist );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_SECURITY );
		strcpy( pPassword, pPasswordSec );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_CONFIGURATION );
		strcpy( pPassword, pPasswordConf );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_RESTORE );
		strcpy( pPassword, pPasswordSave );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_EDIT );
		strcpy( pPassword, pPasswordMsgEdit );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_COUNTERS );
		strcpy( pPassword, pPasswordCount );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_TEST );
		strcpy( pPassword, pPasswordTest );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_SELECT );
		strcpy( pPassword, pPasswordMsgSel );
		pPassword = (LPSTR)pCVT->fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_VIEW_ALL );
		strcpy( pPassword, pPasswordView );
	}
	else
	{
		pCVT->tx_semaphore_put( &(pCVT->pFJglobal->semaROMLock) );
	}

	reload_password(handle,bROMWait);
}										/* @! Function: Post_pw_a  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_TITLE_PRINTER  {{ */
void na_TITLE_PRINTER(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Security" );
}										/* @! Function: na_TITLE_PRINTER  }} */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_PWDIST  {{ */
void na_PWDIST(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_DISTRIBUTOR ) );
}										/* @! Function: na_PWDIST  }} */
/* @! Function: na_PWSEC  {{ */
void na_PWSEC(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_SECURITY ) );
}										/* @! Function: na_PWSEC  }} */
/* @! Function: na_PWCONF  {{ */
void na_PWCONF(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_CONFIGURATION ) );
}										/* @! Function: na_PWCONF  }} */
/* @! Function: na_PWSAVE  {{ */
void na_PWSAVE(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_RESTORE ) );
}										/* @! Function: na_PWSAVE  }} */
/* @! Function: na_PWMSGEDIT  {{ */
void na_PWMSGEDIT(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_MESSAGE_EDIT ) );
}										/* @! Function: na_PWMSGEDIT  }} */
/* @! Function: na_PWCOUNT  {{ */
void na_PWCOUNT(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_COUNTERS ) );
}										/* @! Function: na_PWCOUNT  }} */
/* @! Function: na_PWTEST  {{ */
void na_PWTEST(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_TEST ) );
}										/* @! Function: na_PWTEST  }} */
/* @! Function: na_PWMSGSEL  {{ */
void na_PWMSGSEL(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_MESSAGE_SELECT ) );
}										/* @! Function: na_PWMSGSEL  }} */
/* @! Function: na_PWVIEW  {{ */
void na_PWVIEW(unsigned long handle)
{
	/* add your implementation here: */
	HSSend (handle, (char *)pCVT->fj_SystemGetPasswordForLevel( pCVT->pfsys, FJSYS_PRIORITY_VIEW_ALL ) );
}										/* @! Function: na_PWVIEW  }} */
/* @! Function: na_PWID  {{ */
/* @! End of dynamic functions   */
