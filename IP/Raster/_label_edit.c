/* C file: c:\MyPrograms\FoxJet1\_label_edit.c created
 by NET+Works HTML to C Utility on Monday, November 19, 2001 16:03:02 */
#include "fj.h"
#include "barcode.h"
#include "fj_misc.h"
#include "fj_defines.h"
#include "fj_message.h"
#include "fj_font.h"
#include "fj_barcode.h"
#include "fj_bitmap.h"
#include "fj_counter.h"
#include "fj_datetime.h"
#include "fj_dynimage.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_text.h"
#include "fj_datamatrix.h"
#include "fj_image.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"
#include "fj_memstorage.h"
#include "fj_element.h"

static CHAR sLblEditName[FJLABEL_NAME_SIZE+1];
static CHAR sLblEditNameQuote[FJLABEL_NAME_SIZE*2];

static CHAR sSelMsg[FJMESSAGE_NAME_SIZE+1];
static CHAR sSelMsgQuote[FJMESSAGE_NAME_SIZE*2];
static LONG lSMsgEditSegs;

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_LBL_EDIT_PARAM */
static void na_LBL_EDIT_PARAM (unsigned long handle);
/*  @! File: na_LBL_IDS */
static void na_LBL_IDS (unsigned long handle);
/*  @! File: na_LE_title */
static void na_LE_title (unsigned long handle);
/*  @! File: na_LBL_EDIT_TABLE */
static void na_LBL_EDIT_TABLE (unsigned long handle);
/*  @! File: na_button_submit */
static void na_button_submit (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_MSG_EDIT_TABLE */
static void na_MSG_EDIT_TABLE (unsigned long handle);
/*  @! File: na_button_addcopy */
static void na_button_addcopy (unsigned long handle);
/*  @! File: na_LBL_JS_VAR */
static void na_LBL_JS_VAR (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\label_edit.htm {{ */
void Send_function_15(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<link rel=\"stylesheet\" href=\"fjipstyle1.css\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>Label Edit</title>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "");
	na_LBL_EDIT_PARAM (handle);
	HSSend (handle, "\n");
	HSSend (handle, "");
	na_LBL_IDS (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\" SRC=\"fjscript_edit.htm\"></SCRIPT>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "var of=document.form_le;\n");
	HSSend (handle, "var ofid = of.lbl_id;\n");
	HSSend (handle, "var idok = IDCheck(ofid);\n");
	HSSend (handle, "if ( true == idok ){of.action.value=sa; of.item.value=si;of.submit();}\n");
	HSSend (handle, "}\n");
	HSSend (handle, "function myreset(formObj) {\n");
	HSSend (handle, "formObj.reset();\n");
	HSSend (handle, "}\n");
	HSSend (handle, "");
	na_LBL_JS_VAR (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<p class=\"fjh2\" align=\"left\">");
	na_ID (handle);
	HSSend (handle, " : ");
	na_LE_title (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<form name=\"form_le\" action=\"lbledit_1\" method=\"POST\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" value=\"none\" name=\"action\">\n");
	HSSend (handle, "<input type=\"hidden\" value=\"none\" name=\"item\"> ");
	na_LBL_EDIT_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<table border=\"0\" cellpadding=\"1\">\n");
	HSSend (handle, "");
	na_MSG_EDIT_TABLE (handle);
	HSSend (handle, "</table>\n");
	HSSend (handle, "<p>");
	na_button_submit (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "<p align=\"left\">");
	na_button_addcopy (handle);
	HSSend (handle, "</p>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "</body>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\label_edit.htm }} */
/* @! End HTML page   */

void reload_lable_edit(unsigned long handle, LPSTR pID)
{
	char str[99];
	sprintf( str, "label_edit.htm?lbl_edit=%s", pID );
	fj_reload_page(handle, str);
}
void na_processBarcodeElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJBARCODE  pfb;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    i;
	int    ret;

	pfb = (LPFJBARCODE)pfe->pDesc;
	if ( NULL != pfb )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "text" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBARCODE_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfb->strText, formField );
		}

		strcpy( FieldName, "style" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lSysBCIndex = lValue;
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lWidthValue = lValue;
		}

		strcpy( FieldName, "textalign" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( '0' == formField[0] )
			{
				pfb->bHROutput = FALSE;
			}
			else
			{
				pfb->bHROutput = TRUE;
				pfb->cHRAlign = formField[0];
			}
		}

		strcpy( FieldName, "textbold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfb->lHRBold = lValue;
		}
	}
}
void na_processDynBarcodeElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDYNBARCODE  pfdb;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    i;
	int    ret;

	pfdb = (LPFJDYNBARCODE)pfe->pDesc;
	if ( NULL != pfdb )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "datasrc" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBARCODE_SIZE+1 );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			if ( 0 <lValue && FJMESSAGE_DYNAMIC_SEGMENTS+1> lValue )
				pfdb->lDynamicID = lValue-1;
		}

		strcpy( FieldName, "style" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lSysBCIndex = lValue;
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lWidthValue = lValue;
		}

		strcpy( FieldName, "textalign" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( '0' == formField[0] )
			{
				pfdb->bHROutput = FALSE;
			}
			else
			{
				pfdb->bHROutput = TRUE;
				pfdb->cHRAlign = formField[0];
			}
		}

		strcpy( FieldName, "textbold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lHRBold = lValue;
		}
		strcpy( FieldName, "start" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lFirstChar = lValue-1;
		}

		strcpy( FieldName, "limit" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdb->lNumChar = lValue;
		}


	}
}
void na_processDataMatrixElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDATAMATRIX pfdb;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    i;
	int    ret;

	pfdb = (LPFJDATAMATRIX)pfe->pDesc;
	if ( NULL != pfdb )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "datasrc" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBARCODE_SIZE+1 );
		if ( 0 < ret )
		{
		    if (!strcmp (formField, "Not used"))
			pfdb->lDynIndex = 0;
		    else {
			lValue = atol (formField);

			if (lValue > 0 && lValue <= FJMESSAGE_DYNAMIC_SEGMENTS)
			    pfdb->lDynIndex = lValue;
		    }
		}

		strcpy (FieldName, "style"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    pfdb->lSysBCIndex = atol (formField);

		strcpy (FieldName, "start"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    pfdb->lDynFirstChar = atol (formField);

		strcpy (FieldName, "limit"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    pfdb->lDynNumChar = atol (formField);

		strcpy (FieldName, "text"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    strncpy (pfdb->strText, formField, FJDATAMATRIX_SIZE);

		strcpy (FieldName, "cx"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    pfdb->cx = atol (formField);

		strcpy (FieldName, "cy"); strcat (FieldName, FieldNumber);
		if (fj_HSGetValue (handle, FieldName, formField, sizeof(formField)) > 0)
		    pfdb->cy = atol (formField);

	}
}
void na_processDynTextElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDYNTEXT pfdynt;
	char   formField[FJFONT_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfdynt = (LPFJDYNTEXT)pfe->pDesc;
	if ( NULL != pfdynt )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJFONT_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfdynt->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lGapValue = lValue;
		}

		strcpy( FieldName, "datasrc" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			if ( 0 <lValue && FJMESSAGE_DYNAMIC_SEGMENTS+1> lValue )
				pfdynt->lDynamicID = lValue-1;
		}

		strcpy( FieldName, "start" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lFirstChar = lValue-1;
		}

		strcpy( FieldName, "limit" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lNumChar = lValue;
		}

		strcpy( FieldName, "length" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdynt->lLength = lValue;
		}

		strcpy( FieldName, "align" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 'L' == formField[0] ) pfdynt->cAlign = 'L';
			if ( 'R' == formField[0] ) pfdynt->cAlign = 'R';
		}

		strcpy( FieldName, "fill" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfdynt->cFill = formField[0];
		}
		else pfdynt->cFill = 0;
	}
}
void na_processBitmapElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJBITMAP pfbm;
	char   formField[FJBITMAP_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfbm = (LPFJBITMAP)pfe->pDesc;
	if ( NULL != pfbm )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "image" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJBITMAP_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfbm->strName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfbm->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfbm->lWidthValue = lValue;
		}
	}
}
void na_processCounterElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJCOUNTER pfc;
	char   formField[FJFONT_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfc = (LPFJCOUNTER)pfe->pDesc;
	if ( NULL != pfc )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJFONT_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfc->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lGapValue = lValue;
		}

		strcpy( FieldName, "change" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lChange = lValue;
		}

		strcpy( FieldName, "repeat" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lRepeat = lValue;
		}

		strcpy( FieldName, "start" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lStart = lValue;
		}

		strcpy( FieldName, "limit" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lLimit = lValue;
		}

		strcpy( FieldName, "length" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfc->lLength = lValue;
		}

		strcpy( FieldName, "align" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 'L' == formField[0] ) pfc->cAlign = 'L';
			if ( 'R' == formField[0] ) pfc->cAlign = 'R';
		}

		strcpy( FieldName, "fill" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfc->cFill = formField[0];
		}
	}
}
void na_processDatetimeElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDATETIME pfdt;
	char   formField[FJFONT_NAME_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfdt = (LPFJDATETIME)pfe->pDesc;
	if ( NULL != pfdt )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJFONT_NAME_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pfdt->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lGapValue = lValue;
		}

		strcpy( FieldName, "format" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lFormat = lValue;
		}

		strcpy( FieldName, "time" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lTimeType = lValue;
		}

		strcpy( FieldName, "delim" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfdt->cDelim = formField[0];
		}

		strcpy( FieldName, "strings" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( '1' == formField[0] ) pfdt->bStringNormal = TRUE;
			if ( '0' == formField[0] ) pfdt->bStringNormal = FALSE;
		}

		strcpy( FieldName, "shifts" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			if ( (lValue > 0) && (lValue <= 24) )pfdt->lHourACount = lValue;
		}

		strcpy( FieldName, "length" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfdt->lLength = lValue;
		}

		strcpy( FieldName, "align" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 'L' == formField[0] ) pfdt->cAlign = 'L';
			if ( 'R' == formField[0] ) pfdt->cAlign = 'R';
		}

		strcpy( FieldName, "fill" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pfdt->cFill = formField[0];
		}
	}
}
void na_processDynimageElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJDYNIMAGE pfd;
	char   formField[8];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pfd = (LPFJDYNIMAGE)pfe->pDesc;
	if ( NULL != pfd )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfd->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pfd->lWidthValue = lValue;
		}
	}
}
void na_processTextElementL( unsigned long handle, LPFJELEMENT pfe, int iFieldNumber )
{
	LPFJTEXT      pft;
	char   formField[FJTEXT_SIZE+1];
	char   FieldName[20];
	char   FieldNumber[4];
	LONG   lValue;
	int    ret;

	pft = (LPFJTEXT)pfe->pDesc;
	if ( NULL != pft )
	{
		sprintf( FieldNumber, "%i", (int)iFieldNumber );

		strcpy( FieldName, "text" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, FJTEXT_SIZE+1 );
		if ( 0 < ret )
		{
			strcpy( pft->strText, formField );
		}

		strcpy( FieldName, "font" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, min(sizeof(formField),FJFONT_NAME_SIZE+1) );
		if ( 0 < ret )
		{
			strcpy( pft->strFontName, formField );
		}

		strcpy( FieldName, "bold" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lBoldValue = lValue;
		}

		strcpy( FieldName, "width" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lWidthValue = lValue;
		}

		strcpy( FieldName, "gap" );
		strcat( FieldName, FieldNumber );
		ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
		if ( 0 < ret )
		{
			lValue = atol( formField );
			pft->lGapValue = lValue;
		}
	}
}
//
// routine to avoid compiler blowing up
//
void Post_lbledit_1bug(unsigned long handle)
{
}
//
// routine to avoid compiler blowing up
//
void Post_lbledit_2bug(unsigned long handle)
{
}
/* @! Begin of post function */
/* @! Function: Post_lbledit_1  {{ */
void Post_lbledit_1(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMESSAGE   pfmROM;
	LPFJMESSAGE   pfm;
	LPFJELEMENT  *papfe;
	LPFJELEMENT   pfeNew;
	LPFJELEMENT   pfeAdd;
	LPFJELEMENT   pfe;
	LPFJBARCODE   pfb;
	LPFJBITMAP    pfbm;
	LPFJCOUNTER   pfc;
	LPFJDATETIME  pfdt;
	LPFJTEXT      pft;
	LPFJLABEL     pfl;
	LONG   lValue;
	LONG   lCountOld;
	FLOAT  fValue;
	//    char   formField[FJMESSAGE_NAME_SIZE+1];
	char   formField[256];
	char   FieldName[20];
	char   FieldNumber[4];
	char   action[16];
	char   MsgName[FJMESSAGE_NAME_SIZE+1];
	char   MsgNameQ[FJMESSAGE_NAME_SIZE*2];
	char   LblName[FJLABEL_NAME_SIZE+1];
	int    i;
	int    ret;
	BOOL   bRet;
	int    iSeg;
	int    iSegOld;
	BOOL   bNewMsg;
	BOOL    bClose;

	pfm = NULL;
	pfl = NULL;

	bNewMsg = FALSE;
	ret = fj_HSGetValue( handle, "lbl_id", LblName, sizeof(LblName) );
	if ( 0 < ret )
	{
		strcpy ( MsgName, LblName );
		ret = fj_HSGetValue( handle, "msgname", formField, sizeof(formField) );
		// check to see if the message name changed - this is a new message
		if ( 0 != strcmp(MsgName, formField) ) bNewMsg = TRUE;

		ret = fj_HSGetValue( handle, "action", action, sizeof(action) );
		if ( 0 < ret )
		{
			if ( (0 == strcmp(action,"delete")) || (0 == strcmp(action,"update")) || (0 == strcmp(action,"elemadd")) ||(0 == strcmp(action,"elemcopy")) )
			{
				//bNewMsg = TRUE;
				pfm = pCVT->fj_MessageNew();
				pfm->pfph = pfph;
				strcpy( pfm->strName, MsgName );
				iSeg = 0;
				ret = fj_HSGetValue( handle, "segments", formField, sizeof(formField) );
				if ( 0 < ret ) iSeg = atol(formField);
				for ( i = 0; i < iSeg; i++ )
				{
					sprintf( FieldNumber, "%i", (int)i );
					strcpy( FieldName, "segtype" );
					strcat( FieldName, FieldNumber );
					ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
					if ( 0 < ret )		// there has to be a type for the segment to be processed
					{
						pfe = pCVT->fj_ElementNewFromTypeString( formField);
						if ( NULL != pfe )
						{
							pfe->pfm = pfm;
							// add the new one to the array
							pfm->lCount = pCVT->fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfe );

							strcpy( FieldName, "segname" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, min(sizeof(formField)-1,FJELEMENT_NAME_SIZE+1) );
							if ( (0 < ret) && (0 !=  formField[0]) && (FALSE == bNewMsg) )
							{
								strcpy( pfe->strName, formField );
							}
							else
							{
								pCVT->fj_ElementMakeMemstorName( pfe, pfe->strName );
								// add segment & element to ROM/RAM buffer
								pfe->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfe );
							}

							pfe->lPosition = FJ_POSITION_DISTANCE;
							strcpy( FieldName, "concat" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lPosition = FJ_POSITION_CONCATENATE;
							strcpy( FieldName, "parallel" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lPosition = FJ_POSITION_PARALLEL;

							strcpy( FieldName, "Left" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret )
							{
								fValue = atof( formField );
								pfe->fLeft = pCVT->fj_FloatUnits( fValue, - pfsys->lUnits );
							}

							strcpy( FieldName, "nozzle" );
							strcat( FieldName, FieldNumber );
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret )
							{
								lValue = atol( formField );
								pfe->lRow = lValue;
							}

							lValue = pfe->pActions->fj_DescGetType();
							if      ( FJ_TYPE_BARCODE    == lValue ) na_processBarcodeElementL ( handle, pfe, i );
							else if ( FJ_TYPE_BITMAP     == lValue ) na_processBitmapElementL  ( handle, pfe, i );
							else if ( FJ_TYPE_COUNTER    == lValue ) na_processCounterElementL ( handle, pfe, i );
							else if ( FJ_TYPE_DATETIME   == lValue ) na_processDatetimeElementL( handle, pfe, i );
							else if ( FJ_TYPE_DYNIMAGE   == lValue ) na_processDynimageElementL( handle, pfe, i );
							else if ( FJ_TYPE_TEXT       == lValue ) na_processTextElementL    ( handle, pfe, i );
							else if ( FJ_TYPE_DYNTEXT    == lValue ) na_processDynTextElementL ( handle, pfe, i );
							else if ( FJ_TYPE_DYNBARCODE == lValue ) na_processDynBarcodeElementL( handle, pfe, i );
							else if ( FJ_TYPE_DATAMATRIX == lValue ) na_processDataMatrixElementL( handle, pfe, i );

							pfe->lTransform = 0;
							strcpy( FieldName, "reverse" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_REVERSE;
							strcpy( FieldName, "inverse" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_INVERSE;
							strcpy( FieldName, "negative" );
							strcat( FieldName, FieldNumber );
							// checkbox only returns a value if it is CHECKED
							ret = fj_HSGetValue( handle, FieldName, formField, sizeof(formField) );
							if ( 0 < ret ) pfe->lTransform |= FJ_TRANS_NEGATIVE;
						}
					}
				}						// end of for loop thru elements

				bClose = TRUE;
				pfeNew = NULL;
				if ( 0 == strcmp(action,"elemadd") )
				{
					ret = fj_HSGetValue( handle, "addtype", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						if ( 0 == strcmp(formField, "Barcode") )
						{
							pfeNew = pCVT->fj_ElementBarCodeNew();
							pfb = (LPFJBARCODE)pfeNew->pDesc;
							strcpy( pfb->strText, "new" );
						}
						else if ( 0 == strcmp(formField, "Bitmap") )
						{
							pfeNew = pCVT->fj_ElementBitmapNew();
						}
						else if ( 0 == strcmp(formField, "Counter") )
						{
							pfeNew = pCVT->fj_ElementCounterNew();
						}
						else if ( 0 == strcmp(formField, "Datetime") )
						{
							pfeNew = pCVT->fj_ElementDatetimeNew();
						}
						else if ( 0 == strcmp(formField, "Dynimage") )
						{
							pfeNew = pCVT->fj_ElementDynimageNew();
						}
						else if ( 0 == strcmp(formField, "Text") )
						{
							pfeNew = pCVT->fj_ElementTextNew();
							pft = (LPFJTEXT)pfeNew->pDesc;
							strcpy( pft->strText, "new" );
						}
						else if ( 0 == strcmp(formField, "Dyntext") )
						{
							pfeNew = pCVT->fj_ElementDynTextNew();
						}
						else if ( 0 == strcmp(formField, "Dynbarcode") )
						{
							pfeNew = pCVT->fj_ElementDynBarCodeNew();
						}
						else if ( 0 == strcmp(formField, "DataMatrix") )
						{
							pfeNew = pCVT->fj_ElementDataMatrixNew();
						}
					}
					ret = fj_HSGetValue( handle, "addloc", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
					}
					bClose = FALSE;
				}
				else if ( 0 == strcmp(action,"elemcopy") )
				{
					ret = fj_HSGetValue( handle, "copyfrom", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
						if ( iSeg < pfm->lCount )
						{
							pfe = pfm->papfe[iSeg];
							pfeNew = pCVT->fj_ElementDup( pfe );
						}
					}
					ret = fj_HSGetValue( handle, "copyto", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						iSeg = atoi( formField );
						iSeg--;
					}
					bClose = FALSE;
				}
				else if ( 0 == strcmp(action,"delete") )
				{
					ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
					if ( 0 < ret )
					{
						i = atoi(formField);
						pfe = pfm->papfe[i];
						if ( NULL != pfe )
						{
							pfm->lCount = pCVT->fj_ElementListRemove( &(pfm->papfe), pfm->lCount, pfe );
							pfe->pActions->fj_DescDeleteFromMemstor( pfph->pMemStorEdit, pfe );
							pfe->pActions->fj_DescDestroy( pfe );
						}
					}
					bClose = FALSE;
				}

				if ( NULL != pfeNew )
				{
					pfeNew->pfm = pfm;
					papfe = pfm->papfe;
					lCountOld = pfm->lCount;
					pfm->papfe = NULL;
					pfm->lCount = 0;
					iSegOld = 0;
					for ( i = 0; i <= lCountOld; i++ )
					{
						if ( i == iSeg )
						{
							pfeAdd = pfeNew;
							pCVT->fj_ElementMakeMemstorName( pfeAdd, pfeAdd->strName );
							// add segment & element to ROM/RAM buffer
							pfeAdd->pActions->fj_DescAddToMemstor( pfph->pMemStorEdit, pfeAdd );
						}
						else
						{
							pfeAdd = papfe[iSegOld];
							papfe[iSegOld] = NULL;
							iSegOld++;
						}
						pfm->lCount = pCVT->fj_ElementListAdd( &(pfm->papfe), pfm->lCount, pfeAdd );
					}
					pCVT->fj_ElementListDestroyAll( &papfe, 0 );
				}

				// add to MemStor
				bRet = pCVT->fj_MessageAddToMemstor( pfph->pMemStorEdit, pfm );
				pCVT->fj_MessageDestroy( pfm );

				/*		pfl = pCVT->fj_LabelBuildFromName( pfph->pMemStorEdit, sLblEditNameQuote );

								if ( NULL != pfl )
								{
									strcpy( pfl->fmMessageIDs[0],MsgName );
																				 // add to MemStor
									bRet = pCVT->fj_LabelAddToMemstor( pfph->pMemStorEdit, pfl );
				pCVT->fj_LabelDestroy( pfl );
				}*/

				bNewMsg = TRUE;
			}

			if ( (0 == strcmp(action,"update")) || (0 == strcmp(action,"create")) || (TRUE == bNewMsg) )
			{
				pfl = pCVT->fj_LabelNew();
				strcpy( pfl->strName, LblName );
				ret = fj_HSGetValue( handle, "Width", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					fValue = atof( formField );
					pfl->fBoxWidth = pCVT->fj_FloatUnits( fValue, - pfsys->lUnits );
				}
				ret = fj_HSGetValue( handle, "Height", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					fValue = atof( formField );
					pfl->fBoxHeight = pCVT->fj_FloatUnits( fValue, - pfsys->lUnits );
				}
				ret = fj_HSGetValue( handle, "expv", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					pfl->lExpValue = atoi( formField );
				}
				ret = fj_HSGetValue( handle, "expu", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					pfl->lExpUnits = atoi( formField );
				}

				pfl->bExpRoundUp = FALSE;
				ret = fj_HSGetValue( handle, "expr_up", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					pfl->bExpRoundUp = TRUE;
					pfl->bExpRoundDown = FALSE;
				}

				pfl->bExpRoundDown = FALSE;
				ret = fj_HSGetValue( handle, "expr_down", formField, sizeof(formField) );
				if ( 0 < ret )
				{
					pfl->bExpRoundUp = FALSE;
					pfl->bExpRoundDown = TRUE;
				}

				if (0 == strcmp(action,"create"))
					bClose = FALSE;
				//                else
				strcpy( pfl->fmMessageIDs[0],MsgName );

				/*
				for ( i = 0; i < pfsys->lActiveCount; i++ )
				{
					sprintf( FieldNumber, "%i", i );
					strcpy( FieldName, "lbl" );
					strcat( FieldName, FieldNumber );
					ret = fj_HSGetValue( handle, FieldName, formField, FJMESSAGE_NAME_SIZE+1 );
				if ( 0 < ret )
				{
				if ( 0 == strcmp( formField, "(none)" ) )
				pfl->fmMessageIDs[i][0] = 0;
				else
				{
				strcpy( pfl->fmMessageIDs[i], formField );
				}
				}
				} */

				if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle)) )
				{
					// add to MemStor
					bRet = pCVT->fj_LabelAddToMemstor( pfph->pMemStorEdit, pfl );
				}
			}
			if ( TRUE == bClose )
			{
				HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">opener.location.replace(\"label.htm?pwid=");
				na_PWID(handle);
				HSSend(handle, "\");</SCRIPT>");
				HSSend(handle, "<SCRIPT LANGUAGE=\"JavaScript\">window.close();</SCRIPT>");
			}
			else
			{
				reload_lable_edit(handle, LblName);
			}
			if ( NULL != pfm )pCVT->fj_MessageDestroy( pfm );
			if ( NULL != pfl )pCVT->fj_LabelDestroy( pfl );
		}
	}
}										/* @! Function: Post_lbledit_1  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_ID  {{ */
/* @! Function: na_LBL_EDIT_PARAM  {{ */
void na_LBL_EDIT_PARAM(unsigned long handle)
{
	/* add your implementation here: */
	char   formField[FJLABEL_NAME_SIZE+1];
	int    ret;

	sLblEditName[0] = 0;
	ret = fj_HSGetValue( handle, "lbl_edit", formField, sizeof(formField)-1 );
	if ( 0 < ret )
	{
		if ( 0 != strcmp( formField, "-1") )
		{
			strcpy( sLblEditName, formField );
			// label Name
			pCVT->fj_processParameterStringOutput( sLblEditNameQuote, sLblEditName );
		}
		else
		{
			// sLblEditName is already a 0
			strcpy( sLblEditNameQuote, "\"\"" );
		}
	}
}										/* @! Function: na_LBL_EDIT_PARAM  }} */
/* @! Function: na_LBL_IDS  {{ */
void na_LBL_IDS(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPSTR *papLbl;
	LPSTR  pParams[2];
	STR    str[FJLABEL_MEMSTOR_SIZE];
	int i;

	HSSend(handle, "fjID = \"");
	HSSend(handle, sLblEditName);
	HSSend(handle, "\"\n");
	HSSend(handle, "fjIDs = new Array(");
	if ( NULL != pfph->pMemStorEdit )
	{
		papLbl = pCVT->fj_MemStorFindAllElements( pfph->pMemStorEdit, pCVT->pfj_LabelID );
		for ( i = 0; NULL != *(papLbl+i); i++ )
		{
			strcpy( str, *(papLbl+i) );
			pCVT->fj_ParseKeywordsFromStr( str, ',', pParams, 2 );
			pCVT->fj_processParameterStringInput( pParams[1] );
			if ( 0 != i ) HSSend(handle, ",");
			HSSend(handle, "\"");
			na_String(handle, pParams[1]);
			HSSend(handle, "\"");
			if ( 0 == ((i+1)%10) ) HSSend(handle, "\n");
		}
	}
	HSSend(handle, ")\n");
}										/* @! Function: na_LBL_IDS  }} */
/* @! Function: na_LE_title  {{ */
void na_LE_title(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle)) )
	{
		HSSend(handle, "Label Edit");
	}
	else
	{
		HSSend(handle, "Label Details");
	}
}										/* @! Function: na_LE_title  }} */
/* @! Function: na_LBL_EDIT_TABLE  {{ */
void na_LBL_EDIT_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMEMSTOR   pfms;
	LPFJLABEL     pfl;
	LPSTR *papMsgOrg;
	LPSTR *papMsg;
	LPSTR  pParams[3];
	STR    sMsg[FJMESSAGE_MEMSTOR_SIZE];
	STR    str[16];
	BOOL   bEdit;
	BOOL   bNewLabel;
	int    i;

	bEdit = pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle));
	if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
	else                 pfms = pfph->pMemStor;

	if ( (TRUE == bEdit) && (NULL != pfms) )
	{
		sprintf( str, "%i", FJLABEL_NAME_SIZE-1 );
		HSSend(handle, "<table border=0><tr><td><b>Label Name = </b><input type=text size=");
		HSSend(handle, str);
		HSSend(handle, " maxlength=");
		HSSend(handle, str);
		HSSend(handle, " name=lbl_id onchange=return IDCheck(this) value=\"");
		na_String(handle, sLblEditName);
		HSSend(handle, "\"></td></tr><tr><td>&nbsp;</td></tr></table>\n");

										// new label?
		if ( 0 == strcmp( "\"\"", sLblEditNameQuote) )
		{
			bNewLabel = TRUE;
			pfl = pCVT->fj_LabelNew();
		}
		else
		{
			bNewLabel = FALSE;
			pfl = pCVT->fj_LabelBuildFromName( pfms, sLblEditNameQuote );
		}

		if ( NULL != pfl )
		{
			HSSend(handle, "<table border=0><tr><td align=left class=fjh3>Box</td><td align=right><b>Width</b></td>");
			HSSend(handle, "<td aligh=left><input type=text onchange=\"return distCheck(this)\" size=5 name=Width value=");
			na_Double(handle, (double)pCVT->fj_FloatUnits( pfl->fBoxWidth, pfsys->lUnits), 2 );
			HSSend(handle, "></td>\n");
			HSSend(handle, "<td align=right><b>Height</b></td>");
			HSSend(handle, "<td align=left><input type=text onchange=\"return distCheck(this)\" size=5 name=Height value=");
			na_Double(handle, (double)pCVT->fj_FloatUnits( pfl->fBoxHeight, pfsys->lUnits), 2 );
			//			HSSend(handle, ">&nbsp;");
			HSSend(handle, "></td><td>");
			na_String(handle, (*(pCVT->pfj_aUnits))[pfsys->lUnits].pLong);
			HSSend(handle, "</td></tr>\n");

			HSSend(handle, "<tr><td>&nbsp;</td></tr>\n");
			HSSend(handle, "<tr>");
			HSSend(handle, "<td align=left class=fjh3>Expiration&nbsp;</td><td align=right><b>Value</b></td>");
			HSSend(handle, "<td aligh=left><input type=text size=5 maxlength=9 onchange=\"return parmCheck(this,0,999999999,'Expiration Value');\"name=expv value=");
			na_Long(handle, pfl->lExpValue);
			HSSend(handle, "></td>\n");

			HSSend(handle, "<td align=right><b>Units</b></td>");
			HSSend(handle, "<td align=left><select size=1 name=expu>");
			HSSend(handle, "<option ");
			if ( FJ_LABEL_EXP_DAYS == pfl->lExpUnits ) HSSend(handle, "SELECTED");
			HSSend(handle, " value=");
			na_Long(handle, FJ_LABEL_EXP_DAYS);
			HSSend(handle, ">Days</option>");
			HSSend(handle, "<option ");
			if ( FJ_LABEL_EXP_WEEKS == pfl->lExpUnits ) HSSend(handle, "SELECTED");
			HSSend(handle, " value=");
			na_Long(handle, FJ_LABEL_EXP_WEEKS);
			HSSend(handle, ">Weeks</option>");
			HSSend(handle, "<option ");
			if ( FJ_LABEL_EXP_MONTHS == pfl->lExpUnits ) HSSend(handle, "SELECTED");
			HSSend(handle, " value=");
			na_Long(handle, FJ_LABEL_EXP_MONTHS);
			HSSend(handle, ">Months</option>");
			HSSend(handle, "<option ");
			if ( FJ_LABEL_EXP_YEARS == pfl->lExpUnits ) HSSend(handle, "SELECTED");
			HSSend(handle, " value=");
			na_Long(handle, FJ_LABEL_EXP_YEARS);
			HSSend(handle, ">Years</option>");
			HSSend(handle, "</select></td>\n");

			HSSend(handle, "<td align=right><b>Round up</b></td>");
			HSSend(handle, "<td align=left><input type=checkbox ");
			if ( TRUE == pfl->bExpRoundUp ) HSSend(handle, "CHECKED");
			HSSend(handle, " name=expr_up></td></tr>");

			HSSend(handle, "<td align=right><b>Round down</b></td>");
			HSSend(handle, "<td align=left><input type=checkbox ");
			if ( TRUE == pfl->bExpRoundDown ) HSSend(handle, "CHECKED");
			HSSend(handle, " name=expr_down></td></tr>");

			HSSend(handle, "<tr><td>&nbsp;</td></tr></table>\n");
			/*	
						HSSend(handle, "<table border=0>");
						HSSend(handle, "<tr><td align=right><b>Printer</b></td>");
						HSSend(handle, "<td align=left><b>Message Name</b></td>");
						HSSend(handle, "</tr>\n");
						for ( i = 0; i < pfsys->lActiveCount;i++ )
						{
			pfph = pfsys->gmMembers[i].pfph;
			if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
			else                 pfms = pfph->pMemStor;
			papMsgOrg = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_MessageID );
			HSSend(handle, "<tr><td>");
			na_String(handle, pfsys->gmMembers[i].strID);
			HSSend(handle, "</td>");

			HSSend(handle, "<td><select size=1 name=lbl" );
			na_Long(handle, i);
			HSSend(handle, ">\n");
			HSSend(handle, "<option>(none)</option>\n");
			papMsg = papMsgOrg;
			while ( NULL != *papMsg )
			{
			strcpy( sMsg, *papMsg );
			pCVT->fj_ParseKeywordsFromStr( sMsg, ',', pParams, 3 );

			HSSend(handle, "<option ");
			pCVT->fj_processParameterStringInput( pParams[1] );
			if ( 0 == strcmp(pParams[1], pfl->fmMessageIDs[i]) ) HSSend(handle, "SELECTED");
			HSSend(handle, ">");
			na_String(handle, pParams[1]);
			HSSend(handle, "</option>\n");
			papMsg++;
			}
			HSSend(handle, "</select>" );
			HSSend(handle, "</td></tr>\n");
			}
			*/
			strcpy( sSelMsg, pfl->fmMessageIDs[0] );
			/*			if ( 0 != strlen(pfl->fmMessageIDs[0]) )
						{
							strcpy( sSelMsg, pfl->fmMessageIDs[0] );
						}
						else
						{
							strcpy( sSelMsg, sLblEditName );
			}*/
			// message Name
			pCVT->fj_processParameterStringOutput( sSelMsgQuote, sSelMsg );
			pCVT->fj_LabelDestroy( pfl );
			//			HSSend(handle, "</table>");
		}
	}
}										/* @! Function: na_LBL_EDIT_TABLE  }} */
/* @! Function: na_button_submit  {{ */
void na_button_submit(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;

	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle)) )
	{
		HSSend (handle, "<p align=\"center\"><button class=fjbtn1 onClick='submit1(");
		if ( 0 != strlen( sLblEditName ))
			HSSend (handle, "\"update\",0)'>Submit");
		else
			HSSend (handle, "\"create\",0)'>Create");
		HSSend (handle, "</button><button class=fjbtn1 onClick=\"myreset(this.form)\">Reset</button></p>\n");
	}
}										/* @! Function: na_button_submit  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_MSG_EDIT_TABLE  {{ */
void na_MSG_EDIT_TABLE(unsigned long handle)
{
	LPFJ_GLOBAL  pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph   = pCVT->pfph;
	LPFJMEMSTOR   pfms;
	LPFJMESSAGE   pfm;
	LPFJELEMENT   pfe;

	LONG lType;
	LONG lCols;
	BOOL bEdit;
	int  i, j;
	int  jSeg;

	bEdit = pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle));
	if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
	else                 pfms = pfph->pMemStor;

	lSMsgEditSegs = 0;
	if ( NULL != pfms )
	{
		lSMsgEditSegs = 0;
		jSeg = 0;
		lCols = 17;
		pfm = pCVT->fj_MessageBuildFromName( pfms, sSelMsgQuote );
		if ( NULL != pfm )
		{
			lSMsgEditSegs = pfm->lCount;
			jSeg = pfm->lCount;
		}

		HSSend (handle, "<col align=center style=\"width:130px\">");
		HSSend (handle, "<col align=right  style=\"width: 80px\">");
		HSSend (handle, "<col align=left   style=\"width: 60px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 50px\">");
		HSSend (handle, "<col align=left   style=\"width: 80px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=center style=\"width:200px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 50px\">");
		HSSend (handle, "<col align=left   style=\"width: 80px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 60px\">");
		HSSend (handle, "<col align=left   style=\"width: 60px\">");
		HSSend (handle, "<col              style=\"width:  1px\">");
		HSSend (handle, "<col align=right  style=\"width: 60px\">");
		HSSend (handle, "<col align=left   style=\"width: 20px\">\n");
		HSSend (handle, "<tr><input type=hidden name=segments value=");
		na_Long(handle, jSeg);
		HSSend (handle, "><input type=hidden name=msgname value=");
		na_String(handle, sSelMsg);
		HSSend (handle, "></tr>\n");
		/*		HSSend (handle, "><td align=left colspan=");
				na_Long(handle, lCols);
				HSSend (handle, "><b>Message Name = </b><input type=text size=10 name=msg_id onchange=IDCheck(this) value=\"");
				na_String (handle, sMsgEditName);
				HSSend (handle, "\"></td>\</tr>\n");
		*/
		for ( j = 0; j < jSeg; j++ )
		{
			pfe = NULL;
			lType = FJ_TYPE_TEXT;
			pfe = pfm->papfe[j];
			if ( NULL != pfe )
			{
				lType = pfe->pActions->fj_DescGetType();
			}

			HSSend (handle, "<tr><td colspan=");
			na_Long(handle, lCols);
			HSSend (handle, "><hr></td></tr>\n");
			if      ( FJ_TYPE_BARCODE  == lType ) na_ElementBarcode ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_BITMAP   == lType ) na_ElementBitmap  ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_COUNTER  == lType ) na_ElementCounter ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DATETIME == lType ) na_ElementDatetime( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DYNIMAGE == lType ) na_ElementDynimage( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_TEXT     == lType ) na_ElementText    ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DYNTEXT  == lType ) na_ElementDynText ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DYNBARCODE  == lType ) na_ElementDynBarcode ( handle, pfms, j, pfe, bEdit, lCols );
			else if ( FJ_TYPE_DATAMATRIX  == lType ) na_ElementDataMatrix ( handle, pfms, j, pfe, bEdit, lCols );
		}
		HSSend (handle, "<tr><td colspan=");
		na_Long(handle, lCols);
		HSSend (handle, "><hr></td></tr>\n");
		pCVT->fj_MessageDestroy( pfm );
	}
}										/* @! Function: na_MSG_EDIT_TABLE  }} */
/* @! Function: na_AddLoc  {{ */
void na_AddLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lSMsgEditSegs+1); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_AddLoc  }} */
/* @! Function: na_CopyFromLoc  {{ */
void na_CopyFromLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lSMsgEditSegs); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_CopyFromLoc  }} */
/* @! Function: na_CopyToLoc  {{ */
void na_CopyToLoc(unsigned long handle)
{
	/* add your implementation here: */
	int i;

	for ( i = 0; i < (lSMsgEditSegs+1); i++ )
	{
		HSSend(handle, "<option>");
		na_Long(handle, i+1);
		HSSend(handle, "</option>\n");
	}
}										/* @! Function: na_CopyToLoc  }} */
/* @! Function: na_button_addcopy  {{ */
void na_button_addcopy(unsigned long handle)
{
	if ( 0 != strlen( sLblEditName ))
	{
		if ( TRUE == pCVT->fj_PrintHeadIsEdit(pCVT->pfph, HSRemoteAddress(handle)) )
		{
			HSSend (handle, "<p align=left>\n");
			HSSend (handle, "<table border=0 cellpadding=3>\n");
			HSSend (handle, "<tr align=center>\n");
			HSSend (handle, "<td><button class=fjbtn1 onClick='submit1(\"elemadd\",0)'>&nbsp;Add Element&nbsp;</button></td>\n");
			HSSend (handle, "<td>Type\n");
			HSSend (handle, "<select size=1 name=addtype>\n");
			HSSend (handle, " <option value=Text SELECTED>Text</option>\n");
			HSSend (handle, " <option value=Barcode>Barcode</option>\n");
			HSSend (handle, " <option value=Counter>Counter</option>\n");
			HSSend (handle, " <option value=Datetime>Date&nbsp;Time</option>\n");
			HSSend (handle, " <option value=Bitmap>Image</option>\n");
			HSSend (handle, " <option value=Dynimage>Dynamic Image</option>\n");
			HSSend (handle, " <option value=Dyntext>Dynamic Text</option>\n");
			HSSend (handle, " <option value=Dynbarcode>Dynamic Barcode</option>\n");
			HSSend (handle, " <option value=DataMatrix>DataMatrix</option>\n");
			HSSend (handle, "</select>\n");
			HSSend (handle, "</td>\n");
			HSSend (handle, "<td>At Location\n");
			HSSend (handle, "<select size=1 name=addloc>");
			na_AddLoc (handle);
			HSSend (handle, "\n");
			HSSend (handle, "</select>\n");
			HSSend (handle, "</td>\n");
			HSSend (handle, "</tr>\n");

			if ( lSMsgEditSegs > 0 )
			{
				HSSend (handle, "      <tr align=center>\n");
				HSSend (handle, "        <td><button class=fjbtn1 onClick='submit1(\"elemcopy\",0)'>Copy Element</button></td>\n");
				HSSend (handle, "        <td>From Location\n");
				HSSend (handle, "          <select size=1 name=copyfrom>\n");
				HSSend (handle, "            ");
				na_CopyFromLoc (handle);
				HSSend (handle, "\n");
				HSSend (handle, "          </select>\n");
				HSSend (handle, "        </td>\n");
				HSSend (handle, "        <td>To Location\n");
				HSSend (handle, "          <select size=1 name=copyto>\n");
				HSSend (handle, "            ");
				na_CopyToLoc (handle);
				HSSend (handle, "\n");
				HSSend (handle, "          </select>\n");
				HSSend (handle, "        </td>\n");
				HSSend (handle, "      </tr>\n");
			}
			HSSend (handle, "    </table>\n");
			HSSend (handle, "    </p>\n");
		}
	}
}										/* @! Function: na_button_addcopy  }} */
/* @! Function: na_LBL_JS_VAR  {{ */
void na_LBL_JS_VAR(unsigned long handle)
{
	LPFJSYSTEM pfsys  = fj_CVT.pfsys;
	double MaxDist = 0;

	HSSend (handle, "var MaxDist=");

	if( pfsys->lUnits == FJSYS_UNITS_ENGLISH_FEET )   MaxDist = 10;
	if( pfsys->lUnits == FJSYS_UNITS_ENGLISH_INCHES ) MaxDist = 10*12;
	if( pfsys->lUnits == FJSYS_UNITS_METRIC_METERS )  MaxDist = (10*12*2.54)/100;
	if( pfsys->lUnits == FJSYS_UNITS_METRIC_CM )      MaxDist = 10*12*2.54;
	if( pfsys->lUnits == FJSYS_UNITS_METRIC_MM )      MaxDist = 10*12*2.54*10;

	na_Double(handle, MaxDist);
	HSSend (handle, ";\n");
}										/* @! Function: na_LBL_JS_VAR  }} */
/* @! End of dynamic functions   */
