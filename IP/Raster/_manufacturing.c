/* C file: c:\MyPrograms\FoxJet1\_manufacturing.c created
 by NET+Works HTML to C Utility on Monday, June 05, 2000 13:03:45 */
#include "fj.h"
#include "fj_label.h"
#include "fj_system.h"

extern const STRING nbsp[];

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
void na_Long(unsigned long handle, long l);
void na_Double(unsigned long handle, double dInput, int iPlaces);
void fj_reload_page(unsigned long handle, char *pName);
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
/*  @! File: na_SN */
void na_SN (unsigned long handle);
/*  @! File: na_DManu */
void na_DManu (unsigned long handle);
/*  @! File: na_ID */
void na_ID (unsigned long handle);
/*  @! File: na_IPA */
void na_IPA (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_OPvt */
static void na_OPvt (unsigned long handle);
/*  @! File: na_OPvs */
static void na_OPvs (unsigned long handle);
/*  @! File: na_OPc1 */
static void na_OPc1 (unsigned long handle);
/*  @! File: na_OPc2 */
static void na_OPc2 (unsigned long handle);
/*  @! File: na_OPc3 */
static void na_OPc3 (unsigned long handle);
/*  @! File: na_OPtt */
static void na_OPtt (unsigned long handle);
/*  @! File: na_OPt1 */
static void na_OPt1 (unsigned long handle);
/*  @! File: na_OPin */
static void na_OPin (unsigned long handle);
/*  @! File: na_OPic */
static void na_OPic (unsigned long handle);
/*  @! File: na_PHch */
static void na_PHch (unsigned long handle);
/*  @! File: na_PHor */
static void na_PHor (unsigned long handle);
/*  @! File: na_PHcho */
static void na_PHcho (unsigned long handle);
/*  @! File: na_PHha */
static void na_PHha (unsigned long handle);
/*  @! File: na_PHink */
static void na_PHink (unsigned long handle);
/*  @! File: na_PHspc */
static void na_PHspc (unsigned long handle);
/*  @! File: na_PHspo */
static void na_PHspo (unsigned long handle);
/*  @! File: na_PHccs */
static void na_PHccs (unsigned long handle);
/*  @! File: na_PHcca */
static void na_PHcca (unsigned long handle);
/*  @! File: na_OPtc1 */
static void na_OPtc1 (unsigned long handle);
/*  @! File: na_OPtu */
static void na_OPtu (unsigned long handle);
/*  @! File: na_OPtr */
static void na_OPtr (unsigned long handle);
/*  @! File: na_TITLE_MANU */
static void na_TITLE_MANU (unsigned long handle);
/*  @! File: na_PHams */
static void na_PHams (unsigned long handle);
/*  @! File: na_PHamsa2 */
static void na_PHamsa2 (unsigned long handle);
/*  @! File: na_PHamsa4 */
static void na_PHamsa4 (unsigned long handle);
/*  @! File: na_PHamsa5 */
static void na_PHamsa5 (unsigned long handle);
/*  @! File: na_PHamsa1 */
static void na_PHamsa1 (unsigned long handle);
/*  @! File: na_PHamsa6 */
static void na_PHamsa6 (unsigned long handle);
/*  @! File: na_PHamsa3 */
static void na_PHamsa3 (unsigned long handle);
/*  @! File: na_PHamsa7 */
static void na_PHamsa7 (unsigned long handle);
/*  @! File: na_OPhp */
static void na_OPhp (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\manufacturing.htm {{ */
void Send_function_16(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"expires\" content=\"0\">\n");
	HSSend (handle, "<meta http-equiv=\"pragma\" content=\"no-cache\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_MANU (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Manufacturing\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<form action=manu_3 method=POST>\n");
	HSSend (handle, "    <table width=100% border=1 rules=none>\n");
	HSSend (handle, "      <tr><td><input type=text name=volt_table size=8 value=");
	na_OPvt (handle);
	HSSend (handle, ">Voltage Table</td><td colspan=2><input type=text name=volt_set size=8 value=");
	na_OPvs (handle);
	HSSend (handle, ">Voltage Setting (override setting from any Voltage Sensor)</td><td><input type=checkbox name=force_hp value=ON ");
	na_OPhp (handle);
	HSSend (handle, ">Force HeadPresent</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=curve1 size=8 value=");
	na_OPc1 (handle);
	HSSend (handle, ">Curve 1</td><td><input type=text name=curve2 size=8 value=");
	na_OPc2 (handle);
	HSSend (handle, ">Curve 2</td><td><input type=text name=curve3 size=8 value=");
	na_OPc3 (handle);
	HSSend (handle, ">Curve 3</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=temp_table size=8 value=");
	na_OPtt (handle);
	HSSend (handle, ">Temperature Table</td><td><input type=text name=temp_1 size=8 value=");
	na_OPt1 (handle);
	HSSend (handle, ">Head Temperature C</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td>&nbsp;</td><td><input type=text name=temp_range size=8 value=");
	na_OPtr (handle);
	HSSend (handle, ">Temperature Range C</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=checkbox name=invert_nozz value=ON ");
	na_OPin (handle);
	HSSend (handle, ">Nozzle Invert</td><td><input type=checkbox name=invert_chip value=ON ");
	na_OPic (handle);
	HSSend (handle, ">Chip Invert</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=channels size=8 value=");
	na_PHch (handle);
	HSSend (handle, ">Channels</td><td><input type=text name=orifices size=8 value=");
	na_PHor (handle);
	HSSend (handle, ">Orifices</td><td><input type=text name=ch_offset size=8 value=");
	na_PHcho (handle);
	HSSend (handle, ">Chan. Offset</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=angle size=8 value=");
	na_PHha (handle);
	HSSend (handle, ">Head Angle</td><td><input type=text name=ink size=8 value=");
	na_PHink (handle);
	HSSend (handle, ">Ink picol. per ch</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=span_c size=8 value=");
	na_PHspc (handle);
	HSSend (handle, ">Span - center</td><td><input type=text name=span_o size=8 value=");
	na_PHspo (handle);
	HSSend (handle, ">Span - outer</td><td><input type=text name=cc_s size=8 value=");
	na_PHccs (handle);
	HSSend (handle, ">Ch-to-Ch - straight</td><td><input type=text name=cc_a size=8 value=");
	na_PHcca (handle);
	HSSend (handle, ">Ch-to-Ch - angle</td></tr>\n");
	HSSend (handle, "      <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=checkbox name=ams value=ON ");
	na_PHams (handle);
	HSSend (handle, ">AMS present</td><td><input type=text name=ams_a2 size=8 value=");
	na_PHamsa2 (handle);
	HSSend (handle, ">AMS A2 - time before pulses</td><td><input type=text name=ams_a4 size=8 value=");
	na_PHamsa4 (handle);
	HSSend (handle, ">AMS A4 - time between pulse sets</td><td><input type=text name=ams_a5 size=8 value=");
	na_PHamsa5 (handle);
	HSSend (handle, ">AMS A5 - time after pulses</td></tr>\n");
	HSSend (handle, "      <tr><td><input type=text name=ams_a1 size=8 value=");
	na_PHamsa1 (handle);
	HSSend (handle, ">AMS A1 - pulse sets</td><td><input type=text name=ams_a6 size=8 value=");
	na_PHamsa6 (handle);
	HSSend (handle, ">AMS A6 - pulses per set</td><td><input type=text name=ams_a3 size=8 value=");
	na_PHamsa3 (handle);
	HSSend (handle, ">AMS A3 - time of pulse</td><td><input type=text name=ams_a7 size=8 value=");
	na_PHamsa7 (handle);
	HSSend (handle, ">AMS A7 - time between pulses</td></tr>\n");
	HSSend (handle, "      <tr><td colspan=4><center><input type=hidden name=pwid value=");
	na_PWID (handle);
	HSSend (handle, "><input type=submit value=Submit><input type=reset value=Reset></center></td></tr>\n");
	HSSend (handle, "    </table>\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\manufacturing.htm }} */
/* @! End HTML page   */
void reload_manu(unsigned long handle, BOOL bROMWait)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=manufacturing.htm");
	}
	else
	{
		fj_reload_page(handle, "manufacturing.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_manu_1  {{ */
void Post_manu_1(unsigned long handle)
{
	/* add your implementation here: */
	CLPFJ_GLOBAL    pGlobal   = pCVT->pFJglobal;
	CLPFJ_PARAM     pParamFJ  = pCVT->pFJparam;
	LPFJ_NETOSPARAM pParamNET = pCVT->pNETOSparam;
	char   formField[20];
	BOOL   bROMWait;
	BOOL   bChangedNETOS;
	BOOL   bRet;
	int    retrom;
	int    ret;
	struct tm tm;
	struct tm *pTM;

	bROMWait = FALSE;
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadNETOSRomParams( pParamNET );
	bChangedNETOS = FALSE;
	if ( 0 == retrom  )
	{
		ret = fj_HSGetValue( handle, "serial_number", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			// test validity and set into FJPRINTHEAD
			bRet = pCVT->fj_PrintHeadSetSerial( pCVT->pfph, formField );
			if ( TRUE == bRet )
			{
				// set into NETOS parameters
				pCVT->fj_setSerial( formField );
				bChangedNETOS = TRUE;
			}
		}
	}
	if ( TRUE == bChangedNETOS )
	{
		// write and free ROM lock
		pCVT->fj_WriteNETOSRomParams( pParamNET, TRUE, NULL, 0 );
		bROMWait = TRUE;
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	ret = fj_HSGetValue( handle, "manu_date", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		pTM = pCVT->fj_SystemParseDateTime( formField, &tm );
										// good date?  year has origin of 1900
		if ( (NULL != pTM) && (tm.tm_year > 100) )
		{
			// another thread will update ROM
			memcpy( &(pGlobal->tmNewManu), pTM, sizeof(struct tm) );
		}
	}
	ret = fj_HSGetValue( handle, "inserv_date", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		pTM = pCVT->fj_SystemParseDateTime( formField, &tm );
										// good date?  year has origin of 1900
		if ( (NULL != pTM) && (tm.tm_year > 100) )
		{
			// another thread will update ROM
			memcpy( &(pGlobal->tmNewInservice), pTM, sizeof(struct tm) );
		}
	}
	// checkbox only returns a value if it is CHECKED
	ret = fj_HSGetValue( handle, "inservice_reset", formField, sizeof(formField) );
	if ( ret > 0 )
	{
										// set reset flag
		pGlobal->tmNewInservice.tm_year = 1;
		pCVT->fj_print_head_CountsResetAll();
	}

	reload_manu(handle,bROMWait);
}										/* @! Function: Post_manu_1  }} */
/* @! Function: Post_manu_3  {{ */
void Post_manu_3(unsigned long handle)
{
	/* add your implementation here: */
	CLPFJ_GLOBAL  pGlobal = pCVT->pFJglobal;
	LPFJ_PARAM    pParam  = pCVT->pFJparam;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	BOOL   bROMWait;
	BOOL   bParm;
	int    retrom;
	char   formField[20];
	int    ret;

	bROMWait = FALSE;
	// get ROM structure
	pCVT-> tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	if ( 0 == retrom  )
	{
		ret = fj_HSGetValue( handle, "volt_table", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lVoltageTable = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "volt_set", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			if ( 0xa0 == formField[0] ) formField[0] = ' ';
			pParam->FJPhOp.lVoltage = atoi(formField);
		}
		else
		{
			pParam->FJPhOp.lVoltage = 0;
		}
		// checkbox only returns a value if it is CHECKED
		bParm = FALSE;
		ret = fj_HSGetValue( handle, "force_hp", formField, sizeof(formField) );
		if ( 0 < ret ) bParm = TRUE;
		pParam->FJPhOp.bForceHeadPresent = bParm;
		ret = fj_HSGetValue( handle, "curve1", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.fCurve1 = atof(formField);
		}
		ret = fj_HSGetValue( handle, "curve2", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.fCurve2 = atof(formField);
		}
		ret = fj_HSGetValue( handle, "curve3", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.fCurve3 = atof(formField);
		}
		ret = fj_HSGetValue( handle, "temp_table", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lTempTable = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "temp_1", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lTemp1 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "temp_range", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lTempRange = atoi(formField);
		}
		// checkbox only returns a value if it is CHECKED
		bParm = FALSE;
		ret = fj_HSGetValue( handle, "invert_nozz", formField, sizeof(formField) );
		if ( 0 < ret ) bParm = TRUE;
		pParam->FJPhOp.bHeadInvert = bParm;
		// checkbox only returns a value if it is CHECKED
		bParm = FALSE;
		ret = fj_HSGetValue( handle, "invert_chip", formField, sizeof(formField) );
		if ( 0 < ret ) bParm = TRUE;
		pParam->FJPhOp.bChipInvert = bParm;
		ret = fj_HSGetValue( handle, "channels", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.lChannels = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "orifices", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.lOrifices = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ch_offset", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fOffsetEvenOdd = atof(formField);
		}
		ret = fj_HSGetValue( handle, "angle", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fAngle = atof(formField);
		}
		ret = fj_HSGetValue( handle, "ink", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.lInkDrop = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "span_c", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fSpanCenter = atof(formField);
		}
		ret = fj_HSGetValue( handle, "span_o", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fSpanOuter = atof(formField);
		}
		ret = fj_HSGetValue( handle, "cc_s", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fChChStraight = atof(formField);
		}
		ret = fj_HSGetValue( handle, "cc_a", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhPhy.fChChAngle = atof(formField);
		}
		// checkbox only returns a value if it is CHECKED
		bParm = FALSE;
		ret = fj_HSGetValue( handle, "ams", formField, sizeof(formField) );
		if ( 0 < ret ) bParm = TRUE;
		pParam->FJPhOp.bAMS = bParm;
		ret = fj_HSGetValue( handle, "ams_a1", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA1 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a2", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA2 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a3", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA3 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a4", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA4 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a5", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA5 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a6", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA6 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "ams_a7", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lAMSA7 = atoi(formField);
		}

										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		bROMWait = TRUE;
		retrom = 0;
		if ( 0 == retrom )
		{
			memcpy( pfph->pfop,  &(pParam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
			memcpy( pfph->pfphy, &(pParam->FJPhPhy), sizeof(FJPRINTHEAD_PHY) );
			memcpy( pfph->pfpkg, &(pParam->FJPhPkg), sizeof(FJPRINTHEAD_PKG) );
			pCVT->fj_print_head_setPWMVoltageValue();
			pCVT->fj_PrintHeadReset( pfph );
			pCVT->fj_print_head_KillENI();
			pCVT->fj_print_head_SetGAValues();
			pCVT->fj_print_head_InitENI();
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			pCVT->fj_SystemBuildSelectedLabel( pfsys, pBRAM->strLabelName );
		}
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	reload_manu(handle,bROMWait);
}
/* @! Function: Post_manu_4  {{ */
void Post_manu_4(unsigned long handle)
{
	/* add your implementation here: */
	CLPFJ_GLOBAL  pGlobal = pCVT->pFJglobal;
	LPFJ_PARAM    pParam  = pCVT->pFJparam;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	BOOL   bROMWait;
	int    retrom;
	char   formField[20];
	int    ret;

	bROMWait = FALSE;
	// get NV structure
	pCVT->tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
	retrom = pCVT->fj_ReadFJRomParams( pParam );

	if ( 0 == retrom  )
	{
		ret = fj_HSGetValue( handle, "cal_1", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lCal1 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "cal_2", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lCal2 = atoi(formField);
		}
		ret = fj_HSGetValue( handle, "cal_3", formField, sizeof(formField) );
		if ( 0 < ret )
		{
			pParam->FJPhOp.lCal3 = atoi(formField);
		}
										// write and free ROM lock
		pCVT->fj_WriteFJRomParams( pParam, TRUE, NULL, 0 );
		bROMWait = TRUE;
		retrom = 0;
		if ( 0 == retrom )
		{
			memcpy( pfph->pfop, &(pParam->FJPhOp ), sizeof(FJPRINTHEAD_OP ) );
			pCVT->fj_PrintHeadReset( pfph );
			pCVT->fj_print_head_KillENI();
			pCVT->fj_print_head_SetGAValues();
			pCVT->fj_print_head_InitENI();
		}
	}
	else
	{
		pCVT->tx_semaphore_put( &(pGlobal->semaROMLock) );
	}

	reload_manu(handle,bROMWait);
}										/* @! Function: Post_manu_4  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_ID  {{ */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_IPA  {{ */
/* @! Function: na_SN  {{ */
/* @! Function: na_DManu  {{ */
void na_DManu(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	char   str[16];

	if ( 0 != pfph->tmDateManu.tm_year )
	{
		pCVT->fj_SystemGetDateString( str, &(pfph->tmDateManu) );
		HSSend (handle, str);
	}
	else
	{
		HSSend (handle, nbsp);
	}
}										/* @! Function: na_DManu  }} */
/* @! Function: na_OPvt  {{ */
void na_OPvt(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_PARAM    pParam  = pCVT->pFJparam;

	na_Long(handle, (long)(pParam->FJPhOp.lVoltageTable));
}										/* @! Function: na_OPvt  }} */
/* @! Function: na_OPvs  {{ */
void na_OPvs(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_PARAM    pParam  = pCVT->pFJparam;

	if ( 0 != pParam->FJPhOp.lVoltage ) na_Long(handle, (long)(pParam->FJPhOp.lVoltage));
	else                                HSSend (handle, nbsp);
}										/* @! Function: na_OPvs  }} */
/* @! Function: na_OPc1  {{ */
void na_OPc1(unsigned long handle)
{
	/* add your implementation here: */
	na_Double (handle, pCVT->pFJparam->FJPhOp.fCurve1, 9 );
}										/* @! Function: na_OPc1  }} */
/* @! Function: na_OPc2  {{ */
void na_OPc2(unsigned long handle)
{
	/* add your implementation here: */
	na_Double (handle, pCVT->pFJparam->FJPhOp.fCurve2, 9 );
}										/* @! Function: na_OPc2  }} */
/* @! Function: na_OPc3  {{ */
void na_OPc3(unsigned long handle)
{
	/* add your implementation here: */
	na_Double (handle, pCVT->pFJparam->FJPhOp.fCurve3, 9 );
}										/* @! Function: na_OPc3  }} */
/* @! Function: na_OPtt  {{ */
void na_OPtt(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lTempTable));
}										/* @! Function: na_OPtt  }} */
/* @! Function: na_OPt1  {{ */
void na_OPt1(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lTemp1));
}										/* @! Function: na_OPt1  }} */
/* @! Function: na_OPin  {{ */
void na_OPin(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pFJparam->FJPhOp.bHeadInvert ) HSSend (handle, "CHECKED");
}										/* @! Function: na_OPin  }} */
/* @! Function: na_OPic  {{ */
void na_OPic(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pFJparam->FJPhOp.bChipInvert ) HSSend (handle, "CHECKED");
}										/* @! Function: na_OPic  }} */
/* @! Function: na_PHch  {{ */
void na_PHch(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhPhy.lChannels));
}										/* @! Function: na_PHch  }} */
/* @! Function: na_PHor  {{ */
void na_PHor(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhPhy.lOrifices));
}										/* @! Function: na_PHor  }} */
/* @! Function: na_PHcho  {{ */
void na_PHcho(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pFJparam->FJPhPhy.fOffsetEvenOdd, 9);
}										/* @! Function: na_PHcho  }} */
/* @! Function: na_PHha  {{ */
void na_PHha(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pFJparam->FJPhPhy.fAngle, 9 );
}										/* @! Function: na_PHha  }} */
/* @! Function: na_PHink  {{ */
void na_PHink(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhPhy.lInkDrop));
}										/* @! Function: na_PHink  }} */
/* @! Function: na_PHspc  {{ */
void na_PHspc(unsigned long handle)
{
	/* add your implementation here: */
	na_Double (handle, pCVT->pFJparam->FJPhPhy.fSpanCenter, 9 );
}										/* @! Function: na_PHspc  }} */
/* @! Function: na_PHspo  {{ */
void na_PHspo(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pFJparam->FJPhPhy.fSpanOuter, 9 );
}										/* @! Function: na_PHspo  }} */
/* @! Function: na_PHccs  {{ */
void na_PHccs(unsigned long handle)
{
	/* add your implementation here: */
	na_Double(handle, pCVT->pFJparam->FJPhPhy.fChChStraight, 9 );
}										/* @! Function: na_PHccs  }} */
/* @! Function: na_PHcca  {{ */
void na_PHcca(unsigned long handle)
{
	/* add your implementation here: */
	na_Double (handle, pCVT->pFJparam->FJPhPhy.fChChAngle, 9 );
}										/* @! Function: na_PHcca  }} */
/* @! Function: na_OPtc1  {{ */
void na_OPtc1(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lCal1));
}										/* @! Function: na_OPtc1  }} */
/* @! Function: na_OPtu  {{ */
void na_OPtu(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_PARAM    pParam  = pCVT->pFJparam;
	LPCTEMPINFO   *paTempInfo;
	LPCTEMPINFO   pTempInfo;
	WORD16 (*pawTempTable)[];			// pointer to array of A/D values
	LONG   lTableNumber;
	LONG   lTempTarget;
	LONG   lDelta;
	char   str[16];

	lTableNumber = pParam->FJPhOp.lTempTable;
	paTempInfo = pCVT->paTempInfo;
	pTempInfo = paTempInfo[lTableNumber];
	pawTempTable = pTempInfo->pawTempTable;
	lTempTarget = pParam->FJPhOp.lTemp1 - pTempInfo->wFirstTemp;
	lDelta = (*pawTempTable)[lTempTarget] - (*pawTempTable)[lTempTarget+1];

	sprintf( str, "%i", (int)(lDelta));
	HSSend (handle, str);
}										/* @! Function: na_OPtu  }} */
/* @! Function: na_OPtr  {{ */
void na_OPtr(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lTempRange));
}										/* @! Function: na_OPtr  }} */
/* @! Function: na_TITLE_MANU  {{ */

void na_TITLE_MANU(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Manufacturing" );
}										/* @! Function: na_TITLE_MANU  }} */
/* @! Function: na_PHams  {{ */
void na_PHams(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pFJparam->FJPhOp.bAMS ) HSSend (handle, "CHECKED");
}										/* @! Function: na_PHams  }} */
/* @! Function: na_PHamsa2  {{ */
void na_PHamsa2(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA2));
}										/* @! Function: na_PHamsa2  }} */
/* @! Function: na_PHamsa4  {{ */
void na_PHamsa4(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA4));
}										/* @! Function: na_PHamsa4  }} */
/* @! Function: na_PHamsa5  {{ */
void na_PHamsa5(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA5));
}										/* @! Function: na_PHamsa5  }} */
/* @! Function: na_PHamsa1  {{ */
void na_PHamsa1(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA1));
}										/* @! Function: na_PHamsa1  }} */
/* @! Function: na_PHamsa6  {{ */
void na_PHamsa6(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA6));
}										/* @! Function: na_PHamsa6  }} */
/* @! Function: na_PHamsa3  {{ */
void na_PHamsa3(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA3));
}										/* @! Function: na_PHamsa3  }} */
/* @! Function: na_PHamsa7  {{ */
void na_PHamsa7(unsigned long handle)
{
	/* add your implementation here: */
	na_Long(handle, (long)(pCVT->pFJparam->FJPhOp.lAMSA7));
}										/* @! Function: na_PHamsa7  }} */
/* @! Function: na_OPhp  {{ */
void na_OPhp(unsigned long handle)
{
	/* add your implementation here: */
	if ( TRUE == pCVT->pFJparam->FJPhOp.bForceHeadPresent ) HSSend (handle, "CHECKED");
}										/* @! Function: na_OPhp  }} */
/* @! End of dynamic functions   */
