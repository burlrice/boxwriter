#ifndef _WINDOWS // Burl 1.2009
	#include <appconf.h>					// for DEFINEd application options
	#include <narmsrln.h>					// for nvparams & dialog port output
	#include <netosIo.h>					// for OPEN options
	#include <netos_serl.h>					// for serial port options
	#include <serParam.h>					// for serial port options

	#include "fj.h"
#else
	#include <windows.h> // Burl 1.2009
#endif

//#include "..\IP_BSP_v01\devicenetos_serl.h"		// for serial port options
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_element.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_message.h"

static const STRING    fj_sVersionNumber[] = "3.0003";
static const STRING    fj_sVersionDate[]   = "4/8/2008";

const STRING * fj_getVersionNumber(VOID)
{
	return( fj_sVersionNumber );
}
const STRING * fj_getVersionDate(VOID)
{
	return( fj_sVersionDate );
}

#ifndef _WINDOWS  // Burl 1.2009
// build NETBIOS name
VOID fj_setGroupID( VOID )
{
	fj_groupNETBIOS();					// build netbios-format name
	return;
}
// id is set from short name + serial number
// build NETBIOS name
VOID fj_setID(VOID)
{
	fj_PrintHeadSetID( fj_CVT.pfph );
	fj_nameNETBIOS();					// build netbios-format name
	return;
}
// serial number must be greater than FJ_SYS_SERIAL_MIN and less than FJ_SYS_SERIAL_MAX.
// or, for testing, serial can be less than FJ_SYS_SERIAL_TEST if previous serial is less than FJ_SYS_SERIAL_TEST.
BOOL fj_setSerial( LPSTR pInput )
{
	LPFJ_NETOSPARAM pParamNET = fj_CVT.pNETOSparam;
	int  iLen;
	LONG lInput;
	LONG lSerial;
	BOOL bRet = FALSE;

	iLen = strlen( pInput );
	if ( (0 < iLen) && (iLen <= FJPH_SERIAL_SIZE ) )
	{
		fj_FJtoNETSerial( pInput, pParamNET->serialNumber );
		fj_setID();
		bRet = TRUE;
	}
	return( bRet );
}
// this can only come from a password page.
// no need to check if previously set.
BOOL fj_setDateManu( struct tm *pTM )
{
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal;
	CLPFJ_BRAM    pBRAM   = fj_CVT.pBRAM;
	BOOL  bRet = FALSE;

	if ( pTM->tm_year > 100 )			// good date?  year has origin of 1900
	{
		memcpy( &(fj_CVT.pfph->tmDateManu),          pTM, sizeof(struct tm) );
		memcpy( &(fj_CVT.pFJparam->FJPh.tmDateManu), pTM, sizeof(struct tm) );
		bRet = TRUE;

		memcpy( &(pBRAM->tmLastPrint),     pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmCustomerReset), pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmInkFull),       pTM, sizeof(struct tm) );
	}
	return( bRet );
}
// this can only come from a password page.
// no need to check if previously set.
BOOL fj_setDateInservice( struct tm *pTM )
{
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal;
	CLPFJ_BRAM    pBRAM   = fj_CVT.pBRAM;
	BOOL  bRet = FALSE;

										// good date?  year has origin of 1900
	if ( (pTM->tm_year > 100) || (0 == pTM->tm_year) )
	{
		memcpy( &(fj_CVT.pfph->tmDateInservice),          pTM, sizeof(struct tm) );
		memcpy( &(fj_CVT.pFJparam->FJPh.tmDateInservice), pTM, sizeof(struct tm) );
		bRet = TRUE;

		memcpy( &(pBRAM->tmLastPrint),     pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmCustomerReset), pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmInkFull),       pTM, sizeof(struct tm) );
	}
	return( bRet );
}
// prototyped here to avoid errors on STRICT compile
int open(const char *, int);
int close(int);
int write(int, const void *, unsigned int);
int ioctl(int, unsigned long, unsigned long*);

extern int errno;
int fj_fnoSerial = -1;

int fj_openSerialPort( LPFJPRINTHEAD pfph )
{
	devBoardParamsType nvParams;
	unsigned long lGet;
	unsigned long lSet;
	int iRet;

	NAReadDevBoardParams( &nvParams );
	/* open device */
	fj_fnoSerial = open( "/com/0", O_RDWR | O_NONBLOCK );
	if ( fj_fnoSerial < 0 )  fj_fnoSerial = -1;
	else
	{
		/* set baudrate */
		iRet = ioctl( fj_fnoSerial, SIO_BAUD_SET, &nvParams.baudrate );
		iRet = ioctl( fj_fnoSerial, SIO_HW_OPTS_GET, &lGet );
		lSet = SIO_EIGHT_BIT_DATA_WIDTH | SIO_NO_PARITY | SIO_ONE_STOP_BIT;
		if ( NULL != pfph )
		{
			if      ( TRUE == pfph->bSerialScannerLabel ) lSet |= SIO_NO_HANDSHAKING;
			else if ( TRUE == pfph->bSerialInfo         ) lSet |= SIO_SW_HANDSHAKING;
			else if ( TRUE == pfph->bSerialVarData      ) lSet |= SIO_NO_HANDSHAKING;
			else if ( TRUE == pfph->bSerialDynMem       ) lSet |= SIO_NO_HANDSHAKING;
		}
		else lSet |= SIO_SW_HANDSHAKING;
		iRet = ioctl( fj_fnoSerial, SIO_HW_OPTS_GET, &lSet );
	}
	return fj_fnoSerial;
}
int fj_closeSerialPort( void )
{
	int iRet;
	/* close device */
	iRet = close( fj_fnoSerial );
	return iRet;
}
#define SERIALBUFFERLEN 512
static CHAR  fj_serialPortReadBuffer[SERIALBUFFERLEN];
static LONG  fj_serialReadCount = 0;

int fj_checkSerialPort( void )
{
    CLPFJSYSTEM     pfsys = fj_CVT.pfsys;
    LPFJPRINTHEAD   pfph  = fj_CVT.pfph;
    LPFJLABEL       pfl;
    char c;
    const char cSOP = 17;
    int  i;
    int  iRet;

    iRet = 0;

    if ((pfph->bSerialScannerLabel || pfph->bSerialVarData || pfph->bSerialDynMem) && (fj_CVT.pFJglobal->bInitComplete))
    {
	iRet = read( fj_fnoSerial, fj_serialPortReadBuffer+fj_serialReadCount, SERIALBUFFERLEN -fj_serialReadCount );

	if (iRet > 0)
	    fj_serialReadCount += iRet;
	
	for ( i = 0; i < iRet; i++ )
	{
	    c = fj_serialPortReadBuffer[fj_serialReadCount+i-iRet];
	    if ( (0 == c) || (0x0A == c) || (0x0D == c) )
	    {
		fj_serialPortReadBuffer[fj_serialReadCount+i-iRet] = 0;

		if (TRUE == pfph->bSerialScannerLabel)
		{
		    if (0 != strcmp(pCVT->pBRAM->strLabelName, fj_serialPortReadBuffer))
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );

		    pfl = fj_SystemBuildSelectedLabel( pfsys, fj_serialPortReadBuffer );

		    if (pfl == NULL) {
			if (pfsys->pflScanner != NULL) {
			    pfl = fj_SystemCopyScannerDefaultLabelToSelectedLabel( pfsys );
			}
		    }
		    else {
			// save selected label name
			strcpy( fj_CVT.pBRAM->strLabelName, pfl->strName );
		    }
		    
		    iRet = 0;
		    fj_serialReadCount = 0;
		    break;
		}
		else if (TRUE == pfph->bSerialVarData)
		{
		    // save data to BRAM
		    strncpy( fj_CVT.pBRAM->DynamicSegments[pfph->lSerialVarDataID].strDynData, fj_serialPortReadBuffer, FJTEXT_SIZE);
		    fj_CVT.pBRAM->DynamicSegments[pfph->lSerialVarDataID].bChanged = TRUE;
		    iRet = 0;
		    fj_serialReadCount = 0;
		    break;
		}
		else if (TRUE == pfph->bSerialDynMem)
		{
		    LONG lIndex = 0;
		    CHAR sz [3] = { 0, 0, 0 };

		    sz [0] = fj_serialPortReadBuffer [0];
		    sz [1] = fj_serialPortReadBuffer [1];
		    sscanf (sz, "%d", &lIndex);

		    if (lIndex > 0)
			lIndex--;

		    if (strlen (fj_serialPortReadBuffer) > 2) {
			strncpy (fj_CVT.pBRAM->DynamicSegments[lIndex].strDynData, &fj_serialPortReadBuffer [2], FJTEXT_SIZE);
			fj_CVT.pBRAM->DynamicSegments[lIndex].bChanged = TRUE;
		    }
		    else {
			LPFJELEMENT pfe;
			LONG iSeg;
			LPFJMESSAGE pfm = pfph->pfmSelected;

			if (pfm) {
			    for (iSeg = 0; iSeg < pfm->lCount; iSeg++) {
				pfe = pfm->papfe[iSeg];

				if (NULL != pfe) {
				    switch (pfe->pActions->fj_DescGetType ()) {
					case FJ_TYPE_DYNTEXT:
					    {
						LPFJDYNTEXT pfc = (LPFJDYNTEXT)pfe->pDesc;
						pfc->lDynamicID = lIndex;
						fj_ElementPhotocellBuild (pfe);
					    }
					    break;
					case FJ_TYPE_DYNBARCODE:
					    {
						LPFJDYNBARCODE pfc = (LPFJDYNBARCODE)pfe->pDesc;
						pfc->lDynamicID = lIndex;
						fj_ElementPhotocellBuild (pfe);
					    }
					    break;
				    }
				}
			    }
			}

			fj_MessageResetInternal (pfm);
		    }

		    iRet = 0;
		    fj_serialReadCount = 0;
		    break;
		}
	    }
	}

	if( fj_serialReadCount >= (SERIALBUFFERLEN - 1)) 
	    fj_serialReadCount = 0;
    }

    if (/* fj_serialReadCount == 0 && */ pfph->bSOP && (fj_fnoSerial > 0)) {
	while (pfph->lSOP > 0) {
	    pfph->lSOP--;
		/*
		memset (sz, 0, sizeof (sz) / sizeof (sz [0]));
		//sprintf (sz, "sop: %c\r\n", 17);
		sprintf (sz, "%c", 17);
		write (fj_fnoSerial, sz, strlen (sz));
		*/
	    write (fj_fnoSerial, &cSOP, 1);
	}
    }

    return iRet;
}

int fj_onSOP (LPFJPRINTHEAD pfph)
{
	LONG lDirection = 0;

    if (pfph && pfph->bSOP)
		pfph->lSOP++;

	if (pfph && pfph->bBiDirectional) {
		lDirection = pfph->lDirection;

		if (lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT)
			lDirection == FJPH_DIRECTION_RIGHT_TO_LEFT;
		else
			lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT;

		if ((pfph->lBiDirectionalCount % 2) == 0) 
			pfph->fDistancePC = pfph->fDistancePC1;
		else
			pfph->fDistancePC = pfph->fDistancePC2;

		pfph->lBiDirectionalCount++;
	}
}

void fj_writeInfo( char *pIn )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int iRet;
	int ilen;

	if ( TRUE == pfph->bSerialInfo )
	{
		// write to Green Hills debugger window
		//printf(pIn);

		// write to serial port
		if ( fj_fnoSerial > 0 )
		{
			ilen = strlen(pIn);
			if ( '\n' == pIn[0] ) iRet = write( fj_fnoSerial, "\r", 1);
			iRet = write( fj_fnoSerial, pIn, ilen);
			if ( '\n' == pIn[ilen-1] ) iRet = write( fj_fnoSerial, "\r", 1);
		}

		// write to any debugger sockets
		fj_socketSendAllDebug( pIn );
	}

	return;
}

VOID fj_reboot ()
{
//    char sz [256] = { 0 };
//
//    sprintf (sz, "NETA50_1: %d, (*(NARM_GEN)).ssr.bits.rev: %d\r\n", NETA50_1, (*(NARM_GEN)).ssr.bits.rev);
//    fj_writeInfo (sz);
//    
//    if ( NETA50_1 >= (*(NARM_GEN)).ssr.bits.rev ) fj_watchDog = FALSE;
    
    fj_bKeepAlive = FALSE;
    fj_bStrokeWatchdog = FALSE;
    fj_writeInfo ("Rebooting...\r\n");
//    NAReset ();

    (*NCC_GEN).scr.bits.swt = 1;	// watchdog timer interval of approx 1 sec
    (*NCC_GEN).scr.bits.swt = 3;	
    (*NCC_GEN).scr.bits.swri = 2;	// watchdog timer causes a RESET
    (*NCC_GEN).scr.bits.swe = 1;	// enable watchdog timer
}
#endif //_WINDOWS  // Burl 1.2009
