/* C file: C:\MyPrograms\IP_APP_v01\_notauthorized.c created
 by NET+Works HTML to C Utility on Friday, June 28, 2002 11:28:09 */
#include "fj.h"

void na_PWID(unsigned long handle);

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_NA_LINK */
static void na_NA_LINK (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\notauthorized.htm {{ */
void Send_function_31(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<title>Not Authorized</title>\n");
	HSSend (handle, "</head>\n");
	HSSend (handle, "<body>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "<p>&nbsp;</p>\n");
	HSSend (handle, "");
	na_NA_LINK (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</body>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\notauthorized.htm }} */
/* @! End HTML page   */

/* @! Begin of dynamic functions */
/* @! Function: na_NA_LINK  {{ */
void na_NA_LINK(unsigned long handle)
{
	CLPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;

	if ( TRUE != pGlobal->bReset)
	{
		HSSend (handle, "<p><center>Not authorized to use this page.</center></p>\n");
		HSSend (handle, "<p><center>Enter password on <a href=\"");
		HSSend (handle, "printhead.htm?pwid=");
		na_PWID(handle);
		HSSend (handle, "\">main page</a>.</center></p>\n");
	}
	else
	{
		HSSend (handle, "<p><center><b>Please reboot printer now!</b></center></p>\n");
	}
}										/* @! Function: na_NA_LINK  }} */
/* @! End of dynamic functions   */
