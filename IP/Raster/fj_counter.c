#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#include <stdio.h>
#include <time.h>
#endif
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_image.h"
#include "fj_counter.h"
#include "fj_text.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_system.h"

// used for MemStor identification
const  CHAR fj_CounterID[]   = "Counter";

//
//  FUNCTION: fj_CounterNew()
//
//  PURPOSE:  Create a new FJCOUNTER structure
//
FJDLLExport LPFJCOUNTER fj_CounterNew( VOID )
{
	LPFJCOUNTER pfc;

	pfc = (LPFJCOUNTER)fj_calloc( 1, sizeof (FJCOUNTER) );

	pfc->lBoldValue     =  0;			// amount of boldness. 0 origin.
	pfc->lWidthValue    =  1;			// amount of width stretch. 1 origin.
	pfc->lGapValue      =  0;			// amount of gap. 0 = vertical
	pfc->lChange        =  1;			// Value for change of counter
	pfc->lRepeat        =  1;			// Repeating value of counter
	pfc->lStart         =  1;			// Start value of counter
										// Limit value of counter
	pfc->lLimit         =  FJCOUNTER_MAX_VALUE;
	pfc->lOldValue      =  1;			// Previous value of counter
	pfc->lCurValue      =  1;			// Current value of counter
	pfc->lCurRepeat     =  1;			// Current value of repeat of counter
	pfc->lLength        =  0;			// length of string output. 0 = 'natural' ( no alteration )
	pfc->cAlign         = 'R';			// alignment of value in larger length - L/R
	pfc->cFill          =  0;			// fill character. 0 is no fill.
	pfc->strFontName[0] =  0;			// font name
	pfc->pfeText        =  NULL;		// element for text generation
	return( pfc );
}
//
//  FUNCTION: fj_CounterDestroy()
//
//  PURPOSE:  Destroy a FJCOUNTER structure
//
FJDLLExport VOID fj_CounterDestroy( LPFJELEMENT pfe )
{
	LPFJCOUNTER pfc;
	LPFJELEMENT pfeText;

	pfc = (LPFJCOUNTER)pfe->pDesc;
	if (NULL != pfc)
	{
		pfeText = pfc->pfeText;
		if ( NULL != pfeText )
		{
			fj_ElementDestroy( pfeText );
		}
		fj_free( pfc );
		pfe->pDesc = NULL;
	}
	return;
}
//
//  FUNCTION: fj_CounterDup()
//
//  PURPOSE:  Duplicate a FJCOUNTER structure
//
FJDLLExport LPFJCOUNTER fj_CounterDup( CLPCFJCOUNTER pfcIn )
{
	LPFJCOUNTER pfcDup;
	LPFJELEMENT pfeText;

	pfcDup = (LPFJCOUNTER)fj_malloc( sizeof(FJCOUNTER) );
	memcpy( pfcDup, pfcIn, sizeof(FJCOUNTER) );

	pfeText = pfcIn->pfeText;
	if ( NULL != pfeText )
	{
		pfcDup->pfeText = fj_ElementDup( pfeText );
		pfcDup->pfeText->pfm = pfcIn->pfeText->pfm;
	}

	return( pfcDup );
}
//
//  FUNCTION: fj_CounterUpdate()
//
//  PURPOSE:  Update a FJCOUNTER according to its rules
//
FJDLLExport VOID fj_CounterUpdate( CLPFJCOUNTER pfc )
{
	pfc->lOldValue = pfc->lCurValue;	// save previous value for comparison
	if ( pfc->lCurRepeat < pfc->lRepeat)// Check Repeat value
		pfc->lCurRepeat++;				// if need to repeat counter - bump repeat value
	else
	{
		pfc->lCurRepeat = 1;			// 1-st repeat step

		pfc->lCurValue += pfc->lChange;
		if ( 0 < pfc->lChange )			// if increment counter
		{
			if ( pfc->lCurValue > pfc->lLimit ) pfc->lCurValue = pfc->lStart;
		}
		else							// decrement counter
		{
			if ( pfc->lCurValue < pfc->lLimit ) pfc->lCurValue = pfc->lStart;
		}
	}

	return;
}
FJDLLExport VOID fj_CounterCreateImage( CLPFJELEMENT pfe )
{
	LPFJCOUNTER pfc;
	LPFJELEMENT pfeText;
	LPFJTEXT    pft;

	pfc = (LPFJCOUNTER)pfe->pDesc;
	// pre-process
	// get what we need to build FJIMAGE
	{
		LPFJSYSTEM    pfsys;
		STR   strTemp[1+FJTEXT_SIZE];
		LPSTR pTemp;
		LPSTR pOutput;
		int   i;

		pfc = (LPFJCOUNTER)pfe->pDesc;
		pfeText = pfc->pfeText;
		pfsys = pfe->pfm->pfph->pfsys;

		if ( NULL == pfeText )
		{
			pfeText = fj_ElementTextNew();
			pfeText->pfm = pfe->pfm;
			pfc->pfeText = pfeText;
		}
		pft = (LPFJTEXT)pfeText->pDesc;
		pTemp   = strTemp;
		pOutput = pft->strText;

		strcpy( pTemp, "Counter" );
		sprintf( pTemp, "%d", pfc->lCurValue );
		strcpy( pOutput, pTemp );

		if ( 0 != pfc->lLength )
		{
			int strLen;

			strLen = strlen( pTemp );
			if ( strLen > pfc->lLength )
			{
				pTemp += ( strLen - pfc->lLength );
			}
			strLen = strlen( pTemp );
			if ( strLen < pfc->lLength )
			{
				if ( 0 != pfc->cFill )
				{
					for ( i = 0; i < pfc->lLength; i++ )
					{
						pOutput[i] = pfc->cFill;
					}
					pOutput[i] = 0;
					if ( 'R' == pfc->cAlign ) pOutput += (pfc->lLength - strLen);
					for ( i = 0; i < strLen; i++ )
					{
						pOutput[i] = pTemp[i];
					}
				}
			}
			else
			{
				strcpy( pOutput, pTemp );
			}
		}
	}

	// build FJIMAGE
	pfeText = pfc->pfeText;
	pft = (LPFJTEXT)pfeText->pDesc;

	pft->lBoldValue  = pfc->lBoldValue;
	pft->lWidthValue = pfc->lWidthValue;
	pft->lGapValue   = pfc->lGapValue;
	strcpy( pft->strFontName, pfc->strFontName );

	// let text routines create rasters, do bold and width, etc.
	pfeText->pfi = pfe->pfi;
	pfe->pfi = NULL;
	pfeText->pActions->fj_DescCreateImage( pfeText );
	pfe->pfi = pfeText->pfi;
	pfeText->pfi = NULL;

	return;
}
//
//   bump Counter
//   make new rasters if changed
//
//   actually, it is the opposite order than stated above.
//   1. if we bump the counter first, the first value will never be printed.
//   2. if we bump the counter first, the counter will be not show what the next box will get.
//
FJDLLExport VOID fj_CounterPhotocellBuild( CLPFJELEMENT pfe )
{
	CLPFJCOUNTER pfc = (CLPFJCOUNTER)pfe->pDesc;

	if ( pfc->lOldValue != pfc->lCurValue ) fj_CounterCreateImage( pfe );
	fj_CounterUpdate( pfc );
	return;
}
//
//  FUNCTION: fj_CounterToStr()
//
//  PURPOSE:  Convert a FJCOUNTER structure into a descriptive string
//
FJDLLExport VOID fj_CounterToStr( CLPCFJELEMENT pfe, LPSTR pStr )
{
	CLPCFJCOUNTER pfc = (CLPCFJCOUNTER)pfe->pDesc;
	STR sName[FJCOUNTER_NAME_SIZE*2];
	STR sFill[3];
	STR sFillQuote[5];

										// text element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	sFill[0]  = pfc->cFill;
	sFill[1]  = 0;
										// fill character
	fj_processParameterStringOutput( sFillQuote,  sFill  );
	if (NULL != pStr) sprintf (pStr,"{%s,%s,%f,%d,%c,%d,%d,%d,%c,%c,%c,%d,%d,%d,%d,%d,%c,%s,",
			fj_CounterID,
			sName,						// text element Name
			pfe->fLeft,					// distance from left edge of box
			pfe->lRow,					// row number to start pixels - 0 = bottom of print head
			(CHAR)pfe->lPosition,		// type of positioning
			pfc->lBoldValue,			// amount of boldness. 0 origin.
			pfc->lWidthValue,			// amount of width stretch. 1 origin.
			pfc->lGapValue,				// amount of gap. 0 = vertical
										// true for invert
			(pfe->lTransform & FJ_TRANS_INVERSE  ? 'T' : 'F'),
										// true for reverse
		(pfe->lTransform & FJ_TRANS_REVERSE  ? 'T' : 'F'),
										// true for negative
		(pfe->lTransform & FJ_TRANS_NEGATIVE ? 'T' : 'F'),
		pfc->lChange,					// Value for change of counter
		pfc->lRepeat,					// Repeating value of counter
		pfc->lStart,					// Start value of counter
		pfc->lLimit,					// Limit value of counter
		pfc->lLength,					// length of string output. 0 = 'natural' ( no alteration )
		pfc->cAlign,					// alignment of value in larger length - L/R
		sFillQuote						// fill character. 0 is no fill.
		);
	// font name
	fj_processParameterStringOutput( pStr+strlen(pStr), pfc->strFontName );
	strcat( pStr, "}" );
	return;
}
//
//  FUNCTION: fj_CounterFromStr()
//
//  PURPOSE:  Convert a descriptive string into a FJCOUNTER structure
//
FJDLLExport VOID fj_CounterFromStr( LPFJELEMENT pfe, LPCSTR pStr )
{
#define COUNTER_PARAMETERS 19
	LPFJCOUNTER pfc;
	LPSTR  pParams[COUNTER_PARAMETERS];
	LPSTR  pStrM;
	int res;

	pfc = (LPFJCOUNTER)pfe->pDesc;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJCOUNTER_MEMSTOR_SIZE+1 );
	strncpy( pStrM, pStr, FJCOUNTER_MEMSTOR_SIZE );
	*(pStrM+FJCOUNTER_MEMSTOR_SIZE ) = 0;

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, COUNTER_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_CounterID ) ) res = 0;

	if ( res == COUNTER_PARAMETERS )	// FJ_COUNTER string must have 19 fields!!!!!
	{
		pfe->lTransform   =  0;
		fj_processParameterStringInput( pParams[ 1] );
		strncpy( pfe->strName,          pParams[ 1], FJELEMENT_NAME_SIZE ); pfe->strName[FJELEMENT_NAME_SIZE] = 0;
										// distance from left edge of box
		pfe->fLeft        = (FLOAT)atof(pParams[ 2]);
										// row number to start pixels - 0 = bottom of print head
		pfe->lRow         = atol(pParams[ 3]);
										// type of positioning
		pfe->lPosition    =    *(pParams[ 4]);
										// amount of boldness. 0 origin.
		pfc->lBoldValue   = atol(pParams[ 5]);
										// amount of width stretch. 1 origin.
		pfc->lWidthValue  = atol(pParams[ 6]);
										// amount of gap. 0 = vertical
		pfc->lGapValue    = atol(pParams[ 7]);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 8]) == 'T' ? FJ_TRANS_INVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[ 9]) == 'T' ? FJ_TRANS_REVERSE  : 0);
		// true for inverse
		pfe->lTransform  |=   (*(pParams[10]) == 'T' ? FJ_TRANS_NEGATIVE : 0);
										// Value for change of counter
		pfc->lChange      = atol(pParams[11]);
										// Repeating value of counter
		pfc->lRepeat      = atol(pParams[12]);
										// Start value of counter
		pfc->lStart       = atol(pParams[13]);
										// Limit value of counter
		pfc->lLimit       = atol(pParams[14]);
										// length of string output. 0 = 'natural' ( no alteration )
		pfc->lLength      = atol(pParams[15]);
										// alignment of value in larger length - L/R
		pfc->cAlign       =    *(pParams[16]);
		fj_processParameterStringInput( pParams[17] );
										// fill character. 0 is no fill.
		pfc->cFill        =    *(pParams[17]);
		fj_processParameterStringInput( pParams[18] );
		// font name
		strncpy( pfc->strFontName, pParams[18], FJFONT_NAME_SIZE ); pfc->strFontName[FJFONT_NAME_SIZE] = 0;

										// Current value of counter
		pfc->lOldValue    =  pfc->lStart;
										// Current value of counter
		pfc->lCurValue    =  pfc->lStart;
		pfc->lCurRepeat   =  1;			// Current value of repeat of counter

	}

	fj_free( pStrM );
	return;
}
//
//	FUNCTION: fj_CounterBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJCOUNTER from FJMEMSTOR
//
FJDLLExport VOID fj_CounterBuildFromName( CLPCFJMEMSTOR pfms, LPFJELEMENT pfe, LPCSTR pName )
{
	LPSTR    pTextStr;

	pTextStr = (LPSTR)fj_MemStorFindElement( pfms, fj_CounterID, pName );
	if ( NULL != pTextStr )
	{
		fj_CounterFromStr( pfe, pTextStr );
	}

	return;
}
//
//	FUNCTION: fj_CounterAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJCOUNTER element to FJMEMSTOR
//
FJDLLExport BOOL fj_CounterAddToMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	CHAR buff[FJCOUNTER_MEMSTOR_SIZE];

	fj_CounterToStr( pfe, buff );

	return( fj_MemStorAddElementString( pfms, buff ) );
}
//
//	FUNCTION: fj_CounterDeleteFromMemstor
//
//	Return: TRUE if success.
//
//  Delete FJCOUNTER element from FJMEMSTOR
//
FJDLLExport BOOL fj_CounterDeleteFromMemstor( LPFJMEMSTOR pfms, CLPCFJELEMENT pfe )
{
	STR sName[FJCOUNTER_NAME_SIZE*2];

										// Counter element Name
	fj_processParameterStringOutput( sName, pfe->strName );
	return( fj_MemStorDeleteElement( pfms, fj_CounterID, sName ) );
}
//
//	FUNCTION: fj_CounterGetType
//
//	Return: Counter type
//
//
FJDLLExport enum FJELETYPE fj_CounterGetType( VOID )
{

	return( FJ_TYPE_COUNTER );
}
//
//	FUNCTION: fj_CounterGetTypeString
//
//	Return: Pointer to string for Text type
//
//
FJDLLExport LPCSTR fj_CounterGetTypeString( VOID )
{

	return( fj_CounterID );
}
//
//	FUNCTION: fj_CounterGetTextString
//
//	Return: Copy text string into caller's buffer
//
//
FJDLLExport VOID fj_CounterGetTextString( CLPCFJELEMENT pfe, LPSTR pStr, LONG lMaxCount )
{
	LPFJELEMENT pfeText;
	LPFJCOUNTER pfc;

	pfc = (LPFJCOUNTER)pfe->pDesc;

	pfeText = pfc->pfeText;
	if ( NULL != pfeText )
	{
		strncpy( pStr, ((LPFJTEXT)pfeText->pDesc)->strText, lMaxCount );
		*(pStr+lMaxCount-1) = 0;
	}
	else
	{
		sprintf( pStr, "%d", pfc->lCurValue );
	}
	return;
}
FJCELEMACTION fj_CounterActions =
{
	(LPVOID  (*)( VOID ))fj_CounterNew,
	(LPVOID  (*)( LPVOID ))fj_CounterDup,
	fj_CounterDestroy,
	fj_CounterCreateImage,
	fj_CounterPhotocellBuild,
	fj_CounterBuildFromName,
	fj_CounterAddToMemstor,
	fj_CounterDeleteFromMemstor,
	fj_CounterGetTextString,
	fj_CounterGetType,
	fj_CounterGetTypeString
};
