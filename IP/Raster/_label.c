/* C file: c:\MyPrograms\FoxJet1\_label.c created
 by NET+Works HTML to C Utility on Monday, August 20, 2001 11:50:48 */
#include "fj.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_message.h"
#include "fj_label.h"
#include "fj_system.h"

#define _WEB_TO_C_
/* @! Begin Function Prototype Forward Declaration */
/*  @! File: na_HEADER */
void na_HEADER (unsigned long handle);
void na_Double(unsigned long handle, double dInput, int iPlaces);
/*  @! File: na_DEF_LBL_ID */
static void na_DEF_LBL_ID (unsigned long handle);
/*  @! File: na_LBL_CT */
static void na_LBL_CT (unsigned long handle);
/*  @! File: na_LBL_TABLE */
static void na_LBL_TABLE (unsigned long handle);
/*  @! File: na_EDIT_BUTTONS */
void na_EDIT_BUTTONS (unsigned long handle);
/*  @! File: na_TITLE_LABELS */
static void na_TITLE_LABELS (unsigned long handle);
/*  @! File: na_PWID */
void na_PWID (unsigned long handle);
/*  @! File: na_STAvailEdit */
void na_STAvailEdit (unsigned long handle);
/*  @! File: na_DEF_SCAN_LBL_ID */
static void na_DEF_SCAN_LBL_ID (unsigned long handle);
/*  @! File: na_SEL_BUTTONS */
static void na_SEL_BUTTONS (unsigned long handle);
/* @! End Function Prototype Forward Declaration   */

/* @! Begin HTML page */
/*  @! File: C:\MyPrograms\IP_APP_v01\html\label.htm {{ */
void Send_function_14(unsigned long handle)
{
	HSSend (handle, "<html>\n");
	HSSend (handle, "<head>\n");
	HSSend (handle, "<meta http-equiv=\"Content-Language\" content=\"en-us\">\n");
	HSSend (handle, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">\n");
	HSSend (handle, "<title>");
	na_TITLE_LABELS (handle);
	HSSend (handle, "</title>\n");
	HSSend (handle, "<SCRIPT type=\"text/javascript\">\n");
	HSSend (handle, "var h2text=\"Labels\"\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "<SCRIPT LANGUAGE=\"JavaScript\">\n");
	HSSend (handle, "function submit1(sa,si) {\n");
	HSSend (handle, "var of=document.formedit1;\n");
	HSSend (handle, "of.action.value=sa; of.item.value=si;\n");
	HSSend (handle, "of.submit(); }\n");
	HSSend (handle, "function startedit(lblnum) {\n");
	HSSend (handle, "var str = \"label_edit.htm?lbl_edit=\"+lblnum+\"&pwid=");
	na_PWID (handle);
	HSSend (handle, "\";\n");
	HSSend (handle, "var editwidth = 0;\n");
	HSSend (handle, "var editheight = 0;\n");
	HSSend (handle, "if (window.innerWidth) {\n");
	HSSend (handle, "editwidth = window.innerWidth;\n");
	HSSend (handle, "editheight= window.innerHeight;\n");
	HSSend (handle, "} else if (document.body.clientWidth) {\n");
	HSSend (handle, "editwidth = document.body.clientWidth;\n");
	HSSend (handle, "editheight= document.body.clientHeight; }\n");
	HSSend (handle, "window.open(str,\"Editor\",\"toolbar=no,location=no,directories=no,left=0,top=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=\"+editwidth+\", height=\"+editheight);\n");
	HSSend (handle, "}\n");
	HSSend (handle, "</SCRIPT>\n");
	HSSend (handle, "");
	na_HEADER (handle);
	HSSend (handle, "\n");
	HSSend (handle, "<p>\n");
	HSSend (handle, "<table border=\"0\" width=\"99%\">\n");
	HSSend (handle, "<tr>");
	na_DEF_SCAN_LBL_ID (handle);
	HSSend (handle, "<td align=\"left\"><b>Selected Label Name: ");
	na_DEF_LBL_ID (handle);
	HSSend (handle, "</b></td>\n");
	HSSend (handle, "<td align=left><b>Number of Labels: ");
	na_LBL_CT (handle);
	HSSend (handle, "</b></td><td align=right><table cellpadding=\"4\"><tr><td><b>Available Storage: ");
	na_STAvailEdit (handle);
	HSSend (handle, "</td>\n");
	HSSend (handle, "<td align=right></b>");
	na_EDIT_BUTTONS (handle);
	HSSend (handle, "</td></tr></table></td></tr>\n");
	HSSend (handle, "<tr>");
	na_SEL_BUTTONS (handle);
	HSSend (handle, "<td>&nbsp;</td>\n");
	HSSend (handle, "<td>&nbsp;</td></tr></table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "<center>\n");
	HSSend (handle, "<table border=\"1\" cellpadding=\"2\" width=\"95%\" bgcolor=\"#f5f5f5\">\n");
	HSSend (handle, "");
	na_LBL_TABLE (handle);
	HSSend (handle, "\n");
	HSSend (handle, "</table>\n");
	HSSend (handle, "<br>\n");
	HSSend (handle, "</center>\n");
	HSSend (handle, "<form name=\"formedit1\" action=\"label_input\" method=\"POST\">\n");
	HSSend (handle, "<input type=\"hidden\" name=\"pwid\" value=\"");
	na_PWID (handle);
	HSSend (handle, "\"> <input type=\"hidden\" value=\"none\" name=\"action\">\n");
	HSSend (handle, "<input type=\"hidden\" value=\"none\" name=\"item\">\n");
	HSSend (handle, "</form>\n");
	HSSend (handle, "</html>\n");
}										/*  @! File: C:\MyPrograms\IP_APP_v01\html\label.htm }} */
/* @! End HTML page   */

void reload_label(unsigned long handle, BOOL bROMWait, BOOL bScanner)
{
	if ( TRUE == bROMWait )
	{
		fj_reload_page(handle, "romwait.htm?page=label.htm");
	}
	else if ( TRUE == bScanner )
	{
		fj_reload_page(handle, "label.htm?select_scanner=true");
	}
	else
	{
		fj_reload_page(handle, "label.htm");
	}
}
/* @! Begin of post function */
/* @! Function: Post_label_input  {{ */
void Post_label_input(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMESSAGE   pfm;
	char   formField[50];
	STR    sLblName[FJLABEL_NAME_SIZE*2];
	ULONG  ipAdd;
	BOOL   bROMWait;
	BOOL   bScanner;
	BOOL   bRet;
	int    ret,i;

	bROMWait = FALSE;
	bScanner = FALSE;
	ipAdd = HSRemoteAddress(handle);
	ret = fj_HSGetValue( handle, "action", formField, sizeof(formField) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp(formField,"edit_start") )
		{
			pCVT->fj_print_head_EditStart( ipAdd );
		}
		else if ( 0 == strcmp(formField,"edit_save") )
		{
			pCVT->fj_print_head_EditSave( ipAdd );
			bROMWait = TRUE;
		}
		else if ( 0 == strcmp(formField,"edit_cancel") )
		{
			pCVT->fj_print_head_EditCancel( ipAdd );
		}
		else if ( 0 == strcmp(formField,"select") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				if (0 != strcmp(pCVT->pBRAM->strLabelName, formField))
				{
					pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
				}
				pCVT->fj_SystemBuildSelectedLabel( pfsys, formField );
				strcpy( pCVT->pBRAM->strLabelName, formField );
				pCVT->pBRAM->lSelectedCount = 0;
			}
		}
		else if ( 0 == strcmp(formField,"select_scanner") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				if ( 0 == strcmp("-1", formField) ) bScanner = TRUE;
				else
				{
					strcpy( pCVT->pBRAM->strScannerLabelName, formField );
					pCVT->fj_SystemBuildScannerDefaultLabel( pfsys, formField );
					bScanner = FALSE;
				}
			}

		}
		else if ( 0 == strcmp(formField,"delete") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				bRet = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );
				if ( TRUE == bRet )
				{
					// label Name
					pCVT->fj_processParameterStringOutput( sLblName, formField );
					bRet = pCVT->fj_MemStorDeleteElement( pfph->pMemStorEdit, pCVT->pfj_LabelID, sLblName );
					bRet = fj_MessageDeleteFromMemstorByName( pfph->pMemStorEdit, sLblName );
					/*					pfm = pCVT->fj_MessageBuildFromName( pfph->pMemStorEdit, sLblName );
										if ( NULL != pfm )
										{
											bRet = pCVT->fj_MessageDeleteFromMemstor( pfph->pMemStorEdit, pfm );
											pCVT->fj_MessageDestroy( pfm );
										}
					*/
					if ( 0 == strcmp(pCVT->pBRAM->strLabelName, formField))
					{
						pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
						pCVT->fj_SystemBuildSelectedLabel( pfsys, NULL );
						pCVT->pBRAM->strLabelName[0] = 0;
						pCVT->pBRAM->lSelectedCount = 0;
					}
				}
			}
		}
		else if ( 0 == strcmp(formField,"laction") )
		{
			ret = fj_HSGetValue( handle, "item", formField, sizeof(formField) );
			if ( 0 < ret )
			{
				if ( 0 == strcmp(formField,"stop") )
				{
					pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
					pCVT->fj_SystemBuildSelectedLabel( pfsys, NULL );
					pBRAM->strLabelName[0] = 0;
					pCVT->pBRAM->lSelectedCount = 0;
					pBRAM->bPrintIdle = FALSE;
					if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
					{
						pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
						pCVT->fj_print_head_SendPhotocellMode();
					}
				}
				else if ( 0 == strcmp(formField,"start") )
				{
					pBRAM->bPrintIdle = FALSE;
					if ( FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
					{
						pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
						pCVT->fj_print_head_SendPhotocellMode();
					}
				}
				else if ( 0 == strcmp(formField,"idle") )
				{
					pBRAM->bPrintIdle = TRUE;
					if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
					{
						pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
						pCVT->fj_print_head_SendPhotocellMode();
					}
				}
				else if ( 0 == strcmp(formField,"standby") )
				{
					if ( FALSE == pGlobal->bStandByOn )
					{
						pGlobal->bStandByOn = TRUE;
						fj_print_head_ScheduleStandBy();
						if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
						{
							pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
							pCVT->fj_print_head_SendPhotocellMode();
						}
					}
					else
					{
						pGlobal->bStandByOn = FALSE;
						fj_print_head_ScheduleStandBy();
						if ( FALSE == pBRAM->bPrintIdle &&
							0 != pBRAM->strLabelName[0] &&
							FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
						{
							pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
							pCVT->fj_print_head_SendPhotocellMode();
						}
					}
				}
				fj_print_head_SetStatus();
				fj_print_head_SendStatus();

			}
		}
	}
	reload_label(handle,bROMWait,bScanner);
}										/* @! Function: Post_label_input  }} */
/* @! End of post function   */

/* @! Begin of dynamic functions */
/* @! Function: na_HEADER  {{ */
/* @! Function: na_DEF_LBL_ID  {{ */
void na_DEF_LBL_ID(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	if ( 0 != pBRAM->strLabelName[0] )
	{
		na_String(handle, pBRAM->strLabelName);
		//HSSend(handle, "&nbsp;<button onClick='submit1(\"cancelsel\",\"cancel\");'>Cancel selection</button>&nbsp;" );
	}
	else HSSend(handle, "(none)&nbsp;" );
}										/* @! Function: na_DEF_LBL_ID  }} */
/* @! Function: na_LBL_CT  {{ */
void na_LBL_CT(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJMEMSTOR pfms;
	LPSTR *papLabel;
	int i = 0;

	if ( TRUE == pCVT->fj_PrintHeadIsEdit(pfph, HSRemoteAddress(handle)) ){ pfms = pfph->pMemStorEdit; }
	else                                                                                        { pfms = pfph->pMemStor;     }
	papLabel = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_LabelID );
	while ( NULL != *(papLabel+i) ) i++;
	na_Long(handle, i);
}										/* @! Function: na_LBL_CT  }} */
/* @! Function: na_LBL_TABLE  {{ */
void na_LBL_TABLE(unsigned long handle)
{
	/* add your implementation here: */
	LPFJ_GLOBAL   pGlobal = pCVT->pFJglobal;
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJSYSTEM    pfsys   = pCVT->pfsys;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJMEMSTOR pfms;
	LPFJLABEL   pfl;
	LPFJMESSAGE pfm;
	LPSTR      *papLabel;
	STR         sMsgName[FJMESSAGE_NAME_SIZE*2];
	CHAR        str[100];
	LPSTR  pLabelName;
	BOOL   bScanner;
	BOOL   bEdit;
	ULONG  ipAdd;
	LONG   lLevel;
	int    ret;
	int    i, j;
	int    jSeg;

	ipAdd = HSRemoteAddress(handle);
	lLevel = fj_get_level(handle);
	bEdit = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );

	bScanner = FALSE;
	ret = fj_HSGetValue( handle, "select_scanner", str, sizeof(str) );
	if ( 0 < ret )
	{
		if ( 0 == strcmp("true", str) ) bScanner = TRUE;
	}

	HSSend(handle, "<thead><tr>");
	if ( TRUE == bEdit )
	{
		pfms = pfph->pMemStorEdit;
		HSSend(handle, "<td rowspan=2 colspan=2 align=center><button class=fjbtn1 onClick=\"startedit('-1')\">Create New Label</button></td>");
	}
	else
	{
		pfms = pfph->pMemStor;
		HSSend(handle, "<td rowspan=2>&nbsp;</td>");
	}
	HSSend(handle, "<td rowspan=2 align=center><b>Label Name</b></td>");
	for ( j = 0 ; ((j<pfsys->lActiveCount) && (j<3)); j++ )
	{
		HSSend(handle, "<td rowspan=2 align=center><b>Message ");
		if ( pfsys->lActiveCount > 1 ) na_Long(handle, (j+1));
		HSSend(handle, "</b><small>(Name/Data)</small></td>");
	}
	HSSend(handle, "<td colspan=2 align=center><b>Box</b><br>");
	HSSend(handle, "<td colspan=3 align=center><b>Expiration</b><br>");
	HSSend(handle, "</tr>\n");

	HSSend(handle, "<tr>");
	HSSend(handle, "<td align=center><b>Width</b><br>");
	na_String(handle, (*(pCVT->pfj_aUnits))[pfsys->lUnits].pLong);
	HSSend(handle, "</td>");
	HSSend(handle, "<td align=center><b>Height</b><br>");
	na_String(handle, (*(pCVT->pfj_aUnits))[pfsys->lUnits].pLong);
	HSSend(handle, "</td>");
	HSSend(handle, "<td align=center><b>Value</b><br>&nbsp;</td>");
	HSSend(handle, "<td align=center><b>Units</b><br>&nbsp;</td>");
	HSSend(handle, "<td align=center><b>Roll up</b><br>&nbsp;</td>");
	HSSend(handle, "</tr></thead><tbody>\n");

	papLabel = pCVT->fj_MemStorFindAllElements( pfms, pCVT->pfj_LabelID );
	for ( i = 0; NULL != *(papLabel+i); i++ )
	{
		pfl = pCVT->fj_LabelBuildFromString( pfms, *(papLabel+i) );
		if ( NULL != pfl )
		{
			HSSend(handle, "<tr>");
			if ( TRUE == bEdit )
			{
				HSSend(handle, "<td align=center><button class=fjbtn1 onClick='submit1(\"delete\",\"");
				na_String(handle, pfl->strName);
				HSSend(handle, "\")'>Delete</button></td>\n");

				HSSend(handle, "<td align=center><button class=fjbtn1 onClick=\"startedit('");
				na_String(handle, pfl->strName);
				HSSend(handle, "')\">Edit</button></td>");
			}
			else
			{
				if ( FALSE == bScanner ) pLabelName = pBRAM->strLabelName;
				else                     pLabelName = pBRAM->strScannerLabelName;
				if ( 0 == strcmp(pLabelName, pfl->strName) )
				{
					HSSend(handle, "<td align=center>Selected</td>");
				}
				else
				{
					HSSend(handle, "<td align=center>");
					if ( lLevel >= FJSYS_PRIORITY_MESSAGE_SELECT )
					{
						HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"");
						if ( TRUE == bScanner ) HSSend(handle, "select_scanner");
						else                    HSSend(handle, "select");
						HSSend(handle, "\",\"");
						na_String(handle, pfl->strName);
						HSSend(handle, "\")'>");
						if ( TRUE == bScanner ) HSSend(handle, "Select Scanner Default");
						else                    HSSend(handle, "Select");
						HSSend(handle, "</button>");
					}
					else
					{
						HSSend(handle, "&nbsp;");
					}
					HSSend(handle, "</td>");
				}
			}
			HSSend(handle, "<td align=center>");
			na_String(handle, pfl->strName);
			HSSend(handle, "</td>\n");
			for ( j = 0 ; ((j<pfsys->lActiveCount) && (j<3)); j++ )
			{
				HSSend(handle, "<td>");
				na_String(handle, pfl->fmMessageIDs[j]);
				HSSend(handle, "<br>");
				if ( 0 == pfl->fmMessageIDs[j][0] )
				{
					HSSend(handle, "(none)");
				}
				else
				{
					// message Name
					pCVT->fj_processParameterStringOutput( sMsgName, pfl->fmMessageIDs[j] );
					pfm = pCVT->fj_MessageBuildFromName( pfms, sMsgName );
					if ( NULL != pfm )
					{
						pfm->pfph = pfph;
						pfm->pfl  = pfl;
						// text was made by above call
						pCVT->fj_MessageGetText( pfm, str, 63 );
						na_String(handle, str);
						pCVT->fj_MessageDestroy( pfm );
					}
					else
					{
						HSSend(handle, "Label not on this Printer");
					}
				}
				HSSend(handle, "</td>");
			}
			HSSend(handle, "<td align=center>");
			na_Double(handle, pCVT->fj_FloatUnits( pfl->fBoxWidth, pfsys->lUnits), 2 );
			HSSend(handle, "</td>");
			HSSend(handle, "<td align=center>");
			na_Double(handle, pCVT->fj_FloatUnits( pfl->fBoxHeight, pfsys->lUnits), 2 );
			HSSend(handle, "</td>");
			HSSend(handle, "<td align=center>");
			na_Long(handle, pfl->lExpValue);
			HSSend(handle, "</td>");
			HSSend(handle, "<td align=center>");
			if ( FJ_LABEL_EXP_DAYS   == pfl->lExpUnits ) HSSend(handle, "Days");
			if ( FJ_LABEL_EXP_WEEKS  == pfl->lExpUnits ) HSSend(handle, "Weeks");
			if ( FJ_LABEL_EXP_MONTHS == pfl->lExpUnits ) HSSend(handle, "Months");
			if ( FJ_LABEL_EXP_YEARS  == pfl->lExpUnits ) HSSend(handle, "Years");
			HSSend(handle, "</td>");
			HSSend(handle, "<td align=center>");
			if (pfl->bExpRoundDown == TRUE)			HSSend(handle, "Down");
			else if ( TRUE == pfl->bExpRoundUp )	HSSend(handle, "Yes");
			else									HSSend(handle, "No");
			HSSend(handle, "</td>");
			HSSend(handle, "</tr>\n");
			pCVT->fj_LabelDestroy( pfl );
		}
	}
	HSSend(handle, "</tbody>\n");
}										/* @! Function: na_LBL_TABLE  }} */
/* @! Function: na_EDIT_BUTTONS  {{ */
void na_EDIT_BUTTONS(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	BOOL   bEdit;
	ULONG  ipAdd;
	LONG   lLevel;

	ipAdd = HSRemoteAddress(handle);
	lLevel = fj_get_level(handle);
	bEdit = pCVT->fj_PrintHeadIsEdit( pfph, ipAdd );
	if ( lLevel >= FJSYS_PRIORITY_MESSAGE_EDIT )
	{
		if ( TRUE == bEdit )
		{
			HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"edit_cancel\",\"edit_cancel\")'>Cancel Edit Session</button>");
			HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"edit_save\",\"edit_save\")'>Save Edits</button>");
		}
		else
		{
			if ( 0 == pfph->lEditID )
			{
				HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"edit_start\",\"edit_start\")'>Start Edit Session</button>");
			}
			else
			{
				HSSend(handle, "<button class=fjbtn1 DISABLED>Edit Unavailable</button>");
			}
		}
	}
	else
	{
		HSSend(handle, "<button class=fjbtn1 DISABLED>Not Authorized for Edit</button>");
	}
}										/* @! Function: na_EDIT_BUTTONS  }} */
/* @! Function: na_TITLE_LABELS  {{ */
void na_TITLE_LABELS(unsigned long handle)
{
	/* add your implementation here: */
	na_TITLE_ALL( handle, "Labels" );
}										/* @! Function: na_TITLE_LABELS  }} */
/* @! Function: na_PWID  {{ */
/* @! Function: na_STAvailEdit  {{ */
/* @! Function: na_DEF_SCAN_LBL_ID  {{ */
void na_DEF_SCAN_LBL_ID(unsigned long handle)
{
	/* add your implementation here: */
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;

	if ( TRUE == pfph->bSerialScannerLabel )
	{
		HSSend(handle, "<td align=left><b><button class=fjbtn1 onClick='submit1(\"select_scanner\",\"-1\")'>Select</button>&nbsp;Scanner Default Label Name: " );
		if ( 0 != pBRAM->strScannerLabelName[0] ) na_String(handle, pBRAM->strScannerLabelName);
		else                                      HSSend(handle, "(none)" );
		HSSend(handle, "</b></td>" );
	}
}										/* @! Function: na_DEF_SCAN_LBL_ID  }} */
/* @! Function: na_SEL_BUTTONS  {{ */
void na_SEL_BUTTONS(unsigned long handle)
{
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	if ( TRUE == pfph->bSerialScannerLabel ) HSSend(handle, "<td>&nbsp;</td>");

	if ( 0 != pBRAM->strLabelName[0] && FALSE == pGlobal->bStandByOn )
	{
		HSSend(handle, "<td align=left><button class=fjbtn1 onClick='submit1(\"laction\",\"start\");'");
		if (FALSE == pBRAM->bPrintIdle) HSSend(handle, " DISABLED");
		HSSend(handle, ">Resume</button>&nbsp;" );
		HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"laction\",\"idle\");'");
		if (TRUE == pBRAM->bPrintIdle) HSSend(handle, " DISABLED");
		HSSend(handle, ">Idle</button>&nbsp;" );
		HSSend(handle, "<button class=fjbtn1 onClick='submit1(\"laction\",\"stop\");'>Stop</button></td>" );
	}
	else
	{
		if ( FALSE == pGlobal->bStandByOn ) HSSend(handle, "<td>&nbsp;</td>");
		else HSSend(handle, "<td><FONT color=\"#003399\">Printer is in <b>StandBy</b> mode.</FONT></td>");
	}
	if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
	{
		HSSend(handle, "<td align=left><button class=fjbtn1 onClick='submit1(\"laction\",\"standby\");'>StandBy ");
		if ( FALSE == pGlobal->bStandByOn ) HSSend(handle, "On" );
		else                                HSSend(handle, "Off" );
		HSSend(handle, "</button></td>" );
	}
}										/* @! Function: na_SEL_BUTTONS  }} */
/* @! End of dynamic functions   */
