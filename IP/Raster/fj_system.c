#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#endif
#include <time.h>
#include <stdio.h>
#include "barcode.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_message.h"
#include "fj_element.h"
#include "fj_counter.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"

//
// initial values for the months, days, and hours used for printing
//
static CHAR fj_DayN[7][16] =
{
	'S','U','N','D','A','Y',0  ,0  ,0  ,0  ,0,0,0,0,0,0,
	'M','O','N','D','A','Y',0  ,0  ,0  ,0  ,0,0,0,0,0,0,
	'T','U','E','S','D','A','Y',0  ,0  ,0  ,0,0,0,0,0,0,
	'W','E','D','N','E','S','D','A','Y',0  ,0,0,0,0,0,0,
	'T','H','U','R','S','D','A','Y',0  ,0  ,0,0,0,0,0,0,
	'F','R','I','D','A','Y',0  ,0  ,0  ,0  ,0,0,0,0,0,0,
	'S','A','T','U','R','D','A','Y',0  ,0  ,0,0,0,0,0,0
};
static CHAR fj_DayS[7][16] =
{
	'S','O','N','N','T','A','G',0  ,0  ,0  ,0,0,0,0,0,0,
	'M','O','N','T','A','G',0  ,0  ,0  ,0  ,0,0,0,0,0,0,
	'D','I','E','N','S','T','A','G',0  ,0  ,0,0,0,0,0,0,
	'M','I','T','T','W','O','C','H',0  ,0  ,0,0,0,0,0,0,
	'D','O','N','N','E','R','S','T','A','G',0,0,0,0,0,0,
	'F','R','E','I','T','A','G',0  ,0  ,0  ,0,0,0,0,0,0,
	'S','A','M','S','T','A','G',0  ,0  ,0  ,0,0,0,0,0,0
};
static CHAR fj_MonthN[12][16] =
{
	'J','A','N','U','A','R','Y',0  ,0  ,0,0,0,0,0,0,0,
	'F','E','B','R','U','A','R','Y',0  ,0,0,0,0,0,0,0,
	'M','A','R','C','H',0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'A','P','R','I','L',0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'M','A','Y',0  ,0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'J','U','N','E',0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'J','U','L','Y',0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'A','U','G','U','S','T',0  ,0  ,0  ,0,0,0,0,0,0,0,
	'S','E','P','T','E','M','B','E','R',0,0,0,0,0,0,0,
	'O','E','T','O','B','E','R',0  ,0  ,0,0,0,0,0,0,0,
	'N','O','B','E','M','B','E','R',0  ,0,0,0,0,0,0,0,
	'D','E','C','E','M','B','E','R',0  ,0,0,0,0,0,0,0
};
static CHAR fj_MonthS[12][16] =
{
	'J','A','N','U','A','R',0  ,0  ,0  ,0,0,0,0,0,0,0,
	'F','E','B','R','U','A','R',0  ,0  ,0,0,0,0,0,0,0,
	'M','A','R','Z',0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'A','P','R','I','L',0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'M','A','I',0  ,0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'J','U','N','I',0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'J','U','L','I',0  ,0  ,0  ,0  ,0  ,0,0,0,0,0,0,0,
	'A','U','G','U','S','T',0  ,0  ,0  ,0,0,0,0,0,0,0,
	'S','E','P','T','E','M','B','E','R',0,0,0,0,0,0,0,
	'O','K','T','O','B','E','R',0  ,0  ,0,0,0,0,0,0,0,
	'N','O','V','E','M','B','E','R',0  ,0,0,0,0,0,0,0,
	'D','E','Z','E','M','B','E','R',0  ,0,0,0,0,0,0,0
};
static CHAR fj_AMPM[2][16] =
{
	' ','A','M',0,0,0,0,0,0,0,0,0,0,0,0,0,
	' ','P','M',0,0,0,0,0,0,0,0,0,0,0,0,0
};
static CHAR fj_HourA[24][16] =
{
	'A',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'B',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'C',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'D',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'E',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'F',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'G',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'H',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'I',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'J',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'K',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'L',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'M',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'N',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'O',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'P',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'Q',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'R',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'S',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'T',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'U',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'V',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'W',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	'X',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

//
// months and days used for display in the web pages
//
static LPSTR fj_apWebMonths[12] = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
static LPSTR fj_apWebDays[7]    = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

//
// units table
//
// offset of entries in this table must match enum definition in fj_system.h
//

static int fj_nBwVer = 0;
static int fj_MakeBwVersion (int nMajor, int nMinor)
{
    int n = nMinor;

    if (n < 99)
	n *= 10;
    
    return (nMajor * 1000) + n;
}

#ifdef _WINDOWS
_FJSYSUNITS
#else
FJSYSUNITS
#endif
fj_aUnits[6] = { NULL, NULL, "Feet", "ft", "Inches", "in", "Meters", "m", "Centimeters", "cm", "Millimeters", "mm" };

//
//  FUNCTION: fj_SystemGroupMemInit()
//
//  PURPOSE:  Initialize a new FJSYSGROUPMEM structure
//
VOID fj_SystemGroupMemInit( LPFJSYSGROUPMEM pfsm )
{
	pfsm->lIPaddr  = 0;					// print head IP address
	pfsm->pfph     = NULL;				// pointer to FJPRINTHEAD structure
	return;
}
//
//  FUNCTION: fj_SystemGroupMemClose()
//
//  PURPOSE:  Close a FJSYSGROUPMEM structure
//
VOID fj_SystemGroupMemClose( LPFJSYSGROUPMEM pfsm )
{
	LPFJPRINTHEAD pfph;

	pfph = pfsm->pfph;
	if ( NULL != pfph )
	{
		pfsm->pfph = NULL;
		fj_PrintHeadDestroy( pfph );
	}
	fj_SystemGroupMemInit( pfsm );
	return;
}
struct fjsystable
{
	LPSTR pStr;
	VOID (*pGet)( CLPCFJSYSTEM pfsys, LPSTR pString );
	VOID (*pSet)( CLPFJSYSTEM  pfsys, LPSTR pString );
};

FJDLLExport VOID fj_SysGetID        (CLPCFJSYSTEM pfsys, LPSTR pStr)
{
	STR sName[FJSYS_NAME_SIZE*2];

	fj_processParameterStringOutput( sName, pfsys->strID );
	strcpy( pStr, sName );
}
FJDLLExport VOID fj_SysGetCount     (CLPCFJSYSTEM pfsys, LPSTR pStr){fj_LongToStr( pStr, pfsys->lActiveCount );}
FJDLLExport VOID fj_SysGetMemIDs    (CLPCFJSYSTEM pfsys, LPSTR pStr)
{
	STR sName[FJSYS_NAME_SIZE*2];
	int i;

	*pStr = 0;
	for ( i = 0; i < pfsys->lActiveCount; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( sName, pfsys->gmMembers[i].strID );
		strcat( pStr, sName );
	}
}
FJDLLExport VOID fj_SysGetMemIPAddrs( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[24];
	LONG ip;
	int i;

	*pStr = 0;
	for ( i = 0; i < pfsys->lActiveCount; i++ )
	{
		ip = pfsys->gmMembers[i].lIPaddr;
		sprintf( str, "%d.%d.%d.%d", (int)((unsigned)((ip>>24)&0xff)),(int)((unsigned)((ip>>16)&0xff)),(int)((unsigned)((ip>>8)&0xff)),(int)((unsigned)(ip&0xff)) );
		if ( 0 != i ) strcat( pStr, "," );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetUnits( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	LPSTR pLocal;

	pLocal = "ENGLISH_FEET";
	if      ( FJSYS_UNITS_ENGLISH_INCHES == pfsys->lUnits ) pLocal = "ENGLISH_INCHES";
	else if ( FJSYS_UNITS_METRIC_METERS  == pfsys->lUnits ) pLocal = "METRIC_METERS";
	else if ( FJSYS_UNITS_METRIC_CM      == pfsys->lUnits ) pLocal = "METRIC_CM";
	else if ( FJSYS_UNITS_METRIC_MM      == pfsys->lUnits ) pLocal = "METRIC_MM";
	strcpy( pStr, pLocal );
}
FJDLLExport VOID fj_SysGetTimeDisp( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	LPSTR pLocal;

	pLocal = "24";
	if      ( FJSYS_TIMEDIS_12  == pfsys->lTimeDisplay ) pLocal = "12";
	else if ( FJSYS_TIMEDIS_12M == pfsys->lTimeDisplay ) pLocal = "12M";
	strcpy( pStr, pLocal );
}
FJDLLExport VOID fj_SysGetIOBox            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_BoolToStr  ( pStr, pfsys->bIOBoxUse );}
FJDLLExport VOID fj_SysGetIOBOXStrobe      ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr  ( pStr, (LONG)pfsys->iIOBoxStrobe );}

FJDLLExport VOID fj_SysGetShaftEncoder     ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_BoolToStr  ( pStr, pfsys->bShaftEncoder );}
FJDLLExport VOID fj_SysGetEncoderCounts    ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fEncoder );}
FJDLLExport VOID fj_SysGetEncoderWheel     ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fEncoderWheel );}
FJDLLExport VOID fj_SysGetEncoderDivisor   ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr  ( pStr, pfsys->lEncoderDivisor );}
FJDLLExport VOID fj_SysGetConveyorSpeed    ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fSpeed );}
FJDLLExport VOID fj_SysGetPrintResolution  ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fPrintResolution );}
FJDLLExport VOID fj_SysGetInkBottleSize    ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fInkBottleSize );}
FJDLLExport VOID fj_SysGetInkBottleCost    ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fInkBottleCost );}
FJDLLExport VOID fj_SysGetTimeSource       ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_BoolToStr  ( pStr, pfsys->bInternalClock );}
FJDLLExport VOID fj_SysGetTime             ( CLPCFJSYSTEM pfsys, LPSTR pStr ){struct tm tm;fj_SystemGetTime(&tm);fj_SystemGetDateTimeString( pStr, &tm );}
FJDLLExport VOID fj_SysGetTimeZone         ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, ((FLOAT)pfsys->lTimeZoneSeconds)/3600. );}
FJDLLExport VOID fj_SysGetDSTUSA           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_BoolToStr( pStr, pfsys->bDSTUSA );}
FJDLLExport VOID fj_SysGetFirstDay         ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, pfsys->lWeekDay );}
FJDLLExport VOID fj_SysGetWeek1            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, pfsys->lWeek1 );}
FJDLLExport VOID fj_SysGetWeek53           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, pfsys->lWeek53 );}
FJDLLExport VOID fj_SysGetRollover         ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_DoubleToStr( pStr, pfsys->fRolloverHours );}
FJDLLExport VOID fj_SysGetShift            ( int iShift, LPSTR pStr )
{
	int iHour   = iShift/60;
	int iMinute = iShift - iHour*60;
	sprintf( pStr, "%i:%i", iHour, iMinute );
}
FJDLLExport VOID fj_SysGetShift1           ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	fj_SysGetShift( pfsys->lShift1, pStr );
}
FJDLLExport VOID fj_SysGetShift2           ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	fj_SysGetShift( pfsys->lShift2, pStr );
}
FJDLLExport VOID fj_SysGetShift3           ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	fj_SysGetShift( pfsys->lShift3, pStr );
}
FJDLLExport VOID fj_SysGetShiftCode1       ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	STR sName[FJSYS_SHIFT_CODE_SIZE*2];
	fj_processParameterStringOutput( sName, pfsys->sShiftCode1 );
	strcpy( pStr, sName );
}
FJDLLExport VOID fj_SysGetShiftCode2       ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	STR sName[FJSYS_SHIFT_CODE_SIZE*2];
	fj_processParameterStringOutput( sName, pfsys->sShiftCode2 );
	strcpy( pStr, sName );
}
FJDLLExport VOID fj_SysGetShiftCode3       ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	STR sName[FJSYS_SHIFT_CODE_SIZE*2];
	fj_processParameterStringOutput( sName, pfsys->sShiftCode3 );
	strcpy( pStr, sName );
}
FJDLLExport VOID fj_SysGetDays( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 7; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aDayN[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetMonths( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 12; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aMonthN[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetHours( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 24; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aHour[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetDaysSpecial      ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 7; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aDayS[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetMonthsSpecial    ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 12; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aMonthS[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetAMPM             ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR str[FJDATETIME_STRING_SIZE*2];
	int i;
	*pStr = 0;
	for ( i = 0; i < 2; i++ )
	{
		if ( 0 != i ) strcat( pStr, "," );
		fj_processParameterStringOutput( str, pfsys->aAMPM[i] );
		strcat( pStr, str );
	}
}
FJDLLExport VOID fj_SysGetInternetControl   ( CLPCFJSYSTEM pfsys, LPSTR pStr ){*pStr = pfsys->cInternetControl;*(pStr+1) = 0;}
FJDLLExport VOID fj_SysGetDNSIP             ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	int i;
	for ( i = 0; i < FJSYS_NUMBER_DNS; i++ )
	{
		if ( 0 != i )
		{
			*pStr = ',';
			pStr ++;
		}
		fj_IPToStr( pfsys->alAddrDNS[i], pStr );
		pStr += strlen( pStr );
	}
}
FJDLLExport VOID fj_SysGetEmailIP           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_IPToStr( pfsys->lAddrEmail, pStr );}
FJDLLExport VOID fj_SysGetEmailServer       ( CLPCFJSYSTEM pfsys, LPSTR pStr ){*pStr = 0;fj_processParameterStringOutput( pStr, pfsys->strEmailServer );}
FJDLLExport VOID fj_SysGetEmailAppendage    ( CLPCFJSYSTEM pfsys, LPSTR pStr ){*pStr = 0;fj_processParameterStringOutput( pStr, pfsys->strEmailName );}
FJDLLExport VOID fj_SysGetEmailInk          ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	int i;
	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		if ( 0 != i )
		{
			*pStr = ',';
			pStr ++;
		}
		fj_processParameterStringOutput( pStr, pfsys->astrEmailInk[i] );
		pStr += strlen( pStr );
	}
}
FJDLLExport VOID fj_SysGetEmailService      ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	int i;
	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		if ( 0 != i )
		{
			*pStr = ',';
			pStr ++;
		}
		fj_processParameterStringOutput( pStr, pfsys->astrEmailService[i] );
		pStr += strlen( pStr );
	}
}
FJDLLExport VOID fj_SysGetIOBOXIP           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_IPToStr( pfsys->lIOBoxIP, pStr );}
FJDLLExport VOID fj_SysGetWINSIP            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_IPToStr( pfsys->lAddrWINS, pStr );}
FJDLLExport VOID fj_SysGetWINSType          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){LPSTR pResp = "NONE";if ( FJSYS_NETBIOS_B_NODE == pfsys->sNETBIOSService ) pResp = "B"; else if ( FJSYS_NETBIOS_P_NODE == pfsys->sNETBIOSService ) pResp = "P"; strcpy( pStr, pResp );}

FJDLLExport VOID fj_SysGetPasswords         ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
	LPCSTR pPassword;

	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_DISTRIBUTOR );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_SECURITY );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_CONFIGURATION );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_RESTORE );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_EDIT );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_COUNTERS );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_TEST );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_SELECT );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr ); *pStr = ','; pStr++; *pStr = 0;
	pPassword = fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_VIEW_ALL );
	fj_processParameterStringOutput( pStr, pPassword );
	pStr += strlen( pStr );
}
static LONG           fj_SysBCIndex = 0;
static LONG           fj_SysDMIndex = 0;
static LPFJSYSBARCODE fj_SysBCP = NULL;
static LPFJSYSDATAMATRIX fj_SysDMP = NULL;

FJDLLExport VOID fj_SysGetBCIndex           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCIndex );}
FJDLLExport VOID fj_SysGetBCName            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_processParameterStringOutput(pStr,fj_SysBCP->strName);}
FJDLLExport VOID fj_SysGetBCType            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBCFlags&BARCODE_ENCODING_MASK );}
FJDLLExport VOID fj_SysGetBCHeight          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBCHeight );}
FJDLLExport VOID fj_SysGetBCCheck           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_BoolToStr( pStr, fj_SysBCP->bCheckDigit );}
FJDLLExport VOID fj_SysGetBCBearerH         ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lHor_Bearer );}
FJDLLExport VOID fj_SysGetBCBearerV         ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lVer_Bearer );}
FJDLLExport VOID fj_SysGetBCQuiet           ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lQuietZone );}
FJDLLExport VOID fj_SysGetBCBase            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBarVal[0] );}
FJDLLExport VOID fj_SysGetBCBleed1          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBarVal[1] );}
FJDLLExport VOID fj_SysGetBCBleed2          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBarVal[2] );}
FJDLLExport VOID fj_SysGetBCBleed3          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBarVal[3] );}
FJDLLExport VOID fj_SysGetBCBleed4          ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_SysBCP->lBarVal[4] );}
FJDLLExport VOID fj_SysGetAppVer            ( CLPCFJSYSTEM pfsys, LPSTR pStr ){fj_LongToStr( pStr, fj_nBwVer );} // burl 1.2028

FJDLLExport VOID fj_SysGetDataMatrixIndex	( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_LongToStr( pStr, fj_SysDMIndex );}
FJDLLExport VOID fj_SysGetDataMatrixName	( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_processParameterStringOutput(pStr,fj_SysDMP->strName);}
FJDLLExport VOID fj_SysGetDataMatrixBleed1  ( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_LongToStr( pStr, fj_SysDMP->lHeight );}
FJDLLExport VOID fj_SysGetDataMatrixBleed2  ( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_LongToStr( pStr, fj_SysDMP->lHeightBold );}
FJDLLExport VOID fj_SysGetDataMatrixBleed3  ( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_LongToStr( pStr, fj_SysDMP->lWidth );}
FJDLLExport VOID fj_SysGetDataMatrixBleed4  ( CLPCFJSYSTEM pfsys, LPSTR pStr ){	fj_LongToStr( pStr, fj_SysDMP->lWidthBold );}


FJDLLExport VOID fj_SysSetID        (CLPFJSYSTEM pfsys, LPSTR pStr){fj_processParameterStringInput(pStr);strncpy(pfsys->strID       , pStr, FJSYS_NAME_SIZE  );pfsys->strID[FJSYS_NAME_SIZE]=0;}
FJDLLExport VOID fj_SysSetCount     (CLPFJSYSTEM pfsys, LPSTR pStr){pfsys->lActiveCount =  (LONG)atoi(pStr);}
FJDLLExport VOID fj_SysSetMemIDs    (CLPFJSYSTEM pfsys, LPSTR pStr)
{
	LPSTR pList[FJSYS_NUMBER_PRINTERS];
	int n;
	int i;

	n = fj_ParseKeywordsFromStr( pStr, ',', pList, FJSYS_NUMBER_PRINTERS );
	n = min( n, pfsys->lActiveCount );
	n = min( n, FJSYS_NUMBER_PRINTERS );
	for ( i = 0; i < n; i++ )
	{
		fj_processParameterStringInput( pList[i] );
		strncpy( pfsys->gmMembers[i].strID, pList[i], FJPH_ID_SIZE ); pfsys->gmMembers[i].strID[FJPH_ID_SIZE]=0;
	}
}
FJDLLExport VOID fj_SysSetMemIPAddrs  (CLPFJSYSTEM pfsys, LPSTR pStr)
{
	LPSTR pList[FJSYS_NUMBER_PRINTERS];
	unsigned int a1,a2,a3,a4;
	int n;
	int i;

	n = fj_ParseKeywordsFromStr( pStr, ',', pList, FJSYS_NUMBER_PRINTERS );
	n = min( n, pfsys->lActiveCount );
	n = min( n, FJSYS_NUMBER_PRINTERS );
	for ( i = 0; i < n; i++ )
	{
		sscanf( pList[i], "%d.%d.%d.%d", &a1, &a2, &a3, &a4 );
		pfsys->gmMembers[i].lIPaddr = (a1<<24) + (a2<<16) + (a3<<8) + a4;
	}
}
FJDLLExport VOID fj_SysSetUnits( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	if      ( 0 == strcmp(pStr,"ENGLISH_INCHES") ) pfsys->lUnits = FJSYS_UNITS_ENGLISH_INCHES;
	else if ( 0 == strcmp(pStr,"METRIC_METERS")  ) pfsys->lUnits = FJSYS_UNITS_METRIC_METERS;
	else if ( 0 == strcmp(pStr,"METRIC_CM")      ) pfsys->lUnits = FJSYS_UNITS_METRIC_CM;
	else if ( 0 == strcmp(pStr,"METRIC_MM")      ) pfsys->lUnits = FJSYS_UNITS_METRIC_MM;
	else                                           pfsys->lUnits = FJSYS_UNITS_ENGLISH_FEET;
}
FJDLLExport VOID fj_SysSetTimeDisp( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	if      ( 0 == strcmp(pStr,"12")  ) pfsys->lTimeDisplay = FJSYS_TIMEDIS_12;
	else if ( 0 == strcmp(pStr,"12M") ) pfsys->lTimeDisplay = FJSYS_TIMEDIS_12M;
	else                                pfsys->lTimeDisplay = FJSYS_TIMEDIS_24;
}
FJDLLExport VOID fj_SysSetPrintMode( CLPFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	LPFJPRINTHEAD pfph    = pCVT->pfph;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
	{
		if      ( 0 == strcmp(pStr,"STANDBYON"))
		{
			pGlobal->bStandByOn = TRUE;
			if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}
		}
		else if ( 0 == strcmp(pStr,"STANDBYOFF"))
		{
			pGlobal->bStandByOn = FALSE;
			if ( FALSE == pBRAM->bPrintIdle &&
				0 != pBRAM->strLabelName[0] &&
				FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}
		}
		fj_print_head_SetGAWriteSwitches();
	}

	if  ( 0 != pBRAM->strLabelName[0] && FALSE == pGlobal->bStandByOn )
	{
		if      ( 0 == strcmp(pStr,"RESUME"))
		{
			pBRAM->bPrintIdle = FALSE;
			if ( FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}
		}
		else if ( 0 == strcmp(pStr,"IDLE"))
		{
			pBRAM->bPrintIdle = TRUE;
			if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}
		}
		else if ( 0 == strcmp(pStr,"STOP"))
		{
			pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
			pCVT->fj_SystemBuildSelectedLabel( pfsys, NULL );
			pCVT->pBRAM->strLabelName[0] = 0;
			pCVT->pBRAM->bPrintIdle = FALSE;
			if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}
		}

		fj_print_head_SetStatus();
		fj_print_head_SendStatus();
	}
#endif								//!_WINDOWS
}
FJDLLExport VOID fj_SysGetPrintMode        ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	if ( TRUE == pGlobal->bStandByOn ) strcpy(pStr, "STANDBY");
	else if ( 0 == pBRAM->strLabelName[0] )  strcpy(pStr, "STOP");
	else if (TRUE == pBRAM->bPrintIdle) strcpy(pStr, "IDLE");
	else                                strcpy(pStr, "RESUME");
#endif								//!_WINDOWS
}
FJDLLExport VOID fj_SysSetAPSMode( CLPFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfph       = &(pParam->FJPh);
	LPFJPRINTHEAD   pfphA        = pCVT->pfph; // Burl v 1.2001
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;

	if      ( 0 == strcmp(pStr,"ENABLE"))
	{
		pfphA->bAPSenable = pfph->bAPSenable = TRUE;
	}
	else if ( 0 == strcmp(pStr,"DISABLE"))
	{
		pfphA->bAPSenable = pfph->bAPSenable = FALSE;
	}
	else if ( 0 == strcmp(pStr,"RUN"))
	{
		if (TRUE == pfph->bAPSenable)
		{
										// set AMS start time to now
			pCVT->pBRAM->lAMS_SST = pGlobal->lSeconds;
		}
	}
#endif								//!_WINDOWS
}

FJDLLExport VOID fj_SysGetAPSMode        ( CLPCFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	LPFJ_PARAM      pParam      = pCVT->pFJparam;
	LPFJPRINTHEAD   pfph       = &(pParam->FJPh);
	if   (TRUE ==   pfph->bAPSenable) strcpy(pStr, "ENABLE");
	else                  strcpy(pStr, "DISABLE");
#endif								//!_WINDOWS
}
    
FJDLLExport VOID fj_SysSetIOBox            ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->bIOBoxUse = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_SysSetIOBOXStrobe      ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->iIOBoxStrobe = atoi( pStr );}

FJDLLExport VOID fj_SysSetShaftEncoder     ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->bShaftEncoder = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_SysSetEncoderCounts    ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	pfsys->fEncoder = (FLOAT)atof( pStr );
	if( 0.0 == pfsys->fEncoder ) pfsys->fEncoder=300.0;
#endif								//_WINDOWS
}
FJDLLExport VOID fj_SysSetEncoderWheel     ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
#ifndef _WINDOWS
	pfsys->fEncoderWheel = (FLOAT)atof( pStr );
	if( 0.0 == pfsys->fEncoderWheel ) pfsys->fEncoderWheel=300.0;
#endif								//_WINDOWS
}
FJDLLExport VOID fj_SysSetEncoderDivisor   ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->lEncoderDivisor = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetConveyorSpeed    ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->fSpeed = (FLOAT)atof( pStr );}
FJDLLExport VOID fj_SysSetPrintResolution  ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->fPrintResolution = (FLOAT)atof( pStr );}
FJDLLExport VOID fj_SysSetInkBottleSize    ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->fInkBottleSize = (FLOAT)atof( pStr );}
FJDLLExport VOID fj_SysSetInkBottleCost    ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->fInkBottleCost = (FLOAT)atof( pStr );}
FJDLLExport VOID fj_SysSetTimeSource       ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->bInternalClock = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_SysSetTime             ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	//#ifdef _WINDOWS
	fj_SystemParseDateTime( pStr, &(pfsys->tmPhotoCell) );
#ifndef _WINDOWS					//!_WINDOWS
	fj_print_head_SetRTClockTM( &(pfsys->tmPhotoCell) );
#endif								//!_WINDOWS
}
FJDLLExport VOID fj_SysSetTimeZone         ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->lTimeZoneSeconds = 3600 * (LONG)atof( pStr );}
FJDLLExport VOID fj_SysSetDSTUSA           ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->bDSTUSA = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_SysSetFirstDay         ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->lWeekDay = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetWeek1            ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->lWeek1 = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetWeek53           ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->lWeek53 = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetRollover         ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->fRolloverHours = (FLOAT)atof( pStr );}
FJDLLExport VOID fj_SysSetShift1           ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	char *pcolon;
	int iMinutes = 0;
	int iHours = 0;
	iMinutes = atoi( pStr );
	pcolon = strchr( pStr, ':' );
	if ( NULL != pcolon )
	{
		iHours = iMinutes;
		iMinutes = atoi( pcolon+1 );
	}
	pfsys->lShift1 = iMinutes + iHours * 60;
}
FJDLLExport VOID fj_SysSetShift2           ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	char *pcolon;
	int iMinutes = 0;
	int iHours = 0;
	iMinutes = atoi( pStr );
	pcolon = strchr( pStr, ':' );
	if ( NULL != pcolon )
	{
		iHours = iMinutes;
		iMinutes = atoi( pcolon+1 );
	}
	pfsys->lShift2 = iMinutes + iHours * 60;
}
FJDLLExport VOID fj_SysSetShift3           ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	char *pcolon;
	int iMinutes = 0;
	int iHours = 0;
	iMinutes = atoi( pStr );
	pcolon = strchr( pStr, ':' );
	if ( NULL != pcolon )
	{
		iHours = iMinutes;
		iMinutes = atoi( pcolon+1 );
	}
	pfsys->lShift3 = iMinutes + iHours * 60;
}
FJDLLExport VOID fj_SysSetShiftCode1       ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_processParameterStringInput(pStr);strncpy(pfsys->sShiftCode1, pStr, FJSYS_SHIFT_CODE_SIZE);pfsys->sShiftCode1[FJSYS_SHIFT_CODE_SIZE]=0;}
FJDLLExport VOID fj_SysSetShiftCode2       ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_processParameterStringInput(pStr);strncpy(pfsys->sShiftCode2, pStr, FJSYS_SHIFT_CODE_SIZE);pfsys->sShiftCode2[FJSYS_SHIFT_CODE_SIZE]=0;}
FJDLLExport VOID fj_SysSetShiftCode3       ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_processParameterStringInput(pStr);strncpy(pfsys->sShiftCode3, pStr, FJSYS_SHIFT_CODE_SIZE);pfsys->sShiftCode3[FJSYS_SHIFT_CODE_SIZE]=0;}
FJDLLExport VOID fj_SysSetDays             ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*7+24];
	LPSTR pList[7];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 7 );
	for ( i = 0; i < 7; i++ )
	{
		pfsys->aDayN[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aDayN[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aDayN[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetMonths           ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*12+24];
	LPSTR pList[12];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 12 );
	for ( i = 0; i < 12; i++ )
	{
		pfsys->aMonthN[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aMonthN[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aMonthN[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetHours            ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*24+24];
	LPSTR pList[24];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 24 );
	for ( i = 0; i < 24; i++ )
	{
		pfsys->aHour[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aHour[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aHour[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetDaysSpecial      ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*7+24];
	LPSTR pList[7];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 7 );
	for ( i = 0; i < 7; i++ )
	{
		pfsys->aDayS[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aDayS[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aDayS[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetMonthsSpecial    ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*12+24];
	LPSTR pList[12];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 12 );
	for ( i = 0; i < 12; i++ )
	{
		pfsys->aMonthS[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aMonthS[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aMonthS[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetAMPM             ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR  strIn[FJDATETIME_STRING_SIZE*2*2+24];
	LPSTR pList[2];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, 2 );
	for ( i = 0; i < 2; i++ )
	{
		pfsys->aAMPM[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->aAMPM[i], pList[i], FJDATETIME_STRING_SIZE ); pfsys->aAMPM[i][FJDATETIME_STRING_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetInternetControl  ( CLPFJSYSTEM pfsys, LPSTR pStr ){pfsys->cInternetControl = *pStr;}
FJDLLExport VOID fj_SysSetDNSIP            ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_NUMBER_DNS*24];
	LPSTR pList[FJSYS_NUMBER_DNS];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, FJSYS_NUMBER_DNS );
	for ( i = 0; i < FJSYS_NUMBER_DNS; i++ )
	{
		pfsys->alAddrDNS[i] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			pfsys->alAddrDNS[i] = fj_StrToIP(  pList[i] );
		}
	}
}
FJDLLExport VOID fj_SysSetEmailIP          ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[24];

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	fj_processParameterStringInput( strIn );
	pfsys->lAddrEmail = fj_StrToIP( strIn );
}
FJDLLExport VOID fj_SysSetEmailServer      ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_EMAIL_SERVER_SIZE*2];

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	fj_processParameterStringInput( strIn );
	strncpy( pfsys->strEmailServer, strIn, FJSYS_EMAIL_SERVER_SIZE ); pfsys->strEmailServer[FJSYS_EMAIL_SERVER_SIZE] = 0;
}
FJDLLExport VOID fj_SysSetEmailAppendage   ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_EMAIL_SERVER_SIZE*2];

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	fj_processParameterStringInput( strIn );
	strncpy( pfsys->strEmailName, strIn, FJSYS_EMAIL_SERVER_SIZE ); pfsys->strEmailName[FJSYS_EMAIL_SERVER_SIZE] = 0;
}
FJDLLExport VOID fj_SysSetEmailInk         ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_EMAIL_NUMBER*FJSYS_EMAIL_ID_SIZE*2];
	LPSTR pList[FJSYS_EMAIL_NUMBER];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, FJSYS_EMAIL_NUMBER );
	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		pfsys->astrEmailInk[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->astrEmailInk[i], pList[i], FJSYS_EMAIL_ID_SIZE ); pfsys->astrEmailInk[i][FJSYS_EMAIL_ID_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetEmailService    ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_EMAIL_NUMBER*FJSYS_EMAIL_ID_SIZE*2];
	LPSTR pList[FJSYS_EMAIL_NUMBER];
	int i,n;
	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, FJSYS_EMAIL_NUMBER );
	for ( i = 0; i < FJSYS_EMAIL_NUMBER; i++ )
	{
		pfsys->astrEmailService[i][0] = 0;
		if ( i < n )
		{
			fj_processParameterStringInput( pList[i] );
			strncpy( pfsys->astrEmailService[i], pList[i], FJSYS_EMAIL_ID_SIZE ); pfsys->astrEmailService[i][FJSYS_EMAIL_ID_SIZE] = 0;
		}
	}
}
FJDLLExport VOID fj_SysSetWINSIP           ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[24];

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	fj_processParameterStringInput( strIn );
	pfsys->lAddrWINS = fj_StrToIP( strIn );
}
FJDLLExport VOID fj_SysSetWINSType        ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	pfsys->sNETBIOSService = FJSYS_NETBIOS_NONE;
	if ( 'P' == *pStr ) pfsys->sNETBIOSService = FJSYS_NETBIOS_P_NODE;
	if ( 'B' == *pStr ) pfsys->sNETBIOSService = FJSYS_NETBIOS_B_NODE;
}
FJDLLExport VOID fj_SysSetPasswords       ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[FJSYS_PRIORITY_LEVELS*FJSYS_PASSWORD_SIZE*2];
	LPSTR pList[FJSYS_PRIORITY_LEVELS];
	LPSTR pPassword;
	LPSTR pParm;
	int n;

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	n = (int)fj_ParseKeywordsFromStr( strIn, ',', pList, FJSYS_PRIORITY_LEVELS );
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_VIEW_ALL );
	pParm = pList[8];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_SELECT );
	pParm = pList[7];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_TEST );
	pParm = pList[6];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_COUNTERS );
	pParm = pList[5];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_MESSAGE_EDIT );
	pParm = pList[4];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_RESTORE );
	pParm = pList[3];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_CONFIGURATION );
	pParm = pList[2];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_SECURITY );
	pParm = pList[1];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
	pPassword = (LPSTR)fj_SystemGetPasswordForLevel( pfsys, FJSYS_PRIORITY_DISTRIBUTOR );
	pParm = pList[0];if ( NULL != pParm ){fj_processParameterStringInput( pParm );*(pParm+FJSYS_PASSWORD_SIZE) = 0;strncpy( pPassword, pParm, FJSYS_PASSWORD_SIZE ); *(pPassword+FJSYS_PASSWORD_SIZE) = 0;}
}
FJDLLExport VOID fj_SysSetBCIndex         ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCIndex = (LONG)atoi( pStr );fj_SysBCP = &(pfsys->bcArray[fj_SysBCIndex]);}
FJDLLExport VOID fj_SysSetBCName          ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_processParameterStringInput(pStr);strncpy(fj_SysBCP->strName,pStr,FJSYS_BARCODE_NAME_SIZE);fj_SysBCP->strName[FJSYS_BARCODE_NAME_SIZE]=0;}
FJDLLExport VOID fj_SysSetBCType          ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBCFlags = ((LONG)atoi(pStr)) | fj_SysBCP->lBCFlags&~BARCODE_ENCODING_MASK;}
FJDLLExport VOID fj_SysSetBCHeight        ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBCHeight = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCCheck         ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->bCheckDigit = (*pStr == 'T' ? TRUE : FALSE);}
FJDLLExport VOID fj_SysSetBCBearerH       ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lHor_Bearer = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBearerV       ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lVer_Bearer = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCQuiet         ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lQuietZone = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBase          ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBarVal[0] = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBleed1        ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBarVal[1] = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBleed2        ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBarVal[2] = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBleed3        ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBarVal[3] = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetBCBleed4        ( CLPFJSYSTEM pfsys, LPSTR pStr ){fj_SysBCP->lBarVal[4] = (LONG)atoi( pStr );}

FJDLLExport VOID fj_SysSetDataMatrixIndex	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_SysDMIndex = (LONG)atoi( pStr );fj_SysDMP = &(pfsys->bcDataMatrixArray[fj_SysDMIndex]);}
FJDLLExport VOID fj_SysSetDataMatrixName	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_processParameterStringInput(pStr);strncpy(fj_SysDMP->strName,pStr,FJSYS_BARCODE_NAME_SIZE);fj_SysDMP->strName[FJSYS_BARCODE_NAME_SIZE]=0;}
FJDLLExport VOID fj_SysSetDataMatrixBleed1	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_SysDMP->lHeight = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetDataMatrixBleed2	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_SysDMP->lHeightBold = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetDataMatrixBleed3	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_SysDMP->lWidth = (LONG)atoi( pStr );}
FJDLLExport VOID fj_SysSetDataMatrixBleed4	( CLPFJSYSTEM pfsys, LPSTR pStr ){	fj_SysDMP->lWidthBold = (LONG)atoi( pStr );}

FJDLLExport VOID fj_SysSetAppVer          ( CLPFJSYSTEM pfsys, LPSTR pStr ) // burl 1.2028
{
    int nMajor = 0, nMinor = 0;

    sscanf (pStr, "%d.%d", &nMajor, &nMinor);
    fj_nBwVer = fj_MakeBwVersion (nMajor, nMinor);
}

FJDLLExport VOID fj_SysSetIOBOXIP         ( CLPFJSYSTEM pfsys, LPSTR pStr )
{
	CHAR strIn[24];

	strncpy( strIn, pStr, sizeof(strIn) ); strIn[sizeof(strIn)-1] = 0;
	fj_processParameterStringInput( strIn );
	pfsys->lIOBoxIP = fj_StrToIP( strIn );
}
struct fjsystable fj_SysParams[] =
{
#define GR_PARAM_FIRST   0
	"GROUP_NAME",             fj_SysGetID,                fj_SysSetID,				// 0
	"GROUP_COUNT",            fj_SysGetCount,             fj_SysSetCount,			// 1
	"GROUP_IDS",              fj_SysGetMemIDs,            fj_SysSetMemIDs,			// 2
	"GROUP_IPADDRS",          fj_SysGetMemIPAddrs,        fj_SysSetMemIPAddrs,		// 3
#define GR_PARAM_LAST    3
#define SYS_FIRST        4
#define SYS_PARAM_FIRST  SYS_FIRST
	"UNITS",                  fj_SysGetUnits,             fj_SysSetUnits,				// 4
	"TIME",                   fj_SysGetTimeDisp,          fj_SysSetTimeDisp,			// 5
	"ENCODER",                fj_SysGetShaftEncoder,      fj_SysSetShaftEncoder,		// 6
	"ENCODER_COUNT",          fj_SysGetEncoderCounts,     fj_SysSetEncoderCounts,		// 7
	"ENCODER_WHEEL",          fj_SysGetEncoderWheel,      fj_SysSetEncoderWheel,		// 8
	"ENCODER_DIVISOR",        fj_SysGetEncoderDivisor,    fj_SysSetEncoderDivisor,		// 9
	"CONVEYOR_SPEED",         fj_SysGetConveyorSpeed,     fj_SysSetConveyorSpeed,		// 10
	"PRINT_RESOLUTION",       fj_SysGetPrintResolution,   fj_SysSetPrintResolution,		// 11
	"INK_SIZE",               fj_SysGetInkBottleSize,     fj_SysSetInkBottleSize,		// 12
	"INK_COST",               fj_SysGetInkBottleCost,     fj_SysSetInkBottleCost,		// 13
#define SYS_PARAM_LAST  13			
#define SYS_TIME_FIRST  14
	"SYSTEM_TIME",            fj_SysGetTime,              fj_SysSetTime,				// 14
#define SYS_ROM_TIME_FIRST  15
	"INTERNAL_CLOCK",         fj_SysGetTimeSource,        fj_SysSetTimeSource,			// 15
	"TIME_ZONE",              fj_SysGetTimeZone,          fj_SysSetTimeZone,			// 16
	"OBSERVE_USA_DST",        fj_SysGetDSTUSA,            fj_SysSetDSTUSA,				// 17
	"FIRST_DAY",              fj_SysGetFirstDay,          fj_SysSetFirstDay,			// 18
	"DAYS_WEEK1",             fj_SysGetWeek1,             fj_SysSetWeek1,				// 19
	"WEEK53",                 fj_SysGetWeek53,            fj_SysSetWeek53,				// 20
	"ROLLOVER_HOURS",         fj_SysGetRollover,          fj_SysSetRollover,			// 21
	"SHIFT1",                 fj_SysGetShift1,            fj_SysSetShift1,				// 22
	"SHIFT2",                 fj_SysGetShift2,            fj_SysSetShift2,				// 23
	"SHIFT3",                 fj_SysGetShift3,            fj_SysSetShift3,				// 24
	"SHIFTCODE1",             fj_SysGetShiftCode1,        fj_SysSetShiftCode1,			// 25
	"SHIFTCODE2",             fj_SysGetShiftCode2,        fj_SysSetShiftCode2,			// 26
	"SHIFTCODE3",             fj_SysGetShiftCode3,        fj_SysSetShiftCode3,			// 27
#define SYS_TIME_LAST   27
#define SYS_DATE_FIRST  28
	"DAYS",                   fj_SysGetDays,              fj_SysSetDays,				// 28
	"MONTHS",                 fj_SysGetMonths,            fj_SysSetMonths,				// 29
	"DAYS_SPECIAL",           fj_SysGetDaysSpecial,       fj_SysSetDaysSpecial,			// 30
	"MONTHS_SPECIAL",         fj_SysGetMonthsSpecial,     fj_SysSetMonthsSpecial,		// 31
	"HOURS",                  fj_SysGetHours,             fj_SysSetHours,				// 32
	"AMPM",                   fj_SysGetAMPM,              fj_SysSetAMPM,				// 33
#define SYS_DATE_LAST   33
#define SYS_INET_FIRST  34
	"IOBOX",                  fj_SysGetIOBox,             fj_SysSetIOBox,				// 34
	"IOBOX_IP",               fj_SysGetIOBOXIP,           fj_SysSetIOBOXIP,				// 35
	"IOBOX_STROBE",           fj_SysGetIOBOXStrobe,       fj_SysSetIOBOXStrobe,			// 36
	"INTERNET_CONTROL",       fj_SysGetInternetControl,   fj_SysSetInternetControl,		// 37
	"DNS_IP",                 fj_SysGetDNSIP,             fj_SysSetDNSIP,				// 38
	"EMAIL_IP",               fj_SysGetEmailIP,           fj_SysSetEmailIP,				// 39
	"EMAIL_SERVER",           fj_SysGetEmailServer,       fj_SysSetEmailServer,			// 40
	"EMAIL_SOURCE",           fj_SysGetEmailAppendage,    fj_SysSetEmailAppendage,		// 41
	"EMAIL_INK",              fj_SysGetEmailInk,          fj_SysSetEmailInk,			// 42
	"EMAIL_SERVICE",          fj_SysGetEmailService,      fj_SysSetEmailService,		// 43
	"WINS_IP",                fj_SysGetWINSIP,            fj_SysSetWINSIP,				// 44
	"NETBIOS_TYPE",           fj_SysGetWINSType,          fj_SysSetWINSType,			// 45
#define SYS_INET_LAST   45
#define SYS_PASS_FIRST  46
	"PASSWORD",               fj_SysGetPasswords,         fj_SysSetPasswords,			// 46
#define SYS_PASS_LAST   46
#define SYS_BC_FIRST    47
	"BC_N",                   fj_SysGetBCIndex,           fj_SysSetBCIndex,				// 47
	"BC_NAME",                fj_SysGetBCName,            fj_SysSetBCName,				// 48
	"BC_TY",                  fj_SysGetBCType,            fj_SysSetBCType,				// 49
	"BC_H",                   fj_SysGetBCHeight,          fj_SysSetBCHeight,			// 50
	"BC_CD",                  fj_SysGetBCCheck,           fj_SysSetBCCheck,				// 51
	"BC_HB",                  fj_SysGetBCBearerH,         fj_SysSetBCBearerH,			// 52
	"BC_VB",                  fj_SysGetBCBearerV,         fj_SysSetBCBearerV,			// 53
	"BC_QZ",                  fj_SysGetBCQuiet,           fj_SysSetBCQuiet,				// 54
	"BC_BASE",                fj_SysGetBCBase,            fj_SysSetBCBase,				// 55
	"BC_B1",                  fj_SysGetBCBleed1,          fj_SysSetBCBleed1,			// 56
	"BC_B2",                  fj_SysGetBCBleed2,          fj_SysSetBCBleed2,			// 57
	"BC_B3",                  fj_SysGetBCBleed3,          fj_SysSetBCBleed3,			// 58
	"BC_B4",                  fj_SysGetBCBleed4,          fj_SysSetBCBleed4,			// 59
#define SYS_BC_LAST				59
	"APP_VER",                fj_SysGetAppVer,            fj_SysSetAppVer, 				// 60
#define SYS_DATAMATRIX_FIRST	61
	"DM_N",						fj_SysGetDataMatrixIndex,		fj_SysSetDataMatrixIndex,	// 61
	"DM_NAME",					fj_SysGetDataMatrixName,		fj_SysSetDataMatrixName,	// 62
	"DM_H",						fj_SysGetDataMatrixBleed1,      fj_SysSetDataMatrixBleed1,	// 63
	"DM_HB",					fj_SysGetDataMatrixBleed2,      fj_SysSetDataMatrixBleed2,	// 64
	"DM_W",						fj_SysGetDataMatrixBleed3,      fj_SysSetDataMatrixBleed3,	// 65
	"DM_WB",					fj_SysGetDataMatrixBleed4,      fj_SysSetDataMatrixBleed4,	// 66
#define SYS_DATAMATRIX_LAST		66
#define SYS_LAST        SYS_DATAMATRIX_LAST
#define PRINT_MODE_FIRST		67
	"PRINT_MODE",             fj_SysGetPrintMode,         fj_SysSetPrintMode,			// 67
	"APS_MODE",               fj_SysGetAPSMode,           fj_SysSetAPSMode,				// 68
#define PRINT_MODE_LAST			68
};
LONG fj_SysParamCount = sizeof(fj_SysParams)/sizeof(struct fjsystable);

//
//  FUNCTION: fj_SystemGroupToParameterString()
//
//  PURPOSE:  make a string with system print head group parameters
//
FJDLLExport VOID fj_SystemGroupToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, GR_PARAM_FIRST, GR_PARAM_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemGroupFromString()
//
//  PURPOSE:  Set parameters in a descriptive string into a FJSYSTEM structure
//
FJDLLExport BOOL fj_SystemGroupFromString( LPFJSYSTEM pfsys, LPSTR pString )
{
	BOOL bRet;

	bRet = fj_SetParametersFromString( (struct fjtable *)fj_SysParams, GR_PARAM_FIRST, GR_PARAM_LAST, (LPVOID)pfsys, pString );

	return( bRet );
}
//
//  FUNCTION: fj_SystemToString()
//
//  PURPOSE:  make a string with system parameters
//
FJDLLExport VOID fj_SystemToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_PARAM_FIRST, SYS_PARAM_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemTimeToString()
//
//  PURPOSE:  make a string with system time parameters
//
FJDLLExport VOID fj_SystemTimeToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_TIME_FIRST, SYS_TIME_LAST, (LPVOID)pfsys, pString );
	return;
}
FJDLLExport VOID fj_SystemRomTimeToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_ROM_TIME_FIRST, SYS_TIME_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemDateStringsToString()
//
//  PURPOSE:  make a string with system date strings string parameters
//
FJDLLExport VOID fj_SystemDateStringsToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_DATE_FIRST, SYS_DATE_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemInternetToString()
//
//  PURPOSE:  make a string with system internet parameters
//
FJDLLExport VOID fj_SystemInternetToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_INET_FIRST, SYS_INET_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemPasswordToString()
//
//  PURPOSE:  make a string with system passwords
//
FJDLLExport VOID fj_SystemPasswordToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_PASS_FIRST, SYS_PASS_LAST, (LPVOID)pfsys, pString );
	return;
}
//
//  FUNCTION: fj_SystemBarCodesToString()
//
//  PURPOSE:  make a string with system barcode info
//
FJDLLExport VOID fj_SystemBarCodesToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	int i;
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		fj_SysBCIndex = i;
		fj_SysBCP = (LPFJSYSBARCODE)&(pfsys->bcArray[i]);
		fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_BC_FIRST, SYS_BC_LAST, (LPVOID)pfsys, pString );
		pString += strlen( pString );
	}
	return;
}

FJDLLExport VOID fj_SystemDataMatrixToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	int i;
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		fj_SysDMIndex = i;
		fj_SysDMP = (LPFJSYSDATAMATRIX)&(pfsys->bcDataMatrixArray[i]);
		fj_MakeParameterString( (struct fjtable *)fj_SysParams, SYS_DATAMATRIX_FIRST, SYS_DATAMATRIX_LAST, (LPVOID)pfsys, pString );
		pString += strlen( pString );
	}
	return;
}
//
//  FUNCTION: fj_SystemFromString()
//
//  PURPOSE:  Set parameters in a descriptive string into a FJSYSTEM structure
//
FJDLLExport BOOL fj_SystemFromString( LPFJSYSTEM pfsys, LPSTR pString )
{
	BOOL bRet;

	bRet = fj_SetParametersFromString( (struct fjtable *)fj_SysParams, SYS_FIRST, SYS_LAST, (LPVOID)pfsys, pString );

#ifndef _WINDOWS  // Burl 1.2009
	// check SE params
	{
		LONG GAver = (LONG)(pCVT->pFJglobal->gaRead.rw.read.version);
		BYTE GA_Mver = ((GAver>>4)& 0x0F);
		BYTE GA_Lver = (GAver & 0x0F);
		LPFJSYSTEM pfsys = pCVT->pfsys;
		if ( 1 <= GA_Mver )
		{
			if ( 1 == GA_Mver && 3 > GA_Lver)
				pfsys->fEncoder = pfsys->fEncoderWheel / pfsys->lEncoderDivisor;
			else
				pfsys->fEncoder = pfsys->fEncoderWheel * 2 / pfsys->lEncoderDivisor;
		}
		else
			pfsys->fEncoder = pfsys->fEncoderWheel / pfsys->lEncoderDivisor;
	}
#endif //_WINDOWS // Burl

	return( bRet );
}
FJDLLExport VOID fj_PrintModeToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	fj_MakeParameterString( (struct fjtable *)fj_SysParams, PRINT_MODE_FIRST, PRINT_MODE_LAST, (LPVOID)pfsys, pString );
	return;
}
FJDLLExport BOOL fj_PrintModeFromString( LPFJSYSTEM pfsys, LPSTR pString )
{
	BOOL bRet;

	bRet = fj_SetParametersFromString( (struct fjtable *)fj_SysParams, PRINT_MODE_FIRST, PRINT_MODE_LAST, (LPVOID)pfsys, pString );

	return( bRet );
}
#ifndef _WINDOWS // Burl 1.2009
FJDLLExport VOID fj_StatusModeToString( CLPCFJSYSTEM pfsys, LPSTR pString )
{
	CLPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;

	strcpy(pString,"STATUS_MODE=");
	if (TRUE == pGlobal->bStatusReport)
	{
		strcat(pString,"ON;");
	}
	else
	{
		strcat(pString,"OFF;");
	}
	return;
}
FJDLLExport BOOL fj_StatusModeFromString( LPFJSYSTEM pfsys, LPSTR pString )
{
	CLPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	LPSTR pList[1];
	LPSTR pList2[2];
	LONG lCount;
	LONG lCount2;
	int  j;
	BOOL bRet;

	bRet = FALSE;
	lCount = fj_ParseKeywordsFromStr( pString, ';', pList, 1 );
	if (1 == lCount)
	{
		lCount2 = fj_ParseKeywordsFromStr( pList[0], '=', pList2, 2 );
		if ( 2 == lCount2 )
		{
		    if (0 == strcmp(pList2[0],"STATUS_MODE"))
		    {
			    
			if ( 0 == strcmp(pList2[1],"ON"))
			{
				pGlobal->bStatusReport = TRUE;
				bRet = TRUE;
			}
			else if ( 0 == strcmp(pList2[1],"OFF"))
			{
			    fj_bKeepAlive = FALSE;
			    pGlobal->bStatusReport = FALSE;
			    bRet = TRUE;
			}
			else if ( 0 == strcmp(pList2[1],"KEEP_ALIVE"))
			{
			    pCVT->fj_SystemGetTime(&fj_tmKeepAlive);
			    fj_bKeepAlive = TRUE;
			    bRet = TRUE;
			}
		    }
		}
	}

	return( bRet );
}
#endif //_WINDOWS // Burl 1.2009

//
//  FUNCTION: fj_SystemBarCodeInitEntry()
//
//  PURPOSE:  Initialize a FJSYSBARCODE structure
//
FJDLLExport VOID fj_SystemBarCodeInitEntry( LPFJSYSBARCODE pfsysbc )
{
	pfsysbc->strName[0]  = 0;			// name of this entry
	pfsysbc->lBCFlags    = BARCODE_I25;	// all necessary flags for BC Lib
	pfsysbc->lBCHeight   = 32;			// Height of barcode image
	pfsysbc->bCheckDigit = TRUE;		// TRUE to add check digit tot he barcode
	pfsysbc->lHor_Bearer = 2;			// Horizontal bearer value
	pfsysbc->lVer_Bearer = 33;			// Vertical bearer value
	pfsysbc->lQuietZone  = 99;			// QuietZone value
	pfsysbc->lBarVal[0]  = 31;			// base bar width
	pfsysbc->lBarVal[1]  = 1;			// bleed width
	pfsysbc->lBarVal[2]  = 2;			// bleed width
	pfsysbc->lBarVal[3]  = 3;			// bleed width
	pfsysbc->lBarVal[4]  = 4;			// bleed width
	return;
}

FJDLLExport VOID fj_SystemDataMatrixInitEntry( LPFJSYSDATAMATRIX pfsysbc )
{
	pfsysbc->strName[0]		= 0;
	pfsysbc->lHeight		= 1;
	pfsysbc->lHeightBold	= 12;
	pfsysbc->lWidth			= 1;
	pfsysbc->lWidthBold		= 40;
	return;
}

//
//  FUNCTION: fj_SystemInit()
//
//  PURPOSE:  Initialize a FJSYSTEM structure
//
FJDLLExport VOID fj_SystemInit( LPFJSYSTEM pfsys )
{
	int i;
	memset( pfsys, 0, sizeof(FJSYSTEM) );

										// measurement system for displaying info
	pfsys->lUnits           = FJSYS_UNITS_ENGLISH_INCHES;
										// format for displaying time
	pfsys->lTimeDisplay     = FJSYS_TIMEDIS_24;
	pfsys->bShaftEncoder    = TRUE;		// shaft encoder present
	pfsys->lEncoderDivisor  = 1;		// encoder divisor to get effective encoder rate
										// encoder pulse speed - counts per inch
	pfsys->fEncoder         = (FLOAT)300;
										// encoder wheel pulse speed - counts per inch
	pfsys->fEncoderWheel    = (FLOAT)300;
	pfsys->fSpeed           = 60.0;		// no encoder - conveyor speed - inches per second
	pfsys->fPrintResolution = 150.0;	// no encoder - resolution     - dots per inch
	pfsys->fInkBottleSize   = 500.0;	// bottle volume - milliliters only
	pfsys->fInkBottleCost   = 230.0;	// bottle cost

	pfsys->bInternalClock   = TRUE;		// TRUE = use internal clock. FALSE = use external TOD clock.
	pfsys->lTimeZoneSeconds = -6*60*60;	// difference in hours between UTC and local time
	pfsys->bDSTUSA          = FALSE;	// TRUE = observe USA DST rules. FALSE = ignore DST.
	pfsys->lAddrUDP13       = 0;		// suggested address for UDP 13 tod clock
	pfsys->lAddrTCP13       = 0;		// suggested address for TCP 13 tod clock
	pfsys->lAddrIP          = 0;		// suggested address for IP tod clock
	pfsys->lAddrWINS        = 0;		// ip address for WINS NETBIOS server
										// type of NETBIOS name service
	pfsys->sNETBIOSService  = FJSYS_NETBIOS_NONE;

	pfsys->lActiveCount = 0;			// number of active print heads in group
	pfsys->pfl          = NULL;			// pointer to FJLABEL structure
	pfsys->pflScanner   = NULL;			// pointer to default scanner FJLABEL structure
	for ( i = 0; i < FJSYS_NUMBER_PRINTERS; i++ )
	{
		fj_SystemGroupMemInit( &(pfsys->gmMembers[i]) );
	}
	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		fj_SystemBarCodeInitEntry( &(pfsys->bcArray[i]) );
	}
	strcpy( pfsys->bcArray[0].strName, "Mag 100" );
	strcpy( pfsys->bcArray[1].strName, "Mag  90" );
	strcpy( pfsys->bcArray[2].strName, "Mag  80" );
	strcpy( pfsys->bcArray[3].strName, "Mag  70" );
	strcpy( pfsys->bcArray[4].strName, "Mag  62" );
	for ( i = 5; i < FJSYS_BARCODES; i++ )
	{
		strcpy( pfsys->bcArray[i].strName, "Custom " );
		sprintf( &(pfsys->bcArray[i].strName[7]), "%i", (int)(i-4) );
	}

	for ( i = 0; i < FJSYS_BARCODES; i++ )
	{
		fj_SystemDataMatrixInitEntry( &(pfsys->bcDataMatrixArray[i]) );
	}
	strcpy( pfsys->bcDataMatrixArray[0].strName, "Mag 100" );
	strcpy( pfsys->bcDataMatrixArray[1].strName, "Mag  90" );
	strcpy( pfsys->bcDataMatrixArray[2].strName, "Mag  80" );
	strcpy( pfsys->bcDataMatrixArray[3].strName, "Mag  70" );
	strcpy( pfsys->bcDataMatrixArray[4].strName, "Mag  62" );
	for ( i = 5; i < FJSYS_BARCODES; i++ )
	{
		strcpy( pfsys->bcDataMatrixArray[i].strName, "Custom " );
		sprintf( &(pfsys->bcDataMatrixArray[i].strName[7]), "%i", (int)(i-4) );
	}

	pfsys->fRolloverHours   = 0;		// Datetime - rollover time in hours
	pfsys->lWeekDay         = 1;		// Datetime - first day of week
	pfsys->lWeek1           = 4;		// Datetime - number of January days in first week
	pfsys->lWeek53          = 53;		// Datetime - value to use for week 53
	pfsys->lShift1          = 0;		// Datetime - value for start of shift 1 - in minutes
	pfsys->lShift2          = 0;		// Datetime - value for start of shift 2 - in minutes
	pfsys->lShift3          = 0;		// Datetime - value for start of shift 3 - in minutes
	pfsys->sShiftCode1[0]   = 0;		// Datetime - shift 1 string
	pfsys->sShiftCode2[0]   = 0;		// Datetime - shift 2 string
	pfsys->sShiftCode3[0]   = 0;		// Datetime - shift 3 string
	// Datetime - array of strings for days - normal
	memcpy(pfsys->aDayN  ,fj_DayN,   7*(1+FJDATETIME_STRING_SIZE));
	// Datetime - array of strings for days - special
	memcpy(pfsys->aDayS  ,fj_DayS,   7*(1+FJDATETIME_STRING_SIZE));
	// Datetime - array of strings for months - normal
	memcpy(pfsys->aMonthN,fj_MonthN,12*(1+FJDATETIME_STRING_SIZE));
	// Datetime - array of strings for months - special
	memcpy(pfsys->aMonthS,fj_MonthS,12*(1+FJDATETIME_STRING_SIZE));
	// Datetime - array of strings for AM and PM
	memcpy(pfsys->aAMPM  ,fj_AMPM,   2*(1+FJDATETIME_STRING_SIZE));
	// Datetime - array of strings for alphabetic hours
	memcpy(pfsys->aHour  ,fj_HourA, 24*(1+FJDATETIME_STRING_SIZE));

	pfsys->alPasswordLevel[0] = FJSYS_PRIORITY_VIEW_ALL;
	pfsys->alPasswordLevel[1] = FJSYS_PRIORITY_MESSAGE_SELECT;
	pfsys->alPasswordLevel[2] = FJSYS_PRIORITY_TEST;
	pfsys->alPasswordLevel[3] = FJSYS_PRIORITY_COUNTERS;
	pfsys->alPasswordLevel[4] = FJSYS_PRIORITY_MESSAGE_EDIT;
	pfsys->alPasswordLevel[5] = FJSYS_PRIORITY_RESTORE;
	pfsys->alPasswordLevel[6] = FJSYS_PRIORITY_CONFIGURATION;
	pfsys->alPasswordLevel[7] = FJSYS_PRIORITY_SECURITY;
	pfsys->alPasswordLevel[8] = FJSYS_PRIORITY_DISTRIBUTOR;
	pfsys->alPasswordLevel[9] = FJSYS_PRIORITY_MANUFACTURING;
	for ( i = 0; i < FJSYS_PRIORITY_LEVELS; i++ )
	{
		pfsys->aPassword[i][0]    = 0;
	}
										// type of internet control
	pfsys->cInternetControl = FJSYS_INTERNET_FULL;
	pfsys->lIOBoxIP         = 0;		// IP address for IOBox
	pfsys->bIOBoxUse        = FALSE;	// IOBox present
	pfsys->iIOBoxStrobe = 1;
	return;
}
//
//  FUNCTION: fj_SystemNew()
//
//  PURPOSE:  Create a new FJSYSTEM structure
//
FJDLLExport LPFJSYSTEM fj_SystemNew( VOID )
{
	LPFJSYSTEM pfsys;

	pfsys = (LPFJSYSTEM)fj_malloc( sizeof(FJSYSTEM) );
	fj_SystemInit( pfsys );

	return( pfsys );
}
//
//  FUNCTION: fj_SystemDestroy()
//
//  PURPOSE:  Destroy a FJSYSTEM structure
//
FJDLLExport VOID fj_SystemDestroy( LPFJSYSTEM pfsys )
{
	int i;

	if ( NULL != pfsys-> pfl        ) fj_LabelDestroy( pfsys->pfl );
	if ( NULL != pfsys-> pflScanner ) fj_LabelDestroy( pfsys->pflScanner );
	for ( i = 0; i < FJSYS_NUMBER_PRINTERS; i++ )
	{
		fj_SystemGroupMemClose( &(pfsys->gmMembers[i]) );
	}
	fj_free( pfsys );
	return;
}
#ifndef _WINDOWS

//
//  FUNCTION: fj_SystemBuildSelectedLabel()
//
//  PURPOSE:  Build a label
//
//  NOTE: if the name is null or cannot be found, the old selected label will be destroyed,
//             and there will be no selected label.
//
FJDLLExport LPFJLABEL fj_SystemBuildSelectedLabel( LPFJSYSTEM pfsys, LPCSTR pName )
{
	CLPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJPRINTHEAD pfph;
	LPFJLABEL     pfl;
	LPFJLABEL     pflOld;
	LPSTR         pMsgName;
	STR  sLabelName[FJLABEL_NAME_SIZE*2];
	int  i;
	int  x;
	LPFJMESSAGE pfm,pfm2;
	LPFJELEMENT pfe,pfe2;
	LPFJCOUNTER pfc,pfc2;

	pfl = NULL;
	// save current label
	pflOld   = pfsys->pfl;
	// Burl v 1.1015 // pBRAM->lSelectedCount = 0;

	if ( NULL != pName )
	{
		// build new label, message and fonts
		pfph = pfsys->gmMembers[0].pfph;
		fj_processParameterStringOutput( sLabelName, pName );
		pfl = fj_LabelBuildFromName( pfph->pMemStor, sLabelName );
	}

	// update to new one
	pfsys->pfl = pfl;

	for ( i = 0; i < FJSYS_NUMBER_PRINTERS; i++ )
	{
		pfph = pfsys->gmMembers[i].pfph;
		if ( NULL != pfph )
		{
			pMsgName = NULL;
			if ( NULL != pfl ) pMsgName = pfl->fmMessageIDs[pfph->lGroupNumber];
			fj_PrintHeadBuildSelectedMessage( pfph, pfl, pMsgName );

			if (0 == strcmp(pfph->pfmSelected->strName,pfph->pfmSelected2->strName))
			{
				x = 0;
				pfm  = pfph->pfmSelected;
				pfm2 = pfph->pfmSelected2;
				if ( (NULL != pfm) && (NULL != pfm2))
				{
					while ( (x < pfm->lCount) &&  (x < pfm2->lCount))
					{
						pfe  = pfm->papfe[x];
						pfe2 = pfm2->papfe[x];
						if ( (FJ_TYPE_COUNTER == pfe->pActions->fj_DescGetType()) && (FJ_TYPE_COUNTER == pfe2->pActions->fj_DescGetType()) )
						{
							pfc = (LPFJCOUNTER)pfe->pDesc;
							pfc2 = (LPFJCOUNTER)pfe2->pDesc;
							pfc->lCurValue = pfc2->lCurValue;
						}
						x++;
					}
				}
			}
		}
	}

	// destroy old label
	if ( NULL != pflOld )
	{
		fj_LabelDestroy( pflOld );
	}
	if ( NULL == pfl ) pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
	else if (FALSE == pBRAM->bPrintIdle)    pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
	else pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
	pCVT->fj_print_head_SendPhotocellMode();

	return( pfl );
}
#endif									//!_WINDOWS

//
//  FUNCTION: fj_SystemBuildScannerDefaultLabel()
//
//  PURPOSE:  Build a label
//
FJDLLExport LPFJLABEL fj_SystemBuildScannerDefaultLabel( LPFJSYSTEM pfsys, LPCSTR pName )
{
	LPFJPRINTHEAD pfph;
	LPFJLABEL     pfl;
	LPFJLABEL     pflOld;
	LPSTR         pMsgName;
	STR  sLabelName[FJLABEL_NAME_SIZE*2];
	int  i;

	pfl = NULL;
	// save current label
	pflOld   = pfsys->pflScanner;

	if ( NULL != pName )
	{
		// build new label, message and fonts
		pfph = pfsys->gmMembers[0].pfph;
		fj_processParameterStringOutput( sLabelName, pName );
		pfl = fj_LabelBuildFromName( pfph->pMemStor, sLabelName );
	}

	// update to new one
	pfsys->pflScanner = pfl;

	for ( i = 0; i < FJSYS_NUMBER_PRINTERS; i++ )
	{
		pfph = pfsys->gmMembers[i].pfph;
		if ( NULL != pfph )
		{
			pMsgName = NULL;
			if ( NULL != pfl ) pMsgName = pfl->fmMessageIDs[pfph->lGroupNumber];
			fj_PrintHeadBuildScannerDefaultMessage( pfph, pfl, pMsgName );
		}
	}

	// destroy old label
	if ( NULL != pflOld )
	{
		fj_LabelDestroy( pflOld );
	}
	return( pfl );
}
//
//  FUNCTION: fj_SystemCopyScannerDefaultLabelToSelectedLabel()
//
//  PURPOSE:  Copy the pre-built Scanner Default Label to the Selected Label
//
//
FJDLLExport LPFJLABEL fj_SystemCopyScannerDefaultLabelToSelectedLabel( LPFJSYSTEM pfsys )
{
	LPFJPRINTHEAD pfph;
	LPFJLABEL     pfl;
	LPFJLABEL     pflOld;
	LPFJMESSAGE   pfm;
	LPFJMESSAGE   pfmOld;
	int  i;

	pfl = NULL;
	// save current label
	pflOld   = pfsys->pfl;

	pfl = fj_LabelDup( pfsys->pflScanner );

	// update to new one
	pfsys->pfl = pfl;

	for ( i = 0; i < FJSYS_NUMBER_PRINTERS; i++ )
	{
		pfph = pfsys->gmMembers[i].pfph;
		if ( NULL != pfph )
		{
			pfmOld = pfph->pfmSelected;
			pfm = fj_MessageDup( pfph->pfmScanner );
			pfph->pfmSelected = pfm;
			fj_MessageDestroy( pfmOld );
		}
	}

	// destroy old label
	if ( NULL != pflOld )
	{
		fj_LabelDestroy( pflOld );
	}
	return( pfl );
}
//
//  FUNCTION: fj_SystemGetPasswordForLevel()
//
//  PURPOSE:  return pointer to password for given level
//
FJDLLExport LPCSTR fj_SystemGetPasswordForLevel( CLPCFJSYSTEM pfsys, LONG lLevel )
{
	LPCSTR pRet;
	int  i;

	pRet = NULL;
	for ( i = 0; i < FJSYS_PRIORITY_LEVELS; i++ )
	{
		if ( lLevel == pfsys->alPasswordLevel[i] )
		{
			pRet = pfsys->aPassword[i];
			break;
		}
	}

	return( pRet );
}
//
//  FUNCTION: fj_SystemGetPasswordLevel()
//
//  PURPOSE:  return password level
//
//  return value is highest level found without a password, or
//                                      with a matching password
//
FJDLLExport LONG fj_SystemGetPasswordLevel( CLPCFJSYSTEM pfsys, LPCSTR pPassword )
{
	struct tm TM;
	char sSecret[10];
	LONG lSecret;
	LONG lLevel;
	int  i;

	lLevel = 0;
	// find default level - but not MANUFACTURING
	for ( i = 0; i < FJSYS_PRIORITY_LEVELS-1; i++ )
	{
										// if not defined password,
		if ( 0 == pfsys->aPassword[i][0] )
		{
										// set highest level found without password
			lLevel = pfsys->alPasswordLevel[i];
		}
		else
		{
			break;
		}
	}
	if ( 0 != *pPassword )				// if a password was entered,
	{
										// check remaining levels for matching password
		for ( ; i < FJSYS_PRIORITY_LEVELS; i++ )
		{
			if ( 0 == strcmp(pfsys->aPassword[i],pPassword) )
			{
										// set highest level found with matching password
				lLevel = pfsys->alPasswordLevel[i];
			}
		}
		// make secret MANUFACTURING password from date
		fj_SystemGetTime( &TM );
		lSecret  = TM.tm_wday + 2;		//       1 + day of week. tm_wday is 0 origin and we want Sunday = 1;
		lSecret *= TM.tm_mday + 1;		// times 1 + the day of the month
		lSecret *= TM.tm_mon  + 2;		// times 1 + the month of the year. tm_mon is 0 origin and we want January = 1
		lSecret *= (TM.tm_year%10) +1;	// times 1 + the last digit of the year
		sprintf( sSecret, "%i", (int)lSecret );
		if ( 0 == strcmp(sSecret, pPassword) ) lLevel = FJSYS_PRIORITY_MANUFACTURING;
	}
	//lLevel = FJSYS_PRIORITY_MANUFACTURING; // temp for testing

	return( lLevel );
}
//
//  FUNCTION: fj_SystemMakeLevelID()
//
//  PURPOSE:  return level id for authentication
//
FJDLLExport VOID fj_SystemMakeLevelID( CLPCFJPRINTHEAD pfph, LONG lLevel, ULONG lIPaddr, LPSTR pAuth )
{
	struct tm TM;
	struct tm *pTest;
	ULONG  lValue;
	ULONG  lQuot;
	ULONG  lRemainder;

	lValue = 256*4 - (lIPaddr&0xff) - ((lIPaddr&0xff00)>>8) - ((lIPaddr&0xff0000)>>16) - ((lIPaddr&0xff000000)>>24);
	lValue *= (ULONG)lLevel;
	pTest = fj_SystemGetTime( &TM );
	if ( NULL != pTest )
	{
		lValue *= (ULONG)(TM.tm_mday+1);
		lValue *= (ULONG)(TM.tm_wday+1);
	}
	lQuot = pfph->lSerial / 113;
										// serial number modulo a prime
	lRemainder = pfph->lSerial - (lQuot*113);
	lValue *= lRemainder + 1;
	while ( 0 != lValue )
	{
		lQuot = lValue / 26;
		lRemainder = lValue - (lQuot*26);
		*pAuth = 'A' + (UCHAR)lRemainder;
		pAuth++;
		lValue = lQuot;
	}
	*pAuth = 0;
	return;
}
//
//  FUNCTION: fj_SystemGetLevelID()
//
//  PURPOSE:  extract level id from pwid authentication
//
FJDLLExport LONG fj_SystemGetLevelID( LPFJPRINTHEAD pfph, ULONG lIPaddr, LPSTR pAuth )
{
	struct tm TM;
	struct tm *pTest;
	LPSTR  pAuthEnd;
	ULONG  lQuot;
	ULONG  lRemainder;
	ULONG  lIn;
	ULONG  lValue;
	LONG   lLevel;

	lLevel = 0;
	pAuthEnd = pAuth;

	while ( TRUE == isdigit(*pAuth) ) pAuth++;
	pAuthEnd = pAuth + strlen(pAuth) - 1;
	pAuth--;
	lIn = 0;
	while ( pAuthEnd > pAuth )
	{
		lIn = lIn*26 + (*pAuthEnd-'A');
		pAuthEnd--;
	}

	lValue = 256*4 - (lIPaddr&0xff) - ((lIPaddr&0xff00)>>8) - ((lIPaddr&0xff0000)>>16) - ((lIPaddr&0xff000000)>>24);
	pTest = fj_SystemGetTime( &TM );
	if ( NULL != pTest )
	{
		lValue *= (ULONG)(TM.tm_mday+1);
		lValue *= (ULONG)(TM.tm_wday+1);
	}
	lQuot = pfph->lSerial / 113;
										// serial number modulo a prime
	lRemainder = pfph->lSerial - (lQuot*113);
	lValue *= lRemainder + 1;
										// it shouldn't happen - but just in case, don't divide by zero
	if ( 0 != lValue )lLevel = lIn / lValue;
										// if a remainder, auth is invalid, return 0
	if ( lIn != lValue * (ULONG)lLevel ) lLevel = 0;

	return( lLevel );
}
// some utility routines to return time and date
FJDLLExport struct tm * fj_SystemGetTime( struct tm *pTM )
{
	struct tm *pRet = pTM;
#ifdef NET_OS
	pRet = fj_print_head_GetRTClockTM( pTM );
#else
	{
		time_t t;
		struct tm *pTMlocal;
		time( &t );
		//		pTMlocal = gmtime( &t );
		pTMlocal = localtime( &t );
		memcpy( pTM, pTMlocal, sizeof(struct tm) );
	}
#endif
	return( pRet );
}
void fj_SystemFixTimeString( LPSTR pString )
{
	if ( ('0' == *pString) && ('/' == *(pString+1)) ) *(pString+1) = '0';
	return;
}
FJDLLExport LPSTR fj_SystemGetDateTimeFullString( CLPCFJSYSTEM pfsys, LPSTR pString, struct tm *pTM )
{
	CHAR sT[30];

	sprintf( pString, "%.3s, %.2d %.3s %.4d %s", fj_apWebDays[pTM->tm_wday], (int)pTM->tm_mday, fj_apWebMonths[pTM->tm_mon], (int)(pTM->tm_year + 1900), fj_SystemGetTimeString( pfsys, sT, pTM ) );
	//    fj_SystemGetTime24String( sT, pTM );
	//	fj_SystemGetTimeString( pfsys, sT, pTM );
	//    strcat( pString, sT );

	return( pString );
}
FJDLLExport LPSTR fj_SystemGetDateTimeUnixFileString( CLPCFJSYSTEM pfsys, LPSTR pString, struct tm *pTM )
{
	CHAR sT[30];

	sprintf( pString, "%.3s %.2d ", fj_apWebMonths[pTM->tm_mon], (int)(pTM->tm_mday) );
	fj_SystemGetTime24String( sT, pTM );
	sT[5] = 0;
	strcat( pString, sT );

	return( pString );
}
// yyyy/mm/dd hh:mm:ss
FJDLLExport LPSTR  fj_SystemGetDateTimeString( LPSTR pString, const struct tm * const pTM )
{
	fj_SystemGetDateString( pString, pTM );
	strcat( pString, " " );
	fj_SystemGetTime24String( (pString+strlen(pString)), pTM );

	return( pString );
}
// yyyy/mm/dd
FJDLLExport LPSTR fj_SystemGetDateString( LPSTR pString, const struct tm * const pTM )
{
	strftime( pString, 11, "%Y/%m/%d", (struct tm *)pTM );

	return( pString );
}
// hh:mm:ss
FJDLLExport LPSTR fj_SystemGetTime12String( LPSTR pString, const struct tm * const pTM )
{
	strftime( pString, 9, "%I:%M:%S", (struct tm *)pTM );
	fj_SystemFixTimeString( pString );

	return( pString );
}
// hh:mm:ss am
FJDLLExport LPSTR fj_SystemGetTime12AMPMString( LPSTR pString, const struct tm * const pTM )
{
	strftime( pString, 14, "%I:%M:%S %p", (struct tm *)pTM );
	fj_SystemFixTimeString( pString );

	return( pString );
}
// hh:mm:ss
FJDLLExport LPSTR fj_SystemGetTime24String( LPSTR pString, const struct tm * const pTM )
{
	strftime( pString, 9, "%H:%M:%S", (struct tm *)pTM );
	fj_SystemFixTimeString( pString );

	return( pString );
}
FJDLLExport LPSTR fj_SystemGetAMPMString( LPSTR pString, const struct tm * const pTM )
{
	strftime( pString, 5, "%p", (struct tm *)pTM );

	return( pString );
}
// hh:mm:ss am
FJDLLExport LPSTR fj_SystemGetTimeString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM )
{
	if      ( FJSYS_TIMEDIS_12M == pfsys->lTimeDisplay ) fj_SystemGetTime12AMPMString( pString, pTM );
	else if ( FJSYS_TIMEDIS_12  == pfsys->lTimeDisplay ) fj_SystemGetTime12String    ( pString, pTM );
	else                                                 fj_SystemGetTime24String    ( pString, pTM );
	return( pString );
}
// hh:mm am
FJDLLExport LPSTR fj_SystemGetTimeNoSecString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM )
{
	fj_SystemGetTimeString( pfsys, pString, pTM );
	strcpy( pString+5, pString+8);		// move what's after the seconds to overlay the seconds
	return( pString );
}
// return full name of month
FJDLLExport LPSTR fj_SystemGetMonthString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM )
{
	strcpy( pString, fj_apWebMonths[pTM->tm_mon] );

	return( pString );
}
// return full name of day of week
FJDLLExport LPSTR fj_SystemGetDayString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM )
{
	strcpy( pString, fj_apWebDays[pTM->tm_wday] );

	return( pString );
}
// return full name of day of week plus the day of month
FJDLLExport LPSTR fj_SystemGetDayDateString( CLPCFJSYSTEM pfsys, LPSTR pString, const struct tm * const pTM )
{
	sprintf( pString, "%s %i", fj_apWebDays[pTM->tm_wday], pTM->tm_mday );

	return( pString );
}
//
//  parse a string with the date and/or time in it.
//  the routine tries real hard to make sense out of the input.
//
//  the date and time must be separated by at least one blank.
//  if there is only a date, the time will be 00:00:00.
//  if there is only a time, the current date will be used.
//
//  either the date or time may be first.
//  the general format is yyyy/mm/dd hh:mm:ss.
//  a 4 digit year may be at either end.
//      if year is 2 digit, it must be first,
//           unless the 3rd part is 0 or greater than 31, then a 2 digit year at the end is unamiguous.
//
//  there must be a ':' to indicate a time. seconds are optional.
//  there must be a '/' or a '-' to indicate a date.
//
//  if nothing can be parsed, NULL is returned.
//
FJDLLExport struct tm * fj_SystemParseDateTime( LPSTR pStr, struct tm *pTM )
{

	struct tm *pTMRet = pTM;
	int    d1 = 0;
	int    d2 = 0;
	int    d3 = 0;
	BOOL   bymd;
	BOOL   bmd;
	LPSTR  p1;
	LPSTR  p2;
	LPSTR  p3;
	LPSTR  p4;
	LPSTR  pDate = NULL;
	LPSTR  pTime = NULL;

	fj_SystemGetTime( pTM );			// if no date or time given, use current date/time
	pTM->tm_sec   = 0;
	pTM->tm_min   = 0;
	pTM->tm_hour  = 0;
	p1 = strchr( pStr, ' ' );			// find a blank separator
	if ( NULL != p1 )					// p1 points to the second string
	{
		*p1 = 0;
		p1++;
		while ( ' ' == *p1 ) p1++;		// skip over all blanks
		p2 = strchr( p1, ':' );
		if ( NULL != p2 )
		{
			pTime = p1;
			p3 = strchr( pStr, '/' );
			p4 = strchr( pStr, '-' );
			if ( (NULL != p3) || (NULL != p4) ) pDate = pStr;
		}
		else
		{
			p2 = strchr( pStr, ':' );
			if ( NULL != p2 )
			{
				pTime = pStr;
				p3 = strchr( p1, '/' );
				p4 = strchr( p1, '-' );
				if ( (NULL != p3) || (NULL != p4) ) pDate = p1;
			}
		}
	}
	else
	{
		p2 = strchr( pStr, ':' );
		p3 = strchr( pStr, '/' );
		p4 = strchr( pStr, '-' );
		if      ( NULL != p2 ) pTime = pStr;
		else if ( NULL != p3 ) pDate = pStr;
		else if ( NULL != p4 ) pDate = pStr;
	}
	if ( NULL != pDate )
	{
		p1 = pDate;
		p2 = strchr( p1, '/' );
		if ( NULL == p2 ) p2 = strchr( p1, '-' );
		if ( NULL != p2 )
		{
			*p2 = 0;
			p2++;
			p3 = strchr( p2, '/' );
			if ( NULL == p3 ) p3 = strchr( p2, '-' );
			if ( NULL != p3 )
			{
				*p3 = 0;
				p3++;
				d3 = atoi(p3);
			}
			d2 = atoi(p2);
		}
		d1 = atoi(p1);
		if ( NULL != p3 )				// 3 segment date
		{
			bymd = TRUE;				// assume y/m/d
			if ( (31  < d3) ||			// if last is greater than 31, must be year
				( 0 == d3) )			// if last is 0, must be year
				bymd = FALSE;
			if ( TRUE == bymd )
			{
				// d1 value must be 1900 origin
										// adjust full 4 digit year
				if ( d1 > 100 ) d1 -= 1900;
										// adjust 2 digit year to 2000+
				if ( d1 < 100 ) d1 += 100;
				pTM->tm_year = d1;		// set the year
				pTM->tm_mon  = d2-1;	// set the month - 0 origin
				pTM->tm_mday = d3;		// set the day of month
			}
			else
			{
				// d3 value must be 1900 origin
										// adjust full 4 digit year
				if ( d3 > 100 ) d3 -= 1900;
										// adjust 2 digit year to 2000+
				if ( d1 < 100 ) d1 += 100;
				pTM->tm_year = d3;		// set the year
				bmd = TRUE;				// assume m/d
				if ( 12 < d2 )			// if second is greater than 12, must be day
					bmd = FALSE;
				if ( TRUE == bmd )
				{
					pTM->tm_mon  = d2-1;// set the month - 0 origin
					pTM->tm_mday = d1;	// set the day of month
				}
				else
				{
					pTM->tm_mon  = d1-1;// set the month - 0 origin
					pTM->tm_mday = d2;	// set the day of month
				}
			}
		}
		else if ( NULL != p2 )			// 2 segment date - assume mm/dd with current yyyy
		{
			pTM->tm_mon  = d1-1;		// set the month - 0 origin
			pTM->tm_mday = d2;			// set the day of month
		}
		else							// 1 segment date - assume dd with curent yyyy/mm
		{
			pTM->tm_mday = d1;			// set the day of month
		}
	}
	if ( NULL != pTime )
	{
		p1 = pTime;
		p2 = strchr( p1, ':' );
		if ( NULL != p2 )
		{
			*p2 = 0;
			p2++;
			p3 = strchr( p2, ':' );
			if ( NULL != p3 )
			{
				p3++;
				pTM->tm_sec = atoi(p3);
			}
			pTM->tm_min = atoi(p2);
		}
		pTM->tm_hour = atoi(p1);
	}
	if ( (NULL == pDate) && (NULL == pTime) ) pTMRet = NULL;
	return( pTMRet );
}

#ifndef _WINDOWS // Burl 1.2009
FJDLLExport VOID fj_SaveBRAMCounter( const struct fjelement * pfe )
{
	LPFJ_BRAM pBRAM = pCVT->pBRAM;
	CLPFJCOUNTER pfc = (CLPFJCOUNTER)pfe->pDesc;
	LPBRAMCOUNTER pLabelBRAMCount;
	BOOL bFound = FALSE;
	int i;

	if (FJMESSAGE_NUMBER_SEGMENTS > pBRAM->BRAMcountElm)
	{
		for ( i = 0; i < pBRAM->BRAMcountElm; i++ )
		{
			pLabelBRAMCount = &(pBRAM->LabelBRAMCounters[i]);
			if (0 == strcmp(pLabelBRAMCount->strElmCounter, pfe->strName))
			{
				pLabelBRAMCount->lCurValue = pfc->lCurValue;
				bFound = TRUE;
				break;
			}
		}

		if ( FALSE == bFound )
		{
			pLabelBRAMCount = &(pBRAM->LabelBRAMCounters[pBRAM->BRAMcountElm]);
			strcpy( pLabelBRAMCount->strElmCounter, pfe->strName );
			pLabelBRAMCount->lCurValue = pfc->lCurValue;
			pBRAM->BRAMcountElm++;
		}
	}
}

FJDLLExport VOID fj_ResetBRAMCounters( struct fj_bram * pBRAM )
{
	int i;
	for ( i = 0; i < FJMESSAGE_NUMBER_SEGMENTS; i++ )
	{
		LPBRAMCOUNTER pLabelBRAMCount = &(pBRAM->LabelBRAMCounters[i]);
		pLabelBRAMCount->lCurValue = 0;
		pLabelBRAMCount->strElmCounter[0] = 0;
	}
	pBRAM->BRAMcountElm = 0;
}

FJDLLExport VOID fj_RestoreBRAMCounters( struct fjmessage * pfm )
{
	LPFJ_BRAM     pBRAM   = pCVT->pBRAM;
	LPFJCOUNTER pfc;
	LPCFJELEMENT pfe;
	LPBRAMCOUNTER pLabelBRAMCount;
	int i, n;

	for ( i = 0; i < pfm->lCount; i++ )
	{
		pfe = pfm->papfe[i];
		for ( n = 0; n < pBRAM->BRAMcountElm; n++ )
		{
			pLabelBRAMCount = &(pBRAM->LabelBRAMCounters[n]);
			if ( 0 == strcmp(pfe->strName, pLabelBRAMCount->strElmCounter ))
			{
				pfc = (LPFJCOUNTER)pfe->pDesc;
				pfc->lCurValue = pLabelBRAMCount->lCurValue;
			}
		}
	}
}
#endif //_WINDOWS // Burl 1.2009
