#include "defines.h"
#include "fpga_bit.h"
#include "mphc.h"

    SetMphcBaseAddress ( (unsigned short *)0x40000000 );  //Set Address

    ProgramFPGA( pFPGA_BIT, FPGA_ARRAY_SIZE );	//Load FPGA

    //Change the function in file MyDocuments...Bsp.c
    NAStaticMemoryType const NAStaticMemoryTable[] =
{

	{1, 0x180, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,   0x40000000, 0xFF700001},  //ADDED for CS0

	{1, 0x81, 0x00, 0x01, 0x06, 0x00, 0x05, 0x02,   0x0, 0x0}, /* cs1 */
	{0, 0x181, 0x00, 0x00, 0x1f, 0x1f, 0x1f, 0x0f, 0x03100000, 0xffff8001}, /* cs2 */
	{0, 0x82, 0x02, 0x02, 0x09, 0x02, 0x09, 0x02, 0x0, 0x0}  /* cs3 */
};


	//Add this line to the file in MyDocuments...customizeCache.c
mmuTableType mmuTable[] =
{
/*   Start       End          Page Size          Cache Mode          User Access     Physical Address */
/*   ==========  ==========   ================   =============       ===========     =============== */

    {0x40000000, 0x40000200,  MMU_PAGE_SIZE_4K,  MMU_NONBUFFERED,    MMU_CLIENT_RW,  0x40000000, 0x40000200}, /* CS0 - MPHC */

//Change the function in file MyDocuments...CS.c
void customizeSetupCS0(void)
{

    /*
     * Chip select 0 is used for the LCD controller.
     */
    if (NAStaticMemoryTable[CS_0_INDEX].enabled)
    {
        /* LCD required the extended wait register to be set */  //set function active
       narm_write_reg(NA_MPMC_STATIC_EXTENDED_WAIT,
                       NA_MPMC_STATIC_EXTENDED_WAIT_ADDR,
                       EXTW, 5);


        /* Configure the values for the PrimeCell MultiPort Memory Controller */
        narm_write_reg(NA_MPMC_STATIC_CONFIG0, NA_MPMC_STATIC_CONFIG0_ADDR,
                       config, NAStaticMemoryTable[CS_0_INDEX].StaticConfig | 1);  //This line needs to change





