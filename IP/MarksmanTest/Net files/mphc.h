#ifndef MPHC_H_
	#define MPHC_H_
	#include "defines.h"
	#include "typedefs.h"

	extern tagPHC_Flags gPHCFlags;
	
	unsigned char ReadByte( unsigned char offset );
	unsigned char ReadByte( unsigned char offset );
	void ReadRegister( int RegID, void *pData, int nBytes);
	void WriteRegister( int RegID, void *pData, int nBytes);
	void SetMphcBaseAddress ( unsigned short * pAddress );
	void WriteByte (unsigned char offset, unsigned char data);
	void ReadBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes);
	void WriteBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes);
	void EnableProgrammingMode ( bool Enabled );
	void EnableDataTransferMode( bool Enabled, bool Direction);
	void EnableCommandMode( bool Enabled);
	void ProgramSlantRegs(tagSlantRegs Regs);
	void ReadAmsRegs(tagAMSRegs *Regs);
	void ProgramAmsRegs(tagAMSRegs Regs);
	void ReadConfiguration(tagRegisters *Regs);
	void SetSpeedGateWidth( unsigned short nEncRes);
	void GetIndicators(tagIndicators *Indicators);
	void SetImageLen( unsigned short nImageCols);
	void FireAMS( void );

	int ProgramFPGA(void * pCode, unsigned long lBytes);
#endif /*MPHC_H_*/
