#ifndef DEFINES_H_
	#define DEFINES_H_

	#define bool unsigned char

	#define WRITE_MODE	0
	#define READ_MODE	1

	#define CMD_REG		0x00
	#define IER		0x01
	#define IIR		0x02
	#define LCR		0x03
	#define MCR		0x04
	#define LSR		0x05
	#define MSR		0x06
	#define SCRATCH_PAD	0x07
	#define OTB_STATUS	SCRATCH_PAD
	#define DLAB		0x83
	#define DLL		0x00
	#define DLH		0x01

	#define DTR		0x01	// 0x01 - Original value
	#define RTS		0x02	// Request To Send UART control signal
	#define OP1		0x04
	#define OP2		0x08
	#define LOOP		0x10

	#define LCR_6_SPACE_1	0x39
	#define LCR_6_SPACE_2	0x3D	// Data Transfer Mode Read
	#define LCR_6_MARK_2	0x2D	// Data Transfer mode Write.

	#define LCR_7_SPACE_2	0x3E	// Non Vol .

	#define LCR_8_NONE_1	0x03	// Normal Operation mode.
	#define LCR_8_MARK_2	0x2F	// Programming Mode
	#define LCR_8_SPACE_2	0x3F	// Command Mode.
#endif /*DEFINES_H_*/
