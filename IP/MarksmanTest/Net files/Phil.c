unsigned char WriteControlReg( UCHAR uData )
{

	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x00 );	// Read mode: CMD REG
		WriteByte ( CMD_REG, 0x01 );	// Bytes To Transfer
		WriteByte ( CMD_REG, uData );
// 	Bit 0:1: 0= PH Reset, 1=Slanted, 2= Staggered, 3=Graphics
// 	Bit 2: 1= Single Buffer
// 	Bit 3: Orientation 0=Normal, 1= Inverted
// 	Bit 4: Double Pulse 0= No, 1=Yes
// 	Bit 5: Not Used (AMS)
// 	Bit 6: Print buffer select 0= Buff 0, 1= Buff 1
// 	Bit 7: PH Enable 1=Enable
	EnableCommandMode ( FALSE );

}

unsigned char ReadInterfaceStatusReg( void )
{
	UCHAR uData = 0x00;

	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x84 );	// Read mode; Start Address = 0x04
		WriteByte ( CMD_REG, 0x01 );	// Bytes To Transfer
		uData = ReadByte ( CMD_REG );
// 	Bit 0: Not used
// 	Bit 1: Not Used
// 	Bit 2: Photocell
// 	Bit 3: 1= Print Head Error
// 	Bit 4: 1= HVOK
// 	Bit 5: 1= Low Ink
// 	Bit 6: 1= At Temp
// 	Bit 7: 1= Heater On
	EnableCommandMode ( FALSE );

	return uData;
}


void RxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
//	Debug Stuff
//	EnableCommandMode ( true );
//	WriteByte ( 0x00, 0x08 );
//	WriteByte ( 0x00, 0x02 );
//	WriteByte ( 0x00, wStartCol & 0x00FF );
//	WriteByte ( 0x00, wStartCol & 0xFF00 );
//	EnableCommandMode ( false );

	unsigned short Location = wStartCol * ( nBPC / 2 );
	EnableCommandMode ( true );
	WriteByte ( 0x00, 0x08 );
	WriteByte ( 0x00, 0x02 );
	unsigned char byte;
	byte = Location & 0x00FF;
	WriteByte ( 0x00, byte );
	byte = ((Location>>8) & 0xFF);
	WriteByte ( 0x00, byte );
	EnableCommandMode ( false );

	EnableDataTransferMode ( true, READ_MODE );
	ReadBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( false, READ_MODE );

	printf ( "RxImage() StartCol %i EndCol %i at %i BPC\n", wStartCol, wNumCols, nBPC);
}

void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
	unsigned short Location = wStartCol * ( nBPC / 2 );

	EnableCommandMode ( true );
		WriteByte ( 0x00, 0x08 );
		WriteByte ( 0x00, 0x02 );
		unsigned char byte;
		byte = Location & 0x00FF;
		WriteByte ( 0x00, byte );
		byte = ((Location>>8) & 0xFF);
		WriteByte ( 0x00, byte );
	EnableCommandMode ( false );

	EnableDataTransferMode ( true, WRITE_MODE );
		WriteBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( false, WRITE_MODE );

	printf ( "TxImage() StartCol %i NumCol %i at %i BPC to 0x%x\n", wStartCol, wNumCols, nBPC, m_Address);
}

