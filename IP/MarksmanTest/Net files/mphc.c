#include <stdio.h>
#include <stdlib.h>
#include <tx_api.h>
#include <bsp.h>
#include <i2c_api.h>

#include "gpiomux_def.h"
#include "mc_isr.h"
#include "nagpio.h"
#include "appconf.h"
#include "netosIo.h"
#include "defines.h"
#include "mphc.h"

// This is the units when the conveyor speed is 100 ft/min
// This means, if this value is 1000 then when the conveyor is running 
// at 100 ft/min the value returned when you read the speed register 
// will be 1000  This allows for as much presision as needed.
const double FTACH_100 = 1000.0;

// This is a fixed constant within the MPHC Board.
const double FGATE = 115200.0;

tagPHC_Flags gPHCFlags;

volatile unsigned short *g_pAddress;

static int irq3Isr(void *context)
{
	static unsigned char uIIR;
	static unsigned char uData;
	static unsigned char uBitMask;
	volatile tagPHC_Flags *pFlags;

	pFlags->flagISR++;
	uIIR = ReadByte ( IIR );
	do {
		switch ( uIIR & 0x07 ) {
		case 0:		// MSR
			uData = ReadByte (MSR);
			uBitMask = 0x01;
			while ( uBitMask & 0x0F ) {
				switch ( uData & uBitMask ){
				case PC_INT:
					pFlags->flagPhotoInt++;
					pFlags->PhotocellActivated = TRUE;	// Photocell event has occured on the slave device.
					break;
				case AMS_STATE_CHANGE:
					pFlags->AmsActive = !pFlags->AmsActive;
					break;
				case PRINT_CYCLE_STATE:	//										// cgh 04d07
					switch ( uBitMask & 0x20 ) {	//							// cgh 04d07
						case 0:	// Complete print-cycle is finished.	// cgh 04d07
							pFlags->EndOfPrint = TRUE;							// cgh 04d10
							break;
						case 1:	// Photo delay is finished and Printing has begun.		// cgh 04d07
							break;
					}
				case OTB_INT:
					break;
				}
				uBitMask <<= 1;
			}
			break;
		case 2:  // THR Empty
			break;
		case 4:  // Serial Data Aval.
			pFlags->flagSerial++;
//				ProcessSerialData();
			break;
		case 6:  // LSR Error
			pFlags->flagSerial++;
			ReadByte ( LSR );
			break;
		default:
			pFlags->flagReserved++;
		}
		uIIR = ReadByte ( IIR );
	} while (  !( uIIR & 0x01 ) );
	
	/* clear the interrupt */ 
	narm_write_reg (NA_SCM_EICR_REG, NA_SCM_EICR_3_ADDRESS, clr, 1);
	narm_write_reg (NA_SCM_EICR_REG, NA_SCM_EICR_3_ADDRESS, clr, 0);
	return 0;	
}

void SetMphcBaseAddress ( unsigned short *pAddress )
{
	g_pAddress = (unsigned short *)pAddress;
}

unsigned char ReadByte( unsigned char offset )
{
	unsigned short value;
	volatile unsigned short *pAddress;
	
	pAddress = g_pAddress;
	pAddress += offset;
	value = *pAddress;
	return value;
}

void WriteByte (unsigned char offset, unsigned char data)
{
	unsigned char byte = data;
	volatile unsigned short * pAddress = g_pAddress;

	pAddress += offset;
	*pAddress = byte;
}

void WriteBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes)
{
	unsigned long i;
	
	for ( i = 0; i < lBytes; i++ ) {
		WriteByte ( offset, pBuffer[i] );
	}
}

void ReadBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes)
{
	unsigned long i;
	
	for ( i = 0; i < lBytes; i++ ) {
		pBuffer[i] = ReadByte ( offset );
	}
}

//////////////////////////////////////////////////////////////////////
//	FPGA is told that programming is about to begin by setting the
//		UART for 8 Databits, 1 Parity, and 2 Stopbits.  Reset to 8,N,1 
//		before actual programming begins.
//////////////////////////////////////////////////////////////////////
void EnableProgrammingMode ( bool Enabled )
{
	unsigned char value;
	if ( Enabled ) {
		value = ReadByte ( MSR);
		WriteByte ( LCR, LCR_8_MARK_2);	// Tell UART we want to program it.
		WriteByte ( MCR, DTR);
		WriteByte ( LCR, LCR_8_NONE_1 );	// Reset to 8,N,1
	}
	else
		WriteByte ( LCR, 0x03 );
}

void EnableDataTransferMode( bool Enabled, bool Direction)
{
	if ( Enabled ) {
		WriteByte ( LCR, 0x03 );				// Ensure Baud Rate Divisor is NOT set.
		if ( Direction == WRITE_MODE )
			WriteByte ( LCR, LCR_6_MARK_2 );	// Set Data Transfer Mode. ( Write )
		else
			WriteByte ( LCR, LCR_6_SPACE_2 );	// Set Data Transfer Mode. ( Read )
	}
	else
		WriteByte (LCR, 0x03);
}

void EnableCommandMode( bool Enabled)
{
	if ( Enabled == TRUE ) {
		WriteByte ( LCR, 0x03 );			// Ensure we are in a known state.
		WriteByte ( LCR, LCR_8_SPACE_2 );	// Set Command Mode.
	}
	else
		WriteByte ( LCR, 0x03 );
}

void ReadRegister( int RegID, void *pData, int nBytes)
{
	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, (0x80 | RegID ) );	// Read mode; 
		WriteByte ( CMD_REG, nBytes );			// Bytes To Transfer
		ReadBuffer ( CMD_REG, pData, nBytes);	// Read Data
	EnableCommandMode ( FALSE );
}

void WriteRegister( int RegID, void *pData, int nBytes)
{
	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x7F & RegID);		// Write mode; 
		WriteByte ( CMD_REG, nBytes );			// Bytes To Transfer
		WriteBuffer ( CMD_REG, pData, nBytes );	// Write data
	EnableCommandMode ( FALSE );
}

int ProgramFPGA(void * pCode, unsigned long lBytes)
{
	int status = 0;
	int cnt= 0;

	printf ( "Enabling Programming Mode on 0x%X\n", g_pAddress);
	EnableProgrammingMode ( TRUE );
	// Wait for UART to get ready.  This should be < 1 uSec
	printf ( "Waiting for 0x%X to become ready\n", g_pAddress);
	while ( !( ReadByte ( MSR ) & 0x10 ) ) {
	    tx_thread_sleep( (NABspTicksPerSecond / 500.0) ); // 2 milli-second this is smallest I can sleep
		cnt++;
		if ( cnt > 3 ) {
			return -3;
		}
	}

	if ( cnt < 3 ) 
	{
		printf ( "%s - Programming FPGA with %lu\n", __FILE__, lBytes);
		WriteBuffer (0x00, (unsigned char *)pCode, lBytes);
		cnt = 0;
		printf ( "%s - Waiting For \"Done\" pin assert\n", __FILE__);
		
		WriteByte ( LCR, 0x00 );	
		while ( !(ReadByte( MSR ) & 0x20) ) 
		{	// See if it is done
		    tx_thread_sleep(2);
			cnt++;
			if ( cnt > 10 ) 
			{
				status = -2;
				printf ( "%s - Timeout Waiting For Done Pin\n", __FILE__);
				break;
			}
		}

		if ( status == 0x00 )
			printf ( "FPGA at 0x%X Programmed Successfully.\n", g_pAddress );
	}
	else 
		printf ("Timeout Waiting For UART at 0x%X to Get Ready\n", g_pAddress );
	
	EnableProgrammingMode ( FALSE );

	// The main reason for this is to set the print enable bit to 0
	EnableCommandMode ( TRUE );
	WriteByte ( 0x00, 0x00 );	
	WriteByte ( 0x00, 0x01 );	
	WriteByte ( 0x00, 0x00 );	
	EnableCommandMode ( FALSE );
	return status;
}
void Set_ICR(UCHAR data)
{
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x01 );	// Write mode; Start Address = 0x01 ICR
		WriteByte ( CMD_REG, 0x01 );	// Bytes To Transfer
		WriteByte ( CMD_REG, data);		// Write data
	EnableCommandMode ( FALSE );

	WriteByte ( IER, 0x0F );   // Enable Interrupts
}

int Activate ( void ) 
{
	WriteByte ( LCR, 03 );     // Reset LCR;
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	//setup ISR
	if ( naIsrInstall(EXTERNAL3_INTERRUPT, irq3Isr, &gPHCFlags) == SUCCESS ) {
	//setup GPIO
		if ( NAconfigureGPIOpin(101, NA_GPIO_FUNC2_STATE, 0) != SUCCESS )
			return -1;
	}
	Set_ICR ( 0x03 | 0x10 );	// Set bit 4 to 1 for ISA interrupts
	WriteByte ( IER, 0x0F );	// Enable Interrupts
	return SUCCESS;
}

void ClearOnBoardRam()
{
// Each board has 256k of RAM divided into 2 128K Buffers
	unsigned char Data [ 1024 ];
	unsigned char RegValue;
	int i = 0;
	memset( Data, 0x00, sizeof (Data) );

	// Select buffer 1
	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x80 );
		WriteByte ( 0x00, 0x01 );
		RegValue = ReadByte ( 0x00 );
		WriteByte ( 0x00, 0x00 );
		WriteByte ( 0x00, 0x01 );
		WriteByte ( 0x00,  (RegValue | 0x07) );
	EnableCommandMode ( FALSE );

	EnableDataTransferMode ( TRUE, WRITE_MODE );
		for ( i = 0; i < 256; i++ )
			WriteBuffer ( 0x00, Data, sizeof (Data) );
	EnableDataTransferMode ( FALSE, WRITE_MODE );

	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x00 );
		WriteByte ( 0x00, 0x01 );
		WriteByte ( 0x00,  RegValue );
	EnableCommandMode ( FALSE );
}

void SetPrintDelay ( unsigned short nDelay )
{
	unsigned char value[2];
	
	value[0] = LOBYTE(nDelay);
	value[1] = HIBYTE(nDelay);
	WriteRegister ( PHOTO_DELAY_REG, value, sizeof ( nDelay ) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Sets the Product Lenght.
// The MPHC treats this as a delay, so this is the number of columns to delay after the
// image is printed before allowing the next photocell.
//////////////////////////////////////////////////////////////////////////////////////////////////
void SetProductLen ( unsigned short nLen )
{
	unsigned char value[2];
	
	value[0] = LOBYTE(nLen);
	value[1] = HIBYTE(nLen);
	
	WriteRegister ( PRODUCT_LEN_REG, value, sizeof ( nLen ) );
}

double ReadLineSpeed ( void )
{
	unsigned char value[2];
	unsigned short wSpeed;
	double dSpeed;
	
	ReadRegister ( SPEED_REG, value, sizeof ( value ) );
	wSpeed = value[1];
	wSpeed <<= 8;
	wSpeed |= value[0];
	dSpeed = ((double)wSpeed / (FTACH_100 / 100.0));
	return dSpeed;
}
//////////////////////////////////////////////////////////////////////////////////////////////////
//Sets the Lenght of the actual image.  This needs to include flush buffer
//////////////////////////////////////////////////////////////////////////////////////////////////
void SetImageLen( unsigned short nImageCols)
{
	unsigned char value[2];
	
	value[0] = LOBYTE(nImageCols);
	value[1] = HIBYTE(nImageCols);
	
	WriteRegister ( IMAGE_LEN_REG, value, sizeof ( nImageCols ) );
}

void ProgramSlantRegs(tagSlantRegs Regs)
{
	WriteRegister ( 0x24, &Regs, sizeof ( Regs ) );
}

void ReadAmsRegs(tagAMSRegs *Regs)
{
	ReadRegister ( 0x30, Regs, sizeof ( Regs ) );
}

void ProgramAmsRegs(tagAMSRegs Regs)
{
	WriteRegister ( 0x30, &Regs, sizeof ( Regs ) );
}

void FireAMS( void )
{
}

void ReadConfiguration(tagRegisters *Regs)
{
	ReadRegister ( 0x00, Regs, sizeof ( Regs ) );
}

unsigned short ComputeSpeedGateWidth(unsigned short nEncoderRes)
{
   return ( unsigned short )((double) (FTACH_100 * FGATE ) / (double) ( 20 * nEncoderRes));
}

void SetSpeedGateWidth( unsigned short nEncRes)
{
	unsigned short nSpeed;
	nSpeed = ComputeSpeedGateWidth ( nEncRes );
	WriteRegister ( 0x0E, &nSpeed, sizeof ( nSpeed ) );
}

void GetIndicators(tagIndicators *Indicators)
{
	unsigned char uData;
	unsigned char uMask = 0x10;
	ReadRegister(IFACE_STAUS_REG, &uData, sizeof ( uData ) );
	
	Indicators->HVOK		= ( uData & uMask ) ? TRUE : FALSE; uMask <<= 1;
	Indicators->InkLow	= ( uData & uMask ) ? TRUE : FALSE; uMask <<= 1;
	Indicators->AtTemp	= ( uData & uMask ) ? TRUE : FALSE; uMask <<= 1;
	Indicators->HeaterOn	= ( uData & uMask ) ? TRUE : FALSE;
}

void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
	unsigned short Location = wStartCol * ( nBPC / 2 );

	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x08 );
		WriteByte ( 0x00, 0x02 );
		unsigned char byte;
		byte = Location & 0x00FF;
		WriteByte ( 0x00, byte );
		byte = ((Location>>8) & 0xFF); 
		WriteByte ( 0x00, byte );
	EnableCommandMode ( FALSE );
	
	EnableDataTransferMode ( TRUE, WRITE_MODE );
		WriteBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( FALSE, WRITE_MODE );
}
