#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#ifdef __cplusplus
namespace NEXT {
#endif

typedef struct {
	unsigned char flagISR;
	unsigned char flagPhotoInt;
	unsigned char flagSerial;
	unsigned char flagReserved;
	bool AmsActive;
	bool PhotocellActivated;;	// Photocell event has occured
	bool EndOfPrint;
}tagPHC_Flags;

typedef struct tagImagePkt
{
	ULONG _lAddress;
	ULONG _lStartCol;
	ULONG _lBytes;
	ULONG _lBytesPerCol;
}tagImagePkt;

typedef struct tagIndicators
{
   BOOL HVOK;
   bool AtTemp;
   bool InkLow;
   bool HeaterOn;
}tagIndicators;

#pragma pack(1)
typedef struct tagCtrlReg {						// Control Register
	unsigned char _HeadType		: 2;	// 0-Disabled, 1-Slanted, 2 Staggered, 3-Graphic
	unsigned char _SingleBuffer: 1;	// 1-Single Buffer, 0-DoubleBuffers.
	unsigned char _Inverted		: 1;	// 0-Normal, 1-Inverted
	unsigned char _DoublePulse	: 1;	// Enable Double Pulse.
	unsigned char _FireAMS	   : 1;	// Fire AMS Cycle.
	unsigned char _PrintBuffer	: 1;	// 0-Buffer0, 1-Buffer1 // Selects the buffer which the printer uses when we filling the image. 
	unsigned char _PrintEnable	: 1;	// 1-Enable Printing.
}tagCtrlReg;
#pragma pack()

#pragma pack(1)
typedef struct tagInterruptCtrlReg {			// interrupt Control Register
	unsigned char _Channel		: 4;	// Interrupt Channel Number [0 - 7]
	unsigned char _Mode			: 1;	// Interrupt mode	0-OTB, 1-ISA
	unsigned char _ErrDrv		: 3;	// Do not know what this is.
}tagInterruptCtrlReg;
#pragma pack()

#pragma pack(1)
typedef struct tagInterfaceCtrlReg {			// Interface Control Register
	unsigned char _TachSrc		: 2;	// Tach Source; 0-External, 1-internal, 2-OTB Tach 1, 3-OTB Tach2
	unsigned char _OTBTach1		: 1;	// Drive OTB Tach 1
	unsigned char _OTBTach2		: 1;	// Drive OTB Tach 2
	unsigned char _PhotoSrc		: 2;	// Photocell Source. 0-External, 1-Internal, 2-OTB PC1, 3-OTB PC2
	unsigned char _OTBPhoto1	: 1;	// Drive OTB Photocell 1 (PC1)
	unsigned char _OTBPhoto2	: 1;	// Drive OTB Photocell 2 (PC2)
}tagInterfaceCtrlReg;
#pragma pack()

#pragma pack(1)
typedef struct tagOTBStatusReg
{                                      // Over-the-Top-Bus Status Register
	unsigned char _OTBI			: 7;	// OTB Interrupt
	unsigned char _Reserved		: 1;	// Reserved.
}tagOTBStatusReg;
#pragma pack()

#pragma pack(1)
typedef struct tagInterfaceStatusReg
{												// Interface Status Register
	unsigned char _OTBIP			: 1;	// Over-the-Top interrupt Pending.
	unsigned char _TachIntr		: 1;	// Tachometer interrupt
	unsigned char _Photocell	: 1;	// Photocell Interrupt.
	unsigned char _HeadError	: 1;	// Print Head Error.
	unsigned char _HVOK			: 1;	// High Voltage Ok
	unsigned char _InkLow		: 1;	// Ink Low
	unsigned char _AtTemp		: 1;	// At Temp.
	unsigned char _HeaterOn		: 1;	// Heater ON
	unsigned char _Reserved		: 8;	// Reserved. (Read as 0 ).
}tagInterfaceStatusReg;
#pragma pack()

#pragma pack(1)
typedef struct tagDividerCtrlReg
{												// Divider Control Register
	unsigned char _TachDiv		: 2;	// Reserved
	unsigned char _SClkRate		: 2;	// 3- 7.3728MHz, 2- 3.6864MHz, 1- 1.8432MHz, 0- 921.6kHz
	unsigned char _TackIntRate	: 3;	// Tach Interrupt Rate 16*2^x
	unsigned char _Reserved		: 1;	// Reserved
}tagDividerCtrlReg;
#pragma pack()

#pragma pack(1)
typedef struct tagImgBufBaseAddrReg
{												// Image Buffer Base Address Register
	unsigned char _WindowSize	: 2;	// Window Size: 0- Disabled, 1- 16kB, 2- 32kB, 3- 64kB
	unsigned char _WindowBase	: 6;	// ???
}tagImgBufBaseAddrReg;
#pragma pack()

#pragma pack(1)
typedef struct tagImgLenReg
{												// Image Buffer Offset Register
	unsigned short _Len	: 16;
}tagImgLenReg;
#pragma pack()

#pragma pack(1)
typedef struct tagIntTachGenReg
{													// Internal Tach Generator Register
	unsigned short _TachGenFreg	: 16;	// Freguency given by (desired freg / 7.3728) * 2^16
}tagIntTachGenReg;
#pragma pack()

#pragma pack(1)
typedef struct tagIntPhotoGenReg
{													// Internal Photocell Generator Count Register
	unsigned short _PhotoGenFreg	: 16;
}tagIntPhotoGenReg;
#pragma pack()

#pragma pack(1)
typedef struct tagConveyorSpeed
{													// Conveyor Speed Register
	unsigned short _Speed			: 16; // Read as ft/min
}tagConveyorSpeed;
#pragma pack()

#pragma pack(1)
typedef struct tagFireVibPulse
{	// Fire / Vibration Pluse Register
	unsigned short _FirePulseDelay			: 16; // 
	unsigned short _FirePulseWidth			: 16; // 
	unsigned short _DampPulseDelay			: 16; // 
	unsigned short _DampPulseWidth			: 16; // 
	// Remaining values are for vibration which are no longer used.
	unsigned short _v5			: 16; // 
	unsigned short _v6			: 16; // 
	unsigned short _v7			: 16; // 
	unsigned short _v8			: 16; // 
}tagFireVibPulse;
#pragma pack()

#pragma pack(1)
typedef struct tagPrintDelay
{												// Print Delay Register.
	unsigned short _PrintDelay	: 16; // (Value = Columns)
}tagPrintDelay;
#pragma pack()

#pragma pack(1)
typedef struct tagImageLen
{												// Image Length Register.
	unsigned short _Len			: 16; // (Value = Columns)
}tagImageLen;
#pragma pack()

#pragma pack(1)
typedef struct tagImageStartAddr
{												// Address of Image Register
	unsigned short _Addr1		: 16; // Address of first image WORD 
	unsigned short _Addr2		: 16; // Address of second image WORD
}tagImageStartAddr;
#pragma pack()

#pragma pack(1)
typedef struct tagAddrIncFactor
{												// Address Increment Factor Register
	unsigned short _Incr			: 16; // 
}tagAddrIncFactor;
#pragma pack()

#pragma pack(1)
typedef struct tagColAdjustFactor
{												// Column Adjust Factor Register
	unsigned short _Adjust		: 16; // 
}tagColAdjustFactor;
#pragma pack()

#pragma pack(1)
typedef struct tagProdLenDelay
{												// Product Length Delay Register
	unsigned short _Delay		: 16; // ( Value = Columns )
}tagProdLenDelay;
#pragma pack()

#pragma pack(1)
typedef struct tagRegisters {
	tagCtrlReg					_CtrlReg;				// Control Register.
	tagInterruptCtrlReg		_Interrupt_Reg;		// interrupt Control Register.
	tagInterfaceCtrlReg		_InterfaceCtrlReg;	// Interface Control Register.
	tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
	tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
	tagDividerCtrlReg			_DividerCtrlReg;		// Divider Control Register.
	tagImgBufBaseAddrReg		_ImgBufBaseAddr;		// Image Buffer Base Address Register
	tagImgLenReg				_ImgLenReg;				// Image Length Register
	tagIntTachGenReg			_IntTachGenReg;		// Internal Tach Generator Register
	tagIntPhotoGenReg			_IntPhotoGenReg;		// Internal Photocell Generator Count Register
	tagConveyorSpeed			_Conveyor;				// Conveyor Speed Register
	tagFireVibPulse			_FireVibPulse;			// Fire / Vibration Pulse Register
	tagPrintDelay				_PrintDelay;			// Print Delay Register.
	tagImageLen					_ImageLen;				// Image Length Register.
	tagImageStartAddr			_ImageStartAddr;		// Address of Image Register
	tagAddrIncFactor			_AddrIncFactor;		// Address Increment Factor Register
	tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
	tagProdLenDelay			_ProdLenDelay;			// Product Length Delay Register
}tagRegisters;
#pragma pack()

#pragma pack(1)
typedef struct tagSetupRegs {
	tagCtrlReg					_CtrlReg;				// Control Register.
	tagInterruptCtrlReg		_Interrupt_Reg;		// interrupt Control Register.
	tagInterfaceCtrlReg		_InterfaceCtrlReg;	// Interface Control Register.
	tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
	tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
	tagDividerCtrlReg			_DividerCtrlReg;		// Divider Control Register.
	tagImgBufBaseAddrReg		_ImgBufBaseAddr;		// Image Buffer Base Address Register
	tagImgLenReg		      _ImgLenReg;				// Image Length Register
	tagIntTachGenReg			_IntTachGenReg;		// Internal Tach Generator Register
	tagIntPhotoGenReg			_IntPhotoGenReg;		// Internal Photocell Generator Count Register
}tagSetupRegs;
#pragma pack()

#pragma pack(1)
typedef struct tagSlantRegs {
	tagImageStartAddr			_ImageStartAddr;		// Address of Image Register
	tagAddrIncFactor			_AddrIncFactor;		// Address Increment Factor Register
	tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
}tagSlantRegs;
#pragma pack()

#pragma pack(1)
typedef struct tagAMSRegs
{
   unsigned short A1;	// A1:Number of pulse sets ( purge cycles )
   unsigned short A2;	// A2:Time from vaccum on until purge cyle start
   unsigned short A3;	// A3:
   unsigned short A4;	// A4:
   unsigned short A5;	// A5:
   unsigned short A6;	// A6:
   unsigned short A7;	// A7:
}tagAMSRegs;
#pragma pack()

#ifdef __cplusplus
} ;//namespace NEXT 
#endif

#endif /*TYPEDEFS_H_*/
