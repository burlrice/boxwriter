// IpDemo.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "IpDemo.h"
#include "IpDemoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIpDemoApp

BEGIN_MESSAGE_MAP(CIpDemoApp, CWinApp)
	//{{AFX_MSG_MAP(CIpDemoApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIpDemoApp construction

CIpDemoApp::CIpDemoApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CIpDemoApp object

CIpDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CIpDemoApp initialization

BOOL CIpDemoApp::InitInstance()
{
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CIpDemoDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
