// IpDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IpDemo.h"
#include "IpDemoDlg.h"
#include "fj_socket.h"
#include "CmdDlg.h"
#include "Debug.h"
#include "AnsiString.h"
#include "Parse.h"
#include "Net files\fpga_bit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace FoxjetDatabase;

extern CIpDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIpDemoDlg dialog

CIpDemoDlg::CIpDemoDlg(CWnd* pParent /*=NULL*/)
:	m_hThread (NULL),
	m_hExitEvent (NULL),
	CDialog(CIpDemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIpDemoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIpDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIpDemoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIpDemoDlg, CDialog)
	//{{AFX_MSG_MAP(CIpDemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_CONNECT, OnConnect)
	ON_WM_DESTROY()
	ON_BN_CLICKED(BTN_PROPERTIES, OnProperties)
	ON_BN_CLICKED(BTN_NEW, OnNew)
	ON_NOTIFY(NM_DBLCLK, LV_CMDS, OnDblclkCmds)
	ON_BN_CLICKED(BTN_DEMO, OnDemo)
	ON_BN_CLICKED(BTN_CLEAR, OnClear)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(CB_SCRIPT, OnSelchangeScript)
	ON_BN_CLICKED(BTN_FONT, OnFont)
	ON_BN_CLICKED(BTN_BITMAP, OnBitmap)
	ON_BN_CLICKED(BTN_RESEND, OnResend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIpDemoDlg message handlers

BYTE reverse (BYTE b, int nBits)
{
	int i;
	BYTE bResult = 0;

	if (nBits <= 8) 
		for (i = 0; i < nBits; i++)
			bResult |= ((b & (1 << i)) ? 1 : 0) << ((nBits - 1) - i);

	return bResult;
}

CString ToBinStr (BYTE b)
{
	CString str;

	for (int i = 0; i < 8; i++)
		str += (b & (1 << i)) ? _T ("1") : _T ("0"); 

	return str;
}

BOOL CIpDemoDlg::OnInitDialog()
{
	{
		/*
		for (int i = 0; i < 8; i++) {
			UCHAR c1 = 1 << i;
			UCHAR c2 = reverse (c1, i + 1);

			CString str1 = ToBinStr (c1).Left (i + 1);
			CString str2 = ToBinStr (c2).Left (i + 1);
			CString str;

			str.Format (_T ("%-8s [0x%02X] --> %8s [0x%02X]"), str1, c1, str2, c2);
		
			TRACEF (str);
		}
		*/

		/*
		typedef struct 
		{
			unsigned char _1 : 1;
			unsigned char _2 : 1;
			unsigned char _3 : 1;
			unsigned char _4 : 1;
			unsigned char _5 : 1;
			unsigned char _6 : 1;
			unsigned char _7 : 1;
			unsigned char _8 : 1;
		} STRUCT_1;


		STRUCT_1 s_1;
		int i;
		CString str;

		for (i = 0; i < 8; i++) {
			UCHAR c = 1 << i;
			str.Format (_T ("[%d]: 0x%02X (0x%02X)"), i, c, 1 << i);
			TRACEF (str);
		}
		
		str.Format (_T ("sizeof (s_1): %d\n"), sizeof (s_1));
		TRACEF (str);
		
		memset (&s_1, 0, sizeof (s_1));
		s_1._1 = 1;
		UCHAR * p = (UCHAR *)&s_1;
		str.Format (_T ("[%d] s_1: 0x%02X [%d %d %d %d %d %d %d %d]"), i, * p, s_1._1, s_1._2, s_1._3, s_1._4, s_1._5, s_1._6, s_1._7, s_1._8);
		TRACEF (str);

		for (i = 0; i < 256; i++) {
			memset (&s_1, 0, sizeof (s_1));
			//memcpy (&s_1, i, sizeof (s_1));
			
			UCHAR * p = (UCHAR *)&s_1;
			
			* p = i;
			str.Format (_T ("[%d] s_1: 0x%02X [%d %d %d %d %d %d %d %d]"), i, * p, s_1._1, s_1._2, s_1._3, s_1._4, s_1._5, s_1._6, s_1._7, s_1._8);
			TRACEF (str);
		}
		*/

		/*
		typedef struct tagSetupRegs {
			tagCtrlReg					_CtrlReg;				// Control Register.
			tagInterruptCtrlReg		_Interrupt_Reg;		// interrupt Control Register.
			tagInterfaceCtrlReg		_InterfaceCtrlReg;	// Interface Control Register.
			tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
			tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
			tagDividerCtrlReg			_DividerCtrlReg;		// Divider Control Register.
			tagImgBufBaseAddrReg		_ImgBufBaseAddr;		// Image Buffer Base Address Register
			tagImgLenReg		      _ImgLenReg;				// Image Length Register
			tagIntTachGenReg			_IntTachGenReg;		// Internal Tach Generator Register
			tagIntPhotoGenReg			_IntPhotoGenReg;		// Internal Photocell Generator Count Register
		}tagSetupRegs;
		*/

		/*
		tagSetupRegs t;
		const ULONG lBase = (ULONG)&t;
		struct 
		{
			LPCTSTR m_lpsz;
			ULONG m_lOffset;
			ULONG m_lSize;
		}
		const map [] = 
		{
			_T ("_CtrlReg"),					(ULONG)&t._CtrlReg				- lBase,	sizeof (t._CtrlReg),
			_T ("_Interrupt_Reg"),				(ULONG)&t._Interrupt_Reg		- lBase,	sizeof (t._Interrupt_Reg),
			_T ("_InterfaceCtrlReg"),			(ULONG)&t._InterfaceCtrlReg		- lBase,	sizeof (t._InterfaceCtrlReg),
			_T ("_OTBStatusReg"),				(ULONG)&t._OTBStatusReg			- lBase,	sizeof (t._OTBStatusReg),
			_T ("_InterfaceStatusReg"),			(ULONG)&t._InterfaceStatusReg	- lBase,	sizeof (t._InterfaceStatusReg),
			_T ("_DividerCtrlReg"),				(ULONG)&t._DividerCtrlReg		- lBase,	sizeof (t._DividerCtrlReg),
			_T ("_ImgBufBaseAddr"),				(ULONG)&t._ImgBufBaseAddr		- lBase,	sizeof (t._ImgBufBaseAddr),
			_T ("_ImgLenReg"),					(ULONG)&t._ImgLenReg			- lBase,	sizeof (t._ImgLenReg),
			_T ("_IntTachGenReg"),				(ULONG)&t._IntTachGenReg		- lBase,	sizeof (t._IntTachGenReg),
			_T ("_IntPhotoGenReg"),				(ULONG)&t._IntPhotoGenReg		- lBase,	sizeof (t._IntPhotoGenReg),
		};
		CString str;

		str.Format (_T ("sizeof (tagSetupRegs): %d"), sizeof (tagSetupRegs));
		TRACEF (str);

		for (int i = 0; i < ARRAYSIZE (map); i++) {

			str.Format (_T ("%-20s: %d [%d]"), map [i].m_lpsz, map [i].m_lOffset, map [i].m_lSize);
			TRACEF (str);
		}
		*/
	}

	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);
	CComboBox * pScript = (CComboBox *)GetDlgItem (CB_SCRIPT);

	ASSERT (pLV);
	ASSERT (pScript);
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	if (CIPAddressCtrl * p = (CIPAddressCtrl *)GetDlgItem (IDC_ADDRESS)) {
		BYTE n [4] = 
		{
			theApp.GetProfileInt (_T ("IP Address"), _T ("1"), 192),
			theApp.GetProfileInt (_T ("IP Address"), _T ("2"), 168),
			theApp.GetProfileInt (_T ("IP Address"), _T ("3"), 2),
			theApp.GetProfileInt (_T ("IP Address"), _T ("4"), 163),
		};
		
		p->SetAddress (n [0], n [1], n [2], n [3]);
	}

	pLV->SetExtendedStyle (pLV->GetExtendedStyle () | LVS_EX_FULLROWSELECT);
	pLV->InsertColumn (0, _T ("Command"),	LVCFMT_LEFT, 110);
	pLV->InsertColumn (1, _T ("Data"),		LVCFMT_LEFT, 160);
	pLV->InsertColumn (2, _T ("Response"),	LVCFMT_LEFT, 160);

	try {
		ULONG lScriptID = theApp.GetProfileInt (_T ("Settings"), _T ("lScriptID"), 0);
		m_db.Open (_T ("IpConfig.mdb"));

		CDaoRecordset rst (&m_db);

		rst.Open (dbOpenSnapshot, _T ("SELECT * FROM Scripts;"), dbReadOnly);

		while (!rst.IsEOF ()) {
			ULONG lID = rst.GetFieldValue (_T ("ID")).lVal;
			CString strName = (LPCTSTR)rst.GetFieldValue (_T ("Name")).bstrVal;

			int nIndex = pScript->AddString (strName);
			pScript->SetItemData (nIndex, lID);

			if (lID == lScriptID)
				pScript->SetCurSel (nIndex);

			rst.MoveNext ();
		}

		if (pScript->GetCurSel () == CB_ERR)
			pScript->SetCurSel (0);

		OnSelchangeScript ();
		rst.Close ();
	}
	catch (CDaoException * e)		{ e->ReportError (); e->Delete (); ASSERT (0); }
	catch (CMemoryException * e)	{ e->ReportError (); e->Delete (); ASSERT (0); }

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIpDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIpDemoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CIpDemoDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CIpDemoDlg::OnConnect() 
{
	if (!m_hThread) {
		DWORD dw = 0;
		VERIFY (m_hExitEvent = ::CreateEvent (NULL, TRUE, FALSE, _T ("Exit")));
		VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw));
	}
	else
		KillThread ();
}

CString CIpDemoDlg::ToString (const _FJ_SOCKET_MESSAGE & s)
{
	CString str;

	str.Format (_T ("%s [%d]: %s"),
		GetIpcCmdName (s.Header.Cmd),
		s.Header.BatchID,
		a2w (s.Data));

	return str;
}

void CIpDemoDlg::KillThread()
{
	if (m_hThread) {
		BeginWaitCursor ();
		
		::SetEvent (m_hExitEvent);
		::Sleep (100);
		
		::WaitForSingleObject (m_hThread, 3000);

		if (m_hThread) 
			TRACEF (_T ("Failed to end thread"));

		EndWaitCursor ();
	}
}

void CIpDemoDlg::OnDestroy() 
{
	CDialog::OnDestroy();	
}

void CIpDemoDlg::OnClose() 
{
	OnClear ();
	KillThread ();
	CDialog::OnClose();
}

void CIpDemoDlg::OnOK()
{

}

int CIpDemoDlg::GetSelectedIndex ()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	for (int i = 0; i < pLV->GetItemCount (); i++) 
		if (pLV->GetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED)))
			return i;

	return -1;
}

void CIpDemoDlg::OnProperties() 
{
	CSingleLock lock (&m_cs);

	lock.Lock ();

	if (lock.IsLocked ()) {
		int nIndex = GetSelectedIndex ();
		
		if (nIndex == -1)
			MessageBox (_T ("Nothing selected"));
		else {
			CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

			ASSERT (pLV);

			if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (nIndex)) {
				CCmdDlg dlg (this);

				dlg.m_dwCmd = p->send.Header.Cmd;
				dlg.m_strData = a2w (p->send.Data);
				dlg.m_strResponse = a2w (p->recv.Data);
				dlg.m_lBatchID = p->send.Header.BatchID;

				dlg.DoModal ();
			}
		}

		lock.Unlock ();
	}
}

void CIpDemoDlg::OnNew() 
{
	CCmdDlg dlg (this);
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	if (dlg.DoModal () == IDOK) 
		Add (dlg.m_dwCmd, dlg.m_strData);
}

void CIpDemoDlg::OnDblclkCmds(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnProperties ();	
	*pResult = 0;
}

int CIpDemoDlg::Add (IPCSTRUCT * pItem)
{
	BeginWaitCursor ();

	CSingleLock lock (&m_cs);
	int nIndex = -1;

	lock.Lock ();

	if (lock.IsLocked ()) {
		LV_ITEM lvi;
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM; 
		lvi.iItem = pLV->GetItemCount (); 
		lvi.iSubItem = 0; 
		lvi.iImage = 0;
		lvi.pszText = _T (""); 
		lvi.lParam = (LPARAM)pItem;

		nIndex = pLV->InsertItem (&lvi);
		
		ASSERT (nIndex != -1);

		pLV->SetItemText (nIndex, 0, GetIpcCmdName (pItem->send.Header.Cmd));
		pLV->SetItemText (nIndex, 1, a2w (pItem->send.Data));
		pLV->SetItemText (nIndex, 2, a2w (pItem->recv.Data));

		pLV->EnsureVisible (nIndex, FALSE);

		lock.Unlock ();
	}

	EndWaitCursor ();

	return nIndex;
}

int CIpDemoDlg::Add (DWORD dw, const CString & str)
{
	BeginWaitCursor ();

	CSingleLock lock (&m_cs);
	int nIndex = -1;

	lock.Lock ();

	if (lock.IsLocked ()) {
		IPCSTRUCT * pItem = new IPCSTRUCT;
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		memset (pItem, 0, sizeof (IPCSTRUCT));

		int nLen = min (str.GetLength (), sizeof (pItem->send.Data));

		pItem->send.Header.Cmd = dw;
		memcpy (pItem->send.Data, w2a (str), nLen);

		nIndex = Add (pItem);
	}

	EndWaitCursor ();

	return nIndex;
}

void CIpDemoDlg::OnDemo() 
{
	struct
	{
		DWORD m_dw;
		LPCTSTR m_lpsz;
	}
	static const map [] =
	{
		{ IPC_SET_PH_TYPE,				_T ("1") },
		{ IPC_SET_PRINTER_INFO,			_T ("DIRECTION=LEFT_TO_RIGHT;SLANTVALUE=0;SLANTANGLE=90.000000;DISTANCE=1.000000;HEIGHT=0.000000;PHOTOCELL_BACK=F;PHOTOCELL_INTERNAL=F;OP_AUTOPRINT=10800;") },
		{ IPC_SET_SYSTEM_INFO,			_T ("BC_N=0;BC_NAME=\"Mag 100\";BC_TY=2;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=37;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=1;BC_NAME=\"Mag  90\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=85;BC_BASE=34;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=2;BC_NAME=\"Mag  80\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=78;BC_BASE=30;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=3;BC_NAME=\"Mag  70\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=75;BC_BASE=26;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=4;BC_NAME=\"Mag  62\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=50;BC_BASE=24;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=5;BC_NAME=\"Custom 1\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=6;BC_NAME=\"Custom 2\";BC_TY=1;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=7;BC_NAME=\"Peanut\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;UNITS=ENGLISH_INCHES;TIME=24;INTERNAL_CLOCK=T;SYS") },
		{ IPC_SET_PRINTER_INFO,			_T ("AMS_INTERVAL=4.0") },
		{ IPC_SET_PRINTER_MODE,			_T ("APS_MODE=DISABLE;") },
		{ IPC_GET_LABEL_ID_SELECTED,	_T ("") },
		{ IPC_GET_SW_VER_INFO,			_T ("") },
		{ IPC_GET_PRINTER_MODE,			_T ("") },
		{ IPC_GET_LABEL_IDS,			_T ("") },
		{ IPC_GET_FONT_IDS,				_T ("") },
		{ IPC_GET_GA_VER_INFO,			_T ("") },
		{ IPC_GET_LABEL_ID_SELECTED,	_T ("") },
		{ IPC_EDIT_START,				_T ("") },
		{ IPC_PUT_PRINTER_ELEMENT,		_T ("{Label,\"Barcode\",20.0,20.0,0,3,F,\"Barcode\"}{Message,\"Barcode\",1,\"Barcode-Barcode-4\"}{Barcode,\"Barcode-Barcode-4\",0.000000,0,D,0,1,F,F,F,0,T,C,1,\"123456\"}") },
		{ IPC_EDIT_SAVE,				_T ("") },
		{ IPC_SET_LABEL_ID_SELECTED,	_T ("LABELID=\"Barcode\";") },
		{ IPC_GET_LABEL_ID_SELECTED,	_T ("") },
		{ IPC_GET_SELECTED_COUNT,		_T ("") },
		{ IPC_SET_SELECTED_COUNT,		_T ("999") },
		{ IPC_GET_SELECTED_COUNT,		_T ("") },
	};
	
	for (int i = 0; i < ARRAYSIZE (map); i++) 
		Add (map [i].m_dw, map [i].m_lpsz);
}

void CIpDemoDlg::OnClear() 
{
	CSingleLock lock (&m_cs);

	lock.Lock ();

	if (lock.IsLocked ()) {
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		for (int i = 0; i < pLV->GetItemCount (); i++) 
			if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (i))
				delete p;

		pLV->DeleteAllItems ();
	}
}

bool CIpDemoDlg::CheckTx (CIpDemoDlg & dlg, CSocket & socket, ULONG & lBatchID)
{
	CListCtrl * pLV = (CListCtrl *)dlg.GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	for (int nItem = 0; nItem < pLV->GetItemCount (); nItem++) {
		if (IPCSTRUCT * pCmd = (IPCSTRUCT *)pLV->GetItemData (nItem)) {
			if (pCmd->send.Header.BatchID == 0) {
				CSingleLock lock (&dlg.m_cs);

				lock.Lock ();

				if (lock.IsLocked ()) {
					_FJ_SOCKET_MESSAGE send;

					memset (&send, 0, sizeof (send));

					pCmd->send.Header.BatchID = ++lBatchID;

					send.Header.Cmd		= pCmd->send.Header.Cmd;
					send.Header.BatchID	= lBatchID;
					send.Header.Length	= sizeof (send.Data);

					memcpy (send.Data, pCmd->send.Data, sizeof (send.Data));

					TRACEF (_T ("Send:    ") + ToString (send));

					{
						CString str;

						str.Format (_T ("%d %d %d"), 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
						str.Format (_T ("%p %p %p"), 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
					}

					if (send.Header.Cmd == IPC_PUT_PRINTER_ELEMENT)
						send.Header.Length = strlen ((char *)send.Data) + 1;

					send.Header.Cmd		= htonl (send.Header.Cmd);
					send.Header.BatchID	= htonl (send.Header.BatchID);
					send.Header.Length	= htonl (send.Header.Length);

					{
						CString str;

						str.Format (_T ("%d %d %d"), 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
						str.Format (_T ("%p %p %p"), 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
					}
					{
						CString strFile = GetIpcCmdName (ntohl (send.Header.Cmd));
						
						if (!strFile.GetLength ()) 
							strFile = _T ("cmd");

						strFile += _T (".bin");

						if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
							fwrite (&send, sizeof (send), 1, fp);
							fclose (fp);
						}
					}

					int nSend = SOCKET_ERROR;

					if (send.Header.Cmd == htonl (IPC_GA_UPGRADE)) {
						_FJ_SOCKET_MESSAGE_HEADER Header;
						LPBYTE pFile = NULL;//pFPGA_BIT;
						ULONG lFPGA = 0;//FPGA_ARRAY_SIZE;
						CString strFile = _T ("C:\\Dev\\BoxWriter\\IP\\Marksman\\mphc\\net.bit");
						CFile file;

						TRACEF (strFile);

						if (file.Open (strFile, CFile::modeRead)) {
							lFPGA = file.GetLength ();
							pFile = new UCHAR [lFPGA];
							memset (pFile, NULL, lFPGA);
							DWORD dwActual = file.Read (pFile, lFPGA);
							file.Close();

							memcpy (&Header, &send.Header, sizeof (Header));
							Header.Length = htonl (lFPGA);

							ULONG lLen = sizeof (Header) + lFPGA;
							UCHAR * p = new UCHAR [lLen];
							
							memcpy (p, &Header, sizeof (Header));
							memcpy ((p + sizeof (Header)), pFile, lFPGA);

							nSend = socket.Send (p, lLen);

							delete [] p;
							delete [] pFile;
						}
						else
							::AfxMessageBox (_T ("File not found: ") + strFile);
					}
					else if (pCmd->m_pBuffer) {
						_FJ_SOCKET_MESSAGE_HEADER Header;

						memcpy (&Header, &send.Header, sizeof (Header));
						
						ULONG lFontLen = pCmd->send.Header.Length;
						ULONG lLen = sizeof (Header) + lFontLen;
						UCHAR * p = new UCHAR [lLen];
						
						Header.Length = htonl (pCmd->send.Header.Length);
						memcpy (p, &Header, sizeof (Header));
						memcpy ((p + sizeof (Header)), pCmd->m_pBuffer, lFontLen);

						nSend = socket.Send (p, lLen);

						delete [] p;
					}
					else {
						nSend = socket.Send (&send, sizeof (send));
					}

					if (nSend == SOCKET_ERROR) {
						DWORD dw = GetLastError ();

						switch (dw) {
						case WSAECONNRESET:
						case WSAESHUTDOWN :
							TRACEF (_T ("Lost connection"));
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

/*
{ULONG lBatchID = 0; CSocket socket;
////////////////////////////////////////////////////////////
	_FJ_SOCKET_MESSAGE send;
	char szCmd [SOCKET_BUFFER_SIZE];

	memset (&send, 0, sizeof (send));

	send.Header.Cmd		= IPC_SELECT_LABEL; // note: this an enum, not a string
	send.Header.BatchID	= lBatchID; // we normally use this as a serial number
	send.Header.Length	= sizeof (send.Data);

	sprintf (szCmd, "LABELID=\"%s\";", "TEST");
	memcpy (send.Data, szCmd, strlen (szCmd));

	// convert from host byte order to network byte order.
	send.Header.Cmd		= htonl (send.Header.Cmd);
	send.Header.BatchID	= htonl (send.Header.BatchID);
	send.Header.Length	= htonl (send.Header.Length);

	int nSend = socket.Send (&send, sizeof (send));
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
	_FJ_SOCKET_MESSAGE recv;

	memset (&recv, 0, sizeof (recv));

	int nRevc = socket.Receive (&recv, sizeof (recv));

	// convert network byte order to host byte order.
	recv.Header.Cmd		= ntohl (recv.Header.Cmd);
	recv.Header.BatchID	= ntohl (recv.Header.BatchID);
	recv.Header.Length	= ntohl (recv.Header.Length);
////////////////////////////////////////////////////////////
}
*/

void CIpDemoDlg::CheckRx (CIpDemoDlg & dlg, CSocket & s)
{
	CListCtrl * pLV = (CListCtrl *)dlg.GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	timeval timeout = { 1, 0 };
	fd_set readfds;

	readfds.fd_count = 1;
	readfds.fd_array [0] = s.m_hSocket;

	::SetLastError (0);
	int nSelect = ::select (0, &readfds, NULL, NULL, &timeout);

	if (nSelect > 0) {
		_FJ_SOCKET_MESSAGE recv;

		memset (&recv, 0, sizeof (recv));

		int nRevc = s.Receive (&recv, sizeof (recv));

		if (nRevc != SOCKET_ERROR) {
			CSingleLock lock (&dlg.m_cs);

			lock.Lock ();
			
			bool bMatch = false;

			recv.Header.Cmd		= ntohl (recv.Header.Cmd);
			recv.Header.BatchID	= ntohl (recv.Header.BatchID);
			recv.Header.Length	= ntohl (recv.Header.Length);

			if (lock.IsLocked ()) {
				for (int nItem = 0; nItem < pLV->GetItemCount (); nItem++) {
					if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (nItem)) {
						if (p->send.Header.BatchID == recv.Header.BatchID) {
							TRACEF (_T ("Receive: ") + ToString (recv));
							memcpy (&p->recv, &recv, sizeof (p->recv));
							CString str = a2w (recv.Data);

							if (recv.Header.Cmd == IPC_GET_DISK_USAGE) {
								int flash_size = 0, total_blocks = 0, free_blocks = 0, block_size = 0, total_inodes = 0, free_inodes = 0;

								int nScan = sscanf ((const char *)recv.Data, "flash_size=%d;total_blocks=%d;free_blocks=%d;block_size=%d;total_inodes=%d;free_inodes=%d;",
									&flash_size, &total_blocks, &free_blocks, &block_size, &total_inodes, &free_inodes);

								if (nScan == 6) {
									double dUsed =  ((double)(total_blocks - free_blocks) / (double)total_blocks) * 100.0;
									CString strTmp;

									strTmp.Format (_T ("Usage=%.02f%%;"), dUsed);
									str = strTmp + str;
								}
							}
							
							pLV->SetItemText (nItem, 2, str);

							bMatch = true;
							break;
						}
					}
				}

				if (!bMatch) {
					IPCSTRUCT * p = new IPCSTRUCT;

					TRACEF (_T ("Receive: ") + ToString (recv));
					memset (p, 0, sizeof (IPCSTRUCT));
					p->send.Header.BatchID = -1;
					p->send.Header.Cmd = recv.Header.Cmd;
					memcpy (&p->recv, &recv, sizeof (recv));
					dlg.Add (p);
				}
			}
		}
	}
}

ULONG CALLBACK CIpDemoDlg::ThreadFunc (LPVOID lpData)
{
	ASSERT (lpData);
	
	CIpDemoDlg & dlg = * (CIpDemoDlg *)lpData;
	bool bConnected = false;
	CString strAddress, strConnect;
	ULONG lBatchID = 0;

	if (CButton * p = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) {
		p->GetWindowText (strConnect);
		p->SetWindowText (_T ("Connecting..."));
		p->EnableWindow (FALSE);
	}

	if (CButton * p = (CButton *)dlg.GetDlgItem (IDC_ADDRESS))
		p->EnableWindow (FALSE);

	if (CIPAddressCtrl * p = (CIPAddressCtrl *)dlg.GetDlgItem (IDC_ADDRESS)) {
		BYTE n [4] = { 0 };

		if (p->GetAddress (n [0], n [1], n [2], n [3])) 
			strAddress.Format (_T ("%d.%d.%d.%d"), n [0], n [1], n [2], n [3]);
	}

	TRACEF (_T ("Attempting connection: ") + strAddress);

	if (!::AfxSocketInit ()) {
		AfxMessageBox (IDP_SOCKETS_INIT_FAILED);
		return 0;
	}

	CSocket s;

	if (!s.Create (0, SOCK_STREAM)) {
		TRACEF (_T ("Create failed"));
		return 0;
	}


	dlg.BeginWaitCursor ();
	bool bConnect = s.Connect (strAddress, _FJ_SOCKET_NUMBER) ? true : false;
	dlg.EndWaitCursor ();


	if (!bConnect) {
		dlg.MessageBox (_T ("Failed to connect: ") + strAddress);
	}
	else {
		TRACEF (_T ("Established connection on: ") + strAddress);
		bConnected = true;

		if (CIPAddressCtrl * p = (CIPAddressCtrl *)dlg.GetDlgItem (IDC_ADDRESS)) {
			BYTE n [4] = { 0 };

			if (p->GetAddress (n [0], n [1], n [2], n [3])) {
				theApp.WriteProfileInt (_T ("IP Address"), _T ("1"), n [0]);
				theApp.WriteProfileInt (_T ("IP Address"), _T ("2"), n [1]);
				theApp.WriteProfileInt (_T ("IP Address"), _T ("3"), n [2]);
				theApp.WriteProfileInt (_T ("IP Address"), _T ("4"), n [3]);
			}
		}

		if (CButton * pConnect = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) 
			pConnect->SetWindowText (_T ("&Disconnect"));
	}

	if (CButton * p = (CButton *)dlg.GetDlgItem (BTN_CONNECT))
		p->EnableWindow (TRUE);

	while (bConnected) {
		if (::WaitForSingleObject (dlg.m_hExitEvent, 250) == WAIT_OBJECT_0) {
			::ResetEvent (dlg.m_hExitEvent);
			bConnected = false;
			break;
		}

		if (CheckTx (dlg, s, lBatchID)) 
			CheckRx (dlg, s);
		else
			bConnected = false;
	}

	TRACEF (_T ("Closing connection: ") + strAddress + _T (" ..."));
	s.Close ();

	::CloseHandle (dlg.m_hExitEvent);
	dlg.m_hExitEvent = NULL;
	dlg.m_hThread = NULL;

	if (CButton * pConnect = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) 
		pConnect->SetWindowText (strConnect);

	if (CButton * p = (CButton *)dlg.GetDlgItem (IDC_ADDRESS))
		p->EnableWindow (TRUE);

	TRACEF (_T ("Closed"));

	return 0;
}

void CIpDemoDlg::OnSelchangeScript() 
{
	CComboBox * pScript = (CComboBox *)GetDlgItem (CB_SCRIPT);

	ASSERT (pScript);

	ULONG lScriptID = pScript->GetItemData (pScript->GetCurSel ());

	OnClear ();

	try {
		CDaoRecordset rst (&m_db);
		CString str;

		str.Format (_T ("SELECT * FROM ScriptCmds WHERE ScriptID=%d ORDER BY [Order] ASC;"), lScriptID);
		rst.Open (dbOpenSnapshot, str, dbReadOnly);

		while (!rst.IsEOF ()) {
			ULONG lCmdID = rst.GetFieldValue (_T ("CmdID")).lVal;
			CString strData = (LPCTSTR)rst.GetFieldValue (_T ("Data")).bstrVal;

			Add (lCmdID, strData);

			rst.MoveNext ();
		}
		
		rst.Close ();
		theApp.WriteProfileInt (_T ("Settings"), _T ("lScriptID"), lScriptID);
	}
	catch (CDaoException * e)		{ e->ReportError (); e->Delete (); ASSERT (0); }
	catch (CMemoryException * e)	{ e->ReportError (); e->Delete (); ASSERT (0); }

}

void CIpDemoDlg::OnFont() 
{
	CString strFile = theApp.GetProfileString (_T ("IpDemo"), _T ("Font"), _T ("C:\\Foxjet\\IP\\Fonts\\fx5x6.FJF"));

	CFileDialog dlg (TRUE, _T ("*.fjf"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Foxjet font files (*.fjf)|*.fjf|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDCANCEL)
		return;

	strFile = dlg.GetPathName ();
	theApp.WriteProfileString (_T ("IpDemo"), _T ("Font"), strFile);

	CFile file;
	PUCHAR pBuf = NULL;
	DWORD dwLen;
	
	BeginWaitCursor ();

	try 
	{
		TRACEF (strFile);

		if (file.Open (strFile, CFile::modeRead|CFile::shareDenyWrite)) {
			bool bFailed = false;
			//CPacket reply;

			dwLen = file.GetLength ();
			pBuf = new UCHAR [dwLen];
			memset (pBuf, NULL, dwLen);
			DWORD dwActual = file.Read (pBuf, dwLen);
			file.Close();

			CSingleLock lock (&m_cs);
			int nIndex = -1;

			lock.Lock ();

			if (lock.IsLocked ()) {
				IPCSTRUCT * pItem = new IPCSTRUCT;
				CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

				ASSERT (pLV);

				memset (pItem, 0, sizeof (IPCSTRUCT));

				pItem->send.Header.Cmd = IPC_PUT_FONT;
				pItem->send.Header.BatchID = 0;
				
				pItem->send.Header.Length = dwLen;
				pItem->m_pBuffer = pBuf;

				//ASSERT (dwLen <= _FJ_SOCKET_BUFFER_SIZE);
				//memcpy (pItem->send.Data, pBuf, dwLen);

				OnClear ();

				{
					IPCSTRUCT * pItem = new IPCSTRUCT;
					memset (pItem, 0, sizeof (IPCSTRUCT));

					pItem->send.Header.Cmd = IPC_EDIT_START;
					pItem->send.Header.BatchID = 0;
					pItem->send.Header.Length = 0;
					Add (pItem);
				}

				//Send (IPC_PUT_FONT, batchID, pBuf, dwLen, &reply);
				nIndex = Add (pItem);

				{
					IPCSTRUCT * pItem = new IPCSTRUCT;
					memset (pItem, 0, sizeof (IPCSTRUCT));

					pItem->send.Header.Cmd = IPC_EDIT_SAVE;
					pItem->send.Header.BatchID = 0;
					pItem->send.Header.Length = 0;
					Add (pItem);
				}
			}

			//delete [] pBuf;
		}
	}
	catch ( CMemoryException *e)	{ e->ReportError (); e->Delete (); ASSERT (0); }
	catch ( CFileException *e)		{ e->ReportError (); e->Delete (); ASSERT (0); }

	EndWaitCursor ();
}

#define REVERSEWORD32(word) ( ((word&0x000000ff)<<24) | ((word&0x0000ff00)<<8) | ((word&0x00ff0000)>>8) | ((word&0xff000000)>>24) )
#define REVERSEWORD16(word) ( ((word&0x000000ff)<<8) | ((word&0x0000ff00)>>8) )

#define LOADREVERSEWORD32(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) | (((*(((BYTE*)pointer)+2)))<<16) | (((*(((BYTE*)pointer)+3)))<<24) )
#define LOADREVERSEWORD16(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) )

static BOOL fj_bmpCheckFormat( LPBYTE pbmp )
{
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LONG lLength;						// size of buffer in bytes
	LONG lOffset;						// bmp offset to rows of bits
	LONG lHeight;						// bmp height in bits
	LONG lWidth;						// bmp width in bits
	LONG lHeightBytes;					// bmp height in bytes
	LONG lWidthBytes;					// bmp width in bytes
	LONG lLengthCalc;					// calculated length in bytes
	BOOL bRet;

	bRet = FALSE;
	if ( NULL != pbmp )
	{
		pbmfh = (BITMAPFILEHEADER *)pbmp;
		pbmih = (BITMAPINFOHEADER *)(pbmp+sizeof(BITMAPFILEHEADER));
										// bmp magic number
		if ( ('B' == *pbmp) && ('M' == *(pbmp+1)) )
		{
			lLength = LOADREVERSEWORD32(&pbmfh->bfSize);
			lOffset = LOADREVERSEWORD32(&pbmfh->bfOffBits);

			// Windows bmp header length
			if ( sizeof(BITMAPINFOHEADER) == LOADREVERSEWORD32(&pbmih->biSize) )
			{
				if ( (1 == LOADREVERSEWORD16(&pbmih->biPlanes)) && (1 == LOADREVERSEWORD16(&pbmih->biBitCount)) && (0 == LOADREVERSEWORD32(&pbmih->biCompression)) )
				{
					lHeight = LOADREVERSEWORD32(&pbmih->biHeight);
					lWidth  = LOADREVERSEWORD32(&pbmih->biWidth);
					lHeightBytes =  (lHeight+ 7)/8;
					lWidthBytes  = ((lWidth +31)/32)*4;
					lLengthCalc = lOffset + (lHeight * lWidthBytes);
										// header plus rowdata must equal size
					if ( lLength == lLengthCalc )
					{
						bRet = TRUE;
					}
					// actual size might be rounded up to a multiple of 4
					else if ( (lLength > lLengthCalc) && (lLength <= (lLengthCalc+4)))
					{
						bRet = TRUE;
					}
				}
			}
		}
	}
	return( bRet );
}

void CIpDemoDlg::OnBitmap() 
{
	CFile file;
	bool bResult = false;
	CString strTitle;
	const CString strList [2] =
	{
		_T ("C:\\Foxjet\\IP\\Logos\\Bar32.bmp"),
		_T ("C:\\Foxjet\\IP\\Logos\\test.bmp"),
	};
	static int nCalls = 0;
	const CString strFile = strList [(nCalls++) % ARRAYSIZE (strList)];

	{
		TCHAR szTitle [MAX_PATH] = { 0 };

		::GetFileTitle (strFile, szTitle, ARRAYSIZE (szTitle) - 1);
		strTitle = szTitle;

		int nIndex = strTitle.Find (_T ("."));

		if (nIndex != -1) 
			strTitle = strTitle.Left (nIndex);
	}

	if (file.Open (strFile, CFile::modeRead | CFile::shareDenyWrite)) {
		CString sData;
		DWORD dwBitmapLen = file.GetLength();
		PUCHAR pBitmapData = new UCHAR [ dwBitmapLen ];

		memset ( pBitmapData, NULL, dwBitmapLen );
		file.Read ( pBitmapData, dwBitmapLen );
		file.Close();
		sData.Format ( _T ("{bmp,\"%s\",%lu}"), strTitle, dwBitmapLen );
		TRACEF (sData);
		
		// We must add 1 for NULL byte and we must ensure the NULL and end of Text string is TX'ed.
		DWORD dwTotalLen = sData.GetLength() + dwBitmapLen + 1;
		PUCHAR pData = new UCHAR [ dwTotalLen ];
		memset ( pData, NULL, dwTotalLen );
		PUCHAR p = pData;
		memcpy (p, w2a (sData), sData.GetLength ());
		p+= sData.GetLength() + 1;
		memcpy ( p, pBitmapData, dwBitmapLen );

		ASSERT (fj_bmpCheckFormat (pBitmapData));

		{
			IPCSTRUCT * pItem = new IPCSTRUCT;
			CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

			ASSERT (pLV);

			memset (pItem, 0, sizeof (IPCSTRUCT));

			pItem->send.Header.Cmd = IPC_PUT_BMP;
			pItem->send.Header.BatchID = 0;
			pItem->send.Header.Length = dwBitmapLen;

			ASSERT (dwBitmapLen <= _FJ_SOCKET_BUFFER_SIZE);
			memcpy (pItem->send.Data, pData, dwBitmapLen);

			{
				LPBYTE pData  = (LPBYTE)&(pItem->send.Data);
				char * pElement = NULL;

				pElement = (LPSTR)pData;
				pElement += strlen (pElement) + 1;
				ASSERT (fj_bmpCheckFormat ((LPBYTE)pElement));
			}

			OnClear ();

			{
				IPCSTRUCT * pItem = new IPCSTRUCT;
				memset (pItem, 0, sizeof (IPCSTRUCT));

				pItem->send.Header.Cmd = IPC_EDIT_START;
				pItem->send.Header.BatchID = 0;
				pItem->send.Header.Length = 0;
				Add (pItem);
			}

			//Send (IPC_PUT_BMP, GetNextSequence(), pData, dwTotalLen, NULL);
			int nIndex = Add (pItem);

			{
				IPCSTRUCT * pItem = new IPCSTRUCT;
				memset (pItem, 0, sizeof (IPCSTRUCT));

				pItem->send.Header.Cmd = IPC_EDIT_SAVE;
				pItem->send.Header.BatchID = 0;
				pItem->send.Header.Length = 0;
				Add (pItem);
			}
		}
		
		delete [] pData;
		delete [] pBitmapData;
	}
}


void CIpDemoDlg::OnResend() 
{
	OnSelchangeScript ();	
}

