#if !defined(AFX_CMDDLG_H__863F8C28_00FA_40C3_B6E9_4733B084D38C__INCLUDED_)
#define AFX_CMDDLG_H__863F8C28_00FA_40C3_B6E9_4733B084D38C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CmdDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCmdDlg dialog

class CCmdDlg : public CDialog
{
// Construction
public:
	CCmdDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCmdDlg)
	enum { IDD = IDD_CMD };
	long	m_lBatchID;
	CString	m_strData;
	CString	m_strResponse;
	//}}AFX_DATA

	DWORD m_dwCmd;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCmdDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CCmdDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMDDLG_H__863F8C28_00FA_40C3_B6E9_4733B084D38C__INCLUDED_)
