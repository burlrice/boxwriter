//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by IpDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IPDEMO_DIALOG               102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_CMD                         129
#define IDC_ADDRESS                     1000
#define BTN_CONNECT                     1001
#define LV_CMDS                         1002
#define BTN_NEW                         1003
#define BTN_PROPERTIES                  1004
#define CB_CMD                          1005
#define BTN_DEMO                        1005
#define TXT_DATA                        1006
#define BTN_CLEAR                       1006
#define TXT_RESPONSE                    1007
#define CB_SCRIPT                       1007
#define TXT_BATCHID                     1008
#define BTN_FONT                        1008
#define BTN_BITMAP                      1009
#define BTN_RESEND                      1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
