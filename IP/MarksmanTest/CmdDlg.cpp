// CmdDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IpDemo.h"
#include "CmdDlg.h"
#include "fj_socket.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCmdDlg dialog


CCmdDlg::CCmdDlg(CWnd* pParent /*=NULL*/)
:	m_dwCmd (IPC_STATUS),
	CDialog(CCmdDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCmdDlg)
	m_lBatchID = 0;
	m_strData = _T("");
	m_strResponse = _T("");
	//}}AFX_DATA_INIT
}


void CCmdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCmdDlg)
	DDX_Text(pDX, TXT_BATCHID, m_lBatchID);
	DDX_Text(pDX, TXT_DATA, m_strData);
	DDX_Text(pDX, TXT_RESPONSE, m_strResponse);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCmdDlg, CDialog)
	//{{AFX_MSG_MAP(CCmdDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCmdDlg message handlers

BOOL CCmdDlg::OnInitDialog() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_CMD);

	ASSERT (pCB);
	CDialog::OnInitDialog();

	for (DWORD dw = IPC_COMPLETE; dw <= IPC_GET_STATUS_MODE; dw++) {

		switch (dw) {
		case IPC_FIRMWARE_UPGRADE:
		case IPC_GA_UPGRADE:
			break; // not impl
		default:
			{
				CString str = GetIpcCmdName (dw);
				
				/*
				{
					if (str.GetLength ()) {
						CString strTmp;
						strTmp.Format ("%d, %s", dw, GetIpcCmdName (dw));
						TRACEF (strTmp);
					}
				}
				*/

				if (str.GetLength ()) {
					int nIndex = pCB->AddString (str);

					pCB->SetItemData (nIndex, dw);

					if (m_dwCmd == dw)
						pCB->SetCurSel (nIndex);
				}
			}
			break;
		}
	}
	
	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCmdDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_CMD);

	ASSERT (pCB);

	m_dwCmd = pCB->GetItemData (pCB->GetCurSel ());

	CDialog::OnOK ();
}
