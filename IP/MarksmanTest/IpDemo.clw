; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CIpDemoDlg
LastTemplate=CSocket
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "IpDemo.h"

ClassCount=5
Class1=CIpDemoApp
Class2=CIpDemoDlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_IPDEMO_DIALOG
Class4=CCmdDlg
Class5=CIpSocket
Resource4=IDD_CMD

[CLS:CIpDemoApp]
Type=0
HeaderFile=IpDemo.h
ImplementationFile=IpDemo.cpp
Filter=N

[CLS:CIpDemoDlg]
Type=0
HeaderFile=IpDemoDlg.h
ImplementationFile=IpDemoDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CB_SCRIPT

[CLS:CAboutDlg]
Type=0
HeaderFile=IpDemoDlg.h
ImplementationFile=IpDemoDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_IPDEMO_DIALOG]
Type=1
Class=CIpDemoDlg
ControlCount=13
Control1=IDC_ADDRESS,SysIPAddress32,1342242816
Control2=BTN_CONNECT,button,1342242817
Control3=CB_SCRIPT,combobox,1344340227
Control4=IDC_STATIC,static,1342308352
Control5=LV_CMDS,SysListView32,1350664205
Control6=BTN_NEW,button,1342242816
Control7=BTN_PROPERTIES,button,1342242816
Control8=BTN_DEMO,button,1342242816
Control9=BTN_CLEAR,button,1342242816
Control10=IDC_STATIC,static,1342308352
Control11=BTN_FONT,button,1342242816
Control12=BTN_BITMAP,button,1342242816
Control13=BTN_RESEND,button,1342242816

[DLG:IDD_CMD]
Type=1
Class=CCmdDlg
ControlCount=11
Control1=CB_CMD,combobox,1344340227
Control2=TXT_DATA,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=TXT_BATCHID,edit,1350633600
Control6=TXT_RESPONSE,edit,1350633600
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,button,1342177287
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352

[CLS:CCmdDlg]
Type=0
HeaderFile=CmdDlg.h
ImplementationFile=CmdDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CB_CMD

[CLS:CIpSocket]
Type=0
HeaderFile=IpSocket.h
ImplementationFile=IpSocket.cpp
BaseClass=CSocket
Filter=N
VirtualFilter=uq

