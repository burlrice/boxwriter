#include "stdafx.h"
#include "Debug.h"
#include "fj_socket.h"

#ifndef ARRAYSIZE
	#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct 
{
	DWORD	m_dw;
	LPCTSTR m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),				},
};

CString GetIpcCmdName (DWORD dw)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return "";
}

DWORD GetIpcCmd (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!str.CompareNoCase (::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}
