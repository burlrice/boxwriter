// FJSocketHeader.cpp : implementation file
//

#include "stdafx.h"
#include "FJSocketHeader.h"


CFJSocketHeader::CFJSocketHeader()
{
	m_BachID	= 0;
	m_DataLength= 0;
	m_SocketCmd	= 0;
	pBuffer	= new BYTE[FJSH_BUFSIZE];
	memset(pBuffer,0,FJSH_BUFSIZE);
}

CFJSocketHeader::~CFJSocketHeader()
{
	if (pBuffer != NULL)
	{
		delete [] pBuffer;
		pBuffer = NULL;
	}
}


void CFJSocketHeader::SetBachID(ULONG lID)
{
	this->m_BachID = lID;
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	pHeader->dBatchID[3] = (UCHAR) ( lID &0x00FF);
	pHeader->dBatchID[2] = (UCHAR) ((lID >>8) &0x00FF);
	pHeader->dBatchID[1] = (UCHAR) ((lID >>16)&0x00FF);
	pHeader->dBatchID[0] = (UCHAR) ((lID >>24)&0x00FF);
}

ULONG CFJSocketHeader::GetBachID()
{
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	this->m_BachID = pHeader->dBatchID[3] +
					(pHeader->dBatchID[2] << 8 ) +
					(pHeader->dBatchID[1] << 16) +
					(pHeader->dBatchID[0] << 24);
	return this->m_BachID;
}

void CFJSocketHeader::SetSocketCMD(ULONG lCmd)
{
	this->m_SocketCmd = lCmd;
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	pHeader->dCmd[3] = (UCHAR) ( lCmd &0x00FF);
	pHeader->dCmd[2] = (UCHAR) ((lCmd >>8) &0x00FF);
	pHeader->dCmd[1] = (UCHAR) ((lCmd >>16)&0x00FF);
	pHeader->dCmd[0] = (UCHAR) ((lCmd >>24)&0x00FF);
}

ULONG CFJSocketHeader::GetSocketCMD()
{
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	this->m_SocketCmd = pHeader->dCmd[3] +
					   (pHeader->dCmd[2] << 8 ) +
					   (pHeader->dCmd[1] << 16) +
					   (pHeader->dCmd[0] << 24);
	return this->m_SocketCmd;
}

void CFJSocketHeader::SetDataLength(ULONG lDLen)
{
	this->m_DataLength = lDLen;
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	pHeader->dDataLenght[3] = (UCHAR) ( lDLen &0x00FF);
	pHeader->dDataLenght[2] = (UCHAR) ((lDLen >>8) &0x00FF);
	pHeader->dDataLenght[1] = (UCHAR) ((lDLen >>16)&0x00FF);
	pHeader->dDataLenght[0] = (UCHAR) ((lDLen >>24)&0x00FF);
}

ULONG CFJSocketHeader::GetDataLength()
{
	fj_socket_header *pHeader = (fj_socket_header *)pBuffer;
	this->m_DataLength = pHeader->dDataLenght[3] +
						(pHeader->dDataLenght[2] << 8 ) +
						(pHeader->dDataLenght[1] << 16) +
						(pHeader->dDataLenght[0] << 24);
	return this->m_DataLength;
}

BYTE* CFJSocketHeader::GetBuffer()
{
	return this->pBuffer;
}

UINT CFJSocketHeader::GetBufferSize()
{
	UINT size = FJSH_BUFSIZE;
	return size;
}

void CFJSocketHeader::SetBuffer(BYTE *pBuf)
{
	memcpy(pBuffer,pBuf,FJSH_BUFSIZE);
}
