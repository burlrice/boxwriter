// IPBackup.h : main header file for the IPBACKUP application
//

#if !defined(AFX_IPBACKUP_H__3D6B2F91_8475_495F_81C6_2B65F0DC9F82__INCLUDED_)
#define AFX_IPBACKUP_H__3D6B2F91_8475_495F_81C6_2B65F0DC9F82__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIPBackupApp:
// See IPBackup.cpp for the implementation of this class
//

class CIPBackupApp : public CWinApp
{
public:
	CIPBackupApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPBackupApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CIPBackupApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPBACKUP_H__3D6B2F91_8475_495F_81C6_2B65F0DC9F82__INCLUDED_)
