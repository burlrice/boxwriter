; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CIPBackupDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ipbackup.h"
LastPage=0

ClassCount=6
Class1=CCmdSocket
Class2=CInfoTransfer
Class3=CIPBackupApp
Class4=CAboutDlg
Class5=CIPBackupDlg
Class6=CUDPasocket

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDD_IPBACKUP_DIALOG
Resource3=IDD_INFOTRANS

[CLS:CCmdSocket]
Type=0
BaseClass=CSocket
HeaderFile=CmdSocket.h
ImplementationFile=CmdSocket.cpp

[CLS:CInfoTransfer]
Type=0
BaseClass=CDialog
HeaderFile=InfoTransfer.h
ImplementationFile=InfoTransfer.cpp

[CLS:CIPBackupApp]
Type=0
BaseClass=CWinApp
HeaderFile=IPBackup.h
ImplementationFile=IPBackup.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=IPBackupDlg.cpp
ImplementationFile=IPBackupDlg.cpp
LastObject=CAboutDlg

[CLS:CIPBackupDlg]
Type=0
BaseClass=CDialog
HeaderFile=IPBackupDlg.h
ImplementationFile=IPBackupDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=CIPBackupDlg

[CLS:CUDPasocket]
Type=0
BaseClass=CAsyncSocket
HeaderFile=UDPasocket.h
ImplementationFile=UDPasocket.cpp

[DLG:IDD_INFOTRANS]
Type=1
Class=CInfoTransfer
ControlCount=6
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_TransFrom,static,1342308354
Control4=IDC_TransTo,static,1342308354
Control5=IDC_TDATA,static,1342308353
Control6=IDC_STATIC,button,1342177543

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_IPBACKUP_DIALOG]
Type=1
Class=CIPBackupDlg
ControlCount=8
Control1=IDBACKUP,button,1342275585
Control2=IDRESTORE,button,1342275584
Control3=IDUPDATE,button,1342275584
Control4=IDCANCEL,button,1342275584
Control5=IDC_IPADDRESS,SysIPAddress32,1342242816
Control6=IDC_STATIC,button,1342177287
Control7=IDC_PRINTERLIST,listbox,1486946561
Control8=IDC_STATIC,button,1342177287

