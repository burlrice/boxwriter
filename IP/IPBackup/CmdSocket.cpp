// CmdSocket.cpp : implementation file
//

#include "stdafx.h"
#include "ipbackup.h"
#include "IPBackupDlg.h"
#include "FJSocketHeader.h"
#include "CmdSocket.h"
#include "FJ_SOCKET.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCmdSocket

CCmdSocket::CCmdSocket(CIPBackupDlg *pDlg)
{
	m_pDlg = pDlg;
	pDataBuf = NULL;
}

CCmdSocket::~CCmdSocket()
{
	if (pDataBuf != NULL) delete [] pDataBuf;
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CCmdSocket, CSocket)
	//{{AFX_MSG_MAP(CCmdSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CCmdSocket member functions

void CCmdSocket::OnAccept(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CSocket::OnAccept(nErrorCode);
}

void CCmdSocket::OnClose(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CSocket::OnClose(nErrorCode);
}

void CCmdSocket::OnConnect(int nErrorCode) 
{
	if (!nErrorCode)
	{
		TCHAR szError[256];
		wsprintf(szError, "OnConnect error: %d", nErrorCode);
		AfxMessageBox(szError);
		AfxMessageBox("Please close the application");
	}
	CSocket::OnConnect(nErrorCode);
}

BOOL CCmdSocket::OnMessagePending() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CSocket::OnMessagePending();
}

void CCmdSocket::OnOutOfBandData(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CSocket::OnOutOfBandData(nErrorCode);
}

void CCmdSocket::OnReceive(int nErrorCode) 
{

	CFJSocketHeader fjHeader;
	CString TempStr;
	int iRead;

	iRead = Receive(fjHeader.GetBuffer(), fjHeader.GetBufferSize()); 
	if (iRead != SOCKET_ERROR)
	{
		if ((iRead != 0) && ((UINT)iRead == fjHeader.GetBufferSize()))
		{
			ULONG NewCmd = fjHeader.GetSocketCMD();
			ULONG DataLen= fjHeader.GetDataLength();
			if (DataLen !=0)
			{
				pDataBuf = new BYTE[DataLen+1];
				iRead = Receive(pDataBuf, DataLen);
				pDataBuf[iRead] = 0;
				TempStr = pDataBuf;
				delete [] pDataBuf;
				pDataBuf = NULL;
			}
			bool bFound = false;
			for (int i=0; i< LegalCmds.GetSize();i++)
			{
				if (NewCmd == LegalCmds.GetAt(i))
				{
					bFound = true;
					break;
				}
			}
			if (bFound == true)
				m_pDlg->ProcSocketCmd(&fjHeader,&TempStr);
		}
		//else Close();
	}
	else
	{
		TempStr.Format("Error number is %d", GetLastError());
		AfxMessageBox (TempStr);
	}
	
	CSocket::OnReceive(nErrorCode);
}

void CCmdSocket::OnSend(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CSocket::OnSend(nErrorCode);
}

int CCmdSocket::Receive(void* lpBuf, int nBufLen, int nFlags) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CSocket::Receive(lpBuf, nBufLen, nFlags);
}

int CCmdSocket::Send(const void* lpBuf, int nBufLen, int nFlags) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CSocket::Send(lpBuf, nBufLen, nFlags);
}
