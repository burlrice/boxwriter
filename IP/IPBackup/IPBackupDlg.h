// IPBackupDlg.h : header file
//

#if !defined(AFX_IPBACKUPDLG_H__D66D281B_F98E_4A8F_8821_2C22EEEDAA1F__INCLUDED_)
#define AFX_IPBACKUPDLG_H__D66D281B_F98E_4A8F_8821_2C22EEEDAA1F__INCLUDED_

#include "CmdSocket.h"	// Added by ClassView
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER > 1000

class CCmdSocket;
class CUDPasocket;
class CInfoTransfer;
class CFJSocketHeader;

/////////////////////////////////////////////////////////////////////////////
// CIPBackupDlg dialog

class CIPBackupDlg : public CDialog
{
// Construction
public:
	void SaveEditSession();
	void InitTransConect();
	bool bSetTarget;
	CString LocalIPAddr;
	CString LocalFile;
	void CloseTransConect();
	void TransFTP();
	void SetEditSession();
	UINT comstate;
	void ParseString(CString* pStr,CStringArray* pList, CHAR delim, UINT maxsize=0);
	bool bTargetReady;
	bool bReceivedCmd;
	void ProcSocketCmd(CFJSocketHeader* pHeader,CString* pData);
	CIPBackupDlg(CWnd* pParent = NULL);	// standard constructor

	void EnableElements(BOOL Action = TRUE);
	bool bCancelTrans;
	void SetWebPWD(CInternetSession *pSession);
	void UpdatePList(CString PName,CString PAddr);
	CStringArray m_PrinterIPaddr;
	CStringArray m_PrinterName;
	CUDPasocket* m_pUDPSocket;
	CCmdSocket* m_pCmdSocket;

// Dialog Data
	//{{AFX_DATA(CIPBackupDlg)
	enum { IDD = IDD_IPBACKUP_DIALOG };
	CButton	m_UpdateFirmware;
	CButton	m_Cancel;
	CListBox	m_PrinterList;
	CIPAddressCtrl	m_RemoteAddr;
	CButton	m_Restore;
	CButton	m_Backup;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPBackupDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CInfoTransfer *pInfoDlg;

	// Generated message map functions
	//{{AFX_MSG(CIPBackupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBackup();
	afx_msg void OnRestore();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangePrinterlist();
	virtual void OnCancel();
	afx_msg void OnUpdate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool isUpdate;
	int nSaveEdit;
	CString LocalFilePath;
	CString PrinterAddress;
	CString m_RemoteFileName;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPBACKUPDLG_H__D66D281B_F98E_4A8F_8821_2C22EEEDAA1F__INCLUDED_)
