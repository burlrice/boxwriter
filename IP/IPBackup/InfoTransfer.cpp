// InfoTransfer.cpp : implementation file
//

#include "stdafx.h"
#include "ipbackup.h"
#include "InfoTransfer.h"
#include "IPBackupDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfoTransfer dialog


CInfoTransfer::CInfoTransfer(CWnd* pParent /*=NULL*/)
	: CDialog(CInfoTransfer::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInfoTransfer)
	m_sTransFrom = _T("");
	m_sTransTo = _T("");
	m_sTData = _T("");
	//}}AFX_DATA_INIT
	ASSERT(pParent != NULL);

	m_pParent = pParent;
	m_nID = CInfoTransfer::IDD;
}


void CInfoTransfer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoTransfer)
	DDX_Text(pDX, IDC_TransFrom, m_sTransFrom);
	DDX_Text(pDX, IDC_TransTo, m_sTransTo);
	DDX_Text(pDX, IDC_TDATA, m_sTData);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInfoTransfer, CDialog)
	//{{AFX_MSG_MAP(CInfoTransfer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInfoTransfer message handlers

void CInfoTransfer::OnCancel() 
{
	((CIPBackupDlg*)m_pParent)->bCancelTrans = true;
	DestroyWindow();
}


void CInfoTransfer::PostNcDestroy()
{
	delete this;
}

BOOL CInfoTransfer::Create()
{
	return CDialog::Create(m_nID, m_pParent);
}
