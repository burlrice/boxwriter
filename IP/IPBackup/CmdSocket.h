#if !defined(AFX_CMDSOCKET_H__A1E636D3_1DF0_49CC_ABE4_6DCEBD801DB6__INCLUDED_)
#define AFX_CMDSOCKET_H__A1E636D3_1DF0_49CC_ABE4_6DCEBD801DB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CmdSocket.h : header file
//

#define CMDSOC_BUFSIZE 4096
static  UINT FJ_CMD_PORT = 1281;

class CIPBackupDlg;
/////////////////////////////////////////////////////////////////////////////
// CCmdSocket command target

class CCmdSocket : public CSocket
{
	CIPBackupDlg *m_pDlg;
// Attributes
public:

// Operations
public:
	CCmdSocket(CIPBackupDlg *pDlg);
	virtual ~CCmdSocket();

// Overrides
public:
	CUIntArray LegalCmds;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCmdSocket)
	public:
	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual BOOL OnMessagePending();
	virtual void OnOutOfBandData(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	virtual int Receive(void* lpBuf, int nBufLen, int nFlags = 0);
	virtual int Send(const void* lpBuf, int nBufLen, int nFlags = 0);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CCmdSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
private:
	BYTE* pDataBuf;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMDSOCKET_H__A1E636D3_1DF0_49CC_ABE4_6DCEBD801DB6__INCLUDED_)
