//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by IPBackup.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IPBACKUP_DIALOG             102
#define IDD_INFOBAR                     103
#define IDR_MAINFRAME                   128
#define IDD_INFOTRANS                   131
#define IDC_IPADDRESS                   1000
#define IDBACKUP                        1001
#define IDRESTORE                       1002
#define IDC_PRINTERLIST                 1003
#define IDUPDATE                        1004
#define IDC_OPInfo                      1007
#define IDC_DInfo                       1008
#define IDC_TransFrom                   1009
#define IDC_TransTo                     1010
#define IDC_TDATA                       1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
