// IPBackupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UDPasocket.h"
#include "IPBackup.h"
#include "FJSocketHeader.h"
#include "CmdSocket.h"
#include "InfoTransfer.h"
#include "IPBackupDlg.h"
#include "FJ_SOCKET.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPBackupDlg dialog

CIPBackupDlg::CIPBackupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIPBackupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIPBackupDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pUDPSocket = NULL;
	m_pCmdSocket = NULL;
	pInfoDlg = NULL;

	isUpdate = false;
	bCancelTrans = false;
	bReceivedCmd = false;
	bTargetReady = false;
	bSetTarget   = false;
	m_RemoteFileName = "";
	comstate = 0;
}

void CIPBackupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIPBackupDlg)
	DDX_Control(pDX, IDUPDATE, m_UpdateFirmware);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	DDX_Control(pDX, IDC_PRINTERLIST, m_PrinterList);
	DDX_Control(pDX, IDC_IPADDRESS, m_RemoteAddr);
	DDX_Control(pDX, IDRESTORE, m_Restore);
	DDX_Control(pDX, IDBACKUP, m_Backup);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIPBackupDlg, CDialog)
	//{{AFX_MSG_MAP(CIPBackupDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDBACKUP, OnBackup)
	ON_BN_CLICKED(IDRESTORE, OnRestore)
	ON_WM_DESTROY()
	ON_LBN_SELCHANGE(IDC_PRINTERLIST, OnSelchangePrinterlist)
	ON_BN_CLICKED(IDUPDATE, OnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPBackupDlg message handlers

BOOL CIPBackupDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	CString SocketErr;
	int flag = 1;

	m_pUDPSocket = new CUDPasocket(this);
	if (NULL == m_pUDPSocket)
	{
		AfxMessageBox ("Failed to allocate UDP socket!");
	}
	else
	{
		if (!m_pUDPSocket->Create(FJ_UDP_PORT, SOCK_DGRAM))
		{
			SocketErr.Format("Failed to create UDP socket: %d",m_pUDPSocket->GetLastError());
			AfxMessageBox (SocketErr);
			delete m_pUDPSocket;
			m_pUDPSocket = NULL;
		}
		else if (!m_pUDPSocket->SetSockOpt(SO_BROADCAST, &flag, sizeof(int)))
		{
			SocketErr.Format("UDP SetSockOpt failed to set SO_BROADCAST:% d",m_pUDPSocket->GetLastError());
			AfxMessageBox (SocketErr);
			delete m_pUDPSocket;
			m_pUDPSocket = NULL;
		}
		else
			this->m_PrinterList.EnableWindow();
	}
	//if (NULL != m_pUDPSocket) m_pUDPSocket->GetSockName(LocalIPAddr,port);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIPBackupDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIPBackupDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIPBackupDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CIPBackupDlg::OnBackup() 
{
	m_RemoteFileName = "AllData";
	UpdateData();
	DWORD dwAddress;
	this->m_RemoteAddr.GetAddress(dwAddress);
	if (0 == dwAddress)
	{
		AfxMessageBox("Please select printer or enter valid Remote Address");
		return;
	}

	in_addr iaddr;
	iaddr.S_un.S_addr = ntohl(dwAddress);

	CString PrinterAddress (inet_ntoa(iaddr));
	CString LocalFile(m_RemoteFileName);

	CString PrintName;

	for (int i=0; i<this->m_PrinterIPaddr.GetSize(); i++)
	{
		if ( 0 == PrinterAddress.Compare(m_PrinterIPaddr.GetAt(i)))
		{
			CTime STime = CTime::GetCurrentTime();
			CString s = STime.Format( ".%y%m%d.%H%M." );
			PrintName = this->m_PrinterName.GetAt(i);
			LocalFile = PrintName + s + LocalFile;
			break;
		}
	}

	CFileDialog fd(FALSE, NULL, LocalFile);
	if (fd.DoModal()!=IDOK) return;
	EnableElements(FALSE);

	LocalFile = fd.GetPathName();

	CInternetSession session;
	SetWebPWD(&session);
	CInternetSession FTPsession;
	CFtpConnection* pFTPconnect = FTPsession.GetFtpConnection(PrinterAddress,_T("anonymous"),_T(""));

	if ( NULL != pFTPconnect)
	{
		if ( pInfoDlg == NULL)
		{
			pInfoDlg = new CInfoTransfer(this);
			pInfoDlg->Create();
		}
		else
			pInfoDlg->SetActiveWindow();
		bCancelTrans = false;
		CString TmpStr;
/*		int len = LocalFile.GetLength();
		if (len > 35)
		{
			len-=35;
			TmpStr.Format("...%s",LocalFile.Mid(len));
		}
		else TmpStr = LocalFile;
*/
		TmpStr = fd.GetFileName();
		pInfoDlg->m_sTransFrom = PrinterAddress;
		pInfoDlg->m_sTransTo = TmpStr;
		double dTrans = 0;
		TmpStr.Format("Received: %.1f Kb",dTrans);
		pInfoDlg->m_sTData = TmpStr;
		//if (pInfoDlg->Create() == TRUE)
		//	int asd = 0;

		CInternetFile *remoteFile = NULL;
		CFile		  *outFile    = NULL;
		outFile = new CFile(LocalFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite);
		remoteFile = pFTPconnect->OpenFile(m_RemoteFileName);

		int buf_len = 2048;
		char *pBuffer = new char[buf_len];

		if ( NULL != pBuffer)
		{
			UINT readbytes = remoteFile->Read(pBuffer,buf_len);
			while( readbytes > 0 )
			{
				dTrans += readbytes;
				TmpStr.Format("Received: %.1f Kb",dTrans/1024);
				pInfoDlg->m_sTData = TmpStr;
				pInfoDlg->UpdateData(FALSE);
				outFile->Write(pBuffer,readbytes);
				readbytes = remoteFile->Read(pBuffer,buf_len);
			}
			remoteFile->Close();
			outFile->Close();

			delete [] pBuffer;
		}

		delete remoteFile;
		delete outFile;
		if (NULL != pInfoDlg) delete pInfoDlg;
		pInfoDlg = NULL;


		pFTPconnect->Close();
		delete pFTPconnect;
	}


	session.Close();
	FTPsession.Close();

	EnableElements();
}

void CIPBackupDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if (m_pCmdSocket != NULL)
	{
		m_pCmdSocket->Close();
		delete m_pCmdSocket;
		m_pCmdSocket = NULL;
	}	
	if (m_pUDPSocket != NULL)
	{
		m_pUDPSocket->Close();
		delete m_pUDPSocket;
		m_pUDPSocket = NULL;
	}	
	
}

void CIPBackupDlg::UpdatePList(CString PName, CString PAddr)
{
	BOOL bFound = FALSE;

	for (int i=0; i<m_PrinterName.GetSize();i++)
	{
		if (0 == PName.Compare(m_PrinterName.GetAt(i)))
		{
			m_PrinterIPaddr.SetAt(i,PAddr);
			bFound = TRUE;
			break;
		}
	}
	if (FALSE == bFound)
	{
		m_PrinterName.Add(PName);
		m_PrinterIPaddr.Add(PAddr);
		int iLen = 45;
		iLen -= PName.GetLength() + PAddr.GetLength();
		if (iLen < 1) iLen = 1;
		CString FStr(' ',iLen);
		FStr = "   "+ PName + FStr + PAddr;
		m_PrinterList.GetHorizontalExtent();
		m_PrinterList.AddString(FStr);
	}

}

void CIPBackupDlg::OnSelchangePrinterlist() 
{
	int id = this->m_PrinterList.GetCurSel();
	this->m_RemoteAddr.SetAddress(ntohl(inet_addr(this->m_PrinterIPaddr.GetAt(id))));
}

void CIPBackupDlg::SetWebPWD(CInternetSession *pSession)
{
	DWORD dwAddress;
	this->m_RemoteAddr.GetAddress(dwAddress);
	if (0 == dwAddress)
	{
		AfxMessageBox("Please select printer or enter valid Remote Address");
		return;
	}

	in_addr iaddr;
	iaddr.S_un.S_addr = ntohl(dwAddress);

	CString PrinterAddress (inet_ntoa(iaddr));
	CString strHeaders = _T("Content-Type: application/x-www-form-urlencoded");
	CString strFormData = _T("pwid=&password=");
	CHttpConnection* pHTTPconnect = pSession->GetHttpConnection(PrinterAddress);
	CHttpFile* pFile = pHTTPconnect->OpenRequest(CHttpConnection::HTTP_VERB_POST, _T("/pw_1"));
	BOOL result = pFile->SendRequest(strHeaders, (LPVOID)(LPCTSTR)strFormData, strFormData.GetLength());
	pHTTPconnect->Close();
	delete pFile;
	delete pHTTPconnect;
}

BOOL CIPBackupDlg::DestroyWindow() 
{
	//if ( pInfoDlg != NULL) delete pInfoDlg;
	
	return CDialog::DestroyWindow();
}

void CIPBackupDlg::EnableElements(BOOL Action)
{
	this->m_Backup.EnableWindow(Action);
	this->m_Restore.EnableWindow(Action);
	this->m_RemoteAddr.EnableWindow(Action);
	this->m_PrinterList.EnableWindow(Action);
	this->m_Cancel.EnableWindow(Action);
	this->m_UpdateFirmware.EnableWindow(Action);
}

void CIPBackupDlg::OnCancel() 
{
	CDialog::OnCancel();
}

void CIPBackupDlg::ParseString(CString *pStr, CStringArray *pList, CHAR delim, UINT maxsize)
{
	if ( (NULL != pStr) && (NULL != pList))
	{
		pList->RemoveAll();
		CString SrcStr;
		SrcStr = *pStr;
		SrcStr.TrimLeft ('{');
		SrcStr.TrimRight('}');
		int index = 0;
		while( (0 != SrcStr.GetLength()))
		{
			index = SrcStr.Find(delim);
			if (index == -1)
			{
				pList->Add(SrcStr);
				break;
			}
			pList->Add(SrcStr.Mid(0,index));
			SrcStr = SrcStr.Mid(index+1);
		}
	}
}

void CIPBackupDlg::ProcSocketCmd(CFJSocketHeader *pHeader, CString *pData)
{
	bReceivedCmd = false;
	bTargetReady = false;
	bSetTarget   = false;

	CString TempStr;
	CStringArray ParamList;
	TempStr = *pData; //"EDIT_LOCK=SET;EDIT_IP=n.n.n.n;EDIT_TIME=yyyy/mm/dd hh:mm:ss;";
	//TempStr.Format("%s\n",*pData);
	TRACE(*pData + "\n");
	ParseString(&TempStr,&ParamList,';');
	if (ParamList.GetSize() == 3)
	{
		CStringArray ElmList;
		TempStr = ParamList.GetAt(0); 
		ParseString(&TempStr,&ElmList,'=');
		TempStr.Empty();
		if (ElmList.GetSize() == 2) TempStr = ElmList.GetAt(1);
		TempStr.MakeUpper();
		if (TempStr.Compare("FREE") == 0)
		{
			bTargetReady = true;
			bSetTarget   = false;
		}
		else
		{
			TempStr = ParamList.GetAt(1); 
			ParseString(&TempStr,&ElmList,'=');
			TempStr.Empty();
			if (ElmList.GetSize() == 2) TempStr = ElmList.GetAt(1);
			if (TempStr.Compare(this->LocalIPAddr) == 0)
			{
				bTargetReady = false;
				bSetTarget   = true;
			}
		}
	}
	this->bReceivedCmd = true;
	switch (comstate)
	{
	case 1:
		SetEditSession();
		break;
	case 2:
		TransFTP();
		if (isUpdate == false) SaveEditSession();
		else
		{
			bSetTarget = false;	// bypass check of SaveEdit state in CloseTransConect
			bTargetReady = true;
			CloseTransConect();
			
		}
		break;
	case 3:
		CloseTransConect();
		break;
	}
}

void CIPBackupDlg::OnRestore() 
{
	m_RemoteFileName = "AllData";
	InitTransConect();
	return;
}


void CIPBackupDlg::InitTransConect()
{
	UpdateData();
	DWORD dwAddress;
	this->m_RemoteAddr.GetAddress(dwAddress);
	if (0 == dwAddress)
	{
		AfxMessageBox("Please select printer or enter valid Remote Address");
		return;
	}

	in_addr iaddr;
	iaddr.S_un.S_addr = ntohl(dwAddress);

	PrinterAddress = inet_ntoa(iaddr);
	LocalFile = m_RemoteFileName;
	
	CFileDialog fd(TRUE, NULL, LocalFile);
	if (fd.DoModal()!=IDOK) return;
	EnableElements(FALSE);

	LocalFile = fd.GetFileName();
	LocalFilePath= fd.GetPathName();

	if ( pInfoDlg == NULL)
	{
		pInfoDlg = new CInfoTransfer(this);
		pInfoDlg->Create();
	}
	else
		pInfoDlg->SetActiveWindow();

	pInfoDlg->m_sTransFrom = LocalFile;
	pInfoDlg->m_sTransTo = PrinterAddress;
	pInfoDlg->m_sTData = "Get Edit Session Info";
	pInfoDlg->UpdateData(FALSE);

	bReceivedCmd = false;
	bTargetReady = false;
	m_pCmdSocket = new CCmdSocket(this);
	m_pCmdSocket->LegalCmds.Add(IPC_EDIT_STATUS);
	m_pCmdSocket->Create();
	m_pCmdSocket->Connect(PrinterAddress,FJ_CMD_PORT);
	UINT port;
	m_pCmdSocket->GetSockName(LocalIPAddr,port);

	CFJSocketHeader CmdHeader;
	ULONG lBachID = 1;
	CmdHeader.SetSocketCMD(IPC_GET_EDIT_STATUS);
	CmdHeader.SetBachID(lBachID);
	CmdHeader.SetDataLength(0);
	m_pCmdSocket->Send(CmdHeader.GetBuffer(),CmdHeader.GetBufferSize());

	comstate = 1;
	return;
}

void CIPBackupDlg::SetEditSession()
{
	if (bTargetReady == false && bSetTarget == false)
	{
		CString Error;
		Error.Format("The %s system currently in the Edit Session!\nPlease try later.",PrinterAddress);
		AfxMessageBox(Error);
		bTargetReady = true;
		CloseTransConect();
		return;
	}
	if (bSetTarget == false)
	{
		pInfoDlg->m_sTData = "Set Edit Session";
		pInfoDlg->UpdateData(FALSE);

		CFJSocketHeader CmdHeader;
		ULONG lBachID = 2;
		CmdHeader.SetSocketCMD(IPC_EDIT_START);
		CmdHeader.SetBachID(lBachID);
		CmdHeader.SetDataLength(0);
		m_pCmdSocket->Send(CmdHeader.GetBuffer(),CmdHeader.GetBufferSize());
		
		comstate = 2;
		return;
	}
	else TransFTP();

}

void CIPBackupDlg::TransFTP()
{
	if (bSetTarget == false)
	{
		CString Error;
		Error.Format("Could not set the Edit Session on %s.",PrinterAddress);
		AfxMessageBox(Error);
		TransFTP();
		return;
	}

	CString TmpStr;
	CInternetSession session;
	SetWebPWD(&session);
	CInternetSession FTPsession;
	CFtpConnection* pFTPconnect = FTPsession.GetFtpConnection(PrinterAddress,_T("anonymous"),_T(""));
	double dTrans = 0;
	TmpStr.Format("Sent: %.1f Kb",dTrans);
	pInfoDlg->m_sTData = TmpStr;
	pInfoDlg->UpdateData(FALSE);

	if ( NULL != pFTPconnect)
	{
		CInternetFile *remoteFile = NULL;
		CFile		  *outFile    = NULL;
		outFile = new CFile(LocalFilePath, CFile::modeRead|CFile::shareDenyWrite);
		remoteFile = pFTPconnect->OpenFile(m_RemoteFileName,GENERIC_WRITE);

		int buf_len = 2048;
		char *pBuffer = new char[buf_len];

		if ( NULL != pBuffer)
		{
			UINT readbytes = outFile->Read(pBuffer,buf_len);
			while( readbytes > 0 )
			{
				dTrans += readbytes;
				TmpStr.Format("Sent: %.1f Kb",dTrans/1024);
				pInfoDlg->m_sTData = TmpStr;
				pInfoDlg->UpdateData(FALSE);
				remoteFile->Write(pBuffer,readbytes);
				readbytes = outFile->Read(pBuffer,buf_len);
			}
			pInfoDlg->m_sTData = "Close FTP Session";
			pInfoDlg->UpdateData(FALSE);

			delete [] pBuffer;
		}

		remoteFile->Flush();
		remoteFile->Close();
		outFile->Close();
		pFTPconnect->Close();
		delete remoteFile;
		delete outFile;
		delete pFTPconnect;
	}
	else
	{
		CString Error;
		Error.Format("Could not create FTP Session with %s.",PrinterAddress);
		AfxMessageBox(Error);
	}
	session.Close();
	return;
}

void CIPBackupDlg::SaveEditSession()
{
	nSaveEdit = 3;
	pInfoDlg->m_sTData = "Save Edit Session";
	pInfoDlg->UpdateData(FALSE);

	CFJSocketHeader CmdHeader;
	ULONG lBachID = 3;
	CmdHeader.SetSocketCMD(IPC_EDIT_SAVE);
	CmdHeader.SetBachID(lBachID);
	CmdHeader.SetDataLength(0);
	m_pCmdSocket->Send(CmdHeader.GetBuffer(),CmdHeader.GetBufferSize());
	comstate = 3;
	return;
}

void CIPBackupDlg::CloseTransConect()
{
	if (bSetTarget != false && bTargetReady != true)
	{
		nSaveEdit--;
		if (nSaveEdit == 0)
		{
			CString Error;
			Error.Format("Could not save the Edit Session on %s.",PrinterAddress);
			AfxMessageBox(Error);
		}
		else
		{
			CFJSocketHeader CmdHeader;
			CmdHeader.SetSocketCMD(IPC_EDIT_SAVE);
			CmdHeader.SetBachID(nSaveEdit);
			CmdHeader.SetDataLength(0);
			m_pCmdSocket->Send(CmdHeader.GetBuffer(),CmdHeader.GetBufferSize());
			return;
		}
	}

	if (pInfoDlg != NULL)
	{
		delete pInfoDlg;
		pInfoDlg = NULL;
	}
	if (m_pCmdSocket != NULL)
	{
		CFJSocketHeader CmdHeader;
		ULONG lBachID = 4;
		CmdHeader.SetSocketCMD(IPC_CONNECTION_CLOSE);
		CmdHeader.SetBachID(lBachID);
		CmdHeader.SetDataLength(0);
		m_pCmdSocket->Send(CmdHeader.GetBuffer(),CmdHeader.GetBufferSize());
		m_pCmdSocket->Close();
		delete m_pCmdSocket;
		m_pCmdSocket = NULL;
	}

	PrinterAddress.Empty();
	LocalFilePath.Empty();
	LocalFile.Empty();
	EnableElements();
	comstate = 0;
	isUpdate = false;
	return;
}

void CIPBackupDlg::OnUpdate() 
{
	isUpdate = true;
	m_RemoteFileName = "ram.bin";
	InitTransConect();
	return;	
}
