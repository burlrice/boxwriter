#if !defined(AFX_INFOTRANSFER_H__E0E9DB1E_0B35_4331_B519_D5C88BD83EC4__INCLUDED_)
#define AFX_INFOTRANSFER_H__E0E9DB1E_0B35_4331_B519_D5C88BD83EC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoTransfer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInfoTransfer dialog

class CInfoTransfer : public CDialog
{
// Construction
public:
	CInfoTransfer(CWnd* pParent = NULL);   // standard constructor
	BOOL Create();

// Dialog Data
	//{{AFX_DATA(CInfoTransfer)
	enum { IDD = IDD_INFOTRANS };
	CString	m_sTransFrom;
	CString	m_sTransTo;
	CString	m_sTData;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoTransfer)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	CWnd* m_pParent;
	int m_nID;

	// Generated message map functions
	//{{AFX_MSG(CInfoTransfer)
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFOTRANSFER_H__E0E9DB1E_0B35_4331_B519_D5C88BD83EC4__INCLUDED_)
