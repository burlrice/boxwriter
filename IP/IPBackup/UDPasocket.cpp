// UDPasocket.cpp : implementation file
//

#include "stdafx.h"
#include "UDPasocket.h"
#include "IPBackup.h"
#include "IPBackupDlg.h"
#include "FJSocketHeader.h"
#include "FJ_SOCKET.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUDPasocket

CUDPasocket::CUDPasocket(CIPBackupDlg *pDlg)
{
	m_pDlg = pDlg;
	pReceiveBuffer	= new UCHAR[UDP_BUF_SIZE];

	memset(pReceiveBuffer,0,UDP_BUF_SIZE);
}

CUDPasocket::~CUDPasocket()
{
	if (NULL != pReceiveBuffer) delete [] pReceiveBuffer;
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CUDPasocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CUDPasocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CUDPasocket member functions

void CUDPasocket::OnReceive(int nErrorCode) 
{
	int iRead;
	CString socketAddr;
	UINT	socketPort;
	CFJSocketHeader fjHeader;
	UCHAR* pTempBuf = NULL;

	iRead = ReceiveFrom(pReceiveBuffer,UDP_BUF_SIZE,socketAddr,socketPort);
	switch (iRead)
	{
	case 0:
		Close();
		break;
	case SOCKET_ERROR:
		if (GetLastError() != WSAEWOULDBLOCK) 
		{
			if (GetLastError() != WSAEMSGSIZE)
			{
				CString TmpStr;
				TmpStr.Format("UDP OnReceive error: %d", GetLastError());
				AfxMessageBox (TmpStr);
			}
			else
			{
				AfxMessageBox ("The datagram was too large and was truncated");
				ProcessData(UDP_BUF_SIZE);
				//CString szTemp(pReceiveBuffer);
			}
		}
		break;

	default:
		if (iRead != SOCKET_ERROR && iRead != 0)
		{
			fjHeader.SetBuffer(pReceiveBuffer);
			if (fjHeader.GetSocketCMD() == IPC_SET_GROUP_INFO)
				ProcessData(iRead);
		}

	}
	CAsyncSocket::OnReceive(nErrorCode);
}

void CUDPasocket::ProcessData(int iRead)
{//GROUP_NAME="abc";GROUP_COUNT=n;GROUP_IDS="aaa","bbb","ccc","ddd";GROUP_IPADDRS=n.n.n.n,n.n.n.n,n.n.n.n;
	CFJSocketHeader fjHeader;
	CString TempStr(pReceiveBuffer + fjHeader.GetBufferSize());
	CStringArray ParamList;
	m_pDlg->ParseString(&TempStr,&ParamList,';');
	if (ParamList.GetSize() == 4)
	{
		CString PrintName;
		CString PrintIPaddr;
		CStringArray ElmList;
		TempStr = ParamList.GetAt(2);
		m_pDlg->ParseString(&TempStr,&ElmList,'=');
		if (ElmList.GetSize() == 2) PrintName = ElmList.GetAt(1);
		TempStr = ParamList.GetAt(3);
		m_pDlg->ParseString(&TempStr,&ElmList,'=');
		if (ElmList.GetSize() == 2) PrintIPaddr = ElmList.GetAt(1);

		PrintName.Remove('"');
		m_pDlg->UpdatePList(PrintName,PrintIPaddr);
	}
}

void CUDPasocket::OnSend(int nErrorCode) 
{
	SendInfo();
	CAsyncSocket::OnSend(nErrorCode);
}

void CUDPasocket::OnClose(int nErrorCode) 
{
	((CIPBackupDlg*)m_pDlg)->m_pUDPSocket = NULL;
	
	CAsyncSocket::OnClose(nErrorCode);
}

void CUDPasocket::SendInfo()
{
	CFJSocketHeader fjHeader;
	fjHeader.SetBachID(1);
	fjHeader.SetDataLength(0);
	fjHeader.SetSocketCMD(IPC_GET_GROUP_INFO);
	int dwBytes;

	SOCKADDR_IN remoteAddr;
	remoteAddr.sin_family = AF_INET;
	remoteAddr.sin_port = htons(FJ_UDP_PORT);
	remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	dwBytes = SendTo(fjHeader.GetBuffer(),fjHeader.GetBufferSize(), (const SOCKADDR* )&remoteAddr, sizeof(remoteAddr));
	if (dwBytes == SOCKET_ERROR)
	{
		if (GetLastError() != WSAEWOULDBLOCK)
		{
			TCHAR szError[256];
			wsprintf(szError, "Server Socket failed to send: %d", GetLastError());
			Close();
			AfxMessageBox (szError);
		}

	}

}
