#if !defined(AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_)
#define AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDPasocket.h : header file
//
class CIPBackupDlg;
#define UDP_BUF_SIZE 1024
static  UINT FJ_UDP_PORT = 1281;

/////////////////////////////////////////////////////////////////////////////
// CUDPasocket command target

class CUDPasocket : public CAsyncSocket
{
// Attributes
public:
	CIPBackupDlg *m_pDlg;

	UCHAR		 *pReceiveBuffer;

// Operations
public:
	CUDPasocket(CIPBackupDlg *pDlg);
	virtual ~CUDPasocket();

// Overrides
public:
	void SendInfo();
	void ProcessData(int iRead);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUDPasocket)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CUDPasocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_)
