// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "nice3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace NiceLabel;

/////////////////////////////////////////////////////////////////////////////
// TOcControlEvent properties

/////////////////////////////////////////////////////////////////////////////
// TOcControlEvent operations

long TOcControlEvent::Click()
{
	long result;
	InvokeHelper(DISPID_CLICK, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long TOcControlEvent::DblClick()
{
	long result;
	InvokeHelper(DISPID_DBLCLICK, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long TOcControlEvent::MouseDown(short Button, short Shift, long X, long Y)
{
	long result;
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I4 VTS_I4;
	InvokeHelper(DISPID_MOUSEDOWN, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Button, Shift, X, Y);
	return result;
}

long TOcControlEvent::MouseUp(short Button, short Shift, long X, long Y)
{
	long result;
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I4 VTS_I4;
	InvokeHelper(DISPID_MOUSEUP, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Button, Shift, X, Y);
	return result;
}

long TOcControlEvent::MouseMove(short Button, short Shift, long X, long Y)
{
	long result;
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I4 VTS_I4;
	InvokeHelper(DISPID_MOUSEMOVE, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Button, Shift, X, Y);
	return result;
}

long TOcControlEvent::KeyDown(short* KeyCode, short Shift)
{
	long result;
	static BYTE parms[] =
		VTS_PI2 VTS_I2;
	InvokeHelper(DISPID_KEYDOWN, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		KeyCode, Shift);
	return result;
}

long TOcControlEvent::KeyUp(short* KeyCode, short Shift)
{
	long result;
	static BYTE parms[] =
		VTS_PI2 VTS_I2;
	InvokeHelper(DISPID_KEYUP, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		KeyCode, Shift);
	return result;
}

long TOcControlEvent::ErrorEvent(short Number, LPCTSTR Description, long SCode, LPCTSTR Source, LPCTSTR HelpFile, long HelpContext, BOOL* CancelDisplay)
{
	long result;
	static BYTE parms[] =
		VTS_I2 VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_PBOOL;
	InvokeHelper(DISPID_ERROREVENT, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Number, Description, SCode, Source, HelpFile, HelpContext, CancelDisplay);
	return result;
}

long TOcControlEvent::CustomEvent(long* Number)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0xfffffc19, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Number);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// TOcControl properties

BOOL TOcControl::GetVisible()
{
	BOOL result;
	GetProperty(0x80010007, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetVisible(BOOL propVal)
{
	SetProperty(0x80010007, VT_BOOL, propVal);
}

BOOL TOcControl::GetCancel()
{
	BOOL result;
	GetProperty(0x80010037, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetCancel(BOOL propVal)
{
	SetProperty(0x80010037, VT_BOOL, propVal);
}

BOOL TOcControl::GetDefault()
{
	BOOL result;
	GetProperty(0x80010038, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetDefault(BOOL propVal)
{
	SetProperty(0x80010038, VT_BOOL, propVal);
}

CString TOcControl::GetName()
{
	CString result;
	GetProperty(0x80010000, VT_BSTR, (void*)&result);
	return result;
}

void TOcControl::SetName(LPCTSTR propVal)
{
	SetProperty(0x80010000, VT_BSTR, propVal);
}

long TOcControl::GetLeft()
{
	long result;
	GetProperty(0x80010100, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetLeft(long propVal)
{
	SetProperty(0x80010100, VT_I4, propVal);
}

long TOcControl::GetTop()
{
	long result;
	GetProperty(0x80010101, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetTop(long propVal)
{
	SetProperty(0x80010101, VT_I4, propVal);
}

long TOcControl::GetWidth()
{
	long result;
	GetProperty(0x80010102, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetWidth(long propVal)
{
	SetProperty(0x80010102, VT_I4, propVal);
}

long TOcControl::GetHeight()
{
	long result;
	GetProperty(0x80010103, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetHeight(long propVal)
{
	SetProperty(0x80010103, VT_I4, propVal);
}

long TOcControl::GetBackColor()
{
	long result;
	GetProperty(0xfffffd43, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetBackColor(long propVal)
{
	SetProperty(0xfffffd43, VT_I4, propVal);
}

long TOcControl::GetForeColor()
{
	long result;
	GetProperty(0xfffffd40, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetForeColor(long propVal)
{
	SetProperty(0xfffffd40, VT_I4, propVal);
}

long TOcControl::GetLocaleID()
{
	long result;
	GetProperty(0xfffffd3f, VT_I4, (void*)&result);
	return result;
}

void TOcControl::SetLocaleID(long propVal)
{
	SetProperty(0xfffffd3f, VT_I4, propVal);
}

short TOcControl::GetTextAlign()
{
	short result;
	GetProperty(0xfffffd3c, VT_I2, (void*)&result);
	return result;
}

void TOcControl::SetTextAlign(short propVal)
{
	SetProperty(0xfffffd3c, VT_I2, propVal);
}

BOOL TOcControl::GetMessageReflect()
{
	BOOL result;
	GetProperty(0xfffffd3e, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetMessageReflect(BOOL propVal)
{
	SetProperty(0xfffffd3e, VT_BOOL, propVal);
}

BOOL TOcControl::GetUserMode()
{
	BOOL result;
	GetProperty(0xfffffd3b, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetUserMode(BOOL propVal)
{
	SetProperty(0xfffffd3b, VT_BOOL, propVal);
}

BOOL TOcControl::GetUIDead()
{
	BOOL result;
	GetProperty(0xfffffd3a, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetUIDead(BOOL propVal)
{
	SetProperty(0xfffffd3a, VT_BOOL, propVal);
}

BOOL TOcControl::GetShowGrabHandles()
{
	BOOL result;
	GetProperty(0xfffffd39, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetShowGrabHandles(BOOL propVal)
{
	SetProperty(0xfffffd39, VT_BOOL, propVal);
}

BOOL TOcControl::GetShowHatching()
{
	BOOL result;
	GetProperty(0xfffffd38, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetShowHatching(BOOL propVal)
{
	SetProperty(0xfffffd38, VT_BOOL, propVal);
}

BOOL TOcControl::GetDisplayAsDefault()
{
	BOOL result;
	GetProperty(0xfffffd37, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetDisplayAsDefault(BOOL propVal)
{
	SetProperty(0xfffffd37, VT_BOOL, propVal);
}

BOOL TOcControl::GetSupportsMnemonics()
{
	BOOL result;
	GetProperty(0xfffffd36, VT_BOOL, (void*)&result);
	return result;
}

void TOcControl::SetSupportsMnemonics(BOOL propVal)
{
	SetProperty(0xfffffd36, VT_BOOL, propVal);
}

CString TOcControl::GetDisplayName()
{
	CString result;
	GetProperty(0xfffffd42, VT_BSTR, (void*)&result);
	return result;
}

void TOcControl::SetDisplayName(LPCTSTR propVal)
{
	SetProperty(0xfffffd42, VT_BSTR, propVal);
}

CString TOcControl::GetScaleUnits()
{
	CString result;
	GetProperty(0xfffffd3d, VT_BSTR, (void*)&result);
	return result;
}

void TOcControl::SetScaleUnits(LPCTSTR propVal)
{
	SetProperty(0xfffffd3d, VT_BSTR, propVal);
}

LPDISPATCH TOcControl::GetFont()
{
	LPDISPATCH result;
	GetProperty(0xfffffd41, VT_DISPATCH, (void*)&result);
	return result;
}

void TOcControl::SetFont(LPDISPATCH propVal)
{
	SetProperty(0xfffffd41, VT_DISPATCH, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// TOcControl operations

LPDISPATCH TOcControl::GetParent()
{
	LPDISPATCH result;
	InvokeHelper(0x80010008, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// TOcxView properties

long TOcxView::GetBackColor()
{
	long result;
	GetProperty(0xfffffd43, VT_I4, (void*)&result);
	return result;
}

void TOcxView::SetBackColor(long propVal)
{
	SetProperty(0xfffffd43, VT_I4, propVal);
}

long TOcxView::GetForeColor()
{
	long result;
	GetProperty(0xfffffd40, VT_I4, (void*)&result);
	return result;
}

void TOcxView::SetForeColor(long propVal)
{
	SetProperty(0xfffffd40, VT_I4, propVal);
}

long TOcxView::GetLocaleID()
{
	long result;
	GetProperty(0xfffffd3f, VT_I4, (void*)&result);
	return result;
}

void TOcxView::SetLocaleID(long propVal)
{
	SetProperty(0xfffffd3f, VT_I4, propVal);
}

short TOcxView::GetTextAlign()
{
	short result;
	GetProperty(0xfffffd3c, VT_I2, (void*)&result);
	return result;
}

void TOcxView::SetTextAlign(short propVal)
{
	SetProperty(0xfffffd3c, VT_I2, propVal);
}

BOOL TOcxView::GetMessageReflect()
{
	BOOL result;
	GetProperty(0xfffffd3e, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetMessageReflect(BOOL propVal)
{
	SetProperty(0xfffffd3e, VT_BOOL, propVal);
}

BOOL TOcxView::GetUserMode()
{
	BOOL result;
	GetProperty(0xfffffd3b, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetUserMode(BOOL propVal)
{
	SetProperty(0xfffffd3b, VT_BOOL, propVal);
}

BOOL TOcxView::GetUIDead()
{
	BOOL result;
	GetProperty(0xfffffd3a, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetUIDead(BOOL propVal)
{
	SetProperty(0xfffffd3a, VT_BOOL, propVal);
}

BOOL TOcxView::GetShowGrabHandles()
{
	BOOL result;
	GetProperty(0xfffffd39, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetShowGrabHandles(BOOL propVal)
{
	SetProperty(0xfffffd39, VT_BOOL, propVal);
}

BOOL TOcxView::GetShowHatching()
{
	BOOL result;
	GetProperty(0xfffffd38, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetShowHatching(BOOL propVal)
{
	SetProperty(0xfffffd38, VT_BOOL, propVal);
}

BOOL TOcxView::GetDisplayAsDefault()
{
	BOOL result;
	GetProperty(0xfffffd37, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetDisplayAsDefault(BOOL propVal)
{
	SetProperty(0xfffffd37, VT_BOOL, propVal);
}

BOOL TOcxView::GetSupportsMnemonics()
{
	BOOL result;
	GetProperty(0xfffffd36, VT_BOOL, (void*)&result);
	return result;
}

void TOcxView::SetSupportsMnemonics(BOOL propVal)
{
	SetProperty(0xfffffd36, VT_BOOL, propVal);
}

CString TOcxView::GetDisplayName()
{
	CString result;
	GetProperty(0xfffffd42, VT_BSTR, (void*)&result);
	return result;
}

void TOcxView::SetDisplayName(LPCTSTR propVal)
{
	SetProperty(0xfffffd42, VT_BSTR, propVal);
}

CString TOcxView::GetScaleUnits()
{
	CString result;
	GetProperty(0xfffffd3d, VT_BSTR, (void*)&result);
	return result;
}

void TOcxView::SetScaleUnits(LPCTSTR propVal)
{
	SetProperty(0xfffffd3d, VT_BSTR, propVal);
}

LPDISPATCH TOcxView::GetFont()
{
	LPDISPATCH result;
	GetProperty(0xfffffd41, VT_DISPATCH, (void*)&result);
	return result;
}

void TOcxView::SetFont(LPDISPATCH propVal)
{
	SetProperty(0xfffffd41, VT_DISPATCH, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// TOcxView operations


/////////////////////////////////////////////////////////////////////////////
// INiceLabel properties

CString INiceLabel::GetLabelFileName()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void INiceLabel::SetLabelFileName(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

CString INiceLabel::GetPrinterName()
{
	CString result;
	GetProperty(0xf, VT_BSTR, (void*)&result);
	return result;
}

void INiceLabel::SetPrinterName(LPCTSTR propVal)
{
	SetProperty(0xf, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// INiceLabel operations

LPDISPATCH INiceLabel::GetApplication()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::Save()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::Print(LPCTSTR Quantity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Quantity);
	return result;
}

BOOL INiceLabel::ExecuteMacro(LPCTSTR Macro)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Macro);
	return result;
}

BOOL INiceLabel::ExportToPocket(LPCTSTR ExportDir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ExportDir);
	return result;
}

BOOL INiceLabel::SessionStart()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::SessionPrint(LPCTSTR Quantity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Quantity);
	return result;
}

BOOL INiceLabel::SessionEnd()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::GetLabelPreview(LPCTSTR FileName, long Width, long Height)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName, Width, Height);
	return result;
}

LPDISPATCH INiceLabel::GetObjects()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INiceLabel::GetVariables()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INiceLabel::GetFunctions()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INiceLabel::GetDatabases()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long INiceLabel::GetCurrUnit()
{
	long result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH INiceLabel::GetGlobalVariables()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::NewVariable()
{
	BOOL result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::NewFunction()
{
	BOOL result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::NewDatabase()
{
	BOOL result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::ObjectProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::VariableProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::FunctionProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::DatabaseProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::DeleteObject(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::DeleteVariable(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::DeleteFunction(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::DeleteDatabase(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::SelectVariable(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::SelectDatabase(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL INiceLabel::NewVariableWiz()
{
	BOOL result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INiceLabel::NewDatabaseWiz()
{
	BOOL result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// CNiceApp properties

/////////////////////////////////////////////////////////////////////////////
// CNiceApp operations

LPDISPATCH CNiceApp::GetApplication()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void CNiceApp::Quit()
{
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long CNiceApp::LabelOpen(LPCTSTR FileName)
{
	long result = -1;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		FileName);
	return result;
}

BOOL CNiceApp::LabelClose(long LabelID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID);
	return result;
}

BOOL CNiceApp::LabelPrint(long LabelID, LPCTSTR Quantity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, Quantity);
	return result;
}

BOOL CNiceApp::LabelSetVar(long LabelID, LPCTSTR Name, LPCTSTR Value, long Step, long Count)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, Name, Value, Step, Count);
	return result;
}

long CNiceApp::LabelGetVarCount(long LabelID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		LabelID);
	return result;
}

CString CNiceApp::LabelGetVarName(long LabelID, long Var)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LabelID, Var);
	return result;
}

CString CNiceApp::LabelGetVarProperty(long LabelID, LPCTSTR VarName, LPCTSTR PropName)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LabelID, VarName, PropName);
	return result;
}

CString CNiceApp::GetErrorMessage()
{
	CString result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString CNiceApp::GetDetailedMessage()
{
	CString result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CNiceApp::JobRun(LPCTSTR FileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FileName);
	return result;
}

BOOL CNiceApp::Login(LPCTSTR UserName, long Level)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		UserName, Level);
	return result;
}

BOOL CNiceApp::ExecuteMacro(long LabelID, LPCTSTR Macro)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, Macro);
	return result;
}

BOOL CNiceApp::CloseForm(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL CNiceApp::LabelSetPrinter(long LabelID, LPCTSTR PrinterName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, PrinterName);
	return result;
}

BOOL CNiceApp::LabelSetPrintJobName(long LabelID, LPCTSTR PrinterName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, PrinterName);
	return result;
}

BOOL CNiceApp::LabelTestConnection(long LabelID, LPCTSTR FileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, FileName);
	return result;
}

long CNiceApp::GetErrorID()
{
	long result;
	InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CNiceApp::LabelExportToPocket(long LabelID, LPCTSTR ExportDir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, ExportDir);
	return result;
}

BOOL CNiceApp::LabelSessionStart(long LabelID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID);
	return result;
}

BOOL CNiceApp::LabelSessionPrint(long LabelID, LPCTSTR Quantity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID, Quantity);
	return result;
}

BOOL CNiceApp::LabelSessionEnd(long LabelID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		LabelID);
	return result;
}

LPDISPATCH CNiceApp::LabelOpenEx(LPCTSTR FileName)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		FileName);
	return result;
}

LPDISPATCH CNiceApp::LabelCreate()
{
	LPDISPATCH result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CNiceApp::TestConnection()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CNiceApp::InitConnection()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CNiceApp::LabelImport(long FilterID, long FormatID, LPCTSTR ImportFile, LPCTSTR ExportFile, BOOL EmbedGraphics)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BOOL;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FilterID, FormatID, ImportFile, ExportFile, EmbedGraphics);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ILabel properties

CString ILabel::GetPrinterName()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void ILabel::SetPrinterName(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// ILabel operations

LPDISPATCH ILabel::GetObjects()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILabel::GetVariables()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILabel::GetFunctions()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILabel::GetDatabases()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long ILabel::GetCurrUnit()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILabel::GetGlobalVariables()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL ILabel::NewVariable()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ILabel::NewFunction()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ILabel::NewDatabase()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ILabel::ObjectProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::VariableProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::FunctionProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::DatabaseProperties(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::DeleteObject(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::DeleteVariable(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::DeleteFunction(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::DeleteDatabase(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::SelectVariable(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::SelectDatabase(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL ILabel::NewVariableWiz()
{
	BOOL result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ILabel::NewDatabaseWiz()
{
	BOOL result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IObjectList properties

/////////////////////////////////////////////////////////////////////////////
// IObjectList operations

long IObjectList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IObjectList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IBarcode properties

CString IBarcode::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IBarcode::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IBarcode::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IBarcode::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IBarcode::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IBarcode::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IBarcode::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IBarcode::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IBarcode::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IBarcode::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IBarcode::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IBarcode::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

BOOL IBarcode::GetPrintAsGraphics()
{
	BOOL result;
	GetProperty(0x1a, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetPrintAsGraphics(BOOL propVal)
{
	SetProperty(0x1a, VT_BOOL, propVal);
}

BOOL IBarcode::GetHasQuietZone()
{
	BOOL result;
	GetProperty(0x1b, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetHasQuietZone(BOOL propVal)
{
	SetProperty(0x1b, VT_BOOL, propVal);
}

BOOL IBarcode::GetIncludeCD()
{
	BOOL result;
	GetProperty(0x1c, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetIncludeCD(BOOL propVal)
{
	SetProperty(0x1c, VT_BOOL, propVal);
}

BOOL IBarcode::GetAutoCDCalculation()
{
	BOOL result;
	GetProperty(0x1d, VT_BOOL, (void*)&result);
	return result;
}

void IBarcode::SetAutoCDCalculation(BOOL propVal)
{
	SetProperty(0x1d, VT_BOOL, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IBarcode operations

long IBarcode::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IBarcode::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBarcode::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IBarcode::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IBarcode::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IBarcode::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}

CString IBarcode::GetContents()
{
	CString result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString IBarcode::GetBarcodeType()
{
	CString result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IBarcode::SetContents(LPCTSTR Value)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Value);
	return result;
}

CString IBarcode::GetProperty_(LPCTSTR Name)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Name);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IGraphics properties

CString IGraphics::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IGraphics::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IGraphics::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IGraphics::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IGraphics::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IGraphics::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IGraphics::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IGraphics::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IGraphics::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IGraphics::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IGraphics::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IGraphics::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IGraphics::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

BOOL IGraphics::GetHorzMirror()
{
	BOOL result;
	GetProperty(0x19, VT_BOOL, (void*)&result);
	return result;
}

void IGraphics::SetHorzMirror(BOOL propVal)
{
	SetProperty(0x19, VT_BOOL, propVal);
}

BOOL IGraphics::GetVertMirror()
{
	BOOL result;
	GetProperty(0x1a, VT_BOOL, (void*)&result);
	return result;
}

void IGraphics::SetVertMirror(BOOL propVal)
{
	SetProperty(0x1a, VT_BOOL, propVal);
}

BOOL IGraphics::GetOnMemoryCard()
{
	BOOL result;
	GetProperty(0x1b, VT_BOOL, (void*)&result);
	return result;
}

void IGraphics::SetOnMemoryCard(BOOL propVal)
{
	SetProperty(0x1b, VT_BOOL, propVal);
}

long IGraphics::GetResizeMode()
{
	long result;
	GetProperty(0x1c, VT_I4, (void*)&result);
	return result;
}

void IGraphics::SetResizeMode(long propVal)
{
	SetProperty(0x1c, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IGraphics operations

long IGraphics::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IGraphics::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IGraphics::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IGraphics::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IGraphics::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IGraphics::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}

CString IGraphics::GetFileName()
{
	CString result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IGraphics::Load(LPCTSTR FileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		FileName);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IRTFText properties

CString IRTFText::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IRTFText::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IRTFText::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IRTFText::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IRTFText::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IRTFText::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IRTFText::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IRTFText::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IRTFText::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IRTFText::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IRTFText::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IRTFText::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IRTFText::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IRTFText::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IRTFText operations

long IRTFText::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRTFText::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IRTFText::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IRTFText::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IRTFText::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IRTFText::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IParagraph properties

CString IParagraph::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IParagraph::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IParagraph::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IParagraph::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IParagraph::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IParagraph::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IParagraph::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IParagraph::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IParagraph::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IParagraph::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IParagraph::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

CString IParagraph::GetFontName()
{
	CString result;
	GetProperty(0x19, VT_BSTR, (void*)&result);
	return result;
}

void IParagraph::SetFontName(LPCTSTR propVal)
{
	SetProperty(0x19, VT_BSTR, propVal);
}

BOOL IParagraph::GetIsItalic()
{
	BOOL result;
	GetProperty(0x1a, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetIsItalic(BOOL propVal)
{
	SetProperty(0x1a, VT_BOOL, propVal);
}

BOOL IParagraph::GetIsBold()
{
	BOOL result;
	GetProperty(0x1b, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetIsBold(BOOL propVal)
{
	SetProperty(0x1b, VT_BOOL, propVal);
}

BOOL IParagraph::GetIsInverse()
{
	BOOL result;
	GetProperty(0x1c, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetIsInverse(BOOL propVal)
{
	SetProperty(0x1c, VT_BOOL, propVal);
}

BOOL IParagraph::GetIsMirror()
{
	BOOL result;
	GetProperty(0x1d, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetIsMirror(BOOL propVal)
{
	SetProperty(0x1d, VT_BOOL, propVal);
}

long IParagraph::GetPtSizeX()
{
	long result;
	GetProperty(0x1e, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetPtSizeX(long propVal)
{
	SetProperty(0x1e, VT_I4, propVal);
}

long IParagraph::GetPtSizeY()
{
	long result;
	GetProperty(0x1f, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetPtSizeY(long propVal)
{
	SetProperty(0x1f, VT_I4, propVal);
}

long IParagraph::GetSpacingX()
{
	long result;
	GetProperty(0x20, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetSpacingX(long propVal)
{
	SetProperty(0x20, VT_I4, propVal);
}

long IParagraph::GetSpacingY()
{
	long result;
	GetProperty(0x21, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetSpacingY(long propVal)
{
	SetProperty(0x21, VT_I4, propVal);
}

BOOL IParagraph::GetBestFit()
{
	BOOL result;
	GetProperty(0x23, VT_BOOL, (void*)&result);
	return result;
}

void IParagraph::SetBestFit(BOOL propVal)
{
	SetProperty(0x23, VT_BOOL, propVal);
}

long IParagraph::GetMinSizeX()
{
	long result;
	GetProperty(0x24, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetMinSizeX(long propVal)
{
	SetProperty(0x24, VT_I4, propVal);
}

long IParagraph::GetMinSizeY()
{
	long result;
	GetProperty(0x25, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetMinSizeY(long propVal)
{
	SetProperty(0x25, VT_I4, propVal);
}

long IParagraph::GetMaxSizeX()
{
	long result;
	GetProperty(0x26, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetMaxSizeX(long propVal)
{
	SetProperty(0x26, VT_I4, propVal);
}

long IParagraph::GetMaxSizeY()
{
	long result;
	GetProperty(0x27, VT_I4, (void*)&result);
	return result;
}

void IParagraph::SetMaxSizeY(long propVal)
{
	SetProperty(0x27, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IParagraph operations

long IParagraph::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IParagraph::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IParagraph::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IParagraph::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IParagraph::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IParagraph::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}

CString IParagraph::GetContents()
{
	CString result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IParagraph::SetContents(LPCTSTR Value)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Value);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IText properties

CString IText::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IText::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IText::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IText::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IText::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IText::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IText::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IText::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IText::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IText::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IText::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IText::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IText::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IText::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IText::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IText::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IText::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IText::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

CString IText::GetFontName()
{
	CString result;
	GetProperty(0x19, VT_BSTR, (void*)&result);
	return result;
}

void IText::SetFontName(LPCTSTR propVal)
{
	SetProperty(0x19, VT_BSTR, propVal);
}

BOOL IText::GetIsItalic()
{
	BOOL result;
	GetProperty(0x1a, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetIsItalic(BOOL propVal)
{
	SetProperty(0x1a, VT_BOOL, propVal);
}

BOOL IText::GetIsBold()
{
	BOOL result;
	GetProperty(0x1b, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetIsBold(BOOL propVal)
{
	SetProperty(0x1b, VT_BOOL, propVal);
}

BOOL IText::GetIsInverse()
{
	BOOL result;
	GetProperty(0x1c, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetIsInverse(BOOL propVal)
{
	SetProperty(0x1c, VT_BOOL, propVal);
}

BOOL IText::GetIsMirror()
{
	BOOL result;
	GetProperty(0x1d, VT_BOOL, (void*)&result);
	return result;
}

void IText::SetIsMirror(BOOL propVal)
{
	SetProperty(0x1d, VT_BOOL, propVal);
}

long IText::GetPtSizeX()
{
	long result;
	GetProperty(0x1e, VT_I4, (void*)&result);
	return result;
}

void IText::SetPtSizeX(long propVal)
{
	SetProperty(0x1e, VT_I4, propVal);
}

long IText::GetPtSizeY()
{
	long result;
	GetProperty(0x1f, VT_I4, (void*)&result);
	return result;
}

void IText::SetPtSizeY(long propVal)
{
	SetProperty(0x1f, VT_I4, propVal);
}

long IText::GetSpacingX()
{
	long result;
	GetProperty(0x20, VT_I4, (void*)&result);
	return result;
}

void IText::SetSpacingX(long propVal)
{
	SetProperty(0x20, VT_I4, propVal);
}

long IText::GetSpacingY()
{
	long result;
	GetProperty(0x21, VT_I4, (void*)&result);
	return result;
}

void IText::SetSpacingY(long propVal)
{
	SetProperty(0x21, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IText operations

long IText::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IText::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IText::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IText::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IText::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IText::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}

CString IText::GetContents()
{
	CString result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IText::SetContents(LPCTSTR Value)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Value);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IRectangle properties

CString IRectangle::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IRectangle::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IRectangle::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IRectangle::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IRectangle::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IRectangle::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IRectangle::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IRectangle::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IRectangle::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IRectangle::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IRectangle::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IRectangle::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IRectangle::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

long IRectangle::GetThicknessX()
{
	long result;
	GetProperty(0x18, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetThicknessX(long propVal)
{
	SetProperty(0x18, VT_I4, propVal);
}

long IRectangle::GetThicknessY()
{
	long result;
	GetProperty(0x19, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetThicknessY(long propVal)
{
	SetProperty(0x19, VT_I4, propVal);
}

long IRectangle::GetRadius()
{
	long result;
	GetProperty(0x1a, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetRadius(long propVal)
{
	SetProperty(0x1a, VT_I4, propVal);
}

long IRectangle::GetLineStyle()
{
	long result;
	GetProperty(0x1b, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetLineStyle(long propVal)
{
	SetProperty(0x1b, VT_I4, propVal);
}

long IRectangle::GetFillStyle()
{
	long result;
	GetProperty(0x1c, VT_I4, (void*)&result);
	return result;
}

void IRectangle::SetFillStyle(long propVal)
{
	SetProperty(0x1c, VT_I4, propVal);
}

BOOL IRectangle::GetIsRounded()
{
	BOOL result;
	GetProperty(0x1d, VT_BOOL, (void*)&result);
	return result;
}

void IRectangle::SetIsRounded(BOOL propVal)
{
	SetProperty(0x1d, VT_BOOL, propVal);
}

BOOL IRectangle::GetPrintAsGraphics()
{
	BOOL result;
	GetProperty(0x1e, VT_BOOL, (void*)&result);
	return result;
}

void IRectangle::SetPrintAsGraphics(BOOL propVal)
{
	SetProperty(0x1e, VT_BOOL, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IRectangle operations

long IRectangle::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IRectangle::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IRectangle::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IRectangle::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IRectangle::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IRectangle::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IOleObject properties

CString ImOleObject::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void ImOleObject::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL ImOleObject::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void ImOleObject::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long ImOleObject::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL ImOleObject::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void ImOleObject::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long ImOleObject::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long ImOleObject::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long ImOleObject::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long ImOleObject::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long ImOleObject::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long ImOleObject::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void ImOleObject::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IOleObject operations

long ImOleObject::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ImOleObject::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ImOleObject::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ImOleObject::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void ImOleObject::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long ImOleObject::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IObject properties

CString IObject::GetName()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IObject::SetName(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

BOOL IObject::GetSelected()
{
	BOOL result;
	GetProperty(0x8, VT_BOOL, (void*)&result);
	return result;
}

void IObject::SetSelected(BOOL propVal)
{
	SetProperty(0x8, VT_BOOL, propVal);
}

long IObject::GetRotation()
{
	long result;
	GetProperty(0xc, VT_I4, (void*)&result);
	return result;
}

void IObject::SetRotation(long propVal)
{
	SetProperty(0xc, VT_I4, propVal);
}

BOOL IObject::GetIsLocked()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IObject::SetIsLocked(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IObject::GetAnchorPoint()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IObject::SetAnchorPoint(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IObject::GetAnchorElementID()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IObject::SetAnchorElementID(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

long IObject::GetAnchorLevel()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void IObject::SetAnchorLevel(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

long IObject::GetFormatID()
{
	long result;
	GetProperty(0x12, VT_I4, (void*)&result);
	return result;
}

void IObject::SetFormatID(long propVal)
{
	SetProperty(0x12, VT_I4, propVal);
}

long IObject::GetZOrder()
{
	long result;
	GetProperty(0x13, VT_I4, (void*)&result);
	return result;
}

void IObject::SetZOrder(long propVal)
{
	SetProperty(0x13, VT_I4, propVal);
}

long IObject::GetPageNumber()
{
	long result;
	GetProperty(0x14, VT_I4, (void*)&result);
	return result;
}

void IObject::SetPageNumber(long propVal)
{
	SetProperty(0x14, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IObject operations

long IObject::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetLeft()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetTop()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetHeight()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetKind()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetStatus()
{
	long result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetRotateFlag()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IObject::GetResizeFlag()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IObject::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IObject::Move(long X, long Y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 X, Y);
}

void IObject::Resize(long Width, long Height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, Height);
}

long IObject::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IVariableList properties

/////////////////////////////////////////////////////////////////////////////
// IVariableList operations

long IVariableList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IVariableList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}

LPDISPATCH IVariableList::FindByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IVariableList::FindByID(long ID)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ID);
	return result;
}

LPDISPATCH IVariableList::Create(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IVariableList::CreatePrompted(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

BOOL IVariableList::Add(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL IVariableList::Remove(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL IVariableList::Delete(long ID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ID);
	return result;
}

BOOL IVariableList::FlushList()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IVar properties

CString IVar::GetName()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetName(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString IVar::GetDescription()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetDescription(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

CString IVar::GetPrefix()
{
	CString result;
	GetProperty(0x4, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetPrefix(LPCTSTR propVal)
{
	SetProperty(0x4, VT_BSTR, propVal);
}

CString IVar::GetSuffix()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetSuffix(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

long IVar::GetJustificationType()
{
	long result;
	GetProperty(0x7, VT_I4, (void*)&result);
	return result;
}

void IVar::SetJustificationType(long propVal)
{
	SetProperty(0x7, VT_I4, propVal);
}

CString IVar::GetPadCharacter()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetPadCharacter(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

BOOL IVar::GetFixedLength()
{
	BOOL result;
	GetProperty(0x9, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetFixedLength(BOOL propVal)
{
	SetProperty(0x9, VT_BOOL, propVal);
}

long IVar::GetLength()
{
	long result;
	GetProperty(0xa, VT_I4, (void*)&result);
	return result;
}

void IVar::SetLength(long propVal)
{
	SetProperty(0xa, VT_I4, propVal);
}

BOOL IVar::GetIsMultiLine()
{
	BOOL result;
	GetProperty(0xb, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetIsMultiLine(BOOL propVal)
{
	SetProperty(0xb, VT_BOOL, propVal);
}

BOOL IVar::GetWordWrap()
{
	BOOL result;
	GetProperty(0xc, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetWordWrap(BOOL propVal)
{
	SetProperty(0xc, VT_BOOL, propVal);
}

BOOL IVar::GetBreakLines()
{
	BOOL result;
	GetProperty(0xd, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetBreakLines(BOOL propVal)
{
	SetProperty(0xd, VT_BOOL, propVal);
}

long IVar::GetLineLength()
{
	long result;
	GetProperty(0xe, VT_I4, (void*)&result);
	return result;
}

void IVar::SetLineLength(long propVal)
{
	SetProperty(0xe, VT_I4, propVal);
}

long IVar::GetLineCount()
{
	long result;
	GetProperty(0xf, VT_I4, (void*)&result);
	return result;
}

void IVar::SetLineCount(long propVal)
{
	SetProperty(0xf, VT_I4, propVal);
}

BOOL IVar::GetIsVarQuantity()
{
	BOOL result;
	GetProperty(0x10, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetIsVarQuantity(BOOL propVal)
{
	SetProperty(0x10, VT_BOOL, propVal);
}

BOOL IVar::GetStrictChecking()
{
	BOOL result;
	GetProperty(0x11, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetStrictChecking(BOOL propVal)
{
	SetProperty(0x11, VT_BOOL, propVal);
}

BOOL IVar::GetHasMinValue()
{
	BOOL result;
	GetProperty(0x12, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetHasMinValue(BOOL propVal)
{
	SetProperty(0x12, VT_BOOL, propVal);
}

CString IVar::GetMinValue()
{
	CString result;
	GetProperty(0x13, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetMinValue(LPCTSTR propVal)
{
	SetProperty(0x13, VT_BSTR, propVal);
}

BOOL IVar::GetHasMaxValue()
{
	BOOL result;
	GetProperty(0x14, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetHasMaxValue(BOOL propVal)
{
	SetProperty(0x14, VT_BOOL, propVal);
}

CString IVar::GetMaxValue()
{
	CString result;
	GetProperty(0x15, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetMaxValue(LPCTSTR propVal)
{
	SetProperty(0x15, VT_BSTR, propVal);
}

long IVar::GetFormatID()
{
	long result;
	GetProperty(0x16, VT_I4, (void*)&result);
	return result;
}

void IVar::SetFormatID(long propVal)
{
	SetProperty(0x16, VT_I4, propVal);
}

BOOL IVar::GetIsBasedOnQuantity()
{
	BOOL result;
	GetProperty(0x17, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetIsBasedOnQuantity(BOOL propVal)
{
	SetProperty(0x17, VT_BOOL, propVal);
}

long IVar::GetPromptCount()
{
	long result;
	GetProperty(0x18, VT_I4, (void*)&result);
	return result;
}

void IVar::SetPromptCount(long propVal)
{
	SetProperty(0x18, VT_I4, propVal);
}

CString IVar::GetPrompt()
{
	CString result;
	GetProperty(0x19, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetPrompt(LPCTSTR propVal)
{
	SetProperty(0x19, VT_BSTR, propVal);
}

CString IVar::GetDefaultValue()
{
	CString result;
	GetProperty(0x1a, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetDefaultValue(LPCTSTR propVal)
{
	SetProperty(0x1a, VT_BSTR, propVal);
}

long IVar::GetDefType()
{
	long result;
	GetProperty(0x1b, VT_I4, (void*)&result);
	return result;
}

void IVar::SetDefType(long propVal)
{
	SetProperty(0x1b, VT_I4, propVal);
}

BOOL IVar::GetDynamicValue()
{
	BOOL result;
	GetProperty(0x1c, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetDynamicValue(BOOL propVal)
{
	SetProperty(0x1c, VT_BOOL, propVal);
}

BOOL IVar::GetValueRequired()
{
	BOOL result;
	GetProperty(0x1d, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetValueRequired(BOOL propVal)
{
	SetProperty(0x1d, VT_BOOL, propVal);
}

long IVar::GetIncrementCount()
{
	long result;
	GetProperty(0x20, VT_I4, (void*)&result);
	return result;
}

void IVar::SetIncrementCount(long propVal)
{
	SetProperty(0x20, VT_I4, propVal);
}

long IVar::GetIncrementStep()
{
	long result;
	GetProperty(0x21, VT_I4, (void*)&result);
	return result;
}

void IVar::SetIncrementStep(long propVal)
{
	SetProperty(0x21, VT_I4, propVal);
}

long IVar::GetIncrementStep2()
{
	long result;
	GetProperty(0x22, VT_I4, (void*)&result);
	return result;
}

void IVar::SetIncrementStep2(long propVal)
{
	SetProperty(0x22, VT_I4, propVal);
}

long IVar::GetIncrementStep3()
{
	long result;
	GetProperty(0x23, VT_I4, (void*)&result);
	return result;
}

void IVar::SetIncrementStep3(long propVal)
{
	SetProperty(0x23, VT_I4, propVal);
}

BOOL IVar::GetRollOver()
{
	BOOL result;
	GetProperty(0x24, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetRollOver(BOOL propVal)
{
	SetProperty(0x24, VT_BOOL, propVal);
}

BOOL IVar::GetTraceOn()
{
	BOOL result;
	GetProperty(0x25, VT_BOOL, (void*)&result);
	return result;
}

void IVar::SetTraceOn(BOOL propVal)
{
	SetProperty(0x25, VT_BOOL, propVal);
}

long IVar::GetPictureType()
{
	long result;
	GetProperty(0x26, VT_I4, (void*)&result);
	return result;
}

void IVar::SetPictureType(long propVal)
{
	SetProperty(0x26, VT_I4, propVal);
}

CString IVar::GetInputPicture()
{
	CString result;
	GetProperty(0x27, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetInputPicture(LPCTSTR propVal)
{
	SetProperty(0x27, VT_BSTR, propVal);
}

CString IVar::GetOutputPicture()
{
	CString result;
	GetProperty(0x28, VT_BSTR, (void*)&result);
	return result;
}

void IVar::SetOutputPicture(LPCTSTR propVal)
{
	SetProperty(0x28, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IVar operations

long IVar::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IVar::GetVarType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IVar::GetIncrementType()
{
	long result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IVar::GetIncrementKind()
{
	long result;
	InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IVar::GetIsUsed()
{
	BOOL result;
	InvokeHelper(0x29, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString IVar::GetDefault()
{
	CString result;
	InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IVar::SetValue(LPCTSTR Value)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Value);
	return result;
}

CString IVar::GetValue()
{
	CString result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IVar::DependsOnFunction(long FunctionID)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FunctionID);
	return result;
}

BOOL IVar::ShouldTraceNow(BOOL bStart)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bStart);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IExtFunction properties

CString IExtFunction::GetName()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IExtFunction::SetName(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString IExtFunction::GetDescription()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void IExtFunction::SetDescription(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

BOOL IExtFunction::GetReviewBeforePrint()
{
	BOOL result;
	GetProperty(0x5, VT_BOOL, (void*)&result);
	return result;
}

void IExtFunction::SetReviewBeforePrint(BOOL propVal)
{
	SetProperty(0x5, VT_BOOL, propVal);
}

CString IExtFunction::GetDefinition()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void IExtFunction::SetDefinition(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IExtFunction operations

long IExtFunction::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IExtFunction::GetKind()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IExtFunction::GetInputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IExtFunction::GetOutputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IExtFunction::GetProviderName()
{
	CString result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IExtFunction::GetIsContentsProvider()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDatabase properties

CString IDatabase::GetName()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetName(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString IDatabase::GetDescription()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetDescription(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

BOOL IDatabase::GetReviewBeforePrint()
{
	BOOL result;
	GetProperty(0x5, VT_BOOL, (void*)&result);
	return result;
}

void IDatabase::SetReviewBeforePrint(BOOL propVal)
{
	SetProperty(0x5, VT_BOOL, propVal);
}

CString IDatabase::GetDriverType()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetDriverType(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

CString IDatabase::GetAlias()
{
	CString result;
	GetProperty(0x9, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetAlias(LPCTSTR propVal)
{
	SetProperty(0x9, VT_BSTR, propVal);
}

BOOL IDatabase::GetIsDelimited()
{
	BOOL result;
	GetProperty(0xa, VT_BOOL, (void*)&result);
	return result;
}

void IDatabase::SetIsDelimited(BOOL propVal)
{
	SetProperty(0xa, VT_BOOL, propVal);
}

CString IDatabase::GetDelimiter()
{
	CString result;
	GetProperty(0xb, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetDelimiter(LPCTSTR propVal)
{
	SetProperty(0xb, VT_BSTR, propVal);
}

CString IDatabase::GetSeparator()
{
	CString result;
	GetProperty(0xc, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetSeparator(LPCTSTR propVal)
{
	SetProperty(0xc, VT_BSTR, propVal);
}

CString IDatabase::GetDBPassword()
{
	CString result;
	GetProperty(0xd, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetDBPassword(LPCTSTR propVal)
{
	SetProperty(0xd, VT_BSTR, propVal);
}

CString IDatabase::GetTable()
{
	CString result;
	GetProperty(0xe, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetTable(LPCTSTR propVal)
{
	SetProperty(0xe, VT_BSTR, propVal);
}

CString IDatabase::GetOrder()
{
	CString result;
	GetProperty(0xf, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetOrder(LPCTSTR propVal)
{
	SetProperty(0xf, VT_BSTR, propVal);
}

long IDatabase::GetNavigation()
{
	long result;
	GetProperty(0x11, VT_I4, (void*)&result);
	return result;
}

void IDatabase::SetNavigation(long propVal)
{
	SetProperty(0x11, VT_I4, propVal);
}

CString IDatabase::GetSql()
{
	CString result;
	GetProperty(0x13, VT_BSTR, (void*)&result);
	return result;
}

void IDatabase::SetSql(LPCTSTR propVal)
{
	SetProperty(0x13, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IDatabase operations

long IDatabase::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IDatabase::GetKind()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatabase::GetInputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatabase::GetOutputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatabase::GetFields()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatabase::GetParameters()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IParameterList properties

/////////////////////////////////////////////////////////////////////////////
// IParameterList operations

long IParameterList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IParameterList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}

LPDISPATCH IParameterList::Create(LPCTSTR Field)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Field);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDBParameter properties

CString IDBParameter::GetField()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void IDBParameter::SetField(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

long IDBParameter::GetRelation()
{
	long result;
	GetProperty(0x2, VT_I4, (void*)&result);
	return result;
}

void IDBParameter::SetRelation(long propVal)
{
	SetProperty(0x2, VT_I4, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IDBParameter operations

LPDISPATCH IDBParameter::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDBParameter::SetVariable(long ID)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFunction properties

CString IFunction::GetName()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IFunction::SetName(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString IFunction::GetDescription()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void IFunction::SetDescription(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

BOOL IFunction::GetReviewBeforePrint()
{
	BOOL result;
	GetProperty(0x5, VT_BOOL, (void*)&result);
	return result;
}

void IFunction::SetReviewBeforePrint(BOOL propVal)
{
	SetProperty(0x5, VT_BOOL, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IFunction operations

long IFunction::GetId()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IFunction::GetKind()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFunction::GetInputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFunction::GetOutputVars()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDatabaseList properties

/////////////////////////////////////////////////////////////////////////////
// IDatabaseList operations

long IDatabaseList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatabaseList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}

LPDISPATCH IDatabaseList::FindByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IDatabaseList::FindByID(long ID)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ID);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFunctionList properties

/////////////////////////////////////////////////////////////////////////////
// IFunctionList operations

long IFunctionList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFunctionList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}

LPDISPATCH IFunctionList::FindByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IFunctionList::FindByID(long ID)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ID);
	return result;
}

LPDISPATCH IFunctionList::CreateExternalFunction()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDBDef properties

CString IDBDef::GetDriverType()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void IDBDef::SetDriverType(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

CString IDBDef::GetAlias()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void IDBDef::SetAlias(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

BOOL IDBDef::GetIsDelimited()
{
	BOOL result;
	GetProperty(0x3, VT_BOOL, (void*)&result);
	return result;
}

void IDBDef::SetIsDelimited(BOOL propVal)
{
	SetProperty(0x3, VT_BOOL, propVal);
}

CString IDBDef::GetDelimiter()
{
	CString result;
	GetProperty(0x4, VT_BSTR, (void*)&result);
	return result;
}

void IDBDef::SetDelimiter(LPCTSTR propVal)
{
	SetProperty(0x4, VT_BSTR, propVal);
}

CString IDBDef::GetSeparator()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void IDBDef::SetSeparator(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

CString IDBDef::GetDBPassword()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void IDBDef::SetDBPassword(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// IDBDef operations


/////////////////////////////////////////////////////////////////////////////
// IFieldList properties

/////////////////////////////////////////////////////////////////////////////
// IFieldList operations

long IFieldList::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFieldList::Item(long Index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Index);
	return result;
}

LPDISPATCH IFieldList::FindByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDBField properties

/////////////////////////////////////////////////////////////////////////////
// IDBField operations

long IDBField::GetNumber()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDBField::GetName()
{
	CString result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IDBField::GetLength()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IDBField::GetOffset()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IDBField::GetIsSelected()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDBField::GetVariable()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}
