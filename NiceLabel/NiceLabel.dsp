# Microsoft Developer Studio Project File - Name="NiceLabel" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=NiceLabel - Win32 IP Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "NiceLabel.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "NiceLabel.mak" CFG="NiceLabel - Win32 IP Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "NiceLabel - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "NiceLabel - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "NiceLabel - Win32 IP Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "NiceLabel - Win32 IP Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "NiceLabel - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\Editor\DLL\Common" /D "__BUILD_NICELABEL__" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "VALVEJET" /D "NDEBUG" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386
# Begin Custom Build - Updating \FoxJet\$(TargetName).dll
TargetPath=.\Release\NiceLabel.dll
TargetName=NiceLabel
InputPath=.\Release\NiceLabel.dll
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy            $(TargetPath)            \FoxJet\ 

# End Custom Build

!ELSEIF  "$(CFG)" == "NiceLabel - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\Editor\DLL\Common" /D "__BUILD_NICELABEL__" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "VALVEJET" /D "_DEBUG" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# Begin Custom Build - Updating \FoxJet\$(TargetName).dll
TargetPath=.\Debug\NiceLabel.dll
TargetName=NiceLabel
InputPath=.\Debug\NiceLabel.dll
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy            $(TargetPath)            \FoxJet\ 

# End Custom Build

!ELSEIF  "$(CFG)" == "NiceLabel - Win32 IP Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "NiceLabel___Win32_IP_Debug"
# PROP BASE Intermediate_Dir "NiceLabel___Win32_IP_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "IP_Debug"
# PROP Intermediate_Dir "IP_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "__BUILD_NICELABEL__" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\Editor\DLL\Common" /D "__BUILD_NICELABEL__" /D "_WINDLL" /D "_AFXEXT" /D "_DEBUG" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# Begin Custom Build - Updating \FoxJet\IP\$(TargetName).dll
TargetPath=.\IP_Debug\NiceLabel.dll
TargetName=NiceLabel
InputPath=.\IP_Debug\NiceLabel.dll
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy            $(TargetPath)            \FoxJet\IP\ 

# End Custom Build

!ELSEIF  "$(CFG)" == "NiceLabel - Win32 IP Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "NiceLabel___Win32_IP_Release"
# PROP BASE Intermediate_Dir "NiceLabel___Win32_IP_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "IP_Release"
# PROP Intermediate_Dir "IP_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "__BUILD_NICELABEL__" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\Editor\DLL\Common" /D "NDEBUG" /D "__BUILD_NICELABEL__" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386
# Begin Custom Build - Updating \FoxJet\IP\$(TargetName).dll
TargetPath=.\IP_Release\NiceLabel.dll
TargetName=NiceLabel
InputPath=.\IP_Release\NiceLabel.dll
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy            $(TargetPath)            \FoxJet\IP\ 

# End Custom Build

!ENDIF 

# Begin Target

# Name "NiceLabel - Win32 Release"
# Name "NiceLabel - Win32 Debug"
# Name "NiceLabel - Win32 IP Debug"
# Name "NiceLabel - Win32 IP Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\nice3.cpp
# End Source File
# Begin Source File

SOURCE=.\NiceLabel.cpp
# End Source File
# Begin Source File

SOURCE=.\NiceLabel.def
# End Source File
# Begin Source File

SOURCE=.\NiceLabel.rc
# End Source File
# Begin Source File

SOURCE=.\NiceOLE.odl
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\nice3.h
# End Source File
# Begin Source File

SOURCE=.\NiceLabelApi.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\NiceLabel.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\NiceOLE.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
