#ifndef __NICE3_H__
#define __NICE3_H__

#include "NiceLabelApi.h"

namespace NiceLabel
{
	// Machine generated IDispatch wrapper class(es) created with ClassWizard
	/////////////////////////////////////////////////////////////////////////////
	// TOcControlEvent wrapper class

	class NICELABEL_API TOcControlEvent : public COleDispatchDriver
	{
	public:
		TOcControlEvent() {}		// Calls COleDispatchDriver default constructor
		TOcControlEvent(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		TOcControlEvent(const TOcControlEvent& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long Click();
		long DblClick();
		long MouseDown(short Button, short Shift, long X, long Y);
		long MouseUp(short Button, short Shift, long X, long Y);
		long MouseMove(short Button, short Shift, long X, long Y);
		long KeyDown(short* KeyCode, short Shift);
		long KeyUp(short* KeyCode, short Shift);
		long ErrorEvent(short Number, LPCTSTR Description, long SCode, LPCTSTR Source, LPCTSTR HelpFile, long HelpContext, BOOL* CancelDisplay);
		long CustomEvent(long* Number);
	};
	/////////////////////////////////////////////////////////////////////////////
	// TOcControl wrapper class

	class NICELABEL_API TOcControl : public COleDispatchDriver
	{
	public:
		TOcControl() {}		// Calls COleDispatchDriver default constructor
		TOcControl(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		TOcControl(const TOcControl& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		BOOL GetVisible();
		void SetVisible(BOOL);
		BOOL GetCancel();
		void SetCancel(BOOL);
		BOOL GetDefault();
		void SetDefault(BOOL);
		CString GetName();
		void SetName(LPCTSTR);
		long GetLeft();
		void SetLeft(long);
		long GetTop();
		void SetTop(long);
		long GetWidth();
		void SetWidth(long);
		long GetHeight();
		void SetHeight(long);
		long GetBackColor();
		void SetBackColor(long);
		long GetForeColor();
		void SetForeColor(long);
		long GetLocaleID();
		void SetLocaleID(long);
		short GetTextAlign();
		void SetTextAlign(short);
		BOOL GetMessageReflect();
		void SetMessageReflect(BOOL);
		BOOL GetUserMode();
		void SetUserMode(BOOL);
		BOOL GetUIDead();
		void SetUIDead(BOOL);
		BOOL GetShowGrabHandles();
		void SetShowGrabHandles(BOOL);
		BOOL GetShowHatching();
		void SetShowHatching(BOOL);
		BOOL GetDisplayAsDefault();
		void SetDisplayAsDefault(BOOL);
		BOOL GetSupportsMnemonics();
		void SetSupportsMnemonics(BOOL);
		CString GetDisplayName();
		void SetDisplayName(LPCTSTR);
		CString GetScaleUnits();
		void SetScaleUnits(LPCTSTR);
		LPDISPATCH GetFont();
		void SetFont(LPDISPATCH);

	// Operations
	public:
		LPDISPATCH GetParent();
	};
	/////////////////////////////////////////////////////////////////////////////
	// TOcxView wrapper class

	class NICELABEL_API TOcxView : public COleDispatchDriver
	{
	public:
		TOcxView() {}		// Calls COleDispatchDriver default constructor
		TOcxView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		TOcxView(const TOcxView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		long GetBackColor();
		void SetBackColor(long);
		long GetForeColor();
		void SetForeColor(long);
		long GetLocaleID();
		void SetLocaleID(long);
		short GetTextAlign();
		void SetTextAlign(short);
		BOOL GetMessageReflect();
		void SetMessageReflect(BOOL);
		BOOL GetUserMode();
		void SetUserMode(BOOL);
		BOOL GetUIDead();
		void SetUIDead(BOOL);
		BOOL GetShowGrabHandles();
		void SetShowGrabHandles(BOOL);
		BOOL GetShowHatching();
		void SetShowHatching(BOOL);
		BOOL GetDisplayAsDefault();
		void SetDisplayAsDefault(BOOL);
		BOOL GetSupportsMnemonics();
		void SetSupportsMnemonics(BOOL);
		CString GetDisplayName();
		void SetDisplayName(LPCTSTR);
		CString GetScaleUnits();
		void SetScaleUnits(LPCTSTR);
		LPDISPATCH GetFont();
		void SetFont(LPDISPATCH);

	// Operations
	public:
	};
	/////////////////////////////////////////////////////////////////////////////
	// INiceLabel wrapper class

	class NICELABEL_API INiceLabel : public COleDispatchDriver
	{
	public:
		INiceLabel() {}		// Calls COleDispatchDriver default constructor
		INiceLabel(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		INiceLabel(const INiceLabel& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetLabelFileName();
		void SetLabelFileName(LPCTSTR);
		CString GetPrinterName();
		void SetPrinterName(LPCTSTR);

	// Operations
	public:
		LPDISPATCH GetApplication();
		BOOL Save();
		BOOL Print(LPCTSTR Quantity);
		BOOL ExecuteMacro(LPCTSTR Macro);
		BOOL ExportToPocket(LPCTSTR ExportDir);
		BOOL SessionStart();
		BOOL SessionPrint(LPCTSTR Quantity);
		BOOL SessionEnd();
		BOOL GetLabelPreview(LPCTSTR FileName, long Width, long Height);
		LPDISPATCH GetObjects();
		LPDISPATCH GetVariables();
		LPDISPATCH GetFunctions();
		LPDISPATCH GetDatabases();
		long GetCurrUnit();
		LPDISPATCH GetGlobalVariables();
		BOOL NewVariable();
		BOOL NewFunction();
		BOOL NewDatabase();
		BOOL ObjectProperties(long ID);
		BOOL VariableProperties(long ID);
		BOOL FunctionProperties(long ID);
		BOOL DatabaseProperties(long ID);
		BOOL DeleteObject(long ID);
		BOOL DeleteVariable(long ID);
		BOOL DeleteFunction(long ID);
		BOOL DeleteDatabase(long ID);
		BOOL SelectVariable(long ID);
		BOOL SelectDatabase(long ID);
		BOOL NewVariableWiz();
		BOOL NewDatabaseWiz();
	};
	/////////////////////////////////////////////////////////////////////////////
	// CNiceApp wrapper class

	class NICELABEL_API CNiceApp : public COleDispatchDriver
	{
	public:
		CNiceApp() {}		// Calls COleDispatchDriver default constructor
		CNiceApp(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		CNiceApp(const CNiceApp& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		LPDISPATCH GetApplication();
		void Quit();
		long LabelOpen(LPCTSTR FileName);
		BOOL LabelClose(long LabelID);
		BOOL LabelPrint(long LabelID, LPCTSTR Quantity);
		BOOL LabelSetVar(long LabelID, LPCTSTR Name, LPCTSTR Value, long Step, long Count);
		long LabelGetVarCount(long LabelID);
		CString LabelGetVarName(long LabelID, long Var);
		CString LabelGetVarProperty(long LabelID, LPCTSTR VarName, LPCTSTR PropName);
		CString GetErrorMessage();
		CString GetDetailedMessage();
		BOOL JobRun(LPCTSTR FileName);
		BOOL Login(LPCTSTR UserName, long Level);
		BOOL ExecuteMacro(long LabelID, LPCTSTR Macro);
		BOOL CloseForm(long ID);
		BOOL LabelSetPrinter(long LabelID, LPCTSTR PrinterName);
		BOOL LabelSetPrintJobName(long LabelID, LPCTSTR PrinterName);
		BOOL LabelTestConnection(long LabelID, LPCTSTR FileName);
		long GetErrorID();
		BOOL LabelExportToPocket(long LabelID, LPCTSTR ExportDir);
		BOOL LabelSessionStart(long LabelID);
		BOOL LabelSessionPrint(long LabelID, LPCTSTR Quantity);
		BOOL LabelSessionEnd(long LabelID);
		LPDISPATCH LabelOpenEx(LPCTSTR FileName);
		LPDISPATCH LabelCreate();
		BOOL TestConnection();
		BOOL InitConnection();
		BOOL LabelImport(long FilterID, long FormatID, LPCTSTR ImportFile, LPCTSTR ExportFile, BOOL EmbedGraphics);
	};
	/////////////////////////////////////////////////////////////////////////////
	// ILabel wrapper class

	class NICELABEL_API ILabel : public COleDispatchDriver
	{
	public:
		ILabel() {}		// Calls COleDispatchDriver default constructor
		ILabel(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		ILabel(const ILabel& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetPrinterName();
		void SetPrinterName(LPCTSTR);

	// Operations
	public:
		LPDISPATCH GetObjects();
		LPDISPATCH GetVariables();
		LPDISPATCH GetFunctions();
		LPDISPATCH GetDatabases();
		long GetCurrUnit();
		LPDISPATCH GetGlobalVariables();
		BOOL NewVariable();
		BOOL NewFunction();
		BOOL NewDatabase();
		BOOL ObjectProperties(long ID);
		BOOL VariableProperties(long ID);
		BOOL FunctionProperties(long ID);
		BOOL DatabaseProperties(long ID);
		BOOL DeleteObject(long ID);
		BOOL DeleteVariable(long ID);
		BOOL DeleteFunction(long ID);
		BOOL DeleteDatabase(long ID);
		BOOL SelectVariable(long ID);
		BOOL SelectDatabase(long ID);
		BOOL NewVariableWiz();
		BOOL NewDatabaseWiz();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IObjectList wrapper class

	class NICELABEL_API IObjectList : public COleDispatchDriver
	{
	public:
		IObjectList() {}		// Calls COleDispatchDriver default constructor
		IObjectList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IObjectList(const IObjectList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IBarcode wrapper class

	class NICELABEL_API IBarcode : public COleDispatchDriver
	{
	public:
		IBarcode() {}		// Calls COleDispatchDriver default constructor
		IBarcode(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IBarcode(const IBarcode& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);
		BOOL GetPrintAsGraphics();
		void SetPrintAsGraphics(BOOL);
		BOOL GetHasQuietZone();
		void SetHasQuietZone(BOOL);
		BOOL GetIncludeCD();
		void SetIncludeCD(BOOL);
		BOOL GetAutoCDCalculation();
		void SetAutoCDCalculation(BOOL);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
		CString GetContents();
		CString GetBarcodeType();
		long SetContents(LPCTSTR Value);
		CString GetProperty_(LPCTSTR Name);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IGraphics wrapper class

	class NICELABEL_API IGraphics : public COleDispatchDriver
	{
	public:
		IGraphics() {}		// Calls COleDispatchDriver default constructor
		IGraphics(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IGraphics(const IGraphics& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);
		BOOL GetHorzMirror();
		void SetHorzMirror(BOOL);
		BOOL GetVertMirror();
		void SetVertMirror(BOOL);
		BOOL GetOnMemoryCard();
		void SetOnMemoryCard(BOOL);
		long GetResizeMode();
		void SetResizeMode(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
		CString GetFileName();
		long Load(LPCTSTR FileName);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IRTFText wrapper class

	class NICELABEL_API IRTFText : public COleDispatchDriver
	{
	public:
		IRTFText() {}		// Calls COleDispatchDriver default constructor
		IRTFText(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IRTFText(const IRTFText& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IParagraph wrapper class

	class NICELABEL_API IParagraph : public COleDispatchDriver
	{
	public:
		IParagraph() {}		// Calls COleDispatchDriver default constructor
		IParagraph(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IParagraph(const IParagraph& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);
		CString GetFontName();
		void SetFontName(LPCTSTR);
		BOOL GetIsItalic();
		void SetIsItalic(BOOL);
		BOOL GetIsBold();
		void SetIsBold(BOOL);
		BOOL GetIsInverse();
		void SetIsInverse(BOOL);
		BOOL GetIsMirror();
		void SetIsMirror(BOOL);
		long GetPtSizeX();
		void SetPtSizeX(long);
		long GetPtSizeY();
		void SetPtSizeY(long);
		long GetSpacingX();
		void SetSpacingX(long);
		long GetSpacingY();
		void SetSpacingY(long);
		BOOL GetBestFit();
		void SetBestFit(BOOL);
		long GetMinSizeX();
		void SetMinSizeX(long);
		long GetMinSizeY();
		void SetMinSizeY(long);
		long GetMaxSizeX();
		void SetMaxSizeX(long);
		long GetMaxSizeY();
		void SetMaxSizeY(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
		CString GetContents();
		long SetContents(LPCTSTR Value);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IText wrapper class

	class NICELABEL_API IText : public COleDispatchDriver
	{
	public:
		IText() {}		// Calls COleDispatchDriver default constructor
		IText(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IText(const IText& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);
		CString GetFontName();
		void SetFontName(LPCTSTR);
		BOOL GetIsItalic();
		void SetIsItalic(BOOL);
		BOOL GetIsBold();
		void SetIsBold(BOOL);
		BOOL GetIsInverse();
		void SetIsInverse(BOOL);
		BOOL GetIsMirror();
		void SetIsMirror(BOOL);
		long GetPtSizeX();
		void SetPtSizeX(long);
		long GetPtSizeY();
		void SetPtSizeY(long);
		long GetSpacingX();
		void SetSpacingX(long);
		long GetSpacingY();
		void SetSpacingY(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
		CString GetContents();
		long SetContents(LPCTSTR Value);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IRectangle wrapper class

	class NICELABEL_API IRectangle : public COleDispatchDriver
	{
	public:
		IRectangle() {}		// Calls COleDispatchDriver default constructor
		IRectangle(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IRectangle(const IRectangle& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);
		long GetThicknessX();
		void SetThicknessX(long);
		long GetThicknessY();
		void SetThicknessY(long);
		long GetRadius();
		void SetRadius(long);
		long GetLineStyle();
		void SetLineStyle(long);
		long GetFillStyle();
		void SetFillStyle(long);
		BOOL GetIsRounded();
		void SetIsRounded(BOOL);
		BOOL GetPrintAsGraphics();
		void SetPrintAsGraphics(BOOL);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IOleObject wrapper class

	class NICELABEL_API ImOleObject : public COleDispatchDriver
	{
	public:
		ImOleObject() {}		// Calls COleDispatchDriver default constructor
		ImOleObject(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		ImOleObject(const ImOleObject& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IObject wrapper class

	class NICELABEL_API IObject : public COleDispatchDriver
	{
	public:
		IObject() {}		// Calls COleDispatchDriver default constructor
		IObject(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IObject(const IObject& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		BOOL GetSelected();
		void SetSelected(BOOL);
		long GetRotation();
		void SetRotation(long);
		BOOL GetIsLocked();
		void SetIsLocked(BOOL);
		long GetAnchorPoint();
		void SetAnchorPoint(long);
		long GetAnchorElementID();
		void SetAnchorElementID(long);
		long GetAnchorLevel();
		void SetAnchorLevel(long);
		long GetFormatID();
		void SetFormatID(long);
		long GetZOrder();
		void SetZOrder(long);
		long GetPageNumber();
		void SetPageNumber(long);

	// Operations
	public:
		long GetId();
		long GetLeft();
		long GetTop();
		long GetWidth();
		long GetHeight();
		long GetKind();
		long GetStatus();
		long GetRotateFlag();
		long GetResizeFlag();
		LPDISPATCH GetVariable();
		void Move(long X, long Y);
		void Resize(long Width, long Height);
		long SetVariable(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IVariableList wrapper class

	class NICELABEL_API IVariableList : public COleDispatchDriver
	{
	public:
		IVariableList() {}		// Calls COleDispatchDriver default constructor
		IVariableList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IVariableList(const IVariableList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
		LPDISPATCH FindByName(LPCTSTR Name);
		LPDISPATCH FindByID(long ID);
		LPDISPATCH Create(LPCTSTR Name);
		LPDISPATCH CreatePrompted(LPCTSTR Name);
		BOOL Add(long ID);
		BOOL Remove(long ID);
		BOOL Delete(long ID);
		BOOL FlushList();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IVar wrapper class

	class NICELABEL_API IVar : public COleDispatchDriver
	{
	public:
		IVar() {}		// Calls COleDispatchDriver default constructor
		IVar(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IVar(const IVar& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		CString GetDescription();
		void SetDescription(LPCTSTR);
		CString GetPrefix();
		void SetPrefix(LPCTSTR);
		CString GetSuffix();
		void SetSuffix(LPCTSTR);
		long GetJustificationType();
		void SetJustificationType(long);
		CString GetPadCharacter();
		void SetPadCharacter(LPCTSTR);
		BOOL GetFixedLength();
		void SetFixedLength(BOOL);
		long GetLength();
		void SetLength(long);
		BOOL GetIsMultiLine();
		void SetIsMultiLine(BOOL);
		BOOL GetWordWrap();
		void SetWordWrap(BOOL);
		BOOL GetBreakLines();
		void SetBreakLines(BOOL);
		long GetLineLength();
		void SetLineLength(long);
		long GetLineCount();
		void SetLineCount(long);
		BOOL GetIsVarQuantity();
		void SetIsVarQuantity(BOOL);
		BOOL GetStrictChecking();
		void SetStrictChecking(BOOL);
		BOOL GetHasMinValue();
		void SetHasMinValue(BOOL);
		CString GetMinValue();
		void SetMinValue(LPCTSTR);
		BOOL GetHasMaxValue();
		void SetHasMaxValue(BOOL);
		CString GetMaxValue();
		void SetMaxValue(LPCTSTR);
		long GetFormatID();
		void SetFormatID(long);
		BOOL GetIsBasedOnQuantity();
		void SetIsBasedOnQuantity(BOOL);
		long GetPromptCount();
		void SetPromptCount(long);
		CString GetPrompt();
		void SetPrompt(LPCTSTR);
		CString GetDefaultValue();
		void SetDefaultValue(LPCTSTR);
		long GetDefType();
		void SetDefType(long);
		BOOL GetDynamicValue();
		void SetDynamicValue(BOOL);
		BOOL GetValueRequired();
		void SetValueRequired(BOOL);
		long GetIncrementCount();
		void SetIncrementCount(long);
		long GetIncrementStep();
		void SetIncrementStep(long);
		long GetIncrementStep2();
		void SetIncrementStep2(long);
		long GetIncrementStep3();
		void SetIncrementStep3(long);
		BOOL GetRollOver();
		void SetRollOver(BOOL);
		BOOL GetTraceOn();
		void SetTraceOn(BOOL);
		long GetPictureType();
		void SetPictureType(long);
		CString GetInputPicture();
		void SetInputPicture(LPCTSTR);
		CString GetOutputPicture();
		void SetOutputPicture(LPCTSTR);

	// Operations
	public:
		long GetId();
		long GetVarType();
		long GetIncrementType();
		long GetIncrementKind();
		BOOL GetIsUsed();
		CString GetDefault();
		long SetValue(LPCTSTR Value);
		CString GetValue();
		BOOL DependsOnFunction(long FunctionID);
		BOOL ShouldTraceNow(BOOL bStart);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IExtFunction wrapper class

	class NICELABEL_API IExtFunction : public COleDispatchDriver
	{
	public:
		IExtFunction() {}		// Calls COleDispatchDriver default constructor
		IExtFunction(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IExtFunction(const IExtFunction& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		CString GetDescription();
		void SetDescription(LPCTSTR);
		BOOL GetReviewBeforePrint();
		void SetReviewBeforePrint(BOOL);
		CString GetDefinition();
		void SetDefinition(LPCTSTR);

	// Operations
	public:
		long GetId();
		long GetKind();
		LPDISPATCH GetInputVars();
		LPDISPATCH GetOutputVars();
		CString GetProviderName();
		BOOL GetIsContentsProvider();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IDatabase wrapper class

	class NICELABEL_API IDatabase : public COleDispatchDriver
	{
	public:
		IDatabase() {}		// Calls COleDispatchDriver default constructor
		IDatabase(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IDatabase(const IDatabase& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		CString GetDescription();
		void SetDescription(LPCTSTR);
		BOOL GetReviewBeforePrint();
		void SetReviewBeforePrint(BOOL);
		CString GetDriverType();
		void SetDriverType(LPCTSTR);
		CString GetAlias();
		void SetAlias(LPCTSTR);
		BOOL GetIsDelimited();
		void SetIsDelimited(BOOL);
		CString GetDelimiter();
		void SetDelimiter(LPCTSTR);
		CString GetSeparator();
		void SetSeparator(LPCTSTR);
		CString GetDBPassword();
		void SetDBPassword(LPCTSTR);
		CString GetTable();
		void SetTable(LPCTSTR);
		CString GetOrder();
		void SetOrder(LPCTSTR);
		long GetNavigation();
		void SetNavigation(long);
		CString GetSql();
		void SetSql(LPCTSTR);

	// Operations
	public:
		long GetId();
		long GetKind();
		LPDISPATCH GetInputVars();
		LPDISPATCH GetOutputVars();
		LPDISPATCH GetFields();
		LPDISPATCH GetParameters();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IParameterList wrapper class

	class NICELABEL_API IParameterList : public COleDispatchDriver
	{
	public:
		IParameterList() {}		// Calls COleDispatchDriver default constructor
		IParameterList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IParameterList(const IParameterList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
		LPDISPATCH Create(LPCTSTR Field);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IDBParameter wrapper class

	class NICELABEL_API IDBParameter : public COleDispatchDriver
	{
	public:
		IDBParameter() {}		// Calls COleDispatchDriver default constructor
		IDBParameter(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IDBParameter(const IDBParameter& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetField();
		void SetField(LPCTSTR);
		long GetRelation();
		void SetRelation(long);

	// Operations
	public:
		LPDISPATCH GetVariable();
		long SetVariable(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IFunction wrapper class

	class NICELABEL_API IFunction : public COleDispatchDriver
	{
	public:
		IFunction() {}		// Calls COleDispatchDriver default constructor
		IFunction(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IFunction(const IFunction& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetName();
		void SetName(LPCTSTR);
		CString GetDescription();
		void SetDescription(LPCTSTR);
		BOOL GetReviewBeforePrint();
		void SetReviewBeforePrint(BOOL);

	// Operations
	public:
		long GetId();
		long GetKind();
		LPDISPATCH GetInputVars();
		LPDISPATCH GetOutputVars();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IDatabaseList wrapper class

	class NICELABEL_API IDatabaseList : public COleDispatchDriver
	{
	public:
		IDatabaseList() {}		// Calls COleDispatchDriver default constructor
		IDatabaseList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IDatabaseList(const IDatabaseList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
		LPDISPATCH FindByName(LPCTSTR Name);
		LPDISPATCH FindByID(long ID);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IFunctionList wrapper class

	class NICELABEL_API IFunctionList : public COleDispatchDriver
	{
	public:
		IFunctionList() {}		// Calls COleDispatchDriver default constructor
		IFunctionList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IFunctionList(const IFunctionList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
		LPDISPATCH FindByName(LPCTSTR Name);
		LPDISPATCH FindByID(long ID);
		LPDISPATCH CreateExternalFunction();
	};
	/////////////////////////////////////////////////////////////////////////////
	// IDBDef wrapper class

	class NICELABEL_API IDBDef : public COleDispatchDriver
	{
	public:
		IDBDef() {}		// Calls COleDispatchDriver default constructor
		IDBDef(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IDBDef(const IDBDef& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:
		CString GetDriverType();
		void SetDriverType(LPCTSTR);
		CString GetAlias();
		void SetAlias(LPCTSTR);
		BOOL GetIsDelimited();
		void SetIsDelimited(BOOL);
		CString GetDelimiter();
		void SetDelimiter(LPCTSTR);
		CString GetSeparator();
		void SetSeparator(LPCTSTR);
		CString GetDBPassword();
		void SetDBPassword(LPCTSTR);

	// Operations
	public:
	};
	/////////////////////////////////////////////////////////////////////////////
	// IFieldList wrapper class

	class NICELABEL_API IFieldList : public COleDispatchDriver
	{
	public:
		IFieldList() {}		// Calls COleDispatchDriver default constructor
		IFieldList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IFieldList(const IFieldList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetCount();
		LPDISPATCH Item(long Index);
		LPDISPATCH FindByName(LPCTSTR Name);
	};
	/////////////////////////////////////////////////////////////////////////////
	// IDBField wrapper class

	class NICELABEL_API IDBField : public COleDispatchDriver
	{
	public:
		IDBField() {}		// Calls COleDispatchDriver default constructor
		IDBField(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
		IDBField(const IDBField& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
	public:

	// Operations
	public:
		long GetNumber();
		CString GetName();
		long GetLength();
		long GetOffset();
		BOOL GetIsSelected();
		LPDISPATCH GetVariable();
	};
}; //namespace NiceLabel

#endif //__NICE3_H__
