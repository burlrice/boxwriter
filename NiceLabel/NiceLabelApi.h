#ifndef __NICELABELAPI_H__
#define __NICELABELAPI_H__

#ifdef __BUILD_NICELABEL__
	#define NICELABEL_API __declspec(dllexport)
#else
	#define NICELABEL_API __declspec(dllimport)
#endif //__BUILD_NICELABEL__

#endif //#ifndef __NICELABELAPI_H__
